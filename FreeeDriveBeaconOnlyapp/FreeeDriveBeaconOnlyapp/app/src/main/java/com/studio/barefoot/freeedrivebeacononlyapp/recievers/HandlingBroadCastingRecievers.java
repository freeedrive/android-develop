package com.studio.barefoot.freeedrivebeacononlyapp.recievers;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Chronometer;

import com.studio.barefoot.freeedrivebeacononlyapp.ScoreSynchronizationActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.RidesBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.SubRideBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.sensors.FDsensors;
import com.studio.barefoot.freeedrivebeacononlyapp.services.BackgroundBeaconScan;
import com.studio.barefoot.freeedrivebeacononlyapp.services.Detector;
import com.studio.barefoot.freeedrivebeacononlyapp.services.FDGPSTracker;
import com.studio.barefoot.freeedrivebeacononlyapp.services.LocationService;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.studio.barefoot.freeedrivebeacononlyapp.services.LocationService.accelerate;
import static com.studio.barefoot.freeedrivebeacononlyapp.services.LocationService.braking;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.subRideBeansArrayList;

/**
 * Created by mcs on 1/11/2017.
 */

public class HandlingBroadCastingRecievers extends BroadcastReceiver {
    /**
     * Allows to handle database
     */
    private RideBDD rideBDD;
    private RidesBeans rideFromDB;
    private Boolean isAlreadySaved = false;
    private boolean rideEnd;
    BluetoothAdapter bluetoothadapter;
    Context context;
    public static int reason_ride_end = 0;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        String action = intent.getAction();
        rideBDD = new RideBDD(context);
        //Reciever to handle beacon advertising of i am in range or out
        //rideEnd = intent.getBooleanExtra("RIDE_END_FLAG",false);
        bluetoothadapter = BluetoothAdapter.getDefaultAdapter();

        if (action.equalsIgnoreCase("RIDE_END_FLAG")) {

            if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)){
                AppUtils.startLocService=false;
                try{
                    long totalTimeOver120 = 0;

                    if(LocationService.timeSave.size() == 0){

                        Log.e("No Rides","Size 0");

                    }
                    else{
                        for(int i=0;i<LocationService.timeSave.size();i++){
                            //Current Rides Bean Object
                            RidesBeans rb = (RidesBeans) LocationService.timeSave.get(i);

                            //Time Differene in Current RidesBean Object
                            long timeOver = rb.getEndtime().getTime() - rb.getStarttime().getTime();

                            totalTimeOver120 += timeOver;

                        }
                        LocationService.totalTimeinSeconds = totalTimeOver120 / 1000L;
                        Log.e("currentTime","totaltimeover120 :"+totalTimeOver120);
                        /*ridesBeans = null;

                        Log.e("currentTime","totalTimeinSeconds :"+totalTimeinSeconds);*/
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
                stopFreeeDriveAfter8mins(context);
               // stopFreeeDrive(context);
               /* Intent stopsrvc = new Intent(context, LocationService.class);
                Intent stopsrvc1 = new Intent(context, BackgroundBeaconScan.class);
                context.stopService(stopsrvc);
                context.stopService(stopsrvc1);

                context.startService(stopsrvc1);*/

            }
        }

        if (action.equalsIgnoreCase("IamInRange")) {
            startFreeeDrive(context);
        }
        if (action.equalsIgnoreCase("IamOutOfRange")) {
            if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)){
                AppUtils.startLocService=false;
                long totalTimeOver120 = 0;

                try {
                    if(LocationService.timeSave.size() == 0){
                        Log.e("ArraySize","Is 0");
                    }
                    else{
                        for(int i=0;i<LocationService.timeSave.size();i++){
                            //Current Rides Bean Object
                            RidesBeans rb = (RidesBeans) LocationService.timeSave.get(i);

                            //Time Differene in Current RidesBean Object
                            long timeOver = rb.getEndtime().getTime() - rb.getStarttime().getTime();

                            totalTimeOver120 += timeOver;

                        }
                        LocationService.totalTimeinSeconds = totalTimeOver120 / 1000;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                stopFreeeDrive(context);
            }
        }
        if(action.equalsIgnoreCase(BluetoothAdapter.ACTION_STATE_CHANGED)) {

            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    AppUtils.speedlessCheck =true;
                   // bluetoothadapter.disable();
                         // stopFDSensor();
                    //1 : have to ask
              /*      if (AppConstants.isLocationActive) {
                        Intent stopsrvc = new Intent(context,LocationService.class);
                        context.stopService(stopsrvc);
                    }*/
               /*     Log.e("turnOnBT","turnOnBT");
                    context.stopService(new Intent(context,BackgroundBeaconScan.class));
                    if (AppUtils.rideWasActive) {

                        stopFreeeDrive(context);
                    }*/
                    break;
                case BluetoothAdapter.STATE_ON:

                 //   bluetoothadapter.enable();


              /*        if (AppUtils.isInRange && AppUtils.speedCheck){
                          if (BackgroundBeaconScan.beaconFd!=null ){
                              BackgroundBeaconScan.beaconFd.cancel();
                          }
                          startFreeeDrive(context);
                          context.startService(new Intent(context, BackgroundBeaconScan.class));
                      }*/


            }
        }


    }
    private void stopFreeeDriveAfter8mins(Context context){
        try{
            Long arrivaleTIme= System.currentTimeMillis()/1000L;
            DataHandler.updatePreferences(AppConstants.REDUCTED_ARRIVAL,arrivaleTIme);
            //  String time =AppUtils.formateLongToOnlyDateForServer(arrivaleTIme);
            String time =AppUtils.formate10LongDateToDisplay(arrivaleTIme);

            DataHandler.updatePreferences(AppConstants.KEY_ARRIVAL,time);
            stopFDSensor();
           // startFDTrackerGPS();
            reason_ride_end = 1;
            manageStopChronometer(context);
            DataHandler.updatePreferences(AppConstants.RIDE_WAS_ACTIVE,false);
          //  stopFDTrackerGPS();
            LocationService.currentSpeed = 0;
            subRideBeansArrayList.clear();
            Detector.FdApp.departureLocation = null;
            Detector.FdApp.arrivalLocation = null;
            Detector.FdApp.departure_time = 0;
            Detector.FdApp.arrival_time = 0;
        }catch (Exception e){
            Log.e("Handling,Exception1",""+e);
        }



    }

    private void InsertSubRides(Context context) {

        RideBDD rideBDD = new RideBDD(context);
        rideBDD.open();
        int id = rideBDD.getRideID();
        if (subRideBeansArrayList != null && !subRideBeansArrayList.isEmpty() && subRideBeansArrayList.size() > 1) {
            for (int i = 0; i < subRideBeansArrayList.size(); i++) {
                SubRideBeans subRideBeans = (SubRideBeans) subRideBeansArrayList.get(i);

                String areaType = subRideBeans.getAreaType();
                String startLat = subRideBeans.getStartlat();
                String startLang = subRideBeans.getStartlng();
                String endLat = subRideBeans.getEndlat();
                String endLang = subRideBeans.getEndlng();
                Long currentTime = subRideBeans.getCurrentTime();
                double currentSpeed = subRideBeans.getCurrentSpeed();
                int timeInsideARegion = subRideBeans.getTimeInsideAregion();
                rideBDD.InsertSubRides(id, areaType, startLat, startLang, endLat, endLang, currentTime, currentSpeed,timeInsideARegion);
            }

        }

        rideBDD.close();
       //
    }

    private void stopFreeeDrive(Context context){
        try{
            Long arrivaleTIme= System.currentTimeMillis()/1000L;
            DataHandler.updatePreferences(AppConstants.REDUCTED_ARRIVAL,arrivaleTIme);
            //  String time =AppUtils.formateLongToOnlyDateForServer(arrivaleTIme);
            String time =AppUtils.formate10LongDateToDisplay(arrivaleTIme);

            DataHandler.updatePreferences(AppConstants.KEY_ARRIVAL,time);
            stopFDSensor();
            //startFDTrackerGPS();
            reason_ride_end = 2;
            manageStopChronometer(context);
            DataHandler.updatePreferences(AppConstants.RIDE_WAS_ACTIVE,false);
            stopFDTrackerGPS();
            LocationService.currentSpeed = 0;
            subRideBeansArrayList.clear();

        }catch (Exception e){
              Log.e("Handling,Exception1",""+e);
        }



    }


    /**
     * Allows to start the tracker Location
     */
    private void startFDTrackerGPS(){
        if(Detector.FdApp.fdgpsTracker == null) {
            Detector.FdApp.fdgpsTracker = new FDGPSTracker(Detector.FdApp);
        }

        Detector.FdApp.fdgpsTracker.startFdTrackerLocation();
    }
    private void startFreeeDrive(Context context){


        Long departureTime= System.currentTimeMillis()/1000L;
        //  String time =AppUtils.formateLongToOnlyDateForServer(arrivaleTIme);
        String time =AppUtils.formate10LongDateToDisplay(departureTime);
        DataHandler.updatePreferences(AppConstants.KEY_DEPARTURE,time);
        DataHandler.updatePreferences(AppConstants.RIDE_WAS_ACTIVE,true);
        checkIfGpsIsEnabled(context);
      //  startFDTrackerGPS();

        //Test new journey:
       // isNewJourney();
        manageStartChronometer(context);
        startFDSensor();
    }

    /**
     * Allows to stop the tracker Location
     */
    private void stopFDTrackerGPS(){
        Detector.FdApp.fdgpsTracker.stopFdTrackerLocation();
    }

    /**
     * Allows to stop the sensor event
     */
    private void stopFDSensor(){
        Detector.FdApp.fdSensors.stopSensorsFD();
    }
    /**
     * Allows to stop a chronometer for a ride
     * @param c(in), @Activity represents the Activity
     */
    private void manageStopChronometer(Context c) {
        //launch chronometer :
        AppUtils.chronometer = new Chronometer(c);
        AppUtils.chronometer.setBase(SystemClock.elapsedRealtime());
        //   FDUtils.chronometer.start();
        AppUtils.chronometer.stop();
        AppUtils.isStarted = true;


     /*   Detector.FdApp.fdgpsTracker.setCanSaveDepartureLocation(true);
        Detector.FdApp.arrivalLocation = Detector.FdApp.fdgpsTracker.getLLLocation();
        if(Detector.FdApp.arrivalLocation != null){
            Log.e("Bfpk"," ___ Arrival location is ("+Detector.FdApp.arrivalLocation.getLatitude()+","+Detector.FdApp.arrivalLocation.getLongitude()+")  ___ ");
        }*/
        //update fit char "my last ride" :

       // Date arrival = new Date();
        Long arrival =System.currentTimeMillis()/1000L;
        Log.e("Arrival",""+arrival);
        Detector.FdApp.arrival_time = arrival;
        Detector.FdApp.fdSensors.journeyTotalTime();
        //database:
        RidesBeans ride = new RidesBeans();
        ride.setDeparture_time((Detector.FdApp.departure_time));

        //To set departure location
       if (Detector.FdApp.departureLocation!=null){


        String localityDeparture = addressFromLocation(c, Detector.FdApp.departureLocation);

        if(localityDeparture.contains(",")){
            try{
                //create a location from a string from Ride :
                String[] tabDepartureLocation = localityDeparture.split(",");
                Location locationTmpRide = new Location("Ride");
                double lat = Double.parseDouble(tabDepartureLocation[0]);
                double longi = Double.parseDouble(tabDepartureLocation[1]);
                locationTmpRide.setLatitude(lat);
                locationTmpRide.setLongitude(longi);
                String localityDepartureSplitted = addressFromLocation(c, locationTmpRide);
                Log.e("LocalityDeparture",localityDeparture);
                //save the locality :
                ride.setDeparture_location(localityDepartureSplitted);
            }catch (NumberFormatException fe){
                fe.printStackTrace();
                Log.e("E",fe.getMessage());
            }
        }else{
            ride.setDeparture_location(localityDeparture);
        }
       } else{
           ride.setDeparture_location("000");
       }
       // To set arrival location
        if (Detector.FdApp.arrivalLocation!=null) {

            String localityArrival = addressFromLocation(c, Detector.FdApp.arrivalLocation);

            if (localityArrival.contains(",")) {
                try {
                    //create a location from a string from Ride :
                    String[] tabDepartureLocation = localityArrival.split(",");
                    Location locationTmpRide = new Location("Ride");
                    double lat = Double.parseDouble(tabDepartureLocation[0]);
                    double longi = Double.parseDouble(tabDepartureLocation[1]);
                    locationTmpRide.setLatitude(lat);
                    locationTmpRide.setLongitude(longi);
                    String localityDepartureSplitted = addressFromLocation(c, locationTmpRide);
                    //save the locality :
                    ride.setArrival_location(localityDepartureSplitted);
                } catch (NumberFormatException fe) {
                    fe.printStackTrace();
                }
            } else {
                ride.setArrival_location(localityArrival);
            }
        }else{
            ride.setArrival_location("000");
        }
 /*       //in secondes :
        if(AppUtils.isNewJourney){
            //Log.i("Adneom"," is a new journey, get current change time");
            ride.setDeparture_time((Detector.FdApp.departure_time));
        }else{
            if(AppUtils.tmpRide != null){
                ride.setDeparture_time(AppUtils.tmpRide.getDeparture_time());
            }
        }*/

        ride.setArrival_time((Detector.FdApp.arrival_time));

      /*  if(Detector.FdApp.departureLocation != null) {
            if (AppUtils.isNewJourney) {
                //save the locality :
                String localityDeparture = addressFromLocation(c, Detector.FdApp.departureLocation);
                ride.setDeparture_location(localityDeparture);


            }else
            {
                if(AppUtils.tmpRide != null){
                    ride.setDeparture_location(AppUtils.tmpRide.getDeparture_location());
                    if(AppUtils.tmpRide.getDeparture_location().contains(",")){
                        try{
                            //create a location from a string from Ride :
                            String[] tabDepartureLocation = AppUtils.tmpRide.getDeparture_location().split(",");
                            Location locationTmpRide = new Location("Ride");
                            double lat = Double.parseDouble(tabDepartureLocation[0]);
                            double longi = Double.parseDouble(tabDepartureLocation[1]);
                            locationTmpRide.setAltitude(lat);
                            locationTmpRide.setLongitude(longi);
                            String localityDeparture = addressFromLocation(c, locationTmpRide);
                            Log.e("LocalityDeparture",localityDeparture);
                            //save the locality :
                            ride.setDeparture_location(localityDeparture);
                        }catch (NumberFormatException fe){
                            fe.printStackTrace();
                            Log.i("Adneom", "error " + fe.getMessage());
                            Log.e("E",fe.getMessage());
                        }
                    }
                }
            }
        }
        else{
            ride.setDeparture_location("000");
        }  if(Detector.FdApp.arrivalLocation != null) {
            ride.setArrival_location(Detector.FdApp.arrivalLocation.getLatitude() + "," + Detector.FdApp.arrivalLocation.getLongitude());
            String localityArrival = addressFromLocation(c, Detector.FdApp.arrivalLocation);
            ride.setArrival_location(localityArrival);
        }else{
            ride.setArrival_location("000");
        }
*/
        ride.setTime_elapsed(differenceTime(Detector.FdApp.departure_time, Detector.FdApp.arrival_time));

/*        if(AppUtils.isNewJourney){
            //Log.i("Adneom", "before to send : " + Detector.FdApp.fdSensors.getNewAverage());
            ride.setScore(Detector.FdApp.fdSensors.getNewAverage());// marked
            ride.setBad_behaviour(FDsensors.cptSnsors);
//            int insidetest = FDSensors.cptSnsors;
//            Toast.makeText(c, "Inside Test :"+insidetest, Toast.LENGTH_SHORT).show();
        }else{
            ride.setScore(Detector.FdApp.fdSensors.getNewAverage());
            ride.setBad_behaviour(FDsensors.cptSnsors);
        }*/
    /*    JSONObject objAccoutn = Account.getInstance().getAccount();
        if(objAccoutn != null){
            int driver_id = 0;
            int company_id = 0;
            try {
                driver_id = objAccoutn.getInt("uid");
                company_id = objAccoutn.getInt("company_id");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if(driver_id > 0 || company_id > 0){
                ride.setDriver_id(driver_id);
                ride.setCompany_id(company_id);
            }
        }
        ride.setSend(false);*/

        ride.setOrphandrideBit(AppConstants.ORPHAN_RIDE_BIT);
        ride.setScore(Detector.FdApp.fdSensors.getNewAverage());
        ride.setSend(false);
        ride.setTime(Detector.FdApp.fdSensors.getStringTime());
        //time in secondes:
        ride.setTimeSecondes(Detector.FdApp.fdSensors.getStaticTotalTime());
        //avg score :
        double tmpAVG = calculateAVGRide(ride.getTimeSecondes(),ride.getScore());
        ride.setAverageP(tmpAVG);
        ride.setTotalTimeAbove120(LocationService.totalTimeinSeconds);
        ride.setCauseofendingRide(reason_ride_end);
        ride.setSudden_accelaration(accelerate);
        ride.setSudden_braking(braking);
        ride.setBad_behaviour(FDsensors.cptSnsors);
        rideBDD.open();
        rideBDD.insertRide(ride);

 /*       if(!isAlreadySaved){
            //New journey :
            if(AppUtils.isNewJourney){
                isAlreadySaved = true;

                Log.e("Bfpk",ride.getID()+" : "+ride.getDeparture_time()+" "+ride.getArrival_time()+ "and "+ride.getTime_elapsed());
                  Log.e("ArrivalTIME",""+ride.getArrival_time());
                  Log.e("CONVERTED_TIME",AppUtils.formate10LongDateToDisplay(ride.getArrival_time()));
                long idd = rideBDD.insertRide(ride);

                Log.e("Bfpk"," *** (TestReceiver new journey) Ride "+rideBDD.getRideWithId(idd).toString()+" is saved in database *** ");

                try {
                    AppUtils.tmpRide = ride;
                    int idInt = (int)idd;
                    AppUtils.tmpRide.setID(idInt);
                }catch (Exception e){
                    Log.e("eXCEPTION",e.getMessage());
                }
            }else {
                try {
                    //if already in database :
                    rideBDD.open();
                    String localityDeparture = addressFromLocation(c, Detector.FdApp.departureLocation);
                    Log.i("localityDeparture",localityDeparture);
                    ride.setDeparture_location(localityDeparture);
                    long idd = rideBDD.insertRide(ride);
                    Log.e("Bfpk","Inside Else :"+idd);
                    // Log.i("Adneom","Inside Else Getting Ride Information"+ride.getID()+" : "+ride.getDeparture_time()+" "+ride.getArrival_time()+ "and "+ride.getTime_elapsed());
                    AppUtils.tmpRide = ride;
                    int rideId = (int)idd;
                    rideFromDB = rideBDD.getRideWithId(rideId);
                    Log.e("RideFromDB",""+rideFromDB);
                }catch (Exception e){
                    Log.e("eXEPTION_E",e.getMessage());
                }

                if(rideFromDB != null && rideFromDB.getID() > 0){

                    isAlreadySaved = true;

                    Log.i("Adneom", "(TestReceiver)update : " + AppUtils.tmpRide.toString() + " *** ");

                    //new time :
                    String tmpTime1 = AppUtils.tmpRide.getTime();
                    String tab_tmp_time1[] = tmpTime1.split(":");
                    String tmpTime2 = rideFromDB.getTime();
                    String tab_tmp_time2[] = tmpTime2.split(":");
                    int h = (Integer.parseInt(tab_tmp_time1[0]) +  Integer.parseInt(tab_tmp_time2[0]));
                    int m = (Integer.parseInt(tab_tmp_time1[1]) +  Integer.parseInt(tab_tmp_time2[1]));
                    int s = (Integer.parseInt(tab_tmp_time1[2]) +  Integer.parseInt(tab_tmp_time2[2]));
                    String newTime = h+":"+m+":"+s;
                    //Log.i("Adneom", " current : " + Integer.parseInt(tab_tmp_time1[0]) + ":" + Integer.parseInt(tab_tmp_time1[1]) + ":" + Integer.parseInt(tab_tmp_time1[2]) + " and previous : " + Integer.parseInt(tab_tmp_time2[0]) + ":" + Integer.parseInt(tab_tmp_time2[1]) + ":" + Integer.parseInt(tab_tmp_time2[2])+ " and new time is "+newTime+" **");
                    ride.setTime(newTime);

                    //new avg score :
                    double tmpAVG2 = calculateAVGRide(ride.getTimeSecondes(),ride.getScore());
                    ride.setAverageP(tmpAVG2);
                    ride.setBad_behaviour(FDsensors.cptSnsors);

                    rideBDD.updateRide(AppUtils.tmpRide.getID(), ride);
                    sendMessageToMenuActivity(c);
                }
            }
            //reset location for the next rides :

        }else{
            isAlreadySaved = false;
        }*/
        //INSERTING SUB RIDES.......
        sendMessageToMenuActivity(c);

        InsertSubRides(context);
//        LocationService.totalTimeinSeconds = 0;
        rideBDD.close();
        LocationService.timeSave = null;

        //Sync the Ride After Saving in DataBase
        try{
            if(AppUtils.isNetworkAvailable() ){
                AppUtils.rideSync(context);
        /*        Intent i = new Intent(context.getApplicationContext(), ScoreSynchronizationActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);*/
            }
            else{
                AppUtils.isUpdated = false;
                //Wifi and 4G both are not available
            }
        }catch (Exception exception){
            exception.printStackTrace();
        }
        try{
            if(reason_ride_end != 0){
            reason_ride_end = 0;
                accelerate = 0;
                braking = 0;
            }
            if(AppConstants.ORPHAN_RIDE_BIT !=0){
                AppConstants.ORPHAN_RIDE_BIT = 0;
            }
            if(BackgroundBeaconScan.beaconlostCounter !=0){
                BackgroundBeaconScan.beaconlostCounter = 0;
            }

        }catch (NullPointerException e){
            e.printStackTrace();
        }

    }



    /**
     * Send an Intent with an action named "com.freeedrive_saving_driving.sensor_event".
     * The time represents the total time of a journey and the text represent the text to display in view chart are send to Menu Activity
     * to display on chart the values. Save information in the preferences
     *
     * @param context(in), @Activity represents the activity
     */
    public void sendMessageToMenuActivity(Context context){
        Intent intent = new Intent("com.freeedrive_saving_driving.sensor_event");

        //add data, app. is foreground :
        intent.putExtra("time","0");
        intent.putExtra("text","0");
        intent.putExtra("badbehaviour","0");
        intent.putExtra("value_safetyscore",0.0);
        intent.putExtra("text_safetyscore", "0");
        context.sendBroadcast(intent);
    }


    /**
     * Allows to calculate the average
     * @param sec(in), @Integer represents the secondes
     * @param score(in), @Double represents the score
     * @return(out), @Double represents the average
     */
    private double calculateAVGRide (int sec, double score){
        //Log.i("Adneom"," secondes "+sec+" and score "+score);
        return (sec * score);
    }

    /**
     * Calculate the difference between two times
     * @param departure(in), @Long is the departure time
     * @param arrival(in), @Long is the arrival time
     * @return(out), @Long the difference between departure time and arrival time
     */
    public long differenceTime(long departure, long arrival){
        long convertToMinutes=0;
        if (departure!=0 && arrival!=0){
          //  long timeDifference= TimeUnit.MILLISECONDS.toSeconds(arrival)-TimeUnit.MILLISECONDS.toSeconds(departure);
            long timeDifference= arrival-departure;

            convertToMinutes = timeDifference/60L;
            if (convertToMinutes<=1){
                convertToMinutes=1;
            }
        }else{
            convertToMinutes=1;
        }

        return (convertToMinutes);
    }
    /**
     * Get address from a location
     * @param context, @Activity represents an Activity
     * @param loc, @Location represents a location : latitude and longitude
     * @return the locality
     */
    private String addressFromLocation(Context context, Location loc){
        String locality = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {

            List<Address> addresses = geocoder.getFromLocation(loc.getLatitude(),loc.getLongitude(), 1);
            if(addresses != null && addresses.size() > 0){
                //Log.i("Adneom"," result is "+addresses);
                Address address = addresses.get(0);
                if(address != null && address.getMaxAddressLineIndex() > 0){
                    //Log.i("Adneom"," locality "+address.getLocality());
                    locality = address.getLocality();
                    Log.e("Locality",address.getAdminArea()+"--"+address.getSubAdminArea()+"=="+address.getPremises()+"=="+address.getSubLocality()+"=="+address);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("E", e.getMessage());
            //Log.i("Adneom","error is "+e.getMessage());
        }

        try{
            if(locality.equalsIgnoreCase("")){
                locality = loc.getLatitude()+","+loc.getLongitude();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return locality;
    }
    /**
     * Allows to start the chronometer for a ride
     */
    private void manageStartChronometer(Context c){
        //was a reboot :
    /* if(Detector.FdApp.fdgpsTracker == null) {
            Detector.FdApp.fdgpsTracker = new FDGPSTracker(Detector.FdApp);
        }*/

        //reset cptSensor :
        Detector.cptSnsors = 0;
        //reset totaltimeinseconds over 120km
        LocationService.totalTimeinSeconds = 0;
      //  Date departureTime = new Date();
        Long departureTime = System.currentTimeMillis()/1000L;
        Log.e("FIRED","STARTED");
        Detector.FdApp.departure_time = departureTime;
        String dateFormatted = AppUtils.formate10LongDateToDisplay(departureTime);
        Log.e("DepartureTime",dateFormatted+" long >"+ Detector.FdApp.departure_time);
        AppUtils.chronometer = new Chronometer(c);
        AppUtils.chronometer.setBase(SystemClock.elapsedRealtime());
        AppUtils.chronometer.start();
        AppUtils.isStarted = true;

        //current location :

        /*Detector.FdApp.departureLocation = Detector.FdApp.fdgpsTracker.getLLLocation();
        if(Detector.FdApp.departureLocation != null){
            Log.i("Adneom","____ Departure location is ("+Detector.FdApp.departureLocation.getLatitude()+","+Detector.FdApp.departureLocation.getLongitude()+") ___ ");
        }*/
    }
    private void isNewJourney(){
        if(AppUtils.isStarted){

            //set totaltimeinsecond over 120km to = 0
            LocationService.totalTimeinSeconds = 0;

            long time2  = SystemClock.elapsedRealtime() - AppUtils.chronometer.getBase();
            Date dateAfterDisconnection = new Date(time2);
            DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
            String dateFormatted = formatter.format(dateAfterDisconnection);
            String[] tab = dateFormatted.split(":");

            //values  > 1 min:
            if(Integer.parseInt(tab[2]) >= 10 /*Integer.parseInt(tab[1]) >= 1*/){
                AppUtils.isNewJourney = true;
                //Log.i("Adneom"," *** is a new journey *** ");
            }else{
                //Log.i("Adneom"," *** is not a new journey *** ");
                //Log.i("Adneom"," The ride is "+FDUtils.tmpRide.getDriver_id()+" ***");
                AppUtils.isNewJourney = false;
            }

            AppUtils.chronometer.stop();
            AppUtils.isStarted = false;
            Log.e("isNewJourney","isNewJourney");

        }

    }
    private void startFDSensor() {
        Detector.FdApp.fdSensors.startSensorsFD();
    }

    private void checkIfGpsIsEnabled(final Context context) {
        LocationManager locManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        if(locManager != null){
            try{
                gps_enabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                //Log.i("Adneom","gps : "+gps_enabled);
                if(!gps_enabled){
                    sendAskingGPSToMenuActivity(context);
                }
            }catch (Exception ex){
                ex.printStackTrace();
                Log.e("Bfpk", "error " + ex.getMessage());
                Log.e("Bfpk",ex.getMessage());
            }
        }
    }

    private void sendAskingGPSToMenuActivity(Context context) {
        Intent intent = new Intent("com.freeedrive_saving_driving.enable_gps");
        //1 : have to ask
        intent.putExtra("asking","1");
        context.sendBroadcast(intent);
    }
}
