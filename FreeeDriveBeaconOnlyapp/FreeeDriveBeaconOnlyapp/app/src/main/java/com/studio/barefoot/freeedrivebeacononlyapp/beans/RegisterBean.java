package com.studio.barefoot.freeedrivebeacononlyapp.beans;

/**
 * Created by mcs on 12/21/2016.
 */

public class RegisterBean {

    String first_name;
    String last_name;
    String email;
    String phoneNo;
    String phoneModel;
    String device_id;
    String gcm_Key;
    String phoneName;
    int langid;

    public RegisterBean() {
        super();
    }

    public RegisterBean(String first_name, String last_name, String email, String phoneNo, String phoneModel, String device_id, String gcm_Key, String phoneName, int langid) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.phoneNo = phoneNo;
        this.phoneModel = phoneModel;
        this.device_id = device_id;
        this.gcm_Key = gcm_Key;
        this.phoneName = phoneName;
        this.langid = langid;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getPhoneModel() {
        return phoneModel;
    }

    public void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getGcm_Key() {
        return gcm_Key;
    }

    public void setGcm_Key(String gcm_Key) {
        this.gcm_Key = gcm_Key;
    }

    public String getPhoneName() {
        return phoneName;
    }

    public void setPhoneName(String phoneName) {
        this.phoneName = phoneName;
    }

    public int getLangid() {
        return langid;
    }

    public void setLangid(int langid) {
        this.langid = langid;
    }
}
