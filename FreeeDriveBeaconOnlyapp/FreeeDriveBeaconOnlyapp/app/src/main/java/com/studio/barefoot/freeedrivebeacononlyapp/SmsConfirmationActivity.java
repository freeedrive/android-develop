package com.studio.barefoot.freeedrivebeacononlyapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.ResendSMSAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.SmsAsynctask;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SmsConfirmationActivity extends AppCompatActivity {
    TextView headingSMS, resendSMS;
    EditText et_Sms_Code;
    Button confirmSMScode;
    View actionBarView;
    public static ProgressBarDialog progressBarDialogSMS;
    private String sms4digitCode = "";

    public SmsConfirmationActivity() {
        LocaleUtils.updateConfig(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_confirmation);

        headingSMS = (TextView) findViewById(R.id.tv_headingSms);
        resendSMS = (TextView) findViewById(R.id.resendSms);

        et_Sms_Code = (EditText) findViewById(R.id.et_phone);
        confirmSMScode = (Button) findViewById(R.id.img_sms_next);
        actionBarView = getLayoutInflater().inflate(R.layout.custom_toolbarr, null);

   /*     final SpannableStringBuilder sb = new SpannableStringBuilder("Complete the 4 digit code \nyou received by SMS.");

        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
        sb.setSpan(bss, 13, 20, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
        setupActionBar();
        headingSMS.setText(sb)*/
        ;

        sms4digitCode = DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG);
        if (sms4digitCode != null && !sms4digitCode.isEmpty()) {
            Log.e("sms4digitCode", sms4digitCode);
            if (sms4digitCode.equalsIgnoreCase("en")) {
                String sms4digitcode_en = this.getResources().getString(R.string.sms_desc);
                Log.e("sms4digitcode_en", sms4digitcode_en);
                final SpannableStringBuilder sb = new SpannableStringBuilder(sms4digitcode_en);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 13, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                setupActionBar();
                headingSMS.setText(sb);
            } else if (sms4digitCode.equalsIgnoreCase("es")) {
                String sms4digitcode_es = this.getResources().getString(R.string.sms_desc);
                Log.e("sms4digitcode_en", sms4digitcode_es);
                final SpannableStringBuilder sb = new SpannableStringBuilder(sms4digitcode_es);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 21, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                setupActionBar();
                headingSMS.setText(sb);
            } else if (sms4digitCode.equalsIgnoreCase("fr")) {
                String sms4digitcode_fr = this.getResources().getString(R.string.sms_desc);
                Log.e("sms4digitcode_fr", sms4digitcode_fr);
                final SpannableStringBuilder sb = new SpannableStringBuilder(sms4digitcode_fr);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 21, 34, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                setupActionBar();
                headingSMS.setText(sb);
            } else if (sms4digitCode.equalsIgnoreCase("nl")) {
                String sms4digitcode_nl = this.getResources().getString(R.string.sms_desc);
                Log.e("sms4digitcode_nl", sms4digitcode_nl);
                final SpannableStringBuilder sb = new SpannableStringBuilder(sms4digitcode_nl);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 11, 20, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                setupActionBar();
                headingSMS.setText(sb);
            }
        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("en")) {
            String sms4digitcode_en = this.getResources().getString(R.string.sms_desc);
            Log.e("sms4digitcode_en", sms4digitcode_en);
            final SpannableStringBuilder sb = new SpannableStringBuilder(sms4digitcode_en);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 13, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            setupActionBar();
            headingSMS.setText(sb);
        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("es")) {
            String sms4digitcode_es = this.getResources().getString(R.string.sms_desc);
            Log.e("sms4digitcode_en", sms4digitcode_es);
            final SpannableStringBuilder sb = new SpannableStringBuilder(sms4digitcode_es);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 21, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            setupActionBar();
            headingSMS.setText(sb);
        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("fr")) {
            String sms4digitcode_fr = this.getResources().getString(R.string.sms_desc);
            Log.e("sms4digitcode_fr", sms4digitcode_fr);
            final SpannableStringBuilder sb = new SpannableStringBuilder(sms4digitcode_fr);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 2, 34, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            setupActionBar();
            headingSMS.setText(sb);
        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("nl")) {
            String sms4digitcode_nl = this.getResources().getString(R.string.sms_desc);
            Log.e("sms4digitcode_nl", sms4digitcode_nl);
            final SpannableStringBuilder sb = new SpannableStringBuilder(sms4digitcode_nl);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 11, 20, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            setupActionBar();
            headingSMS.setText(sb);
        } else {
            String sms4digitcode_en = this.getResources().getString(R.string.sms_desc);
            Log.e("sms4digitcode_en", sms4digitcode_en);
            final SpannableStringBuilder sb = new SpannableStringBuilder(sms4digitcode_en);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 13, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            setupActionBar();
            headingSMS.setText(sb);
        }



        /*ActivityCompat.requestPermissions(SmsConfirmationActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.SEND_SMS, android.Manifest.permission.CAMERA ,android.Manifest.permission.READ_PHONE_STATE}, 1001);*/

        //  headingSMS.setTypeface(UIUtils.getInstance().getLightFont(this));
        // et_Sms_Code.setTypeface(UIUtils.getInstance().getLightFont(this));

        resendSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtils.isNetworkAvailable()) {
                    progressBarDialogSMS = new ProgressBarDialog(SmsConfirmationActivity.this);
                    progressBarDialogSMS.setTitle(getString(R.string.title_progress_dialog));
                    progressBarDialogSMS.setMessage(getString(R.string.body_progress_dialog));
                    progressBarDialogSMS.show();

                    String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER);
                    List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                    mParams.add(new BasicNameValuePair("phone_number", phoneNumber));
                    ResendSMSAsyncTask resendSMSAsyncTask = new ResendSMSAsyncTask(SmsConfirmationActivity.this, WebServiceConstants.END_POINT_RESEND_SMS, mParams);
                    resendSMSAsyncTask.execute();
                } else {
                    AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(SmsConfirmationActivity.this);
                    error_No_Internet.setMessage(SmsConfirmationActivity.this.getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_No_Internet.show();
                }

            }
        });
        confirmSMScode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkFields()) {
                    if (AppUtils.isNetworkAvailable()) {
                        verifySMScode();
                    } else {
                        AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(SmsConfirmationActivity.this);
                        error_No_Internet.setMessage(SmsConfirmationActivity.this.getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_No_Internet.show();
                    }

                }
            }
        });

    }
    //For custom fonts we are using calligraphy lib

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }

    public boolean checkFields() {
        et_Sms_Code.setError(null);
        boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(et_Sms_Code.getText().toString())) {

            et_Sms_Code.setError(getString(R.string.error_field_required));
            focusView = et_Sms_Code;
            cancel = true;

        } else if (et_Sms_Code.getText().toString().length() < 4) {
            et_Sms_Code.setError(getString(R.string.error_invalid_code));
            focusView = et_Sms_Code;
            cancel = true;
        }
        if (cancel) {

            focusView.requestFocus();

        }
        return cancel;
    }

    public void verifySMScode() {

        progressBarDialogSMS = new ProgressBarDialog(SmsConfirmationActivity.this);
        progressBarDialogSMS.setTitle(getString(R.string.title_progress_dialog));
        progressBarDialogSMS.setMessage(getString(R.string.body_progress_dialog));
        progressBarDialogSMS.show();

        String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER);
        List<NameValuePair> mParams = new ArrayList<NameValuePair>();
        mParams.add(new BasicNameValuePair("phone_number", phoneNumber));
        mParams.add(new BasicNameValuePair("sms_code", et_Sms_Code.getText().toString()));
        Log.e("smsAsync", "" + mParams);
        SmsAsynctask smsAsynctask = new SmsAsynctask(SmsConfirmationActivity.this, WebServiceConstants.END_POINT_SMS_VERIFICATION, mParams);
        smsAsynctask.execute();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
