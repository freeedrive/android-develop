package com.studio.barefoot.freeedrivebeacononlyapp.interfacess;

/**
 * Interface to handle response from server after a rigister request
 * Created by gtshilombowanticale on 05-09-16.
 */
public interface IRegisterFD {

    /**
     * Allows to handle the responses
     * @param response(in) from server
     * @param r(in), @String reponse in a JSON
     */
    public void manageRegister(String response, String r);


}
