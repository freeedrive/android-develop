package com.studio.barefoot.freeedrivebeacononlyapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.RidesBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbookpro on 4/26/17.
 */

public class PhoneSafetyAdapter extends RecyclerView.Adapter<PhoneSafetyAdapter.MyViewHolder> {


    private ArrayList<RidesBeans> ridesBeansList;
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.phone_safety, parent, false);

        return new MyViewHolder(itemView);    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RidesBeans ridesBeans = ridesBeansList.get(position);
        String dateofRide = AppUtils.formate10LongTimeToDisplay(ridesBeans.getDeparture_time());
        String startAndEndTimeOfRide = AppUtils.formate10LongDateToDisplay(ridesBeans.getDeparture_time())+" > "+AppUtils.formate10LongDateToDisplay(ridesBeans.getArrival_time());
        int distractionsOfEachRide = ridesBeans.getBad_behaviour();
        holder.date.setText(dateofRide +"  |");
        holder.time.setText(startAndEndTimeOfRide);
        holder.distractions.setText(""+distractionsOfEachRide);
    }
    public PhoneSafetyAdapter(ArrayList<RidesBeans> moviesList) {
        this.ridesBeansList = moviesList;
    }
    @Override
    public int getItemCount() {
        return ridesBeansList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView date , time, distractions;

        public MyViewHolder(View view) {
            super(view);
            date = (TextView) view.findViewById(R.id.rideDate);
            time = (TextView) view.findViewById(R.id.startEndTime);
            distractions = (TextView) view.findViewById(R.id.distractions);
        }
    }
}
