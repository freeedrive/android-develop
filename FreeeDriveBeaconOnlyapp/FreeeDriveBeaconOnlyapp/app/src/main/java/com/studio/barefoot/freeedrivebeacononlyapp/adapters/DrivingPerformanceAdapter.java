package com.studio.barefoot.freeedrivebeacononlyapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.RidesBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;

import java.util.ArrayList;

/**
 * Created by macbookpro on 4/26/17.
 */

public class DrivingPerformanceAdapter extends RecyclerView.Adapter<DrivingPerformanceAdapter.MyViewHolder> {


    private ArrayList<RidesBeans> ridesBeansList;
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.driving_performance, parent, false);

        return new MyViewHolder(itemView);    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RidesBeans ridesBeans = ridesBeansList.get(position);
        String dateofRide = AppUtils.formate10LongTimeToDisplay(ridesBeans.getDeparture_time());
        String startAndEndTimeOfRide = AppUtils.formate10LongDateToDisplay(ridesBeans.getDeparture_time())+" > "+AppUtils.formate10LongDateToDisplay(ridesBeans.getArrival_time());
        int distractionsOfEachRide = ridesBeans.getOver_speeding_count();
        int sudden_accelerate = ridesBeans.getSudden_accelaration();
        Log.e("sudden_accelerate"," : "+sudden_accelerate);
        int sudden_braking = ridesBeans.getSudden_braking();
        Log.e("sudden_braking"," : "+sudden_braking);
        holder.date.setText(dateofRide +"  | ");
        holder.time.setText(startAndEndTimeOfRide);
        holder.brakes.setText(""+sudden_braking);
        holder.acceleration.setText(""+sudden_accelerate);
       if (distractionsOfEachRide == -1){
           distractionsOfEachRide = 0;
       }
        holder.speeding.setText(""+distractionsOfEachRide);
    }
    public DrivingPerformanceAdapter(ArrayList<RidesBeans> moviesList) {
        this.ridesBeansList = moviesList;
    }
    @Override
    public int getItemCount() {
        return ridesBeansList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView date , time,brakes,acceleration,speeding;

        public MyViewHolder(View view) {
            super(view);
            date = (TextView) view.findViewById(R.id.rideDate);
            time = (TextView) view.findViewById(R.id.startEndTime);
            brakes = (TextView) view.findViewById(R.id.braking);
            acceleration = (TextView) view.findViewById(R.id.accelration);
            speeding = (TextView) view.findViewById(R.id.speeding);
        }
    }
}
