package com.studio.barefoot.freeedrivebeacononlyapp.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;


import com.studio.barefoot.freeedrivebeacononlyapp.ProfileActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.RegisterActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.UIUtils;

import java.util.LinkedList;
import java.util.List;

//import com.freeedrive.R;

public class DropDownListAdapter extends BaseAdapter
{
	private Activity mActivity;
	private List<String> mListItems;
	private LayoutInflater mInflater;
	private ViewHolder holder;

	public DropDownListAdapter(Activity la, List<String> items)
	{
		mListItems = new LinkedList<String>();
		mListItems.addAll(items);
		mInflater = LayoutInflater.from(la);
		mActivity = la;
	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mListItems.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.drop_down_list_row, null);
			holder = new ViewHolder();
			holder.tv = (TextView) convertView.findViewById(R.id.tv);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.tv.setText(mListItems.get(position));
		holder.tv.setTypeface(UIUtils.getInstance().getLightFont(mActivity));

		return convertView;
	}

	private class ViewHolder {
		TextView tv;
		CheckBox chkbox;
	}
}
