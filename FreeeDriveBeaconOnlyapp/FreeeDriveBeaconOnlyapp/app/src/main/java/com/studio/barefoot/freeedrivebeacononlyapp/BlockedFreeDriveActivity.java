package com.studio.barefoot.freeedrivebeacononlyapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.adapters.MessagesAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;

import java.io.File;

import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.fdLogout;

public class BlockedFreeDriveActivity extends AppCompatActivity {

    public int backCount = 0;
    TextView textView_tryAgain;

    public BlockedFreeDriveActivity(){
        LocaleUtils.updateConfig(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked_free_drive);

        textView_tryAgain = (TextView) findViewById(R.id.txtview_tryAgain);
        textView_tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //deleteCache(getApplicationContext());
                DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY,false);
                fdLogout(getApplicationContext());
            }
        });
    }

    //Delete application cache
    public void deleteCache(Context context) {
        try {
            File cache = getCacheDir();
            File appDir = new File(cache.getParent());
            if(appDir.exists()){
                String[] children = appDir.list();
                for(String s : children){
                    if(!s.equals("lib")){
                        deleteDir(new File(appDir, s));
                        Log.i("TAG", "File /data/data/APP_PACKAGE/" + s +" DELETED");
                    }
                }
            }
        } catch (Exception e) {}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
    @Override
    public void onBackPressed() {

        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
        moveTaskToBack(true);
    }
}
