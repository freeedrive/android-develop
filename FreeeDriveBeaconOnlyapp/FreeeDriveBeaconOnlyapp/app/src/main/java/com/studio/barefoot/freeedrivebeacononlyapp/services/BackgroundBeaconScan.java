package com.studio.barefoot.freeedrivebeacononlyapp.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.altbeacon.beacon.startup.RegionBootstrap;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by mcs on 12/7/2016.
 */

public class BackgroundBeaconScan extends Service implements BootstrapNotifier, BeaconConsumer {
    private static final String TAG = "BackgroundScan";
    private BeaconManager mBeaconManager;
    private RegionBootstrap regionBootstrap;
    Region regions[];
    public boolean disconnect = false;
    public static int beaconlostCounter = 0;

    /**
     * Allows to handle database
     */
    // private RideBDD rideBDD;

    //
    // private Ride rideFromDB;

    Boolean rideIsActive = false;
    Boolean rideWasFininshed = false;

    private boolean userAlreadyLoggedIn = false;
    private int count = 0;
    public NotificationManager notificationManager;
    public static GoogleApiClient mApiClient;
    private Drawable drawable;
    private String namespace = "";
    private String instanceID = "";
    public static Timer beaconFd;
    public static boolean beaconlost = false;
    private String postiBeaconUUID = "";
    public static boolean beaconlostPermanently = false;


    @Override
    public void onCreate() {
        super.onCreate();
        AppUtils.isBGServiceActive = true;
        Log.e("Beacon", "Service Start");
        Log.e("Bfpk", " *** Beacon  Service is started *** ");

        mBeaconManager = org.altbeacon.beacon.BeaconManager.getInstanceForApplication(this);
        try {
            Log.e("BG", "iam in mBeaconManager to unbind it");
            mBeaconManager.unbind(this);
            mBeaconManager.removeAllRangeNotifiers();
            mBeaconManager.removeAllMonitorNotifiers();
            mBeaconManager.removeMonitoreNotifier(this);
        } catch (OutOfMemoryError e) {
            Log.e("BG", "iam in mBeaconManager to unbind it excep ");

        }
        mBeaconManager.getBeaconParsers().clear();
        //set Beacon Layout for Eddystone-UID packet


        mBeaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.EDDYSTONE_UID_LAYOUT));
        //   rideBDD = new RideBDD(getApplicationContext());
        mBeaconManager.setForegroundScanPeriod(30001);
        mBeaconManager.setForegroundBetweenScanPeriod(30001);
        mBeaconManager.setBackgroundScanPeriod(30001);
        mBeaconManager.setBackgroundBetweenScanPeriod(30001);
        BeaconManager.setAndroidLScanningDisabled(true);

        notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        namespace = DataHandler.getStringPreferences(AppConstants.UUID_NAME_SPACE);
        instanceID = DataHandler.getStringPreferences(AppConstants.UUID_INSTANCE_ID);
        Log.e("BG", "namespace" + namespace);
        Log.e("BG", "namespace" + instanceID);


        //To add Eddystone-UID region it's necessary to pass as parameters --> (uniqueId = region name, id1=namespace, id2 = instance, id3 = null)
        //  regions = new Region[1];
        //  regions[0] = new Region("EdstUIDAdvertising", Identifier.parse(namespace), Identifier.parse(instanceID), null);

        try {
            Log.e("BG", "iam in mBeaconManager.isBound");
            mBeaconManager.bind(this);
            Log.e("BG", "iam in mBeaconManager.bind");

        } catch (OutOfMemoryError e) {
            Log.e("BG", "iam in mBeaconManager.bindexce", e);

        }

    }

    @Override
    public void onStart(Intent intent, int startId) {
        Log.e("BG", "iam pnstart");
   /*      if (mApiClient ==null){
            mApiClient = new GoogleApiClient.Builder(this)
                     .addApi(ActivityRecognition.API)
                     .addConnectionCallbacks(this)
                     .addOnConnectionFailedListener(this)
                     .build();
         }*/
//        currentspeed = MenuActivity.speed;
//        Log.e("Static","CurrentSpeed :"+currentspeed);

//        if(beaconFound){
//        startRide();
//        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("BG", "iam in destroy");
        AppUtils.isBGServiceActive = false;

        try {
            mBeaconManager.unbind(this);
            mBeaconManager.removeMonitoreNotifier(this);

            Log.e("BG", "iam in   mBeaconManager.unbind");
            if (beaconFd != null) {
                beaconFd.cancel();
                beaconFd = null;

            }
        } catch (Exception e) {
            Log.e("BG", "iam in   mBeaconManager.unbind Exception" + e);
        }

    }

    public void enableRegions() {
        try {

            if (regionBootstrap == null) {
                List<Region> list = new ArrayList<>();
                for (int i = 0; i < regions.length; i++) {
                    if (regions[i] != null) {
                        list.add(regions[i]);
                    }
                }
                regionBootstrap = new RegionBootstrap(this, list);
            }

            for (int i = 0; i < regions.length; i++) {
                if (regions[i] != null) {
                    mBeaconManager.startRangingBeaconsInRegion(regions[i]);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.e("BG", "iam in IBinder");
        return null;
    }

    @Override
    public void onBeaconServiceConnect() {
        // String preiBeaconUUID = DataHandler.getStringPreferences(AppConstants.UUID);
        // postiBeaconUUID=AppUtils.addDashes(preiBeaconUUID);
        Log.e("BG", "iam in onBeaconServiceConnect");
        namespace = DataHandler.getStringPreferences(AppConstants.UUID_NAME_SPACE);
        instanceID = DataHandler.getStringPreferences(AppConstants.UUID_INSTANCE_ID);
        Identifier myBeaconNamespaceId = Identifier.parse(namespace);
        Identifier myBeaconInstanceId = Identifier.parse(instanceID);
        Region region = new Region("EdstUIDAdvertising", myBeaconNamespaceId, myBeaconInstanceId, null);
        mBeaconManager.addMonitorNotifier(this);
        try {
            Log.e("BG", "iam in startMonitoringBeaconsInRegion");
            mBeaconManager.startMonitoringBeaconsInRegion(region);
        } catch (RemoteException e) {
            Log.e("BG", "iam in startMonitoringBeaconsInRegionExce" + e);
            e.printStackTrace();
        }
        mBeaconManager.addMonitorNotifier(new MonitorNotifier() {
            @Override
            public void didEnterRegion(Region region) {
                Log.e("Monitored", "entered");
                /*try {
                    if(LocationService.future2minTimer != null){
                        beaconFd.cancel();
                        beaconFd = null;

                    }
                }catch (NullPointerException e){

                }catch (Exception e){

                }*/
                beaconlostCounter++;
                if (beaconlostCounter == 2) {
                    AppConstants.ORPHAN_RIDE_BIT = 0;
                }
                try {
                    if (beaconFd != null) {
                        beaconFd.cancel();
                        beaconFd = null;

                    }
                } catch (Exception e) {

                }

                userAlreadyLoggedIn = DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY);
                if (userAlreadyLoggedIn) {
                    AppUtils.isInRange = true;
                    AppUtils.IamInExit = false;
                    beaconlostPermanently = false;
                    startService(new Intent(BackgroundBeaconScan.this, LocationService.class));
                     /*long[] v = {0, 200};
                     Bitmap largeIcon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.freeedrive_newlogo);
                     NotificationCompat.Builder mBuilder =
                             new NotificationCompat.Builder(getApplicationContext())
                                     .setSmallIcon(R.drawable.freeedrive_newlogo)
                                     .setLargeIcon(largeIcon)
                                     .setContentText(AppConstants.BODY_LOCATION_SERVICE_ON).setContentTitle(AppConstants.TITLE_LOCATION_SERVICE)
                                     .setPriority(Notification.PRIORITY_HIGH).setVibrate(v);
                     notificationManager.notify(1,mBuilder.build());*/

                    Log.e("Beacon", "Found");
                 /*    long[] v = {0, 200};
                     Bitmap largeIcon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.freeedrive_newlogo);
                     NotificationCompat.Builder mBuilder =
                             new NotificationCompat.Builder(getApplicationContext())
                                     .setSmallIcon(R.drawable.freeedrive_newlogo)
                                     .setLargeIcon(largeIcon)
                                     .setContentText(AppConstants.BODY_NOTIFICATION_INRANGE).setContentTitle(AppConstants.TITLE_NOTIFICATION_INRANGE)
                                     .setPriority(Notification.PRIORITY_HIGH).setVibrate(v);
                     notificationManager.notify(1,mBuilder.build());*/
                    //startScoring();
                }

            }

            @Override
            public void didExitRegion(Region region) {
                Log.e("Monitored", "exited");
                AppUtils.IamInExit = true;
                disconnect = true;
                beaconlostCounter++;
                if (beaconlostCounter <= 1 && DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE) && LocationService.currentSpeed > 15) {
                    AppConstants.ORPHAN_RIDE_BIT = 1;
                }
                if (disconnect) {
                    try {
                        //mBeaconManager.stopMonitoringBeaconsInRegion(region);
                        Log.e("disconnectTimer", "hited");
                        disconnectTimer();


                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("ExitRegiondidExitRegion", "" + e);
                    }

                }
            }

            @Override
            public void didDetermineStateForRegion(int i, Region region) {

            }
        });
       /* mBeaconManager.setRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {

                Log.e(TAG, "FoundV2 " + beacons.size() + " beacons in Region " + region.getUniqueId() + " - " + region.getId1() + " - " + region.getId2() + " - " + region.getId3());
                //Toast.makeText(getApplicationContext(), "Beacon Found", Toast.LENGTH_SHORT).show();
                AppUtils.beaconFound = true;
                if (beacons.size() > 0) {
               *//*     if (userAlreadyLoggedIn) {
                        if (!AppUtils.startLocService){
                            AppUtils.isInRange = true;
                            startService(new Intent(ApplicationController.getmAppcontext(),LocationService.class));
                            AppUtils.startLocService = true;
                        }

                    }*//*
                    count++;
                    for (Beacon beacon : beacons) {
                        int rssi = beacon.getRssi();
                        Log.e("BGrssi", "" + rssi);

                        if (rssi > -115) {

                       *//*     if (!FDUtils.InRangeBroadCastSent){
                                Intent sendInRangeIntent = new Intent("IamInRange");
                                sendBroadcast(sendInRangeIntent);
                                FDUtils.isInRange = true;
                                FDUtils.InRangeBroadCastSent = true;
                            }*//*


                        }
                    }

                }

            }

        });
        enableRegions();*/
        // we can call startRide method here as well because we can get true beacon found flag here

    }

    @Override
    public void didEnterRegion(Region region) {
        //Log.e("EnteredRegion", "entered");
//        userAlreadyLoggedIn = DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY);
//        if (userAlreadyLoggedIn) {
//
//            AppUtils.isInRange = true;
//            beaconlost = true;
//            startService(new Intent(this,LocationService.class));
//            Log.e("Beacon","Found");
//            long[] v = {0, 200};
//            Bitmap largeIcon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.freeedrive_newlogo);
//            NotificationCompat.Builder mBuilder =
//                    new NotificationCompat.Builder(getApplicationContext())
//                            .setSmallIcon(R.drawable.freeedrive_newlogo)
//                            .setLargeIcon(largeIcon)
//                            .setContentText(AppConstants.BODY_NOTIFICATION_INRANGE).setContentTitle(AppConstants.TITLE_NOTIFICATION_INRANGE)
//                            .setPriority(Notification.PRIORITY_HIGH).setVibrate(v);
//            notificationManager.notify(1,mBuilder.build());
//            //startScoring();
//        }
//        if(beaconFd != null){
//            beaconFd.cancel();
//            beaconFd = null;
//        }
    }

    @Override
    public void didExitRegion(Region region) {
        Log.e("EnteredRegion", "exited");
//        stopService(new Intent(this,LocationService.class));
//        Log.e("ServiceStop","");
//        disconnect = true;
//        if(disconnect){
//            try {
//                //mBeaconManager.stopMonitoringBeaconsInRegion(region);
//                AppUtils.speedCheck = false;
//                beaconlost = false;
//                disconnectTimer();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }
    }

    public void disconnectTimer() {
        beaconFd = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                //   stopFDSensor();
                //  getToasthandlerpauseRide.sendEmptyMessage(0);
                userAlreadyLoggedIn = DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY);
                if (userAlreadyLoggedIn) {
                    if (!DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)) {
                        Intent stopsrvc = new Intent(getApplicationContext(), LocationService.class);
                        stopService(stopsrvc);
                             /*long[] v = {0, 200};
                             Bitmap largeIcon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.freeedrive_newlogo);
                             NotificationCompat.Builder mBuilder =
                                     new NotificationCompat.Builder(getApplicationContext())
                                             .setSmallIcon(R.drawable.freeedrive_newlogo)
                                             .setLargeIcon(largeIcon)
                                             .setContentText(AppConstants.BODY_LOCATION_SERVICE_OFF).setContentTitle(AppConstants.TITLE_LOCATION_SERVICE)
                                             .setPriority(Notification.PRIORITY_HIGH).setVibrate(v);
                             notificationManager.notify(1,mBuilder.build());*/
                    } else if (AppUtils.speedlessCheck) {
                        Intent stopsrvc = new Intent(getApplicationContext(), LocationService.class);
                        stopService(stopsrvc);
                        Log.e("Inside", "DisconnectTimer");
                        Intent sendInRangeIntent = new Intent("IamOutOfRange");
                        sendBroadcast(sendInRangeIntent);

                        AppUtils.isLocationSrvcStopped = true;
                        AppUtils.isInRange = false;
                        AppUtils.speedCheck = false;
                             /*long[] v = {0, 200};
                             Bitmap largeIcon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.freeedrive_newlogo);
                             NotificationCompat.Builder mBuilder =
                                     new NotificationCompat.Builder(getApplicationContext())
                                             .setSmallIcon(R.drawable.freeedrive_newlogo)
                                             .setLargeIcon(largeIcon)
                                             .setContentText(AppConstants.BODY_LOCATION_SERVICE_OFF).setContentTitle(AppConstants.TITLE_LOCATION_SERVICE)
                                             .setPriority(Notification.PRIORITY_HIGH).setVibrate(v);
                             notificationManager.notify(1,mBuilder.build());*/
                    } else {
                        beaconlostPermanently = true;
                        if (beaconFd != null) {
                            beaconFd.cancel();
                            beaconFd = null;
                        }
                    }

                    Log.e("Beacon", "Not Found");

                  /*  long[] v = {0, 200};
                    Bitmap largeIcon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.freeedrive_newlogo);
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(getApplicationContext())
                                    .setSmallIcon(R.drawable.freeedrive_newlogo)
                                    .setLargeIcon(largeIcon)
                                    .setContentText(AppConstants.BODY_NOTIFICATION_OUTRANGE).setContentTitle(AppConstants.TITLE_NOTIFICATION_INRANGE)
                                    .setPriority(Notification.PRIORITY_HIGH).setVibrate(v);
                    notificationManager.notify(1,mBuilder.build());*/
                }


            }
        };
        //

        //beaconFd.schedule(timerTask,120000);
        beaconFd.schedule(timerTask, 30000);
    }


    @Override
    public void didDetermineStateForRegion(int i, Region region) {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("BG", "iam in start command");
        mBeaconManager = org.altbeacon.beacon.BeaconManager.getInstanceForApplication(this);
        try {
            Log.e("BG", "iam in mBeaconManager to unbind it");
            mBeaconManager.unbind(this);
            mBeaconManager.removeAllRangeNotifiers();
            mBeaconManager.removeAllMonitorNotifiers();
            mBeaconManager.removeMonitoreNotifier(this);
        } catch (OutOfMemoryError e) {
            Log.e("BG", "iam in mBeaconManager to unbind it excep ");

        }
        mBeaconManager.getBeaconParsers().clear();
        //set Beacon Layout for Eddystone-UID packet


        mBeaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.EDDYSTONE_UID_LAYOUT));
        //   rideBDD = new RideBDD(getApplicationContext());
        mBeaconManager.setForegroundScanPeriod(30001);
        mBeaconManager.setForegroundBetweenScanPeriod(30001);
        mBeaconManager.setBackgroundScanPeriod(30001);
        mBeaconManager.setBackgroundBetweenScanPeriod(30001);
        BeaconManager.setAndroidLScanningDisabled(true);

        notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        namespace = DataHandler.getStringPreferences(AppConstants.UUID_NAME_SPACE);
        instanceID = DataHandler.getStringPreferences(AppConstants.UUID_INSTANCE_ID);
        Log.e("BG", "namespace" + namespace);
        Log.e("BG", "namespace" + instanceID);


        //To add Eddystone-UID region it's necessary to pass as parameters --> (uniqueId = region name, id1=namespace, id2 = instance, id3 = null)
        //  regions = new Region[1];
        //  regions[0] = new Region("EdstUIDAdvertising", Identifier.parse(namespace), Identifier.parse(instanceID), null);

        try {
            Log.e("BG", "iam in mBeaconManager.isBound");
            mBeaconManager.bind(this);
            Log.e("BG", "iam in mBeaconManager.bind");

        } catch (OutOfMemoryError e) {
            Log.e("BG", "iam in mBeaconManager.bindexce", e);

        }

        return START_STICKY;
    }


}
