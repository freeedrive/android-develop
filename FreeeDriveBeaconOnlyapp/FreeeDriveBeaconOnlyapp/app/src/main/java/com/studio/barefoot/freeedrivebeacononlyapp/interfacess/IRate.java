package com.studio.barefoot.freeedrivebeacononlyapp.interfacess;

/**
 * Interface to handle response from server after a feedback request
 * Created by gtshilombowanticale on 05-09-16.
 */
public interface IRate {

    /**
     * Allows to handle the response
     * @param resposne from server
     */
    public void managerate(String resposne);
}
