package com.studio.barefoot.freeedrivebeacononlyapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.RateAsynctask;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.interfacess.IRate;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ContactUsActivity extends AppCompatActivity implements IRate{
    EditText et_msg;
    Button img_Send_Contactus;
    public static ProgressBarDialog progressBarDialogContactUs;
    private String RELEASE="";
    private String PHONE_MODEL="";
    private String PHONE_BRAND="";
    public ContactUsActivity()
    {
        LocaleUtils.updateConfig(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        et_msg = (EditText) findViewById(R.id.et_msg_contactsUs);
        img_Send_Contactus = (Button) findViewById(R.id.img_Send_Contactus);
        RELEASE = Build.VERSION.RELEASE;
        PHONE_MODEL=Build.MODEL;
        PHONE_BRAND=Build.BRAND;
     /*   try {
            long installed = this
                    .getPackageManager()
                    .getPackageInfo("package.name", 0)
                    .firstInstallTime
                    ;
            Log.e("Installion date",AppUtils.formateLongToOnlyDateForServer(installed));

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }*/
        //   PHONE_BRAND=Build.DEVICE;


        setupActionBar();
        img_Send_Contactus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkFields()) {
                    if (AppUtils.isNetworkAvailable()) {

                        sendFeedBack();
                    } else {
                        AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(ContactUsActivity.this);
                        error_No_Internet.setMessage(ContactUsActivity.this.getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_No_Internet.show();
                    }
                }
            }
        });
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
    private void sendFeedBack() {
        progressBarDialogContactUs = new ProgressBarDialog(ContactUsActivity.this);
        progressBarDialogContactUs.setTitle(getString(R.string.title_progress_dialog));
        progressBarDialogContactUs.setMessage(getString(R.string.body_progress_dialog));
        progressBarDialogContactUs.show();
        RateAsynctask rateAsynctask = new RateAsynctask(this,this);
        rateAsynctask.execute(et_msg.getText().toString()+"\n Phone Model: "+PHONE_BRAND+" "+PHONE_MODEL+"\n OS ver: "+RELEASE);
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }
    public boolean checkFields(){
        et_msg.setError(null);
        boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(et_msg.getText().toString())) {

            et_msg.setError(getString(R.string.error_field_required));
            focusView = et_msg;
            cancel = true;

        }
        if (cancel) {

            focusView.requestFocus();

        }
        return cancel;
    }



    @Override
    public void managerate(String response) {
        Log.e("Response",""+response);
        try{
            if(response != null && response.length() > 0){
                //Log.i("Adneom", "RateActivity) response from server is " + response);
                final AlertDialog.Builder builder = new AlertDialog.Builder(ContactUsActivity.this);
                builder.setMessage(getResources().getString(R.string.popup_confirmation_feedback));
                builder.setPositiveButton(getResources().getString(R.string.popup_confirmation_btn_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        Intent intent = new Intent(ContactUsActivity.this,MenuActivity.class);
                        startActivity(intent);
                    }
                });
                builder.show();
            }
        }catch (Exception exception){
            exception.printStackTrace();
        }

    }
}
