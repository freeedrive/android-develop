package com.studio.barefoot.freeedrivebeacononlyapp.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.studio.barefoot.freeedrivebeacononlyapp.ContactUsActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.FaqActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.MainActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.MenuActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;

/**
 * Base class for communicating with Firebase Messaging.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    PendingIntent pendingIntent;
    private Drawable drawable;
    private Context context;
    private String notificationBody="";
    private String notificationHeading="";
    private NotificationManager notificationManager;
    private Uri uri;
    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Method to handle a message received
     * @param remoteMessage(in), @RemoteMessage(Object)
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e("Barefoot", "From: " + remoteMessage.getFrom());
        Log.e("BarefootBarefoot", "Notification Message Body: " + remoteMessage.getNotification().getBody());
        Log.e("BarefootBarefoot", "Notification Message getTitle: " + remoteMessage.getNotification().getTitle());

        context = ApplicationController.getmAppcontext();

        //String click_action = remoteMessage.getNotification().getClickAction();
       /* notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        uri=RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notificationBody=remoteMessage.getNotification().getBody();
        notificationHeading = remoteMessage.getNotification().getTitle();

        if (notificationHeading.equalsIgnoreCase("")){
            notificationHeading="FLEET MANAGER";
        }
        //context = ApplicationController.getmAppcontext();
        if(android.os.Build.VERSION.SDK_INT < 23) {
            long[] v = {0, 200};
            Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.freeedrive_newlogo);
            drawable = context.getResources().getDrawable(R.drawable.freeedrive_newlogo);

            Intent push = new Intent();
            push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //push.putExtra("PushNotification",click_action);
            push.setClass(context, MenuActivity.class);
            PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(context, 0,push, PendingIntent.FLAG_CANCEL_CURRENT);
            Notification.Builder notif = new Notification.Builder(ApplicationController.getmAppcontext());
            notif.setContentText(notificationBody).setContentTitle(notificationHeading)
                    .setAutoCancel(true)
                    .setLargeIcon(largeIcon)
                    .setSmallIcon(R.drawable.freeedrive_newlogo)
                    .setSound(uri).setPriority(Notification.PRIORITY_HIGH).setVibrate(v);
            notif.setFullScreenIntent(fullScreenPendingIntent, true);
            notificationManager.notify(1, notif.build());
        }
        else {
            NotificationCompat.Builder notificationBuider = createNotificationBuider(context ,notificationHeading,notificationBody);
            notificationBuider.setPriority(Notification.PRIORITY_HIGH);
            notificationBuider.setSound(uri);

            // The '0' here means to repeat indefinitely
            // Vibrate for 500 milliseconds = 0.5 Second
            long[] v = {0, 200};
            notificationBuider.setVibrate(v);
            Intent push = new Intent();
            push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //push.putExtra("PushNotification",click_action);
            push.setClass(context, MenuActivity.class);

            PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(context, 0,push,PendingIntent.FLAG_CANCEL_CURRENT);
            notificationBuider.setFullScreenIntent(fullScreenPendingIntent, true);
            notificationManager.notify(1 , notificationBuider.build());
        }*/

        /*if(click_action.equalsIgnoreCase("M")){
            notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
            uri=RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notificationBody=remoteMessage.getNotification().getBody();
            notificationHeading = remoteMessage.getNotification().getTitle();

            if (notificationHeading.equalsIgnoreCase("")){
                notificationHeading="FLEET MANAGER";
            }
            //context = ApplicationController.getmAppcontext();
            if(android.os.Build.VERSION.SDK_INT < 23) {
                long[] v = {0, 200};
                Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.freeedrive_newlogo);
                drawable = context.getResources().getDrawable(R.drawable.freeedrive_newlogo);

                Intent push = new Intent(click_action);
                push.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                push.putExtra("PushNotification",click_action);
                //push.setClass(context, MenuActivity.class);
                PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0,push, PendingIntent.FLAG_ONE_SHOT);
                Notification.Builder notif = new Notification.Builder(ApplicationController.getmAppcontext());
                notif.setContentText(notificationBody).setContentTitle(notificationHeading)
                        .setAutoCancel(true)
                        .setLargeIcon(largeIcon)
                        .setSmallIcon(R.drawable.freeedrive_newlogo)
                        .setSound(uri).setPriority(Notification.PRIORITY_HIGH).setVibrate(v);
                notif.setFullScreenIntent(fullScreenPendingIntent, true);
                notificationManager.notify(1, notif.build());
                //startActivity(push);

            }
            else {
                NotificationCompat.Builder notificationBuider = createNotificationBuider(context ,notificationHeading,notificationBody);
                notificationBuider.setPriority(Notification.PRIORITY_HIGH);
                notificationBuider.setSound(uri);

                // The '0' here means to repeat indefinitely
                // Vibrate for 500 milliseconds = 0.5 Second
                long[] v = {0, 200};
                notificationBuider.setVibrate(v);
                Intent push = new Intent();
                push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                push.putExtra("PushNotification",click_action);
                push.setClass(context, MenuActivity.class);

                PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(context, 0,push,PendingIntent.FLAG_CANCEL_CURRENT);
                notificationBuider.setFullScreenIntent(fullScreenPendingIntent, true);
                notificationManager.notify(1 , notificationBuider.build());
            }
            Intent intent = new Intent(context, MenuActivity.class);
            intent.putExtra("PushNotification",click_action);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
        else if (click_action.equalsIgnoreCase("T")){
            notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
            uri=RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notificationBody=remoteMessage.getNotification().getBody();
            notificationHeading = remoteMessage.getNotification().getTitle();

            if (notificationHeading.equalsIgnoreCase("")){
                notificationHeading="FLEET MANAGER";
            }
            context = ApplicationController.getmAppcontext();
            if(android.os.Build.VERSION.SDK_INT < 23) {
                long[] v = {0, 200};
                Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.freeedrive_newlogo);
                drawable = context.getResources().getDrawable(R.drawable.freeedrive_newlogo);
                Intent push = new Intent();
                push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                push.putExtra("PushNotification",click_action);
                push.setClass(context, MenuActivity.class);

                PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(context, 0,push,PendingIntent.FLAG_CANCEL_CURRENT);
                Notification.Builder notif = new Notification.Builder(ApplicationController.getmAppcontext());
                notif.setContentText(notificationBody).setContentTitle(notificationHeading)
                        .setAutoCancel(true)
                        .setLargeIcon(largeIcon)
                        .setSmallIcon(R.drawable.freeedrive_newlogo)
                        .setSound(uri).setPriority(Notification.PRIORITY_HIGH).setVibrate(v);
                notif.setFullScreenIntent(fullScreenPendingIntent, true);
                notificationManager.notify(1, notif.build());
            }
            else {
                NotificationCompat.Builder notificationBuider = createNotificationBuider(context ,notificationHeading,notificationBody);
                notificationBuider.setPriority(Notification.PRIORITY_HIGH);
                notificationBuider.setSound(uri);

                // The '0' here means to repeat indefinitely
                // Vibrate for 500 milliseconds = 0.5 Second
                long[] v = {0, 200};
                notificationBuider.setVibrate(v);
                Intent push = new Intent();
                push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                push.putExtra("PushNotification",click_action);
                push.setClass(context, MenuActivity.class);

                PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(context, 0,push,PendingIntent.FLAG_CANCEL_CURRENT);
                notificationBuider.setFullScreenIntent(fullScreenPendingIntent, true);
                notificationManager.notify(1 , notificationBuider.build());
            }
            Intent intent = new Intent(context, MenuActivity.class);
            intent.putExtra("PushNotification",click_action);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }*/

       /* if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            user_id = remoteMessage.getData().get("user_id");
            date = remoteMessage.getData().get("date");
            cal_id = remoteMessage.getData().get("hal_id");
            M_view = remoteMessage.getData().get("M_view");
        }*/

        String click_action = remoteMessage.getNotification().getClickAction();

        //Calling method to generate notification
        sendNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle(), click_action);
    }




    private void sendNotification(String messageBody, String messageTitle, String click_action) {
        Intent intent = new Intent(click_action);
        //intent.putExtra("PushNotification",click_action);

        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        long[] v = {0, 200};
        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.freeedrive_newlogo);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setSmallIcon(R.drawable.logo)
                .setLargeIcon(largeIcon)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageTitle))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setVibrate(v)
                .setContentIntent(pendingIntent)
                .setFullScreenIntent(pendingIntent,true);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }

    /*public NotificationCompat.Builder createNotificationBuider(Context context, String title, String message) {
        Drawable drawable = context.getResources().getDrawable(R.drawable.logo);
//        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.me);
        Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
        Log.e("BitMap",""+bitmap);
        return new NotificationCompat.Builder(context)
                .setLargeIcon(bitmap)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(title))
                .setContentText(message)
                //.setContentIntent(pendingIntent)
                .setAutoCancel(true);

    }*/
}
