package com.studio.barefoot.freeedrivebeacononlyapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.Result;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.QrCodeAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ChangePhoneNumberDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.QrCodeUIDdialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class QrCodeActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    TextView headingQrcode, headingDrivePadInfo;
    ImageView img_Scan_qrCode;
    Button scan;
    View actionBarView;
    private ZXingScannerView mScannerView;
    QrCodeUIDdialog qrCodeUIDdialog;
    public static ProgressBarDialog progressBarDialogQrCodeDialog;
    private boolean goBack = false;
    private String languagePref = "";

    public QrCodeActivity() {
        LocaleUtils.updateConfig(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code);
//comment test
        /*ActivityCompat.requestPermissions(QrCodeActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.SEND_SMS, android.Manifest.permission.CAMERA ,android.Manifest.permission.READ_PHONE_STATE}, 1001);*/
        if (getIntent().getStringExtra("validateQr") != null && getIntent().getStringExtra("validateQr").equalsIgnoreCase("validated")) {

            String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER);
            String uuid = DataHandler.getStringPreferences(AppConstants.TEMP_UUID);

            //  String uuid = DataHandler.getStringPreferences(AppConstants.UUID_iBEACON);

            List<NameValuePair> mParams = new ArrayList<NameValuePair>();
            mParams.add(new BasicNameValuePair("phone_number", phoneNumber));
            mParams.add(new BasicNameValuePair("uuid", uuid));
            progressBarDialogQrCodeDialog = new ProgressBarDialog(this);
            progressBarDialogQrCodeDialog.setTitle(getString(R.string.title_progress_dialog));
            progressBarDialogQrCodeDialog.setMessage(getString(R.string.body_progress_dialog));
            progressBarDialogQrCodeDialog.show();
            QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(ApplicationController.getmAppcontext(), WebServiceConstants.END_POINT_UUID_v3, mParams);
            qrCodeAsyncTask.execute();
            isFinishing();
            Log.e("qrAsync", "" + mParams);

        } else if (getIntent().getStringExtra("validateSerial") != null && getIntent().getStringExtra("validateSerial").equalsIgnoreCase("validatedSerial")) {
            String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER);
            String uuid = DataHandler.getStringPreferences(AppConstants.TEMP_IBKS_SERIAL_NO);
            Log.e("iBKS", "Serial :" + uuid);
            List<NameValuePair> mParams = new ArrayList<NameValuePair>();
            mParams.add(new BasicNameValuePair("phone_number", phoneNumber));
            mParams.add(new BasicNameValuePair("uuid", uuid));
            progressBarDialogQrCodeDialog = new ProgressBarDialog(this);
            progressBarDialogQrCodeDialog.setTitle(getString(R.string.title_progress_dialog));
            progressBarDialogQrCodeDialog.setMessage(getString(R.string.body_progress_dialog));
            progressBarDialogQrCodeDialog.show();
            QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(ApplicationController.getmAppcontext(), WebServiceConstants.END_POINT_UUID_v3, mParams);
            qrCodeAsyncTask.execute();
            isFinishing();
        } else if (getIntent().getStringExtra("comingFromProfile") != null && getIntent().getExtras().getString("comingFromProfile").equalsIgnoreCase("1")) {
            goBack = true;
        }
        headingDrivePadInfo = (TextView) findViewById(R.id.drivePadinfo);
        headingQrcode = (TextView) findViewById(R.id.tv_headingQr);
        scan = (Button) findViewById(R.id.img_Next_qrCode);
        actionBarView = getLayoutInflater().inflate(R.layout.custom_toolbarr, null);
        img_Scan_qrCode = (ImageView) findViewById(R.id.img_Scan_qrCode);
        //headingDrivePadInfo.setPaintFlags(headingDrivePadInfo.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

       /*headingDrivePadInfo.setText(Html.fromHtml("<u>info@freeedrive.com</u>"));
        *//*headingDrivePadInfo.setText("info@freeedrive.com");*//*
        headingDrivePadInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "info@freeedrive.com" });
                startActivity(Intent.createChooser(intent, ""));
                *//*Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://info@freeedrive.com"));
                startActivity(intentBrowser);*//*
            }
        });*/
        languagePref = DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG);
        String localLanguage = Locale.getDefault().getLanguage();
        if (languagePref != null || !localLanguage.isEmpty()) {
            if (languagePref.equalsIgnoreCase("en") || Locale.getDefault().getLanguage().equalsIgnoreCase("en")) {
                String qrdesc = this.getResources().getString(R.string.qr_desc);

                final SpannableStringBuilder sb = new SpannableStringBuilder(qrdesc);

                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 9, 16, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold

                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                        /*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*/
                    }
                };
                final StyleSpan bss_ = new StyleSpan(android.graphics.Typeface.BOLD);
                sb.setSpan(bss_, 44, 53, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                sb.setSpan(clickableSpan, 44, 53, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 44, 53, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingQrcode.setText(sb);
                headingQrcode.setMovementMethod(LinkMovementMethod.getInstance());


            } else if (languagePref.equalsIgnoreCase("es") || Locale.getDefault().getLanguage().equalsIgnoreCase("es")) {
                String qrdesc_es = this.getResources().getString(R.string.qr_desc);

                final SpannableStringBuilder sb = new SpannableStringBuilder(qrdesc_es);

                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 18, 28, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold

                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                        /*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*/
                    }
                };
                final StyleSpan bss_ = new StyleSpan(android.graphics.Typeface.BOLD);
                sb.setSpan(bss_, 58, 66, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                sb.setSpan(clickableSpan, 58, 66, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 58, 66, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingQrcode.setText(sb);
                headingQrcode.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (languagePref.equalsIgnoreCase("fr") || Locale.getDefault().getLanguage().equalsIgnoreCase("fr")) {
                String qrdesc = this.getResources().getString(R.string.qr_desc);

                final SpannableStringBuilder sb = new SpannableStringBuilder(qrdesc);

                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 20, 28, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold

                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                        /*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*/
                    }
                };
                final StyleSpan bss_ = new StyleSpan(android.graphics.Typeface.BOLD);
                sb.setSpan(bss_, 45, 53, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                sb.setSpan(clickableSpan, 45, 53, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 45, 53, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingQrcode.setText(sb);
                headingQrcode.setMovementMethod(LinkMovementMethod.getInstance());

            } else if (languagePref.equalsIgnoreCase("nl") || Locale.getDefault().getLanguage().equalsIgnoreCase("nl")) {
                String qrdesc = this.getResources().getString(R.string.qr_desc);

                final SpannableStringBuilder sb = new SpannableStringBuilder(qrdesc);

                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 11, 18, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold

                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                        /*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*/
                    }
                };
                final StyleSpan bss_ = new StyleSpan(android.graphics.Typeface.BOLD);
                sb.setSpan(bss_, 63, 71, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                sb.setSpan(clickableSpan, 63, 71, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 63, 71, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingQrcode.setText(sb);
                headingQrcode.setMovementMethod(LinkMovementMethod.getInstance());
            } else {
                String qrdesc = "Scan the QR code placed\\n on the back of your DrivePad.";

                final SpannableStringBuilder sb = new SpannableStringBuilder(qrdesc);

                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 9, 16, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold

                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                        /*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*/
                    }
                };
                final StyleSpan bss_ = new StyleSpan(android.graphics.Typeface.BOLD);
                sb.setSpan(bss_, 44, 53, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                sb.setSpan(clickableSpan, 44, 53, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 44, 53, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingQrcode.setText(sb);
                headingQrcode.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }

        // drive pad info

        if (languagePref != null && !languagePref.isEmpty()) {
            if (languagePref.equalsIgnoreCase("en")) {
                String drivepad_info = this.getResources().getString(R.string.drivpad_info);
                Log.e("drivepad_info", drivepad_info);
                final SpannableStringBuilder sb = new SpannableStringBuilder(drivepad_info);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 21, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                        /*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*/
                    }
                };
                sb.setSpan(clickableSpan, 21, 29, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingDrivePadInfo.setText(sb);
                headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());
               /* String newconnector_en = this.getResources().getString(R.string.new_connector);

                final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_en);
                final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb2.setSpan(bss2, 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent startQrcodeIntent = new Intent(ProfileActivity.this,QrCodeActivity.class);
                        startQrcodeIntent.putExtra("comingFromProfile","1");
                        startActivity(startQrcodeIntent);
                    }
                };
                sb2.setSpan(clickableSpan1, 21, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorText)),21,25,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                tv_new_connector.setText(sb2);
                tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());
*/


            } else if (languagePref.equalsIgnoreCase("es")) {
                String newphoneno_es = this.getResources().getString(R.string.drivpad_info);
                Log.e("newphoneno_es", newphoneno_es);
                final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_es);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 18, 26, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                       /* changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*/
                    }
                };
                sb.setSpan(clickableSpan, 18, 26, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 18, 26, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingDrivePadInfo.setText(sb);
                headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());
               /* String newconnector_es = this.getResources().getString(R.string.new_connector);

                final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_es);
                final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb2.setSpan(bss2, 26, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent startQrcodeIntent = new Intent(ProfileActivity.this,QrCodeActivity.class);
                        startQrcodeIntent.putExtra("comingFromProfile","1");
                        startActivity(startQrcodeIntent);
                    }
                };
                sb2.setSpan(clickableSpan1, 26, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorText)),26,30,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                tv_new_connector.setText(sb2);
                tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());*/
            } else if (languagePref.equalsIgnoreCase("fr")) {
                String newphoneno_fr = this.getResources().getString(R.string.drivpad_info);
                Log.e("newphoneno_fr", newphoneno_fr);
                final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_fr);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 22, 31, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                        /*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*/
                    }
                };
                sb.setSpan(clickableSpan, 22, 31, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 22, 31, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingDrivePadInfo.setText(sb);
                headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());

                /*String newconnector_fr = this.getResources().getString(R.string.new_connector);
                final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_fr);
                final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb2.setSpan(bss2, 33, 36, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent startQrcodeIntent = new Intent(ProfileActivity.this,QrCodeActivity.class);
                        startQrcodeIntent.putExtra("comingFromProfile","1");
                        startActivity(startQrcodeIntent);
                    }
                };
                sb2.setSpan(clickableSpan1, 33, 36, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorText)),33,36,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                tv_new_connector.setText(sb2);
                tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());*/
            } else if (languagePref.equalsIgnoreCase("nl")) {
                String newphoneno = this.getResources().getString(R.string.drivpad_info);
                final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 29, 37, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                       /* changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*/
                    }
                };
                sb.setSpan(clickableSpan, 29, 37, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 29, 37, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingDrivePadInfo.setText(sb);
                headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());

               /* String newconnector_nl = this.getResources().getString(R.string.new_connector);
                final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_nl);
                final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb2.setSpan(bss2, 23, 27, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent startQrcodeIntent = new Intent(ProfileActivity.this,QrCodeActivity.class);
                        startQrcodeIntent.putExtra("comingFromProfile","1");
                        startActivity(startQrcodeIntent);
                    }
                };
                sb2.setSpan(clickableSpan1, 23, 27, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorText)),23,27,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                tv_new_connector.setText(sb2);
                tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());*/
            }


        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("en")) {
            String newphoneno_en = this.getResources().getString(R.string.drivpad_info);
            Log.e("newphoneno_en", newphoneno_en);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_en);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 21, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                    startActivity(intentBrowser);
                    /*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                    changePhoneNoDialog.show();*/
                }
            };
            sb.setSpan(clickableSpan, 21, 29, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            headingDrivePadInfo.setText(sb);
            headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());

            /*String newconnector_en = this.getResources().getString(R.string.new_connector);

            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_en);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent startQrcodeIntent = new Intent(ProfileActivity.this,QrCodeActivity.class);
                    startQrcodeIntent.putExtra("comingFromProfile","1");
                    startActivity(startQrcodeIntent);
                }
            };
            sb2.setSpan(clickableSpan1, 21, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorText)),21,25,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_connector.setText(sb2);
            tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());*/
        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("es")) {
            String newphoneno_es = this.getResources().getString(R.string.drivpad_info);
            Log.e("newphoneno_es", newphoneno_es);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_es);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 18, 26, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                    startActivity(intentBrowser);
                   /* changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                    changePhoneNoDialog.show();*/
                }
            };
            sb.setSpan(clickableSpan, 18, 26, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 18, 26, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            headingDrivePadInfo.setText(sb);
            headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());

/*            String newconnector_es = this.getResources().getString(R.string.new_connector);

            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_es);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 26, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent startQrcodeIntent = new Intent(ProfileActivity.this,QrCodeActivity.class);
                    startQrcodeIntent.putExtra("comingFromProfile","1");
                    startActivity(startQrcodeIntent);
                }
            };
            sb2.setSpan(clickableSpan1, 26, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorText)),26,30,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_connector.setText(sb2);
            tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());*/

        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("fr")) {
            String newphoneno_fr = this.getResources().getString(R.string.drivpad_info);
            Log.e("newphoneno_fr", newphoneno_fr);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_fr);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 22, 31, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                    startActivity(intentBrowser);
                    /*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                    changePhoneNoDialog.show();*/
                }
            };
            sb.setSpan(clickableSpan, 22, 31, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 22, 31, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            headingDrivePadInfo.setText(sb);
            headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());

            /*String newconnector_fr = this.getResources().getString(R.string.new_connector);
            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_fr);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 33, 36, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent startQrcodeIntent = new Intent(ProfileActivity.this,QrCodeActivity.class);
                    startQrcodeIntent.putExtra("comingFromProfile","1");
                    startActivity(startQrcodeIntent);
                }
            };
            sb2.setSpan(clickableSpan1, 33, 36, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorText)),33,36,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_connector.setText(sb2);
            tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());*/
        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("nl")) {
            String newphoneno = this.getResources().getString(R.string.drivpad_info);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 29, 37, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                    startActivity(intentBrowser);
                    /*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                    changePhoneNoDialog.show();*/
                }
            };
            sb.setSpan(clickableSpan, 29, 37, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 29, 37, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            headingDrivePadInfo.setText(sb);
            headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());

            /*String newconnector_nl = this.getResources().getString(R.string.new_connector);
            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_nl);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 23, 27, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent startQrcodeIntent = new Intent(ProfileActivity.this,QrCodeActivity.class);
                    startQrcodeIntent.putExtra("comingFromProfile","1");
                    startActivity(startQrcodeIntent);
                }
            };
            sb2.setSpan(clickableSpan1, 23, 27, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorText)),23, 27,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_connector.setText(sb2);
            tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());*/
        } else {
            String newphoneno_en = "If you do not have a DrivePad,";
            Log.e("newphoneno_en", newphoneno_en);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_en);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 21, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                    startActivity(intentBrowser);
                    /*changePhoneNoDialog = new ChangePhoneNumberDialog(ProfileActivity.this);
                    changePhoneNoDialog.show();*/
                }
            };
            sb.setSpan(clickableSpan, 21, 29, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            headingDrivePadInfo.setText(sb);
            headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());

            /*String newconnector_en = "New DrivePad? Update here";

            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_en);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent startQrcodeIntent = new Intent(ProfileActivity.this, QrCodeActivity.class);
                    startQrcodeIntent.putExtra("comingFromProfile", "1");
                    startActivity(startQrcodeIntent);
                }
            };
            sb2.setSpan(clickableSpan1, 21, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_connector.setText(sb2);
            tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());*/
        }


        setupActionBar();
        img_Scan_qrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtils.isNetworkAvailable()) {
                    QrScanner(v);
                    isFinishing();
                } else {
                    AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(QrCodeActivity.this);
                    error_No_Internet.setMessage(QrCodeActivity.this.getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_No_Internet.show();

                }




          /*      //FOR UPDATION OF UUID
                  //code is 409 conflict uuid already exist
                //  String uuid = DataHandler.getStringPreferences(AppConstants.UUID);
                List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                mParams.add(new BasicNameValuePair("phone_number","+923361056568"));
                mParams.add(new BasicNameValuePair("uuid_old","09edc26d-80cc-493c-b8f5-9cd035c4"));
                mParams.add(new BasicNameValuePair("uuid","09edc26d-80cc-493c-b8f5-9cd035c5"));
                Log.e("Params",""+mParams);
                progressBarDialogQrCodeDialog = new ProgressBarDialog(QrCodeActivity.this);
                progressBarDialogQrCodeDialog.setTitle(getString(R.string.title_progress_dialog));
                progressBarDialogQrCodeDialog.setMessage(getString(R.string.body_progress_dialog));
                progressBarDialogQrCodeDialog.show();
                //   QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(ApplicationController.getmAppcontext(),"uuidUpdate",mParams);
                QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(ApplicationController.getmAppcontext(), WebServiceConstants.END_POINT_UPDATE_UUID,mParams);
                //QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(ApplicationController.getmAppcontext(), WebServiceConstants.END_POINT_UPDATE_PHONE_NUMBER,mParams);

                qrCodeAsyncTask.execute(*/
                ;


                //   QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(ApplicationController.getmAppcontext(),"uuidUpdate",mParams);
                //   QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(ApplicationController.getmAppcontext(), WebServiceConstants.END_POINT_UUID,mParams);
                //QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(ApplicationController.getmAppcontext(), WebServiceConstants.END_POINT_UPDATE_PHONE_NUMBER,mParams);

                //  qrCodeAsyncTask.execute();
            }
        });
    }

    public void QrScanner(View view) {


        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.setAutoFocus(true);
        mScannerView.startCamera();

        // Start camera
        // mScannerView.resumeCameraPreview(this);
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }

    //For custom fonts we are using calligraphy lib
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    @Override
    public void handleResult(Result rawResult) {
        Log.e("handler", rawResult.getText()); // Prints scan results
        Log.e("handler", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode)
        // show the scanner result into dialog box.

        qrCodeUIDdialog = new QrCodeUIDdialog(QrCodeActivity.this, rawResult.getText());
        qrCodeUIDdialog.show();
    }

    /*  public boolean isCameraUsebyApp() {
          Camera camera = null;
          try {
              camera = Camera.open();
          } catch (RuntimeException e) {
              return true;
          } finally {
              if (camera != null) camera.release();
          }
          return false;
      }*/
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mScannerView == null) {
            mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        }
        mScannerView.stopCameraPreview();
        mScannerView.stopCamera();
        mScannerView = null;
/*        if (!goBack){
            if (mScannerView==null){
                mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
            }
            mScannerView.stopCameraPreview();
            mScannerView.stopCamera();
            mScannerView=null;
            startActivity(new Intent(this,QrCodeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }else {
            super.onBackPressed();
        }*/

        //setResult(RESULT_OK);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mScannerView == null) {
            mScannerView = new ZXingScannerView(this);
            mScannerView.stopCamera();
            // Programmatically initialize the scanner view
        } else {
            mScannerView.stopCamera();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mScannerView == null) {
            mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        }
        if (getIntent().getStringExtra("comingFromProfile") != null && getIntent().getExtras().getString("comingFromProfile").equalsIgnoreCase("1")) {
            goBack = true;
        }

    }
}