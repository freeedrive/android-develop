package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.PREF_KEY_FD_STATUS;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.fdBlockedScreen;

/**
 * Created by mcs on 12/28/2016.
 */

public class FetchDrivingPerformanceScores extends BaseAsyncTask {




    public FetchDrivingPerformanceScores(Context context, String route, List<NameValuePair> pp) {
        super(context, route, pp);
    }



    /**
     * AsyncTask method basic calls during a request, calls the parent's method.
     */
    protected String doInBackground(String... params) {

        return "";
    }
    /**
     * AsyncTask method basic calls after a request.
     */
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s != null) {
            int intResponse = Integer.parseInt(response);
            Log.e("ResponseCode", "" + intResponse);
            Log.e("response", "" + response);
            Log.e("resultat", "" + resultat);
            JSONObject jsonObject = new JSONObject();
            JSONArray syncresponse = new JSONArray();
            JSONObject drivingPerformanceObject = new JSONObject();

            switch (intResponse) {
                case 200:

                try{

                    if (resultat!=null || !resultat.isEmpty()) {

                        JSONObject objAccount = new JSONObject(resultat);

                        boolean fd_active_status_flag = objAccount.getBoolean("active");
                        if(fd_active_status_flag == false){
                            DataHandler.updatePreferences(PREF_KEY_FD_STATUS,fd_active_status_flag);
                            DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY,false);
                            fdBlockedScreen(context);
                            /*Intent intent = new Intent(context,BlockedFreeDriveActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            context.startActivity(intent);*/
                        }

                        if(objAccount.getJSONArray("scores")!=null&&!objAccount.getJSONArray("scores").equals("") ){
                            syncresponse = objAccount.getJSONArray("scores");
                            Log.e("syncresponse",""+syncresponse.toString());
                        }
                        if(objAccount.getJSONObject("contract")!=null&&!objAccount.getJSONObject("contract").equals("") ){
                            drivingPerformanceObject = objAccount.getJSONObject("contract");
                        }
                        if (objAccount.get("avg_performance")!=null && !objAccount.getString("avg_performance").equalsIgnoreCase("")){
                            int avg_performance = objAccount.getInt("avg_performance");
                            DataHandler.updatePreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_SCORE,avg_performance);

                        }

                             int private_hours_enable = objAccount.getInt("private_hour_enable");
                                 DataHandler.updatePreferences(AppConstants.ACCESS_PRIVATE_HOURS, private_hours_enable);


                        if (drivingPerformanceObject!=null&&drivingPerformanceObject.length()>0){
                            boolean driving_performanceActive = false;
                            for (int k=0;k<drivingPerformanceObject.length();k++){

                                 driving_performanceActive = drivingPerformanceObject.getBoolean("driving_performance");

                                  /*if (!driving_performanceActive.equalsIgnoreCase("")){
                                     break;
                                 }*/
                            }
                            if (driving_performanceActive == true){
                                DataHandler.updatePreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE,true);
                            }else {
                                DataHandler.updatePreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE,false);

                            }
                        }
                        if (syncresponse!=null&&syncresponse.length()>0){
                            for (int j=0; j<syncresponse.length();j++){
                                JSONObject ScorejsonObject = syncresponse.getJSONObject(j);

                                int remote_id= ScorejsonObject.getInt("remote_id");
                                int over_speeding_count = ScorejsonObject.getInt("over_speeding_count");
                                if (over_speeding_count==-1){
                                    over_speeding_count = 0;
                                }

                                upDatePerformanceScores(remote_id,over_speeding_count);
                            }

                        }



                        Intent intent = new Intent("com.freeedrive_saving_driving.speeding_event");
                        context.sendBroadcast(intent);

                        try{
                            objAccount = null;
                            syncresponse = null;
                        }catch (Exception e){

                        }



                    }

                } catch (JSONException e) {
                    try{

                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }

                    e.printStackTrace();
                }
                catch (Exception e) {
                    e.printStackTrace();

                }
                break;
                /*case 400:
                    //Phone numbre not found
                    AlertDialog.Builder error_400 = new AlertDialog.Builder(context);
                    error_400.setMessage(context.getResources().getString(R.string.error_auth_token_expire_400)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deletePreference(FILE_NAME_SHARED_PREF);
                            fdLogout(context);
                            DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY,false);
                            dialog.dismiss();
                        }
                    });
                    error_400.show();
                    break;*/
                case 500:
                    try{

                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }

                    try{
                        AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                        error_500.setMessage(context.getResources().getString(R.string.error_login_404)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_500.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    break;
            }
        }else{


            try{

            }catch (NullPointerException exception){
                exception.printStackTrace();
            }

        }
    }



    private void upDatePerformanceScores(int remote_id,int over_speed_count) {
        RideBDD tmp = new RideBDD(context);
        tmp.open();
        tmp.updateRidePerformance(remote_id,over_speed_count);
        tmp.close();
    }


}
