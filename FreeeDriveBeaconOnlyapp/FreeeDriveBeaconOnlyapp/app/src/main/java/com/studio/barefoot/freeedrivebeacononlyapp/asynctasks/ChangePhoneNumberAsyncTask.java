package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.QrCodeActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.SmsConfirmationActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.PrivateHoursDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ChangePhoneNumberDialog.progressBarDialogCHngPhn;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesFragment.progressBarDialogNotifications;

/**
 * Created by mcs on 1/5/2017.
 */

public class ChangePhoneNumberAsyncTask extends BaseAsyncTask {
    public static ProgressBarDialog progressBarDialogSyncDataFromUpdatePhoneNUmberDialog;

    /**
     * AsyncTask method basic calls during a request, calls the parent's method.
     */
    protected String doInBackground(String... params) {

        return "";
    }
    public ChangePhoneNumberAsyncTask(Context context, String route, List<NameValuePair> pp) {
        super(context, route, pp);

    }

    /**
     * AsyncTask method basic calls after a request.
     */
    protected void onPostExecute(String s) {
        super.onPostExecute(s);


        Log.e("s",s);
        if(s != null) {
            int intResponse = Integer.parseInt(response);
            Log.e("ResponseCode", "" + intResponse);
            Log.e("response", "" + response);
            Log.e("resultat",resultat);
            try{
                progressBarDialogCHngPhn.dismiss();
                progressBarDialogCHngPhn=null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }
            switch (intResponse) {
                case 200:
                    String switchNumbers= DataHandler.getStringPreferences(AppConstants.PHONE_TEMP);
                    DataHandler.updatePreferences(AppConstants.PHONE_NUMBER,switchNumbers);
                    context.startActivity(new Intent(context, SmsConfirmationActivity.class));
                    Log.e("Response",""+intResponse);
                    break;
                case 422:
                    //duplicate entiry
                    break;
                case 404:
                    AlertDialog.Builder error_404 = new AlertDialog.Builder(context);
                    error_404.setMessage(context.getResources().getString(R.string.error_sms_404)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_404.show();
                    //not found
                    break;
                case 500:
                    AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                    error_500.setMessage(context.getResources().getString(R.string.error_sms_500)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_500.show();
                    //internal error
                    break;
                case 401:
                    //phone no not found
                    AlertDialog.Builder error_401 = new AlertDialog.Builder(context);
                    error_401.setMessage(context.getResources().getString(R.string.error_changePhone_401)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_401.show();
                    break;

                case 403:
                    //authentication
                    break;
                case 409:
                    //number already exists
                    AlertDialog.Builder error_409 = new AlertDialog.Builder(context);
                    error_409.setMessage(context.getResources().getString(R.string.error_changePhone_409)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_409.show();

                    break;
                case 594:
                    //uuid already exists
                    String verifiedPhoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_TEMP);
                    DataHandler.updatePreferences(AppConstants.PHONE_NUMBER,verifiedPhoneNumber);
                    DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY,true);
                    FetchUserDetails();

                    break;
                case 593:
                    //uuid does not exists
                    context.startActivity(new Intent(context,QrCodeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    DataHandler.updatePreferences((AppConstants.USER_AWAITING_QR_CODE),true);

                    break;
            }
        }
    }
    private void FetchUserDetails() {
        progressBarDialogSyncDataFromUpdatePhoneNUmberDialog = new ProgressBarDialog(context);
        progressBarDialogSyncDataFromUpdatePhoneNUmberDialog.setTitle("Syncing");
        progressBarDialogSyncDataFromUpdatePhoneNUmberDialog.setMessage("wait..");
        progressBarDialogSyncDataFromUpdatePhoneNUmberDialog.show();
        List<NameValuePair> mParams = new ArrayList<NameValuePair>();
        mParams.add(new BasicNameValuePair("phone_number",DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));

        FetchProfileAsyncTaskForLogin fetchProfileAsyncTask = new FetchProfileAsyncTaskForLogin(context, WebServiceConstants.END_POINT_FETCH_PROFILE,mParams);
        fetchProfileAsyncTask.execute();
    }
}
