package com.studio.barefoot.freeedrivebeacononlyapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.MenuActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.NotificationBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import at.blogc.android.views.ExpandableTextView;

import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesFragment.stringArrayListOfPositionsDeleted;

/**
 * Created by macbookpro on 4/26/17.
 */

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MyViewHolder> {


    public static ArrayList<NotificationBeans> ridesBeansList;
    public static boolean isToggled = false;
    public boolean showMore = false;
    public boolean pleaseToggle = false;
    Context context;

    MenuActivity.OnBackPressedListener onBackPressedListener;
    boolean undoOn; // is undo on, you can turn it on from the toolbar menu
   /* List<String> itemsPendingRemoval;
    private Handler handler = new Handler(); // hanlder for running delayed runnables
    HashMap<String, Runnable> pendingRunnables = new HashMap<>(); // map of items to pending runnables, so we can cancel a removal if need be
*/
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.expandable_list_messages_header, parent, false);


        return new MyViewHolder(itemView);    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
/*
        stringArrayListOfPositionsDeleted = new ArrayList<>();
*/
/*
        itemsPendingRemoval = new ArrayList<>();
*/
        NotificationBeans ridesBeans = ridesBeansList.get(position);
        String dateofRide = ridesBeans.getDateTime();
        String title = ridesBeans.getTitle();
        String body = ridesBeans.getBody();
        final String item = String.valueOf(ridesBeansList.get(position));
        holder.title_messages.setText(title);
        holder.header.setText(dateofRide +" | ");
        holder.footer.setText(body);
        holder.footer.setAnimationDuration(1000l);

        if (body.length()>70){
            showMore = true;
        }else{
            showMore = false;
        }
        // set interpolators for both expanding and collapsing animations
        holder.footer.setInterpolator(new OvershootInterpolator());
        if (showMore){
            holder.more.setVisibility(View.VISIBLE);
        }else{
            holder.more.setVisibility(View.INVISIBLE);
        }
        if (isToggled){
            holder.checkBox.setVisibility(View.VISIBLE);
        }
        if (MessagesFragment.areAllSelected){
            holder.checkBox.setChecked(true);


        }else {
            holder.checkBox.setChecked(false);

        }
/*        if (itemsPendingRemoval.contains(item)) {
            // we need to show the "undo" state of the row
            holder.itemView.setBackgroundColor(Color.RED);

            holder.header.setVisibility(View.GONE);
            holder.footer.setVisibility(View.GONE);
            holder.checkBox.setVisibility(View.GONE);
            holder.more.setVisibility(View.GONE);

    *//*        holder.undoButton.setVisibility(View.VISIBLE);
            holder.undoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // user wants to undo the removal, let's cancel the pending task
                    Runnable pendingRemovalRunnable = pendingRunnables.get(item);
                    pendingRunnables.remove(item);
                    if (pendingRemovalRunnable != null) handler.removeCallbacks(pendingRemovalRunnable);
                    itemsPendingRemoval.remove(item);
                    // this will rebind the row in "normal" state
                    notifyItemChanged(stringArrayListOfPositionsDeleted.indexOf(item));
                }
            });*//*
        }else {*/
            // we need to show the "normal" state


// or set them separately
        holder.footer.setExpandInterpolator(new OvershootInterpolator());
        holder.footer.setCollapseInterpolator(new OvershootInterpolator());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });


        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.footer.toggle();
               // holder.more.setText(holder.footer.isExpanded() ? "less" : "more");
                /*if ( holder.footer.isExpanded())
                {
                    holder.footer.collapse();
                    holder.more.setText("less");
                }
                else
                {
                    holder.footer.expand();
                    holder.more.setText("more..");
                }*/
            }
        });


        holder.footer.setOnExpandListener(new ExpandableTextView.OnExpandListener()
        {
            @Override
            public void onExpand(final ExpandableTextView view)
            {
                holder.more.setText("..less");
            }

            @Override
            public void onCollapse(final ExpandableTextView view)
            {
                holder.more.setText("more..");
            }
        });


        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {

                    if (isChecked){
                        stringArrayListOfPositionsDeleted.add(ridesBeansList.get(holder.getLayoutPosition()).getMessagesID());
                        Log.e("PositionsAdded", ""+stringArrayListOfPositionsDeleted);
                    }else {
                        int index =-1;

                        // Loop through array and find index of item unchecked
                        for (int i = 0; i < stringArrayListOfPositionsDeleted.size(); i++) {
                            if (ridesBeansList.get(holder.getLayoutPosition()).getMessagesID() == stringArrayListOfPositionsDeleted.get(i)) {
                                index = i;
                                break;
                            }
                        }

                        // If index was found -> remove the item
                        if (index != -1)
                            stringArrayListOfPositionsDeleted.remove(index);
                        Log.e("PositionsDeleted", ""+stringArrayListOfPositionsDeleted);


                    }

                }catch (IndexOutOfBoundsException e){

                }
            }
        });
    }

    public MessagesAdapter(ArrayList<NotificationBeans> moviesList) {
        this.ridesBeansList = moviesList;
    }


    public void setIsToggled(boolean isToggeld) {
        this.isToggled = isToggeld;
        pleaseToggle = true;
    }
    public void setUndoOn(boolean undoOn) {
        this.undoOn = undoOn;
    }


    public boolean isUndoOn() {
        return undoOn;
    }
/*    public void pendingRemoval(int position) {
        final String item = String.valueOf(ridesBeansList.get(position));
        if (!itemsPendingRemoval.contains(item)) {
            itemsPendingRemoval.add(item);
            // this will redraw row in "undo" state
            notifyItemChanged(position);
            // let's create, store and post a runnable to remove the item
            Runnable pendingRemovalRunnable = new Runnable() {
                @Override
                public void run() {
                    remove(ridesBeansList.indexOf(item));
                }
            };
            handler.postDelayed(pendingRemovalRunnable, 3000);
            pendingRunnables.put(item, pendingRemovalRunnable);
        }
    }*/

    public void remove(int position) {
       // String item = String.valueOf(ridesBeansList.get(position));
        stringArrayListOfPositionsDeleted.add(ridesBeansList.get(position).getMessagesID());

      /*  if (itemsPendingRemoval.contains(item)) {
            itemsPendingRemoval.remove(item);
        }*/
      /*  if (ridesBeansList.contains(item)) {
            ridesBeansList.remove(position);
            notifyItemRemoved(position);
        }*/
    }

   /* public boolean isPendingRemoval(int position) {
        String item =String.valueOf(ridesBeansList.get(position));
        return itemsPendingRemoval.contains(item);
    }*/
    @Override
    public int getItemCount() {
        return ridesBeansList.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView header,more,title_messages ;
        public final ExpandableTextView footer;
        public  CheckBox checkBox;

        public MyViewHolder(View view) {
            super(view);
            header = (TextView) view.findViewById(R.id.date_time);
            more = (TextView) view.findViewById(R.id.button_toggle);
            title_messages = (TextView) view.findViewById(R.id.title_messages);
            checkBox = (CheckBox) view.findViewById(R.id.checkbox_Delete);
         //   checkBox.setVisibility(View.INVISIBLE);
            footer = (ExpandableTextView) view.findViewById(R.id.secondLine);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    isToggled = true;
                    int p=getLayoutPosition();
                    MessagesFragment.deleteMessages.setVisibility(View.VISIBLE);
                    /*MessagesFragment.deleteAllMessages.setVisibility(View.VISIBLE);*/
                    MessagesFragment.selectAll.setVisibility(View.VISIBLE);
                    notifyDataSetChanged();

                    Log.e("clicked","clicke"+ridesBeansList.get(0));
                    return true;// returning true instead of false, works for me
                }
            });


        }

    }




}
