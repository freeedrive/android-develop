package com.studio.barefoot.freeedrivebeacononlyapp.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

import java.io.IOException;


/**
 * Base class to handler Firebase Instance ID token refresh events. (push notification)
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    /**
     * Firebase Method basic. It is called when the token is refrsh
     */
    @Override
    public void onTokenRefresh() {
        //super.onTokenRefresh();
        //Getting registration token

        try {
            String refreshedToken =FirebaseInstanceId.getInstance().getToken();
                    Log.e("refreshedToken",refreshedToken);
            DataHandler.updatePreferences(AppConstants.FIRE_BASE_TOKE,refreshedToken);
        }catch (Exception e){
            Log.e("ExceptionFireBase",""+e);

        }

        //Displaying token on logcat
        //Log.d("Adneom", "Refreshed token: " + refreshedToken);
    }



    private void sendRegistrationToServer(String token) {
        //You can implement this method to store the token on your server
        //Not required for current project
    }

}
