package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.MenuActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.QrCodeActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.SmsConfirmationActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.LoginActivity.progressBarDialogLogin;

/**
 * Created by mcs on 12/28/2016.
 */

public class LoginAsyncTask extends BaseAsyncTask {
    private JSONArray jsonArray;
    public static ProgressBarDialog progressBarDialogSyncData;


    public LoginAsyncTask(Context context, String route, List<NameValuePair> pp) {
        super(context, route, pp);
    }

    /**
     * AsyncTask method basic calls during a request, the parent's method is called
     */
    protected String doInBackground(String... params) {
        return "";
    }

    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if(s != null) {
            int intResponse = Integer.parseInt(response);
            Log.e("Response",""+intResponse);
            try{
                Log.e("token",resultat);

            }catch (NullPointerException exception){
                exception.printStackTrace();
            }

            switch (intResponse){
                case 200 : //success
                     if(!resultat.isEmpty()){
                         String token = resultat;
                         token =  token.replaceAll("\"", "");
                         DataHandler.updatePreferences(AppConstants.TOKEN_NUMBER,token);
                         Log.e("Token Login",resultat);
                     }

                    //sms verfified and uuid is also verified
                  //  if (DataHandler.getStringPreferences(AppConstants.UUID).equalsIgnoreCase("")||DataHandler.getStringPreferences(AppConstants.UUID)==null){
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    FetchUserDetails();

                  //  }else{
                    /*    Intent menuAcitivtyIntent = new Intent(context, MenuActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(menuAcitivtyIntent);
                    }*/

                 break;
                case 409://sms confirmation
                /*    Intent menuAcitivtyIntent2 = new Intent(context, MenuActivity.class);
                    context.startActivity(menuAcitivtyIntent2);*/
                    Intent smsCodeIntent = new Intent(context, SmsConfirmationActivity.class);
                  //  smsCodeIntent.putExtra(AppConstants.EXTRA_KEY_LOGIN_ACTIVITY,AppConstants.EXTRA_VALUE_LOGIN_ACTIVITY);
                    context.startActivity(smsCodeIntent);
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;
                case 401://user not registered
                    try{
                        AlertDialog.Builder error_401 = new AlertDialog.Builder(context);
                        error_401.setMessage(context.getResources().getString(R.string.error_login_401aa)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_401.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                     break;
                case 403:// user device id is changed
                    try{
                        AlertDialog.Builder error_422 = new AlertDialog.Builder(context);
                        error_422.setMessage(context.getResources().getString(R.string.error_login_403aa)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_422.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;

                case 404:
                    try{
                        AlertDialog.Builder error_404 = new AlertDialog.Builder(context);
                        error_404.setMessage(context.getResources().getString(R.string.error_login_404)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_404.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;
                case 405:
                    Intent qrCodeIntent = new Intent(context, QrCodeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(qrCodeIntent);
                 /*   DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY,true);
                    Intent menuAcitivtyIntent3 = new Intent(context, MenuActivity.class);
                    context.startActivity(menuAcitivtyIntent3);*/
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;
                case 500:
                    try{
                        AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                        error_500.setMessage(context.getResources().getString(R.string.error_login_404)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_500.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;

            }
        }else {
            try{
                progressBarDialogLogin.dismiss();
                progressBarDialogLogin =null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }

        }
        }

    private void FetchUserDetails() {
        progressBarDialogSyncData = new ProgressBarDialog(context);
        /*progressBarDialogSyncData.setTitle(context.getString(R.string.title_progress_dialog));
        progressBarDialogSyncData.setMessage(context.getString(R.string.body_progress_dialog));*/
        progressBarDialogSyncData.setTitle("Syncing");
        progressBarDialogSyncData.setMessage("wait..");
        progressBarDialogSyncData.show();
        List<NameValuePair> mParams = new ArrayList<NameValuePair>();
        mParams.add(new BasicNameValuePair("phone_number",DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));

        FetchProfileAsyncTaskForLogin fetchProfileAsyncTask = new FetchProfileAsyncTaskForLogin(context, WebServiceConstants.END_POINT_FETCH_PROFILE,mParams);
        fetchProfileAsyncTask.execute();
    }
}
