package com.studio.barefoot.freeedrivebeacononlyapp.appcontroller;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.sensors.FDsensors;
import com.studio.barefoot.freeedrivebeacononlyapp.services.Detector;
import com.studio.barefoot.freeedrivebeacononlyapp.services.FDGPSTracker;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;

import java.util.Date;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by mcs on 12/21/2016.
 */

public class ApplicationController extends Application
{

    private  static Context mAppcontext;
    /**
     * Represents the Sensor
     */
    public FDsensors fdSensors;

    /**
     * Timestamp for the departure time
     */
    public static long departure_time;
    /**
     * Tiestamp for the arrival time
     */
    public static long arrival_time;

    /**
     * Represents the departure's location
     */
    public Location departureLocation;

    /**
     * Represents the arrival's location
     */
    public Location arrivalLocation;
    /**
     * Allows to get the current location
     */

    BluetoothAdapter mBluetoothAdaptater;
    public FDGPSTracker fdgpsTracker;
    private Locale locale = null;
    String language = "";
    Configuration config;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Fabric.with(this, new Answers());
        mAppcontext = getApplicationContext();
        //setDevicePolicy();

        new DataHandler(mAppcontext);
        config = getBaseContext().getResources().getConfiguration();
        try{
            language = DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG);
            Log.e("language","appcontroller :"+language);
            if(language !=null && !language.isEmpty()){
                //default to the phone's language if EN, FR or NL
                String lang = language;
                Log.e("lang","if :"+lang);
                locale = new Locale(lang);
                locale.setDefault(locale);
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

            }
            else{
                //default to the phone's language if EN, FR or NL

                String currentLang = config.locale.getLanguage();
                if (currentLang.equalsIgnoreCase("fr")){
                    currentLang="fr";
                }else if (currentLang.equalsIgnoreCase("nl")){
                    currentLang="nl";
                }else if(currentLang.equalsIgnoreCase("es")){
                    currentLang="es";
                }else if (currentLang.equalsIgnoreCase("en")){
                    currentLang="en";
                }else {
                    currentLang="en";
                }
                LocaleUtils.setLocale(new Locale(currentLang));
                LocaleUtils.updateConfig(this, getBaseContext().getResources().getConfiguration());
            }
        }catch (Exception e){
            e.printStackTrace();

            //default to the phone's language if EN, FR or NL
            String currentLang = config.locale.getLanguage();
            LocaleUtils.setLocale(new Locale(currentLang));
            LocaleUtils.updateConfig(this, getBaseContext().getResources().getConfiguration());
        }


        DataHandler.updatePreferences(AppConstants.FIRST_TIME_USER,1);
        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/DINNextLTPro-LightCondensed.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
        startService(new Intent(this, Detector.class));
        fdSensors = new FDsensors(getmAppcontext());
        fdgpsTracker = new FDGPSTracker(getmAppcontext());

        //set unlock to TRUE by default :
        AppUtils.isUnlock = false;
        if(DataHandler.getLongreferences(AppUtils.LAST_UPDATE)!=null){
            if (DataHandler.getLongreferences(AppUtils.LAST_UPDATE)==0){
                Date lastUpdate = new Date();
                DataHandler.updatePreferences(AppUtils.LAST_UPDATE,lastUpdate.getTime());
            }
        }

    }
    public void updateLocale(String lang)
    {
        LocaleUtils.setLocale(new Locale(lang));
        //    LocaleUtils.getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        LocaleUtils.updateConfig(this, getBaseContext().getResources().getConfiguration());


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleUtils.updateConfig(this, newConfig);
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    @Override
    public void onTerminate() {
        super.onTerminate();
        startService(new Intent(this, Detector.class));

    }

    public static Context getmAppcontext() {
        return mAppcontext;
    }

    /*public  static void setmAppcontext(Context appcontext){
        mAppcontext = appcontext;
    }*/

 /*   private void setDevicePolicy() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD) {
            final StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    try {
                        StrictMode.setThreadPolicy(policy);
                    } catch (Throwable ex) {}
                }
            });
        }
    }*/


}
