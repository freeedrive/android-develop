package com.studio.barefoot.freeedrivebeacononlyapp.databases;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LongSparseArray;

import com.studio.barefoot.freeedrivebeacononlyapp.beans.NotificationBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.RidesBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.SubRideBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;


public class RideBDD {

    private static final int VERSION_BDD = 11;

    /**
     * Represents the database name
     */
    private static final String NOM_BDD = "freeedriveV2.db";
    /**
     * Represents the tabale name
     */
    private static final String TABLE_RIDES = "table_rides";


    /**
     * Represents the tabale name
     */
    private static final String TABLE_SUB_RIDES = "table_sub_rides";

    /**
     * Represents column ID in the tabale
     */
    private static final String COL_ID = "ID";
    /**
     * Represents position column ID in the tabale
     */
    private static final int NUM_COL_ID = 0;
    /**
     * Represents column departure time in the tabale
     */
    private static final String COL_DEPARTURE_TIME = "DEPARTURE_TIME";
    /**
     * Represents position column departure time in the tabale
     */
    private static final int NUM_COL_DEPARTURE_TIME = 1;
    /**
     * Represents column arrival time in the tabale
     */
    private static final String COL_ARRIVAL_TIME = "ARRIVAL_TIME";
    /**
     * Represents position column arrival time in the tabale
     */
    private static final int NUM_COL_ARRIVAL_TIME = 2;
    /**
     * Represents column departure location in the tabale
     */
    private static final String COL_DEPARTURE_LOCATION = "DEPARTURE_LOCATION";
    /**
     * Represents position departure location in the tabale
     */
    private static final int NUM_COL_DEPARTURE_LOCATION = 3;
    /**
     * Represents column arrival location in the tabale
     */
    private static final String COL_ARRIVAL_LOCATION = "ARRIVAL_LOCATION";
    /**
     * Represents position column arrival location in the tabale
     */
    private static final int NUM_COL_ARRIVAL_LOCATION = 4;
    /**
     * Represents column elapsed in the tabale
     */
    private static final String COL_TIME_ELAPSED = "TIME_ELAPSED";
    /**
     * Represents position column elapsed in the tabale
     */
    private static final int NUM_COL_TIME_ELAPSED = 5;
    /**
     * Represents column score in the tabale
     */
    private static final String COL_SCORE = "SCORE";

    /**
     * Represents position column score in the tabale
     */
    private static final int NUM_COL_SCORE = 6;

    // Represents column count of bad behaviour


    /**
     * Represents column driver id in the tabale
     */
    private static final String COL_DRIVER_ID = "DRIVE_ID";
    /**
     * Represents position column driver id in the tabale
     */
    private static final int NUM_COL_DRIVER_ID = 7;
    /**
     * Represents column company id in the tabale
     */
    private static final String COL_COMPANY_ID = "COMPANY_ID";
    /**
     * Represents position column company id in the tabale
     */
    private static final int NUM_COL_OMPANY_ID = 8;
    /**
     * Represents column send(@Boolean) in the tabale
     */
    private static final String COL_SEND = "SEND";
    /**
     * Represents position column send in the tabale
     */
    private static final int NUM_COL_SEND = 9;
    /**
     * Represents column time in the tabale
     */
    private static final String COL_TIME = "TIME";
    /**
     * Represents position column time in the tabale
     */
    private static final int NUM_COL_TIME = 10;
    /**
     * Represents column avg time in the tabale
     */
    private static final String COL_AVG_TIME = "AVG_TIME";
    /**
     * Represents position column avg time in the tabale
     */
    private static final int NUM_COL_AVG_TIME = 11;
    /**
     * Represents column total time in secondes in the tabale
     */
    private static final String COL_TIME_TOTAL_SECONDES = "TIME_TOTAL_SECONDES";
    /**
     * Represents position column total time in secondes in the tabale
     */
    private static final int NUM_COL_TIME_TOTAL_SECONDES = 12;

    private static final String COL_BAD_BEHAVIOUR = "COUNT_BAD_BEHAVIOUR";
    private static final int NUM_COL_BAD_BEHAVIOUR = 13;

    private static final String COL_TIME_ABOVE_120 = "COL_TIME_ABOVE_120";
    private static final int NUM_COL_TIME_ABOVE_120 = 14;

    private static final String COL_CAUSE_OF_ENDING_RIDE = "COL_CAUSE_OF_ENDING_RIDE";
    private static final int NUM_COL_CAUSE_OF_ENDING_RIDE = 15;


    private static final String COL_SUDDEN_ACCELERATION = "COL_SUDDEN_ACCELERATION";
    private static final int NUM_COL_SUDDEN_ACCELERATION = 16;

    private static final String COL_SUDDEN_BRAKING = "COL_SUDDEN_BRAKING";
    private static final int NUM_COL_SUDDEN_SPEEDING = 19;
    private static final int NUM_COL_SUDDEN_BRAKING = 17;
    private static final String COL_DISTANCE_TRAVELLED = "DISTANCE_TRAVELLED";
    private static final int NUM_COL_DISTANCE_TRAVELLED = 18;

    private static final int NUM_COL_IS_SPEEDING_UPDATED = 20;

    /**
     * Represents the table name
     * For displaying offline messages
     */
    private static final String TABLE_MESSAGES = "table_messages";
    private static final String COL_MESSAGES_ID = "ID";
    private static final String COL_REMOTE_MESSAGES_ID = "REMOTE_MESSAGES_ID";
    private static final String COL_MESSAGES_DATE_TIME = "SCHEDULED_AT";
    private static final String COL_MESSAGES_TITLE = "MESSAGES_TITLE ";
    private static final String COL_MESSAGES_BODY = "MESSAGES_BODY";
    private static final String COL_MESSAGES_IS_DELETED = "IS_DELETED";
    private static final String COL_SUDDEN_SPEEDING = "COL_OVER_SPEEDING";
    private static final String COL_IS_SPEEDING_UPDATED = "COL_IS_OVER_SPEEDING_UPDATED";
    private static final String COL_TIME_ZONE = "TIME_ZONE";
    private static final int NUM_COL_TIME_ZONE = 19;
    private static final String COL_ORPHAN_RIDE = "COL_ORPHAN_RIDE";
    private static final int NUM_COL_ORPHAN_RIDE = 20;
    /**
     * Represents the current database
     */
    private SQLiteDatabase bdd;
    /**
     * Represents the base sqlite
     */
    private MyBaseSQLite myBaseSQLite;

    /**
     * Represents the context : Activity
     */
    private Context context;

    /**
     * Indicates if we can send the ride
     * If the ride has a locality and no cordonates.
     */
    private Boolean canSaveRide = true;

    /**
     *
     */
    private JSONObject jObject;
    /**
     *
     */
    public JSONArray listeWithLocality = new JSONArray();
    /**
     *
     */
    public int sizeCurrentListe;
    /**
     *
     */
    public int currentIndex;

    /**
     * Class Constructor
     *
     * @param context(in), @Activity represents the activity
     */
    public RideBDD(Context context) {

        myBaseSQLite = new MyBaseSQLite(context, NOM_BDD, VERSION_BDD);
        this.context = context;
    }

    /**
     * Allows to open database
     */
    public void open() {
        //on ouvre la BDD en écriture
        bdd = myBaseSQLite.getWritableDatabase();
    }

    /**
     * Allows to close database
     */
    public void close() {
        //on ferme l'accès à la BDD
        bdd.close();
    }

    /**
     * Returns an instance o fdatabase
     *
     * @return(out), @SQLite
     */
    public SQLiteDatabase getBDD() {
        return bdd;
    }

    /**
     * Allows to set json object
     *
     * @param oo, @JSONOBject represents the current ride in format json : new value
     */
    public void setJOBject(JSONObject oo) {
        jObject = oo;
    }


/*    //get DataBase Name from SharedPreference
    public void getDataBaseName(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(FDUtils.KEY_DATA_BASE_CHECK,Context.MODE_PRIVATE);
        String dbName = sharedPreferences.getString("DataBaseName",null);
        Log.e("dbNameSP",dbName);
    }*/

    //Insert Sub Rides

    public long InsertSubRides(int id, String areaT, String startlat, String startLang, String endLat, String endLang, Long currentTime, double currentSpeed, int timeInRegion) {
        ContentValues contentValues = new ContentValues();

        contentValues.put("ID_MAIN_RIDE", id);
        contentValues.put("AREA_TYPE", areaT);
        contentValues.put("START_LAT_LNG", startlat + "," + startLang);
        contentValues.put("END_LAT_LNG", endLat + "," + endLang);
        contentValues.put("TIME_REGION_ENTERED", currentTime);
        contentValues.put("CURRENT_SPEED", currentSpeed);
        contentValues.put("TIME_LAPSED_INSIDE_A_REGION", timeInRegion);

        return bdd.insert("table_sub_rides", null, contentValues);
    }

    public long InsertMessages(int messages_remote_id, String title, String messagesBody, String dateTime) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_REMOTE_MESSAGES_ID, messages_remote_id);
        contentValues.put(COL_MESSAGES_TITLE, title);
        contentValues.put(COL_MESSAGES_BODY, messagesBody);
        contentValues.put(COL_MESSAGES_DATE_TIME, dateTime);

        return bdd.insert(TABLE_MESSAGES, null, contentValues);
    }


    /**
     * Allows to register a ride in the database
     *
     * @param ride(in), @Ride( represent the Object
     * @return(out), @Long  the id of ride saved
     */
    public long insertRide(RidesBeans ride) {
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues contentValues = new ContentValues();

        //on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        Log.e("arrivalb4", "" + ride.getArrival_time());
        Log.e("departureb4", "" + ride.getDeparture_time());
        contentValues.put(COL_DEPARTURE_TIME, ride.getDeparture_time());
        contentValues.put(COL_ARRIVAL_TIME, ride.getArrival_time());
        String arrivalTimeString = AppUtils.formate10LongDateToDisplay(ride.getArrival_time());
        DataHandler.updatePreferences(AppConstants.ARRIVAL_TIME, arrivalTimeString);
        Log.e("arrival", arrivalTimeString);
        contentValues.put(COL_DEPARTURE_LOCATION, ride.getDeparture_location());
        contentValues.put(COL_ARRIVAL_LOCATION, ride.getArrival_location());
        contentValues.put(COL_TIME_ELAPSED, ride.getTime_elapsed());
        contentValues.put(COL_SCORE, ride.getScore());
        contentValues.put(COL_DRIVER_ID, ride.getDriver_id());
        contentValues.put(COL_COMPANY_ID, ride.getCompany_id());
        contentValues.put(COL_SEND, ride.getSend());
        contentValues.put(COL_TIME, ride.getTime());
        contentValues.put(COL_AVG_TIME, ride.getAverageP());
        contentValues.put(COL_TIME_TOTAL_SECONDES, ride.getTimeSecondes());

        contentValues.put(COL_BAD_BEHAVIOUR, ride.getBad_behaviour());
        /*Log.e("getBadBehaviour",""+ride.getBad_behaviour());*/
        Log.e("getTotalTimeOver120", " : " + ride.getTotalTimeAbove120());
        contentValues.put(COL_TIME_ABOVE_120, ride.getTotalTimeAbove120());
        contentValues.put(COL_CAUSE_OF_ENDING_RIDE, ride.getCauseofendingRide());
        Log.e("causeof :", "EndingrideValue" + ride.getCauseofendingRide());
        contentValues.put(COL_SUDDEN_ACCELERATION, ride.getSudden_accelaration());
        Log.e("Sudden ", " acceleration:" + ride.getSudden_accelaration());
        contentValues.put(COL_SUDDEN_BRAKING, ride.getSudden_braking());
        Log.e("Suddent ", " braking:" + ride.getSudden_braking());
        contentValues.put(COL_DISTANCE_TRAVELLED, "10");
        contentValues.put(COL_ORPHAN_RIDE, ride.getOrphandrideBit());
        String timeZone = TimeZone.getDefault().getID();
        contentValues.put(COL_TIME_ZONE, timeZone);
        return bdd.insert(TABLE_RIDES, null, contentValues);
    }
    /*
    * This method after fetching the synced rides from server in case of an uninstall re-enters it in the db.
    * */

    public long insertFetchedRides(String deptTime, String arrivalTime, String deptLoc, String arrivalLoc, int timeLapsed, double score, int driverId, int companyID, int countBad, int highRideTime, int highRideDist, int suddenAccelerate, int suddenBraking, String createdAt, int performance_count, String time_zone, int orphanRide) {
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues contentValues = new ContentValues();
        /*contentValues.put(COL_ID, remote_id);*/
        contentValues.put(COL_DEPARTURE_TIME, deptTime);
        contentValues.put(COL_ARRIVAL_TIME, arrivalTime);

        contentValues.put(COL_DEPARTURE_LOCATION, deptLoc);
        contentValues.put(COL_ARRIVAL_LOCATION, arrivalLoc);
        contentValues.put(COL_TIME_ELAPSED, timeLapsed);
        contentValues.put(COL_SCORE, score);
        contentValues.put(COL_DRIVER_ID, driverId);
        contentValues.put(COL_COMPANY_ID, companyID);
        contentValues.put(COL_SEND, 1);
        contentValues.put(COL_TIME, createdAt);
        contentValues.put(COL_AVG_TIME, 0);
        contentValues.put(COL_TIME_TOTAL_SECONDES, 0);

        contentValues.put(COL_BAD_BEHAVIOUR, countBad);
        /*Log.e("getBadBehaviour",""+ride.getBad_behaviour());*/
        contentValues.put(COL_TIME_ABOVE_120, highRideTime);
        contentValues.put(COL_DISTANCE_TRAVELLED, highRideDist);
        contentValues.put(COL_SUDDEN_BRAKING, suddenBraking);
        if (suddenAccelerate >= 0) {
            contentValues.put(COL_SUDDEN_ACCELERATION, suddenAccelerate);
        } else {
            contentValues.put(COL_SUDDEN_ACCELERATION, 0);
        }

        if (suddenBraking >= 0) {
            contentValues.put(COL_SUDDEN_BRAKING, suddenBraking);
        } else {
            contentValues.put(COL_SUDDEN_BRAKING, 0);
        }

        if (performance_count >= 0) {
            contentValues.put(COL_SUDDEN_SPEEDING, performance_count);
        } else {
            contentValues.put(COL_SUDDEN_SPEEDING, 0);
        }
        contentValues.put(COL_ORPHAN_RIDE, orphanRide);
        contentValues.put(COL_TIME_ZONE, time_zone);
        return bdd.insert(TABLE_RIDES, null, contentValues);
    }


    public void deleteMessageInDbWithID(int ID) {

        bdd.execSQL("DELETE FROM " + TABLE_MESSAGES + " where ID =" + ID);
        bdd.close();
    }


    public void deleteMessageInDb() {
        int SEND = 1;
        String nameColumn = "SEND";
        //Cursor cursor1 = bdd.delete(TABLE_RIDES,nameColumn+ "= 1",new String[]{"1"});
        bdd.execSQL("DELETE FROM " + TABLE_MESSAGES + " where ID > -1");
        bdd.close();
    }

    public int sumMessageCount() {
        int sumDb = 0;
        Cursor cursor = bdd.rawQuery("SELECT COUNT(*) FROM " + TABLE_MESSAGES, null);
        if (cursor != null && cursor.getCount() >= 0) {
            if (cursor.moveToFirst()) {

                sumDb = Integer.parseInt(cursor.getString(0));
            }
        }
        return sumDb;
    }

    public int sumDb() {
        int sumDb = 0;
        Cursor cursor = bdd.rawQuery("SELECT SUM(COUNT_BAD_BEHAVIOUR) FROM " + TABLE_RIDES, null);
        if (cursor != null && cursor.getCount() >= 0) {
            if (cursor.moveToFirst()) {

                sumDb = Integer.parseInt(cursor.getString(0));
            }
        }
        return sumDb;
    }

    public int sumBadBehaviorDb() {
        int sumDb = 0;
        Cursor cursor = bdd.rawQuery("SELECT SUM(COL_OVER_SPEEDING) FROM " + TABLE_RIDES, null);
        if (cursor != null && cursor.getCount() >= 0) {
            if (cursor.moveToFirst()) {

                sumDb = Integer.parseInt(cursor.getString(0));
            }
        }
        return sumDb;
    }

    // Get the last Ride ID
    public long getLastRideDeptTime() {
        long deptTime = 0l;
        Cursor cursor = bdd.rawQuery("Select  DEPARTURE_TIME From table_rides  order by id desc LIMIT 1", null);
        if (cursor != null && cursor.getCount() >= 0) {
            if (cursor.moveToNext()) {
                do {
                    deptTime = Long.parseLong(cursor.getString(0));

                } while (cursor.moveToNext());


            }

        }

        return deptTime;
    }


    // Get the last Ride ID
    public long getLastRideArrivalTime() {
        long arrivalTime = 0l;
        Cursor cursor = bdd.rawQuery("Select  ARRIVAL_TIME From table_rides  order by id desc LIMIT 1", null);
        if (cursor != null && cursor.getCount() >= 0) {
            if (cursor.moveToNext()) {
                do {
                    arrivalTime = Long.parseLong(cursor.getString(0));

                } while (cursor.moveToNext());


            }

        }

        return arrivalTime;
    }

    // Get the last Ride ID
    public int getLastRideIsPerformanceUpdatedStatus() {
        int status = -3;
        Cursor cursor = bdd.rawQuery("Select  COL_IS_OVER_SPEEDING_UPDATED From table_rides  order by id desc LIMIT 1", null);
        if (cursor != null && cursor.getCount() >= 0) {
            if (cursor.moveToNext()) {
                do {
                    status = (cursor.getInt(0));

                } while (cursor.moveToNext());


            }

        }

        return status;
    }

    // Get the last Ride ID
    public int getLastRideBadBehaviourCount() {
        int id = 0;
        int sum = 0;
        int sp, br, acc = 0;
        Cursor cursor = bdd.rawQuery("Select   COL_OVER_SPEEDING , COL_SUDDEN_BRAKING , COL_SUDDEN_ACCELERATION  From table_rides  order by id desc LIMIT 1", null);
        if (cursor != null && cursor.getCount() >= 0) {
            if (cursor.moveToNext()) {
                do {
                    sp = Integer.parseInt(cursor.getString(0));
                    br = Integer.parseInt(cursor.getString(1));
                    acc = Integer.parseInt(cursor.getString(2));
                    Log.e("sp", "" + sp);
                    Log.e("br", "" + br);
                    Log.e("acc", "" + acc);
                    sum += sp + br + acc;
                    Log.e("sum", "" + sum);
                } while (cursor.moveToNext());


            }

        }

        return sum;
    }

    // Get the last Ride ID
    public int getRideID() {
        int id = 0;
        Cursor cursor = bdd.rawQuery("SELECT ID FROM table_rides ORDER BY ID DESC LIMIT 1", null);
        if (cursor != null && cursor.getCount() >= 0) {
            if (cursor.moveToFirst()) {

                id = Integer.parseInt(cursor.getString(0));
            }
        }
        return id;
    }
       /*
    * Deletes all rides in DB which are synced already and who's status is 1;
    * nofal
    * */

    public void deleteDataInDb() {
        int SEND = 1;
        String nameColumn = "SEND";
        //Cursor cursor1 = bdd.delete(TABLE_RIDES,nameColumn+ "= 1",new String[]{"1"});
        bdd.execSQL("DELETE FROM " + TABLE_RIDES + " WHERE " + nameColumn + " = " + SEND);
        bdd.close();
    }

    public int totaltime() {
        int totaltime = 0;
        Cursor cursor = bdd.rawQuery("SELECT SUM(TIME_ELAPSED) FROM " + TABLE_RIDES, null);
        if (cursor != null && cursor.getCount() >= 0) {
            if (cursor.moveToFirst()) {

                totaltime = Integer.parseInt(cursor.getString(0));
            }
        }
        Log.e("totaltime", "" + totaltime);
        return totaltime;

    }

    /**
     * Allows to get a list of all rides in JSONArray
     *
     * @return(out), @JSONArray a list of ride json objects
     */
    public JSONArray getAllRides(Boolean canTransformLocation) {
        JSONArray listRides = null;
        Cursor cursor = bdd.query(TABLE_RIDES, new String[]{COL_ID, COL_DEPARTURE_TIME, COL_ARRIVAL_TIME, COL_DEPARTURE_LOCATION, COL_ARRIVAL_LOCATION, COL_TIME_ELAPSED, COL_SCORE, COL_DRIVER_ID, COL_COMPANY_ID, COL_SEND, COL_TIME, COL_AVG_TIME, COL_TIME_TOTAL_SECONDES, COL_BAD_BEHAVIOUR, COL_TIME_ABOVE_120, COL_CAUSE_OF_ENDING_RIDE, COL_DISTANCE_TRAVELLED, COL_SUDDEN_ACCELERATION, COL_SUDDEN_BRAKING}, null, null, null, null, null);
        //  Cursor cursor = bdd.rawQuery("SELECT "+COL_ID+ COL_DEPARTURE_TIME+ COL_ARRIVAL_TIME+COL_DEPARTURE_LOCATION+COL_ARRIVAL_LOCATION+COL_TIME_ELAPSED+COL_SCORE+COL_DRIVER_ID+COL_COMPANY_ID+COL_SEND+COL_TIME+COL_AVG_TIME+COL_TIME_TOTAL_SECONDES+COL_BAD_BEHAVIOUR+COL_TIME_ABOVE_120+COL_DISTANCE_TRAVELLED +"FROM " +TABLE_RIDES +" WHERE "+COL_SEND+ " =" +0,null );
        int countbad = 0;
        if (cursor.moveToFirst()) {
            listRides = new JSONArray();
            do {
                //getting the ride :
                RidesBeans ride = new RidesBeans();
                ride.setID(Integer.parseInt(cursor.getString(NUM_COL_ID)));
                ride.setDeparture_time(Long.parseLong(cursor.getString(NUM_COL_DEPARTURE_TIME)));
                ride.setArrival_time(Long.parseLong(cursor.getString(NUM_COL_ARRIVAL_TIME)));
                ride.setDeparture_location(cursor.getString(NUM_COL_DEPARTURE_LOCATION));
                ride.setArrival_location(cursor.getString(NUM_COL_ARRIVAL_LOCATION));
                ride.setTime_elapsed(Double.parseDouble(cursor.getString(NUM_COL_TIME_ELAPSED)));
                ride.setScore(Double.parseDouble(cursor.getString(NUM_COL_SCORE)));
                ride.setDriver_id(Integer.parseInt(cursor.getString(NUM_COL_DRIVER_ID)));
                ride.setCompany_id(Integer.parseInt(cursor.getString(NUM_COL_OMPANY_ID)));
                int valSend = Integer.parseInt(cursor.getString(NUM_COL_SEND));
                Boolean isSend = (valSend == 1) ? true : false;
                ride.setSend(isSend);
                ride.setTime(cursor.getString(NUM_COL_TIME));
                ride.setAverageP(cursor.getDouble(NUM_COL_AVG_TIME));
                ride.setTimeSecondes(cursor.getInt(NUM_COL_TIME_TOTAL_SECONDES));
                ride.setBad_behaviour(Integer.parseInt(cursor.getString(NUM_COL_BAD_BEHAVIOUR)));
                ride.setTotalTimeAbove120(Long.parseLong(cursor.getString(NUM_COL_TIME_ABOVE_120)));
                ride.setCauseofendingRide(Integer.parseInt(cursor.getString(NUM_COL_CAUSE_OF_ENDING_RIDE)));
                ride.setSudden_accelaration(Integer.parseInt(cursor.getString(NUM_COL_SUDDEN_ACCELERATION)));
                ride.setSudden_braking(Integer.parseInt(cursor.getString(NUM_COL_SUDDEN_BRAKING)));
                ride.setTotalDistance(Integer.parseInt(cursor.getString(NUM_COL_DISTANCE_TRAVELLED)));
                /*int db = Integer.parseInt(cursor.getString(NUM_COL_BAD_BEHAVIOUR));*/
              /*  Log.e("Bad_Behaviour :",""+db);
                long totatimeover120 = cursor.getLong(NUM_COL_TIME_ABOVE_120);
                Log.e("totatimeover120",""+totatimeover120);*/

                //create JSON OBJECT of Ride :
                JSONObject obj = new JSONObject();
                try {
                    obj.put("remote_id", ride.getID());
                    obj.put("departure_time", ride.getDeparture_time());
                    obj.put("arrival_time", (ride.getArrival_time()));
                    obj.put("time_elapsed", ride.getTime_elapsed());
                    obj.put("departure_location", ride.getDeparture_location());
                    obj.put("arrival_location", ride.getArrival_location());
                    obj.put("score", ride.getScore());
                    obj.put("driver_id", ride.getDriver_id());
                    obj.put("company_id", ride.getCompany_id());
                    obj.put("send", ride.getSend());
                    obj.put("time", ride.getTime());
                    obj.put("avg_time", ride.getAverageP());
                    obj.put("time_total_secondes", ride.getTimeSecondes());
                    obj.put("count_bad_behaviour", ride.getBad_behaviour());
                    obj.put("high_ride_time", ride.getTotalTimeAbove120());
                    obj.put("reason_end_ride", ride.getCauseofendingRide());
                    obj.put("high_ride_distance", ride.getTotalDistance());
                    obj.put("sudden_accelerate", ride.getSudden_accelaration());
                    obj.put("sudden_brake", ride.getSudden_braking());
                    //if for sync, departure and arrival are not empty and not already save
                    if (canTransformLocation && !ride.getDeparture_location().equals("") && !ride.getArrival_location().equals("") && !ride.getSend()) {
                        listRides.put(obj);

                    } else {
                        //display on menu activity :
                        listRides.put(obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.i("Adneom", "error " + e.getMessage());
                }
            } while (cursor.moveToNext());

            // if for a synchronization :
            if (canTransformLocation) {
                //loop to get address because an asynctask is used :
                if (listRides != null && listRides.length() > 0) {
                    sizeCurrentListe = (listRides.length() * 2);
                    currentIndex = 0;
                    for (int i = 0; i < listRides.length(); i++) {
                        try {
                            JSONObject objRide = listRides.getJSONObject(i);
                            if (objRide != null) {
                                if (objRide.has("departure_location") && objRide.has("arrival_location")) {
                                    this.jObject = objRide;
                                    transfromLocationToAddress(objRide.getString("departure_location"), this.jObject, 1);
                                    transfromLocationToAddress(objRide.getString("arrival_location"), this.jObject, 2);
                                    //Log.i("Adneom","gere");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.i("Adneom", "(RideBDD) error is " + e.getMessage());
                        }
                    }
                    if (listeWithLocality != null && listeWithLocality.length() > 0) {
                        askSynchonizationToActivity();
                    }
                }
            }
        } else {
            //if list is empty :
            if (canTransformLocation) {
                askSynchonizationToActivity();
            }
        }
        return listRides;
    }

    public ArrayList<Integer> getAlltheIDsOftheDeletedMessages() {
        ArrayList<Integer> integerArrayList = new ArrayList<>();
        Cursor cursor = bdd.rawQuery("Select REMOTE_MESSAGES_ID FROM table_messages where IS_DELETED = -2", null);

        if (cursor.moveToFirst()) {


            do {
                integerArrayList.add(cursor.getInt(0));

            } while (cursor.moveToNext());


        }
        return integerArrayList;
    }

    public ArrayList<NotificationBeans> getAllMessagesFromDB() {
        ArrayList<NotificationBeans> listSubRides = new ArrayList<NotificationBeans>();

        Cursor cursor = bdd.query(TABLE_MESSAGES, new String[]{COL_REMOTE_MESSAGES_ID, COL_MESSAGES_TITLE, COL_MESSAGES_BODY, COL_MESSAGES_DATE_TIME}, "IS_DELETED = 0", null, null, null, null);

        if (cursor.moveToFirst()) {


            do {
                NotificationBeans notificationBeans = new NotificationBeans();
                notificationBeans.setMessagesID(cursor.getInt(0));
                notificationBeans.setTitle(cursor.getString(1));
                notificationBeans.setBody(cursor.getString(2));
                notificationBeans.setDateTime(cursor.getString(3));
                listSubRides.add(notificationBeans);
            } while (cursor.moveToNext());

        }

        return listSubRides;
    }

    /**
     * Allows to get a list of all rides in JSONArray
     *
     * @return(out), @JSONArray a list of ride json objects
     */
    public ArrayList<RidesBeans> getAllRidesForPhoneSafety() {
        ArrayList<RidesBeans> listRides = new ArrayList<>();

        int n = 1;
        //   Cursor cursor = bdd.query(TABLE_RIDES, new String[]{COL_ID, COL_DEPARTURE_TIME, COL_ARRIVAL_TIME,COL_DEPARTURE_LOCATION,COL_ARRIVAL_LOCATION,COL_TIME_ELAPSED,COL_SCORE,COL_DRIVER_ID,COL_COMPANY_ID,COL_SEND,COL_TIME,COL_AVG_TIME,COL_TIME_TOTAL_SECONDES,COL_BAD_BEHAVIOUR,COL_TIME_ABOVE_120,COL_DISTANCE_TRAVELLED},null,null,null,null,null);
        // Cursor cursor = bdd.query("SELECT "+COL_ID+ COL_DEPARTURE_TIME+ COL_ARRIVAL_TIME+COL_DEPARTURE_LOCATION+COL_ARRIVAL_LOCATION+COL_TIME_ELAPSED+COL_SCORE+COL_DRIVER_ID+COL_COMPANY_ID+COL_SEND+COL_TIME+COL_AVG_TIME+COL_TIME_TOTAL_SECONDES+COL_BAD_BEHAVIOUR+COL_TIME_ABOVE_120+COL_DISTANCE_TRAVELLED +"FROM " +TABLE_RIDES +" WHERE "+COL_SEND+ " =" +n,null,null,null,null );
        Cursor cursor = bdd.query(TABLE_RIDES, new String[]{COL_ID, COL_DEPARTURE_TIME, COL_ARRIVAL_TIME, COL_DEPARTURE_LOCATION, COL_ARRIVAL_LOCATION, COL_TIME_ELAPSED, COL_SCORE, COL_DRIVER_ID, COL_COMPANY_ID, COL_SEND, COL_TIME, COL_AVG_TIME, COL_TIME_TOTAL_SECONDES, COL_BAD_BEHAVIOUR, COL_TIME_ABOVE_120, COL_CAUSE_OF_ENDING_RIDE, COL_SUDDEN_ACCELERATION, COL_SUDDEN_BRAKING, COL_SUDDEN_SPEEDING, COL_DISTANCE_TRAVELLED}, null, null, null, null, "ID DESC LIMIT 100");

        int countbad = 0;
        if (cursor.moveToFirst()) {
            do {
                //getting the ride :
                RidesBeans ride = new RidesBeans();
                //   ride.setID(Integer.parseInt(cursor.getString(NUM_COL_ID)));
                ride.setDeparture_time(Long.parseLong(cursor.getString(NUM_COL_DEPARTURE_TIME)));
                ride.setArrival_time(Long.parseLong(cursor.getString(NUM_COL_ARRIVAL_TIME)));
                // ride.setDeparture_location(cursor.getString(NUM_COL_DEPARTURE_LOCATION));
                // ride.setArrival_location(cursor.getString(NUM_COL_ARRIVAL_LOCATION));
               /* ride.setTime_elapsed(Double.parseDouble(cursor.getString(NUM_COL_TIME_ELAPSED)));
                ride.setScore(Double.parseDouble(cursor.getString(NUM_COL_SCORE)));
                ride.setDriver_id(Integer.parseInt(cursor.getString(NUM_COL_DRIVER_ID)));
                ride.setCompany_id(Integer.parseInt(cursor.getString(NUM_COL_OMPANY_ID)));
                int valSend = Integer.parseInt(cursor.getString(NUM_COL_SEND));
                Boolean isSend = (valSend == 1 ) ? true : false;
                ride.setSend(isSend);
                ride.setTime(cursor.getString(NUM_COL_TIME));
                ride.setAverageP(cursor.getDouble(NUM_COL_AVG_TIME));
                ride.setTimeSecondes(cursor.getInt(NUM_COL_TIME_TOTAL_SECONDES));*/
                ride.setBad_behaviour(Integer.parseInt(cursor.getString(NUM_COL_BAD_BEHAVIOUR)));
                ride.setSudden_accelaration(Integer.parseInt(cursor.getString(NUM_COL_SUDDEN_ACCELERATION)));
                ride.setSudden_braking(Integer.parseInt(cursor.getString(NUM_COL_SUDDEN_BRAKING)));
                ride.setOver_speeding_count(Integer.parseInt(cursor.getString(18)));
                /*ride.setTotalTimeAbove120(Long.parseLong(cursor.getString(NUM_COL_TIME_ABOVE_120)));
                ride.setCauseofendingRide(Integer.parseInt(cursor.getString(NUM_COL_CAUSE_OF_ENDING_RIDE)));
                ride.setTotalDistance(Integer.parseInt(cursor.getString(NUM_COL_DISTANCE_TRAVELLED)));
                int db = Integer.parseInt(cursor.getString(NUM_COL_BAD_BEHAVIOUR));*/
              /*  Log.e("Bad_Behaviour :",""+db);
                long totatimeover120 = cursor.getLong(NUM_COL_TIME_ABOVE_120);
                Log.e("totatimeover120",""+totatimeover120);*/

                //create JSON OBJECT of Ride :
              /*  JSONObject obj = new JSONObject();
                try {
                    obj.put("remote_id",ride.getID());
                    obj.put("departure_time",ride.getDeparture_time());
                    obj.put("arrival_time",(ride.getArrival_time()));
                    obj.put("time_elapsed",ride.getTime_elapsed());
                    obj.put("departure_location",ride.getDeparture_location());
                    obj.put("arrival_location",ride.getArrival_location());
                    obj.put("score",ride.getScore());
                    obj.put("driver_id",ride.getDriver_id());
                    obj.put("company_id",ride.getCompany_id());
                    obj.put("send",ride.getSend());
                    obj.put("time",ride.getTime());
                    obj.put("avg_time",ride.getAverageP());
                    obj.put("time_total_secondes",ride.getTimeSecondes());
                    obj.put("count_bad_behaviour",ride.getBad_behaviour());
                    obj.put("high_ride_time", ride.getTotalTimeAbove120());
                    obj.put("reason_end_ride",ride.getCauseofendingRide());
                    obj.put("high_ride_distance", ride.getTotalDistance());
                    //if for sync, departure and arrival are not empty and not already save
                    if(!ride.getDeparture_location().equals("") && !ride.getArrival_location().equals("") && !ride.getSend()){
                        listRides.add(ride);
                    }else{
                        //display on menu activity :
                        listRides.add(ride);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.i("Adneom", "error " + e.getMessage());
                }*/
                listRides.add(ride);
            } while (cursor.moveToNext());


        }
        return listRides;
    }

    public JSONArray getAllSubRidesForSyncing() {
        JSONArray listSubRides = null;
        JSONObject emptyObject = new JSONObject();

        Cursor cursor = bdd.query(TABLE_SUB_RIDES, new String[]{"ID_SUB_RIDE", "ID_MAIN_RIDE", "AREA_TYPE", "START_LAT_LNG", "END_LAT_LNG", "TIME_REGION_ENTERED", "CURRENT_SPEED", "TIME_LAPSED_INSIDE_A_REGION", "IS_SENT"}, "IS_SENT = 0", null, null, null, null);

        if (cursor.moveToFirst()) {
            listSubRides = new JSONArray();

            do {
                JSONObject obj = new JSONObject();
                SubRideBeans subRideBeans = new SubRideBeans();
                subRideBeans.setCurrentTime(cursor.getLong(5));

                try {
                    obj.put("id_sub_ride", cursor.getString(0));
                    obj.put("id_main_ride", cursor.getString(1));
                    obj.put("area_type", cursor.getString(2));
                    obj.put("start_lat_lng", cursor.getString(3));
                    obj.put("end_lat_lng", cursor.getString(4));
                    obj.put("time_region_entered", subRideBeans.getCurrentTime());
                    obj.put("current_speed", cursor.getString(6));
                    obj.put("time_inside_a_region", cursor.getString(7));
                    obj.put("is_sent", cursor.getString(8));

                    listSubRides.put(obj);

                } catch (JSONException w) {

                }
            } while (cursor.moveToNext());

        }
        if (listSubRides == null) {
            listSubRides = new JSONArray();

     /*       String empty = "No sub rides for this Ride";
            try {
                emptyObject.put("");
            } catch (JSONException e) {
                e.printStackTrace();
            }*/

        }
        return listSubRides;
    }


    /**
     * Allows to get a list of all rides in JSONArray
     *
     * @return(out), @JSONArray a list of ride json objects
     */
    public JSONArray getAllRidesForSyncing() {
        JSONArray listRides = null;
        int n = 1;
        //   Cursor cursor = bdd.query(TABLE_RIDES, new String[]{COL_ID, COL_DEPARTURE_TIME, COL_ARRIVAL_TIME,COL_DEPARTURE_LOCATION,COL_ARRIVAL_LOCATION,COL_TIME_ELAPSED,COL_SCORE,COL_DRIVER_ID,COL_COMPANY_ID,COL_SEND,COL_TIME,COL_AVG_TIME,COL_TIME_TOTAL_SECONDES,COL_BAD_BEHAVIOUR,COL_TIME_ABOVE_120,COL_DISTANCE_TRAVELLED},null,null,null,null,null);
        // Cursor cursor = bdd.query("SELECT "+COL_ID+ COL_DEPARTURE_TIME+ COL_ARRIVAL_TIME+COL_DEPARTURE_LOCATION+COL_ARRIVAL_LOCATION+COL_TIME_ELAPSED+COL_SCORE+COL_DRIVER_ID+COL_COMPANY_ID+COL_SEND+COL_TIME+COL_AVG_TIME+COL_TIME_TOTAL_SECONDES+COL_BAD_BEHAVIOUR+COL_TIME_ABOVE_120+COL_DISTANCE_TRAVELLED +"FROM " +TABLE_RIDES +" WHERE "+COL_SEND+ " =" +n,null,null,null,null );
        Cursor cursor = bdd.query(TABLE_RIDES, new String[]{COL_ID, COL_DEPARTURE_TIME, COL_ARRIVAL_TIME, COL_DEPARTURE_LOCATION, COL_ARRIVAL_LOCATION, COL_TIME_ELAPSED, COL_SCORE, COL_DRIVER_ID, COL_COMPANY_ID, COL_SEND, COL_TIME, COL_AVG_TIME, COL_TIME_TOTAL_SECONDES, COL_BAD_BEHAVIOUR, COL_TIME_ABOVE_120, COL_CAUSE_OF_ENDING_RIDE, COL_SUDDEN_ACCELERATION, COL_SUDDEN_BRAKING, COL_DISTANCE_TRAVELLED, COL_TIME_ZONE, COL_ORPHAN_RIDE}, "SEND = 0", null, null, null, null);
        // Cursor cursor = bdd.query(TABLE_RIDES, new String[]{COL_ID, COL_DEPARTURE_TIME, COL_ARRIVAL_TIME,COL_DEPARTURE_LOCATION,COL_ARRIVAL_LOCATION,COL_TIME_ELAPSED,COL_SCORE,COL_DRIVER_ID,COL_COMPANY_ID,COL_SEND,COL_TIME,COL_AVG_TIME,COL_TIME_TOTAL_SECONDES,COL_BAD_BEHAVIOUR,COL_TIME_ABOVE_120,COL_CAUSE_OF_ENDING_RIDE,COL_DISTANCE_TRAVELLED},"SEND = 0",null,null,null,null,null);
        //  Cursor cursor = bdd.rawQuery("SELECT * FROM table_rides LEFT  JOIN table_sub_rides ON table_rides.ID = table_sub_rides.ID_MAIN_RIDE ",null);

        int countbad = 0;
        if (cursor.moveToFirst()) {
            listRides = new JSONArray();
            do {
                //getting the ride :
                RidesBeans ride = new RidesBeans();
                ride.setID(Integer.parseInt(cursor.getString(NUM_COL_ID)));
                ride.setDeparture_time(Long.parseLong(cursor.getString(NUM_COL_DEPARTURE_TIME)));
                ride.setArrival_time(Long.parseLong(cursor.getString(NUM_COL_ARRIVAL_TIME)));
                ride.setDeparture_location(cursor.getString(NUM_COL_DEPARTURE_LOCATION));
                ride.setArrival_location(cursor.getString(NUM_COL_ARRIVAL_LOCATION));
                ride.setTime_elapsed(Double.parseDouble(cursor.getString(NUM_COL_TIME_ELAPSED)));
                ride.setScore(Double.parseDouble(cursor.getString(NUM_COL_SCORE)));
                ride.setDriver_id(Integer.parseInt(cursor.getString(NUM_COL_DRIVER_ID)));
                ride.setCompany_id(Integer.parseInt(cursor.getString(NUM_COL_OMPANY_ID)));
                int valSend = Integer.parseInt(cursor.getString(NUM_COL_SEND));
                Boolean isSend = (valSend == 1) ? true : false;
                ride.setSend(isSend);
                ride.setTime(cursor.getString(NUM_COL_TIME));
                ride.setAverageP(cursor.getDouble(NUM_COL_AVG_TIME));
                ride.setTimeSecondes(cursor.getInt(NUM_COL_TIME_TOTAL_SECONDES));
                ride.setBad_behaviour(Integer.parseInt(cursor.getString(NUM_COL_BAD_BEHAVIOUR)));
                ride.setTotalTimeAbove120(Long.parseLong(cursor.getString(NUM_COL_TIME_ABOVE_120)));
                ride.setCauseofendingRide(Integer.parseInt(cursor.getString(NUM_COL_CAUSE_OF_ENDING_RIDE)));
                ride.setSudden_accelaration(Integer.parseInt(cursor.getString(NUM_COL_SUDDEN_ACCELERATION)));
                ride.setSudden_braking(Integer.parseInt(cursor.getString(NUM_COL_SUDDEN_BRAKING)));
                ride.setTotalDistance(Integer.parseInt(cursor.getString(NUM_COL_DISTANCE_TRAVELLED)));
                int db = Integer.parseInt(cursor.getString(NUM_COL_BAD_BEHAVIOUR));
                ride.setOrphandrideBit(Integer.parseInt(cursor.getString(NUM_COL_ORPHAN_RIDE)));
                ride.setTime_zone(cursor.getString(NUM_COL_TIME_ZONE));

              /*  Log.e("Bad_Behaviour :",""+db);

                /*  Log.e("Bad_Behaviour :",""+db);
                long totatimeover120 = cursor.getLong(NUM_COL_TIME_ABOVE_120);
                Log.e("totatimeover120",""+totatimeover120);*/

                //create JSON OBJECT of Ride :
                JSONObject obj = new JSONObject();

                try {
                    obj.put("remote_id", ride.getID());
                    obj.put("departure_time", ride.getDeparture_time());
                    obj.put("arrival_time", (ride.getArrival_time()));
                    obj.put("time_elapsed", ride.getTime_elapsed());
                    obj.put("departure_location", ride.getDeparture_location());
                    obj.put("arrival_location", ride.getArrival_location());
                    obj.put("score", ride.getScore());
                    obj.put("driver_id", ride.getDriver_id());
                    obj.put("company_id", ride.getCompany_id());
                    obj.put("send", ride.getSend());
                    obj.put("time", ride.getTime());
                    obj.put("avg_time", ride.getAverageP());
                    obj.put("time_total_secondes", ride.getTimeSecondes());
                    obj.put("count_bad_behaviour", ride.getBad_behaviour());
                    obj.put("high_ride_time", ride.getTotalTimeAbove120());
                    obj.put("reason_end_ride", ride.getCauseofendingRide());
                    obj.put("sudden_accelerate", ride.getSudden_accelaration());
                    obj.put("sudden_brake", ride.getSudden_braking());
                    obj.put("high_ride_distance", ride.getTotalDistance());
                    obj.put("orphan_ride", ride.getOrphandrideBit());
                    obj.put("time_zone", ride.getTime_zone());


           /*         obj.put("ID_MAIN_RIDE",cursor.getString(18));
                    obj.put("AREA_TYPE",cursor.getString(19));
                    obj.put("START_LAT_LNG",cursor.getString(20));
                    obj.put("END_LAT_LNG",cursor.getString(21));
                    obj.put("CURRENT_TIME",cursor.getString(22));
                    obj.put("CURRENT_SPEED",cursor.getString(23));
                    obj .put("IS_SENT",cursor.getString(24));*/

//
                    //if for sync, departure and arrival are not empty and not already save
                    if (!ride.getDeparture_location().equals("") && !ride.getArrival_location().equals("") && !ride.getSend()) {
                        listRides.put(obj);

                    } else {
                        //display on menu activity :
                        listRides.put(obj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.i("Adneom", "error " + e.getMessage());
                }
 /*               if (cursor.isLast()){

                    try {
                        JSONObject obj1 =new JSONObject();
                        obj1.put("subData",getAllSubRidesForSyncing().toString());
                        listRides.put(obj1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }*/
            } while (cursor.moveToNext());


        }
        return listRides;
    }

    /**
     * Get address from a string
     *
     * @param cordinates, @String represents a location in string : latitude,longitude
     * @return the locality
     */
    private String transfromLocationToAddress(String cordinates, JSONObject ride, int location) {
        //test connectivity :
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        String locality = cordinates;

        if (!cordinates.equals("") && cordinates.contains(",") && ride != null) {
            //getting latitude and logitude
            String[] tabCordinates = cordinates.split(",");
            AsyncTaskGeocoder task = new AsyncTaskGeocoder(tabCordinates[0], tabCordinates[1], context, ride, location, this);
            task.execute();
        } else {
            listeWithLocality.put(ride);
        }

        return locality;
    }

    public void askSynchonizationToActivity() {
        Intent intent = new Intent("com.freeedrive_saving_driving.synchronization");

        //1 : have to ask
        //Log.i("Adneom","liste end is : "+listeWithLocality);
        intent.putExtra("sync", "OK");
        intent.putExtra("liste", listeWithLocality.toString());
        context.sendBroadcast(intent);
    }


    public int updateRidePerformance(int remote_id, int speeding_count) {
        //new value of column send :
        ContentValues values = new ContentValues();
        values.put(COL_SUDDEN_SPEEDING, speeding_count);
        values.put(COL_IS_SPEEDING_UPDATED, 1);
        int entry = -1;

        if (bdd != null) {
            entry = bdd.update(TABLE_RIDES, values, COL_ID + " = " + remote_id, null);
        }
        return entry;
    }

    /**
     * Allows to update a ride, the send value
     *
     * @param id(in),       @Integer represents the ID
     * @param newValue(in), @Integer represents the new value
     * @return(out), @Integer represents the ID
     */

    public int updateRide(int id, int newValue) {
        //new value of column send :
        ContentValues values = new ContentValues();
        values.put(COL_SEND, 1);
        int entry = -1;

        if (bdd != null) {
            entry = bdd.update(TABLE_RIDES, values, COL_SEND + " = 0", null);
        }
        return entry;
    }


    public void updateMessageIsDelete(int ID) {
        //new value of column send :

        ContentValues values = new ContentValues();
        values.put("IS_DELETED", -2);
        int entry = -1;

        if (bdd != null) {
      /*      bdd.rawQuery("UPDATE table_messages SET "+COL_MESSAGES_IS_DELETED+" = -2" +" WHERE "+COL_REMOTE_MESSAGES_ID +" = "+ID,null);
            bdd.update("table_messages",values,COL_REMOTE_MESSAGES_ID +" = "+ID,null);*/
            /*bdd.update(TABLE_MESSAGES, values, COL_REMOTE_MESSAGES_ID +" = "+ID, null);*/
            String query = "UPDATE table_messages SET IS_DELETED = -2 WHERE REMOTE_MESSAGES_ID = " + ID;
            bdd.execSQL(query);
        }

    }

    /**
     * Allows to update a ride, the send value
     *
     * @param id(in),       @Integer represents the ID
     * @param newValue(in), @Integer represents the new value
     * @return(out), @Integer represents the ID
     */
    public int updateSubRide(int id, int newValue) {
        //new value of column send :

        ContentValues values = new ContentValues();
        values.put("IS_SENT", 1);
        int entry = -1;

        if (bdd != null) {
            entry = bdd.update(TABLE_SUB_RIDES, values, "IS_SENT = 0", null);
        }
        bdd.close();
        return entry;
    }

    /**
     * Allows to update a ride
     *
     * @param id(in),   @Long represents the ID
     * @param ride(in), @Ride represents the Object
     * @return(out), @Integer represenst the ID
     */
    public int updateRide(long id, RidesBeans ride) {
        //new value of column send :
        ContentValues values = new ContentValues();
        values.put(COL_SCORE, ride.getScore());
        values.put(COL_ARRIVAL_LOCATION, ride.getArrival_location());
        values.put(COL_ARRIVAL_TIME, ride.getArrival_time());
        //Log.i("Adneom","(RideBDD) new time : "+ride.getTime());
        values.put(COL_TIME, ride.getTime());
        values.put(COL_AVG_TIME, ride.getAverageP());
        values.put(COL_TIME_TOTAL_SECONDES, ride.getTimeSecondes());
        Log.e("InsideUpdateRide", "" + ride.getBad_behaviour());
        values.put(COL_BAD_BEHAVIOUR, ride.getBad_behaviour());
        return bdd.update(TABLE_RIDES, values, COL_ID + " = " + id, null);
    }

    /**
     * Delete a ride from ID
     *
     * @param id(int), @Integer represents hte ID
     * @return(out), @Integer represents the ID
     */
    public int removeRideWithID(int id) {
        //Suppression d'un livre de la BDD grâce à l'ID
        return bdd.delete(TABLE_RIDES, COL_ID + " = " + id, null);
    }

    /**
     * Allows to get a ride
     *
     * @param idRide(in), @Integer represents the ID
     * @return(out), @Ride represnts the Object
     */
    public RidesBeans getRideFromID(int idRide) {
        //Récupère dans un Cursor les valeurs correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)

        Cursor c = bdd.query(TABLE_RIDES, new String[]{COL_ID, COL_DEPARTURE_TIME, COL_ARRIVAL_TIME, COL_DEPARTURE_LOCATION, COL_ARRIVAL_LOCATION, COL_TIME_ELAPSED, COL_SCORE, COL_DRIVER_ID, COL_COMPANY_ID, COL_SEND, COL_TIME, COL_AVG_TIME, COL_TIME_TOTAL_SECONDES}, COL_ID + " = " + idRide, null, null, null, null);
        return cursorToRide(c);
    }

    /**
     * Allows to get a ride
     *
     * @param idRide (in), @Integer represents the ID
     * @return(out), @Ride represnts the Object
     */
    public RidesBeans getRideWithId(long idRide) {
        //Récupère dans un Cursor les valeurs correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)

        Cursor c = bdd.query(TABLE_RIDES, new String[]{COL_ID, COL_DEPARTURE_TIME, COL_ARRIVAL_TIME, COL_DEPARTURE_LOCATION, COL_ARRIVAL_LOCATION, COL_TIME_ELAPSED, COL_SCORE, COL_DRIVER_ID, COL_COMPANY_ID, COL_SEND, COL_TIME, COL_AVG_TIME, COL_TIME_TOTAL_SECONDES}, COL_ID + " = " + idRide, null, null, null, null);
        return cursorToRide(c);
    }

    /**
     * Allows to get the number of rows in table
     *
     * @return(out), @Long number of rows
     */
    public long getEntries() {
        long numRows = DatabaseUtils.queryNumEntries(bdd, TABLE_RIDES);
        return numRows;
    }

    /**
     * Is called byt the getter methods to return the object Ride
     *
     * @param c(in), @Activity represents the current cursor to get the values
     * @return(out), @Ride represents the object
     */
    private RidesBeans cursorToRide(Cursor c) {
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //Sinon on se place sur le premier élément
        c.moveToFirst();
        //On créé un ride
        RidesBeans r = new RidesBeans();
        //getting the ride :
        r.setID(Integer.parseInt(c.getString(NUM_COL_ID)));
        //r.setDeparture_time(Double.parseDouble(c.getString(NUM_COL_DEPARTURE_TIME)));
        //r.setArrival_time(Double.parseDouble(c.getString(NUM_COL_ARRIVAL_TIME)));
        r.setDeparture_time(Long.parseLong(c.getString(NUM_COL_DEPARTURE_TIME)));
        r.setArrival_time(Long.parseLong(c.getString(NUM_COL_ARRIVAL_TIME)));
        r.setDeparture_location(c.getString(NUM_COL_DEPARTURE_LOCATION));
        r.setArrival_location(c.getString(NUM_COL_ARRIVAL_LOCATION));
        r.setTime_elapsed(Double.parseDouble(c.getString(NUM_COL_TIME_ELAPSED)));
        r.setScore(Double.parseDouble(c.getString(NUM_COL_SCORE)));
        r.setDriver_id(Integer.parseInt(c.getString(NUM_COL_DRIVER_ID)));
        r.setCompany_id(Integer.parseInt(c.getString(NUM_COL_OMPANY_ID)));
        int valSend = Integer.parseInt(c.getString(NUM_COL_SEND));
        Boolean isSend = (valSend == 1) ? true : false;
        r.setSend(isSend);
        r.setTime(c.getString(NUM_COL_TIME));
        r.setAverageP(c.getDouble(NUM_COL_AVG_TIME));
        r.setTimeSecondes(c.getInt(NUM_COL_TIME_TOTAL_SECONDES));
        //r.setBad_behaviour(c.getInt(NUM_COL_BAD_BEHAVIOUR));

        //On ferme le cursor
        c.close();

        //On retourne le user
        return r;
    }

    /**
     * This class inhetits from Asynctask
     * Allows to execute the request to get address from latitude and logitude to Google map API
     */
    private class AsyncTaskGeocoder extends AsyncTask<String, Void, String> {

        /**
         * Url Google map api
         */
        private String urlGoogleMap = "http://maps.googleapis.com/maps/api/geocode/json?latlng=";

        /**
         * @Double presents the latitude
         */
        private String lat;
        /**
         * @Double represents the longitude
         */
        private String lng;

        /**
         * @Context represents the Activity
         */
        private Context context;

        /**
         * @Integer represents the response code from server
         */
        private int responseCode;

        /**
         * @String represents the resposne from server
         */
        private String resultat;
        /**
         * @JSONObject represents the ride in format json
         */
        private JSONObject ride;

        /**
         * @Double represents the location if 1 : departure
         * 2: arrival
         */
        private int location;

        /**
         * @Double represents the current context of database
         */
        private RideBDD objBDD;

        public AsyncTaskGeocoder(String lat, String lng, Context c, JSONObject ride, int location, RideBDD objBDD) {
            this.lat = lat;
            this.lng = lng;
            this.context = c;
            this.ride = ride;
            this.location = location;
            this.objBDD = objBDD;

            this.urlGoogleMap += this.lat + "," + this.lng + "&sensor=" + true;
            //Log.i("Adneom", "url is " + this.urlGoogleMap);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            //if( netInfo != null && netInfo.isConnected()) {
            try {
                URL url = new URL(this.urlGoogleMap);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("GET");

                responseCode = httpURLConnection.getResponseCode();

                InputStream inputStream;
                // get stream
                if (responseCode < HttpURLConnection.HTTP_BAD_REQUEST) {
                    //Log.i("Adneom", "(Ride) Response code is " + responseCode + " and is ok !!!");
                    inputStream = httpURLConnection.getInputStream();
                } else {
                    inputStream = httpURLConnection.getErrorStream();
                    //Log.i("Adneom","(Ride) Response code is "+responseCode+" and is not ok !!!");
                }
                // parse stream
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String temp, response = "";
                while ((temp = bufferedReader.readLine()) != null) {
                    response += temp;
                }
                //Log.i("Adneom","response(Asynctask) is "+response+" **/ ");
                resultat = response;
                manageResponse(resultat);

            } catch (MalformedURLException e) {
                e.printStackTrace();
                Log.i("Adneom", "(RideBDD) Error url : " + e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
                Log.i("Adneom", "(RideBDD) Error open connection : " + e.getMessage());
            }
            //}

            return null;
        }

        /**
         * This method allows to send a message to Menu Activity to launch the synchronization
         * wtih intent-filter : com.freeedrive_saving_driving.synchronization
         */
        public void askSynchonizationToMenuActivity() {
            Intent intent = new Intent("com.freeedrive_saving_driving.synchronization");

            //1 : have to ask
            //Log.i("Adneom","liste end is :"+objBDD.listeWithLocality);
            intent.putExtra("sync", "OK");
            intent.putExtra("liste", objBDD.listeWithLocality.toString());
            context.sendBroadcast(intent);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            objBDD.currentIndex++;
            //Log.i("Adneom","current is "+rr.currentObj+" and maximum is "+rr.max);
            if (objBDD.sizeCurrentListe == objBDD.currentIndex) {
                //Log.i("Adneom","finish tt ");
                askSynchonizationToMenuActivity();
            }
        }

        /**
         * Allows to handle the response from Google map API to retrieve the address
         *
         * @param resp, @String represents the reponse
         */
        private void manageResponse(String resp) {
            if (resp != null) {
                JSONObject data = null;
                try {
                    data = new JSONObject(resp);
                    //Log.i("Adneom", " data : " + data.toString());
                    JSONArray results = null;
                    if (data != null) {
                        results = data.getJSONArray("results");
                        if (results != null && results.length() > 0) {
                            Log.i("Adneom", " results : " + results);
                            JSONObject obj = results.getJSONObject(0);
                            if (obj != null) {
                                //Log.i("Adneom","obj is "+obj);
                                JSONArray address_components = obj.getJSONArray("address_components");
                                if (address_components != null) {
                                    //Log.i("Adneom"," address components "+address_components);
                                    JSONObject addresses = address_components.getJSONObject(2);
                                    if (addresses != null) {
                                        //Log.i("Adneom","addresses "+addresses);
                                        if (addresses.has("long_name")) {
                                            String locality = addresses.getString("long_name");
                                            //Log.i("Adneom","locality is "+locality);
                                            //departure
                                            if (ride != null && location == 1) {
                                                ride.put("departure_location", locality);
                                            } else {
                                                //arrival:
                                                ride.put("arrival_location", locality);
                                            }
                                            objBDD.setJOBject(ride);
                                            Log.i("Adneom", "asynctask " + ride);
                                            objBDD.listeWithLocality.put(ride);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Log.i("Adneom", "(RideBDD) Error first jsonobject : " + ex.getMessage());
                }
            }
        }
    }

}
