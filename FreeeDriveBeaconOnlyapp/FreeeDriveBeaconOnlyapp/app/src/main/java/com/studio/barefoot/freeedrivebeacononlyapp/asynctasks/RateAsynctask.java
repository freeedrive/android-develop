package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;


import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.interfacess.IRate;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import static com.studio.barefoot.freeedrivebeacononlyapp.ContactUsActivity.progressBarDialogContactUs;

/**
 * This class allows to execute the request to make a feedback to Freedrive
 * Created by gtshilombowanticale on 05-09-16.
 */
public class RateAsynctask extends AsyncTask<String,Void,String> {

    /**
     * Represents Activity
     */
    private Context context;
    /**
     * Code returned by server
     */
    private int responseCode;
    /**
     * Represent response from server
     */
    private String resultat;
    /**
     * Interface allowing to communicate with activity
     */
    private IRate iRate;
    /**
     * Represents the token
     */
    private String token;

    /**
     * Represent call api
     */
    private static final String mUrl = WebServiceConstants.WEBSERVICE_URL_PREFIX+WebServiceConstants.END_POINT_FEEDBACK;

    /**
     * Represents the parameter
     */
    private List<NameValuePair> mParams;

    /**
     * The class constructor
     * @param context(in),@Activity represents the activity
     * @param iRate(in), @Interface represents the interface to handler the response from server
     */
    public RateAsynctask(Context context, IRate iRate){
        this.context = context;
        this.iRate = iRate;

                token = "Bearer "+ DataHandler.getStringPreferences(AppConstants.TOKEN_NUMBER);


            //token = "Bearer "+Account.getInstance().getToken();

        mParams = new ArrayList<NameValuePair>();
    }

    /**
     * AsyncTask method basic calls before a request.
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     * AsyncTask method basic calls during a request.
     */
    @Override
    protected String doInBackground(String... params) {
        try {
            URL url = new URL(mUrl);
            Log.e("URL",""+url);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();


            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Authorization", token);
            urlConnection.setRequestProperty("X-Requested-With", "XMLHttpRequest");


            mParams.add(new BasicNameValuePair("message", params[0]));
            OutputStream os = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getQuery(mParams));
            writer.flush();
            writer.close();
            os.close();

            urlConnection.connect();

            responseCode= urlConnection.getResponseCode();
            Log.e("ResponseCode",""+responseCode);
            InputStream inputStream;
            // get stream
            if (responseCode < HttpURLConnection.HTTP_BAD_REQUEST) {
                Log.i("Adneom", "(RateAsynctask) Response code is " + responseCode + " and is ok !!!");
                inputStream = urlConnection.getInputStream();
            } else {
                inputStream = urlConnection.getErrorStream();
                Log.i("Adneom","(RateAsynctask)Response code is "+responseCode+" and is not ok !!!");
            }
            if (responseCode==500){
                try{
                    AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                    error_500.setMessage(context.getResources().getString(R.string.error_rate_500)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_500.show();
                }catch (Exception exception){
                    exception.printStackTrace();
                }

            }

            // parse stream
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String temp, response = "";
            while ((temp = bufferedReader.readLine()) != null) {
                response += temp;
            }
            //Log.i("Adneom","response(Asynctask) is "+response+" **/ ");
            resultat = response;

        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.i("Adneom","Error url : "+e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("Adneom", "Error open connection : "+e.getMessage());
        }
        return null;
    }

    /**
     * AsyncTask method basic calls after a request.
     */
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try{
            progressBarDialogContactUs.dismiss();
            progressBarDialogContactUs = null;
        }catch (NullPointerException exception){
            exception.printStackTrace();
        }

        Log.e("resultat","error "+resultat);
        Log.e("responseCode","error "+responseCode);

        switch (responseCode){
            case 200:
                iRate.managerate(resultat);
                break;
            case 401:
                Log.e("resultat","error "+resultat);
                break;
            case 422:
                break;
            case 500:
                try{
                    AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                    error_500.setMessage(context.getResources().getString(R.string.error_rate_500)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_500.show();
                }catch (Exception e){
                    e.printStackTrace();
                }

                break;
        }
    }

    /**
     * Allows to the parameters to support UTF-8
     * @param params(in), @List represent the parameters list
     * @return(out), the paraemters in String
     */
    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }
}
