package com.studio.barefoot.freeedrivebeacononlyapp.fragments;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.GraphPhoneSafetyAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.PerformancePackDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;



public class GraphPhoneSafetyFragment extends Fragment {

    View graphView;
    Button phoneSafety,drivingPerformance;
    Fragment fragment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    RelativeLayout relativeLayoutPhoneSafety,relativeLayoutdriving,relativeLayoutdrivingPerformance,relativeLayoutPhone;
    TextView phonesafety,drivingperformance;
    public static ProgressBarDialog progressBarDialogNotifications;
    static LineChart chart;
    static JSONObject jsonObject_driverAvg = new JSONObject();
    static JSONObject jsonObject_companyAvg = new JSONObject();
    PerformancePackDialog performancePackDialog;
    ImageView ImgDrivingPerformance;
    public GraphPhoneSafetyFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        graphView =  inflater.inflate(R.layout.fragment_phone_saftey, container, false);

        SharedPreferences settings = null;
        chart = (LineChart) graphView.findViewById(R.id.chart);
        //intiliaze the Views
        relativeLayoutPhoneSafety = (RelativeLayout) graphView.findViewById(R.id.insidecontaintermobilehotspot);
        relativeLayoutdriving = (RelativeLayout) graphView.findViewById(R.id.drivingrelativelayout);
        relativeLayoutdrivingPerformance = (RelativeLayout) graphView.findViewById(R.id.graph_drivingrelativelayout);
        relativeLayoutPhone = (RelativeLayout) graphView.findViewById(R.id.insidecontaintermobilehotspot);
        ImgDrivingPerformance = (ImageView) graphView.findViewById(R.id.drivingperformance);

        //intiliaze the TextViews
        phonesafety = (TextView) graphView.findViewById(R.id.textviewmobilehotSpot);
        drivingperformance = (TextView) graphView.findViewById(R.id.textviewdrivingperformance);

        phoneSafety = (Button) graphView.findViewById(R.id.graph_fragment_phonesafety_btn);
        drivingPerformance = (Button) graphView.findViewById(R.id.graph_fragment_drivingperformance_btn);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            relativeLayoutPhoneSafety.setBackground(getActivity().getResources().getDrawable(R.drawable.phonesafety_custom_shape));
        }
        phonesafety.setTextColor(getActivity().getResources().getColor(R.color.white));
        if (!DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                relativeLayoutdrivingPerformance.setBackground(getActivity().getResources().getDrawable(R.drawable.performance_pack_custom_shape));
                drivingperformance.setTextColor(getActivity().getResources().getColor(R.color.colorBTestMenu));
                ImgDrivingPerformance.setImageResource(R.drawable.tab_icon_car_grey);
            }

        }

        //settings = getActivity().getSharedPreferences(FILE_NAME_SHARED_PREF,Context.MODE_PRIVATE);
        if(AppUtils.isNetworkAvailable()) {

             progressBarDialogNotifications =new ProgressBarDialog(getActivity());
             progressBarDialogNotifications.setTitle(getString(R.string.title_progress_dialog));
             progressBarDialogNotifications.setMessage(getString(R.string.body_progress_dialog));
             progressBarDialogNotifications.show();
             String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER);
             List<NameValuePair> mParams = new ArrayList<NameValuePair>();
             mParams.add(new BasicNameValuePair("phone_number",phoneNumber));
             Log.e("PARAMS", "" + mParams);
             GraphPhoneSafetyAsyncTask getphonesafetygraphData = new GraphPhoneSafetyAsyncTask(getActivity(), WebServiceConstants.END_POINT_GRAPH_SCORE,mParams );
             getphonesafetygraphData.execute();
            //generateDataLine();
         }
         else if(!DataHandler.getStringPreferences(AppConstants.GRAPH_PHONE_SAFETY_AVG).isEmpty()){
            String graphValue = DataHandler.getStringPreferences(AppConstants.GRAPH_PHONE_SAFETY_AVG);
            try {
                JSONObject jsonObj = new JSONObject(graphValue);
                JSONArray graph_driverAvg_jsonArray = jsonObj.getJSONArray("driverAvg");
                JSONArray graph_companyAvg_jsonArray = jsonObj.getJSONArray("companyAvg");

                generateDataLine(graph_driverAvg_jsonArray,graph_companyAvg_jsonArray);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else{
            AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(getActivity());
            error_No_Internet.setMessage(getActivity().getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //NotificationActivity.super.onBackPressed();
                }
            });
            error_No_Internet.show();
        }

        drivingPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        relativeLayoutdrivingPerformance.setBackground(getActivity().getResources().getDrawable(R.drawable.driving_performance_custom_shape));
                    }
                    drivingperformance = (TextView) graphView.findViewById(R.id.textviewdrivingperformance);
                    drivingperformance.setTextColor(getActivity().getResources().getColor(R.color.white));


                    fragment = new GraphDrivingPerformanceFragment();
                    fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.phone_fragment, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        relativeLayoutPhone.setBackground(getActivity().getResources().getDrawable(R.drawable.phone_safety_custom_shape));
                    }
                    phonesafety = (TextView) graphView.findViewById(R.id.textviewmobilehotSpot);
                    phonesafety.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
                }else {
                    performancePackDialog = new PerformancePackDialog(getActivity());
                    performancePackDialog.show();
                }
            }
        });

        return graphView;


    }


    /**
     * generates a random ChartData object with just one DataSet
     *
     * @return
     */
    public static LineData generateDataLine(JSONArray driverArray , JSONArray companyArray) {
        LineData cd = null;
        String driverAvg_Date = null;
        String driverAvg_Score = null;
        String companyAvg_Date = null;
        String companyAvg_Score = null;
        ArrayList<Entry> e1 = new ArrayList<Entry>();
        ArrayList<Entry> e2 = new ArrayList<Entry>();
        ArrayList<Entry> e3 = new ArrayList<Entry>();
        if(driverArray !=null && companyArray !=null){

        if (driverArray !=null) {
            for(int i = 0;i < driverArray.length();i++){

                try {
                    jsonObject_driverAvg = driverArray.getJSONObject(i);
                     Log.e("DriverAvgObject "," : "+jsonObject_driverAvg);

                    /*driverAvg_Date = jsonObject_driverAvg.getString("date");
                    Log.e("GRAPH_driverAvg_Date", driverAvg_Date);*/

                    driverAvg_Score = jsonObject_driverAvg.getString("value");
                    Log.e("GRAPH_drivingAvg_Score",driverAvg_Score);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                float graph_dirver_avg_score = Float.parseFloat(driverAvg_Score);
                e1.add(new Entry(i,graph_dirver_avg_score));


            }
        }


        if(companyArray !=null){
            for(int i=0;i<companyArray.length();i++){
                try {
                    jsonObject_companyAvg = companyArray.getJSONObject(i);
                    Log.e("CompanyAvgObject "," : "+jsonObject_companyAvg);

                    companyAvg_Date = jsonObject_companyAvg.getString("date");
                    Log.e("companyAvg_Date",companyAvg_Date);
                    companyAvg_Score =jsonObject_companyAvg.getString("value");
                    Log.e("companyAvg_Score",companyAvg_Score);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                float graph_company_avg_score  = Float.parseFloat(companyAvg_Score);
                //e2.add(new Entry(i, e1.get(i).getY() - 30));
                e2.add(new Entry(i, graph_company_avg_score));

            }


        }

            LineDataSet d1 = new LineDataSet(e1, "");
            d1.setLineWidth(2.5f);
            //d1.setCircleRadius(4.5f);
            d1.setHighLightColor(Color.rgb(0,191,255));
            d1.setDrawValues(true);
            d1.disableDashedLine();
            d1.setDrawCircleHole(false);


            LineDataSet d2 = new LineDataSet(e2, "");
            d2.setLineWidth(2.5f);
            //d2.setCircleRadius(4.5f);
            d2.setColor(Color.rgb(0,191,255));
            d2.setHighLightColor(Color.rgb(65,105,225));
            //d2.setColor(ColorTemplate.VORDIPLOM_COLORS[0]);
            d2.setCircleColor(Color.rgb(0,191,255));
            d2.setDrawValues(true);
            d2.disableDashedLine();
            d2.setDrawCircleHole(true);

            ArrayList<ILineDataSet> sets = new ArrayList<ILineDataSet>();
            sets.add(d1);
            sets.add(d2);

            // apply styling
            chart.getDescription().setEnabled(false);
            chart.setDrawMarkers(true);
            chart.setDrawGridBackground(false);
            chart.setScaleEnabled(true);



            XAxis xAxis = chart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setEnabled(false);
            /*xAxis.setLabelCount(5);
            xAxis.setAxisMinimum(0f);
            xAxis.setAxisMaximum(10f);*/
            xAxis.setDrawGridLines(true);
            xAxis.setDrawAxisLine(true);


            YAxis leftAxis = chart.getAxisLeft();
        /*leftAxis.setTypeface(mTf);*/
            leftAxis.setLabelCount(8, false);
            /*leftAxis.setDrawGridLines(true);*/
            leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
            leftAxis.setAxisMaximum(105f);

            YAxis rightAxis = chart.getAxisRight();
        /*rightAxis.setTypeface(mTf);*/
            rightAxis.setLabelCount(8, false);
            /*rightAxis.setDrawGridLines(true);*/
            rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
            rightAxis.setAxisMaximum(105f);



            // do not forget to refresh the chart
            // holder.chart.invalidate();
            chart.animateX(750);


            cd = new LineData(sets);
            // set data
            chart.setData(cd);


        }
        return cd;
    }


}
