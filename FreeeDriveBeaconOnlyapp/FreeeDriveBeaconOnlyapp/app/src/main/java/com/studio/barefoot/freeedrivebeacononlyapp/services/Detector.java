package com.studio.barefoot.freeedrivebeacononlyapp.services;

import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.service.notification.NotificationListenerService;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Chronometer;

import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.sensors.FDsensors;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

/**
 * Created by mcs on 1/11/2017.
 */
@TargetApi(21)
public class Detector extends NotificationListenerService {

    private  long MAIN_TIMER_RATE = 10000;
    /**
     * Represents The application
     */
    ApplicationController FDapp;
    public static ApplicationController FdApp;
    /**
     * Represents the sum of sensors (bad behaviros)
     */
    public static int cptSnsors;
    /**
     * Chronometer to detect the secondes of movement
     * (BluetoothActivity)
     */
    public static Chronometer sensorStaticChronometer;

    private boolean mInBackground;
    public Foreground.Listener mForegroundListener = new Foreground.Listener()
    {
        public void onBecameForeground()
        {
            Log.e("FD","app went foreground");
            //Log.e("Adneom","(Detector) app went foreground");
            mInBackground = false;
        }
        public void onBecameBackground()
        {
            Log.e("FD","app went background");
            //Log.e("Adneom","(Detector) app went background");
            mInBackground = true;
        }

    };
    private static int count;
    private boolean isAlreadyAsking;
    private static Foreground.Listener mForegroundListenerStatic;
    private Boolean userAlreadyLoggedIn=false;
    private BroadcastReceiver networkBroadCastReciever;

    //Help to run the service in bg forever

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    public void onCreate()
    {
        super.onCreate();

        FDapp = (ApplicationController) getApplicationContext();
        Log.e("Bfpk"," *** detected that device is started *** ");
        Log.e("DetectorServiceStarted","started");
        /* ********************    ****************** */
        FdApp = FDapp;
        count = 0;
        isAlreadyAsking = false;
        networkBroadCastReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                    NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                    if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
                        Log.e("NETWORK__Detector","INTERNET");
                        if (!AppUtils.isUpdated) {

                            try{
                                if (AppUtils.isNetworkAvailable()){
                                    AppUtils.rideSync(context);


                                }

                /*        Intent i = new Intent(context.getApplicationContext(), ScoreSynchronizationActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);*/
                            }catch (NullPointerException exception){
                                exception.printStackTrace();
                            }
                  /*  differenceTimes(context);*/
                            //isUpdated = true;
                        } else {
                            AppUtils.isUpdated = false;
                        }
                        Log.d("Network", "Internet YAY");
                    } else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED) {
                        Log.d("Network", "No internet :(");
                    }
                }
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            RegisterNetworkBroadCastReciever();
        }
        Foreground.get(this).addListener(mForegroundListener);

        mForegroundListenerStatic = mForegroundListener;

        //reboot :
        Boolean val = DataHandler.getBooleanPreferences(AppConstants.KEY_REBOOT_DEVICE);
        if(val == true){
            //Log.i("Adneom", " *** (Detector) restart service(Detector) is started  *** ");

            //force app launches in background after reboot :
            mForegroundListener.onBecameBackground();

        }else{
            userAlreadyLoggedIn = DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY);
            if (userAlreadyLoggedIn){
                Intent serviceIntent = new Intent(this, BackgroundBeaconScan.class);
                startService(serviceIntent);
                Log.e("Detector","I started beacon service");
            }
        }
         startService();


    }

    private void startService() {




        //chronometer :
        sensorStaticChronometer = new Chronometer(FdApp);

        //screen_off:
        registerScreenStateBroadcastReceiver();


        guiHandler.sendMessageDelayed(new Message(), MAIN_TIMER_RATE);
        Log.e("FD", "detector started");
    }
    private Handler guiHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            mainRun();
            guiHandler.sendMessageDelayed(new Message(), MAIN_TIMER_RATE);
        }
    };

    private void mainRun() {

        //is BT enabled?
        try
        {
            if(!isBluetoothEnabled())
            {
                //send to back old back end BT's state:
                //logBluetooth(false);
                if(mInBackground)
                {
                    Log.e("FD","BT disabled in background");
                    return;
                }
                else
                {
                    Intent intent = new Intent("com.freeedrive.bluetooth_enable");
                    sendBroadcast(intent);
                    return;
                }

            }
            if (!checkIfGpsIsEnabled()){
                if(mInBackground)
                {
                    Log.e("FD","GPS disabled in background");
                    return;
                }
                else
                {
                    Intent intent = new Intent("com.freeedrive_saving_driving.enable_gps");
                    sendBroadcast(intent);
                    return;
                }
            }
            try
            {
                if(!areNotificationsEnabled())
                {
                    //Log.i("Adneom","*** (Detector) ask permission notification : "+isAlreadyAsking+" *** ");
                    isAlreadyAsking = true;

                    Intent intent = new Intent("com.freeedrive.notification_settings");
                    sendBroadcast(intent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try
            {
                if(!forDonotDisturbMode()){
                    Intent intent = new Intent(
                            android.provider.Settings
                                    .ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
                    sendBroadcast(intent);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        catch(Exception ex)
        {
            //Log.e("FD","BT exception disabled");
            ex.printStackTrace();
            Log.e("FD","BT exception disabled"+ex);

            //send to back old back end BT's state:
            //logBluetooth(false);
            return;
        }
        try{
            if (!AppUtils.isBGServiceActive) {
                Intent serviceIntent = new Intent(this, BackgroundBeaconScan.class);
                startService(serviceIntent);
            }{
                return;
            }
        }catch (Exception e)
        {

        }
        //Add notification listener if to be added.
    }
    private boolean areNotificationsEnabled(){
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        boolean areNotificationsEnabled = notificationManagerCompat.areNotificationsEnabled();
        return areNotificationsEnabled;
    }
    private boolean forDonotDisturbMode(){
        Boolean donotDisturb = true;
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N
                && !notificationManager.isNotificationPolicyAccessGranted()) {
            donotDisturb = false;
        }
        return donotDisturb;
    }


    private boolean
    isNotificationManager() throws Exception
    {

        ContentResolver contentResolver = getContentResolver();
        String enabledNotificationListeners = Settings.Secure.getString(contentResolver, "enabled_notification_listeners");
        //Log.e("FD","enabledNotificationListeners: " + enabledNotificationListeners);
        String packageName = getPackageName();
        return enabledNotificationListeners != null && enabledNotificationListeners.contains(packageName);
    }
    public void onDestroy()
    {

        Foreground.get(this).removeListener(mForegroundListener);
        super.onDestroy();
        Log.e("FD", "detector stopped");
    }
    public static boolean isBluetoothEnabled() throws Exception
    {
        boolean ret = true;
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(bluetoothAdapter == null || !bluetoothAdapter.isEnabled())
        {
            ret = false;
        }

        return ret;
    }   public static boolean checkIfGpsIsEnabled() throws Exception {
        LocationManager locManager = (LocationManager) ApplicationController.getmAppcontext().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        if(locManager != null){
                gps_enabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                //Log.i("Adneom","gps : "+gps_enabled);

            AppUtils.gpsenabled = true;
            if(!gps_enabled){
                AppUtils.gpsenabled = false;
            }
        }
        return gps_enabled;
    }

    private void RegisterNetworkBroadCastReciever(){
       ApplicationController.getmAppcontext().registerReceiver(networkBroadCastReciever,new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

    }
    /**
     * Method allowing to manage screen's state to set boolean to false (isUnlock)
     * The receiver broadcast is used
     */
    private void  registerScreenStateBroadcastReceiver() {
        final IntentFilter theFilter = new IntentFilter();
        /** System Defined Broadcast */
        theFilter.addAction(Intent.ACTION_SCREEN_OFF);
        theFilter.addAction(Intent.ACTION_SCREEN_ON);
        final KeyguardManager myKM = (KeyguardManager) getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);

        BroadcastReceiver screenOnOffReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String strAction = intent.getAction();
                boolean isPhoneLocked = myKM.inKeyguardRestrictedInputMode();
                Log.e("PhoneLockStatus",""+isPhoneLocked);
                if (strAction.equals(Intent.ACTION_SCREEN_OFF))
                {
                    Log.e("BFpk", "screen is OFF ");
                    AppUtils.isUnlock = false;
                }    else if (strAction.equals(Intent.ACTION_SCREEN_ON) && isPhoneLocked == false)
                {
                    Log.e("BFpk", "screen is ON ");
                    AppUtils.isUnlock = true;
                }
//                else{
//                    FDUtils.isUnlock = true;
//                }

            }
        };

        getApplicationContext().registerReceiver(screenOnOffReceiver, theFilter);
    }
}
