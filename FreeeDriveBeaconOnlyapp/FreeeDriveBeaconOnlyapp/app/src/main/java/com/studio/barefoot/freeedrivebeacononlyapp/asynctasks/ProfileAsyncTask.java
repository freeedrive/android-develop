package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.studio.barefoot.freeedrivebeacononlyapp.BlockedFreeDriveActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.interfacess.IRegisterFD;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.ProfileActivity.progressBarDialogProfile;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.PREF_KEY_FD_STATUS;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.fdBlockedScreen;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.fdLogout;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler.FILE_NAME_SHARED_PREF;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler.deletePreference;

/**
 * Created by mcs on 12/28/2016.
 */

public class ProfileAsyncTask extends AsyncTask<Void,Void,String> {

    /**
     * Represents the url website
     */
    private static final String mUrl = WebServiceConstants.WEBSERVICE_URL_PREFIX+WebServiceConstants.END_POINT_UPDATE_PROFILE;
    /**
     * Represents the context : Acitivty
     */
    private Context contex;
    /**
     * Represents the parameters to send to server
     */
    private List<NameValuePair> mParams;
    /**
     * Represents the result from server
     */
    private String resultat;
    /**
     * Represents the response code from server
     */
    private int responseCode;
    /**
     * Represents the account token to send to server
     */
    private String token;

    /**
     * Represents the interface to handle the response from server
     */
    private IRegisterFD iRegisterFD;

    /**
     * The class constructor
     * @param context(in),@Activity represent the activity
     * @param iRegisterFD(in), @Interface represents the interface which handles the response from server
     * @param first_name(in), @String represents the first name
     * @param last_name(in), @String represents the last name
     * @param email(in), @Activity represents the ae-mail
     */
    public ProfileAsyncTask(IRegisterFD iRegisterFD,Context context, String first_name,String last_name, String email,String lang){
        //public ProfileAsyncTask(IRegisterFD iRegisterFD,Context context, String first_name,String last_name, String email, String phone_number,String city){
        this.contex = context;
        this.iRegisterFD = iRegisterFD;
        mParams = new ArrayList<NameValuePair>();
        mParams.add(new BasicNameValuePair("first_name",first_name));
        mParams.add(new BasicNameValuePair("last_name", last_name));
        mParams.add(new BasicNameValuePair("email", email));
        //add token from frirebase :
        String gcm_token = FirebaseInstanceId.getInstance().getToken();
        // mParams.add(new BasicNameValuePair("gcm_token", gcm_token));
        mParams.add(new BasicNameValuePair("lang", lang));
        //mParams.add(new BasicNameValuePair("city", city));
        Log.e("BFpk i was here",lang);
    }


    @Override
    protected String doInBackground(Void... voids) {
        ConnectivityManager cm = (ConnectivityManager)contex.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if( netInfo != null && netInfo.isConnected()) {

            URL url = null;
            try {
                url = new URL(mUrl);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                token = "Bearer "+ DataHandler.getStringPreferences(AppConstants.TOKEN_NUMBER);



                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Authorization", token);
                urlConnection.setRequestProperty("X-Requested-With", "XMLHttpRequest");

                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(mParams));
                writer.flush();
                writer.close();
                os.close();

                responseCode = urlConnection.getResponseCode();

                responseCode = urlConnection.getResponseCode();

                InputStream inputStream;
                // get stream
                if (responseCode < HttpURLConnection.HTTP_BAD_REQUEST) {
                    Log.e("Adneom", "(ProfileAsynctask) Response code is " + responseCode + ", is ok !!!");
                    inputStream = urlConnection.getInputStream();
                } else {
                    inputStream = urlConnection.getErrorStream();
                    Log.e("Adneom","(ProfileAsynctask) Response code is "+responseCode+", is not ok !!!");
                }
                // parse stream
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String temp, response = "";
                while ((temp = bufferedReader.readLine()) != null) {
                    response += temp;
                }
                resultat = response;
                Log.e("resultat",""+resultat);


            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            //Log.i("Adneom","url is "+mUrl);
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resultat;
    }
    /**
     * AsyncTask method basic calls before a request.
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
    /**
     * AsyncTask method basic calls after a request.
     */
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s != null) {
            try{
                progressBarDialogProfile.dismiss();
                progressBarDialogProfile = null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }

            JSONObject drivingPerformanceObject = new JSONObject();
            switch (responseCode) {
                case 200 :
                    if(resultat != null) {


                        JSONObject objAccount = null;
                        try {
                            objAccount = new JSONObject(resultat);

                            for (int i = 0; i < objAccount.length(); i++) {


                                if (objAccount != null) {
                                    boolean fd_active_status_flag = objAccount.getBoolean("active");

                                    if (fd_active_status_flag == false) {
                                        DataHandler.updatePreferences(PREF_KEY_FD_STATUS,fd_active_status_flag);
                                        DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY,false);
                                        fdBlockedScreen(contex);
                                        /*Intent intent = new Intent(ApplicationController.getmAppcontext(), BlockedFreeDriveActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        ApplicationController.getmAppcontext().startActivity(intent);*/
                                    }
                                }

                            }
                            if(objAccount.getJSONObject("contract")!=null&&!objAccount.getJSONObject("contract").equals("0") ){
                                drivingPerformanceObject = objAccount.getJSONObject("contract");
                            }

                            if (drivingPerformanceObject!=null&&drivingPerformanceObject.length()>0){
                                boolean driving_performanceActive = false;
                                for (int k=0;k<drivingPerformanceObject.length();k++){

                                    driving_performanceActive = drivingPerformanceObject.getBoolean("driving_performance");
                                 /*if (!driving_performanceActive.equalsIgnoreCase("")){
                                     break;
                                 }*/
                                }
                                if (driving_performanceActive == true){
                                    DataHandler.updatePreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE,true);
                                }else {
                                    DataHandler.updatePreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE,false);

                                }
                            }

                            iRegisterFD.manageRegister("updated", resultat);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    //  smsCodeIntent.putExtra(AppConstants.EXTRA_KEY_LOGIN_ACTIVITY,AppConstants.EXTRA_VALUE_LOGIN_ACTIVITY);
                    break;
                case 409:
                  /*  Intent smsCodeIntent = new Intent(context, SmsConfirmationActivity.class);
                    //  smsCodeIntent.putExtra(AppConstants.EXTRA_KEY_LOGIN_ACTIVITY,AppConstants.EXTRA_VALUE_LOGIN_ACTIVITY);
                    context.startActivity(smsCodeIntent);*/
                    break;
                case 401:
                    //Number already exists
                    try{
                        AlertDialog.Builder error_401 = new AlertDialog.Builder(contex);
                        error_401.setMessage(contex.getResources().getString(R.string.error_login_401aa)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_401.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    break;
                case 400:
                    //Phone numbre not found
                    AlertDialog.Builder error_400 = new AlertDialog.Builder(contex);
                    error_400.setMessage(contex.getResources().getString(R.string.error_auth_token_expire_400)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deletePreference(FILE_NAME_SHARED_PREF);
                            fdLogout(contex);
                            DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY,false);
                            dialog.dismiss();
                        }
                    });
                    error_400.show();
                    break;
                case 403:
                    try{
                        AlertDialog.Builder error_422 = new AlertDialog.Builder(contex);
                        error_422.setMessage(contex.getResources().getString(R.string.error_login_403aa)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_422.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }

                    break;
            }
        }else {
            try{
                progressBarDialogProfile.dismiss();
                progressBarDialogProfile = null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }

        }
    }

    /**
     * Allows to the parameters to support UTF-8
     * @param params(in), @List represent the parameters list
     * @return(out), the paraemters in String
     */
    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }
}
