package com.studio.barefoot.freeedrivebeacononlyapp.dialogs;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.RegisterActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

import java.util.Locale;

/**
 * Created by mcs on 12/28/2016.
 */

public class EcoDrivingDialog extends BaseAlertDialog  {
    TextView contact_us;
    Button btn_accept_privacy;
    private String languagePref="";
    ContentResolver resolver;
    KeyEvent keyEvent;

    public EcoDrivingDialog(final Context context) {
        super(context);

        LayoutInflater factory = LayoutInflater.from(context);
        final View progressBarView = factory.inflate(R.layout.eco_driving_dialog, null);
        contact_us = (TextView) progressBarView.findViewById(R.id.contact_us);
        setView(progressBarView);
        this.context = context;
       /* tv_privacy_Link = (TextView) progressBarView.findViewById(R.id.tv_text_privacy_link);
        btn_accept_privacy = (Button) progressBarView.findViewById(R.id.img_accept_privacy);
        tv_privacy_Link.setPaintFlags(tv_privacy_Link.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tv_privacy_Link.setText(context.getResources().getString(R.string.link_privacy_dialog));
        resolver = context.getContentResolver();
        keyEvent = new KeyEvent(4,4);
        //setCancelable(true);

        tv_privacy_Link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/privacy"));
                intentBrowser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ApplicationController.getmAppcontext().startActivity(intentBrowser);
            }
        });

        btn_accept_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                DataHandler.updatePreferences(AppConstants.USER_ACCEPTED_PRIVACY,true);
                context.startActivity(new Intent(context,RegisterActivity.class));
            }
        });
        onKeyDown(4,keyEvent);*/
        languagePref =  DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG);
        String localLanguage= Locale.getDefault().getLanguage();


        if(languagePref !=null || !localLanguage.isEmpty()) {
            if (languagePref.equalsIgnoreCase("en")||Locale.getDefault().getLanguage().equalsIgnoreCase("en")) {
                String qrdesc = context.getResources().getString(R.string.eco_contact_us);

                final SpannableStringBuilder sb = new SpannableStringBuilder(qrdesc);


                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        sendEmail();
                    }
                };
                sb.setSpan(clickableSpan, 1, 15, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new UnderlineSpan(), 0, sb.length(), 0);
                contact_us.setText(sb);
                contact_us.setMovementMethod(LinkMovementMethod.getInstance());



            }
        }

    }
    protected void sendEmail() {
        String[] TO = {"info@freeedrive.com"};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Missing car model");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Following car mode is missing");

        try {
            context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));

        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void dismiss() {
            super.dismiss();
    }


}
