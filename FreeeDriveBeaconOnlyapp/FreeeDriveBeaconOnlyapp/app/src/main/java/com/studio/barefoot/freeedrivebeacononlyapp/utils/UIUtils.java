package com.studio.barefoot.freeedrivebeacononlyapp.utils;

import android.app.Activity;
import android.graphics.Typeface;

/**
 * Created by kl on 2/15/16.
 */
public class UIUtils
{
    private static UIUtils sUIUtils;

    public static UIUtils getInstance()
    {
        if(sUIUtils == null)
            sUIUtils = new UIUtils();

        return sUIUtils;
    }

    public Typeface getMediumFont(Activity a)
    {
        Typeface typeface = Typeface.createFromAsset(a.getAssets(), "fonts/DINNextLTPro-MediumCond.ttf");
        return typeface;
    }

    public Typeface getLightFont(Activity a)
    {
        Typeface typeface = Typeface.createFromAsset(a.getAssets(), "fonts/DINNextLTPro-LightCondensed.ttf");
        return typeface;
    }
}
