package com.studio.barefoot.freeedrivebeacononlyapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.hbb20.CountryCodePicker;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.LoginAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ChangePhoneNumberDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.UIUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity {
    Button imgLogin;
    TextView tv_Hello, updatephoneNo, updateConnector;
    EditText et_Phone;
    View actionBarView;
    public static ProgressBarDialog progressBarDialogLogin;
    CountryCodePicker ccp;
    private String gcm_token = "";
    String phonenNumber = "";
    String phoneNumberFormat = "";
    String correctPhoneNumber = "";
    String updatephonenoandconnector;
    String phone_number_temp = "";
    ChangePhoneNumberDialog changePhoneNoDialog;


    public LoginActivity() {
        LocaleUtils.updateConfig(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        imgLogin = (Button) findViewById(R.id.img_sms_next);
        et_Phone = (EditText) findViewById(R.id.et_phone);

        tv_Hello = (TextView) findViewById(R.id.hello);
        tv_Hello.setTypeface(UIUtils.getInstance().getMediumFont(this));
        updatephoneNo = (TextView) findViewById(R.id.login_tv_new_phn);
        updateConnector = (TextView) findViewById(R.id.login_tv_new_connector);
        et_Phone.setTypeface(UIUtils.getInstance().getLightFont(this));
        actionBarView = getLayoutInflater().inflate(R.layout.custom_toolbarr, null);
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        setupActionBar();

 /*       File f = this.getDatabasePath("freeedriveV2.db");

        long dbSize = f.getTotalSpace();
        long length = f.length();
        long maxSize = f.getTotalSpace();
        Log.e("DBSIZE", "" + dbSize + " length--> " + length);*/

        et_Phone.setMaxLines(1);
        et_Phone.setSingleLine();

        phoneNumberFormat = ccp.getDefaultCountryCodeWithPlus();
        imgLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(LoginActivity.this,SmsConfirmationActivity.class));
                LoginUser();
            }
        });
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {

                //  et_Sms_Code.setText(ccp.getSelectedCountryCodeWithPlus());
                phoneNumberFormat = ccp.getSelectedCountryCodeWithPlus();


            }
        });

        updatephonenoandconnector = DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG);
        Log.e("updatephone", updatephonenoandconnector);

        if (updatephonenoandconnector != null && !updatephonenoandconnector.isEmpty()) {
            if (updatephonenoandconnector.equalsIgnoreCase("en")) {
                String newphoneno_en = this.getResources().getString(R.string.new_phoneNumber);
                Log.e("newphoneno_en", newphoneno_en);
                final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_en);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 25, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        if (!checkFields()) {
                            phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                            phone_number_temp = phoneNumberFormat + phonenNumber;
                            DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                            changePhoneNoDialog = new ChangePhoneNumberDialog(LoginActivity.this, phone_number_temp);
                            changePhoneNoDialog.show();
                        }

                    }
                };
                sb.setSpan(clickableSpan, 25, 29, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 25, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                updatephoneNo.setText(sb);
                updatephoneNo.setMovementMethod(LinkMovementMethod.getInstance());
                String newconnector_en = this.getResources().getString(R.string.new_connector);

                final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_en);
                final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb2.setSpan(bss2, 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {

                        if (!checkFields()) {
                            phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                            phone_number_temp = phoneNumberFormat + phonenNumber;
                            DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                            Intent startQrcodeIntent = new Intent(LoginActivity.this, QrCodeActivity.class);
                            startQrcodeIntent.putExtra("comingFromProfile", "1");
                            startActivity(startQrcodeIntent);
                        }

                    }
                };
                sb2.setSpan(clickableSpan1, 21, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                updateConnector.setText(sb2);
                updateConnector.setMovementMethod(LinkMovementMethod.getInstance());


            } else if (updatephonenoandconnector.equalsIgnoreCase("es")) {
                String newphoneno_es = this.getResources().getString(R.string.new_phoneNumber);
                Log.e("newphoneno_es", newphoneno_es);
                final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_es);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 36, 40, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        if (!checkFields()) {
                            phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                            phone_number_temp = phoneNumberFormat + phonenNumber;
                            DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                            changePhoneNoDialog = new ChangePhoneNumberDialog(LoginActivity.this, phone_number_temp);
                            changePhoneNoDialog.show();
                        }
                    }
                };
                sb.setSpan(clickableSpan, 36, 40, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 36, 40, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                updatephoneNo.setText(sb);
                updatephoneNo.setMovementMethod(LinkMovementMethod.getInstance());
                String newconnector_es = this.getResources().getString(R.string.new_connector);

                final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_es);
                final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb2.setSpan(bss2, 26, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        if (!checkFields()) {
                            phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                            phone_number_temp = phoneNumberFormat + phonenNumber;
                            DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                            Intent startQrcodeIntent = new Intent(LoginActivity.this, QrCodeActivity.class);
                            startQrcodeIntent.putExtra("comingFromProfile", "1");
                            startActivity(startQrcodeIntent);
                        }
                    }
                };
                sb2.setSpan(clickableSpan1, 26, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 26, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                updateConnector.setText(sb2);
                updateConnector.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (updatephonenoandconnector.equalsIgnoreCase("fr")) {
                String newphoneno_fr = this.getResources().getString(R.string.new_phoneNumber);
                Log.e("newphoneno_fr", newphoneno_fr);
                final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_fr);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 44, 47, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        if (!checkFields()) {
                            phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                            phone_number_temp = phoneNumberFormat + phonenNumber;
                            DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                            changePhoneNoDialog = new ChangePhoneNumberDialog(LoginActivity.this, phone_number_temp);
                            changePhoneNoDialog.show();
                        }
                    }
                };
                sb.setSpan(clickableSpan, 44, 47, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 44, 47, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                updatephoneNo.setText(sb);
                updatephoneNo.setMovementMethod(LinkMovementMethod.getInstance());
                String newconnector_fr = this.getResources().getString(R.string.new_connector);
                final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_fr);
                final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb2.setSpan(bss2, 33, 36, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        if (!checkFields()) {
                            phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                            phone_number_temp = phoneNumberFormat + phonenNumber;
                            DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                            Intent startQrcodeIntent = new Intent(LoginActivity.this, QrCodeActivity.class);
                            startQrcodeIntent.putExtra("comingFromProfile", "1");
                            startActivity(startQrcodeIntent);
                        }
                    }
                };
                sb2.setSpan(clickableSpan1, 33, 36, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 33, 36, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                updateConnector.setText(sb2);
                updateConnector.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (updatephonenoandconnector.equalsIgnoreCase("nl")) {
                String newphoneno = this.getResources().getString(R.string.new_phoneNumber);
                final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        if (!checkFields()) {
                            phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                            phone_number_temp = phoneNumberFormat + phonenNumber;
                            DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                            changePhoneNoDialog = new ChangePhoneNumberDialog(LoginActivity.this, phone_number_temp);
                            changePhoneNoDialog.show();
                        }
                    }
                };
                sb.setSpan(clickableSpan, 21, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                updatephoneNo.setText(sb);
                updatephoneNo.setMovementMethod(LinkMovementMethod.getInstance());
                String newconnector_nl = this.getResources().getString(R.string.new_connector);
                final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_nl);
                final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb2.setSpan(bss2, 23, 27, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        if (!checkFields()) {
                            phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                            phone_number_temp = phoneNumberFormat + phonenNumber;
                            DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                            Intent startQrcodeIntent = new Intent(LoginActivity.this, QrCodeActivity.class);
                            startQrcodeIntent.putExtra("comingFromProfile", "1");
                            startActivity(startQrcodeIntent);
                        }
                    }
                };
                sb2.setSpan(clickableSpan1, 23, 27, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 23, 27, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                updateConnector.setText(sb2);
                updateConnector.setMovementMethod(LinkMovementMethod.getInstance());
            }


        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("en")) {
            String newphoneno_en = this.getResources().getString(R.string.new_phoneNumber);
            Log.e("newphoneno_en", newphoneno_en);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_en);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 25, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    if (!checkFields()) {
                        phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                        phone_number_temp = phoneNumberFormat + phonenNumber;
                        DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                        changePhoneNoDialog = new ChangePhoneNumberDialog(LoginActivity.this, phone_number_temp);
                        changePhoneNoDialog.show();
                    }

                }
            };
            sb.setSpan(clickableSpan, 25, 29, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 25, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            updatephoneNo.setText(sb);
            updatephoneNo.setMovementMethod(LinkMovementMethod.getInstance());
            String newconnector_en = this.getResources().getString(R.string.new_connector);

            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_en);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    if (!checkFields()) {
                        phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                        phone_number_temp = phoneNumberFormat + phonenNumber;
                        DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                        Intent startQrcodeIntent = new Intent(LoginActivity.this, QrCodeActivity.class);
                        startQrcodeIntent.putExtra("comingFromProfile", "1");
                        startActivity(startQrcodeIntent);
                    }

                }
            };
            sb2.setSpan(clickableSpan1, 21, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            updateConnector.setText(sb2);
            updateConnector.setMovementMethod(LinkMovementMethod.getInstance());
        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("es")) {
            String newphoneno_es = this.getResources().getString(R.string.new_phoneNumber);
            Log.e("newphoneno_es", newphoneno_es);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_es);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 36, 40, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    if (!checkFields()) {
                        phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                        phone_number_temp = phoneNumberFormat + phonenNumber;
                        DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                        changePhoneNoDialog = new ChangePhoneNumberDialog(LoginActivity.this, phone_number_temp);
                        changePhoneNoDialog.show();
                    }
                }
            };
            sb.setSpan(clickableSpan, 36, 40, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 36, 40, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            updatephoneNo.setText(sb);
            updatephoneNo.setMovementMethod(LinkMovementMethod.getInstance());
            String newconnector_es = this.getResources().getString(R.string.new_connector);

            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_es);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 26, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    if (!checkFields()) {
                        phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                        phone_number_temp = phoneNumberFormat + phonenNumber;
                        DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                        Intent startQrcodeIntent = new Intent(LoginActivity.this, QrCodeActivity.class);
                        startQrcodeIntent.putExtra("comingFromProfile", "1");
                        startActivity(startQrcodeIntent);
                    }
                }
            };
            sb2.setSpan(clickableSpan1, 26, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 26, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            updateConnector.setText(sb2);
            updateConnector.setMovementMethod(LinkMovementMethod.getInstance());

        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("fr")) {
            String newphoneno_fr = this.getResources().getString(R.string.new_phoneNumber);
            Log.e("newphoneno_fr", newphoneno_fr);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_fr);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 44, 47, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    if (!checkFields()) {
                        phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                        phone_number_temp = phoneNumberFormat + phonenNumber;
                        DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                        changePhoneNoDialog = new ChangePhoneNumberDialog(LoginActivity.this, phone_number_temp);
                        changePhoneNoDialog.show();
                    }
                }
            };
            sb.setSpan(clickableSpan, 44, 47, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 44, 47, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            updatephoneNo.setText(sb);
            updatephoneNo.setMovementMethod(LinkMovementMethod.getInstance());
            String newconnector_fr = this.getResources().getString(R.string.new_connector);
            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_fr);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 33, 36, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    if (!checkFields()) {
                        phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                        phone_number_temp = phoneNumberFormat + phonenNumber;
                        DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                        Intent startQrcodeIntent = new Intent(LoginActivity.this, QrCodeActivity.class);
                        startQrcodeIntent.putExtra("comingFromProfile", "1");
                        startActivity(startQrcodeIntent);
                    }
                }
            };
            sb2.setSpan(clickableSpan1, 33, 36, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 33, 36, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            updateConnector.setText(sb2);
            updateConnector.setMovementMethod(LinkMovementMethod.getInstance());
        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("nl")) {
            String newphoneno = this.getResources().getString(R.string.new_phoneNumber);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    if (!checkFields()) {
                        phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                        phone_number_temp = phoneNumberFormat + phonenNumber;
                        DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                        changePhoneNoDialog = new ChangePhoneNumberDialog(LoginActivity.this, phone_number_temp);
                        changePhoneNoDialog.show();
                    }
                }
            };
            sb.setSpan(clickableSpan, 21, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            updatephoneNo.setText(sb);
            updatephoneNo.setMovementMethod(LinkMovementMethod.getInstance());
            String newconnector_nl = this.getResources().getString(R.string.new_connector);
            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_nl);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 23, 27, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    if (!checkFields()) {
                        phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                        phone_number_temp = phoneNumberFormat + phonenNumber;
                        DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                        Intent startQrcodeIntent = new Intent(LoginActivity.this, QrCodeActivity.class);
                        startQrcodeIntent.putExtra("comingFromProfile", "1");
                        startActivity(startQrcodeIntent);
                    }
                }
            };
            sb2.setSpan(clickableSpan1, 23, 27, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 23, 27, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            updateConnector.setText(sb2);
            updateConnector.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
            String newphoneno_en = "New phone number? Update here";
            Log.e("newphoneno_en", newphoneno_en);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_en);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 25, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {

                    if (!checkFields()) {
                        phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                        phone_number_temp = phoneNumberFormat + phonenNumber;
                        DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                        changePhoneNoDialog = new ChangePhoneNumberDialog(LoginActivity.this, phone_number_temp);
                        changePhoneNoDialog.show();
                    }

                }
            };
            sb.setSpan(clickableSpan, 25, 29, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 25, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            updatephoneNo.setText(sb);
            updatephoneNo.setMovementMethod(LinkMovementMethod.getInstance());
            String newconnector_en = "New DrivePad? Update here";

            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_en);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    if (!checkFields()) {
                        if (!checkFields()) {
                            phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");
                            phone_number_temp = phoneNumberFormat + phonenNumber;
                            DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, phone_number_temp);
                            Intent startQrcodeIntent = new Intent(LoginActivity.this, QrCodeActivity.class);
                            startQrcodeIntent.putExtra("comingFromProfile", "1");
                            startActivity(startQrcodeIntent);
                        }
                    }
                }
            };
            sb2.setSpan(clickableSpan1, 21, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            updateConnector.setText(sb2);
            updateConnector.setMovementMethod(LinkMovementMethod.getInstance());
        }


    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }

    // Checks the fields and request server for login
    private void LoginUser() {
        try {
            if (!checkFields()) {
                if (AppUtils.isNetworkAvailable()) {
                    progressBarDialogLogin = new ProgressBarDialog(LoginActivity.this);
                    progressBarDialogLogin.setTitle(getString(R.string.title_progress_dialog));
                    progressBarDialogLogin.setMessage(getString(R.string.body_progress_dialog));
                    progressBarDialogLogin.show();
                    correctPhoneNumber = phoneNumberFormat + phonenNumber;
                    DataHandler.updatePreferences(AppConstants.PHONE_NUMBER_TEMP, correctPhoneNumber);
                    final TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    String deviceId = telephonyManager.getDeviceId();
                    List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                    mParams.add(new BasicNameValuePair("phone_number", correctPhoneNumber));
                    mParams.add(new BasicNameValuePair("device_id", deviceId));
                    //add token from frirebase :
                    gcm_token = DataHandler.getStringPreferences(AppConstants.FIRE_BASE_TOKE);
                    //String version =   pInfo.versionName;
                    mParams.add(new BasicNameValuePair("gcm_token", gcm_token));
                    Log.e("PARAMS", "" + mParams);
                    DataHandler.updatePreferences(AppConstants.PHONE_NUMBER, correctPhoneNumber);
                    DataHandler.updatePreferences(AppConstants.PHONE_TEMP, correctPhoneNumber);
                    LoginAsyncTask loginAsyncTask = new LoginAsyncTask(LoginActivity.this, WebServiceConstants.END_POINT_LOGIN, mParams);
                    loginAsyncTask.execute();
                    isFinishing();
                } else {
                    AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(this);
                    error_No_Internet.setMessage(this.getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_No_Internet.show();
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }


    public boolean checkFields() {
        et_Phone.setError(null);
        boolean cancel = false;
        View focusView = null;
        phonenNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");

        if (TextUtils.isEmpty(phonenNumber)) {

            et_Phone.setError(getString(R.string.error_field_required));
            focusView = et_Phone;
            cancel = true;

        } else if (!AppUtils.isValidPhone(phonenNumber)) {
            et_Phone.setError(getString(R.string.error_invalid_mobilenumber));
            focusView = et_Phone;
            cancel = true;
        } else if (!AppUtils.isValidPhonemaxlength(phonenNumber)) {
            et_Phone.setError(getString(R.string.error_invalid_mobilenumber_length));
            focusView = et_Phone;
            cancel = true;
        }
        if (cancel) {

            focusView.requestFocus();

        }
        return cancel;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(LoginActivity.this, VerificationActivity.class));

    }
}
