package com.studio.barefoot.freeedrivebeacononlyapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.adapters.DropDownListAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.FetchProfileAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.ProfileAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ChangePhoneNumberDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.interfacess.IRegisterFD;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.UIUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileActivity extends AppCompatActivity implements IRegisterFD {
    EditText et_frstName, et_LastName, et_Email, et_Phone, et_Language;
    TextView tv_profile, tv_new_phn, tv_new_connector;
    View actionBarView;
    ImageView spinner;
    Button img_Update_Profile;
    ChangePhoneNumberDialog changePhoneNoDialog;
    public static ProgressBarDialog progressBarDialogProfile;
    private String selectedLang = "";
    SharedPreferences sharedpreferences;
    String updatephonenoandconnector;
    String email = "";

    public ProfileActivity() {
        LocaleUtils.updateConfig(this);
    }

    @SuppressLint("LongLogTag")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        // LocaleUtils.updateConfig(this);

        sharedpreferences = getSharedPreferences("AutoReply", ProfileActivity.this.MODE_PRIVATE);
        et_frstName = (EditText) findViewById(R.id.et_firstName);
        et_LastName = (EditText) findViewById(R.id.et_lastName);
        et_Email = (EditText) findViewById(R.id.et_Email);
        et_Phone = (EditText) findViewById(R.id.et_phone);
        tv_new_phn = (TextView) findViewById(R.id.tv_new_phn);
        tv_new_connector = (TextView) findViewById(R.id.tv_new_connector);
        tv_profile = (TextView) findViewById(R.id.hello);
        spinner = (ImageView) findViewById(R.id.language_Spinner);

        et_Language = (EditText) findViewById(R.id.et_language);
        img_Update_Profile = (Button) findViewById(R.id.img_Update_Profile);
        actionBarView = getLayoutInflater().inflate(R.layout.custom_toolbarr, null);
        et_Language = (EditText) findViewById(R.id.et_language);
        et_frstName.setTypeface(UIUtils.getInstance().getLightFont(this));
        et_LastName.setTypeface(UIUtils.getInstance().getLightFont(this));
        et_Email.setTypeface(UIUtils.getInstance().getLightFont(this));
        et_Phone.setTypeface(UIUtils.getInstance().getLightFont(this));
        et_Language.setTypeface(UIUtils.getInstance().getLightFont(this));
        tv_profile.setTypeface(UIUtils.getInstance().getMediumFont(this));
        tv_new_connector.setTypeface(UIUtils.getInstance().getLightFont(this));
        tv_new_phn.setTypeface(UIUtils.getInstance().getLightFont(this));
        et_Phone.setFocusable(false);

        et_frstName.setMaxLines(1);
        et_frstName.setSingleLine();

        et_LastName.setMaxLines(1);
        et_LastName.setSingleLine();

        et_Email.setMaxLines(1);
        et_Email.setSingleLine();
  /*      try{
            if (progressBarDialogProfile == null) {
                progressBarDialogProfile = new ProgressBarDialog(ProfileActivity.this);
            }
            progressBarDialogProfile.setTitle(getString(R.string.title_progress_dialog));
            progressBarDialogProfile.setMessage(getString(R.string.body_progress_dialog));
            progressBarDialogProfile.show();
        }catch (Exception e){
            e.printStackTrace();
        }*/

        updatephonenoandconnector = DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG);
        Log.e("updatephonenoandconnector", updatephonenoandconnector);
        if (updatephonenoandconnector != null && !updatephonenoandconnector.isEmpty()) {
            if (updatephonenoandconnector.equalsIgnoreCase("en")) {
                String newphoneno_en = this.getResources().getString(R.string.new_phoneNumber);
                Log.e("newphoneno_en", newphoneno_en);
                final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_en);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 25, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        changePhoneNoDialog = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();
                    }
                };
                sb.setSpan(clickableSpan, 25, 29, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 25, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                tv_new_phn.setText(sb);
                tv_new_phn.setMovementMethod(LinkMovementMethod.getInstance());
                String newconnector_en = this.getResources().getString(R.string.new_connector);

                final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_en);
                final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb2.setSpan(bss2, 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent startQrcodeIntent = new Intent(ProfileActivity.this, QrCodeActivity.class);
                        startQrcodeIntent.putExtra("comingFromProfile", "1");
                        startActivity(startQrcodeIntent);
                    }
                };
                sb2.setSpan(clickableSpan1, 21, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                tv_new_connector.setText(sb2);
                tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());


            } else if (updatephonenoandconnector.equalsIgnoreCase("es")) {
                String newphoneno_es = this.getResources().getString(R.string.new_phoneNumber);
                Log.e("newphoneno_es", newphoneno_es);
                final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_es);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 36, 40, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        changePhoneNoDialog = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();
                    }
                };
                sb.setSpan(clickableSpan, 36, 40, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 36, 40, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                tv_new_phn.setText(sb);
                tv_new_phn.setMovementMethod(LinkMovementMethod.getInstance());
                String newconnector_es = this.getResources().getString(R.string.new_connector);

                final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_es);
                final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb2.setSpan(bss2, 26, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent startQrcodeIntent = new Intent(ProfileActivity.this, QrCodeActivity.class);
                        startQrcodeIntent.putExtra("comingFromProfile", "1");
                        startActivity(startQrcodeIntent);
                    }
                };
                sb2.setSpan(clickableSpan1, 26, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 26, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                tv_new_connector.setText(sb2);
                tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (updatephonenoandconnector.equalsIgnoreCase("fr")) {
                String newphoneno_fr = this.getResources().getString(R.string.new_phoneNumber);
                Log.e("newphoneno_fr", newphoneno_fr);
                final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_fr);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 44, 47, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        changePhoneNoDialog = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();
                    }
                };
                sb.setSpan(clickableSpan, 44, 47, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 44, 47, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                tv_new_phn.setText(sb);
                tv_new_phn.setMovementMethod(LinkMovementMethod.getInstance());
                String newconnector_fr = this.getResources().getString(R.string.new_connector);
                final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_fr);
                final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb2.setSpan(bss2, 33, 36, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent startQrcodeIntent = new Intent(ProfileActivity.this, QrCodeActivity.class);
                        startQrcodeIntent.putExtra("comingFromProfile", "1");
                        startActivity(startQrcodeIntent);
                    }
                };
                sb2.setSpan(clickableSpan1, 33, 36, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 33, 36, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                tv_new_connector.setText(sb2);
                tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (updatephonenoandconnector.equalsIgnoreCase("nl")) {
                String newphoneno = this.getResources().getString(R.string.new_phoneNumber);
                final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        changePhoneNoDialog = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();
                    }
                };
                sb.setSpan(clickableSpan, 21, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                tv_new_phn.setText(sb);
                tv_new_phn.setMovementMethod(LinkMovementMethod.getInstance());
                String newconnector_nl = this.getResources().getString(R.string.new_connector);
                final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_nl);
                final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb2.setSpan(bss2, 23, 27, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent startQrcodeIntent = new Intent(ProfileActivity.this, QrCodeActivity.class);
                        startQrcodeIntent.putExtra("comingFromProfile", "1");
                        startActivity(startQrcodeIntent);
                    }
                };
                sb2.setSpan(clickableSpan1, 23, 27, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 23, 27, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                tv_new_connector.setText(sb2);
                tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());
            }


        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("en")) {
            String newphoneno_en = this.getResources().getString(R.string.new_phoneNumber);
            Log.e("newphoneno_en", newphoneno_en);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_en);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 25, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    changePhoneNoDialog = new ChangePhoneNumberDialog(ProfileActivity.this);
                    changePhoneNoDialog.show();
                }
            };
            sb.setSpan(clickableSpan, 25, 29, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 25, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_phn.setText(sb);
            tv_new_phn.setMovementMethod(LinkMovementMethod.getInstance());
            String newconnector_en = this.getResources().getString(R.string.new_connector);

            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_en);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent startQrcodeIntent = new Intent(ProfileActivity.this, QrCodeActivity.class);
                    startQrcodeIntent.putExtra("comingFromProfile", "1");
                    startActivity(startQrcodeIntent);
                }
            };
            sb2.setSpan(clickableSpan1, 21, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_connector.setText(sb2);
            tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());
        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("es")) {
            String newphoneno_es = this.getResources().getString(R.string.new_phoneNumber);
            Log.e("newphoneno_es", newphoneno_es);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_es);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 36, 40, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    changePhoneNoDialog = new ChangePhoneNumberDialog(ProfileActivity.this);
                    changePhoneNoDialog.show();
                }
            };
            sb.setSpan(clickableSpan, 36, 40, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 36, 40, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_phn.setText(sb);
            tv_new_phn.setMovementMethod(LinkMovementMethod.getInstance());
            String newconnector_es = this.getResources().getString(R.string.new_connector);

            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_es);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 26, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent startQrcodeIntent = new Intent(ProfileActivity.this, QrCodeActivity.class);
                    startQrcodeIntent.putExtra("comingFromProfile", "1");
                    startActivity(startQrcodeIntent);
                }
            };
            sb2.setSpan(clickableSpan1, 26, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 26, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_connector.setText(sb2);
            tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());

        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("fr")) {
            String newphoneno_fr = this.getResources().getString(R.string.new_phoneNumber);
            Log.e("newphoneno_fr", newphoneno_fr);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_fr);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 44, 47, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    changePhoneNoDialog = new ChangePhoneNumberDialog(ProfileActivity.this);
                    changePhoneNoDialog.show();
                }
            };
            sb.setSpan(clickableSpan, 44, 47, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 44, 47, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_phn.setText(sb);
            tv_new_phn.setMovementMethod(LinkMovementMethod.getInstance());
            String newconnector_fr = this.getResources().getString(R.string.new_connector);
            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_fr);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 33, 36, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent startQrcodeIntent = new Intent(ProfileActivity.this, QrCodeActivity.class);
                    startQrcodeIntent.putExtra("comingFromProfile", "1");
                    startActivity(startQrcodeIntent);
                }
            };
            sb2.setSpan(clickableSpan1, 33, 36, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 33, 36, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_connector.setText(sb2);
            tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());
        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("nl")) {
            String newphoneno = this.getResources().getString(R.string.new_phoneNumber);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    changePhoneNoDialog = new ChangePhoneNumberDialog(ProfileActivity.this);
                    changePhoneNoDialog.show();
                }
            };
            sb.setSpan(clickableSpan, 21, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_phn.setText(sb);
            tv_new_phn.setMovementMethod(LinkMovementMethod.getInstance());
            String newconnector_nl = this.getResources().getString(R.string.new_connector);
            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_nl);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 23, 27, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent startQrcodeIntent = new Intent(ProfileActivity.this, QrCodeActivity.class);
                    startQrcodeIntent.putExtra("comingFromProfile", "1");
                    startActivity(startQrcodeIntent);
                }
            };
            sb2.setSpan(clickableSpan1, 23, 27, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 23, 27, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_connector.setText(sb2);
            tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
            String newphoneno_en = "New phone number? Update here";
            Log.e("newphoneno_en", newphoneno_en);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_en);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 25, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    changePhoneNoDialog = new ChangePhoneNumberDialog(ProfileActivity.this);
                    changePhoneNoDialog.show();
                }
            };
            sb.setSpan(clickableSpan, 25, 29, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 25, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_phn.setText(sb);
            tv_new_phn.setMovementMethod(LinkMovementMethod.getInstance());
            String newconnector_en = "New DrivePad? Update here";

            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_en);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent startQrcodeIntent = new Intent(ProfileActivity.this, QrCodeActivity.class);
                    startQrcodeIntent.putExtra("comingFromProfile", "1");
                    startActivity(startQrcodeIntent);
                }
            };
            sb2.setSpan(clickableSpan1, 21, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_connector.setText(sb2);
            tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());
        }

        setupActionBar();
        setupProfile();

        spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spawnPopUp(et_Language);
            }
        });


        img_Update_Profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (!checkFields()) {
                        if (AppUtils.isNetworkAvailable()) {
                            if (progressBarDialogProfile == null) {
                                progressBarDialogProfile = new ProgressBarDialog(ProfileActivity.this);
                            }
                            progressBarDialogProfile.setTitle(getString(R.string.title_progress_dialog));
                            progressBarDialogProfile.setMessage(getString(R.string.body_progress_dialog));
                            progressBarDialogProfile.show();
                            String lang = "";
                            if (et_Language.getText().toString().equals(getResources().getString(R.string.english)))
                                lang = "en";
                            else if (et_Language.getText().toString().equals(getResources().getString(R.string.french)))
                                lang = "fr";
                            else if (et_Language.getText().toString().equals(getResources().getString(R.string.spanish)))
                                lang = "es";
                            else if (et_Language.getText().toString().equals(getResources().getString(R.string.dutch)))
                                lang = "nl";
                            else {
                                String jsonObjProfile = DataHandler.getStringPreferences(AppConstants.TEMP_PROFILE_KEY);

                                if (jsonObjProfile != null && !jsonObjProfile.isEmpty()) {

                                    try {
                                        JSONObject jsonObject = new JSONObject(jsonObjProfile);
                                        lang = jsonObject.getString("lang");
                                    } catch (Exception exception) {
                                        exception.printStackTrace();
                                    }

                                }
                            }

                            JSONObject jsonObject = new JSONObject();
                            if (jsonObject != null) {

                                try {
                                    jsonObject.put("first_name", et_frstName.getText().toString().trim());
                                    jsonObject.put("last_name", et_LastName.getText().toString().trim());
                                    jsonObject.put("email", et_Email.getText().toString().trim().toLowerCase());
                                    jsonObject.put("lang", lang);

                                    DataHandler.updatePreferences(AppConstants.TEMP_PROFILE_KEY, jsonObject.toString());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            ProfileAsyncTask profileAsyncTask = new ProfileAsyncTask(ProfileActivity.this, ProfileActivity.this, et_frstName.getText().toString(), et_LastName.getText().toString(), et_Email.getText().toString().trim().toLowerCase(), lang);
                            profileAsyncTask.execute();


                        } else {
                            AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(ProfileActivity.this);
                            error_No_Internet.setMessage(ProfileActivity.this.getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            error_No_Internet.show();
                        }

                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }


            }
        });

    }

    private void spawnPopUp(View layout1) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.pop_up_window, (ViewGroup) findViewById(R.id.popup_layout));
        int dp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, getResources().getDisplayMetrics());
        final PopupWindow popup = new PopupWindow(layout, dp, RelativeLayout.LayoutParams.WRAP_CONTENT, true);
        popup.setTouchable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.setOutsideTouchable(true);
        popup.setWidth(900);
        popup.setHeight(RelativeLayout.LayoutParams.WRAP_CONTENT);
        popup.setTouchInterceptor(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popup.dismiss();
                    return true;
                }
                return false;
            }
        });
        popup.setContentView(layout);
        popup.showAsDropDown(layout1);


        //populate the drop-down list
        final List<String> languages = new LinkedList<String>();
        languages.add(getResources().getString(R.string.english));
        languages.add(getResources().getString(R.string.french));
        languages.add(getResources().getString(R.string.dutch));
        languages.add(getResources().getString(R.string.spanish));
        final ListView list = (ListView) layout.findViewById(R.id.list);
        DropDownListAdapter adapter = new DropDownListAdapter(ProfileActivity.this, languages);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedLang = languages.get(position).toString();
                popup.dismiss();
                et_Language.setText(selectedLang);
                if (selectedLang.equals(getResources().getString(R.string.english))) {
                    selectedLang = "en";
                } else if (selectedLang.equals(getResources().getString(R.string.french))) {
                    selectedLang = "fr";
                } else if (selectedLang.equals(getResources().getString(R.string.dutch))) {
                    selectedLang = "nl";
                } else if (selectedLang.equals(getResources().getString(R.string.spanish)))
                    selectedLang = "es";
                else {
                    selectedLang = "en";
                }


            }
        });
    }

    private void restartActivity() {
        Intent intent = new Intent(ProfileActivity.this, ProfileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(intent);
    }

    public boolean checkFields() {
        et_frstName.setError(null);
        et_LastName.setError(null);
        et_Email.setError(null);
        et_Phone.setError(null);
        et_Language.setError(null);
        boolean cancel = false;
        View focusView = null;
        // email= et_Email.getText().toString().trim().replaceAll("\\s++$", "");
        // email= et_Email.getText().toString().trim().replaceAll("^\\s+|\\s+$", "");
        if (TextUtils.isEmpty(et_frstName.getText().toString().trim())) {

            et_frstName.setError(getString(R.string.error_field_required));
            focusView = et_frstName;
            cancel = true;

        } else if (TextUtils.isEmpty(et_LastName.getText().toString().trim())) {

            et_LastName.setError(getString(R.string.error_field_required));
            focusView = et_LastName;
            cancel = true;
        } else if (TextUtils.isEmpty(et_Email.getText().toString().trim())) {

            et_Email.setError(getString(R.string.error_field_required));
            focusView = et_Email;
            cancel = true;
        } else if (!AppUtils.emailValidator(et_Email.getText().toString().trim())) {

            et_Email.setError(getString(R.string.valid_email));
            focusView = et_Email;
            cancel = true;
        } else if (TextUtils.isEmpty(et_Phone.getText().toString().trim())) {

            et_Phone.setError(getString(R.string.error_field_required));
            focusView = et_Phone;
            cancel = true;
        } else if (TextUtils.isEmpty(et_Language.getText().toString())) {

            et_Language.setError(getString(R.string.error_field_required));
            focusView = et_Language;
            cancel = true;
        }
        if (cancel) {

            focusView.requestFocus();

        }
        return cancel;
    }

    private void setupProfile() {
        String jsonObjProfile = DataHandler.getStringPreferences(AppConstants.TEMP_PROFILE_KEY);
        Log.e("jsonObjProfile :", jsonObjProfile);
        if (jsonObjProfile != null && !jsonObjProfile.isEmpty()) {
            try {
                JSONObject jsonObject = new JSONObject(jsonObjProfile);
                et_frstName.setText(jsonObject.getString("first_name"));
                et_LastName.setText(jsonObject.getString("last_name"));
                et_Email.setText(jsonObject.getString("email").trim());
                et_Phone.setText(DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER));


                String lang = "";
                if (jsonObject.getString("lang").equals("en"))
                    lang = "English";
                else if (jsonObject.getString("lang").equals("fr"))
                    lang = "Français";
                else if (jsonObject.getString("lang").equalsIgnoreCase("nl"))
                    lang = "Dutch";
                else if (jsonObject.getString("lang").equalsIgnoreCase("es"))
                    lang = "Español";
                else lang = "English";
                et_Language.setText(lang);
            /*     String lang = "";
                 if (jsonObject.getString("language_id").equals("1"))
                     lang = "English";
                 else if (jsonObject.getString("language_id").equals("2"))
                     lang = "French";
                 else if(jsonObject.getString("language_id").equals("3"))
                     lang = "Dutch";
                 else lang="English";
                 et_Language.setText(lang);
                 */
          /*      try{
                    progressBarDialogProfile.dismiss();
                    progressBarDialogProfile = null;
                }catch (NullPointerException exception){
                    exception.printStackTrace();
                }*/

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            et_Phone.setText(DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER));

        } else {
            if (AppUtils.isNetworkAvailable()) {

                fetchUserData();
            } else {
                AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(ProfileActivity.this);
                error_No_Internet.setMessage(ProfileActivity.this.getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                error_No_Internet.show();
            /*    try{
                    progressBarDialogProfile.dismiss();
                    progressBarDialogProfile = null;
                }catch (NullPointerException exception){
                    exception.printStackTrace();
                }*/
            }
        }
    }

    public void fetchUserData() {

        try {
            if (progressBarDialogProfile == null) {
                progressBarDialogProfile = new ProgressBarDialog(ProfileActivity.this);
            }
            progressBarDialogProfile.setTitle(getString(R.string.title_progress_dialog));
            progressBarDialogProfile.setMessage(getString(R.string.body_progress_dialog));
            progressBarDialogProfile.show();
            List<NameValuePair> mParams = new ArrayList<NameValuePair>();
            mParams.add(new BasicNameValuePair("phone_number", DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
            Log.e("mParams", mParams.toString());
            FetchProfileAsyncTask fetchProfileAsyncTask = new FetchProfileAsyncTask(ProfileActivity.this, WebServiceConstants.END_POINT_FETCH_PROFILE, mParams);
            fetchProfileAsyncTask.execute();
        } catch (NullPointerException exception) {

        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }


    @Override
    public void manageRegister(String response, String r) {
        if (response != null && response.equals("updated")) {
            try {
                JSONObject jsonObject = new JSONObject(r);
                if (jsonObject != null) {
                    String accounto = DataHandler.getStringPreferences(AppConstants.TEMP_PROFILE_KEY);
                    JSONObject acc = new JSONObject(accounto);
                    acc.put("email", jsonObject.getString("email").trim());
                    acc.put("first_name", jsonObject.getString("first_name"));
                    acc.put("last_name", jsonObject.getString("last_name"));
                    acc.put("lang", jsonObject.getString("lang"));
                    acc.put("phone_number", jsonObject.getString("phone_number"));
//                   acc.put("total_bad_counts",jsonObject.get("total_bad_counts"));
//                   acc.put("total_ride_time",jsonObject.get("total_time_elapsed"));

                    DataHandler.updatePreferences(AppConstants.TEMP_PROFILE_KEY, acc.toString());
                    AlertDialog.Builder reg = new AlertDialog.Builder(this);
                    reg.setMessage(getResources().getString(R.string.profile_updated)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                            String jsonObjProfile = DataHandler.getStringPreferences(AppConstants.TEMP_PROFILE_KEY);

                            if (jsonObjProfile != null && !jsonObjProfile.isEmpty()) {

                                try {
                                    JSONObject jsonObject = new JSONObject(jsonObjProfile);
                                    String appLang = jsonObject.getString("lang");
                                    Log.e("appLang", appLang);
                                    selectedLang = jsonObject.getString("lang");
                                } catch (Exception exception) {
                                    exception.printStackTrace();
                                }

                            }
                            ApplicationController controller = (ApplicationController) getApplication();
                            controller.updateLocale(selectedLang);
                            Intent intent = new Intent(ProfileActivity.this, MenuActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            DataHandler.updatePreferences(AppConstants.PREF_KEY_LANG, selectedLang);
                            //   setupProfile();
                        }
                    });
                    reg.show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ProfileActivity.this, MenuActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        finish();
    }
}
