package com.studio.barefoot.freeedrivebeacononlyapp.utils;

import com.studio.barefoot.freeedrivebeacononlyapp.beans.SubRideBeans;

import java.util.ArrayList;

/**
 * Created by mcs on 12/21/2016.
 */

public class AppConstants {
    public static final String USER_ACCEPTED_PRIVACY ="USER_ACCEPTED_PRIVACY";
    public static final String USER_AWAITING_QR_CODE ="USER_AWAITING_QR_CODE";
    public static final String USER_AWAITING_SMS ="USER_AWAITING_SMS";
    public static final String RIDE_WAS_ACTIVE ="RIDE_WAS_ACTIVE";
    public static final String KEY_ARRIVAL ="KEY_ARRIVAL";
    public static final String KEY_DEPARTURE ="KEY_DEPARTURE";
    public static final String REDUCTED_ARRIVAL ="REDUCTED_ARRIVAL";
    public static final String KEY_AUTOREPLY ="KEY_AUTOREPLY";
    public static final String KEY_AUTOREPLY_MSG ="KEY_AUTOREPLY_MSG";
    public static final String ARRIVAL_TIME ="ARRIVAL_TIME";
    public static final String INSURANCE_POLICY_KEY ="INSURANCE_POLICY_KEY";
    public static final String GRAPH_PHONE_SAFETY_AVG = "GRAPH_PHONE_SAFETY_AVG";
    public static final String GRAPH_DRIVING_PERFORMANCE_AVG = "GRAPH_DRIVING_PERFORMANCE_AVG";
    public static final String TEMP_PROFILE_KEY ="TEMP_PROFILE_KEY";
    public static final String TEMP_DISPLAY_KEY ="TEMP_DISPLAY_KEY";
    //forscoring purposes
    public static final String SCORE_TOTAL_BAD_COUNTS ="SCORE_TOTAL_BAD_COUNTS";
    public static final String SCORE_TOTAL_ELAPSED_TIME ="SCORE_TOTAL_ELAPSED_TIME";
    public static final String SCORE_TOTAL_SCORE ="SCORE_TOTAL_SCORE";

    public static final String SCORE_TIME_KEY = "SCORE_TIME_KEY";
    public static final String FIRST_TIME_USER ="FIRST_TIME_USER_PREF_KEY";
    public static final String PREF_KEY_LANG ="PREF_KEY_LANG";
    public static final String PREF_KEY_DRIVING_PERFORMANCE_ACTIVE ="PREF_KEY_DRIVING_PERFORMANCE_ACTIVE";
    public static final String PREF_KEY_DRIVING_PERFORMANCE_SCORE ="PREF_KEY_DRIVING_PERFORMANCE_SCORE";
    public static final String FIRE_BASE_TOKE ="FIRE_BASE_TOKE";
    public static final String PREF_KEY_LOGIN_ACTIVITY ="IsUserLoggedIn";
    public static final String KEY_REBOOT_DEVICE ="deviceRebooted";
    public static final String PREF_KEY_FD_STATUS = "PREF_KEY_FD_STATUS";
    /*public static final String TITLE_LOCATION_SERVICE ="Location Check ";
    public static final String BODY_LOCATION_SERVICE_ON ="Location Service ON!";
    public static final String BODY_LOCATION_SERVICE_OFF = "Location Service OFF";*/


    public static final String PHONE_NUMBER ="PHONE_NUMBER";
    public static final String PHONE_TEMP ="PHONE_TEMP";
    public static final String TOKEN_NUMBER ="TOKEN";
    public static final String PHONE_NUMBER_TEMP = "PHONE_NUMBER_TEMP";
    public static final String UUID ="UUID";
    public static final String TEMP_UUID = "TEMP_UUID";
    public static final String TEMP_UUID_NAME_SPACE="TEMP_UUID_NAME_SPACE";
    public static final String TEMP_UUID_INSTANCE_ID = "TEMP_UUID_INSTANCE_ID";
    public static final String UUID_iBEACON ="UUID_iBEACON";
    public static final String UUID_NAME_SPACE ="UUID_NAME_SPACE";
    public static final String UUID_INSTANCE_ID ="UUID_INSTANCE_ID";
    public static final String IBKS_SERIAL_NO ="IBKS_SERIAL_NO";
    public static final String TEMP_IBKS_SERIAL_NO="TEMP_IBKS_SERIAL_NO";
    public static final String ACCESS_PRIVATE_HOURS_UPDATED = "ACCESS_PRIVATE_HOURS_UPDATED";
    public static final String ACCESS_PRIVATE_HOURS = "ACCESS_PRIVATE_HOURS";
    public static  Boolean isLocationActive =false;

    public static final long TIMEOUT_CONNECT = 20;
    public static final long TIMEOUT_CONNECT_VIDEO = 5;
    public static final long READ_TIMEOUT = 180;
    public static int ORPHAN_RIDE_BIT = 0;
   // public final static String REG_EMIAL = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public final static String REG_EMIAL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";;

    public static ArrayList<SubRideBeans> subRideBeansArrayList = new ArrayList<>();
}
