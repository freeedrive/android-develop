package com.studio.barefoot.freeedrivebeacononlyapp;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesFragment;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;

import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.fdLogout;

public class MainActivity extends AppCompatActivity {
    private ImageView splashScreen;
    private android.support.v4.app.FragmentManager fragmentManager;

    public MainActivity() {
        LocaleUtils.updateConfig(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        splashScreen = (ImageView) findViewById(R.id.img_splashScreen);
        splashScreen.setVisibility(View.VISIBLE);
        Message msg = new Message();
        msg.what = -1;
        guiHandler.sendMessageDelayed(msg, 2000);
    }

    // Handler sets the navigation of which activity to show at which time
    private Handler guiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == -1) {
                if (DataHandler.getIntPreferences(AppConstants.FIRST_TIME_USER) == 1) {

                    DataHandler.updatePreferences(AppConstants.FIRST_TIME_USER, 2);
                    if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY)) {
                        startActivity(new Intent(MainActivity.this, MenuActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();
                    } else if (DataHandler.getBooleanPreferences(AppConstants.USER_AWAITING_SMS)) {
                        startActivity(new Intent(MainActivity.this, SmsConfirmationActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();
                    } else if (DataHandler.getBooleanPreferences(AppConstants.USER_AWAITING_QR_CODE)) {
                        startActivity(new Intent(MainActivity.this, QrCodeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();
                    } else {
                        startActivity(new Intent(MainActivity.this, VerificationActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();
                    }
/*}
                        if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY)){
                            startActivity(new Intent(MainActivity.this,MenuActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            finish();
                        }else{
                            startActivity(new Intent(MainActivity.this,VerificationActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            finish();
                        }

                        */


                } else if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY)) {
                    startActivity(new Intent(MainActivity.this, MenuActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();
                } else if (DataHandler.getBooleanPreferences(AppConstants.USER_AWAITING_SMS)) {
                    startActivity(new Intent(MainActivity.this, SmsConfirmationActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();
                } else if (DataHandler.getBooleanPreferences(AppConstants.USER_AWAITING_QR_CODE)) {
                    startActivity(new Intent(MainActivity.this, QrCodeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();
                } else if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY) == false) {
                    fdLogout(getApplicationContext());
                }
                    /*else {
                        startActivity(new Intent(MainActivity.this,MenuActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();
                    }*/
            }

        }
    };
}
