package com.studio.barefoot.freeedrivebeacononlyapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CreditsActivity extends AppCompatActivity {
     TextView fd_Version,fd_Web;

    public CreditsActivity()
    {
        LocaleUtils.updateConfig(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credits);
        fd_Version = (TextView) findViewById(R.id.fd_Version);
        fd_Web = (TextView) findViewById(R.id.fd_web);

        setupActionBar();


        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;
        fd_Web.setPaintFlags(fd_Web.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        fd_Version.setText("Version: 1.4.2");
        fd_Web.setText("www.freeedrive.com");

        fd_Web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com"));
                startActivity(intentBrowser);
            }
        });
    }
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }

}
