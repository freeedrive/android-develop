package com.studio.barefoot.freeedrivebeacononlyapp.beans;

/**
 * Created by mcs on 12/23/2016.
 */

public class NotificationBeans {

    String dateTime;
    String title;
    String body;
    int messagesID;
    boolean isCheckBoxVisibility;
    int is_Deleted ;


    public NotificationBeans() {
        super();
    }

    public int getIs_Deleted() {
        return is_Deleted;
    }

    public void setIs_Deleted(int is_Deleted) {
        this.is_Deleted = is_Deleted;
    }

    public boolean isCheckBoxVisibility() {
        return isCheckBoxVisibility;
    }

    public void setCheckBoxVisibility(boolean checkBoxVisibility) {
        isCheckBoxVisibility = checkBoxVisibility;
    }

    public int getMessagesID() {
        return messagesID;
    }

    public void setMessagesID(int messagesID) {
        this.messagesID = messagesID;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
