package com.studio.barefoot.freeedrivebeacononlyapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.amitshekhar.DebugDB;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.FetchInsuranceDetails;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class InsuranceActivity extends AppCompatActivity {
    public static ProgressBarDialog progressBarInsurance;
    TextView insuranceNo,phone,email,steps,fleet_message;
    ImageView insuranceLogo;
    public InsuranceActivity()
    {
        LocaleUtils.updateConfig(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insurance);

        insuranceNo = (TextView) findViewById(R.id.isurance_policy_number);
        phone = (TextView) findViewById(R.id.phone_number);
        email = (TextView) findViewById(R.id.email);
        steps = (TextView) findViewById(R.id.damages_list);
        fleet_message = (TextView) findViewById(R.id.fleet_message);
        insuranceLogo = (ImageView) findViewById(R.id.insurance_image);
        String DbLog = DebugDB.getAddressLog().toString();

        Log.e("DbLog",DbLog);

        if (getIntent().getStringExtra("comingFromNetwork")!=null&&getIntent().getStringExtra("comingFromNetwork").equalsIgnoreCase("1")) {

            if (!DataHandler.getStringPreferences(AppConstants.INSURANCE_POLICY_KEY).isEmpty()) {
                String dispValue = DataHandler.getStringPreferences(AppConstants.INSURANCE_POLICY_KEY);
                try {
                    JSONObject jsonObj = new JSONObject(dispValue);
                    if (jsonObj != null) {



                        String image_path = jsonObj.getString("image").trim();
                        if (!image_path.isEmpty() && !image_path.equals("")) {
                          // Picasso.with(this).load("https://fd-development.frogeek.com/api/resources/1").into(insuranceLogo);
                            String compressedImage = jsonObj.getString("blobImage");
                            insuranceLogo.setImageBitmap(getImageIcon(compressedImage));
                        }
                        String insuranceNos = jsonObj.getString("policy_number").trim();
                        if (!insuranceNos.isEmpty() && !insuranceNos.equals("")) {
                            insuranceNo.setText(jsonObj.getString("policy_heading")+" "+insuranceNos);
                        }

                        String phoneN = jsonObj.getString("phone_number").trim();
                        if (!phoneN.isEmpty() && !phoneN.equals("")) {
                            phone.setText(phoneN);

                        }

                        String emails = jsonObj.getString("email").trim();
                        if (!emails.isEmpty() && !emails.equals("")) {
                            email.setText(emails);
                        }

                        String stepss = jsonObj.getString("damage_case_step").trim();
                        if (!stepss.isEmpty() && !stepss.equals("")) {
                            steps.setText(stepss);

                        }

                        String fleetmessages = jsonObj.getString("fleet_message").trim();
                        if (!fleetmessages.isEmpty() && !fleetmessages.equals("")) {

                            fleet_message.setText(fleetmessages);
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }else if(AppUtils.isNetworkAvailable()){
                FetchInsurance();

            }
            else if (!DataHandler.getStringPreferences(AppConstants.INSURANCE_POLICY_KEY).isEmpty()) {
                    String dispValue = DataHandler.getStringPreferences(AppConstants.INSURANCE_POLICY_KEY);
                    try {
                        JSONObject jsonObj = new JSONObject(dispValue);
                        if (jsonObj != null) {

                            String image_path = jsonObj.getString("image").trim();
                            if (!image_path.isEmpty() && !image_path.equals("")) {
                                // Picasso.with(this).load("https://fd-development.frogeek.com/api/resources/1").into(insuranceLogo);
                                String compressedImage = jsonObj.getString("blobImage");
                                insuranceLogo.setImageBitmap(getImageIcon(compressedImage));
                            }

                            String insuranceNos = jsonObj.getString("policy_number").trim();
                            if (!insuranceNos.isEmpty() && !insuranceNos.equals("")) {
                                insuranceNo.setText(jsonObj.getString("policy_heading")+" "+insuranceNos);
                            }

                            String phoneN = jsonObj.getString("phone_number").trim();
                            if (!phoneN.isEmpty() && !phoneN.equals("")) {
                                phone.setText(phoneN);

                            }

                            String emails = jsonObj.getString("email").trim();
                            if (!emails.isEmpty() && !emails.equals("")) {
                                email.setText(emails);
                            }

                            String stepss = jsonObj.getString("damage_case_step").trim();
                            if (!stepss.isEmpty() && !stepss.equals("")) {
                                steps.setText(stepss);

                            }

                            String fleetmessages = jsonObj.getString("fleet_message").trim();
                            if (!fleetmessages.isEmpty() && !fleetmessages.equals("")) {

                                fleet_message.setText(fleetmessages);
                            }

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                 else {
                AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(this);
                error_No_Internet.setMessage(this.getResources().getString(R.string.error_No_Internet)).setPositiveButton(this.getResources().getString(R.string.string_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                error_No_Internet.show();
            }
        setupActionBar();

        }


    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }



    private void FetchInsurance() {

        Log.e("TimeZone",TimeZone.getDefault().getID());
        Log.e("TimeZonegetDSTSavings",""+TimeZone.getDefault().getDSTSavings());
        TimeZone t1z = TimeZone.getDefault();
        Log.e("t1z","="+t1z.getDisplayName(false,TimeZone.LONG)+"...."+t1z.getDisplayName(false,TimeZone.SHORT));

        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        Log.e("TimeZOneCal","="+tz.getDisplayName());

                progressBarInsurance = new ProgressBarDialog(InsuranceActivity.this);
            progressBarInsurance.setTitle(getString(R.string.title_progress_dialog));
            progressBarInsurance.setMessage(getString(R.string.body_progress_dialog));
            progressBarInsurance.show();
            List<NameValuePair> mParams = new ArrayList<NameValuePair>();
            mParams.add(new BasicNameValuePair("phone_number", DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
            FetchInsuranceDetails fetchInsuranceDetails = new FetchInsuranceDetails(this, WebServiceConstants.END_POINT_FETCH_INSURANCE, mParams);
            fetchInsuranceDetails.execute();

    }
    public Bitmap getImageIcon(String image_icon_data) {
        if(image_icon_data!=null) {
            byte[] image_data = Base64.decode(image_icon_data, Base64.NO_WRAP);

            BitmapFactory.Options options = new BitmapFactory.Options();
           /* options.outHeight = 32; //32 pixles
            options.outWidth = 32;*/ //32 pixles
            options.outMimeType = "jpeg"; //this could be image/jpeg, image/png, etc

            return BitmapFactory.decodeByteArray(image_data, 0, image_data.length, options);
        }
        return null;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(InsuranceActivity.this,MenuActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        finish();
    }
}
