package com.studio.barefoot.freeedrivebeacononlyapp.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;

/**
 * Created by mcs on 12/27/2016.
 */

public class MyBaseSQLite extends SQLiteOpenHelper {

    /**
     * Represents an activity
     */
    private Context context;

    /**
     * Reepresents the table name
     */
    private static final String TABLE_RIDES = "table_rides";
    /**
     * Represents the column ID
     */
    private static final String COL_ID = "ID";
    /**
     * Represents the column departure time
     */
    private static final String COL_DEPARTURE_TIME = "DEPARTURE_TIME";
    /**
     * Represents the column arrival time
     */
    private static final String COL_ARRIVAL_TIME = "ARRIVAL_TIME";
    /**
     * Represents the column departure location
     */
    private static final String COL_DEPARTURE_LOCATION = "DEPARTURE_LOCATION";
    /**
     * Represents the column arrival location
     */
    private static final String COL_ARRIVAL_LOCATION = "ARRIVAL_LOCATION";
    /**
     * Represents the column elapsed time
     */
    private static final String COL_TIME_ELAPSED = "TIME_ELAPSED";
    /**
     * Represents the column score
     */
    private static final String COL_SCORE = "SCORE";

    // Represent the total bad behaviour column
    private static final String COL_BAD_BEHAVIOUR = "COUNT_BAD_BEHAVIOUR";
    /**
     * Represents the column driver id
     */
    private static final String COL_DRIVER_ID = "DRIVE_ID";
    /**
     * Represents the column company id
     */
    private static final String COL_COMPANY_ID = "COMPANY_ID";
    /**
     * Represents the columnsend
     */
    private static final String COL_SEND = "SEND";
    /**
     * Represents the column time in string
     */
    private static final String COL_TIME = "TIME";
    /**
     * Represents the column avg time
     */
    private static final String COL_AVG_TIME = "AVG_TIME";
    /**
     * Represents the column total time in seoncdes
     */
    private static final String COL_TIME_TOTAL_SECONDES = "TIME_TOTAL_SECONDES";

    /**
     * Represents the column COL_TIME_ABOVE_120 time
     */
    private static final String COL_TIME_ABOVE_120 = "COL_TIME_ABOVE_120";

    /**
     * Represent the column COL_CAUSE_OF_ENDING_RIDE
     */
    private static final String COL_CAUSE_OF_ENDING_RIDE = "COL_CAUSE_OF_ENDING_RIDE";

    private static final String COL_SUDDEN_ACCELERATION = "COL_SUDDEN_ACCELERATION";
    private static final String COL_SUDDEN_BRAKING = "COL_SUDDEN_BRAKING";
    private static final String COL_SUDDEN_SPEEDING = "COL_OVER_SPEEDING";
    private static final String COL_IS_SPEEDING_UPDATED = "COL_IS_OVER_SPEEDING_UPDATED";
    private static final String COL_TIME_INSIDE_A_REGION = "TIME_INSIDE_A_REGION";
    /**
     * Represents the column COL_DISTANCE_TRAVELLED
     */
    private static final String COL_DISTANCE_TRAVELLED = "DISTANCE_TRAVELLED";

    /**
     * Represent the Orphan Ride Column Bit
     */
    private static final String COL_ORPHAN_RIDE = "COL_ORPHAN_RIDE";

    /**
     * Represents the table name
     * For displaying offline messages
     */
    private static final String TABLE_MESSAGES = "table_messages";
    private static final String COL_MESSAGES_ID = "ID";
    private static final String COL_REMOTE_MESSAGES_ID = "REMOTE_MESSAGES_ID";
    private static final String COL_MESSAGES_DATE_TIME = "SCHEDULED_AT";
    private static final String COL_MESSAGES_TITLE = "MESSAGES_TITLE ";
    private static final String COL_MESSAGES_BODY = "MESSAGES_BODY";
    private static final String COL_IS_DELETED = "IS_DELETED";
    private static final String COL_TIME_ZONE = "TIME_ZONE";



    private static final String REQUEST_CREATE_RIDES_TABLE = "CREATE TABLE IF NOT EXISTS "
            +TABLE_RIDES+" ("+
            COL_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "+
            COL_DEPARTURE_TIME+" TEXT NOT NULL DEFAULT '0',"+
            COL_ARRIVAL_TIME+" TEXT NOT NULL DEFAULT '0',"+
            COL_DEPARTURE_LOCATION+" TEXT NOT NULL DEFAULT '0',"+
            COL_ARRIVAL_LOCATION+" TEXT NOT NULL DEFAULT '0',"+
            COL_TIME_ELAPSED+" INTEGER,"+
            COL_SCORE+" REAL,"+
            COL_DRIVER_ID+" INTEGER,"+
            COL_COMPANY_ID+" INTEGER,"+
            COL_SEND+" INTEGER,"+
            COL_TIME+" TEXT,"+
            COL_AVG_TIME+ " DOUBLE,"+
            COL_TIME_TOTAL_SECONDES+ " REAL,"+
            COL_BAD_BEHAVIOUR+ " INTEGER,"+
            COL_TIME_ABOVE_120+ " TEXT NOT NULL DEFAULT '0',"+
            COL_CAUSE_OF_ENDING_RIDE+ " TEXT NOT NULL DEFAULT '0',"+
            COL_SUDDEN_ACCELERATION+ " INTEGER DEFAULT '0',"+
            COL_SUDDEN_BRAKING+ " INTEGER DEFAULT 0,"+
            COL_DISTANCE_TRAVELLED+ " TEXT NOT NULL DEFAULT '0',"+
            COL_SUDDEN_SPEEDING+ " INTEGER DEFAULT 0,"+
            COL_IS_SPEEDING_UPDATED+" INTEGER DEFAULT 0,"+
            COL_ORPHAN_RIDE+" INTEGER DEFAULT 0,"+
            COL_TIME_ZONE+" TEXT NOT NULL DEFAULT '0');";



    /**
     * Represents the table name
     */
    private static final String TABLE_CATEGORIES = "table_categories";

    /**
     * Represents the table name
     */
    private static final String TABLE_SUB_RIDES = "table_sub_rides";
    /**
     * Represents the column ID
     */
    private static final String COL_ID_CATEGORY = "ID";
    /**
     * Represents the column categories
     */
    private static final String COL_CATEGORIES = "CATEGORIES";
    /**
     * Represents the database request to create categories table
     */
    private static final String REQUEST_CREATE_CATEGORIES_TABLE = "CREATE TABLE IF NOT EXISTS "
            +TABLE_CATEGORIES+" ("
            +COL_ID_CATEGORY+" INTEGER PRIMARY KEY AUTOINCREMENT, "
            +COL_CATEGORIES+" TEXT);";



    /**
     * Represents the table name
     */
    private static final String TABLE_ALLOWED_APPS = "table_allowed_apps";
    /**
     * Represents the column ID
     */
    private static final String COL_ID_ALLOWED = "ID";
    /**
     * Represents the column allowed
     */
    private static final String COL_ALLOWED = "ALLOWED";
    /**
     * Represents the database request to create callowed applications table
     */

    private static final String REQUEST_CREATE_SUBRIDES_TABLE = "CREATE TABLE IF NOT EXISTS "
            +TABLE_SUB_RIDES+" ("

            +"ID_SUB_RIDE"+" INTEGER PRIMARY KEY AUTOINCREMENT, "

            +"ID_MAIN_RIDE"+" INTEGER,"

            +"AREA_TYPE"+" TEXT,"

            +"START_LAT_LNG"+" TEXT NOT NULL DEFAULT '0',"

            +"END_LAT_LNG"+" TEXT NOT NULL DEFAULT '0',"

            +"TIME_REGION_ENTERED"+" TEXT NOT NULL DEFAULT '0' ,"

            +"CURRENT_SPEED"+" TEXT,"

            +"TIME_LAPSED_INSIDE_A_REGION"+" INTEGER DEFAULT 0,"

            + "IS_SENT" +" INTEGER DEFAULT 0);";


    private static final String REQUEST_CREATE_MESSAGES_TABLE = "CREATE TABLE IF NOT EXISTS "
            +TABLE_MESSAGES+" ("

            +COL_MESSAGES_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "

            +COL_REMOTE_MESSAGES_ID+" INTEGER,"

            +COL_MESSAGES_TITLE+" TEXT,"

            +COL_MESSAGES_BODY+" TEXT,"

            +COL_MESSAGES_DATE_TIME+" TEXT NOT NULL DEFAULT '0',"

            + COL_IS_DELETED +" INTEGER DEFAULT 0);";



    private static final String REQUEST_CREATE_ALLOWED_APPS_TABLE = "CREATE TABLE IF NOT EXISTS "
            +TABLE_ALLOWED_APPS+" ("
            +COL_ID_ALLOWED+" INTEGER PRIMARY KEY AUTOINCREMENT, "
            +COL_ALLOWED+" TEXT);";

    /**
     * Class constructor
     * @param context(in), @Activity represents activity
     * @param nameDB(in), @String is the database name
     * @param version(in), @Integer is the database version
     */
    public MyBaseSQLite(Context context, String nameDB, int version){
        super(context, nameDB, null, version);
        //Log.i("Adneom"," in constructor DB ");
        SQLiteDatabase db = getWritableDatabase();
        long size = new File(db.getPath()).length();
        Log.v("DB SIZE", "Percentage database space used: " + size);
    }


    /**
     * Allows to create the tables in database.
     * @param db(in), @SQLite represents the database instnace
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(REQUEST_CREATE_RIDES_TABLE);
        db.execSQL(REQUEST_CREATE_CATEGORIES_TABLE);
        db.execSQL(REQUEST_CREATE_ALLOWED_APPS_TABLE);
        db.execSQL(REQUEST_CREATE_SUBRIDES_TABLE);
        db.execSQL(REQUEST_CREATE_MESSAGES_TABLE);

    }

    //call when we change version
    /**
     * Allows to re create a new database with a neww version. SQLiteOpenHelepr method basic.
     * @param db(in), @SQLite represents the instance database
     * @param oldVersion(in), @Integer represents the previous database version
     * @param newVersion(in), @Integer represents the new database version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Log.i("Adneom"," drop table ");

        Log.i("onUpgrade", "Update Schema to version: "+Integer.toString(oldVersion)+"->"+Integer.toString(newVersion));

        if(newVersion > oldVersion){
/*
            db.execSQL("ALTER TABLE " + TABLE_RIDES + " ADD COLUMN " + COL_DISTANCE_TRAVELLED + " TEXT NOT NULL DEFAULT '0'");
            db.execSQL("ALTER TABLE " + TABLE_RIDES + " ADD COLUMN " + COL_TIME_ABOVE_120 + " TEXT NOT NULL DEFAULT '0'");
            db.execSQL("ALTER TABLE " + TABLE_RIDES + " ADD COLUMN " + COL_CAUSE_OF_ENDING_RIDE + " TEXT NOT NULL DEFAULT '0'");*/

            db.execSQL("ALTER TABLE " + TABLE_RIDES + " ADD COLUMN " + COL_ORPHAN_RIDE + " INTEGER DEFAULT 0");
            /*db.execSQL("ALTER TABLE " + TABLE_RIDES + " ADD COLUMN " + COL_SUDDEN_BRAKING + " INTEGER DEFAULT 0");


            db.execSQL("ALTER TABLE " + TABLE_RIDES + " ADD COLUMN " + COL_SUDDEN_SPEEDING + " INTEGER DEFAULT 0");
            db.execSQL("ALTER TABLE " + TABLE_RIDES + " ADD COLUMN " + COL_IS_SPEEDING_UPDATED + " INTEGER DEFAULT 0");
            db.execSQL("ALTER TABLE " + TABLE_RIDES + " ADD COLUMN " + COL_TIME_ZONE + " TEXT NOT NULL DEFAULT '0'");*/

            /*db.execSQL(REQUEST_CREATE_SUBRIDES_TABLE);
            db.execSQL(REQUEST_CREATE_MESSAGES_TABLE);*/
            db.execSQL(REQUEST_CREATE_RIDES_TABLE);
        }
        else{
            // I removed onCreate() method from here because new version will already have the new table
            /*  db.execSQL("DROP TABLE " + TABLE_RIDES + ";");
        db.execSQL("DROP TABLE " + TABLE_CATEGORIES + ";");
        db.execSQL("DROP TABLE " + TABLE_ALLOWED_APPS + ";");
        //Log.i("Adneom"," create table rides ** ");
        onCreate(db);*/
        }


    }
}
