package com.studio.barefoot.freeedrivebeacononlyapp.services;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.studio.barefoot.freeedrivebeacononlyapp.R;


public class FDGPSTracker implements LocationListener {

    /**
     * Represents the actual context
     */
    private final Context context;

    /**
     * Represents a location manager to handle the location
     */
    private LocationManager locationManager;

    /**
     * Represents the location from location manager
     */
    Location location;

    /**
     * This the status of GPS provider, true if gps is enabled else false
     */
    private Boolean isGPSEnabled;

    /**
     * This the status of Network provider, true if network is enabled else false
     */
    private Boolean isNetworkEnabled;

    /**
     * This the status of Passive provider, true if passive is enabled else false
     */
    private Boolean isPassiveEnabled;

    /**
     * The minimum distance to change Updates in meters
     */
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 10 meters

    /**
     * The minimum time between updates in milliseconds
     */
    private static final long MIN_TIME_BW_UPDATES = 1; // 1 minute

    /**
     * Represents the latitude
     */
    private static double latitude;
    /**
     * Represents the longitude
     */
    private static double longitude;
    /**
     * Represents a location
     */
    private static Location ll;

    /**
     * Represents a boolean, it allows to save the departure location after start tracker.
     * It is not possible from Receiver because
     * the first time the location is null.
     */
    public boolean canSaveDepartureLocation = true;
    /**
     * Represents a boolean, it allows to save the arrival location after start tracker.
     */
    public boolean canSaveArrivalLocation = false;


    /**
     * Class construcor
     * @param context, @Activity represents the context from TestReceiver.
     */
    public FDGPSTracker(Context context) {
        this.context = context;
    }

    /**
     * Allows to get the current location
     *
     * @return Location
     */
    public Location getLLLocation() {
        return ll;
    }

    /**
     * Represents the location
     *
     * @param val(in), @Integer represents the provider, 1 = GPS and 2 = Network
     */
    private Location gettingLocation(int val) {
        location = null;
        Location locationTmpGPS = null, locationTmpNetwork = null, locationTmpPassive = null;
        if (locationManager != null) {
            switch (val) {
                case 1:
                    if (android.os.Build.VERSION.SDK_INT <23 || ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.

                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0,
                            new LocationListener() {

                                public void onLocationChanged(Location location) {
                                    FDGPSTracker.ll = location;
                                    //Log.i("Adneom","location changed for gps : "+FDGPSTracker.ll);
                                }

                                public void onProviderDisabled(String provider) {
                                }

                                public void onProviderEnabled(String provider) {
                                }

                                public void onStatusChanged(String provider, int status,
                                                            Bundle extras) {
                                }
                            });
                    locationTmpGPS = locationManager
                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (locationTmpGPS != null) {
                        //Log.i("Adneom"," -- location is from gps ("+locationTmpGPS.getLatitude()+","+locationTmpGPS.getLatitude()+") location is setted with GPS -- ");
                        location = locationTmpGPS;
                        latitude = locationTmpGPS.getLatitude();
                        longitude = locationTmpGPS.getLongitude();
                    }
                    break;

                case 2:
                    if (android.os.Build.VERSION.SDK_INT <23 || ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)

                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0,
                            new LocationListener() {

                                public void onLocationChanged(Location location) {
                                    FDGPSTracker.ll = location;
                                    //Log.i("Adneom","location changed for NETWORK : "+FDGPSTracker.ll);
                                }

                                public void onProviderDisabled(String provider) {
                                }

                                public void onProviderEnabled(String provider) {
                                }

                                public void onStatusChanged(String provider, int status,
                                                            Bundle extras) {
                                }
                            });
                    locationTmpNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (locationTmpNetwork != null) {
                        //Log.i("Adneom"," -- location is from network ("+locationTmpNetwork.getLatitude()+","+locationTmpNetwork.getLatitude()+") ps : location is setted with network -- ");
                        location = locationTmpNetwork;
                        latitude = locationTmpNetwork.getLatitude();
                        longitude = locationTmpNetwork.getLongitude();
                    }
                    break;
                case 3:
                    if (android.os.Build.VERSION.SDK_INT <23 || ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)

                        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 1000, 0,
                            new LocationListener() {

                                public void onLocationChanged(Location location) {
                                    FDGPSTracker.ll = location;
                                    //Log.i("Adneom","location changed for PASSIVE : "+FDGPSTracker.ll);
                                }

                                public void onProviderDisabled(String provider) {
                                }

                                public void onProviderEnabled(String provider) {
                                }

                                public void onStatusChanged(String provider, int status,
                                                            Bundle extras) {
                                }
                            });
                    locationTmpPassive = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                    if (locationTmpPassive != null) {
                        //Log.i("Adneom"," -- location is from passive ("+locationTmpPassive.getLatitude()+","+locationTmpPassive.getLatitude()+") ps : location is setted with passive -- ");
                        location = locationTmpPassive;
                        latitude = locationTmpPassive.getLatitude();
                        longitude = locationTmpPassive.getLongitude();
                    }
                    break;
            }
        }
        //Log.i("Adneom","location saved is "+location.getLatitude()+" "+location.getLongitude());
        //return location;
        return ll;
    }

    /**
     * LocationLister method basic, not used in my case
     * @param location(in), @Location represents a location
     */
    @Override
    public void onLocationChanged(Location location) {
        ll = location;
        Log.i("Adneom"," Location from on lacation changed is ("+ll.getLatitude()+","+ll.getLongitude()+")");

        //save the departure location after that tracker has been started:
        if (canSaveDepartureLocation && Detector.FdApp.departureLocation == null) {
            Detector.FdApp.departureLocation = ll;
            //Log.i("Adneom","____ Departure location is ("+Detector.FdApp.departureLocation.getLatitude()+","+Detector.FdApp.departureLocation.getLongitude()+") ___ ");
            canSaveDepartureLocation = false;
            //stop tracker :
            stopFdTrackerLocation();
        }

        //getting the arrival :
        if (canSaveArrivalLocation && Detector.FdApp.arrivalLocation == null) {
            Detector.FdApp.arrivalLocation = ll;
            //Log.i("Adneom","____ Arrival location is ("+Detector.FdApp.arrivalLocation.getLatitude()+","+Detector.FdApp.arrivalLocation.getLongitude()+") ___ ");
            canSaveArrivalLocation = false;
            //stop tracker :
            stopFdTrackerLocation();
        }
    }

    /**
     * LocationLister method basic, not used in my case
     * @param provider(in), @String represents theprovider
     * @param status(in), @Integer represents the status
     * @param extras(in), @Bundle extras info
     */
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    /**
     * LocationLister method basic, not used in my case
     * @param provider(in), @String represents the provider
     */
    @Override
    public void onProviderEnabled(String provider) {
    }

    /**
     * LocationLister method basic, not used in my case
     * @param provider(in), @String represents the provider
     */
    @Override
    public void onProviderDisabled(String provider) {
    }


    /**
     * Allows to start the location manager
     */
    public void startFdTrackerLocation() {
        //instantiated location manager
        if (android.os.Build.VERSION.SDK_INT <23 || ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 0, 0, this);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
//            location.getSpeed();

        }else{
            Toast.makeText(context, R.string.location_permission_not_granted,Toast.LENGTH_SHORT).show();
            Log.e("Permision Denied","DENIED");
            return;
        }

        //Log.i("Adneom", "start tracker");
    }

    /**
     * Allows to stop the location manager
     */
    public void stopFdTrackerLocation() {
        if (locationManager != null) {
            if (android.os.Build.VERSION.SDK_INT <23 || ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                locationManager.removeUpdates(this);
            }
            //Log.i("Adneom", "stop tracker");
        }
    }

    /**
     * Allows to set the boolean to true to get the departure location.
     * @param val, @Boolean the new value
     */
    public void setCanSaveDepartureLocation(Boolean val){
        this.canSaveDepartureLocation = val;
    }

    /**
     * Allows to set the boolean to true to get the arrival location.
     * @param val, @Boolean the new value
     */
    public void setCanSaveArrivalLocation(boolean val){
        this.canSaveArrivalLocation = val;
    }
}
