package com.studio.barefoot.freeedrivebeacononlyapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.MenuActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.UIUtils;


//import com.freeedrive.R;

public class MenuAdapter extends BaseAdapter
{
	public Context mActivity;
	private int autoReply=100;
	String menudrawerLanaguge = "";


	public MenuAdapter(Context ma)
	{
		this.mActivity = ma;
	}

	@Override
	public int getCount()
	{
		return 6;
	}

	@Override
	public Object getItem(int arg0)
	{
		return null;
	}

	@Override
	public long getItemId(int arg0)
	{
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{

		convertView = LayoutInflater.from(mActivity).inflate(R.layout.menu_cell, null);
		TextView tv = (TextView) convertView.findViewById(R.id.tv);
		final ImageView iv = (ImageView) convertView.findViewById(R.id.auto_switch_button_autoreply);
		iv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				autoReply = DataHandler.getIntPreferences(AppConstants.KEY_AUTOREPLY);
				if (autoReply==0){
					iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.off_switch));

					DataHandler.updatePreferences(AppConstants.KEY_AUTOREPLY,1);
				}else  {
					DataHandler.updatePreferences(AppConstants.KEY_AUTOREPLY,0);
					iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.on_switch));

				}
			}
		});
		//tv.setTypeface(UIUtils.getInstance().getMediumFont(mActivity));
		//	ImageView iv = (ImageView) convertView.findViewById(R.id.iv);
		if(position == 0) {
			tv.setText(mActivity.getResources().getString(R.string.title_profile));
			iv.setVisibility(View.INVISIBLE);


			//iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.bluetooth));
		}
		if(position == 1) {
			tv.setText(mActivity.getResources().getString(R.string.heading_Insurance));
			iv.setVisibility(View.INVISIBLE);
			//iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.notifications));
		}

		if(position == 2) {
			tv.setText(mActivity.getResources().getString(R.string.title_auto_reply));
			tv.isClickable();
			iv.setVisibility(View.VISIBLE);
			autoReply = DataHandler.getIntPreferences(AppConstants.KEY_AUTOREPLY);
			if (autoReply==1){
				iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.off_switch));
			}else {
				iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.on_switch));

			}


			//	iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.auto));
		}
		if(position == 3) {
			tv.setText(mActivity.getResources().getString(R.string.title_contact_us));
			iv.setVisibility(View.INVISIBLE);
			//	iv.setChecked(true);
		}
	/*		if(position == 4) {
				tv.setText(mActivity.getResources().getString(R.string.title_find_my_car));
				iv.setVisibility(View.INVISIBLE);
			//	iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.account));
			}*/
		if(position == 4) {
			tv.setText(mActivity.getResources().getString(R.string.FAQ));
			iv.setVisibility(View.INVISIBLE);
			//	iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.credits));
		}
		/*	if(position == 5) {
				tv.setText(mActivity.getResources().getString(R.string.title_credits));
			iv.setVisibility(View.INVISIBLE);
				//	iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.credits));
			}*/

		if(position == 5) {

			convertView = LayoutInflater.from(mActivity).inflate(R.layout.info_layout, null);
			convertView.setClickable(false);
			convertView.setEnabled(false);
			convertView.setOnClickListener(null);
		}
	//	tv.setTypeface(UIUtils.getInstance().getLightFont(mActivity));

		return convertView;
	}
}
