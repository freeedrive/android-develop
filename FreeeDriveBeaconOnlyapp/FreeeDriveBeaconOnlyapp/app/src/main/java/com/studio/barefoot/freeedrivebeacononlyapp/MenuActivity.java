package com.studio.barefoot.freeedrivebeacononlyapp;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Trace;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/*import com.amitshekhar.DebugDB;*/
import com.amitshekhar.DebugDB;
import com.google.firebase.iid.FirebaseInstanceId;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.MenuAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.MessagesAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.TurOnGpsDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.TurnOnBTDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.TurnOnDonotDisturbDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.TurnOnNotifcationDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.fragments.HomeScreenFragment;
import com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesFragment;
import com.studio.barefoot.freeedrivebeacononlyapp.fragments.GraphPhoneSafetyFragment;
import com.studio.barefoot.freeedrivebeacononlyapp.fragments.TripPhoneSafetyFragment;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.fdBlockedScreen;

public class MenuActivity extends AppCompatActivity  {

    View actionBarView;
    private static DrawerLayout mDrawerLayout;
    private ListView mMenuList;
    ImageView drawerImage,imageView_thums,action_share_on_newsfeed_via_actionbar;
    private String intentAction = "";

    TextView tv_menu_total_distractions,tv_all_ride_time, tv_start_time,tv_menu_total_score,tv_no_of_distractions,tv_last_ride_time,tv_menu_arrival,tv_menu_remarks,tv_menu_remarks_desc;

    RelativeLayout layout_behaviour;
    private int backCount=0;

    private static final String TAG = MainActivity.class.getSimpleName();
    private BottomNavigationView bottomNavigation;
    private Fragment fragment;
    private FragmentManager fragmentManager;

    protected OnBackPressedListener onBackPressedListener;


    public MenuActivity()
    {
        LocaleUtils.updateConfig(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean fd_active_status_flag;
        if (!DataHandler.getStringPreferences(AppConstants.TEMP_DISPLAY_KEY).isEmpty()){

            try {
                String fd_active_status = DataHandler.getStringPreferences(AppConstants.TEMP_DISPLAY_KEY);
                JSONObject jsonObj = new JSONObject(fd_active_status);
                if(jsonObj !=null){

                     fd_active_status_flag = jsonObj.getBoolean("active");
                    if(fd_active_status_flag == false){
                        Log.e("Your FD Locked","Due to Harsh Driving :"+fd_active_status_flag);
                       // deleteCache(getApplicationContext());
                        DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY,false);
                        fdBlockedScreen(getApplicationContext());
                        /*Intent intent = new Intent(MenuActivity.this,BlockedFreeDriveActivity.class);
                        startActivity(intent);*/

                    }
                    else{

                        // LocaleUtils.updateConfig(this);
                        setContentView(R.layout.activity_menu);
                        //Log.e("Your are","good to GO");
                        bottomNavigation = (BottomNavigationView)findViewById(R.id.bottom_navigation);
                        removeShiftMode(bottomNavigation);
                        setupNavigationView();

                        actionBarView = getLayoutInflater().inflate(R.layout.custom_toolbarr, null);
                        tv_menu_remarks = (TextView) findViewById(R.id.tv_menu_remarks);
                        tv_menu_remarks_desc = (TextView) findViewById(R.id.tv_menu_remarks_desc);
                        tv_menu_arrival = (TextView) findViewById(R.id.tv_end_time);
                        tv_start_time = (TextView) findViewById(R.id.tv_start_time);

                        //tv_menu_total_distractions = (TextView) findViewById(R.id.tv_menu_total_distractions);
                        layout_behaviour = (RelativeLayout) findViewById(R.id.layout_behaviour);

                        //tv_all_ride_time = (TextView) findViewById(R.id.tv_menu_allmyRidesTime);
                        tv_last_ride_time = (TextView) findViewById(R.id.tv_lastRide_min);
                        tv_no_of_distractions = (TextView) findViewById(R.id.tv_no_of_distractions);
                        tv_menu_total_score = (TextView) findViewById(R.id.tv_menu_total_score);
                        imageView_thums = (ImageView) findViewById(R.id.imageView_thums);
                        action_share_on_newsfeed_via_actionbar = (ImageView) findViewById(R.id.action_share_on_newsfeed_via_actionbar);

                        try{
                        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                        Log.e("refreshedToken", refreshedToken);
                        }catch (Exception exception){
                            exception.printStackTrace();
                        }



                        try
                        {
                            if(!isNotificationManager())
                            {
                                //Log.i("Adneom","*** (Detector) ask permission notification : "+isAlreadyAsking+" *** ");
                                Intent intent = new Intent("com.freeedrive.notification_settings");
                                sendBroadcast(intent);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        /// RegisterRecievers();
                        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        drawerImage = (ImageView) findViewById(R.id.customisedDrawer);
                        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
                            @Override
                            public void onDrawerSlide(View drawerView, float slideOffset) {
                                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

                            }

                            @Override
                            public void onDrawerOpened(View drawerView) {

                            }

                            @Override
                            public void onDrawerClosed(View drawerView) {

                            }

                            @Override
                            public void onDrawerStateChanged(int newState) {

                            }
                        });


                        mMenuList = (ListView) mDrawerLayout.findViewById(R.id.listViewNotifications);
                        mMenuList.setAdapter(new MenuAdapter(this));
                        mMenuList.setFooterDividersEnabled(false);
                        mMenuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                if (position == 0) {
                                    //Log.i("Adneom", " *** Menu bt *** ");
               /*     Intent i = new Intent(MenuActivity.this, BluetoothActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);*/
                                    Intent i = new Intent(MenuActivity.this, ProfileActivity.class);
                                    startActivity(i);

                                }
                                else if (position == 1) {
                                    //Log.i("Adneom", " *** Menu bt *** ");
               /*     Intent i = new Intent(MenuActivity.this, BluetoothActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);*/
                                    Intent i = new Intent(MenuActivity.this, InsuranceActivity.class);
                                    startActivity(i);
                                } else if (position == 2) {
                                    Intent i = new Intent(MenuActivity.this, AutoReplyActivity.class);
                                    startActivity(i);

                                }
                                else if (position == 3) {
                                    Intent i = new Intent(MenuActivity.this, ContactUsActivity.class);
                                    i.putExtra("accountMode", true);
                                    startActivity(i);
                                }
            /*    else if (position == 4) {
                    Intent i = new Intent(MenuActivity.this,ScoreSynchronizationActivity.class);
                    startActivity(i);
                }*/ else if (position == 4) {
                /*    Intent i = new Intent(MenuActivity.this, RateActivity.class);
                    startActivity(i);*/

                                    Intent i = new Intent(MenuActivity.this, FaqActivity.class);
                                    startActivity(i);
                                } else if (position == 5) {
                                    Intent i = new Intent(MenuActivity.this, FaqActivity.class);
                                    startActivity(i);
                                } else if (position == 6) {
                                    Intent i = new Intent(MenuActivity.this, FaqActivity.class);
                                    startActivity(i);
                                }

                            }
                        });
                        // Get the ActionBar here to configure the way it behaves.

                        // disable the default title element here (for centered title)

                        setupActionBar();

                        drawerImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (!mDrawerLayout.isDrawerOpen(Gravity.RIGHT))
                                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                                else
                                    mDrawerLayout.closeDrawers();
                            }
                        });


                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{


            try {

                        // LocaleUtils.updateConfig(this);
                        setContentView(R.layout.activity_menu);
                        //Log.e("Your are","good to GO");
                        bottomNavigation = (BottomNavigationView)findViewById(R.id.bottom_navigation);
                        removeShiftMode(bottomNavigation);
                        setupNavigationView();

                        actionBarView = getLayoutInflater().inflate(R.layout.custom_toolbarr, null);
                        tv_menu_remarks = (TextView) findViewById(R.id.tv_menu_remarks);
                        tv_menu_remarks_desc = (TextView) findViewById(R.id.tv_menu_remarks_desc);
                        tv_menu_arrival = (TextView) findViewById(R.id.tv_end_time);
                        tv_start_time = (TextView) findViewById(R.id.tv_start_time);

                        //tv_menu_total_distractions = (TextView) findViewById(R.id.tv_menu_total_distractions);
                        layout_behaviour = (RelativeLayout) findViewById(R.id.layout_behaviour);

                        //tv_all_ride_time = (TextView) findViewById(R.id.tv_menu_allmyRidesTime);
                        tv_last_ride_time = (TextView) findViewById(R.id.tv_lastRide_min);
                        tv_no_of_distractions = (TextView) findViewById(R.id.tv_no_of_distractions);
                        tv_menu_total_score = (TextView) findViewById(R.id.tv_menu_total_score);
                        imageView_thums = (ImageView) findViewById(R.id.imageView_thums);
                        action_share_on_newsfeed_via_actionbar = (ImageView) findViewById(R.id.action_share_on_newsfeed_via_actionbar);

                        try{
                            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                            Log.e("refreshedToken", refreshedToken);
                        }catch (Exception exception){
                            exception.printStackTrace();
                        }

    /*    if (!AppUtils.isBGServiceActive) {
            Intent serviceIntent = new Intent(this, BackgroundBeaconScan.class);
            startService(serviceIntent);
        }*/

                        /*try{
                            turOnGpsDialog = new TurOnGpsDialog(MenuActivity.this);
                            turnOnBTDialog =new TurnOnBTDialog(MenuActivity.this);
                            turnOnDonotDisturbDialog = new TurnOnDonotDisturbDialog(this);
                            turnOnNotifcationDialog =new TurnOnNotifcationDialog(MenuActivity.this);
                            String DbLog = DebugDB.getAddressLog().toString();
                            Log.e("DbLog",DbLog);

                        }catch (Exception e){
                            e.printStackTrace();
                        }*/
                        try
                        {
                            if(!isNotificationManager())
                            {
                                //Log.i("Adneom","*** (Detector) ask permission notification : "+isAlreadyAsking+" *** ");
                                Intent intent = new Intent("com.freeedrive.notification_settings");
                                sendBroadcast(intent);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        /// RegisterRecievers();
                        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        drawerImage = (ImageView) findViewById(R.id.customisedDrawer);
                        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
                            @Override
                            public void onDrawerSlide(View drawerView, float slideOffset) {
                                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

                            }

                            @Override
                            public void onDrawerOpened(View drawerView) {

                            }

                            @Override
                            public void onDrawerClosed(View drawerView) {

                            }

                            @Override
                            public void onDrawerStateChanged(int newState) {

                            }
                        });


                        mMenuList = (ListView) mDrawerLayout.findViewById(R.id.listViewNotifications);
                        mMenuList.setAdapter(new MenuAdapter(this));
                        mMenuList.setFooterDividersEnabled(false);
                        mMenuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                if (position == 0) {
                                    //Log.i("Adneom", " *** Menu bt *** ");
               /*     Intent i = new Intent(MenuActivity.this, BluetoothActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);*/
                                    Intent i = new Intent(MenuActivity.this, ProfileActivity.class);
                                    startActivity(i);

                                }
                                else if (position == 1) {
                                    //Log.i("Adneom", " *** Menu bt *** ");
               /*     Intent i = new Intent(MenuActivity.this, BluetoothActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);*/
                                    Intent i = new Intent(MenuActivity.this, InsuranceActivity.class);
                                    startActivity(i);
                                } else if (position == 2) {
                                    Intent i = new Intent(MenuActivity.this, AutoReplyActivity.class);
                                    startActivity(i);

                                }
                                else if (position == 3) {
                                    Intent i = new Intent(MenuActivity.this, ContactUsActivity.class);
                                    i.putExtra("accountMode", true);
                                    startActivity(i);
                                }
            /*    else if (position == 4) {
                    Intent i = new Intent(MenuActivity.this,ScoreSynchronizationActivity.class);
                    startActivity(i);
                }*/ else if (position == 4) {
                /*    Intent i = new Intent(MenuActivity.this, RateActivity.class);
                    startActivity(i);*/

                                    Intent i = new Intent(MenuActivity.this, FaqActivity.class);
                                    startActivity(i);
                                } else if (position == 5) {
                                    Intent i = new Intent(MenuActivity.this, FaqActivity.class);
                                    startActivity(i);
                                } else if (position == 6) {
                                    Intent i = new Intent(MenuActivity.this, FaqActivity.class);
                                    startActivity(i);
                                }

                            }
                        });
                        // Get the ActionBar here to configure the way it behaves.

                        // disable the default title element here (for centered title)

                        setupActionBar();

                        drawerImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (!mDrawerLayout.isDrawerOpen(Gravity.RIGHT))
                                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                                else
                                    mDrawerLayout.closeDrawers();
                            }
                        });




            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try{
            String intent_action = getIntent().getStringExtra("PushNotification");
            if(intent_action.equalsIgnoreCase("M")){
                //Toast.makeText(this, "MessageWarning", Toast.LENGTH_SHORT).show();
                setupNavigationViewForPushNotification();
                //pushFragment(new MessagesFragment());
            }
            else if(intent_action.equalsIgnoreCase("T")){
                //Toast.makeText(this, "Fuck this shit", Toast.LENGTH_SHORT).show();
                setupNavigationViewForPushNotification();
                //pushFragment(new TripPhoneSafetyFragment());
            }

        }catch (NullPointerException e){
            e.printStackTrace();
        }

        try {
            boolean intent_action = getIntent().getAction().equalsIgnoreCase("M");
            String intent = getIntent().getAction().toString();
            if(intent.equalsIgnoreCase("M")){
                setupNavigationViewForPushNotification();
                //pushFragment(new MessagesFragment());
            }
            else if(intent.equalsIgnoreCase("T")){
                setupNavigationViewForPushNotification();
                //pushFragment(new TripPhoneSafetyFragment());
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }

    }

    //Delete application cache
    public void deleteCache(Context context) {
        try {
            File cache = getCacheDir();
            File appDir = new File(cache.getParent());
            if(appDir.exists()){
                String[] children = appDir.list();
                for(String s : children){
                    if(!s.equals("lib")){
                        deleteDir(new File(appDir, s));
                        Log.i("TAG", "File /data/data/APP_PACKAGE/" + s +" DELETED");
                    }
                }
            }
        } catch (Exception e) {}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public interface OnBackPressedListener {
        void doBack();
    }
    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }


    //Disable the shiftmode of icon in Botton Navigation of Fragement
    public void removeShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("ERROR NO SUCH FIELD", "Unable to get shift mode field");
        } catch (IllegalAccessException e) {
            Log.e("ERROR ILLEGAL ALG", "Unable to change value of shift mode");
        }
    }


    /**
     * This method allows to close menu after the choice of a Bluetooth
     */
    public static void closeD() {
        if (mDrawerLayout != null && mDrawerLayout.isShown()) {
            mDrawerLayout.closeDrawers();
        }
    }
    private boolean isNotificationManager() throws Exception
    {
        ContentResolver contentResolver = getContentResolver();
        String enabledNotificationListeners = Settings.Secure.getString(contentResolver, "enabled_notification_listeners");
        //Log.e("FD","enabledNotificationListeners: " + enabledNotificationListeners);
        String packageName = getPackageName();
        return enabledNotificationListeners != null && enabledNotificationListeners.contains(packageName);
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }



    private void setupNavigationViewForPushNotification(){

        if(bottomNavigation !=null){
            // Select first menu item by default and show Fragment accordingly.
            bottomNavigation.getMenu().getItem(0).setChecked(false);
            Menu menu = bottomNavigation.getMenu();

            if(intentAction.equalsIgnoreCase("M")){
            selectFragment(menu.getItem(3));
            bottomNavigation.getMenu().getItem(3).setChecked(true);
            }
            if(intentAction.equalsIgnoreCase("T")){
                selectFragment(menu.getItem(2));
                bottomNavigation.getMenu().getItem(2).setChecked(true);
            }
            //removeShiftMode((BottomNavigationView) bottomNavigation.getMenu().getItem(3));

            // Set action to perform when any menu-item is selected.
            bottomNavigation.setOnNavigationItemSelectedListener(
                    new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                            if(intentAction.equalsIgnoreCase("M")){

                                bottomNavigation.getMenu().getItem(0).setChecked(true);
                                selectFragment(item);
                                bottomNavigation.getMenu().getItem(3).setChecked(false);
                            }
                            if(intentAction.equalsIgnoreCase("T")){
                                bottomNavigation.getMenu().getItem(0).setChecked(true);
                                selectFragment(item);
                                bottomNavigation.getMenu().getItem(2).setChecked(false);
                            }

                            return true;
                        }
                    });

        }
    }

    public void backup() {
        try {
            File sdcard = Environment.getExternalStorageDirectory();
            File outputFile = new File(sdcard,
                    "FD.sql");

            if (!outputFile.exists())
                outputFile.createNewFile();

            File data = Environment.getDataDirectory();
            File inputFile = new File(data, "data/com.studio.barefoot.freeedrivebeacononlyapp/databases/freeedriveV2.db");
            InputStream input = new FileInputStream(inputFile);
            OutputStream output = new FileOutputStream(outputFile);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
            output.flush();
            output.close();
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new Error("Copying Failed");
        }
    }

    private void setupNavigationView() {

        if(bottomNavigation !=null){
            // Select first menu item by default and show Fragment accordingly.
            Menu menu = bottomNavigation.getMenu();
            selectFragment(menu.getItem(0));

            // Set action to perform when any menu-item is selected.
            bottomNavigation.setOnNavigationItemSelectedListener(
                    new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                            selectFragment(item);
                            return true;
                        }
                    });

        }
    }

    /**
     * Perform action when any item is selected.
     *
     * @param item Item that is selected.
     */
    protected void selectFragment(MenuItem item) {

        //item.setChecked(true);

        switch (item.getItemId()){
            case R.id.action_home:
                pushFragment(new HomeScreenFragment());

                break;
            case R.id.action_graph:
                pushFragment(new GraphPhoneSafetyFragment());

                break;
            case R.id.action_trips:
                pushFragment(new TripPhoneSafetyFragment());

                break;
            case R.id.action_messages:
                pushFragment(new MessagesFragment());
               // pushFragment(new GraphDrivingPerformanceFragment());

                break;
        }
    }

    /**
     * Method to push any fragment into given id.
     *
     * @param fragment An instance of Fragment to show into the given id.
     */
    protected void pushFragment(Fragment fragment) {
        if (fragment == null)
            return;

        fragmentManager  = getSupportFragmentManager();
        if (fragmentManager != null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (ft != null) {
                ft.replace(R.id.main_container, fragment);
                ft.commit();
            }
        }
    }



    @Override
    public void onBackPressed() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            Trace.endSection();
        }
        if (onBackPressedListener != null && MessagesAdapter.isToggled) {
            onBackPressedListener.doBack();

        } else {


            closeD();
            backCount++;
            if (backCount == 2) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                startActivity(intent);
                moveTaskToBack(true);
                finish();
                backCount = 0;
            }
            //setResult(RESULT_OK);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onBackPressedListener = null;
        mDrawerLayout= null;
    }
}
