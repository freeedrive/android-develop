package com.studio.barefoot.freeedrivebeacononlyapp.dialogs;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.studio.barefoot.freeedrivebeacononlyapp.QrCodeActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

/**
 * Created by mcs on 12/28/2016.
 */

public class QrCodeUIDdialog extends BaseAlertDialog implements DialogInterface.OnClickListener {
    TextView tv_uuid;
    ContentResolver resolver;
    String uuid="";
    private String dashless="";

    public QrCodeUIDdialog(Context context,String UID) {
        super(context);

        LayoutInflater factory = LayoutInflater.from(context);
        final View progressBarView = factory.inflate(R.layout.custom_dialog_uuid, null);

        setView(progressBarView);
        this.context = context;
        tv_uuid = (TextView) progressBarView.findViewById(R.id.tv_uuid);
        setButton(BUTTON_POSITIVE, context.getString(R.string.action_send), this);

        resolver = context.getContentResolver();

        if (UID != null && !UID.isEmpty() && UID.length() == 32){
            setTitle(context.getString(R.string.title_uuid_dialog));
            tv_uuid.setText(UID);
        }
        else if(UID.length() !=32 && !UID.isEmpty()){
            Log.e("iBKS Serial",UID);
            setTitle(context.getString(R.string.title_ibks_serial_dialog));
            tv_uuid.setText(UID);
        }
        uuid =UID;
        setCancelable(false);
        if(DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER_TEMP).isEmpty()){
        String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER_TEMP);
        DataHandler.updatePreferences(AppConstants.PHONE_NUMBER,phoneNumber);
        }
        else{
            String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER_TEMP);
            DataHandler.updatePreferences(AppConstants.PHONE_NUMBER,phoneNumber);
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
         dismiss();
        if (uuid.contains("-")){
            dashless= AppUtils.removeDashes(uuid);
            DataHandler.updatePreferences(AppConstants.TEMP_UUID,dashless);

        }
        else {
            dashless=uuid;
            DataHandler.updatePreferences(AppConstants.TEMP_UUID,dashless);
        }

        if (dashless.length()==32){
            final int mid = 20; //get the middle of the String
            String[] parts = {dashless.substring(0, mid),dashless.substring(mid)};
            String nameSpace = parts[0];
            String instanceID=parts[1];
            DataHandler.updatePreferences(AppConstants.TEMP_UUID_NAME_SPACE,nameSpace);
            DataHandler.updatePreferences(AppConstants.TEMP_UUID_INSTANCE_ID,instanceID);

            Log.e("uuid",""+parts[0]);
            Log.e("instanceID",""+parts[1]);
            Intent refresh = new Intent(context,QrCodeActivity.class);
            refresh.putExtra("validateQr","validated");
            context.startActivity(refresh);
            dismiss();
        }
        else if(dashless.length()!=32){

            DataHandler.updatePreferences(AppConstants.TEMP_IBKS_SERIAL_NO,dashless);
            Intent refresh = new Intent(context,QrCodeActivity.class);
            refresh.putExtra("validateSerial","validatedSerial");
            context.startActivity(refresh);
            dismiss();
        }/*else{
            dismiss();
            Toast.makeText(context,"Wrong uuid code",Toast.LENGTH_LONG).show();
            Intent startQrcodeIntent = new Intent(context,QrCodeActivity.class);
            startQrcodeIntent.putExtra("comingFromProfile","1");
            context. startActivity(startQrcodeIntent);

        }*/


    }
}
