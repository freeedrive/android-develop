package com.studio.barefoot.freeedrivebeacononlyapp.fragments;


import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.PhoneSafetDividerDecor;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.PhoneSafetyAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.RidesBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.PerformancePackDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

import java.util.ArrayList;



/**
 * A simple {@link Fragment} subclass.
 */
public class TripPhoneSafetyFragment extends Fragment {

    RecyclerView listViewPhoneSafety;
    private ArrayList<RidesBeans> ridesBeansArrayList = new ArrayList<>();
    private PhoneSafetyAdapter phoneSafetyAdapter;
    LinearLayoutManager mLayoutManager;
    View tripphoneSafety;
    Fragment fragment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    RelativeLayout relativeLayout_trip_phone_saftey, relativeLayoutdriving, relativeLayout_trip_driving_performance, relativeLayoutPhone;
    TextView phonesafety, drivingperformance,emptyRidesTextView;
    Button tripfragment_phone_safety, tripfragment_driving_performance;

    ImageView ImgDrivingPerformance;
    PerformancePackDialog performancePackDialog;
    public TripPhoneSafetyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        tripphoneSafety = inflater.inflate(R.layout.fragment_trip_phone_safety, container, false);
        emptyRidesTextView = (TextView) tripphoneSafety.findViewById(R.id.emptyTextViewForPhoneSafetRides);
        ImgDrivingPerformance = (ImageView) tripphoneSafety.findViewById(R.id.drivingperformance);

        getPhoneSafetyRides();
        listViewPhoneSafety = (RecyclerView) tripphoneSafety.findViewById(R.id.listViewPhoneSafety);

        mLayoutManager = new LinearLayoutManager(getActivity());

        listViewPhoneSafety.setHasFixedSize(true);
        listViewPhoneSafety.setLayoutManager(mLayoutManager);
        listViewPhoneSafety.addItemDecoration(new PhoneSafetDividerDecor(getActivity(), LinearLayoutManager.VERTICAL));
        listViewPhoneSafety.setItemAnimator(new DefaultItemAnimator());
        phoneSafetyAdapter = new PhoneSafetyAdapter(ridesBeansArrayList);
        listViewPhoneSafety.setAdapter(phoneSafetyAdapter);

        phoneSafetyAdapter.notifyDataSetChanged();

        //Layout intiliaze
        relativeLayout_trip_phone_saftey = (RelativeLayout) tripphoneSafety.findViewById(R.id.insidecontaintermobilehotspot);
        relativeLayoutdriving = (RelativeLayout) tripphoneSafety.findViewById(R.id.drivingrelativelayout);
        relativeLayout_trip_driving_performance = (RelativeLayout) tripphoneSafety.findViewById(R.id.drivingrelativelayout);
        relativeLayoutPhone = (RelativeLayout) tripphoneSafety.findViewById(R.id.insidecontaintermobilehotspot);

        //textview inttiliaze
        phonesafety = (TextView) tripphoneSafety.findViewById(R.id.textviewmobilehotSpot);
        drivingperformance = (TextView) tripphoneSafety.findViewById(R.id.textviewdrivingperformance);

        tripfragment_phone_safety = (Button) tripphoneSafety.findViewById(R.id.tripfragment_phonesafety_btn);
        tripfragment_driving_performance = (Button) tripphoneSafety.findViewById(R.id.trip_fragment_drivingperformance_btn);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            relativeLayout_trip_phone_saftey.setBackground(getActivity().getResources().getDrawable(R.drawable.phonesafety_custom_shape));
        }
        phonesafety.setTextColor(getActivity().getResources().getColor(R.color.white));
        if (!DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                relativeLayoutdriving.setBackground(getActivity().getResources().getDrawable(R.drawable.performance_pack_custom_shape));
                drivingperformance.setTextColor(getActivity().getResources().getColor(R.color.colorBTestMenu));
                ImgDrivingPerformance.setImageResource(R.drawable.tab_icon_car_grey);
            }

        }

        tripfragment_driving_performance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                 if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)){
                     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                         relativeLayout_trip_driving_performance.setBackground(getActivity().getResources().getDrawable(R.drawable.driving_performance_custom_shape));
                     }
                     drivingperformance = (TextView) tripphoneSafety.findViewById(R.id.textviewdrivingperformance);
                     drivingperformance.setTextColor(getActivity().getResources().getColor(R.color.white));


                     fragment = new TripDrivingPerformanceFragment();
                     fragmentManager = getActivity().getSupportFragmentManager();
                     fragmentTransaction = fragmentManager.beginTransaction();
                     fragmentTransaction.replace(R.id.trip_fragment, fragment);
                     fragmentTransaction.addToBackStack(null);
                     fragmentTransaction.commit();

                     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                         relativeLayoutPhone.setBackground(getActivity().getResources().getDrawable(R.drawable.phone_safety_custom_shape));
                     }
                     phonesafety = (TextView) tripphoneSafety.findViewById(R.id.textviewmobilehotSpot);
                     phonesafety.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
                 }else{
                     performancePackDialog = new PerformancePackDialog(getActivity());
                     performancePackDialog.show();
                 }

            }
        });

        return tripphoneSafety;
    }

    private void getPhoneSafetyRides() {
        RideBDD tmp = new RideBDD(getContext());
        tmp.open();
        ridesBeansArrayList = tmp.getAllRidesForPhoneSafety();
        tmp.close();
        if (ridesBeansArrayList.size()==0){
            emptyRidesTextView.setVisibility(View.VISIBLE);
        }

    }

}
