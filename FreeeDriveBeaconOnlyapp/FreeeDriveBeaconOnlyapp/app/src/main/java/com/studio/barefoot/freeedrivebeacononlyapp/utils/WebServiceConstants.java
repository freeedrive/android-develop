package com.studio.barefoot.freeedrivebeacononlyapp.utils;

import android.content.Context;

/**
 * Created by mcs on 12/21/2016.
 */

public class WebServiceConstants {
         //public static final String WEBSERVICE_URL_PREFIX = "http://192.168.10.14/api";
         // developement URL
       public static final String WEBSERVICE_URL_PREFIX = "https://fd-development.frogeek.com/api";
       // quality insurance Url
   //   public static final String WEBSERVICE_URL_PREFIX = "https://fd-qa.frogeek.com/api";
     // For Staging Url
     // public static final String WEBSERVICE_URL_PREFIX = "https://fd-staging.frogeek.com/api";
      // for Production
     //public static final String WEBSERVICE_URL_PREFIX = "https://fd-production.frogeek.com/api";

   // public static final String WEBSERVICE_URL_PREFIX = "http://xyperdemos.com/pk_freeedrive_backend-master/public/api";

   // public final static String  WEBSERVICE_PROJECT_PREFIX ="/FaceRubServer";
    public final static String END_POINT_NOTIFICATIONS  ="getnotifications";
    public final static String END_POINT_UPDATE_PHONE_NUMBER ="updatePhonenumber";
    public final static String END_POINT_UPDATE_PROFILE ="/profile";
    public final static String END_POINT_RESEND_SMS ="mobileresendsms";

   public final static String END_POINT_FETCH_INSURANCE ="mobileInsurance";

 public final static String END_POINT_FETCH_PROFILE ="getuserdata";
    public final static String END_POINT_FEEDBACK ="/feedback";
    public final static String END_POINT_DELETE_NOTIFICATIONS ="/deletenotifications";
    //public final static String END_POINT_SCORE = "/score";
    public final static String END_POINT_SCORE = "/v3/score";
    //public final static String END_POINT_SCORE = "/score";

    public final static String END_POINT_UPDATE_UUID ="UpdateUuid";
    public final static String END_POINT_UUID ="uuid";
    public final static String END_POINT_UUID_v3 ="v3/uuid";
    public final static String END_POINT_LOGIN ="auth";
    public final static String END_POINT_REGISTER="register";
    public final static String END_POINT_SMS_VERIFICATION="smsVerification";
    public final static String END_POINT_GRAPH_SCORE = "safetyscore/graph";
    public final static String END_POINT_PERFORMANCE_SCORE = "performancescore/graph";
    public final static String END_POINT_SPEEDING_SCORE = "data/scores";


    public static final String END_POINT_UPDATE_PRIVATE_HOURS ="v3/sp/userprivatehour" ;
}
