package com.studio.barefoot.freeedrivebeacononlyapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.adapters.FaqTextListAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FaqActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    public static String[] titles;
    public static String[] descriptions ;
    List<FaqTextListAdapter.Item> data;
    public RecyclerView listViewNotifications;
    public TextView fd_Version,fd_Web;
    public FaqActivity()
    {
        LocaleUtils.updateConfig(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_faq);

        fd_Version = (TextView) findViewById(R.id.fd_Version);
        fd_Web = (TextView) findViewById(R.id.fd_web);
        data = new ArrayList<>();
        listViewNotifications = (RecyclerView) findViewById(R.id.faqlistViewNotifications);
        listViewNotifications.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));


        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);



        titles = this.getResources().getStringArray(R.array.question);
        descriptions = this.getResources().getStringArray(R.array.answer);

        for(int i=0; i < titles.length;i++){
            FaqTextListAdapter.Item places = new FaqTextListAdapter.Item(FaqTextListAdapter.HEADER,titles[i]);
            places.invisibleChildren = new ArrayList<>();
            places.invisibleChildren.add(new FaqTextListAdapter.Item(FaqTextListAdapter.CHILD,descriptions[i]));
            data.add(places);
        }

        FaqTextListAdapter adapter = new FaqTextListAdapter(data);
        listViewNotifications.setLayoutManager(llm);
        listViewNotifications.setAdapter(adapter);

        setupActionBar();


        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;
        fd_Web.setPaintFlags(fd_Web.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        fd_Version.setText("Version: 3.6_debug");
        fd_Web.setText("www.freeedrive.com");

        fd_Web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com"));
                startActivity(intentBrowser);
            }
        });
    }


    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//        Toast toast = Toast.makeText(getApplicationContext(),
//                "Item " + (position + 1) + ": " + faqItem.get(position),
//                Toast.LENGTH_SHORT);

    }
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
}

