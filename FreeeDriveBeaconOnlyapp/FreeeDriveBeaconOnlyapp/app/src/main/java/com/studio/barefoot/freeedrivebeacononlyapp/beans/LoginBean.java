package com.studio.barefoot.freeedrivebeacononlyapp.beans;

/**
 * Created by mcs on 12/21/2016.
 */

public class LoginBean {
    private String phone_Number;
    private String device_Id;
    private String gcm_Key;

    public LoginBean() {
        super();
    }

    public String getPhone_Number() {
        return phone_Number;
    }

    public void setPhone_Number(String phone_Number) {
        this.phone_Number = phone_Number;
    }

    public String getDevice_Id() {
        return device_Id;
    }

    public void setDevice_Id(String device_Id) {
        this.device_Id = device_Id;
    }

    public String getGcm_Key() {
        return gcm_Key;
    }

    public void setGcm_Key(String gcm_Key) {
        this.gcm_Key = gcm_Key;
    }
}
