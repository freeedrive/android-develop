package com.studio.barefoot.freeedrivebeacononlyapp.dialogs;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;

/**
 * Created by mcs on 12/28/2016.
 */

public class TurnOnDonotDisturbDialog extends BaseAlertDialog implements DialogInterface.OnClickListener {
   TextView tv_uuid;
    ContentResolver resolver;


    public TurnOnDonotDisturbDialog(Context context) {
        super(context);

        LayoutInflater factory = LayoutInflater.from(context);
        final View progressBarView = factory.inflate(R.layout.custom_dialog_uuid, null);

        setView(progressBarView);
        this.context = context;
        tv_uuid = (TextView) progressBarView.findViewById(R.id.tv_uuid);
        setButton(BUTTON_POSITIVE, context.getString(R.string.action_send), this);
        setButton(BUTTON_NEGATIVE, context.getString(R.string.act_cancel), this);

        setTitle("ACESS DONOT DISTURB MODE");
        resolver = context.getContentResolver();

            tv_uuid.setText("Please allow access to DO NOT DISTURB MODE for proper functioning of the app.");
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which){
            case BUTTON_POSITIVE:
                dialog.dismiss();
                Intent DonotDisturbintent = new Intent(
                        android.provider.Settings
                                .ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);

                context.startActivity(DonotDisturbintent);
               //context.startActivity(new Intent("android.settings.APP_NOTIFICATION_SETTINGS"));
                 dismiss();
                break;
            case BUTTON_NEGATIVE:
                dismiss();
        }

    }

    @Override
    public void dismiss() {
            super.dismiss();
    }
}
