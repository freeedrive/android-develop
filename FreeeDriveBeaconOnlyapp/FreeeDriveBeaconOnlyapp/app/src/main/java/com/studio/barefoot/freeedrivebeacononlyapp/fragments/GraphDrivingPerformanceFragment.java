package com.studio.barefoot.freeedrivebeacononlyapp.fragments;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.GraphDrivingPerformanceAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class GraphDrivingPerformanceFragment extends Fragment {
    Button tripfragment_phone_safety, tripfragment_driving_performance;
    Fragment fragment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    RelativeLayout relativeLayout_trip_phone_saftey, relativeLayoutdriving, relativeLayout_trip_driving_performance, relativeLayoutPhone;
    TextView phonesafety, drivingperformance;
    View rootView;
    static LineChart chart;
    public static ProgressBarDialog progressBarDialogNotifications;
    static JSONObject jsonObject_driverAvg = new JSONObject();
    static JSONObject jsonObject_companyAvg = new JSONObject();
    public GraphDrivingPerformanceFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.driving_performance_fragment, container, false);


        chart = (LineChart) rootView.findViewById(R.id.chart);

        // Inflate the layout for this fragment
        tripfragment_phone_safety = (Button) rootView.findViewById(R.id.tripfragment_phonesafety_btn);
        tripfragment_driving_performance = (Button) rootView.findViewById(R.id.trip_fragment_drivingperformance_btn);

        //Layout intiliaze
        relativeLayout_trip_phone_saftey = (RelativeLayout) rootView.findViewById(R.id.insidecontaintermobilehotspot);
        relativeLayoutdriving = (RelativeLayout) rootView.findViewById(R.id.drivingrelativelayout);
        relativeLayout_trip_driving_performance = (RelativeLayout) rootView.findViewById(R.id.drivingrelativelayout);
        relativeLayoutPhone = (RelativeLayout) rootView.findViewById(R.id.insidecontaintermobilehotspot);

        //textview inttiliaze
        phonesafety = (TextView) rootView.findViewById(R.id.textviewmobilehotSpot);
        drivingperformance = (TextView) rootView.findViewById(R.id.textviewdrivingperformance);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            relativeLayout_trip_driving_performance.setBackground(getActivity().getResources().getDrawable(R.drawable.driving_performance_custom_shape));
        }
        drivingperformance = (TextView) rootView.findViewById(R.id.textviewdrivingperformance);
        drivingperformance.setTextColor(getActivity().getResources().getColor(R.color.white));


        if(AppUtils.isNetworkAvailable()) {

            progressBarDialogNotifications =new ProgressBarDialog(getActivity());
            progressBarDialogNotifications.setTitle(getString(R.string.title_progress_dialog));
            progressBarDialogNotifications.setMessage(getString(R.string.body_progress_dialog));
            progressBarDialogNotifications.show();
            String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER);
            List<NameValuePair> mParams = new ArrayList<NameValuePair>();
            mParams.add(new BasicNameValuePair("phone_number",phoneNumber));
            Log.e("PARAMS", "" + mParams);
            GraphDrivingPerformanceAsyncTask getphonesafetygraphData = new GraphDrivingPerformanceAsyncTask(getActivity(), WebServiceConstants.END_POINT_PERFORMANCE_SCORE,mParams );
            getphonesafetygraphData.execute();
        }
        else if(!DataHandler.getStringPreferences(AppConstants.GRAPH_DRIVING_PERFORMANCE_AVG).isEmpty()){
            String graphValue = DataHandler.getStringPreferences(AppConstants.GRAPH_DRIVING_PERFORMANCE_AVG);
            try {
                JSONObject jsonObj = new JSONObject(graphValue);
                JSONArray graph_driverAvg_jsonArray = jsonObj.getJSONArray("driverAvg");
                JSONArray graph_companyAvg_jsonArray = jsonObj.getJSONArray("companyAvg");

                generateDataLine(graph_driverAvg_jsonArray,graph_companyAvg_jsonArray);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else{
            AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(getActivity());
            error_No_Internet.setMessage(getActivity().getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //NotificationActivity.super.onBackPressed();
                }
            });
            error_No_Internet.show();
        }


        tripfragment_phone_safety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Custom Animation by switching tabs
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    relativeLayout_trip_phone_saftey.setBackground(getActivity().getResources().getDrawable(R.drawable.phonesafety_custom_shape));
                }
                phonesafety.setTextColor(getActivity().getResources().getColor(R.color.white));


                fragment = new GraphPhoneSafetyFragment();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.phone_fragment, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    relativeLayoutdriving.setBackground(getActivity().getResources().getDrawable(R.drawable.drivingperformance_custom_shape));
                }

                drivingperformance.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
            }
        });

        return rootView;
    }


    /**
     * generates a random ChartData object with just one DataSet
     *
     * @return
     */
    public static LineData generateDataLine(JSONArray driverArray , JSONArray companyArray) {
        LineData cd = null;
        String driverAvg_Date = null;
        String driverAvg_Score = null;
        String companyAvg_Date = null;
        String companyAvg_Score = null;
        ArrayList<Entry> e1 = new ArrayList<Entry>();
        ArrayList<Entry> e2 = new ArrayList<Entry>();
        if(driverArray !=null && companyArray !=null){

            if (driverArray !=null) {


                for(int i = 0;i < driverArray.length();i++){

                    try {
                        jsonObject_driverAvg = driverArray.getJSONObject(i);
                        Log.e("DriverAvgObject "," : "+jsonObject_driverAvg);

                        driverAvg_Date = jsonObject_driverAvg.getString("date");
                        Log.e("GRAPH_driverAvg_Date", driverAvg_Date);

                        driverAvg_Score = jsonObject_driverAvg.getString("value");
                        Log.e("GRAPH_drivingAvg_Score",driverAvg_Score);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //float temp_1 = Float.parseFloat(driverAvg_Date);
                    //Log.e("GRAPH_drivingAvg_Date"," FLOAT "+temp_1);
                    float graph_dirver_avg_score = Float.parseFloat(driverAvg_Score);
                    //Log.e("GRAPH_drivingAvg_Score"," FLOAT "+temp_2);
                    e1.add(new Entry(i,graph_dirver_avg_score));

                }
            }

            if(companyArray !=null){

                for(int i=0;i<companyArray.length();i++){
                    try {
                        jsonObject_companyAvg = companyArray.getJSONObject(i);
                        Log.e("CompanyAvgObject "," : "+jsonObject_companyAvg);

                        companyAvg_Date = jsonObject_companyAvg.getString("date");
                        Log.e("companyAvg_Date",companyAvg_Date);
                        companyAvg_Score =jsonObject_companyAvg.getString("value");
                        Log.e("companyAvg_Score",companyAvg_Score);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    float graph_company_avg_score  = Float.parseFloat(companyAvg_Score);
                    //e2.add(new Entry(i, e1.get(i).getY() - 30));
                    e2.add(new Entry(i, graph_company_avg_score));
                }

            }
            LineDataSet d1 = new LineDataSet(e1, "");
            d1.setLineWidth(2.5f);
            //d1.setCircleRadius(4.5f);
            d1.setHighLightColor(Color.rgb(0,191,255));
            d1.setDrawValues(true);
            d1.disableDashedLine();
            d1.setDrawCircleHole(false);

            LineDataSet d2 = new LineDataSet(e2, "");
            d2.setLineWidth(2.5f);
            //d2.setCircleRadius(4.5f);
            d2.setColor(Color.rgb(0,191,255));
            d2.setHighLightColor(Color.rgb(65,105,225));
            //d2.setColor(ColorTemplate.VORDIPLOM_COLORS[0]);
            d2.setCircleColor(Color.rgb(0,191,255));
            d2.setDrawValues(true);
            d2.disableDashedLine();
            d2.setDrawCircleHole(true);

            ArrayList<ILineDataSet> sets = new ArrayList<ILineDataSet>();
            sets.add(d1);
            sets.add(d2);

            // apply styling
            chart.getDescription().setEnabled(false);
            chart.setDrawMarkers(true);
            chart.setDrawGridBackground(false);
            chart.setScaleEnabled(true);




            XAxis xAxis = chart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setEnabled(false);
        /*xAxis.setTypeface(mTf);*/
            xAxis.setDrawGridLines(true);
            xAxis.setDrawAxisLine(true);

            YAxis leftAxis = chart.getAxisLeft();
        /*leftAxis.setTypeface(mTf);*/
            leftAxis.setLabelCount(8, false);
            /*leftAxis.setDrawGridLines(true);*/
            leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
            leftAxis.setAxisMaximum(105f);

            YAxis rightAxis = chart.getAxisRight();
        /*rightAxis.setTypeface(mTf);*/
            rightAxis.setLabelCount(8, false);
            /*rightAxis.setDrawGridLines(true);*/
            rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
            rightAxis.setAxisMaximum(105f);



            // do not forget to refresh the chart
            // holder.chart.invalidate();
            chart.animateX(750);


            cd = new LineData(sets);
            // set data
            chart.setData(cd);


        }
        return cd;
    }


}
