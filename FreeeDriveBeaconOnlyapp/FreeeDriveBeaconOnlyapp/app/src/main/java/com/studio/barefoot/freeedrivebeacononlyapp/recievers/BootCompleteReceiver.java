package com.studio.barefoot.freeedrivebeacononlyapp.recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.services.BackgroundBeaconScan;
import com.studio.barefoot.freeedrivebeacononlyapp.services.Detector;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

/**
 * This class allows to restart Freedrive when device restarts.
 *
 * Created by gtshilombowanticale on 14-07-16.
 */
public class BootCompleteReceiver extends BroadcastReceiver{
    /**
     * BoradcastReceiver method basic, used when the device restarts
     * @param context(in), @Activity represenst the activity
     * @param intent(in), @Intent the represents the inten
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("Bfpk"," *** Boot completed that device is started *** ");

        DataHandler.updatePreferences(AppConstants.KEY_REBOOT_DEVICE,true);

        Intent Detetctorservice = new Intent(context, Detector.class);
        context.startService(Detetctorservice);


        Intent BackgroundBeaconScan = new Intent(context, BackgroundBeaconScan.class);
        context.startService(BackgroundBeaconScan);
    }
}
