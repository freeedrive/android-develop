package com.studio.barefoot.freeedrivebeacononlyapp.fragments;


import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.DrivingPerformanceAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.PhoneSafetDividerDecor;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.RidesBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class TripDrivingPerformanceFragment extends Fragment {

    View tripView;
    Button tripfragment_phone_safety, tripfragment_driving_performance;
    Fragment fragment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    RelativeLayout relativeLayout_trip_phone_saftey, relativeLayoutdriving, relativeLayout_trip_driving_performance, relativeLayoutPhone;
    TextView phonesafety, drivingperformance,emptyRidesTextView;



    RecyclerView listViewDrivingPerformance;
    private ArrayList<RidesBeans> ridesBeansArrayList = new ArrayList<>();
    private DrivingPerformanceAdapter drivingPerformanceAdapter;
    LinearLayoutManager mLayoutManager;
    View rootView;


    public TripDrivingPerformanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_trip_driving_performance, container, false);
        emptyRidesTextView = (TextView) rootView.findViewById(R.id.emptyTextViewForPhoneSafetRides);

        getPhoneSafetyRides();
        listViewDrivingPerformance = (RecyclerView) rootView.findViewById(R.id.list_view_driving_performance);

        mLayoutManager  = new LinearLayoutManager(getActivity());

        listViewDrivingPerformance.setHasFixedSize(true);
        listViewDrivingPerformance.setLayoutManager(mLayoutManager);
        listViewDrivingPerformance.addItemDecoration(new PhoneSafetDividerDecor(getActivity(), LinearLayoutManager.VERTICAL));
        listViewDrivingPerformance.setItemAnimator(new DefaultItemAnimator());
        drivingPerformanceAdapter = new DrivingPerformanceAdapter(ridesBeansArrayList);
        listViewDrivingPerformance.setAdapter(drivingPerformanceAdapter);

        drivingPerformanceAdapter.notifyDataSetChanged();



        tripfragment_phone_safety = (Button) rootView.findViewById(R.id.tripfragment_phonesafety_btn);
        tripfragment_driving_performance = (Button) rootView.findViewById(R.id.trip_fragment_drivingperformance_btn);

        //Layout intiliaze
        relativeLayout_trip_phone_saftey = (RelativeLayout) rootView.findViewById(R.id.insidecontaintermobilehotspot);
        relativeLayoutdriving = (RelativeLayout) rootView.findViewById(R.id.drivingrelativelayout);
        relativeLayout_trip_driving_performance = (RelativeLayout) rootView.findViewById(R.id.drivingrelativelayout);
        relativeLayoutPhone = (RelativeLayout) rootView.findViewById(R.id.insidecontaintermobilehotspot);

        //textview inttiliaze
        phonesafety = (TextView) rootView.findViewById(R.id.textviewmobilehotSpot);
        drivingperformance = (TextView) rootView.findViewById(R.id.textviewdrivingperformance);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            relativeLayout_trip_driving_performance.setBackground(getActivity().getResources().getDrawable(R.drawable.driving_performance_custom_shape));
        }
        drivingperformance = (TextView) rootView.findViewById(R.id.textviewdrivingperformance);
        drivingperformance.setTextColor(getActivity().getResources().getColor(R.color.white));

        tripfragment_phone_safety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Custom Animation by switching tabs
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    relativeLayout_trip_phone_saftey.setBackground(getActivity().getResources().getDrawable(R.drawable.phonesafety_custom_shape));
                }
                phonesafety.setTextColor(getActivity().getResources().getColor(R.color.white));


                fragment = new TripPhoneSafetyFragment();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.trip_fragment, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    relativeLayoutdriving.setBackground(getActivity().getResources().getDrawable(R.drawable.drivingperformance_custom_shape));
                }

                drivingperformance.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
            }
        });

        /*tripfragment_driving_performance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    relativeLayout_trip_driving_performance.setBackground(getActivity().getResources().getDrawable(R.drawable.driving_performance_custom_shape));
                }
                drivingperformance = (TextView) rootView.findViewById(R.id.textviewdrivingperformance);
                drivingperformance.setTextColor(getActivity().getResources().getColor(R.color.white));


                fragment = new TripDrivingPerformanceFragment();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.trip_fragment, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    relativeLayoutPhone.setBackground(getActivity().getResources().getDrawable(R.drawable.phone_safety_custom_shape));
                }
                phonesafety = (TextView) rootView.findViewById(R.id.textviewmobilehotSpot);
                phonesafety.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
            }
        });
*/
        return rootView;
    }
    private void getPhoneSafetyRides() {
        RideBDD tmp = new RideBDD(getContext());
        tmp.open();
        ridesBeansArrayList= tmp.getAllRidesForPhoneSafety();
        tmp.close();
        if (ridesBeansArrayList.size()==0){
            emptyRidesTextView.setVisibility(View.VISIBLE);
        }
    }

}

