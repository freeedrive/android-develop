package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ChangePhoneNumberDialog.progressBarDialogCHngPhn;

/**
 * Created by mcs on 1/5/2017.
 */

public class UnReadMessagesAsyncTask extends BaseAsyncTask {

    /**
     * AsyncTask method basic calls during a request, calls the parent's method.
     */
    protected String doInBackground(String... params) {

        return "";
    }
    public UnReadMessagesAsyncTask(Context context, String route, List<NameValuePair> pp) {
        super(context, route, pp);
    }

    /**
     * AsyncTask method basic calls after a request.
     */
    protected void onPostExecute(String s) {
        super.onPostExecute(s);



        try {
            Log.e("s",s);
            if(s != null) {
                int intResponse = Integer.parseInt(response);
                Log.e("ResponseCode", "" + intResponse);
                Log.e("response", "" + response);
                Log.e("resultat",resultat);
                switch (intResponse) {
                    case 200:

                        try {
                            JSONObject objAccount = new JSONObject(resultat);
                            Log.e("count", "onPostExecute: "+objAccount.getInt("count"));
                            DataHandler.updatePreferences(AppConstants.MESSAGES_COUNT,objAccount.getInt("count"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.e("Response",""+intResponse);
                        break;

                    case 500:

                        //internal error
                        break;
                    case 401:
                        //phone no not found

                        break;


                }
            }else {


            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }

    }
}
