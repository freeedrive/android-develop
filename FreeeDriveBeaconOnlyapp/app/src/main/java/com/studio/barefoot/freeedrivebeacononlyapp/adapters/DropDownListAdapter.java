package com.studio.barefoot.freeedrivebeacononlyapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;


import com.studio.barefoot.freeedrivebeacononlyapp.ProfileActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.RegisterActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.UIUtils;

import java.util.LinkedList;
import java.util.List;

//import com.freeedrive.R;

public class DropDownListAdapter extends BaseAdapter
{
	private Activity mActivity;
	private List<String> mListItems;
	private LayoutInflater mInflater;
	private ViewHolder holder;
	Context c;

	public DropDownListAdapter(Activity la, List<String> items)
	{
		mListItems = new LinkedList<String>();
		mListItems.addAll(items);
		mInflater = LayoutInflater.from(la);
		mActivity = la;
	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mListItems.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Context context = parent.getContext();
		c = context;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.drop_down_list_row, null);
			holder = new ViewHolder();
			holder.tv = (TextView) convertView.findViewById(R.id.tv);
			holder.flags = (ImageView) convertView.findViewById(R.id.flags);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.tv.setText(mListItems.get(position));
		holder.tv.setTypeface(UIUtils.getInstance().getLightFont(mActivity));
		if (position==0){
			holder.flags.setImageResource(R.drawable.uk);
		}else if (position ==1){
			holder.flags.setImageResource(R.drawable.france);
		}else if (position ==2){
			holder.flags.setImageResource(R.drawable.netherlands);

		}else if (position ==3){
			holder.flags.setImageResource(R.drawable.spain);

		}else if (position ==4){
			holder.flags.setImageResource(R.drawable.italy);
		}else if (position ==5){
			holder.flags.setImageResource(R.drawable.denmark);
		}else if (position ==6){
			holder.flags.setImageResource(R.drawable.norway);
		}else if (position ==7){
			holder.flags.setImageResource(R.drawable.germany);
		}else if (position ==8){
			holder.flags.setImageResource(R.drawable.portugal);
		}else if (position ==9){
			holder.flags.setImageResource(R.drawable.finland);
		}
	/*	for (int i=0; i<=mListItems.size();i++){
			if (mListItems.g){
				holder.flags.setImageResource(R.drawable.uk);
			}else if(mListItems.get(i).equalsIgnoreCase("Français")){
				holder.flags.setImageResource(R.drawable.france);
			}else if(mListItems.get(i).equalsIgnoreCase("Dutch")){
				holder.flags.setImageResource(R.drawable.netherlands);
			}else if(mListItems.get(i).equalsIgnoreCase("Español")){
				holder.flags.setImageResource(R.drawable.spain);
			}
		}*/
		return convertView;
	}

	private class ViewHolder {
		TextView tv;
		CheckBox chkbox;
		ImageView flags;
	}
}
