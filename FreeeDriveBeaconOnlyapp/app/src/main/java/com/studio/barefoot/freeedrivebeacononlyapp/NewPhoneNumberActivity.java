package com.studio.barefoot.freeedrivebeacononlyapp;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;

public class NewPhoneNumberActivity extends AppCompatActivity {
     Boolean isThisAnewDevice;
    Button yes,no;

    public NewPhoneNumberActivity(){
        LocaleUtils.updateConfig(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_phone_number);
        isThisAnewDevice = getIntent().getBooleanExtra(AppConstants.IsThisNewDevice,false);
        yes = (Button) findViewById(R.id.btn_yes_new_number);
        no = (Button) findViewById(R.id.btn_no_new_number);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //I have a new device  & a new phone number
/*
               if (isThisAnewDevice){
*/
                   Intent intentNewNumberActivity = new Intent(NewPhoneNumberActivity.this,NewPhoneNumberTextActivity.class);
                   intentNewNumberActivity.putExtra(AppConstants.IS_THIS_NEW_PHONE_NO,true);
                   intentNewNumberActivity.putExtra(AppConstants.IsThisNewDevice,true);

                   startActivity(intentNewNumberActivity);
/*               }else {

                   //I have an old  device  & a new phone number

                   Intent intentNewNumberActivity = new Intent(NewPhoneNumberActivity.this,NewPhoneNumberTextActivity.class);
                   intentNewNumberActivity.putExtra(AppConstants.IS_THIS_NEW_PHONE_NO,true);
                   intentNewNumberActivity.putExtra(AppConstants.IsThisNewDevice,false);
                   startActivity(intentNewNumberActivity);
               }*/

            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // I have a new device but an old phone number
/*
                if (isThisAnewDevice){
*/
                    Intent intentNewNumberActivity = new Intent(NewPhoneNumberActivity.this,LoginActivity.class);
                   startActivity(intentNewNumberActivity);

             /*       intentNewNumberActivity.putExtra(AppConstants.IS_THIS_NEW_PHONE_NO,false);
                    intentNewNumberActivity.putExtra(AppConstants.IsThisNewDevice,true);

                }else {
                    // I have an old device and old phone number
                    Intent intentNewNumberActivity = new Intent(NewPhoneNumberActivity.this,LoginActivity.class);
                    intentNewNumberActivity.putExtra(AppConstants.IS_THIS_NEW_PHONE_NO,false);
                    intentNewNumberActivity.putExtra(AppConstants.IsThisNewDevice,false);
                    startActivity(intentNewNumberActivity);
                }*/
            }
        });
        setupActionBar();
    }
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }
}
