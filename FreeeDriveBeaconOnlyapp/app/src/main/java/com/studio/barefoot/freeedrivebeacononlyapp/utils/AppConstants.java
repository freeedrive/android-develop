package com.studio.barefoot.freeedrivebeacononlyapp.utils;

import com.studio.barefoot.freeedrivebeacononlyapp.beans.SubRideBeans;

import java.util.ArrayList;

/**
 * Created by mcs on 12/21/2016.
 */

public class AppConstants {
    public static final String USER_ACCEPTED_PRIVACY ="USER_ACCEPTED_PRIVACY";
    public static final String USER_AWAITING_QR_CODE ="USER_AWAITING_QR_CODE";
    public static final String USER_AWAITING_SMS ="USER_AWAITING_SMS";
    public static final String RIDE_WAS_ACTIVE ="RIDE_WAS_ACTIVE";
    public static final String WABCO_ACCOUNT_VERIFY = "WABCO_ACCOUNT_VERIFY";
    public static final String KEY_ARRIVAL ="KEY_ARRIVAL";
    public static final String KEY_ARRIVAL_LONG ="KEY_ARRIVAL_LONG";
    public static final String KEY_DEPARTURE ="KEY_DEPARTURE";
    public static final String KEY_DEPARTURE_LONG ="KEY_DEPARTURE_LONG";
    public static final String REDUCTED_ARRIVAL ="REDUCTED_ARRIVAL";
    public static final String KEY_AUTOREPLY = "KEY_AUTOREPLY";
    public static final String KEY_PASSENGER_MODE = "KEY_PASSENGER_MODE";
    public static final String KEY_AUTOREPLY_MSG ="KEY_AUTOREPLY_MSG";
    public static final String ARRIVAL_TIME ="ARRIVAL_TIME";
    public static final String INSURANCE_POLICY_KEY ="INSURANCE_POLICY_KEY";
    public static final String GRAPH_WEEKLY_SAFETY_AVG = "GRAPH_WEEKLY_SAFETY_AVG";
    public static final String GRAPH_WEEKLY_DRIVERS_AVG = "GRAPH_WEEKLY_DRIVERS_AVG";
    public static final String GRAPH_WEEKLY_COMPANYs_AVG = "GRAPH_WEEKLY_COMPANYs_AVG";
    public static final String GRAPH_WEEKLY_COMPANYs_AVG_TODAY = "GRAPH_WEEKLY_COMPANYs_AVG_TODAY";
    public static final String GRAPH_WEEKLY_COMPANYs_NAME = "GRAPH_WEEKLY_COMPANYs_NAME";
    public static final String GRAPH_WEEKLY_DRIVERs_AVG_TODAY = "GRAPH_WEEKLY_DRIVERs_AVG_TODAY";
    public static final String GRAPH_DRIVING_PERFORMANCE_AVG = "GRAPH_DRIVING_PERFORMANCE_AVG";
    public static final String TEMP_PROFILE_KEY ="TEMP_PROFILE_KEY";
    public static final String TEMP_DISPLAY_KEY ="TEMP_DISPLAY_KEY";
    //forscoring purposes
    public static final String SCORE_TOTAL_BAD_COUNTS ="SCORE_TOTAL_BAD_COUNTS";
    public static final String SCORE_TOTAL_ELAPSED_TIME ="SCORE_TOTAL_ELAPSED_TIME";
    public static final String SCORE_TOTAL_SCORE ="SCORE_TOTAL_SCORE";

    public static final String SCORE_TIME_KEY = "SCORE_TIME_KEY";
    public static final String FIRST_TIME_USER ="FIRST_TIME_USER_PREF_KEY";
    public static final String PREF_KEY_LANG ="PREF_KEY_LANG";
    public static final String PREF_KEY_DRIVING_PERFORMANCE_ACTIVE ="PREF_KEY_DRIVING_PERFORMANCE_ACTIVE";
    public static final String PREF_KEY_DRIVING_PERFORMANCE_SCORE ="PREF_KEY_DRIVING_PERFORMANCE_SCORE";
    public static final String PREF_KEY_DRIVE_PAD_NAME= "PREF_KEY_DRIVE_PAD_NAME";
    public static final String PREF_KEY_ACCIDENT_HELP_EMAIL = "PREF_KEY_ACCIDENT_HELP_EMAIL";
    public static final String PREF_KEY_ACCIDENT_HELP_PHONE = "PREF_KEY_ACCIDENT_HELP_PHONE";
    public static final String FIRE_BASE_TOKE ="FIRE_BASE_TOKE";
    public static final String PREF_KEY_LOGIN_ACTIVITY ="IsUserLoggedIn";
    public static final String KEY_REBOOT_DEVICE ="deviceRebooted";
    public static final String PREF_KEY_FD_STATUS = "PREF_KEY_FD_STATUS";
    public static final String PREF_KEY_APP_TERMINATED = "PREF_KEY_APP_TERMINATED";
    public static final String PREF_CURRENT_LATITUDE = "PREF_CURRENT_LATITUDE";
    public static final String PREF_CURRENT_LONGTITUDE = "PREF_CURRENT_LONGTITUDE";
    /*public static final String TITLE_LOCATION_SERVICE ="Location Check ";
    public static final String BODY_LOCATION_SERVICE_ON ="Location Service ON!";
    public static final String BODY_LOCATION_SERVICE_OFF = "Location Service OFF";*/


    public static final String PHONE_NUMBER ="PHONE_NUMBER";
    public static final String PHONE_TEMP ="PHONE_TEMP";
    public static final String TOKEN_NUMBER ="TOKEN";
    public static final String PHONE_NUMBER_TEMP = "PHONE_NUMBER_TEMP";
    public static final String UUID ="UUID";
    public static final String TEMP_UUID = "TEMP_UUID";
    public static final String TEMP_UUID_NAME_SPACE="TEMP_UUID_NAME_SPACE";
    public static final String IsThisNewDevice="IsThisNewDevice";
    public static final String IS_THIS_NEW_PHONE_NO="IS_THIS_NEW_PHONE_NO";

    public static final String TEMP_UUID_INSTANCE_ID = "TEMP_UUID_INSTANCE_ID";
    public static final String UUID_iBEACON ="UUID_iBEACON";
    public static final String UUID_NAME_SPACE ="UUID_NAME_SPACE";
    public static final String UUID_INSTANCE_ID ="UUID_INSTANCE_ID";
    public static final String IBKS_SERIAL_NO ="IBKS_SERIAL_NO";
    public static final String TEMP_IBKS_SERIAL_NO="TEMP_IBKS_SERIAL_NO";
    public static final String ACCESS_PRIVATE_HOURS_UPDATED = "ACCESS_PRIVATE_HOURS_UPDATED";
    public static final String ACCESS_PRIVATE_HOURS = "ACCESS_PRIVATE_HOURS";
    public static final String QR_CODE_RECOVERY = "QR_CODE_RECOVERY";
    public static final String QR_CODE_RECOVERY_key = "QR_CODE_RECOVERY_key";
    public static final String PRIVACY_DESCION = "privacyDescionReturned";
    public static final String MESSAGES_COUNT = "MESSAGES_COUNT";
    public static final String INTENT_KEY_MENU = "INTENT_KEY_MENU";
    public static final String PIXELS_DIFFERENCE = "PIXELS_DIFFERENCE";
    public static final String DEMO_MODE = "DEMO_MODE";
    public static final String COUNT_DEMO_RIDES = "COUNT_DEMO_RIDES";
    public static final String RIDE_BUTTON_PRESSED = "RIDE_BUTTON_PRESSED";
    public static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    public static final String DEPARTURE_LAT = "DEPARTURE_LAT";
    public static final String DEPARTURE_LANG = "DEPARTURE_LANG";
    public static final String ARRIVAL_LAT = "ARRIVAL_LAT";
    public static final String ARRIVAL_LANG = "ARRIVAL_LANG";
    public static final String RIDE_SCORE = "RIDE_SCORE";
    public static final String IS_BLUETOOTH = "IS_BLUETOOTH";
    public static  Boolean isLocationActive =false;

    public static final long TIMEOUT_CONNECT = 20;
    public static final long TIMEOUT_CONNECT_VIDEO = 5;
    public static final long READ_TIMEOUT = 180;
    public static int ORPHAN_RIDE_BIT = 0;
   // public final static String REG_EMIAL = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public final static String REG_EMIAL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";;

    public static ArrayList<SubRideBeans> subRideBeansArrayList = new ArrayList<>();
    public static String PREF_KEY_SCREEN_SIZE="PREF_KEY_SCREEN_SIZE";
    public static String DEMO_MODE_SUBSCRIPTION_EXPIRED = "DEMO_MODE_SUBSCRIPTION_EXPIRED";
    public static float currentSpeed = 0.0f;
    public static boolean passengerModeFlag = false;
    public static long  totalminutesPassengerMode = 0;
    public static boolean switchCheck = false;
    public static long minutesPassengerSwitchOff = 0;
    public static boolean ispassengerSwitch = false;
    public static long currentTime = 0;
    public static  boolean  broadcastFlag = true;

}
