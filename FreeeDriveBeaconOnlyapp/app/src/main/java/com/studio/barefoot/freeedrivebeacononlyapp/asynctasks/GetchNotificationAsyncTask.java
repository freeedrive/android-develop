package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.DividerItemDecoration;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.ExpandableListAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.MessagesAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.NotificationBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesActivity.deleteMessages;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesActivity.listNotifications;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesActivity.listViewNMessages;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesActivity.progressBarDialogNotifications;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesActivity.selectAll;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesActivity.tv_noNotifications;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.fdLogout;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler.FILE_NAME_SHARED_PREF;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler.deletePreference;

/**
 * Created by mcs on 12/23/2016.
 */

public class GetchNotificationAsyncTask extends BaseAsyncTask {

    JSONArray jsonArray;
    //public static ArrayList<String> ListOfIDs= new ArrayList<>();
    ExpandableListAdapter mAdapter;
    public static  MessagesAdapter testMEssagesAdapter;


    public GetchNotificationAsyncTask(Context context,String rout,List<NameValuePair> pp) {
        super(context,rout,pp);
    }


    /**
     * AsyncTask method basic calls during a request, the parent's method is called
     */
    protected String doInBackground(String... params) {
        return "";
    }


    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s != null){
            int intResponse = Integer.parseInt(response);
            List<ExpandableListAdapter.Item> data;
            JSONObject   objAccount= new JSONObject();
            //  listViewNotifications.setAdapter(notificationsHistoryAdapter);
            RideBDD tmp = new RideBDD(context);

            data = new ArrayList<>();
            switch (intResponse) {
                case 200:

                    try {
                        if (resultat!=null || !resultat.isEmpty()) {
                            jsonArray = new JSONArray(resultat);
                             Log.e("jsonArray",""+jsonArray);
                            Log.e("InsideNotificationTask","InsideNotificationTask");
                            listNotifications = new ArrayList<>();
                             Boolean toInsert = false;
                            // DataHandler.deletePreference(AppConstants.NOTIFICATION_KEY);
                             if (jsonArray.length()>0) {
                                 tmp.open();
                                 int count =tmp.sumMessageCount();
                                if (count>0){
                                    tmp.deleteMessageInDb();
                                }
                                 tmp.close();

                                 for (int i = jsonArray.length() - 1; i >= 0; i--) {

                             /*        for (int i = 0;i<jsonArray.length();i++) {*/
                                         NotificationBeans notificationBeans =new NotificationBeans();

                                        objAccount = jsonArray.getJSONObject(i);

                                      notificationBeans.setDateTime(objAccount.getString("scheduled_at"));
                                      notificationBeans.setTitle(objAccount.getString("title"));
                                      notificationBeans.setBody(objAccount.getString("message"));
                                         notificationBeans.setMessagesID(objAccount.getInt("id"));

                                   /*  ExpandableListAdapter.Item places = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, objAccount.getString("scheduled_at") + " | " + objAccount.getString("title").toUpperCase());
                                     places.invisibleChildren = new ArrayList<>();
                                     places.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, objAccount.getString("message")));
                                     data.add(places);*/
                                     //ListOfIDs.add(objAccount.getString("id"));
                                        //listNotifications.add(notificationBeans);
                                        InsertMessagesInDB(objAccount.getInt("id"),objAccount.getString("title"),objAccount.getString("message"),objAccount.getString("scheduled_at"));

                                 }


                                 //Log.e("ListOfIDs",ListOfIDs.toString());

                             }
                            tmp.open();
                            MessagesActivity.areAllSelected = false;
                            selectAll.setChecked(false);
                            listNotifications =new ArrayList<>();
                            listNotifications = tmp.getAllMessagesFromDB();
                            DataHandler.updatePreferences(AppConstants.MESSAGES_COUNT,0);
                            listViewNMessages.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
                            testMEssagesAdapter = new MessagesAdapter(listNotifications);
                            listViewNMessages.setAdapter(testMEssagesAdapter);
                            testMEssagesAdapter.notifyDataSetChanged();
                            tmp.close();

                           // listViewNotifications.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
/*                            listViewNMessages.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));


                             mAdapter = new ExpandableListAdapter(data);
                            //listViewNotifications.setAdapter(mAdapter);
                            listViewNMessages.setAdapter(mAdapter);*/

                            if (jsonArray.length()<=0){
                                try{
                                    tv_noNotifications.setText(context.getResources().getString(R.string.emptyMessages));
                                    tv_noNotifications.setVisibility(View.VISIBLE);
                                    deleteMessages.setVisibility(View.INVISIBLE);
                                    selectAll.setVisibility(View.INVISIBLE);
                                }catch (OutOfMemoryError memoryError){
                                    memoryError.printStackTrace();
                                }

                            }
                            try{
                                progressBarDialogNotifications.dismiss();
                                progressBarDialogNotifications=null;
                            }catch (NullPointerException exception){
                                exception.printStackTrace();
                            }
                           // DataHandler.updatePreferences(AppConstants.NOTIFICATION_KEY, notificationBeans);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case 400:
                    //Phone numbre not found
                    AlertDialog.Builder error_400 = new AlertDialog.Builder(context);
                    error_400.setMessage(context.getResources().getString(R.string.error_auth_token_expire_400)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deletePreference(FILE_NAME_SHARED_PREF);
                            fdLogout(context);
                            DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY,false);
                            dialog.dismiss();
                        }
                    });
                    error_400.show();
                    break;
            }
            try{
                progressBarDialogNotifications.dismiss();
                progressBarDialogNotifications=null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }
        }else {
            try{
                progressBarDialogNotifications.dismiss();
                progressBarDialogNotifications=null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }

        }

    }

    private void updateMessages(int id, String title, String message, String scheduled_at) {
    }


    private void InsertMessagesInDB(int id, String title, String messages, String scheduled_at) {
        RideBDD tmp = new RideBDD(context);
        tmp.open();
        tmp.InsertMessages(id,title,messages,scheduled_at);
        tmp.close();
    }
}
