package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.SmsConfirmationActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

import org.apache.http.NameValuePair;

import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.NewPhoneNumberTextActivity.progressBarDialogCHngPhn;


/**
 * Created by mcs on 1/5/2017.
 */

public class UpdatePrivateHoursAsyncTask extends BaseAsyncTask {

    /**
     * AsyncTask method basic calls during a request, calls the parent's method.
     */
    protected String doInBackground(String... params) {

        return "";
    }
    public UpdatePrivateHoursAsyncTask(Context context, String route, List<NameValuePair> pp) {
        super(context, route, pp);
    }

    /**
     * AsyncTask method basic calls after a request.
     */
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        try{
            progressBarDialogCHngPhn.dismiss();
            progressBarDialogCHngPhn =null;
        }catch (NullPointerException exception){
            exception.printStackTrace();
        }

        Log.e("s",s);
        if(s != null) {
            int intResponse = Integer.parseInt(response);
            Log.e("ResponseCode", "" + intResponse);
            Log.e("response", "" + response);
            Log.e("resultat",resultat);
            switch (intResponse) {
                case 200:
                    DataHandler.updatePreferences(AppConstants.ACCESS_PRIVATE_HOURS_UPDATED,true);
                    Log.e("Response",""+intResponse);
                    break;

                case 500:
                    AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                    error_500.setMessage(context.getResources().getString(R.string.error_sms_500)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_500.show();
                    //internal error
                    break;
                case 401:
                    //phone no not found
                    AlertDialog.Builder error_401 = new AlertDialog.Builder(context);
                    error_401.setMessage(context.getResources().getString(R.string.error_changePhone_401)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_401.show();
                    break;


            }
        }else {
            try{
                progressBarDialogCHngPhn.dismiss();
                progressBarDialogCHngPhn =null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }

        }
    }
}
