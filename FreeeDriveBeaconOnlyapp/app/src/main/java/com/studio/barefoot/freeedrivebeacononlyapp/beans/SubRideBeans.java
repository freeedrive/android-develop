package com.studio.barefoot.freeedrivebeacononlyapp.beans;

/**
 * Created by mcs on 12/23/2016.
 */

public class SubRideBeans {
    String areaType ="";
    String Startlat ="";
    String Startlng ="";

    String Endlat ="";
    String Endlng ="";
    int TimeInsideAregion =0;

    long currentTime ;

     float currentSpeed ;


    public SubRideBeans() {
        super();
    }

    public int getTimeInsideAregion() {
        return TimeInsideAregion;
    }

    public void setTimeInsideAregion(int timeInsideAregion) {
        this.TimeInsideAregion = timeInsideAregion;
    }

    public String getAreaType() {
        return areaType;
    }

    public void setAreaType(String areaType) {
        this.areaType = areaType;
    }

    public String getStartlat() {
        return Startlat;
    }

    public void setStartlat(String startlat) {
        Startlat = startlat;
    }

    public String getStartlng() {
        return Startlng;
    }

    public void setStartlng(String startlng) {
        Startlng = startlng;
    }

    public String getEndlat() {
        return Endlat;
    }

    public void setEndlat(String endlat) {
        Endlat = endlat;
    }

    public String getEndlng() {
        return Endlng;
    }

    public void setEndlng(String endlng) {
        Endlng = endlng;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    public float getCurrentSpeed() {
        return currentSpeed;
    }

    public void setCurrentSpeed(float currentSpeed) {
        this.currentSpeed = currentSpeed;
    }
}
