package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;


import android.content.Context;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LoggingOperations;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.fdLogout;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.formateLongToOnlyDateForServer;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler.FILE_NAME_SHARED_PREF;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler.deletePreference;


/**
 * Created by ttwyf on 1/20/2017.
 */

public class ScoreAsynctask extends AsyncTask<Void,Void,Void> {
    /**
     * Url server
     */
    private static final String mUrl =  WebServiceConstants.WEBSERVICE_URL_PREFIX+WebServiceConstants.END_POINT_SCORE;
            //"http://xyperdemos.com/pk_freeedrive_backend-master/public/api/score";
    /**
     * Represents the account token
     */
    private String token;
    /**
     * Represents the response code from server
     */
    private int responseCode;
    /**
     * Represents the resultat from server
     */
    private String resultat;
    /**
     * Represents the liste that contains all the rides
     */
    private JSONArray data;

    /**
     * Represents the liste that contains all the SUBrides
     */
    private JSONArray subdata;

    /**
     * Represent the device id string
     */
    String deviceId = "";
    /**
     * Represents the context, the Activity
     */
    private Context context;
    /**
     * Represents the database
     */
    RideBDD rideBDD;

    public  Timer drivingPerformanceCallTimer;


    public ScoreAsynctask(JSONArray listRide,JSONArray listSubRides,Context context,String deviceId){
        //Log.i("Adneom","(MenuActivity) list is "+listRide);
        this.data = listRide;
        this.subdata = listSubRides;
        this.deviceId = deviceId;

     /*    if (listSubRides.length()>0){
             this.subdata = listSubRides;
         }*/

        this.context = context;
        token = "Bearer "+ DataHandler.getStringPreferences(AppConstants.TOKEN_NUMBER);
        Log.e("AuthToken",token);
        //db:
        rideBDD = new RideBDD(context);
        rideBDD.open();
    }
    @Override
    protected Void doInBackground(Void... voids) {
        try {
            URL url = new URL(mUrl);
            Log.e("Url",""+url);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            //urlConnection.setUseCaches(false);

            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Authorization", token);
            urlConnection.setRequestProperty("X-Requested-With", "XMLHttpRequest");

            //Expermentation for json arrays

     /*       String listofRides = this.data.toString();
            String listofSubRides = this.subdata.toString();

            Log.e("listofRides",listofRides);
            Log.e("listofSubRides",listofSubRides);*/

            //json in body:
            JSONObject dataObj = new JSONObject();
            dataObj.put("data", this.data);

            dataObj.put("subData",this.subdata);
            dataObj.put("devID",this.deviceId);
            dataObj.put("app_version", "2.5");
            Log.e("Bundle", "" + dataObj.toString());
            byte[] outputInBytes = dataObj.toString().getBytes("UTF-8");
            Log.e("x",""+dataObj);
            Log.e("dataObj",""+outputInBytes);
            OutputStream os = urlConnection.getOutputStream();
            os.write(outputInBytes);
            Log.e("os",""+os);
            os.close();

            responseCode= urlConnection.getResponseCode();
            String message =  urlConnection.getResponseMessage();
            Log.e("Response Message :",message);
            Log.e("Response Code :",""+responseCode);

            InputStream inputStream;
            // get stream
            if (responseCode< HttpURLConnection.HTTP_BAD_REQUEST) {
                Log.i("Adneom","Response code (SYNC) is "+responseCode+" and is OK");
                inputStream = urlConnection.getInputStream();
            } else {
                inputStream = urlConnection.getErrorStream();
                Log.i("Adneom","Response code (SYNC) is "+responseCode+" and is NOT OK");
            }

            // parse stream
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String temp, response = "";
            while ((temp = bufferedReader.readLine()) != null) {
                response += temp;
            }
            resultat = response;
            Log.e("resultAt",""+resultat);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.i("Adneom","Error url : "+e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("Adneom", "Error open connection : "+e.getMessage());
        } catch (JSONException e) {
            e.printStackTrace();
            Log.i("Adneom", "Error JSON OBJECT : " + e.getMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        try {

            Long currentTime_response =  System.currentTimeMillis();
            String timea_response=  formateLongToOnlyDateForServer(currentTime_response);
            LoggingOperations.writeToFile(context,"I'm PUSHING THE RIDE ON THE SERVER WITH RESPONSE > " + timea_response +" " +responseCode);
            switch (responseCode) {

                case 200:
                    //Log.i("Adneom","(MenuActivity) code OK is "+responseCode+" \n from server : "+resultat);
                    try {
                        //update_rides_db(new JSONObject(resultat));
                        rideBDD.open();
                        Long currentTime =  System.currentTimeMillis();
                        String timea=  formateLongToOnlyDateForServer(currentTime);
                        LoggingOperations.writeToFile(context,"I PUSHED THE RIDE ON THE SERVER WITH RESPONSE > "+timea +" "+responseCode);
                        rideBDD.updateRide(1, 1);
                        rideBDD.updateSubRide(0, 0);
                        rideBDD.close();

                      /*  if (DataHandler.getBooleanPreferences(AppConstants.ACCESS_PRIVATE_HOURS_UPDATED)==false){
                            List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                            final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                            String deviceId = telephonyManager.getDeviceId();
                            mParams.add(new BasicNameValuePair("devID",deviceId));
                            mParams.add(new BasicNameValuePair("phone_number",DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
                            *//*mParams.add(new BasicNameValuePair("private_hour_enable",String.valueOf(DataHandler.getIntPreferences(AppConstants.ACCESS_PRIVATE_HOURS))));*//*
                            mParams.add(new BasicNameValuePair("token",DataHandler.getStringPreferences(AppConstants.TOKEN_NUMBER)));
                            Log.e("PARAMS", "" + mParams);
                            UpdatePrivateHoursAsyncTask updatePrivateHoursAsyncTask = new UpdatePrivateHoursAsyncTask(context, WebServiceConstants.END_POINT_UPDATE_PRIVATE_HOURS,mParams);
                            updatePrivateHoursAsyncTask.execute();

                        }*/
                        String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER);
                        String logFile="";
                        File f = new File(LoggingOperations.getLogFile().toString());

                        try {
                            //File f = new File(LoggingOperations.getLogFile().toString());
                            FileInputStream is = new FileInputStream(f);
                            int size = is.available();
                            byte[] buffer = new byte[size];
                            is.read(buffer);
                            is.close();
                            logFile = new String(buffer);
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                UploadLogsAsyncTask uploadLogsAsyncTask = new UploadLogsAsyncTask(f,phoneNumber);
                uploadLogsAsyncTask.execute();
                        //PerformanceTimer();

                    /*AppUtils.isUpdated = true;*/

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.i("Adneom", " (MenuActivity) error is " + e.getMessage());
                        Log.e("E", "error : " + e.getMessage());
                    }
                /*ScoreSynchronizationActivity.progressBarDialogNotifications.dismiss();
                ScoreSynchronizationActivity.progressBarDialogNotifications = null;*/
                    break;
                case 422:
                    Log.i("Adneom", "(MenuActivity) code NOT OK is " + responseCode + " \n from server : " + resultat);
                case 409:
                    Log.i("Adneom", "(MenuActivity) code NOT OK is " + responseCode + " \n from server : " + resultat);
                    break;
                case 400:
                    deletePreference(FILE_NAME_SHARED_PREF);
                    fdLogout(context);
                    DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY,false);
                   /* AlertDialog.Builder error_400 = new AlertDialog.Builder(context);
                    error_400.setMessage(context.getResources().getString(R.string.error_auth_token_expire_400)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    });
                    error_400.show();*/
                    break;
                case 500:
                    Log.i("Adneom", "(MenuActivity) code NOT OK is " + responseCode + " \n from server : " + resultat);
                    break;
                default:
                    try {

                    } catch (NullPointerException exception) {
                        exception.printStackTrace();
                    }

                    break;
            }
        }catch (Exception exception){
                exception.printStackTrace();
            }


        }

    private void PerformanceTimer() {
        drivingPerformanceCallTimer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Log.e("Inside","PerformanceTimer");
                final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                String deviceId = telephonyManager.getDeviceId();
                List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                mParams.add(new BasicNameValuePair("devID",deviceId));
                mParams.add(new BasicNameValuePair("phone_number",DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
                FetchDrivingPerformanceScores fetchDrivingPerformanceScores = new FetchDrivingPerformanceScores(context, WebServiceConstants.END_POINT_SPEEDING_SCORE,mParams);
                fetchDrivingPerformanceScores.execute();
                Log.e("params",""+mParams);
            }
        };
        drivingPerformanceCallTimer.schedule(timerTask,60000);
    }

    /**
     * Allows to update all tides after synchronization
     * @param obj represents the json object from server.
     */
    private void update_rides_db(JSONObject obj){
        if(obj != null){
            //Log.i("Adneom"," Tab  : "+obj.toString());
            try {
                JSONArray tabRides = new JSONArray(obj.get("data").toString());
                if(tabRides != null && tabRides.length() > 0){
                    for(int i = 0; i < tabRides.length();i++){
                        try {
                            JSONObject ob = tabRides.getJSONObject(i);
                            //if value exists :
                            if(ob.has("remote_id")){
                                //Log.i("Adneom","(MenuActivity) remote_id is "+ob.getInt("remote_id"));
                                rideBDD.updateRide(ob.getInt("remote_id"), 1);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.i("Adneom","error json object is "+e.getMessage());
                        }
                    }

                    Log.i("Adneom"," ** (MenuActivity) all rides are updated in db : "+rideBDD.getAllRides(false).toString()+" ** ");
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Log.i("Adneom","error conversion JSON : "+e.getMessage());
            }
        }
        rideBDD.close();
    }
}

