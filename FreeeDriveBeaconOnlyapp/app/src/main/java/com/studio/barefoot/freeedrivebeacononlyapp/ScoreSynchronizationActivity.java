package com.studio.barefoot.freeedrivebeacononlyapp;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.ScoreAsynctask;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LoggingOperations;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.formateLongToOnlyDateForServer;

/**
 * Created by ttwyf on 1/19/2017.
 */

public class ScoreSynchronizationActivity extends Activity {

    public static ProgressBarDialog progressBarDialogNotifications;

    /**
     * Represents the database
     */
    public ScoreSynchronizationActivity() {
        LocaleUtils.updateConfig(this);
    }

    RideBDD rideBDD;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rideBDD = new RideBDD(this);
        rideBDD.open();
        JSONArray jsonArrayRides = new JSONArray();
        JSONArray jsonArrayLastSubRides = new JSONArray();
        jsonArrayRides = rideBDD.getAllRidesForSyncing();
        jsonArrayLastSubRides = rideBDD.getAllSubRidesForSyncing();
//        String strListe = String.valueOf(rideBDD.listeWithLocality);
//
        Log.e("strListe", "" + jsonArrayRides);
        try {
            if (jsonArrayRides != null && jsonArrayRides.length() > 0) {
                manageSynchronization(jsonArrayRides, jsonArrayLastSubRides);
                progressBarDialogNotifications = new ProgressBarDialog(this);
                progressBarDialogNotifications.setTitle(getString(R.string.title_progress_dialog));
                progressBarDialogNotifications.setMessage(getString(R.string.sync_body_progress_dialog));
                progressBarDialogNotifications.show();
            } else {
                Toast.makeText(this, "There is no Ride to Sync !Thanks", Toast.LENGTH_SHORT).show();
                this.finish();
                Log.e("something went wrong", "");
            }
        } catch (NullPointerException exception) {
            exception.printStackTrace();
        }


    }


    /**
     * Allows to execute the synchronization request and update all rides after synchronization.
     */
    private void manageSynchronization(JSONArray liste, JSONArray listSubRides) {
        //Allows to execute request if the listes are not empty :
        Boolean canExecuteAsynctaslk = true;

        //db:
        rideBDD = new RideBDD(ScoreSynchronizationActivity.this);
        rideBDD.open();
        //get all rides :
        JSONArray listeRides = liste;  //JSONArray listeRides = rideBDD.getAllRides(true);
        JSONArray newListeRides = null;

        //listes :
        if (listeRides == null || listeRides.length() == 0) {
            //Log.i("Adneom","(MenuActivity) list is "+listeRides);
            canExecuteAsynctaslk = false;
        } else {
            //keep just good rides :
            //newListeRides = containsCoordinates(listeRides);
            newListeRides = liste;
            if (newListeRides == null || newListeRides.length() == 0) {
                Log.i("Adneom", "(MenuActivity) newlistRides is " + newListeRides);
                canExecuteAsynctaslk = false;
            }
        }

        //execute request :
        if (canExecuteAsynctaslk) {
            final TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String deviceId = telephonyManager.getDeviceId();
            Long currentTime =  System.currentTimeMillis();
            String timea=  formateLongToOnlyDateForServer(currentTime);
            LoggingOperations.writeToFile(this,"WHEN ENDING THE RIDE THESE WERE MY VALUES > "+timea +" "+newListeRides.toString());
            ScoreAsynctask synchronizationMAsynctask = new ScoreAsynctask(newListeRides, listSubRides, this,deviceId);
            synchronizationMAsynctask.execute();
        } else {
            //listeRides or newListeRides are null or empty :
            try {
                if (progressBarDialogNotifications != null && progressBarDialogNotifications.isShowing()) {
                    progressBarDialogNotifications.cancel();
                }
            } catch (NullPointerException exception) {

            }

        }
    }

    /**
     * Test if a ride in the list contains a coordinate et no a locality,
     * if yes we delete the list, we send just the ride with locality
     *
     * @param liste,@JSONArray is a list of rides
     * @return @JSONArray, the new liste that contains only tjhe rides with their locality
     */
    public static JSONArray containsCoordinates(JSONArray liste) {
        JSONArray newListe = null;
        if (liste != null && liste.length() > 0) {
            //Log.i("Adneom","current liste is "+liste.toString());
            newListe = new JSONArray();
            for (int i = 0; i < liste.length(); i++) {
                JSONObject jsonObject = null;
                //Log.i("Adneom","liste is "+liste);
                try {
                    jsonObject = liste.getJSONObject(i);
                    if (jsonObject != null && jsonObject.has("departure_location") && jsonObject.has("arrival_location")) {
                        Boolean depConatainsCoord = false;
                        Boolean arrConatainsCoord = false;
                        String departure = jsonObject.getString("departure_location");
                        String arrival = jsonObject.getString("arrival_location");

                        //not null and if not empty or contains "," : true
                        if (departure != null && (departure.equals("") || departure.contains(","))) {
                            depConatainsCoord = true;
                        }
                        if (arrival != null && (arrival.equals("") || arrival.contains(","))) {
                            arrConatainsCoord = true;
                        }
                        //if departure and arrival doesn't contain "," : add
                        if (!depConatainsCoord && !arrConatainsCoord) {
                            newListe.put(jsonObject);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        //Log.i("Adneom"," new liste is "+newListe.toString());
        return newListe;
    }
}
