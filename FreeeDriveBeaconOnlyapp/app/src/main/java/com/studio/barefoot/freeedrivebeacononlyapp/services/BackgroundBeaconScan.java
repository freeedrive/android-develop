package com.studio.barefoot.freeedrivebeacononlyapp.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.studio.barefoot.freeedrivebeacononlyapp.MenuActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LoggingOperations;

import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.powersave.BackgroundPowerSaver;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.altbeacon.beacon.startup.RegionBootstrap;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;


import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.PREF_KEY_APP_TERMINATED;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.RIDE_WAS_ACTIVE;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.currentSpeed;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.formateLongToOnlyDateForServer;

/**
 * Created by mcs on 12/7/2016.
 */

public class BackgroundBeaconScan extends Service implements BootstrapNotifier, BeaconConsumer {
    private static final String TAG = "BackgroundScan";
    private BeaconManager mBeaconManager;
    //private BackgroundPowerSaver
    private RegionBootstrap regionBootstrap;
    Region regions[];
    public boolean disconnect = false;
    public static int beaconlostCounter = 0;
    private int enterCount = 0;
    public static ForegroundLocationService mService = null;

    /**
     * Allows to handle database
     */
    // private RideBDD rideBDD;

    //
    // private Ride rideFromDB;

    Boolean rideIsActive = false;
    Boolean rideWasFininshed = false;

    private boolean userAlreadyLoggedIn = false;
    private int count = 0;
    public NotificationManager notificationManager;
    private Drawable drawable;
    private String namespace = "";
    private String instanceID = "";
    public static Timer beaconFd;
    public static boolean beaconlost = false;
    private String postiBeaconUUID = "";
    public static boolean beaconlostPermanently = false;
    String notificationLanguage = "";
    String contextTitle = "";
    String contentText = "";
    public static boolean beaconenterregionCounter = true;
    private int exitCount = 0;
    private int exitedPermanentlyCount = 0;
    // Monitors the state of the connection to the service.
    private ServiceConnection mServiceConnection = null;

    @Override
    public void onCreate() {
        super.onCreate();
        AppUtils.isBGServiceActive = true;
        //AppUtils.appTerminated = false;
        Log.e("Beacon", "Service Start");
        Log.e("Bfpk", " *** Beacon  Service is started *** ");
        LoggingOperations logger = new LoggingOperations();
        Thread.setDefaultUncaughtExceptionHandler(logger);


        mBeaconManager = org.altbeacon.beacon.BeaconManager.getInstanceForApplication(this);
        try {
            Log.e("BG", "iam in mBeaconManager to unbind it");
            mBeaconManager.unbind(this);
            mBeaconManager.removeAllRangeNotifiers();
            mBeaconManager.removeAllMonitorNotifiers();
            mBeaconManager.removeMonitoreNotifier(this);
        } catch (OutOfMemoryError e) {
            Log.e("BG", "iam in mBeaconManager to unbind it excep ");

        }
        mBeaconManager.getBeaconParsers().clear();
        //set Beacon Layout for Eddystone-UID packet


        mBeaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.EDDYSTONE_UID_LAYOUT));
        //   rideBDD = new RideBDD(getApplicationContext());
/*        mBeaconManager.setForegroundScanPeriod(300000L);

        mBeaconManager.setForegroundBetweenScanPeriod(300000L);

        mBeaconManager.setBackgroundScanPeriod(300000L);

        mBeaconManager.setBackgroundBetweenScanPeriod(150000L);
        mBeaconManager.setBackgroundMode(true);

        mBeaconManager.setDebug(true);

        BeaconManager.setAndroidLScanningDisabled(true);*/

        notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        namespace = DataHandler.getStringPreferences(AppConstants.UUID_NAME_SPACE);
        instanceID = DataHandler.getStringPreferences(AppConstants.UUID_INSTANCE_ID);
        Log.e("BG", "namespace :" + namespace);
        Log.e("BG", "instanceID :" + instanceID);
        Long currentTime = System.currentTimeMillis();
        String time = formateLongToOnlyDateForServer(currentTime);
        LoggingOperations.writeToFile(BackgroundBeaconScan.this,"UUID > " + time + " -- > " + namespace + " - " + instanceID);

        //To add Eddystone-UID region it's necessary to pass as parameters --> (uniqueId = region name, id1=namespace, id2 = instance, id3 = null)
        //  regions = new Region[1];
        //  regions[0] = new Region("EdstUIDAdvertising", Identifier.parse(namespace), Identifier.parse(instanceID), null);

        try {
            Log.e("BG", "iam in mBeaconManager.isBound");
            mBeaconManager.bind(this);
            Log.e("BG", "iam in mBeaconManager.bind");

        } catch (OutOfMemoryError e) {
            Log.e("BG", "iam in mBeaconManager.bindexce", e);

        }


    }


    @Override
    public void onStart(Intent intent, int startId) {
        Log.e("BG", "iam pnstart");
   /*      if (mApiClient ==null){
            mApiClient = new GoogleApiClient.Builder(this)
                     .addApi(ActivityRecognition.API)
                     .addConnectionCallbacks(this)
                     .addOnConnectionFailedListener(this)
                     .build();
         }*/
//        currentspeed = MenuActivity.speed;
//        Log.e("Static","CurrentSpeed :"+currentspeed);

//        if(beaconFound){
//        startRide();
//        }
    }


    @Override
    public void onDestroy() {

        super.onDestroy();
        Long currentTime = System.currentTimeMillis();
        String time = formateLongToOnlyDateForServer(currentTime);
        LoggingOperations.writeToFile(BackgroundBeaconScan.this,"BEACON SERVICE > " + time + " DESTROYED");
        Log.e("BG", "iam in destroy");
        AppUtils.isBGServiceActive = false;

        try {
            mBeaconManager.unbind(this);
            mBeaconManager.removeMonitoreNotifier(this);
            mService.removeLocationUpdates();
            unbindService(mServiceConnection);

            Log.e("BG", "iam in   mBeaconManager.unbind");
            if (beaconFd != null) {
                beaconFd.cancel();
                beaconFd = null;

            }
        } catch (Exception e) {
            Log.e("BG", "iam in   mBeaconManager.unbind Exception" + e);
        }


    }

    public void enableRegions() {
        try {

            if (regionBootstrap == null) {
                List<Region> list = new ArrayList<>();
                for (int i = 0; i < regions.length; i++) {
                    if (regions[i] != null) {
                        list.add(regions[i]);
                    }
                }
                regionBootstrap = new RegionBootstrap(this, list);
            }

            for (int i = 0; i < regions.length; i++) {
                if (regions[i] != null) {
                    mBeaconManager.startRangingBeaconsInRegion(regions[i]);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.e("BG", "iam in IBinder");
        bindService(new Intent(BackgroundBeaconScan.this, ForegroundLocationService.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
        return null;
    }

    @Override
    public void onBeaconServiceConnect() {
        // String preiBeaconUUID = DataHandler.getStringPreferences(AppConstants.UUID);
        // postiBeaconUUID=AppUtils.addDashes(preiBeaconUUID);
        Region region;
        try {
            Log.e("BG", "iam in onBeaconServiceConnect");
            namespace = DataHandler.getStringPreferences(AppConstants.UUID_NAME_SPACE);

            instanceID = DataHandler.getStringPreferences(AppConstants.UUID_INSTANCE_ID);

            Identifier myBeaconNamespaceId = Identifier.parse(namespace);

            Identifier myBeaconInstanceId = Identifier.parse(instanceID);

            region = new Region("EdstUIDAdvertising", myBeaconNamespaceId, myBeaconInstanceId, null);

            mBeaconManager.addMonitorNotifier(this);
            Log.e("BG", "iam in startMonitoringBeaconsInRegion");

        } catch (Exception e) {
            Log.e("BG", "iam in startMonitoringBeaconsInRegionExce" + e);
            e.printStackTrace();
            namespace = DataHandler.getStringPreferences(AppConstants.UUID_NAME_SPACE);

            instanceID = DataHandler.getStringPreferences(AppConstants.UUID_INSTANCE_ID);

            Identifier myBeaconNamespaceId = Identifier.parse(namespace);

            /* Identifier myBeaconInstanceId = Identifier.parse(instanceID);*/
            region = new Region("EdstUIDAdvertising", myBeaconNamespaceId, null, null);
            mBeaconManager.addMonitorNotifier(this);
            Log.e("BG", "iam in startMonitoringBeaconsInRegion");

        }
        AppUtils.isBGServiceActive = true;
        try {
            mBeaconManager.startMonitoringBeaconsInRegion(region);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        mBeaconManager.addMonitorNotifier(new MonitorNotifier() {
            @Override
            public void didEnterRegion(Region region) {
                Log.e("Monitored", "entered");
                enterCount++;
                Long currentTime = System.currentTimeMillis();
                String time = formateLongToOnlyDateForServer(currentTime);
                LoggingOperations.writeToFile(BackgroundBeaconScan.this,"ENTERED > " + time + "  count Times >  " + enterCount + " beacon > ");
                /*try {
                    if(LocationService.future2minTimer != null){
                        beaconFd.cancel();
                        beaconFd = null;

                    }
                }catch (NullPointerException e){

                }catch (Exception e){

                }*/
                beaconlostCounter++;
                if (beaconlostCounter == 2) {
                    AppConstants.ORPHAN_RIDE_BIT = 0;
                }
                try {
                    if (beaconFd != null) {
                        beaconFd.cancel();
                        beaconFd = null;

                    }
                } catch (Exception e) {

                }

                userAlreadyLoggedIn = DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY);
                if (userAlreadyLoggedIn && DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE) == false) {
                    AppUtils.isInRange = true;
                    AppUtils.IamInExit = false;
                    beaconlostPermanently = false;
                    // startService(new Intent(BackgroundBeaconScan.this, LocationService.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));



                     /*long[] v = {0, 200};
                     Bitmap largeIcon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.freeedrive_newlogo);
                     NotificationCompat.Builder mBuilder =
                             new NotificationCompat.Builder(getApplicationContext())
                                     .setSmallIcon(R.drawable.freeedrive_newlogo)
                                     .setLargeIcon(largeIcon)
                                     .setContentText(AppConstants.BODY_LOCATION_SERVICE_ON).setContentTitle(AppConstants.TITLE_LOCATION_SERVICE)
                                     .setPriority(Notification.PRIORITY_HIGH).setVibrate(v);
                     notificationManager.notify(1,mBuilder.build());*/

                    Log.e("Beacon", "Found");

                    if (beaconenterregionCounter==true && AppUtils.IsBluetoothEnabled==true&&AppUtils.isInRange == true ) {
                        showStandardHeadsUpNotification(getApplicationContext());
                        mServiceConnection = new ServiceConnection() {

                            @Override
                            public void onServiceConnected(ComponentName name, IBinder service) {
                                ForegroundLocationService.LocalBinder binder = (ForegroundLocationService.LocalBinder) service;
                                mService = binder.getService();
                                mService.requestLocationUpdates();

                            }

                            @Override
                            public void onServiceDisconnected(ComponentName name) {
                                mService = null;
                            }
                        };
                        bindService(new Intent(BackgroundBeaconScan.this, ForegroundLocationService.class), mServiceConnection,
                                Context.BIND_AUTO_CREATE);
                        beaconenterregionCounter = false;
                    }

                }

            }

            @Override
            public void didExitRegion(Region region) {
                Log.e("Monitored", "exited");
                AppUtils.IamInExit = true;
                disconnect = true;
                beaconlostCounter++;
                exitCount++;
                Long currentTime = System.currentTimeMillis();
                String time = formateLongToOnlyDateForServer(currentTime);
                LoggingOperations.writeToFile(BackgroundBeaconScan.this,"EXIT CALLED > " + time + " - count Times >  " + exitCount + " - Ride was active >" + DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE));
                if (beaconlostCounter <= 1 && DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE) && currentSpeed > 15) {
                    AppConstants.ORPHAN_RIDE_BIT = 1;
                }
                if (disconnect) {
                    try {
                        //mBeaconManager.stopMonitoringBeaconsInRegion(region);
                        Log.e("disconnectTimer", "hited");
                        disconnectTimer();


                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("ExitRegiondidExitRegion", "" + e);
                    }
                }
            }

            @Override
            public void didDetermineStateForRegion(int i, Region region) {

            }
        });


    }

    @Override
    public void didEnterRegion(Region region) {
        //Log.e("EnteredRegion", "entered");

    }

    @Override
    public void didExitRegion(Region region) {

    }

    public void disconnectTimer() {
        beaconFd = new Timer();
        TimerTask timerTask = new TimerTask() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void run() {
                //   stopFDSensor();
                //  getToasthandlerpauseRide.sendEmptyMessage(0);
                userAlreadyLoggedIn = DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY);
                if (userAlreadyLoggedIn) {
                    if (!DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)) {
/*
                        Intent stopsrvc = new Intent(getApplicationContext(), LocationService.class);

                        stopService(stopsrvc);*/

                        mService.removeLocationUpdates();
                        mService.stopForeground(true);
                        mService.stopSelf();
                        beaconenterregionCounter = true;
                             /*long[] v = {0, 200};
                             Bitmap largeIcon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.freeedrive_newlogo);
                             NotificationCompat.Builder mBuilder =
                                     new NotificationCompat.Builder(getApplicationContext())
                                             .setSmallIcon(R.drawable.freeedrive_newlogo)
                                             .setLargeIcon(largeIcon)
                                             .setContentText(AppConstants.BODY_LOCATION_SERVICE_OFF).setContentTitle(AppConstants.TITLE_LOCATION_SERVICE)
                                             .setPriority(Notification.PRIORITY_HIGH).setVibrate(v);
                             notificationManager.notify(1,mBuilder.build());*/
                    } else if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)==true) {
                        /*if(applicationTerminate){

                        }*/
                       /* Intent stopsrvc = new Intent(getApplicationContext(), LocationService.class);
                        stopService(stopsrvc);*/
                        Log.e("Inside", "DisconnectTimer");

                        Intent sendInRangeIntent = new Intent("IamOutOfRange");

                        sendBroadcast(sendInRangeIntent);

                        beaconenterregionCounter = true;

                        AppUtils.isLocationSrvcStopped = true;
                        AppUtils.isInRange = false;
                        AppUtils.speedCheck = false;
                             /*long[] v = {0, 200};
                             Bitmap largeIcon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.freeedrive_newlogo);
                             NotificationCompat.Builder mBuilder =
                                     new NotificationCompat.Builder(getApplicationContext())
                                             .setSmallIcon(R.drawable.freeedrive_newlogo)
                                             .setLargeIcon(largeIcon)
                                             .setContentText(AppConstants.BODY_LOCATION_SERVICE_OFF).setContentTitle(AppConstants.TITLE_LOCATION_SERVICE)
                                             .setPriority(Notification.PRIORITY_HIGH).setVibrate(v);
                             notificationManager.notify(1,mBuilder.build());*/
                             try{
                                 exitedPermanentlyCount++;
                                 Long currentTime = System.currentTimeMillis();
                                 String time = formateLongToOnlyDateForServer(currentTime);
                                 LoggingOperations.writeToFile(BackgroundBeaconScan.this,"EXITED PERMANENTLY > " + time + " - count Times >  " + exitedPermanentlyCount + " - Ride was active >" + DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE));
                                 mService.removeLocationUpdates();
                                 mService.stopForeground(true);
                                 unbindService(mServiceConnection);
                                 mService.stopSelf();
                             }catch (Exception e){
                                 e.printStackTrace();
                             }


                    } else {
                        beaconlostPermanently = true;
                        if (beaconFd != null) {
                            beaconFd.cancel();
                            beaconFd = null;
                        }
                    }

                    Log.e("Beacon", "Not Found");

                    //lease plam commit

                  /*  long[] v = {0, 200};
                    Bitmap largeIcon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.freeedrive_newlogo);
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(getApplicationContext())
                                    .setSmallIcon(R.drawable.freeedrive_newlogo)
                                    .setLargeIcon(largeIcon)
                                    .setContentText(AppConstants.BODY_NOTIFICATION_OUTRANGE).setContentTitle(AppConstants.TITLE_NOTIFICATION_INRANGE)
                                    .setPriority(Notification.PRIORITY_HIGH).setVibrate(v);
                    notificationManager.notify(1,mBuilder.build());*/
                }


            }
        };
        //

        beaconFd.schedule(timerTask,370000);
        //  beaconFd.schedule(timerTask, 15000);
        //just for the code to commit
    }

    private void showStandardHeadsUpNotification(Context context) {
        try {
            Long currentTimeRec =  System.currentTimeMillis();
            String timeRec=  formateLongToOnlyDateForServer(currentTimeRec);
            LoggingOperations.writeToFile(context,"I SHOWED BEACON FOUND NOTIFICATION AT >" +timeRec);

            notificationLanguage = DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG);
            if (notificationLanguage != null && !notificationLanguage.isEmpty()) {
                if (notificationLanguage.equalsIgnoreCase("NL")) {
                    contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_nl);
                    contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_nl);
                    //  notificationHeading = getmAppcontext().getResources().getString(R.string.text_notification_less_23_nl);
                } else if (notificationLanguage.equalsIgnoreCase("ES")) {
                    contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_es);
                    contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_es);
                    //  notificationHeading = getmAppcontext().getResources().getString(R.string.text_notification_less_23_nl);

                } else if (notificationLanguage.equalsIgnoreCase("FR")) {
                    contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_fr);
                    contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_fr);
                    //  notificationHeading = getmAppcontext().getResources().getString(R.string.text_notification_less_23_fr);

                } else if (notificationLanguage.equalsIgnoreCase("IT")) {
                    contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_it);
                    contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_it);
                } else if (notificationLanguage.equalsIgnoreCase("DA")) {
                    contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_da);
                    contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_da);
                } else if (notificationLanguage.equalsIgnoreCase("NO")) {
                    contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_no);
                    contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_no);
                } else if (notificationLanguage.equalsIgnoreCase("DE")) {
                    contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_de);
                    contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_de);
                } else if (notificationLanguage.equalsIgnoreCase("PT")) {
                    contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_pt);
                    contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_pt);
                } else if (notificationLanguage.equalsIgnoreCase("FI")) {
                    contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_fi);
                    contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_fi);
                } else if (notificationLanguage.equalsIgnoreCase("EN")) {
                    contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_en);
                    contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_en);
                    // notificationHeading = getmAppcontext().getResources().getString(R.string.text_notification_less_23);

                }
            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("en")) {
                contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_en);
                contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_en);
            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("nl")) {
                contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_nl);
                contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_nl);
                //  notificationHeading = getmAppcontext().getResources().getString(R.string.text_notification_less_23_nl);

            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("es")) {
                contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_es);
                contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_es);
                //  notificationHeading = getmAppcontext().getResources().getString(R.string.text_notification_less_23_nl);

            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("fr")) {
                contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_fr);
                contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_fr);
                // notificationHeading = getmAppcontext().getResources().getString(R.string.text_notification_less_23_fr);

            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("it")) {
                contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_it);
                contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_it);
            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("da")) {
                contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_da);
                contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_da);
            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("no")) {
                contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_no);
                contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_no);
            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("de")) {
                contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_de);
                contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_de);
            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("pt")) {
                contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_pt);
                contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_pt);
            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("fi")) {
                contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_fi);
                contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_fi);
            } else {
                contextTitle = getApplicationContext().getResources().getString(R.string.beacon_notify_body_en);
                contentText = getApplicationContext().getResources().getString(R.string.beacon_notify_title_en);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
       /* String jsonObjProfile =DataHandler.getStringPreferences(AppConstants.TEMP_PROFILE_KEY);

        String firstName = "";
        if(jsonObjProfile !=null && !jsonObjProfile.isEmpty()){

            try{
                JSONObject jsonObject = new JSONObject(jsonObjProfile);
                firstName = jsonObject.getString("first_name");
                Log.e("firstName",firstName);
            }catch (Exception exception){
                exception.printStackTrace();
            }

        }*/




        long[] v = {0, 200};
        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.freeedrive_newlogo);
        /*// Sets an ID for the notification, so it can be updated.
        int notifyID = 1;
        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        CharSequence name = "Channel one";// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);*/
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setLargeIcon(largeIcon)
                        .setSmallIcon(R.drawable.freee_drive_statusbar_logo)
                        .setContentTitle(contextTitle)
                        .setContentText(contentText)
                        .setAutoCancel(true)
                        .setPriority(Notification.PRIORITY_HIGH).setVibrate(v);
        notificationManager.notify(3, mBuilder.build());

    }


    @Override
    public void didDetermineStateForRegion(int i, Region region) {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

/*
            Intent notificationIntent = new Intent(this, Detector.class);
            notificationIntent.setAction("Main");
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    notificationIntent, 0);
            Intent nextIntent = new Intent(this, BackgroundBeaconScan.class);
            nextIntent.setAction("Disconnect");
            PendingIntent pnextIntent = PendingIntent.getService(this, 0,
                    nextIntent, 0);

            Bitmap icon = BitmapFactory.decodeResource(getResources(),
                    R.drawable.freeedrive_newlogo);
            Notification notification = new NotificationCompat.Builder(this)
                    .setContentTitle("FD Drive Pad")
                    .setContentText("We are connecting! with your drive pad")
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setLargeIcon(
                            Bitmap.createScaledBitmap(icon, 128, 128, false))
                    .setContentIntent(pendingIntent)
                    .setOngoing(true)
                    .setPriority((Notification.PRIORITY_MAX))
                    .addAction(android.R.drawable.ic_media_next, "Disconnect",
                            pnextIntent).build();
            startForeground(1,
                    notification);
*/


        try{


            //For foreground

            if (intent.getAction()!=null&& intent.getAction().equals("Connect")) {


                Log.e("BG", "iam in start command");
                mBeaconManager = org.altbeacon.beacon.BeaconManager.getInstanceForApplication(this);
                try {
                    Log.e("BG", "iam in mBeaconManager to unbind it");
                    mBeaconManager.unbind(this);
                    mBeaconManager.removeAllRangeNotifiers();
                    mBeaconManager.removeAllMonitorNotifiers();
                    mBeaconManager.removeMonitoreNotifier(this);
                } catch (OutOfMemoryError e) {
                    Log.e("BG", "iam in mBeaconManager to unbind it excep ");

                }
                mBeaconManager.getBeaconParsers().clear();
                //set Beacon Layout for Eddystone-UID packet


                mBeaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.EDDYSTONE_UID_LAYOUT));
                //   rideBDD = new RideBDD(getApplicationContext());
    /*        mBeaconManager.setForegroundScanPeriod(300000L);

            mBeaconManager.setForegroundBetweenScanPeriod(300000L);

            mBeaconManager.setBackgroundScanPeriod(300000L);

            mBeaconManager.setBackgroundBetweenScanPeriod(150000L);
            //mBeaconManager.setBackgroundMode(true);
            mBeaconManager.setDebug(true);
            BeaconManager.setAndroidLScanningDisabled(true);*/

                notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

                namespace = DataHandler.getStringPreferences(AppConstants.UUID_NAME_SPACE);
                instanceID = DataHandler.getStringPreferences(AppConstants.UUID_INSTANCE_ID);
                Log.e("BG", "namespace :" + namespace);
                Log.e("BG", "instanceID :" + instanceID);


                //To add Eddystone-UID region it's necessary to pass as parameters --> (uniqueId = region name, id1=namespace, id2 = instance, id3 = null)
                //  regions = new Region[1];
                //  regions[0] = new Region("EdstUIDAdvertising", Identifier.parse(namespace), Identifier.parse(instanceID), null);

                try {
                    Log.e("BG", "iam in mBeaconManager.isBound");
                    mBeaconManager.bind(this);
                    Log.e("BG", "iam in mBeaconManager.bind");

                } catch (OutOfMemoryError e) {
                    Log.e("BG", "iam in mBeaconManager.bindexce", e);

                }


            }
            else if (intent.getAction()!=null&&intent.getAction().equals("Disconnect")) {

                stopForeground(true);
                stopSelf();
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e("IamDying", "END");
        DataHandler.updatePreferences(RIDE_WAS_ACTIVE, false);
        beaconenterregionCounter=true;
        AppConstants.broadcastFlag = true;
        Long currentTime = System.currentTimeMillis();
        String time = formateLongToOnlyDateForServer(currentTime);
        LoggingOperations.writeToFile(BackgroundBeaconScan.this,"BEACON SERVICE > " + time + " DESTROYED onTaskRemoved");

    }
}
