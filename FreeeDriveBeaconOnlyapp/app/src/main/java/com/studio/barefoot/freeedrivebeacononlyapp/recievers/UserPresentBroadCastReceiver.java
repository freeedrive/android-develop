package com.studio.barefoot.freeedrivebeacononlyapp.recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;

/**
 * Created by ttwyf on 1/22/2017.
 * This receiver allows to manage the event unlock.
 */

public class UserPresentBroadCastReceiver extends BroadcastReceiver {
     /*Sent when the user is present after
    *device wakes up (e.g when the keyguard is gone)
     */
    /**
     * BoradcastReceiver method basic, used to handle the screen's state
     * @param context(in), @Activity represenst the activity
     * @param intent(in), @Intent the represents the inten
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(Intent.ACTION_USER_PRESENT)){
            AppUtils.isUnlock = true;
            Log.e("BFpk","user present is On");



        }
    }
}
