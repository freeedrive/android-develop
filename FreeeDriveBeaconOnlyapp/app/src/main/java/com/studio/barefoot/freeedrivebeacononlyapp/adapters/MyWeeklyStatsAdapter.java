package com.studio.barefoot.freeedrivebeacononlyapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.RidesBeans;
import com.txusballesteros.widgets.FitChart;
import com.txusballesteros.widgets.FitChartValue;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by macbookpro on 4/26/17.
 */

public class MyWeeklyStatsAdapter extends RecyclerView.Adapter<MyWeeklyStatsAdapter.MyViewHolder> {


    private ArrayList<RidesBeans> ridesBeansList;
    Context context;
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.driving_performance, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RidesBeans ridesBeans = ridesBeansList.get(position);
        int totalelapsedTime =(int) ridesBeans.getTime_elapsed();
        int totalScore =(int) ridesBeans.getScore();
        int distractionsOfEachRide = ridesBeans.getBad_behaviour();
        String weeklyDate = ridesBeans.getDepartureDate();
        /*holder.date.setText(dateofRide);
        holder.time.setText(startAndEndTimeOfRide);
        holder.weekly_no_of_peeks.setText("");*/
        String[]newdate1 =weeklyDate.split("-");
        String newDate = newdate1[2].toString()+"/"+newdate1[1].toString();
        holder.date.setText(""+newDate);
        holder.weeklyridesScore.setText(""+totalScore);
        holder.time.setText(""+totalelapsedTime+"mn");


        Collection<FitChartValue> fitChartValues = new ArrayList<>();
        if(distractionsOfEachRide <5){
            holder.weekly_no_of_peeks.setText(""+distractionsOfEachRide);
            holder.weekly_no_of_peeks.setTextColor(context.getResources().getColor(R.color.colorPieChartGreen));
        }
        else if(distractionsOfEachRide >=5 && distractionsOfEachRide <10){
            holder.weekly_no_of_peeks.setText(""+distractionsOfEachRide);
            holder.weekly_no_of_peeks.setTextColor(Color.parseColor("#FF9800"));
        }
        else{
            holder.weekly_no_of_peeks.setText(""+distractionsOfEachRide);
            holder.weekly_no_of_peeks.setTextColor(context.getResources().getColor(R.color.colorPieChartRed));

        }
        if(totalScore >=80){
            holder.weeklyridesScore.setText(""+totalScore);
            holder.weeklyridesScore.setTextColor(context.getResources().getColor(R.color.colorPieChartGreen));
            holder.txtviewweeklypercentage.setTextColor(context.getResources().getColor(R.color.colorPieChartGreen));
            fitChartValues.add(new FitChartValue(totalScore,context.getResources().getColor(R.color.colorPieChartGreen)));
        }else if(totalScore >= 50 && totalScore < 80){


            holder.weeklyridesScore.setText(""+totalScore);
            holder.weeklyridesScore.setTextColor(context.getResources().getColor(R.color.colorPieChartOrange));
            holder.txtviewweeklypercentage.setTextColor(context.getResources().getColor(R.color.colorPieChartOrange));
            fitChartValues.add(new FitChartValue(totalScore,context.getResources().getColor(R.color.colorPieChartOrange)));
        }else{

            holder.weeklyridesScore.setText(""+totalScore);
            holder.weeklyridesScore.setTextColor(context.getResources().getColor(R.color.colorPieChartRed));
            holder.txtviewweeklypercentage.setTextColor(context.getResources().getColor(R.color.colorPieChartRed));
            fitChartValues.add(new FitChartValue(totalScore ,context.getResources().getColor(R.color.colorPieChartRed)));
        }
        holder.weeklyfitchartScore.setValues(fitChartValues);

      /* if (distractionsOfEachRide == -1){
           distractionsOfEachRide = 0;
       }
        holder.weekly_no_of_peeks.setText(""+distractionsOfEachRide);*/
    }
    public MyWeeklyStatsAdapter(ArrayList<RidesBeans> moviesList) {
        this.ridesBeansList = moviesList;
    }
    @Override
    public int getItemCount() {
        return ridesBeansList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView date,time,weekly_no_of_peeks,weeklyridesScore,txtviewweeklypercentage;
        private FitChart weeklyfitchartScore;

        public MyViewHolder(View view) {
            super(view);
            date = (TextView) view.findViewById(R.id.weekly_stats_date);
            time = (TextView) view.findViewById(R.id.weekly_stats_total_elapsedTime);
            weekly_no_of_peeks = (TextView) view.findViewById(R.id.weekly_stats_total_no_peeks_value);
            weeklyridesScore = (TextView) view.findViewById(R.id.weekly_stats_score_total_ride_per_day);
            txtviewweeklypercentage = (TextView) view.findViewById(R.id.weekly_stats_score_total_ride_percentage);
            weeklyfitchartScore = (FitChart) view.findViewById(R.id.weekly_stats_fitcharttotalride_score_per_day);
        }
    }
}
