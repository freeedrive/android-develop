package com.studio.barefoot.freeedrivebeacononlyapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.ChangePhoneNumberAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NewPhoneNumberTextActivity extends AppCompatActivity {

    private boolean isThisAnewDevice;
    private boolean isThisAnewPhone;
    EditText et_old_phoneNumber,new_phone_no;
    Button next;
    public static ProgressBarDialog progressBarDialogCHngPhn;
    public static ProgressBarDialog progressBarDialogReCHngPhn;

    public NewPhoneNumberTextActivity() {
        LocaleUtils.updateConfig(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_phone_number_text);
        isThisAnewDevice = getIntent().getBooleanExtra(AppConstants.IsThisNewDevice,false);
        isThisAnewPhone = getIntent().getBooleanExtra(AppConstants.IS_THIS_NEW_PHONE_NO,false);
        et_old_phoneNumber = (EditText) findViewById(R.id.et_old_phoneNumber);
        new_phone_no = (EditText) findViewById(R.id.new_phone_no);
        next = (Button) findViewById(R.id.img_Next_Register);
        /*if (isThisAnewDevice && isThisAnewPhone){


        }*/
        if (getIntent().getStringExtra("comingfromProfile") != null && getIntent().getStringExtra("comingfromProfile").equalsIgnoreCase("updateNo")) {
            et_old_phoneNumber.setEnabled(false);
            if (DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER) != null && !DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER).isEmpty()){
                et_old_phoneNumber.setText(DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER));
            }
        }


    next.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!checkFields()){
                changePhoneNumber();
              //startActivity(new Intent(NewPhoneNumberTextActivity.this,SmsConfirmationActivity.class).putExtra(AppConstants.PHONE_NUMBER_TEMP,new_phone_no.getText().toString().trim()));
            }
        }
    });

        setupActionBar();
    }


    private boolean checkFields() {

        et_old_phoneNumber.setError(null);
        new_phone_no.setError(null);
        boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(et_old_phoneNumber.getText().toString() )) {
            et_old_phoneNumber.setError(this.getString(R.string.error_field_required));
            focusView = et_old_phoneNumber;
            cancel = true;

        } else if (TextUtils.isEmpty(new_phone_no.getText().toString() )) {
            new_phone_no.setError(this.getString(R.string.error_field_required));
            focusView = new_phone_no;
            cancel = true;

        }

        else if (TextUtils.isEmpty(et_old_phoneNumber.getText().toString() )) {
            et_old_phoneNumber.setError(this.getString(R.string.error_field_required));
            focusView = et_old_phoneNumber;
            cancel = true;
        }
        else if (TextUtils.isEmpty(new_phone_no.getText().toString() )) {
            new_phone_no.setError(this.getString(R.string.error_field_required));
            focusView = new_phone_no;
            cancel = true;
        }else if(!AppUtils.isValidPhonemaxlength(et_old_phoneNumber.getText().toString())){
            et_old_phoneNumber.setError(this.getString(R.string.error_invalid_mobilenumber_length));
            focusView = et_old_phoneNumber;
            cancel = true;
        }
        else if(!AppUtils.isValidPhonemaxlength(new_phone_no.getText().toString())){
            new_phone_no.setError(this.getString(R.string.error_invalid_mobilenumber_length));
            focusView = new_phone_no;
            cancel = true;
        }
        else if(!AppUtils.isValidPhoneForUpdatePhoneNUmber(et_old_phoneNumber.getText().toString())){
            et_old_phoneNumber.setError(this.getString(R.string.error_invalid_mobilenumber_during_update));
            focusView = et_old_phoneNumber;
            cancel = true;
        } else if(!AppUtils.isValidPhoneForUpdatePhoneNUmber(new_phone_no.getText().toString())){
            new_phone_no.setError(this.getString(R.string.error_invalid_mobilenumber_during_update));
            focusView = new_phone_no;
            cancel = true;
        }
        if (cancel) {

            focusView.requestFocus();

        }
        return cancel;

    }


    private void changePhoneNumber() {
        progressBarDialogCHngPhn = new ProgressBarDialog(NewPhoneNumberTextActivity.this);
        progressBarDialogCHngPhn.setTitle("Verifying");
        progressBarDialogCHngPhn.setMessage("Please wait..");
        progressBarDialogCHngPhn.showProgressBar();
        progressBarDialogCHngPhn.show();
        List<NameValuePair> mParams = new ArrayList<NameValuePair>();
        final TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = telephonyManager.getDeviceId();
        mParams.add(new BasicNameValuePair("device_id",deviceId));
        mParams.add(new BasicNameValuePair("devID",deviceId));
        mParams.add(new BasicNameValuePair("phone_number_old",et_old_phoneNumber.getText().toString()));
        mParams.add(new BasicNameValuePair("phone_number",new_phone_no.getText().toString()));
        mParams.add(new BasicNameValuePair("lang", DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG)));
        Log.e("Params",""+mParams);
        DataHandler.updatePreferences(AppConstants.PHONE_TEMP,new_phone_no.getText().toString());
        ChangePhoneNumberAsyncTask changePhoneNumberAsyncTask = new ChangePhoneNumberAsyncTask(this, WebServiceConstants.END_POINT_UPDATE_PHONE_NUMBER,mParams,false);
        changePhoneNumberAsyncTask.execute();
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
}
