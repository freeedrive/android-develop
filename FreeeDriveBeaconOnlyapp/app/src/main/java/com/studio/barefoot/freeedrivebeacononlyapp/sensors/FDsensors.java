package com.studio.barefoot.freeedrivebeacononlyapp.sensors;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Chronometer;
import android.widget.Toast;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AudioPlayer;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LoggingOperations;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import android.support.v4.app.NotificationCompat;

import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.currentSpeed;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.formateLongToOnlyDateForServer;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.ispassengerMode;

/**
 * Created by mcs on 1/9/2017.
 */

public class FDsensors extends Service implements SensorEventListener {

    /**
     * Represents the context of application
     */
    private Context context;
    /**
     * This is the sensor manager
     */
    private SensorManager sensorManager;

    boolean gyrcoscope = false;
    boolean accelerometersesnor = false;
    /**
     * Sensor accelerometer
     */
    private Sensor accelerometer;
    /**
     * Sensor mgyroscope
     */
    private Sensor gyroscope;
    /**
     * Chronometer to detect the secondes of movement
     */

    public Chronometer sensorStaticChronometer;

    public NotificationManager notificationManager;
    String notificationLanguage = "";
    Uri uri;
    /**
     * Handler is executed each the 1 sec
     */
    int interval = 500;
    /**
     * Time interval between the peeks
     */
    private final long PEEKS_INTERVAL = 2000;
    /**
     * Allows to execute a method each seconde, the method to detect the sensor'values
     */
    /**
     * Allowing to execute a message
     */
    Handler handler;
    /**
     * Represents the sum of sensors (bad behaviors)
     */
    public static int cptSnsors;
    private String notificationHeading = "";
    private String notificationBody = "";
    public static int ctpSensorsCountBelowSpeed;
    private int volumeLevel;
    private int count = 0;
    BluetoothAdapter bluetoothadapter;
    /**
     * Time text which will be disaply in scoring view
     */
    public String staticTextTotalTime;
    /**
     * Indicates if we can test value axis X
     */
    private Boolean firstTimeAxisX = false;
    /**
     * Indicates if we can test value axis Z
     */
    private Boolean firstTimeAxisZ = false;
    /**
     * Indicates if we can test value axis Y
     */
    private Boolean firstTimeAaxisY = false;

    /**
     * Represents the time in format HH:mm:ss for TestReceiver, save it in Ride which will be saved in Database
     */
    String stringTime;
    /**
     * Allows to calculate the time total of a sensor (hour+minutes+secondes);
     */
    public int staticTotalTime;
    /**
     * Represents the score, this is the value to fill fit chart and this is saved in database
     */
    public static float newAverage;
    private int countY;
    private Drawable drawable;
    private boolean areSensorsRecursive = false;
    private long lastUpdate = 0;
    private int SHAKE_THRESHOLD = 140;
    private int countX = 0;
    private float last_x, last_y, last_z;
    private ArrayList<Long> longArrayListForCollecting15secsChunk = new ArrayList<>();
    private Sensor linear_accelerometer;

    Timer peeksDelayTimer;
    AudioManager audioManager;
    private boolean peeksdelayflag = true;
    private boolean peeksdelaybelowSpeed = true;
    int resourceId;
    MediaPlayer mediaPlayer;
    AudioPlayer audioPlayer;
    Timer shortTimer;
    private boolean bluetooth = false;

    /**
     * Class constructor
     *
     * @param context(in), @Activity represents the activity
     */
    public FDsensors(Context context) {

        setContext(context);
    }

    /**
     * Allows to get the context
     *
     * @return(out), @Activity represents an activity
     */
    public Context getContext() {

        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * Allows to get the total time
     *
     * @return(out), @BInteger represents the total time
     */
    public int getStaticTotalTime() {
        return staticTotalTime;
    }

    /**
     * Allows to get the time in string
     *
     * @return(out), @Boolean represents the time in string
     */
    public String getStringTime() {
        return stringTime;
    }


    /**
     * Allows to get the average
     *
     * @return(out), @Boolean represents the avaerage
     */
    public static float getNewAverage() {

        return newAverage;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    private final Runnable processSensors = new Runnable() {
        @Override
        public void run() {
            mediaPlayer = new MediaPlayer();
            /*audioPlayer = new AudioPlayer();*/
            // The Runnable is posted to run again here:
            bluetoothadapter = BluetoothAdapter.getDefaultAdapter();
            notificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
            areSensorsRecursive = true;
            handler.postDelayed(this, interval);

        }
    };

    @Override
    public void onSensorChanged(SensorEvent event) {
        /*AudioManager audioManager = (AudioManager) getContext().getSystemService(AUDIO_SERVICE);
        try {
            volumeLevel = audioManager.getRingerMode();
        } catch (Exception e) {
            e.printStackTrace();

        }*/

        float y, x, z;
        count++;
        if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)) {
            AppUtils.IsBluetoothEnabled = true;
        }
        if (AppUtils.isInRange && AppUtils.speedCheck && DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE) && AppUtils.gpsenabled && AppUtils.isUnlock && AppUtils.IsBluetoothEnabled && ispassengerMode) {
            count = 0;
            Log.e("event values0", "" + Math.abs(event.values[0]));
            Log.e("event values1", "" + Math.abs(event.values[1]));
            Log.e("event values2", "" + Math.abs(event.values[2]));
            x = event.values[0];
            // pitch
            y = event.values[1];
            // roll
            z = event.values[2];
            if (gyrcoscope) {
                /*Long currentTime = System.currentTimeMillis();
                String time = formateLongToOnlyDateForServer(currentTime);
                LoggingOperations.writeToFile(context, "SENSORS > " + time + " -- > " + "GYRO is RUNNING");*/
                //X: 4.1f y>5f
                if ((!firstTimeAxisX && Math.abs(x) < 0.0f && Math.abs(y) > 0.5f) || (Math.abs(y) < 0.0f && Math.abs(x) > 0.5f)) {
                    //Log.i("Adneom", " (FDSensor) pitch  is " + x + " to " + formattedTimerSensor() + " sec. --- ");

                    long badCountTime = System.currentTimeMillis() / 1000L;

                    /*longArrayListForCollecting15secsChunk.add(badCountTime);*/

                    if (peeksdelayflag){
                        showStandardHeadsUpNotification(context);
                        cptSnsors++;
                        longArrayListForCollecting15secsChunk.add(badCountTime);
                        peeksdelayflag = false;
                       /* peeksdelayTime();*/
                    }

                    firstTimeAxisX = true;

                    Log.e("cptsnsr:firstTimeAxixX", "" + cptSnsors);
                } else if (Math.abs(x - y) > 3f) {

                    long badCountTime = System.currentTimeMillis() / 1000L;

                    if (peeksdelayflag){
                        showStandardHeadsUpNotification(context);
                        cptSnsors++;
                        longArrayListForCollecting15secsChunk.add(badCountTime);
                        peeksdelayflag = false;
                       /* peeksdelayTime();*/
                    }

                }
                //Y:
                if ((!firstTimeAaxisY && Math.abs(x) < 0.0f && Math.abs(y) > 0.0f) || (Math.abs(y) < 0.0f && Math.abs(x) > 0.0f)) {
                    // vibration for 0,5 sec.
                    //((Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE)).vibrate(500);
                    //Log.i("Adneom", " (FDSensor) position couché (azimut) is " + y+" to "+formattedTimerSensor()+" sec. --- ");
                    firstTimeAaxisY = true;
                    countY++;
                    long badCountTime = System.currentTimeMillis() / 1000L;

                    if (peeksdelayflag){
                        showStandardHeadsUpNotification(context);
                        peeksdelayflag = false;
                        cptSnsors++;
                        longArrayListForCollecting15secsChunk.add(badCountTime);
                       /* peeksdelayTime();*/
                    }

                    Log.e("countY", "" + countY);

                    Log.e("ctpsnsr:firstTimeAxixY", "" + cptSnsors);
                    //boolean becomes FALSE when device comes back to the value 0 with axis Y
                } else if (Math.abs(x - y) > 6.5f) {
                    long badCountTime = System.currentTimeMillis() / 1000L;

                    if (peeksdelayflag){
                        showStandardHeadsUpNotification(context);
                        peeksdelayflag = false;
                        cptSnsors++;
                        longArrayListForCollecting15secsChunk.add(badCountTime);
                       /* peeksdelayTime();*/
                    }
                }
            } else {
                long curTime = System.currentTimeMillis();
                if ((curTime - lastUpdate) > 100) {
                    long diffTime = (curTime - lastUpdate);
                    lastUpdate = curTime;

                    float speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000;

                    if (speed > SHAKE_THRESHOLD) {
                        countX++;
                        Log.e("countX", "" + countX);
                        if (countX == 2) {
                            countX = 0;
                            long badCountTime = System.currentTimeMillis() / 1000L;

                            if (peeksdelayflag){
                                showStandardHeadsUpNotification(context);
                                peeksdelayflag = false;
                                cptSnsors++;
                                longArrayListForCollecting15secsChunk.add(badCountTime);
                       /* peeksdelayTime();*/
                            }
                        }
     /*                   if (speed > 0) {
                            countX++;
                            Log.e("countX", "" + countX);
                            if (countX == 2) {
                                countX = 0;
                                showStandardHeadsUpNotification(context);
                                cptSnsors++;

                                long badCountTime =System.currentTimeMillis()/1000L;
                                longArrayListForCollecting15secsChunk.add(badCountTime);
                            }

                        }*/
                        last_x = x;
                        last_y = y;
                        last_z = z;
                    }
                }
            }
           /* Long currentTime = System.currentTimeMillis();
            String time = formateLongToOnlyDateForServer(currentTime);
            LoggingOperations.writeToFile(context, "SENSORS > " + time + " badCount>> " + cptSnsors);*/
        }
        areSensorsRecursive = false;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void showStandardHeadsUpNotification(Context context) {
        try {
            /*MediaPlayer mp;*/
            long duration = 0;
            String audiofilePath = null;
            // all these are invoked so that the notifications should come in all forms
            audioManager = (AudioManager) getContext().getSystemService(AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
            audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, audioManager.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION), 0);
            audioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, audioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM), 0);
            audioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, audioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
            audioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, audioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM), 0);

            audioManager.adjustVolume(1, 0);


            notificationLanguage = DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG);
            if (notificationLanguage != null && !notificationLanguage.isEmpty()) {
                if (notificationLanguage.equalsIgnoreCase("NL")) {
                    notificationHeading = getContext().getResources().getString(R.string.text_notification_title_nl);
                    notificationBody = getContext().getResources().getString(R.string.ndl_notify);
                    //  notificationHeading = getmAppcontext().getResources().getString(R.string.text_notification_less_23_nl);
                    uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/nlnotify");
                    resourceId = context.getResources().getIdentifier("nlnotify", "raw", context.getPackageName());
                } else if (notificationLanguage.equalsIgnoreCase("ES")) {
                    notificationHeading = getContext().getResources().getString(R.string.text_notification__title_es);
                    notificationBody = getContext().getResources().getString(R.string.es_notify);
                    //  notificationHeading = getmAppcontext().getResources().getString(R.string.text_notification_less_23_nl);
                    uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/esnotify");
                    resourceId = context.getResources().getIdentifier("esnotify", "raw", context.getPackageName());

                } else if (notificationLanguage.equalsIgnoreCase("FR")) {
                    notificationHeading = getContext().getResources().getString(R.string.text_notification__title_fr);
                    notificationBody = getContext().getResources().getString(R.string.fr_notify);
                    //  notificationHeading = getmAppcontext().getResources().getString(R.string.text_notification_less_23_fr);
                    uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/frnotify");
                    resourceId = context.getResources().getIdentifier("frnotify", "raw", context.getPackageName());

                } else if (notificationLanguage.equalsIgnoreCase("IT")) {
                    notificationHeading = getContext().getResources().getString(R.string.text_notification__title_it);
                    notificationBody = getContext().getResources().getString(R.string.it_notify);
                    uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/itnotify");
                    resourceId = context.getResources().getIdentifier("itnotify", "raw", context.getPackageName());

                } else if (notificationLanguage.equalsIgnoreCase("DA")) {
                    notificationHeading = getContext().getResources().getString(R.string.text_notification__title_da);
                    notificationBody = getContext().getResources().getString(R.string.da_notify);
                    uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/danotify");
                    resourceId = context.getResources().getIdentifier("danotify", "raw", context.getPackageName());

                } else if (notificationLanguage.equalsIgnoreCase("NO")) {
                    notificationHeading = getContext().getResources().getString(R.string.text_notification__title_no);
                    notificationBody = getContext().getResources().getString(R.string.no_notify);
                    uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/nonotify");
                    resourceId = context.getResources().getIdentifier("nonotify", "raw", context.getPackageName());

                } else if (notificationLanguage.equalsIgnoreCase("DE")) {
                    notificationHeading = getContext().getResources().getString(R.string.text_notification__title_de);
                    notificationBody = getContext().getResources().getString(R.string.de_notify);
                    uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/denotify");
                    resourceId = context.getResources().getIdentifier("denotify", "raw", context.getPackageName());

                } else if (notificationLanguage.equalsIgnoreCase("PT")) {
                    notificationHeading = getContext().getResources().getString(R.string.text_notification__title_pt);
                    notificationBody = getContext().getResources().getString(R.string.pt_notify);
                    uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/ptnotify");
                    resourceId = context.getResources().getIdentifier("ptnotify", "raw", context.getPackageName());

                } else if (notificationLanguage.equalsIgnoreCase("FI")) {
                    notificationHeading = getContext().getResources().getString(R.string.text_notification__title_fi);
                    notificationBody = getContext().getResources().getString(R.string.fi_notify);
                    uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/finotify");
                    resourceId = context.getResources().getIdentifier("finotify", "raw", context.getPackageName());

                } else if (notificationLanguage.equalsIgnoreCase("EN")) {
                    notificationHeading = getContext().getResources().getString(R.string.text_notification__title_eng);
                    notificationBody = getContext().getResources().getString(R.string.en_notify);
                    // notificationHeading = getmAppcontext().getResources().getString(R.string.text_notification_less_23);
                    uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/ennotify");
                    resourceId = context.getResources().getIdentifier("ennotify", "raw", context.getPackageName());

                }
            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("en")) {
                notificationHeading = getContext().getResources().getString(R.string.text_notification__title_eng);
                notificationBody = getContext().getResources().getString(R.string.en_notify);
                uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/ennotify");
                resourceId = context.getResources().getIdentifier("ennotify", "raw", context.getPackageName());

            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("nl")) {
                notificationHeading = getContext().getResources().getString(R.string.text_notification_title_nl);
                notificationBody = getContext().getResources().getString(R.string.ndl_notify);
                //  notificationHeading = getmAppcontext().getResources().getString(R.string.text_notification_less_23_nl);
                uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/nlnotify");
                resourceId = context.getResources().getIdentifier("nlnotify", "raw", context.getPackageName());

            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("es")) {
                notificationHeading = getContext().getResources().getString(R.string.text_notification__title_es);
                notificationBody = getContext().getResources().getString(R.string.es_notify);
                //  notificationHeading = getmAppcontext().getResources().getString(R.string.text_notification_less_23_nl);
                uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/esnotify");
                resourceId = context.getResources().getIdentifier("esnotify", "raw", context.getPackageName());

            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("fr")) {
                notificationHeading = getContext().getResources().getString(R.string.text_notification__title_fr);
                notificationBody = getContext().getResources().getString(R.string.fr_notify);
                // notificationHeading = getmAppcontext().getResources().getString(R.string.text_notification_less_23_fr);
                uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/frnotify");
                resourceId = context.getResources().getIdentifier("frnotify", "raw", context.getPackageName());
            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("it")) {
                notificationHeading = getContext().getResources().getString(R.string.text_notification__title_it);
                notificationBody = getContext().getResources().getString(R.string.it_notify);
                uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/itnotify");
                resourceId = context.getResources().getIdentifier("itnotify", "raw", context.getPackageName());

            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("da")) {
                notificationHeading = getContext().getResources().getString(R.string.text_notification__title_da);
                notificationBody = getContext().getResources().getString(R.string.da_notify);
                uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/danotify");
                resourceId = context.getResources().getIdentifier("danotify", "raw", context.getPackageName());

            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("no")) {
                notificationHeading = getContext().getResources().getString(R.string.text_notification__title_no);
                notificationBody = getContext().getResources().getString(R.string.no_notify);
                uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/nonotify");
                resourceId = context.getResources().getIdentifier("nonotify", "raw", context.getPackageName());
            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("de")) {
                notificationHeading = getContext().getResources().getString(R.string.text_notification__title_de);
                notificationBody = getContext().getResources().getString(R.string.de_notify);
                uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/denotify");
                resourceId = context.getResources().getIdentifier("denotify", "raw", context.getPackageName());

            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("pt")) {
                notificationHeading = getContext().getResources().getString(R.string.text_notification__title_pt);
                notificationBody = getContext().getResources().getString(R.string.pt_notify);
                uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/ptnotify");
                resourceId = context.getResources().getIdentifier("ptnotify", "raw", context.getPackageName());

            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("fi")) {
                notificationHeading = getContext().getResources().getString(R.string.text_notification__title_fi);
                notificationBody = getContext().getResources().getString(R.string.fi_notify);
                uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/finotify");
                resourceId = context.getResources().getIdentifier("finotify", "raw", context.getPackageName());

            } else {
                notificationHeading = getContext().getResources().getString(R.string.text_notification__title_eng);
                notificationBody = getContext().getResources().getString(R.string.en_notify);
                uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/raw/ennotify");
                resourceId = context.getResources().getIdentifier("ennotify", "raw", context.getPackageName());

            }

            long[] v = {0, 200};
            Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.freeedrive_newlogo);
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.freee_drive_statusbar_logo)
                            .setLargeIcon(largeIcon)
                            .setColor(context.getResources().getColor(R.color.colorPrimaryNew))
                            .setContentTitle(notificationHeading)
                            .setContentText(notificationBody)
                            .setPriority(Notification.PRIORITY_HIGH).setVibrate(v);
            notificationManager.notify(1, mBuilder.build());

            /*mediaPlayer = null;*/

            mediaPlayer = MediaPlayer.create(context,resourceId);
            mediaPlayer.start();
           /* duration = audioPlayer.play(context, resourceId);*/

            duration = mediaPlayer.getDuration();
            peeksdelayTime(duration);
            Log.e("AudioLength", ":" + duration);
            Long currentTime = System.currentTimeMillis();
            String time = formateLongToOnlyDateForServer(currentTime);
            LoggingOperations.writeToFile(context, "SENSORS > " + time + " Audio Length>> " + duration);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public NotificationCompat.Builder createNotificationBuider(Context context, String title, String message) {
        drawable = context.getResources().getDrawable(R.drawable.freeedrive_newlogo);
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.freeedrive_newlogo);
        // Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
        Log.e("BitMap", "" + bitmap);
        return new NotificationCompat.Builder(context)
                .setLargeIcon(bitmap)
                .setSmallIcon(R.drawable.freee_drive_statusbar_logo)
                .setContentTitle(title)
                .setContentText(message)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(title))
                .setAutoCancel(true);
    }

    /**
     * Calculate the time of journey
     * Warning, if the journey starts at 5h10 and ends at 7h00 the difference is 1:59
     * if the journey starts at 5h10 and ends at 7h25 the difference is 2:15
     * Test on minutes of end time, its <= 0 then hours will be -1.
     */
    public void journeyTotalTime() {
        int hoursTotal = 0;
        int minutesTotal = 0;
        //reset to zero :
        //totalTime = 0;
        staticTotalTime = 0;

        /*Date endDate = null;
        Date beginDate = null;
        try {
            beginDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse("28/08/2016 17:35:09");
            endDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse("28/08/2016 19:02:01");
        } catch (ParseException e) {
            e.printStackTrace();
        }*/

        //This is for you nofal

        if (sensorStaticChronometer != null) {
            sensorStaticChronometer.stop();
            //}
            Long endDate = System.currentTimeMillis();
            Long beginDate = (sensorStaticChronometer.getBase());
            Long elapsedTime = AppUtils.differenceTimeUtils(beginDate, endDate);
            Log.e("Bfpk", "(elapsedTime" + elapsedTime + "endDate" + endDate + "beginDate" + beginDate);

            DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
            String dateFormattedEndDate = formatter.format(endDate);
            String dateFormattedBeginDate = formatter.format(beginDate);
            //Log.i("Adneom"," (FDSensor) end : "+dateFormattedEndDate+" begin is "+dateFormattedBeginDate);
            long diffTime = endDate - beginDate;
            Date dateDifference = new Date(diffTime);
            //Log.i("Adneom","(Sensor) difference time is "+formatter.format(diffTime));
            String[] tabDiffTime = formatter.format(diffTime).split(":");
            String[] tab_end_date = dateFormattedEndDate.split(":");
            String[] tab_begin_date = dateFormattedBeginDate.split(":");
            int hours = Math.abs(Integer.parseInt(tab_end_date[0]) - Integer.parseInt(tab_begin_date[0]));
            //if midnight :
            if (Integer.parseInt(tab_end_date[0]) == 0 && Integer.parseInt(tab_begin_date[0]) != 0) {
                hours = (24 - hours);
            }
            //int minutes = Math.abs( Integer.parseInt(tab_end_date[1]) - Integer.parseInt(tab_begin_date[1]) );
            //int secondes = Math.abs( Integer.parseInt(tab_end_date[2]) - Integer.parseInt(tab_begin_date[2]) );
            int minutes = Integer.parseInt(tabDiffTime[1]);
            int secondes = Integer.parseInt(tabDiffTime[2]);


            //button time :
            StringBuilder strTime = new StringBuilder();
            if (hours > 0) {
                if (minutes > 0) {
                    //cf : Warning
                    if (Integer.parseInt(tab_begin_date[1]) > Integer.parseInt(tab_end_date[1])) {
                        hours--;
                        minutes = ((60 - Integer.parseInt(tab_begin_date[1])) + Integer.parseInt(tab_end_date[1]));
                    }
                    if (hours <= 0) {
                        strTime.append(String.valueOf(minutes) + " mins");
                    } else {
                        //adding 0 before :
                        if (minutes < 10) {
                            strTime.append(String.valueOf(hours) + ":0" + String.valueOf(minutes) + " mins");
                        } else {
                            strTime.append(String.valueOf(hours) + ":" + String.valueOf(minutes) + " mins");
                        }
                    }
                    // format string after the tests :
                    this.stringTime = hours + ":" + minutes + ":0";
                    staticTotalTime = ((hours * 3600) + (minutes * 60));
                } else {
                    //Log.i("Adneom"," (FDSensor) else");
                    strTime.append(String.valueOf(hours) + " h");
                    // format string after the tests :
                    this.stringTime = hours + ":0:0";
                    staticTotalTime = (hours * 3600);
                }
            } else {
                if (minutes > 0) {
                    strTime.append(String.valueOf(minutes) + " mins");
                    // format string after the tests :
                    this.stringTime = "0:" + minutes + ":0";
                    staticTotalTime = ((minutes * 60) + secondes);
                } else {
                    if (secondes > 0) {
                        strTime.append(String.valueOf(secondes) + " secs");
                        // format string after the tests :
                        this.stringTime = "0:0:" + secondes;
                        staticTotalTime = secondes;
                    }
                }
            }

            //   staticTextTotalTime = strTime.toString();
            staticTextTotalTime = elapsedTime.toString();
            //if cpt is 0 = good behavior so 0% else bad behavior
            Log.e("Adneom", "(Sensor) cpt is " + cptSnsors + " and total time is " + staticTextTotalTime);

            //double average = (cptSnsors == 0) ? 100 : ((1 - ((double) cptSnsors / staticTotalTime)) * 100);
            Long validTimeStamp = 0L;
            if (DataHandler.getLongreferences(AppConstants.REDUCTED_ARRIVAL) != null) ;
            {
                validTimeStamp = DataHandler.getLongreferences(AppConstants.REDUCTED_ARRIVAL) - 15;

            }
            int reductedBadCount = 0;
            for (int i = 0; i < longArrayListForCollecting15secsChunk.size(); i++) {
                if (longArrayListForCollecting15secsChunk.get(i) < validTimeStamp) {
                    reductedBadCount++;
                }

            }
            Log.e("reductedBadCount", "" + reductedBadCount);
            Log.e("cptSnsors", "" + cptSnsors);
            cptSnsors = 0;
            cptSnsors = reductedBadCount;
            Log.e("reductedBadCount2", "" + reductedBadCount);
            double average = 100 - (cptSnsors * 5);
            int total = 100;
            /*int score = 0;
            score=total-cptSnsors;
            average = score*(currentRideTime/staticTotalTime);*/
            //if average less than 0 than its GOOD BEHAVIOUR
            if (average < 0) {
                average = 0;

            }


            Log.e("Average", "" + average);

            //Else cpt !=0 then add average with using Math.round function or rounding the average value
            //Log.i("Adneom", " (FDSensor) The average is " + average+" --> "+Math.round(average));
            newAverage = Math.round(average);
            int averageText = (int) newAverage;
            Long currentTime = System.currentTimeMillis();
            String time = formateLongToOnlyDateForServer(currentTime);
            LoggingOperations.writeToFile(context, "newAverage > " + time + " newAverage>> " + newAverage);

            //Log.i("Adneom", " (FDSensor) The average is " + average+" newAverage is "+newAverage+" and averageText "+averageText);
            sendMessageToMenuActivity(newAverage, averageText, reductedBadCount);
            //RidesBeans ride = new RidesBeans();
            //ride.setBad_behaviour(reductedBadCount);
            ;
            DataHandler.updatePreferences(AppConstants.SCORE_TOTAL_BAD_COUNTS, reductedBadCount);
            DataHandler.updatePreferences(AppConstants.RIDE_SCORE, newAverage);
            try {
                longArrayListForCollecting15secsChunk.clear();
            } catch (Exception e) {

            }
        }
    }

    /**
     * Send an Intent with an action named "com.freeedrive_saving_driving.sensor_event".
     * The time represents the total time of a journey and the text represent the text to display in view chart are send to Menu Activity
     * to display on chart the values. Save information in the preferences
     *
     * @param valueSafetyScore value of chart fit (last ride)
     * @param textAverageScore value which will be display (last ride)
     */
    //text Total Bad Behaviour counts for home screen
    private void sendMessageToMenuActivity(float valueSafetyScore, int textAverageScore, int textcurrentbadbehaviour) {
        Intent intent = new Intent("com.freeedrive_saving_driving.sensor_event");
        JSONObject objLastRide = new JSONObject();
        try {
            objLastRide.put("time", staticTotalTime);
            objLastRide.put("text", staticTextTotalTime);//CurrentTime
            objLastRide.put("badbehaviour", textcurrentbadbehaviour);//count
            objLastRide.put("value_safetyscore", valueSafetyScore);// score
            objLastRide.put("text_safetyscore", textAverageScore);//score same
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (objLastRide != null) {
            DataHandler.updatePreferences(AppUtils.SAFETY_SCORE_LAST_RIDE, objLastRide.toString());
        }
        //add data, app. is foreground :
        intent.putExtra("time", staticTotalTime);
        intent.putExtra("text", staticTextTotalTime);
        intent.putExtra("badbehaviour", textcurrentbadbehaviour);
        intent.putExtra("value_safetyscore", valueSafetyScore);
        intent.putExtra("text_safetyscore", textAverageScore);
        context.sendBroadcast(intent);
    }

    /**
     * Allows to start the sensor Manager by the receiver when Freedrive is connected to a paired device.
     */
    public void startSensorsFD() {
        //screen's state
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        //if screen is on, we can test sensor
        if (pm.isScreenOn()) {
            AppUtils.isUnlock = true;
        }
//        AudioManager audioManager = (AudioManager)getmAppcontext().getSystemService(AUDIO_SERVICE);
//        int volumeLevel  = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//        Log.e("VolumeLevel",""+volumeLevel);
//        if(volumeLevel == audioManager.RINGER_MODE_VIBRATE){
//            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,audioManager.getStreamMaxVolume(AudioManager.STREAM_RING),0);
//            showStandardHeadsUpNotification(getmAppcontext());
//            Log.e("Success","Logic");
//        }
//        else{
//            Log.e("NotWorking","Logic");
//        }

        //Chronometer :
        sensorStaticChronometer = new Chronometer(context);
        //get the current time :
       /* Date dd = new Date ();*/
        /*DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        String dateFormatted = formatter.format(dd);
        Log.i("Adneom", "(FDSensor) start chrono time is " + dateFormatted + " --> " + dd.getTime());*/
        //sensorStaticChronometer.setBase(SystemClock.elapsedRealtime());
        sensorStaticChronometer.setBase(System.currentTimeMillis());
        //reset to zero :
        staticTotalTime = 0;
        //set cpt sensor to zero
        cptSnsors = 0;
        ctpSensorsCountBelowSpeed = 0;

        //management the differents sensor :
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        // magnetic = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        linear_accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        if (sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE_UNCALIBRATED) != null && sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            sensorManager.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_NORMAL);
            gyrcoscope = true;
        } else if (sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION) != null && sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) == null) {
            sensorManager.registerListener(this, linear_accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        } else if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null && sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) == null) {
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
            accelerometersesnor = true;
        } else {
            Toast.makeText(context, "Your Device Does Not Have Sensor", Toast.LENGTH_SHORT).show();
        }
        //  vector = sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
        //sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        // sensorManager.registerListener(this,magnetic,SensorManager.SENSOR_DELAY_NORMAL);

        //  sensorManager.registerListener(this,vector,SensorManager.SENSOR_DELAY_NORMAL);
        // mAccelCurrent = SensorManager.GRAVITY_EARTH;
        // mAccelLast = SensorManager.GRAVITY_EARTH;
        // launch handler to execute each demi-second the detection of sensor's value :
        handler = new Handler();
        handler.post(processSensors);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /**
     * Allows to stop the sensor manager after Freeedrive is disconnected to a paired Bluetooth
     */
    public void stopSensorsFD() {

        if (sensorManager != null) {
            sensorManager.unregisterListener(this);
        }
        if (handler != null) {
            handler.removeCallbacks(processSensors);
        }

        if (sensorStaticChronometer != null) {
            sensorStaticChronometer.stop();
        }

    }

    public void peeksdelayTime(long audiolength) {

        peeksDelayTimer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                try {
                    peeksdelayflag = true;
                  /*  audioPlayer.stop();*/
                    mediaPlayer.stop();
                    mediaPlayer.release();
                /*mediaPlayer.reset();*/
                    peeksDelayTimer.cancel();
                    peeksDelayTimer = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };
        peeksDelayTimer.schedule(timerTask, audiolength);
    }

   /* public void peeksdelaybelowSpeed() {
        shortTimer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                peeksdelaybelowSpeed = true;
                shortTimer.cancel();

            }
        };
        shortTimer.schedule(timerTask, 3000);
    }*/

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}