package com.studio.barefoot.freeedrivebeacononlyapp.dialogs;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.UpdatePrivateHoursAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mcs on 12/28/2016.
 */

public class PrivateHoursDialog extends BaseAlertDialog implements DialogInterface.OnClickListener {
   TextView tv_uuid;
    ContentResolver resolver;


    public PrivateHoursDialog(Context context) {
        super(context);

        LayoutInflater factory = LayoutInflater.from(context);
        final View progressBarView = factory.inflate(R.layout.custom_dialog_uuid, null);

        setView(progressBarView);
        this.context = context;
        tv_uuid = (TextView) progressBarView.findViewById(R.id.tv_uuid);
        setButton(BUTTON_POSITIVE, context.getString(R.string.accept), this);
        setButton(BUTTON_NEGATIVE, context.getString(R.string.decline), this);
        setTitle(context.getString(R.string.link_privacy_dialog));
        setCancelable(true);

        resolver = context.getContentResolver();

            tv_uuid.setText(context.getString(R.string.context_privacy_policy));
    }
    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which){
            case BUTTON_POSITIVE:
                DataHandler.updatePreferences(AppConstants.ACCESS_PRIVATE_HOURS,1);
                dialog.dismiss();
                if (AppUtils.isNetworkAvailable()){
                    List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                    final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                    String deviceId = telephonyManager.getDeviceId();
                    mParams.add(new BasicNameValuePair("devID",deviceId));
                    mParams.add(new BasicNameValuePair("phone_number",DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
                    mParams.add(new BasicNameValuePair("private_hour_enable","1"));
                    mParams.add(new BasicNameValuePair("token",DataHandler.getStringPreferences(AppConstants.TOKEN_NUMBER)));
                    Log.e("PARAMS", "" + mParams);
                    UpdatePrivateHoursAsyncTask updatePrivateHoursAsyncTask = new UpdatePrivateHoursAsyncTask(context,WebServiceConstants.END_POINT_UPDATE_PRIVATE_HOURS,mParams);
                    updatePrivateHoursAsyncTask.execute();
                }else{
                    DataHandler.updatePreferences(AppConstants.ACCESS_PRIVATE_HOURS_UPDATED,false);

                }
                break;
            case BUTTON_NEGATIVE:
                DataHandler.updatePreferences(AppConstants.ACCESS_PRIVATE_HOURS,0);
                if (AppUtils.isNetworkAvailable()){
                    List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                    final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                    String deviceId = telephonyManager.getDeviceId();
                    mParams.add(new BasicNameValuePair("devID",deviceId));
                    mParams.add(new BasicNameValuePair("phone_number",DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
                    mParams.add(new BasicNameValuePair("private_hour_enable","0"));
                    mParams.add(new BasicNameValuePair("token",DataHandler.getStringPreferences(AppConstants.TOKEN_NUMBER)));
                    UpdatePrivateHoursAsyncTask updatePrivateHoursAsyncTask = new UpdatePrivateHoursAsyncTask(context,WebServiceConstants.END_POINT_UPDATE_PRIVATE_HOURS,mParams);
                    updatePrivateHoursAsyncTask.execute();
                }else{
                    DataHandler.updatePreferences(AppConstants.ACCESS_PRIVATE_HOURS_UPDATED,false);

                }
                dismiss();
        }

    }

    @Override
    public void dismiss() {
            super.dismiss();
    }
}
