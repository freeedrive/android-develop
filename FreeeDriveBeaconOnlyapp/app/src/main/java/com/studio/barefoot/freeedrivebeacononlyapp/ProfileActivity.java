package com.studio.barefoot.freeedrivebeacononlyapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.studio.barefoot.freeedrivebeacononlyapp.adapters.DropDownListAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.FetchProfileAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.ProfileAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ChangePhoneNumberDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.DemoModeEndDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.interfacess.IRegisterFD;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.UIUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MySafetyFragmentGraph.DemoModeDialog;

public class ProfileActivity extends AppCompatActivity implements IRegisterFD {
    EditText et_frstName, et_LastName, et_Email, et_Phone, et_Language,et_mobileSync,et_drivePad_Name,et_helpLine,et_helpMail;
    TextView tv_profile, tv_new_phn, tv_new_connector,tv_privacy_policy;
    View actionBarView;
    ImageView spinner,imageView_flags;
    Button img_Update_Profile,btn_sync_newNo,btn_sync_newBeacon;
    ChangePhoneNumberDialog changePhoneNoDialog;
    public static ProgressBarDialog progressBarDialogProfile;
    private String selectedLang = "";
    SharedPreferences sharedpreferences;
    String updatephonenoandconnector;
    String email = "";
    Configuration config;
    private int privateHour = 0;
    RelativeLayout relativeLayout;
    DemoModeEndDialog endDialog;
    FragmentManager fm;
    public ProfileActivity() {
        LocaleUtils.updateConfig(this);
    }


    @SuppressLint("LongLogTag")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        // LocaleUtils.updateConfig(this);
        // For setting up the different logo of toll bar for demo and paid mode
        RelativeLayout toolbar = (RelativeLayout)findViewById(R.id.topToolbar);
        ImageView fd_logo = (ImageView)toolbar.findViewById(R.id.tol_bar_logo);
        if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)){
            fd_logo.setImageResource(R.drawable.logo_freeedrive_demo_mode);
        }  else{
            fd_logo.setImageResource(R.drawable.logo_topbar);
        }
        fm = getSupportFragmentManager();
        endDialog = DemoModeEndDialog.newInstance("");
        sharedpreferences = getSharedPreferences("AutoReply", ProfileActivity.this.MODE_PRIVATE);
        et_frstName = (EditText) findViewById(R.id.et_firstName);
        et_mobileSync = (EditText) findViewById(R.id.et_mobileSync);
        et_drivePad_Name = (EditText) findViewById(R.id.et_beaconSync);
        btn_sync_newNo = (Button) findViewById(R.id.btn_sync_newNo);
        btn_sync_newBeacon = (Button) findViewById(R.id.btn_sync_newBeacon);
        imageView_flags = (ImageView) findViewById(R.id.flags_img);
        et_LastName = (EditText) findViewById(R.id.et_lastName);
        et_Email = (EditText) findViewById(R.id.et_Email);
        et_helpLine = (EditText) findViewById(R.id.et_helpLine);
        et_helpMail = (EditText) findViewById(R.id.et_helpMail);
        //et_Language = (EditText) findViewById(R.id.et_language);
        /*et_Phone = (EditText) findViewById(R.id.et_phone);*/
        tv_new_phn = (TextView) findViewById(R.id.tv_new_phn);
        tv_new_connector = (TextView) findViewById(R.id.tv_new_connector);
        tv_profile = (TextView) findViewById(R.id.hello);
        tv_privacy_policy = (TextView) findViewById(R.id.fd_privacy_policy);
        spinner = (ImageView) findViewById(R.id.language_Spinner);

        /*checkBoxPrivateHours = (CheckBox) findViewById(R.id.checkbox_private_hours);*/

        et_Language = (EditText) findViewById(R.id.et_language);
        img_Update_Profile = (Button) findViewById(R.id.img_Update_Profile);
        actionBarView = getLayoutInflater().inflate(R.layout.custom_toolbarr, null);
        /*tv_new_connector.setTypeface(UIUtils.getInstance().getLightFont(this));
        tv_new_phn.setTypeface(UIUtils.getInstance().getLightFont(this));
        et_Phone.setTypeface(UIUtils.getInstance().getLightFont(this));
        et_Phone.setFocusable(false);*/

        et_frstName.setMaxLines(1);
        et_frstName.setSingleLine();

        et_LastName.setMaxLines(1);
        et_LastName.setSingleLine();

        et_Email.setMaxLines(1);
        et_Email.setSingleLine();
  /*      try{
            if (progressBarDialogProfile == null) {
                progressBarDialogProfile = new ProgressBarDialog(ProfileActivity.this);
            }
            progressBarDialogProfile.setTitle(getString(R.string.title_progress_dialog));
            progressBarDialogProfile.setMessage(getString(R.string.body_progress_dialog));
            progressBarDialogProfile.show();
        }catch (Exception e){
            e.printStackTrace();
        }*/

       /* checkBoxPrivateHours.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    privateHour = 1;
                } else {
                    privateHour = 0;
                }
                Log.e("privateHours", "" + privateHour);
            }
        });
*/
        tv_privacy_policy.setPaintFlags(tv_privacy_policy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tv_privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.freeedrive.com/en/privacy"));
                startActivity(intentBrowser);
            }
        });
        et_helpLine.setEnabled(false);
        et_helpMail.setEnabled(false);
        et_mobileSync.setEnabled(false);
        et_mobileSync.setGravity(Gravity.RIGHT);

        et_drivePad_Name.setEnabled(false);
        et_drivePad_Name.setGravity(Gravity.RIGHT);
        if (DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER) != null && !DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER).isEmpty()){
            et_mobileSync.setText(DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER));
        }
        if(DataHandler.getStringPreferences(AppConstants.PREF_KEY_DRIVE_PAD_NAME)!= null && !DataHandler.getStringPreferences(AppConstants.PREF_KEY_DRIVE_PAD_NAME).isEmpty()){
            et_drivePad_Name.setText(DataHandler.getStringPreferences(AppConstants.PREF_KEY_DRIVE_PAD_NAME));
        }


        if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)!=null && DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)==true) {
            et_Language.setText(getResources().getString(R.string.english));
            imageView_flags.setImageResource(R.drawable.uk);

        }else {


            if (DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG) != null || !DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG).isEmpty()) {
                String currentLang = DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG);
                if (currentLang.equalsIgnoreCase(getResources().getString(R.string.fr))) {
                    currentLang = getResources().getString(R.string.french);
                    imageView_flags.setImageResource(R.drawable.france);
                } else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.nl))) {
                    currentLang = getResources().getString(R.string.dutch);
                    imageView_flags.setImageResource(R.drawable.netherlands);
                } else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.es))) {
                    currentLang = getResources().getString(R.string.spanish);
                    imageView_flags.setImageResource(R.drawable.spain);
                } else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.it))) {
                    currentLang = getResources().getString(R.string.italy);
                    imageView_flags.setImageResource(R.drawable.italy);
                }/*else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.da))){
                currentLang = getResources().getString(R.string.danish);
                imageView_flags.setImageResource(R.drawable.denmark);
            }else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.no))){
                currentLang = getResources().getString(R.string.norway);
                imageView_flags.setImageResource(R.drawable.norway);
            }else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.de))){
                currentLang = getResources().getString(R.string.deutsch);
                imageView_flags.setImageResource(R.drawable.germany);
            }*/ else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.pt))) {
                    currentLang = getResources().getString(R.string.portuguese);
                    imageView_flags.setImageResource(R.drawable.portugal);
                }/*else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.fi))) {
                currentLang = getResources().getString(R.string.finnish);
                imageView_flags.setImageResource(R.drawable.finland);
            }*/ else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.en))) {
                    currentLang = getResources().getString(R.string.english);
                    imageView_flags.setImageResource(R.drawable.uk);
                } else {
                    currentLang = getResources().getString(R.string.english);
                    imageView_flags.setImageResource(R.drawable.uk);
                }
                et_Language.setText(currentLang);
            } else {
                String currentLang = config.locale.getLanguage();
                if (currentLang.equalsIgnoreCase(getResources().getString(R.string.fr))) {
                    currentLang = getResources().getString(R.string.french);
                    imageView_flags.setImageResource(R.drawable.france);
                } else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.nl))) {
                    currentLang = getResources().getString(R.string.dutch);
                    imageView_flags.setImageResource(R.drawable.netherlands);
                } else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.es))) {
                    currentLang = getResources().getString(R.string.spanish);
                    imageView_flags.setImageResource(R.drawable.spain);
                } else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.it))) {
                    currentLang = getResources().getString(R.string.italy);
                    imageView_flags.setImageResource(R.drawable.italy);
                }/*else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.da))){
                currentLang = getResources().getString(R.string.danish);
                imageView_flags.setImageResource(R.drawable.denmark);
            }else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.no))){
                currentLang = getResources().getString(R.string.norway);
                imageView_flags.setImageResource(R.drawable.norway);
            }else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.de))){
                currentLang = getResources().getString(R.string.deutsch);
                imageView_flags.setImageResource(R.drawable.germany);
            }*/ else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.pt))) {
                    currentLang = getResources().getString(R.string.portuguese);
                    imageView_flags.setImageResource(R.drawable.portugal);
                }/*else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.fi))){
                currentLang = getResources().getString(R.string.finnish);
                imageView_flags.setImageResource(R.drawable.finland);
            }*/ else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.en))) {
                    currentLang = getResources().getString(R.string.english);
                    imageView_flags.setImageResource(R.drawable.uk);
                } else {
                    currentLang = getResources().getString(R.string.english);
                    imageView_flags.setImageResource(R.drawable.uk);
                }

                et_Language.setText(currentLang);
            }
        }
        btn_sync_newNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)) {
                    Toast toast= Toast.makeText(ProfileActivity.this,
                            "This option is available in the full version of Freeedrive", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();
                }
                else if (DataHandler.getBooleanPreferences(AppConstants.WABCO_ACCOUNT_VERIFY) == true){
                    Toast toast= Toast.makeText(ProfileActivity.this,
                            "You can not update your Number", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();
                }
                else{
                    Intent intent = new Intent(ProfileActivity.this,NewPhoneNumberTextActivity.class);
                    intent.putExtra("comingfromProfile","updateNo");
                    startActivity(intent);
                }

            }
        });
        btn_sync_newBeacon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startQrcodeIntent = new Intent(ProfileActivity.this, QrCodeActivity.class);
                startQrcodeIntent.putExtra("comingFromProfile", "1");
                startActivity(startQrcodeIntent);
            }
        });


        setupActionBar();
        setupProfile();
        hideSoftKeyboard();

        if(DataHandler.getStringPreferences(AppConstants.PREF_KEY_ACCIDENT_HELP_EMAIL)!= null &&
                !DataHandler.getStringPreferences(AppConstants.PREF_KEY_ACCIDENT_HELP_EMAIL).isEmpty()
                && DataHandler.getStringPreferences(AppConstants.PREF_KEY_ACCIDENT_HELP_PHONE)!= null &&
                !DataHandler.getStringPreferences(AppConstants.PREF_KEY_ACCIDENT_HELP_PHONE).isEmpty()){
            et_helpLine.setText(DataHandler.getStringPreferences(AppConstants.PREF_KEY_ACCIDENT_HELP_PHONE));
            et_helpMail.setText(DataHandler.getStringPreferences(AppConstants.PREF_KEY_ACCIDENT_HELP_EMAIL));

        }
        //et_Language.setText(getResources().getString(R.string.english));
      //  et_Language.setClickable(false);
        et_Language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)) {
                    Toast toast= Toast.makeText(ProfileActivity.this,
                            "This option is available in the full version of Freeedrive", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();                }
                    else{
                    spawnPopUp(et_Language);
                }
                    
            }
        });


        img_Update_Profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (!checkFields()) {
                        if (AppUtils.isNetworkAvailable()) {
                            if (progressBarDialogProfile == null) {
                                progressBarDialogProfile = new ProgressBarDialog(ProfileActivity.this);
                            }
                            progressBarDialogProfile.setTitle(getString(R.string.title_progress_dialog));
                            progressBarDialogProfile.setMessage(getString(R.string.body_progress_dialog));
                            progressBarDialogProfile.show();
                            String lang = "";
                            if (et_Language.getText().toString().equalsIgnoreCase(getResources().getString(R.string.english)))
                                lang = getResources().getString(R.string.en);
                            else if (et_Language.getText().toString().equals(getResources().getString(R.string.french)))
                                lang = getResources().getString(R.string.fr);
                            else if (et_Language.getText().toString().equals(getResources().getString(R.string.spanish)))
                                lang = getResources().getString(R.string.es);
                            else if (et_Language.getText().toString().equals(getResources().getString(R.string.dutch)))
                                lang = getResources().getString(R.string.nl);
                            else if (et_Language.getText().toString().equals(getResources().getString(R.string.italy)))
                                lang = getResources().getString(R.string.it);
                           /* else if (et_Language.getText().toString().equals(getResources().getString(R.string.danish)))
                                lang = getResources().getString(R.string.da);
                            else if (et_Language.getText().toString().equals(getResources().getString(R.string.norway))){
                                lang = getResources().getString(R.string.no);
                            }else if (et_Language.getText().toString().equals(getResources().getString(R.string.deutsch))){
                                lang = getResources().getString(R.string.de);
                            }*/else if (et_Language.getText().toString().equals(getResources().getString(R.string.portuguese))){
                                lang = getResources().getString(R.string.pt);
                            }/*else if (et_Language.getText().toString().equals(getResources().getString(R.string.finnish))){
                                lang = getResources().getString(R.string.fi);
                            }*/
                            else {
                                String jsonObjProfile = DataHandler.getStringPreferences(AppConstants.TEMP_PROFILE_KEY);

                                if (jsonObjProfile != null && !jsonObjProfile.isEmpty()) {

                                    try {
                                        JSONObject jsonObject = new JSONObject(jsonObjProfile);
                                        lang = jsonObject.getString("lang");
                                    } catch (Exception exception) {
                                        exception.printStackTrace();
                                    }

                                }
                            }

                            JSONObject jsonObject = new JSONObject();
                            if (jsonObject != null) {

                                try {
                                    jsonObject.put("first_name", et_frstName.getText().toString().trim());
                                    jsonObject.put("last_name", et_LastName.getText().toString().trim());
                                    jsonObject.put("email", et_Email.getText().toString().trim().toLowerCase());
                                    jsonObject.put("lang", lang);

                                    DataHandler.updatePreferences(AppConstants.TEMP_PROFILE_KEY, jsonObject.toString());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            ProfileAsyncTask profileAsyncTask = new ProfileAsyncTask(ProfileActivity.this, ProfileActivity.this, et_frstName.getText().toString(), et_LastName.getText().toString(), et_Email.getText().toString().trim().toLowerCase(), lang);
                            profileAsyncTask.execute();


                        } else {
                            AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(ProfileActivity.this);
                            error_No_Internet.setMessage(ProfileActivity.this.getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            error_No_Internet.show();
                        }

                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }

            }
        });

        if(getIntent().getStringExtra("LangUpdate")!=null && getIntent().getStringExtra("LangUpdate").equalsIgnoreCase("LangUpdate")
                && getIntent().getStringExtra("updated")!=null){
            String result = getIntent().getStringExtra("updated");

            manageRegister("updated",result);
        }

        if(DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE_SUBSCRIPTION_EXPIRED) == true){
            DemoModeDialog();
        }
    }

    private void DemoModeDialog() {

        try {
            if (endDialog != null && endDialog.isAdded()) {
                return;
            } else {
               /* Log.e("BF-PK-FD", "DEMO_MODE_END_POPUP SHOW");*/
                endDialog.show(fm, "");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    private void spawnPopUp(View layout1) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.pop_up_window, (ViewGroup) findViewById(R.id.popup_layout));
        int dp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, getResources().getDisplayMetrics());
        final PopupWindow popup = new PopupWindow(layout, dp, RelativeLayout.LayoutParams.WRAP_CONTENT, true);
        popup.setTouchable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.setOutsideTouchable(true);
        popup.setWidth(900);
        popup.setHeight(RelativeLayout.LayoutParams.WRAP_CONTENT);
        popup.setTouchInterceptor(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popup.dismiss();
                    return true;
                }
                return false;
            }
        });
        popup.setContentView(layout);
        popup.showAsDropDown(layout1);


        //populate the drop-down list
        final List<String> languages = new LinkedList<String>();
        languages.add(getResources().getString(R.string.english));
        languages.add(getResources().getString(R.string.french));
        languages.add(getResources().getString(R.string.dutch));
        languages.add(getResources().getString(R.string.spanish));
        languages.add(getResources().getString(R.string.italy));
      /*  languages.add(getResources().getString(R.string.danish));
        languages.add(getResources().getString(R.string.norway));
        languages.add(getResources().getString(R.string.deutsch));
      */  languages.add(getResources().getString(R.string.portuguese));
/*
        languages.add(getResources().getString(R.string.finnish));
*/
        final ListView list = (ListView) layout.findViewById(R.id.list);
        DropDownListAdapter adapter = new DropDownListAdapter(ProfileActivity.this, languages);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedLang = languages.get(position).toString();
                popup.dismiss();
                et_Language.setText(selectedLang);
                if (selectedLang.equals(getResources().getString(R.string.english))) {
                    imageView_flags.setImageResource(R.drawable.uk);
                    selectedLang = getResources().getString(R.string.en);
                } else if (selectedLang.equals(getResources().getString(R.string.french))) {
                    selectedLang = getResources().getString(R.string.fr);
                    imageView_flags.setImageResource(R.drawable.france);
                } else if (selectedLang.equals(getResources().getString(R.string.dutch))) {
                    selectedLang = getResources().getString(R.string.nl);
                    imageView_flags.setImageResource(R.drawable.netherlands);
                } else if (selectedLang.equals(getResources().getString(R.string.spanish))) {
                    selectedLang = getResources().getString(R.string.es);
                    imageView_flags.setImageResource(R.drawable.spain);
                }
                else if (selectedLang.equals(getResources().getString(R.string.italy))) {
                    selectedLang = getResources().getString(R.string.it);
                    imageView_flags.setImageResource(R.drawable.italy);
                }
               /* else if (selectedLang.equals(getResources().getString(R.string.danish))) {
                    selectedLang = getResources().getString(R.string.da);
                    imageView_flags.setImageResource(R.drawable.denmark);
                }
                else if (selectedLang.equals(getResources().getString(R.string.norway))) {
                    selectedLang = getResources().getString(R.string.no);
                    imageView_flags.setImageResource(R.drawable.norway);
                }
                else if (selectedLang.equals(getResources().getString(R.string.deutsch))){
                    selectedLang = getResources().getString(R.string.de);
                    imageView_flags.setImageResource(R.drawable.germany);
                }*/
                else if (selectedLang.equals(getResources().getString(R.string.portuguese))){
                    selectedLang = getResources().getString(R.string.pt);
                    imageView_flags.setImageResource(R.drawable.portugal);
                }/*else if (selectedLang.equals(getResources().getString(R.string.finnish))){
                    selectedLang = getResources().getString(R.string.fi);
                    imageView_flags.setImageResource(R.drawable.finland);
                }*/
                else {
                    selectedLang = getResources().getString(R.string.en);
                    imageView_flags.setImageResource(R.drawable.uk);
                }
            }
        });
    }

    private void restartActivity() {
        Intent intent = new Intent(ProfileActivity.this, ProfileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(intent);
    }

    public boolean checkFields() {
        et_frstName.setError(null);
        et_LastName.setError(null);
        et_Email.setError(null);
        /*et_Phone.setError(null);*/
        et_Language.setError(null);
        boolean cancel = false;
        View focusView = null;
        // email= et_Email.getText().toString().trim().replaceAll("\\s++$", "");
        // email= et_Email.getText().toString().trim().replaceAll("^\\s+|\\s+$", "");
        if (TextUtils.isEmpty(et_frstName.getText().toString().trim())) {

            et_frstName.setError(getString(R.string.error_field_required));
            focusView = et_frstName;
            cancel = true;

        } else if (TextUtils.isEmpty(et_LastName.getText().toString().trim())) {

            et_LastName.setError(getString(R.string.error_field_required));
            focusView = et_LastName;
            cancel = true;
        } else if (TextUtils.isEmpty(et_Email.getText().toString().trim())) {

            et_Email.setError(getString(R.string.error_field_required));
            focusView = et_Email;
            cancel = true;
        } else if (!AppUtils.emailValidator(et_Email.getText().toString().trim())) {

            et_Email.setError(getString(R.string.valid_email));
            focusView = et_Email;
            cancel = true;
        }
        /*else if (TextUtils.isEmpty(et_Phone.getText().toString().trim())) {

            et_Phone.setError(getString(R.string.error_field_required));
            focusView = et_Phone;
            cancel = true;
        }*/ else if (TextUtils.isEmpty(et_Language.getText().toString())) {

            et_Language.setError(getString(R.string.error_field_required));
            focusView = et_Language;
            cancel = true;
        }
        if (cancel) {

            focusView.requestFocus();

        }
        return cancel;
    }

    private void setupProfile() {
        String jsonObjProfile = DataHandler.getStringPreferences(AppConstants.TEMP_PROFILE_KEY);
        Log.e("jsonObjProfile :", jsonObjProfile);
        if (jsonObjProfile != null && !jsonObjProfile.isEmpty()) {
            try {
                JSONObject jsonObject = new JSONObject(jsonObjProfile);
                et_frstName.setText(jsonObject.getString("first_name"));
                et_LastName.setText(jsonObject.getString("last_name"));
                et_Email.setText(jsonObject.getString("email").trim());
                et_mobileSync.setText(DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER));
              /*  int private_hr_enable = (jsonObject.getInt("private_hour_enable"));
                Log.e("private_hour_enable",""+private_hr_enable);
                if(private_hr_enable == 1){
                    checkBoxPrivateHours.setChecked(true);
                }
                else{
                    checkBoxPrivateHours.setChecked(false);
                }*/
                String lang = "";
                if (jsonObject.getString("lang").equalsIgnoreCase(getResources().getString(R.string.en)))
                    lang = "English";
                else if (jsonObject.getString("lang").equalsIgnoreCase(getResources().getString(R.string.fr)))
                    lang = "Français";
                else if (jsonObject.getString("lang").equalsIgnoreCase(getResources().getString(R.string.nl)))
                    lang = "Dutch";
                else if (jsonObject.getString("lang").equalsIgnoreCase(getResources().getString(R.string.es)))
                    lang = "Español";
                else if (jsonObject.getString("lang").equalsIgnoreCase(getResources().getString(R.string.it)))
                    lang = "Italian";
                /*else if (jsonObject.getString("lang").equalsIgnoreCase(getResources().getString(R.string.da)))
                    lang = "Danish";
                else if (jsonObject.getString("lang").equalsIgnoreCase(getResources().getString(R.string.no)))
                    lang = "Norwegian";
                else if (jsonObject.getString("lang").equalsIgnoreCase(getResources().getString(R.string.de)))
                    lang = getResources().getString(R.string.deutsch);
               */ else if (jsonObject.getString("lang").equalsIgnoreCase(getResources().getString(R.string.pt)))
                    lang = getResources().getString(R.string.portuguese);
              /*  else if (jsonObject.getString("lang").equalsIgnoreCase(getResources().getString(R.string.fi)))
                    lang = getResources().getString(R.string.finnish);
             */   else lang = "English";
                et_Language.setText(lang);
            /*     String lang = "";
                 if (jsonObject.getString("language_id").equals("1"))
                     lang = "English";
                 else if (jsonObject.getString("language_id").equals("2"))
                     lang = "French";
                 else if(jsonObject.getString("language_id").equals("3"))
                     lang = "Dutch";
                 else lang="English";
                 et_Language.setText(lang);
                 */
          /*      try{
                    progressBarDialogProfile.dismiss();
                    progressBarDialogProfile = null;
                }catch (NullPointerException exception){
                    exception.printStackTrace();
                }*/

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            /*et_Phone.setText(DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER));*/

        } else {
            if (AppUtils.isNetworkAvailable()) {

                fetchUserData();
            } else {
                AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(ProfileActivity.this);
                error_No_Internet.setMessage(ProfileActivity.this.getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                error_No_Internet.show();
            /*    try{
                    progressBarDialogProfile.dismiss();
                    progressBarDialogProfile = null;
                }catch (NullPointerException exception){
                    exception.printStackTrace();
                }*/
            }
        }
    }
    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
    public void fetchUserData() {

        try {
            if (progressBarDialogProfile == null) {
                progressBarDialogProfile = new ProgressBarDialog(ProfileActivity.this);
            }
            progressBarDialogProfile.setTitle(getString(R.string.title_progress_dialog));
            progressBarDialogProfile.setMessage(getString(R.string.body_progress_dialog));
            progressBarDialogProfile.show();
            List<NameValuePair> mParams = new ArrayList<NameValuePair>();
            final TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String deviceId = telephonyManager.getDeviceId();
            mParams.add(new BasicNameValuePair("devID",deviceId));
            mParams.add(new BasicNameValuePair("phone_number", DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
            Log.e("ProfileParams", mParams.toString());
            FetchProfileAsyncTask fetchProfileAsyncTask = new FetchProfileAsyncTask(ProfileActivity.this, WebServiceConstants.END_POINT_FETCH_PROFILE, mParams);
            fetchProfileAsyncTask.execute();
        } catch (NullPointerException exception) {

        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }


    @Override
    public void manageRegister(String response, String r) {
        if (response != null && response.equals("updated")) {
            try {
                JSONObject jsonObject = new JSONObject(r);
                if (jsonObject != null) {
                    String accounto = DataHandler.getStringPreferences(AppConstants.TEMP_PROFILE_KEY);
                 //for displaing values in the fragments
                    String displayAccount =  DataHandler.getStringPreferences(AppConstants.TEMP_DISPLAY_KEY);
                    JSONObject acc1 = new JSONObject(displayAccount);
                    JSONObject acc = new JSONObject(accounto);
                    acc.put("email", jsonObject.getString("email").trim());
                    acc.put("first_name", jsonObject.getString("first_name"));
                    acc1.put("first_name", jsonObject.getString("first_name"));

                    acc.put("last_name", jsonObject.getString("last_name"));
                    acc.put("lang", jsonObject.getString("lang"));
                    acc.put("phone_number", jsonObject.getString("phone_number"));
                    /*acc.put("private_hour_enable", jsonObject.getInt("private_hour_enable"));
                    int private_hour_enable_bit = jsonObject.getInt("private_hour_enable");
                    Log.e("manageregister"," : "+private_hour_enable_bit);*/
//                   acc.put("total_bad_counts",jsonObject.get("total_bad_counts"));
//                   acc.put("total_ride_time",jsonObject.get("total_time_elapsed"));
                    DataHandler.updatePreferences(AppConstants.TEMP_DISPLAY_KEY, acc1.toString());
                    DataHandler.updatePreferences(AppConstants.TEMP_PROFILE_KEY, acc.toString());
                    AlertDialog.Builder reg = new AlertDialog.Builder(this);
                    reg.setMessage(getResources().getString(R.string.profile_updated)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            String jsonObjProfile = DataHandler.getStringPreferences(AppConstants.TEMP_PROFILE_KEY);

                            if (jsonObjProfile != null && !jsonObjProfile.isEmpty()) {

                                try {
                                    JSONObject jsonObject = new JSONObject(jsonObjProfile);
                                    String appLang = jsonObject.getString("lang");
                                    Log.e("appLang", appLang);
                                    selectedLang = jsonObject.getString("lang");
                                }
                                catch (Exception exception) {
                                    exception.printStackTrace();
                                }

                            }
                            ApplicationController controller = (ApplicationController) getApplication();
                            controller.updateLocale(selectedLang);
                            Intent intent = new Intent(ProfileActivity.this, MenuActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            DataHandler.updatePreferences(AppConstants.PREF_KEY_LANG, selectedLang);
                            finish();
                          //  DataHandler.updatePreferences(AppConstants.PREF_KEY_LANG, "en");
                            //   setupProfile();
                        }
                    });
                    reg.show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ProfileActivity.this, MenuActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        finish();
    }*/
}
