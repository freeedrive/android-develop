package com.studio.barefoot.freeedrivebeacononlyapp.beans;

import java.sql.Timestamp;

/**
 * Created by mcs on 12/27/2016.
 */

public class RidesBeans {
    /**
     * Id score
     */
    private int ID;
    /**
     * departure time of journer
     */
    private long departure_time;
    /**
     * arrival time of journey
     */
    private long arrival_time;
    /**
     * departure location of journey
     */
    private String departure_location;
    /**
     * arrival departure of journey
     */
    private String arrival_location;
    /**
     * difference between two times
     */
    private double time_elapsed;
    /**
     * score of journey
     */
    private double score;

    // COUNT OF BAD BEHAVIOUR
    private int bad_behaviour;

    private int sudden_accelaration;
    private int sudden_braking;
    private int over_speeding_count;
    private String time_zone;
    private int orphandrideBit;
    private String departureDate;
    private long totalpassengerModeTime;

    public int getBad_behaviour() {
        return bad_behaviour;
    }

    public void setBad_behaviour(int bad_behaviour) {
        this.bad_behaviour = bad_behaviour;
    }

    /**
     * driver id
     */
    private int driver_id;
    /**
     * company id
     */
    private int company_id;
    /**
     * Indicates if ride is saved in server
     */
    private Boolean send;

    /**
     * Represents the time of score : HH:mm:ss
     */
    private String time;

    /**
     * anticipe moyenne pondéré
     */
    private double averageP;

    /**
     * Time total in secondes
     */
    private int timeSecondes;


    // Saving time greater than 120km
    private Timestamp starttime;

    //Saving timer less than 120km
    private Timestamp endtime;

    //Total time above 120km
    private long totalTimeAbove120 = 0;

    private Timestamp passengermodeOn;
    private Timestamp passengermodeOff;
    //total distance travel
    private int totalDistance = 0;

    private int causeofendingRide = 0;


    public RidesBeans(){

    }

    public int getOver_speeding_count() {
        return over_speeding_count;
    }

    public void setOver_speeding_count(int over_speeding_count) {
        this.over_speeding_count = over_speeding_count;
    }

    public String getTime_zone() {
        return time_zone;
    }

    public void setTime_zone(String time_zone) {
        this.time_zone = time_zone;
    }

    /**
     * Allows to get ID
     * @return(out), @Integer represents the ID
     */
    public int getID() {
        return ID;
    }

    /**
     * Allows to set the ID
     * @param ID(in), @Integer represent the new ID
     */
    public void setID(int ID) {
        this.ID = ID;
    }

    /**
     * Allows to get departure time
     * @return(out), @double represents the departure time
     */
    public long getDeparture_time() {
        return departure_time;
    }
    /**
     * Allows to set the departure time
     * @param departure_time(in), @Double represent the new departure time
     */
    public void setDeparture_time(long departure_time) {
        this.departure_time = departure_time;
    }

    /**
     * Allows to get arrival time
     * @return(out), @double represents the arrival time
     */
    public long getArrival_time() {
        return arrival_time;
    }
    /**
     * Allows to set the arrival time
     * @param arrival_time(in), @Double represent the new arrival time
     */
    public void setArrival_time(long arrival_time) {
        this.arrival_time = arrival_time;
    }

    /**
     * Allows to get departure location
     * @return(out), @String represents the departure location
     */
    public String getDeparture_location() {
        return departure_location;
    }
    /**
     * Allows to set the departure location
     * @param departure_location(in), @String represent the new departure location
     */
    public void setDeparture_location(String departure_location) {
        this.departure_location = departure_location;
    }

    /**
     * Allows to get arrival location
     * @return(out), @String represents the arrival location
     */
    public String getArrival_location() {
        return arrival_location;
    }
    /**
     * Allows to set the arrival location
     * @param arrival_location(in), @String represent the new arrival location
     */
    public void setArrival_location(String arrival_location) {
        this.arrival_location = arrival_location;
    }

    /**
     * Allows to get elapsed time
     * @return(out), @double represents the elapsed time
     */
    public double getTime_elapsed() {
        return time_elapsed;
    }
    /**
     * Allows to set the elapsed time
     * @param time_elapsed(in), @Double represent the new elapsed time
     */
    public void setTime_elapsed(double time_elapsed) {
        this.time_elapsed = time_elapsed;
    }

    /**
     * Allows to get score
     * @return(out), @double represents the score
     */
    public double getScore() {
        return score;
    }
    /**
     * Allows to set the score
     * @param score(in), @Double represent the new score
     */
    public void setScore(double score) {
        this.score = score;
    }

    /**
     * Allows to get driver id
     * @return(out), @Integer represents the driver id
     */
    public int getDriver_id() {
        return driver_id;
    }
    /**
     * Allows to set the driver id
     * @param driver_id(in), @Integer represent the new driver id
     */
    public void setDriver_id(int driver_id) {
        this.driver_id = driver_id;
    }

    /**
     * Allows to get company id
     * @return(out), @Integer represents the company id
     */
    public int getCompany_id() {
        return company_id;
    }
    /**
     * Allows to set the company id
     * @param company_id(in), @Integer represent the new company id
     */
    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    /**
     * Allows to get send
     * @return(out), @IBoolean represents the send
     */
    public Boolean getSend() {
        return send;
    }
    /**
     * Allows to set the send
     * @param send(in), @Boolean represent the new value
     */

    public void setSend(Boolean send) {
        this.send = send;
    }

    /**
     * Allows to get time in string
     * @return(out), @String represents the time in string
     */
    public String getTime(){return time; }

    /**
     * Allows to set the time
     * @param time(in), @String represent the new time
     */
    public void setTime(String time){ this.time = time; }

    /**
     * Allows to get average
     * @return(out), @double represents the average
     */
    public double getAverageP() {
        return averageP;
    }
    /**
     * Allows to set the average
     * @param avg(in), @Double represent the new value
     */
    public void setAverageP(double avg){
        this.averageP = avg;
    }

    /**
     * Allows to get dtotal time in secondes
     * @return(out), @Integer represents the total time in secondes
     */
    public int getTimeSecondes() {
        return timeSecondes;
    }
    /**
     * Allows to set the time in secondes
     * @param timeSecondes(in), @Integer represent the new value
     */
    public void setTimeSecondes(int timeSecondes){
        this.timeSecondes = timeSecondes;
    }

    public Timestamp getStarttime() {
        return starttime;
    }

    public void setStarttime(Timestamp starttime) {
        this.starttime = starttime;
    }

    public Timestamp getEndtime() {
        return endtime;
    }

    public void setEndtime(Timestamp endtime) {
        this.endtime = endtime;
    }

    public long getTotalTimeAbove120() {
        return totalTimeAbove120;
    }

    public void setTotalTimeAbove120(long totalTimeAbove120) {
        this.totalTimeAbove120 = totalTimeAbove120;
    }

    public int getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(int totalDistance) {
        this.totalDistance = totalDistance;
    }

    public int getCauseofendingRide() {
        return causeofendingRide;
    }

    public void setCauseofendingRide(int causeofendingRide) {
        this.causeofendingRide = causeofendingRide;
    }

    public int getSudden_accelaration() {
        return sudden_accelaration;
    }

    public void setSudden_accelaration(int sudden_accelaration) {
        this.sudden_accelaration = sudden_accelaration;
    }

    public int getSudden_braking() {
        return sudden_braking;
    }

    public void setSudden_braking(int sudden_braking) {
        this.sudden_braking = sudden_braking;
    }

    public int getOrphandrideBit() {
        return orphandrideBit;
    }

    public void setOrphandrideBit(int orphandrideBit) {
        this.orphandrideBit = orphandrideBit;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public long getTotalpassengerModeTime() {
        return totalpassengerModeTime;
    }

    public void setTotalpassengerModeTime(long totalpassengerModeTime) {
        this.totalpassengerModeTime = totalpassengerModeTime;
    }

    public Timestamp getPassengermodeOn() {
        return passengermodeOn;
    }

    public void setPassengermodeOn(Timestamp passengermodeOn) {
        this.passengermodeOn = passengermodeOn;
    }

    public Timestamp getPassengermodeOff() {
        return passengermodeOff;
    }

    public void setPassengermodeOff(Timestamp passengermodeOff) {
        this.passengermodeOff = passengermodeOff;
    }

}
