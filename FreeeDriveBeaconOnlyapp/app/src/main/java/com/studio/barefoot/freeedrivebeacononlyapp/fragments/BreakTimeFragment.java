package com.studio.barefoot.freeedrivebeacononlyapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.txusballesteros.widgets.FitChart;

/**
 * A simple {@link Fragment} subclass.
 */
public class BreakTimeFragment extends Fragment {

    View viewbreakTime;
    FitChart fitchart_numberofstep,fitchart_numberofminutes;
    TextView txtviewtotalnoofminutes;

    public BreakTimeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewbreakTime =  inflater.inflate(R.layout.fragment_break_time, container, false);

        fitchart_numberofstep = (FitChart) viewbreakTime.findViewById(R.id.break_time_fitcharttotalrides_number_of_steps);
        fitchart_numberofminutes = (FitChart) viewbreakTime.findViewById(R.id.break_time_fitcharttotalrides_number_of_minutes);
        txtviewtotalnoofminutes = (TextView) viewbreakTime.findViewById(R.id.total_no_of_minutes);
        fitchart_numberofstep.setValue(100);
        fitchart_numberofminutes.setValue(50);
        txtviewtotalnoofminutes.setText(""+50);
        return viewbreakTime;
    }

}
