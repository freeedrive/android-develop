package com.studio.barefoot.freeedrivebeacononlyapp.dialogs;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.studio.barefoot.freeedrivebeacononlyapp.QrCodeActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.QrCodeAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.QrCodeAsyncTaskRecovery;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import static android.R.attr.phoneNumber;

/**
 * Created by mcs on 12/28/2016.
 */

public class QrCodeUIDdialog extends BaseAlertDialog implements DialogInterface.OnClickListener {
    TextView tv_uuid;
    ContentResolver resolver;
    String uuid="";
    private String dashless="";
    public static ProgressBarDialog progressBarDialogQrCodeDialog;
    Context context;
    Boolean recovery = false;
    public QrCodeUIDdialog(Context context,String UID,Boolean recoveryy) {
        super(context);

        LayoutInflater factory = LayoutInflater.from(context);
        final View progressBarView = factory.inflate(R.layout.custom_dialog_uuid, null);

        setView(progressBarView);
        this.context = context;
        tv_uuid = (TextView) progressBarView.findViewById(R.id.tv_uuid);
        setButton(BUTTON_POSITIVE, context.getString(R.string.action_send), this);

        resolver = context.getContentResolver();

        if (UID != null && !UID.isEmpty() && UID.length() == 32){
            setTitle(context.getString(R.string.title_uuid_dialog));
            tv_uuid.setText(UID);
        }
        else if(UID.length() !=32 && !UID.isEmpty()){
            Log.e("iBKS Serial",UID);
            setTitle(context.getString(R.string.title_ibks_serial_dialog));
            tv_uuid.setText(UID);
        }
        uuid =UID;
        setCancelable(false);
        if(DataHandler.getStringPreferences(AppConstants.PHONE_TEMP).isEmpty()){
            String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_TEMP);
            DataHandler.updatePreferences(AppConstants.PHONE_NUMBER,phoneNumber);
        }
        else{
            String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_TEMP);
            DataHandler.updatePreferences(AppConstants.PHONE_NUMBER,phoneNumber);
        }
        recovery = true;
    }
    public QrCodeUIDdialog(Context context,String UID) {
        super(context);

        LayoutInflater factory = LayoutInflater.from(context);
        final View progressBarView = factory.inflate(R.layout.custom_dialog_uuid, null);

        setView(progressBarView);
        this.context = context;
        tv_uuid = (TextView) progressBarView.findViewById(R.id.tv_uuid);
        setButton(BUTTON_POSITIVE, context.getString(R.string.action_send), this);

        resolver = context.getContentResolver();

        if (UID != null && !UID.isEmpty() && UID.length() == 32){
            setTitle(context.getString(R.string.title_uuid_dialog));
            tv_uuid.setText(UID);
        }
        else if(UID.length() !=32 && !UID.isEmpty()){
            Log.e("iBKS Serial",UID);
            setTitle(context.getString(R.string.title_ibks_serial_dialog));
            tv_uuid.setText(UID);
        }
        uuid =UID;
        setCancelable(false);
        if(DataHandler.getStringPreferences(AppConstants.PHONE_TEMP).isEmpty()){
        String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_TEMP);
        DataHandler.updatePreferences(AppConstants.PHONE_NUMBER,phoneNumber);
        }
        else{
            String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_TEMP);
            DataHandler.updatePreferences(AppConstants.PHONE_NUMBER,phoneNumber);
        }
        recovery = false;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
         dismiss();

        if (uuid.length()>32&&uuid.contains("-")){
            dashless= AppUtils.removeDashes(uuid);
            uuid = dashless;
            dismiss();
            if (recovery ==true){
                validateQrCodeRecovery();
            }else{
                validateQrCode();
            }
        }

       else  if (uuid.length() == 32 || uuid.length() != 32){
          /*  final int mid = 20; //get the middle of the String
            String[] parts = {dashless.substring(0, mid),dashless.substring(mid)};
            String nameSpace = parts[0];
            String instanceID=parts[1];
            DataHandler.updatePreferences(AppConstants.TEMP_UUID_NAME_SPACE,nameSpace);
            DataHandler.updatePreferences(AppConstants.TEMP_UUID_INSTANCE_ID,instanceID);*/
            dismiss();
            if (recovery ==true){
                validateQrCodeRecovery();
            }else{
                validateQrCode();
            }
        }
       /* else if(dashless.length()!=32){

            DataHandler.updatePreferences(AppConstants.TEMP_IBKS_SERIAL_NO,dashless);
            Intent refresh = new Intent(context,QrCodeActivity.class);
            refresh.putExtra("invalidQrcode","invalidQrcode");
            context.startActivity(refresh);
            dismiss();

        }*/

        /*else{
            dismiss();
            Toast.makeText(context,"Wrong uuid code",Toast.LENGTH_LONG).show();
            Intent startQrcodeIntent = new Intent(context,QrCodeActivity.class);
            startQrcodeIntent.putExtra("comingFromProfile","1");
            context. startActivity(startQrcodeIntent);

        }*/


    }


    public void validateQrCodeRecovery(   ){
        Log.e("Recovery is called", "I AM CALLED" );
        List<NameValuePair> mParams = new ArrayList<NameValuePair>();

        String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER);
        //String uuid = DataHandler.getStringPreferences(AppConstants.UUID);

              /*  final TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                String deviceId = telephonyManager.getDeviceId();
                mParams.add(new BasicNameValuePair("devID",deviceId));*/

        mParams.add(new BasicNameValuePair("phone_number", phoneNumber));
        mParams.add(new BasicNameValuePair("uuid", uuid));
        progressBarDialogQrCodeDialog = new ProgressBarDialog(context);
        progressBarDialogQrCodeDialog.setTitle(context.getString(R.string.title_progress_dialog));
        progressBarDialogQrCodeDialog.setMessage(context.getString(R.string.body_progress_dialog));
        progressBarDialogQrCodeDialog.show();
        Log.e("mParams",""+mParams);
        QrCodeAsyncTaskRecovery qrCodeAsyncTaskRecovery = new QrCodeAsyncTaskRecovery(context,WebServiceConstants.END_POINT_RECOVERY_VIA_UUID,mParams);
        qrCodeAsyncTaskRecovery.execute();
    }



    public void validateQrCode(){
        List<NameValuePair> mParams = new ArrayList<NameValuePair>();
        final TelephonyManager telephonyManager = (TelephonyManager)context. getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = telephonyManager.getDeviceId();
        mParams.add(new BasicNameValuePair("devID",deviceId));
        mParams.add(new BasicNameValuePair("phone_number", DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
        mParams.add(new BasicNameValuePair("uuid", uuid));
        progressBarDialogQrCodeDialog = new ProgressBarDialog(context);
        progressBarDialogQrCodeDialog.setTitle(context.getString(R.string.title_progress_dialog));
        progressBarDialogQrCodeDialog.setMessage(context.getString(R.string.body_progress_dialog));
        progressBarDialogQrCodeDialog.show();
        QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(context, WebServiceConstants.END_POINT_UUID_v3, mParams);
        qrCodeAsyncTask.execute();
    }
}
