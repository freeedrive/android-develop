/*
package com.studio.barefoot.freeedrivebeacononlyapp.fragments;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.studio.barefoot.freeedrivebeacononlyapp.AutoReplyActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.ContactUsActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.CreditsActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.FaqActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.MainActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.ProfileActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.ScoreSynchronizationActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.MenuAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.QrCodeAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.QrCodeAsyncTaskRecovery;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.PrivateHoursDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.TurOnGpsDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.TurnOnBTDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.TurnOnDonotDisturbDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.TurnOnNotifcationDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.services.Detector;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


*/
/**
 * Created by yasir on 25/04/2017.
 *//*


public class HomeScreenFragment extends Fragment {
    View actionBarView;
    private static DrawerLayout mDrawerLayout;
    private ListView mMenuList;
    ImageView drawerImage,imageView_thums,action_share_on_newsfeed_via_actionbar,imagePhone;
    private float temptotaltime;
    private int totalbadbehaviour;
    TextView  tv_start_time,tv_menu_total_score,tv_no_of_distractions,tv_last_ride_time,tv_menu_arrival,tv_menu_remarks,tv_menu_remarks_desc;
    private TurnOnBTDialog turnOnBTDialog;
    private TurnOnDonotDisturbDialog turnOnDonotDisturbDialog;
    private TurnOnNotifcationDialog turnOnNotifcationDialog;
    private TurOnGpsDialog turOnGpsDialog;
    RelativeLayout layout_behaviour;
    private static final String TAG = MainActivity.class.getSimpleName();
    EditText enterSpeed;
    public static int speed;
    ImageView imagePerformanceCar;
    RelativeLayout relativeLayoutDrivingPerformance;
    TextView TVdrivingPerformanceScore, TVtotalBadBehaviors;
    public HomeScreenFragment()
    {

    }

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Getting application context
        Context context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        actionBarView = inflater.inflate(R.layout.main_fragment_layout_new,container,false);



        relativeLayoutDrivingPerformance = (RelativeLayout) actionBarView.findViewById(R.id.relative_driving_performance) ;
        imagePerformanceCar = (ImageView)actionBarView. findViewById(R.id.image_car);
        TVdrivingPerformanceScore = (TextView) actionBarView.findViewById(R.id.tv_menu_total_score_driving);
        TVtotalBadBehaviors = (TextView) actionBarView.findViewById(R.id.tv_no_of_bad_behaviours);



        //actionBarView = inflater.inflate(R.layout.custom_toolbarr, null);
        tv_menu_remarks = (TextView) actionBarView.findViewById(R.id.tv_menu_remarks);
        tv_menu_remarks_desc = (TextView) actionBarView.findViewById(R.id.tv_menu_remarks_desc);
        tv_menu_arrival = (TextView) actionBarView.findViewById(R.id.tv_end_time);
        tv_start_time = (TextView) actionBarView.findViewById(R.id.tv_start_time);

        //tv_menu_total_distractions = (TextView) findViewById(R.id.tv_menu_total_distractions);
        layout_behaviour = (RelativeLayout) actionBarView.findViewById(R.id.layout_behaviour);

        //tv_all_ride_time = (TextView) findViewById(R.id.tv_menu_allmyRidesTime);
        tv_last_ride_time = (TextView) actionBarView.findViewById(R.id.tv_lastRide_min);
        tv_no_of_distractions = (TextView) actionBarView.findViewById(R.id.tv_no_of_distractions);
        tv_menu_total_score = (TextView) actionBarView.findViewById(R.id.tv_menu_total_score);
        imageView_thums = (ImageView) actionBarView.findViewById(R.id.imageView_thums);
        action_share_on_newsfeed_via_actionbar = (ImageView) actionBarView.findViewById(R.id.action_share_on_newsfeed_via_actionbar);


        enterSpeed = (EditText)actionBarView. findViewById(R.id.manuallySpeed);

        if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)!=null && DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)==false){
            LockDrivingPerformance();
        }
        else{
            UnLockDrivingPerformance();
        }
        File f = getActivity().getDatabasePath("freeedriveV2.db");

        long dbSize = f.getTotalSpace();
        long length=      f.length();
        Log.e("DBSIZE",""+dbSize+" length--> "+length);

        imagePhone = (ImageView) actionBarView.findViewById(R.id.image_phone);
        imagePhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



             */
/*   EcoDrivingDialog ecoDrivingDialog  = new EcoDrivingDialog(getActivity());
                ecoDrivingDialog.show();*//*

             */
/*   PrivateHoursDialog privateHoursDialog = new PrivateHoursDialog(getActivity());
                privateHoursDialog.show();*//*

                */
/*double latitude = 33.738045;
                double longitude = 73.084488;

                Uri gmmIntentUri = Uri.parse("google.navigation:q="+latitude+","+ longitude+" &mode=w");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");

                startActivity(mapIntent);

                */
/*if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }*//*

            }
        });


        enterSpeed.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    String temp = enterSpeed.getText().toString();
                    //speedCheck.setText(temp);
                    speed = Integer.parseInt(temp);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });


        try {

            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.e("refreshedToken", refreshedToken);
        }catch (Exception e){
            e.printStackTrace();
        }
        turOnGpsDialog = new TurOnGpsDialog(getContext());
        turnOnBTDialog =new TurnOnBTDialog(getContext());
        turnOnDonotDisturbDialog = new TurnOnDonotDisturbDialog(getContext());
        turnOnNotifcationDialog =new TurnOnNotifcationDialog(getContext());

*/
/*
        enterSpeed = (EditText) actionBarView.findViewById(R.id.manuallySpeed);
*//*


        try
        {
            if(!isNotificationManager())
            {
                //Log.i("Adneom","*** (Detector) ask permission notification : "+isAlreadyAsking+" *** ");
                Intent intent = new Intent("com.freeedrive.notification_settings");
                getActivity().sendBroadcast(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try{
        mDrawerLayout = (DrawerLayout) actionBarView.findViewById(R.id.drawerLayout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerImage = (ImageView) actionBarView.findViewById(R.id.customisedDrawer);
        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
        }catch (NullPointerException exception){
            exception.printStackTrace();
        }

*/
/*        imagePerformanceCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                mParams.add(new BasicNameValuePair("phone_number",DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
                FetchDrivingPerformanceScores fetchDrivingPerformanceScores = new FetchDrivingPerformanceScores(getActivity(), WebServiceConstants.END_POINT_SPEEDING_SCORE,mParams);
                fetchDrivingPerformanceScores.execute();
            }
        });*//*



*/
/*        enterSpeed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    String temp = enterSpeed.getText().toString();
                    //speedCheck.setText(temp);
                    speed = Integer.parseInt(temp);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });*//*




        try{
        mMenuList = (ListView) mDrawerLayout.findViewById(R.id.listViewNotifications);
        mMenuList.setAdapter(new MenuAdapter(getActivity()));
        mMenuList.setFooterDividersEnabled(false);
        mMenuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    //Log.i("Adneom", " *** Menu bt *** ");

                    Intent i = new Intent(getActivity(), FaqActivity.class);
                    startActivity(i);
                }
                else if (position == 1) {
                    //Log.i("Adneom", " *** Menu bt *** ");

              */
/*      Intent i = new Intent(getActivity(), NotificationActivity.class);
                    startActivity(i);*//*

                } else if (position == 2) {
                    Intent i = new Intent(getActivity(), ProfileActivity.class);
                    startActivity(i);

                }
                else if (position == 3) {
                    Intent i = new Intent(getActivity(), AutoReplyActivity.class);
                    startActivity(i);
                }
                else if (position == 4) {

                    Intent i = new Intent(getActivity(), ContactUsActivity.class);
                    i.putExtra("accountMode", true);
                    startActivity(i);
                } else if (position == 5) {
                    Intent i = new Intent(getActivity(), CreditsActivity.class);
                    startActivity(i);
                } else if (position == 6) {

                }
            }
        });
        }catch (NullPointerException exception){
            exception.printStackTrace();
        }
        // Get the ActionBar here to configure the way it behaves.

        // disable the default title element here (for centered title)

        //setupActionBar();
        try {


        drawerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!mDrawerLayout.isDrawerOpen(Gravity.RIGHT))
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                else
                    mDrawerLayout.closeDrawers();
            }
        });
        }catch (NullPointerException exception){
            exception.printStackTrace();
        }

        action_share_on_newsfeed_via_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }

        });
        return  actionBarView;
    }

    private void UnLockDrivingPerformance() {
        relativeLayoutDrivingPerformance.setBackgroundColor(getActivity().getResources().getColor(R.color.colorBG));
        TVdrivingPerformanceScore.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
        imagePerformanceCar.setImageResource(R.drawable.icon_car_blue);
        TVtotalBadBehaviors.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
    }

    public void LockDrivingPerformance(){
             relativeLayoutDrivingPerformance.setBackgroundColor(getActivity().getResources().getColor(R.color.color_Performance_pack));
             TVdrivingPerformanceScore.setTextColor(getActivity().getResources().getColor(R.color.colorBTestMenu));
             TVdrivingPerformanceScore.setText("0");
             TVtotalBadBehaviors.setTextColor(getActivity().getResources().getColor(R.color.colorBTestMenu));
             TVtotalBadBehaviors.setText("0");
             imagePerformanceCar.setImageResource(R.drawable.icon_lastride_blue);

     }
    */
/**
     * This method allows to close menu after the choice of a Bluetooth
     *//*

    public static void closeD() {
        if (mDrawerLayout != null && mDrawerLayout.isShown()) {
            mDrawerLayout.closeDrawers();
        }
    }
    private boolean isNotificationManager() throws Exception
    {
        ContentResolver contentResolver = getContext().getContentResolver();
        String enabledNotificationListeners = Settings.Secure.getString(contentResolver, "enabled_notification_listeners");
        //Log.e("FD","enabledNotificationListeners: " + enabledNotificationListeners);
        String packageName = getContext().getPackageName();
        return enabledNotificationListeners != null && enabledNotificationListeners.contains(packageName);
    }

    */
/*@Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }*//*

  */
/*  private void setupActionBar() {
        ActionBar actionBar = getActivity().getSupportActionBar();

        if (actionBar != null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }*//*




           */
/* ************************  ********************* *//*

    */
/**
     * Handler for received Intents for the "com.freeedrive_saving_driving.sensor_event" event
     * It allows to get information after a ride, from FDSensor :
     *//*


    private void manageAllRides() {
        RideBDD tmp = new RideBDD(getContext());
        tmp.open();
        if (tmp.getLastRideDeptTime()>0L){
            tv_start_time.setText(""+AppUtils.formate10LongDateToDisplay(tmp.getLastRideDeptTime()));

        }
        if (tmp.getLastRideArrivalTime()>0L){
            tv_menu_arrival.setText(""+AppUtils.formate10LongDateToDisplay(tmp.getLastRideArrivalTime()));

        }
        Log.e("start_time",""+""+AppUtils.formate10LongDateToDisplay(tmp.getLastRideDeptTime()));
        Log.e("tv_menu_arrival",""+""+AppUtils.formate10LongDateToDisplay(tmp.getLastRideArrivalTime()));

        //the rides list :
        JSONArray listeRides = tmp.getAllRides(false);
        if (listeRides != null) {
            temptotaltime = tmp.totaltime();

            int totalAvergaeScore = 0;
            float sumSensors = 0;

            totalAvergaeScore = calculateAverage(listeRides, temptotaltime);
       */
/*     if(!DataHandler.getStringPreferences(AppConstants.SCORE_TOTAL_ELAPSED_TIME).isEmpty()) {
                String  elapsedserverTime = DataHandler.getStringPreferences(AppConstants.SCORE_TOTAL_ELAPSED_TIME).trim();
                int  intelapsedserverTime = Integer.valueOf(elapsedserverTime);
                temptotaltime = temptotaltime+intelapsedserverTime;
            }*//*

            String time_score_all_rides = "0";
            for (int i = 0; i < listeRides.length(); i++) {
                try {
                    JSONObject obj = listeRides.getJSONObject(i);
                    double score_double = obj.getDouble("score");//New Formula Score
                    //Log.e("DOUBLE",""+score_double);
                    float score_float = (float) score_double;
                    sumSensors += score_float;
                    totalbadbehaviour = obj.getInt("count_bad_behaviour");
                    Log.e("total_bad_behaviour",""+totalbadbehaviour);
                    //Log.i("Adneom","score double "+score_double+ " and score float "+score_float);
                   */
/* String time = obj.getString("time"); // LastRideTime
                    //Log.i("Adneom","(MenuActivity) time is "+time+" *** ");
                    String[] tab_time = time.split(":");
                    hours += Integer.parseInt(tab_time[0]);
                    minutes += Integer.parseInt(tab_time[1]);
                    secondes += Integer.parseInt(tab_time[2]);*//*

                    //Log.i("Adneom", " hour(s) is " + Integer.parseInt(tab_time[0]) + " minute(s) is " + Integer.parseInt(tab_time[1]) + " and seconde(s) is " + Integer.parseInt(tab_time[2])+" and score "+score_float);

                    //new avg:
                    //  totalScore += obj.getDouble("avg_time");
                    //totalScore2 += obj.getDouble("score");
                    //  totalTime += obj.getInt("time_total_secondes");
//                    Log.e("CHECK_TOTAL_TIME",""+totalTime);
                    double valueScore = obj.getDouble("score");
                    //   int valueTime = obj.getInt("time_total_secondes");
                    //  totalScore2 += (valueScore * valueTime);
                    //Log.i("Adneom","(MenuActivity) total score is ("+valueScore+","+valueTime+")"+(valueScore * valueTime));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (temptotaltime < 60) {
                int lessthenhour = (int) temptotaltime;
                String less_then_hour = lessthenhour + " min";
                //tv_all_ride_time.setText(less_then_hour);

            } else {
                Log.e("tempTotalTime", "" + temptotaltime);
                int hour = (int) (temptotaltime / 60);
                Log.e("hour", "" + hour);
                int mintue = (int) (temptotaltime - (hour * 60));
                Log.e("minute", "" + mintue);
                time_score_all_rides = hour + "h" +" "+ mintue + "min";
                // tv_all_ride_time.setText(time_score_all_rides);
            }
            int checktest = tmp.sumDb();
            int lastBadBehaviourCount =tmp.getLastRideBadBehaviourCount();
            if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)!=null && DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)==false){
                TVtotalBadBehaviors.setText("0");

            }else{
                TVtotalBadBehaviors.setText(""+lastBadBehaviourCount);

            }
            Log.e("checktest", "" + checktest);
       */
/*     if(!DataHandler.getStringPreferences(AppConstants.SCORE_TOTAL_BAD_COUNTS).isEmpty()){
                String  serverBadCounts =DataHandler.getStringPreferences(AppConstants.SCORE_TOTAL_BAD_COUNTS).trim();
                int intServerBadCounts = Integer.valueOf(serverBadCounts) ;
                checktest = checktest+intServerBadCounts;
            }*//*

            //tv_menu_total_distractions.setText("0");

            float newAVGAllRides = (float) (totalAvergaeScore);
            //max :
            if (newAVGAllRides > 100) {
                newAVGAllRides = 100;
            } else if (newAVGAllRides <= 0) {
                newAVGAllRides = 0;

            }
            int value_all_rides = (int) Math.round(newAVGAllRides);
            if (checktest==0){
                tv_menu_total_score.setText(""+100);
            }else{
                tv_menu_total_score.setText(String.valueOf(value_all_rides));
            }


//            btn_all_drive.setText(time_score_all_rides);

        }
        tmp.close();
    }

        */
/* ********************  ******************** *//*


    */
/**
     * This method allows to update the fit chart and driving time of All my rides.
     * It takes the rides from database, calculate the avg and driving time.
     *//*

    public int calculateAverage(JSONArray listRide, float totalTime) {
        double tempvaTotal_ = totalTime;
        double average = 0.0;
        for (int i = 0; i < listRide.length(); i++) {
            try {
                JSONObject obj = listRide.getJSONObject(i);
                double currentTime = Double.valueOf(obj.getInt("time_elapsed"));
                double currentScore = (obj.getDouble("score"));

                double divvar = currentTime / tempvaTotal_;

                average = average + currentScore * divvar;


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        //Math.round(average);
        int average_int = (int) average;
        Log.e("average", "" + average_int);

   */
/*     if (!DataHandler.getStringPreferences(AppConstants.SCORE_TOTAL_SCORE).isEmpty()){
            String serverAvg =DataHandler.getStringPreferences(AppConstants.SCORE_TOTAL_SCORE);
            int  intserverAvg = Integer.valueOf(serverAvg);
            int newAvg = (intserverAvg+average_int)/2;
            average_int =  Math.round(newAvg);;
        }*//*

        return average_int;
    }

    public void listenForBluetoothEnable() {

        Boolean isEnabled = false;
        BluetoothAdapter mBluetoothAdaptater = BluetoothAdapter.getDefaultAdapter();
        if(mBluetoothAdaptater != null && mBluetoothAdaptater.isEnabled()){
            isEnabled = true;
        }

        try {
            if (turnOnBTDialog !=null && turnOnBTDialog.isShowing()){
                turnOnBTDialog.dismiss();
                return;
            }else if (!isEnabled){
                turnOnBTDialog.show();
            }
        }catch (OutOfMemoryError  e){
            e.printStackTrace();
        }catch (Exception  e){
            e.printStackTrace();
        }

        //  closeD();


    }
    @Override
    public void onResume() {
        super.onResume();
 */
/*       getActivity().registerReceiver(mMessageReceiverSensor,new IntentFilter("com.freeedrive_saving_driving.sensor_event"));
//register to MessageReceiver to receive a message to activate GPS:
        getActivity().registerReceiver(mMessageReceiverGPSENABLE,new IntentFilter("com.freeedrive_saving_driving.enable_gps"));
        //register to MessageReceiver to receive a message to launch synchronization :
        getActivity().registerReceiver(mMessageReceiverBTenable,new IntentFilter("com.freeedrive.bluetooth_enable"));
        getActivity().registerReceiver(mMessageReceiverNotificationenable,new IntentFilter("com.freeedrive.notification_settings"));
        //register to MessageReceiver to receive a message to launch synchronization :
        getActivity().registerReceiver(mMessageReceiverSynchonization,new IntentFilter("com.freeedrive_saving_driving.synchronization"));

        getActivity().registerReceiver(mMessageReceiverDonotDistrub,new IntentFilter("android.settings.NOTIFICATION_POLICY_ACCESS_SETTINGS"));

        getActivity().registerReceiver(broadcastReceiverSpeeding,new IntentFilter("com.freeedrive_saving_driving.speeding_event"));*//*


        if (turnOnBTDialog.isShowing()) {
            return;
        }
        int accessPrivateHours = 0;
        accessPrivateHours = DataHandler.getIntPreferences(AppConstants.ACCESS_PRIVATE_HOURS);
         if(accessPrivateHours== -2){
              PrivateHoursDialog privateHoursDialog = new PrivateHoursDialog(getActivity());
              privateHoursDialog.show();
          }
        if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)!=null && DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)==false){
            TVtotalBadBehaviors.setText("0");

        }else{
            if (DataHandler.getIntPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_SCORE)>=0){
                TVdrivingPerformanceScore.setText(""+DataHandler.getIntPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_SCORE));
            }
        }
try {
    String strinJsonObject = DataHandler.getStringPreferences(AppUtils.SAFETY_SCORE_LAST_RIDE);
    Log.e("Safety_Score_Last_Ride", strinJsonObject);
    if (!strinJsonObject.equalsIgnoreCase("")) {
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(strinJsonObject);
            if (jsonObj != null) {
                String lastrideScore = String.valueOf(jsonObj.get("text"));

                if (!lastrideScore.isEmpty()) {
                    int LastRideTimeMins = Integer.valueOf(lastrideScore);


                    if (LastRideTimeMins < 60) {
                        int lessthenhour = (int) LastRideTimeMins;
                        String less_then_hour = lessthenhour + " mins";
                        tv_last_ride_time.setText(less_then_hour);

                    } else {
                        int hour = (int) (LastRideTimeMins / 60);
                        int mintue = (int) (LastRideTimeMins - (hour * 60));
                        String time_score_all_rides = hour + "h" + " " + mintue + "mins";
                        tv_last_ride_time.setText(time_score_all_rides);
                    }
                }

                //tv_last_ride_time.setText(lastrideScore);
                tv_no_of_distractions.setText(jsonObj.getString("badbehaviour"));
                ;//timeOflastRide
                //badCount
                double value_safety_score = jsonObj.getDouble("value_safetyscore");//avg
                float val = (float) value_safety_score;
                String badCount = "";
                badCount = jsonObj.getString("badbehaviour");
                int setBadCount = Integer.valueOf(badCount);


                if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)!=null && DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)==false){


                    if (Integer.valueOf(jsonObj.getString("badbehaviour").toString()) <= 1) {
                        tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_good));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            layout_behaviour.setBackground(getResources().getDrawable(R.drawable.blue_outline));
                        }
                        tv_menu_remarks.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_good));
                        imageView_thums.setImageResource(R.drawable.thumbsup);
                    }
                    if (Integer.valueOf(jsonObj.getString("badbehaviour").toString()) > 1 && Integer.valueOf(jsonObj.getString("badbehaviour").toString()) <= 4) {
                        tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_netural));
                        tv_menu_remarks.setTextColor(getResources().getColor(R.color.orange));
                        tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_netural));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            layout_behaviour.setBackground(getResources().getDrawable(R.drawable.orange_outline));
                        }
                        imageView_thums.setVisibility(View.INVISIBLE);
                    }
                    if (Integer.valueOf(jsonObj.getString("badbehaviour").toString()) >= 5) {
                        tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_bad));
                        tv_menu_remarks.setTextColor(getResources().getColor(R.color.red));
                        tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_bad));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            layout_behaviour.setBackground(getResources().getDrawable(R.drawable.red_outline));
                        }
                        imageView_thums.setVisibility(View.INVISIBLE);
                    }
                }
                else {
                    if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)!=null && DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)==true){
                        int score = DataHandler.getIntPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_SCORE);
                        if (score > 95 && setBadCount <= 1) {
                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_good));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.blue_outline));
                            }
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_good));
                            imageView_thums.setImageResource(R.drawable.thumbsup);

                        }
                        if (score > 95 && setBadCount >= 2 && setBadCount <= 4) {
                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_netural));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.orange));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_netural));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.orange_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);

                        }
                        if (score > 95 && setBadCount >= 5) {
                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_bad));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.red));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_bad));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.red_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);
                        }

                        if (score >= 75 && score <= 95 && setBadCount <= 1) {

                            tv_menu_remarks.setText(getResources().getString(R.string.menu_performance_remarks_header));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.blue_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);
                        }

                        if (score >= 75 && score <= 95 && setBadCount >= 2 && setBadCount <= 4) {

                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_netural));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.orange));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance_bad));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.orange_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);
                        }
                        if (score >= 75 && score <= 95 && setBadCount >= 5) {

                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_bad));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.red));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance_worst));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.red_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);
                        }

                        // when the driving performance is <75 %

                        if (score < 75 && setBadCount <= 1) {

                            tv_menu_remarks.setText(getResources().getString(R.string.menu_performance_remarks_header));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance_bad_2));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.blue_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);
                        }

                        if (score < 75 && setBadCount >= 2 && setBadCount <= 4) {

                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_netural));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.orange));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance_bad_3));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.orange_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);
                        }
                        if (score < 75 && setBadCount >= 5) {

                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_dangerous));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.red));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance_worst_1));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.red_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);
                        }

                    }
                }


                String lapsedLastRideTime = "";
                lapsedLastRideTime = jsonObj.getString("text").trim();
                Log.e("lapsedLastRideTime","Trim :" +lapsedLastRideTime);
                if (!lapsedLastRideTime.isEmpty()) {
                    int lapsedLastRideTimeMins = Integer.valueOf(lapsedLastRideTime);


                    if (lapsedLastRideTimeMins < 60) {
                        int lessthenhour = (int) lapsedLastRideTimeMins;
                        String less_then_hour = lessthenhour + " mins";
                        tv_last_ride_time.setText(less_then_hour);

                    } else {
                        int hour = (int) (lapsedLastRideTimeMins / 60);
                        int mintue = (int) (lapsedLastRideTimeMins - (hour * 60));
                        String time_score_all_rides = hour + "h" + " " + mintue + "mins";
                        tv_last_ride_time.setText(time_score_all_rides);
                    }
                }


                //   tv_last_ride_time.setText(""+jsonObj.getString("text"));
                String badCount1 = "";
                badCount = jsonObj.getString("badbehaviour");
                int setBadCount1 = Integer.valueOf(badCount);

             */
/*       String formattedDate= AppUtils.formate10LongDateToDisplay(Detector.FdApp.arrival_time);
                   if (!DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)) {
                       if (DataHandler.getStringPreferences(AppConstants.KEY_ARRIVAL) != null) {
                           tv_menu_arrival.setText(DataHandler.getStringPreferences(AppConstants.KEY_ARRIVAL));
                           // tv_menu_arrival.setText(formattedDate);
                       }
                       if (DataHandler.getStringPreferences(AppConstants.KEY_DEPARTURE) != null) {
                           tv_start_time.setText(DataHandler.getStringPreferences(AppConstants.KEY_DEPARTURE));
                           // tv_menu_arrival.setText(formattedDate);
                       }
                   }*//*


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    } else {
        if (!DataHandler.getStringPreferences(AppConstants.TEMP_DISPLAY_KEY).isEmpty()) {
            try {
                String dispValue = DataHandler.getStringPreferences(AppConstants.TEMP_DISPLAY_KEY);
                JSONObject jsonObj = new JSONObject(dispValue);
                if (jsonObj != null) {
                    String lapsedLastRideTime = jsonObj.getString("last_time_elapsed").trim();
                    if (!lapsedLastRideTime.isEmpty()) {
                        int lapsedLastRideTimeMins = Integer.valueOf(lapsedLastRideTime);


                        if (lapsedLastRideTimeMins < 60) {
                            int lessthenhour = (int) lapsedLastRideTimeMins;
                            String less_then_hour = lessthenhour + " mins";
                            tv_last_ride_time.setText(less_then_hour);

                        } else {
                            int hour = (int) (lapsedLastRideTimeMins / 60);
                            int mintue = (int) (lapsedLastRideTimeMins - (hour * 60));
                            String time_score_all_rides = hour + "h" + " " + mintue + "mins";
                            tv_last_ride_time.setText(time_score_all_rides);
                        }
                    }

                    String lastRidearrivalTime = jsonObj.getString("last_arrival_time").trim();
                    if (!lastRidearrivalTime.isEmpty()) {
                        Long longlastRidearrivalTime = Long.valueOf(lastRidearrivalTime);
                        String displastRidearrivalTime = AppUtils.formate10LongDateToDisplay(longlastRidearrivalTime);
                        tv_menu_arrival.setText(displastRidearrivalTime);
                    }
                    String lastRideadepartureTime = jsonObj.getString("last_departure_time").trim();
                    if (!lastRideadepartureTime.isEmpty()) {
                        Long lastRideadepartureTimeL = Long.valueOf(lastRideadepartureTime);
                        String displastRideaDepartureTime = AppUtils.formate10LongDateToDisplay(lastRideadepartureTimeL);
                        tv_start_time.setText(displastRideaDepartureTime);
                    }
                    String stringLastRideBadCount = jsonObj.getString("last_count_bad_behaviour").trim();
                    int setBadCount = Integer.valueOf(jsonObj.getString("last_count_bad_behaviour"));

                    if (!stringLastRideBadCount.isEmpty()) {

                        tv_no_of_distractions.setText(stringLastRideBadCount);
                        if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)!=null && DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)==false){

                            if (Integer.valueOf(jsonObj.getString("last_count_bad_behaviour").toString()) == 0 || Integer.valueOf(jsonObj.getString("last_count_bad_behaviour").toString()) == 1) {
                                tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_good));
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                    layout_behaviour.setBackground(getResources().getDrawable(R.drawable.blue_outline));
                                }
                                tv_menu_remarks.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_good));
                                imageView_thums.setImageResource(R.drawable.thumbsup);
                            }
                            if (Integer.valueOf(jsonObj.getString("last_count_bad_behaviour").toString()) > 1 && Integer.valueOf(jsonObj.getString("last_count_bad_behaviour").toString()) <= 4) {
                                tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_netural));
                                tv_menu_remarks.setTextColor(getResources().getColor(R.color.orange));
                                tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_netural));
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                    layout_behaviour.setBackground(getResources().getDrawable(R.drawable.orange_outline));
                                }
                                imageView_thums.setVisibility(View.INVISIBLE);
                            }
                            if (Integer.valueOf(jsonObj.getString("last_count_bad_behaviour").toString()) >= 5) {
                                tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_bad));
                                tv_menu_remarks.setTextColor(getResources().getColor(R.color.red));
                                tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_phonesaftey_bad));
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                    layout_behaviour.setBackground(getResources().getDrawable(R.drawable.red_outline));
                                }
                                imageView_thums.setVisibility(View.INVISIBLE);
                            }
                        }
                        else {
                            if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)!=null && DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)==true){
                                int score = DataHandler.getIntPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_SCORE);
                                if (score >= 95 && setBadCount <= 1) {
                                    tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_good));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        layout_behaviour.setBackground(getResources().getDrawable(R.drawable.blue_outline));
                                    }
                                    tv_menu_remarks.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                    tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_good));
                                    imageView_thums.setImageResource(R.drawable.thumbsup);

                                }
                                if (score >= 95 && setBadCount >= 2 && setBadCount <= 4) {
                                    tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_netural));
                                    tv_menu_remarks.setTextColor(getResources().getColor(R.color.orange));
                                    tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_netural));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        layout_behaviour.setBackground(getResources().getDrawable(R.drawable.orange_outline));
                                    }
                                    imageView_thums.setVisibility(View.INVISIBLE);

                                }
                                if (score >= 95 && setBadCount >= 5) {
                                    tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_bad));
                                    tv_menu_remarks.setTextColor(getResources().getColor(R.color.red));
                                    tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_bad));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        layout_behaviour.setBackground(getResources().getDrawable(R.drawable.red_outline));
                                    }
                                    imageView_thums.setVisibility(View.INVISIBLE);
                                }

                                if (score >= 75 && score <= 95 &&  setBadCount <= 1) {

                                    tv_menu_remarks.setText(getResources().getString(R.string.menu_performance_remarks_header));
                                    tv_menu_remarks.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                    tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        layout_behaviour.setBackground(getResources().getDrawable(R.drawable.blue_outline));
                                    }
                                    imageView_thums.setVisibility(View.INVISIBLE);
                                }

                                if (score >= 75 && score < 95 && setBadCount >= 2 && setBadCount <= 4) {

                                    tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_netural));
                                    tv_menu_remarks.setTextColor(getResources().getColor(R.color.orange));
                                    tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance_bad));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        layout_behaviour.setBackground(getResources().getDrawable(R.drawable.orange_outline));
                                    }
                                    imageView_thums.setVisibility(View.INVISIBLE);
                                }
                                if (score >= 75 && score < 95 && setBadCount >= 5) {

                                    tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_bad));
                                    tv_menu_remarks.setTextColor(getResources().getColor(R.color.red));
                                    tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance_worst));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        layout_behaviour.setBackground(getResources().getDrawable(R.drawable.red_outline));
                                    }
                                    imageView_thums.setVisibility(View.INVISIBLE);
                                }

                                // when the driving performance is <75 %

                                if (score < 75 && setBadCount <= 1) {

                                    tv_menu_remarks.setText(getResources().getString(R.string.menu_performance_remarks_header));
                                    tv_menu_remarks.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                    tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance_bad_2));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        layout_behaviour.setBackground(getResources().getDrawable(R.drawable.blue_outline));
                                    }
                                    imageView_thums.setVisibility(View.INVISIBLE);
                                }

                                if (score < 75 && setBadCount >= 2 && setBadCount <= 4) {

                                    tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_netural));
                                    tv_menu_remarks.setTextColor(getResources().getColor(R.color.orange));
                                    tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance_bad_3));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        layout_behaviour.setBackground(getResources().getDrawable(R.drawable.orange_outline));
                                    }
                                    imageView_thums.setVisibility(View.INVISIBLE);
                                }
                                if (score < 75 && setBadCount >= 5) {

                                    tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_bad));
                                    tv_menu_remarks.setTextColor(getResources().getColor(R.color.red));
                                    tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance_worst_1));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        layout_behaviour.setBackground(getResources().getDrawable(R.drawable.red_outline));
                                    }
                                    imageView_thums.setVisibility(View.INVISIBLE);
                                }

                            }
                        }
                    }
                    int last_avg_performance = jsonObj.getInt("avg_performance");


                    if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)!=null && DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)==false){
                        TVdrivingPerformanceScore.setText("0");

                    } else {
                        if (last_avg_performance >= 0) {
                            TVdrivingPerformanceScore.setText("" + last_avg_performance);
                        }
                    }

                    String total_time_elapsed = jsonObj.getString("total_time_elapsed").toString().trim();
                    if (!total_time_elapsed.isEmpty()) {

                        int inttotal_time_elapsed = Integer.valueOf(total_time_elapsed);
                        DataHandler.updatePreferences(AppConstants.SCORE_TOTAL_ELAPSED_TIME, total_time_elapsed);
                        if (inttotal_time_elapsed < 60) {
                            int lessthenhour = (int) inttotal_time_elapsed;
                            String less_then_hour = lessthenhour + " mins";
                            //tv_all_ride_time.setText(less_then_hour);

                        } else {
                            int hour = (int) (inttotal_time_elapsed / 60);
                            int mintue = (int) (inttotal_time_elapsed - (hour * 60));
                            String time_score_all_rides = hour + "h" + " " + mintue + "mins";
                            //tv_all_ride_time.setText(time_score_all_rides);
                        }
                    }
                    String stringTotalavgScore = jsonObj.getString("avg_score").toString().trim();
                    if (!stringTotalavgScore.isEmpty()) {

                        Float dblTotalAvgScore = Float.valueOf(stringTotalavgScore);
                        float newAVGAllRides = (float) (dblTotalAvgScore);
                        //max :
                        if (newAVGAllRides > 100) {
                            newAVGAllRides = 100;
                        } else if (newAVGAllRides <= 0) {
                            newAVGAllRides = 0;

                        }
                        int value_all_rides = (int) Math.round(newAVGAllRides);
                        if (stringLastRideBadCount.equalsIgnoreCase("0")) {
                            tv_menu_total_score.setText("" + 100);
                        } else {
                            tv_menu_total_score.setText(String.valueOf(value_all_rides));
                        }
                        String stringvalue_all_rides = String.valueOf(value_all_rides);
                        DataHandler.updatePreferences(AppConstants.SCORE_TOTAL_SCORE, stringvalue_all_rides);
                    }
                    String stringTotalBadCounts = jsonObj.getString("total_bad_counts").toString().trim();
                    if (!stringTotalBadCounts.isEmpty()) {

                        int intTotalBadCount = Integer.valueOf(stringTotalBadCounts);
                        // tv_menu_total_distractions.setText("");

                        DataHandler.updatePreferences(AppConstants.SCORE_TOTAL_BAD_COUNTS, stringTotalBadCounts);
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}catch (OutOfMemoryError error){

}catch (Exception error){

}
        manageAllRides();
        if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)!=null && DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)==false){
            LockDrivingPerformance();
        }
    }

    */
/**
     * Handler for received Intents for the "com.freeedrive_saving_driving.synchronization" event
     * It allows to launch sync with the new list with the localities  :
     *//*

    private BroadcastReceiver mMessageReceiverDonotDistrub = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            try{
                if (turnOnDonotDisturbDialog !=null && turnOnDonotDisturbDialog.isShowing()){
                    turnOnDonotDisturbDialog.dismiss();
                    return;
                }else{
                    turnOnDonotDisturbDialog.show();
                }
            }catch (Exception e){

            }


        }
    };


    */
/**
     * Handler for received Intents for the "com.freeedrive_saving_driving.synchronization" event
     * It allows to launch sync with the new list with the localities  :
     *//*

    private BroadcastReceiver mMessageReceiverSynchonization = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            Intent i = new Intent(getActivity(),ScoreSynchronizationActivity.class);
            startActivity(i);
        }
    };

    private BroadcastReceiver broadcastReceiverSpeeding = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)!=null && DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)==false){
                LockDrivingPerformance();

            }else if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)!=null && DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)==true){
                  UnLockDrivingPerformance();
                if (DataHandler.getIntPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_SCORE)>=0){
                    TVdrivingPerformanceScore.setText(""+DataHandler.getIntPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_SCORE));
                }
                RideBDD tmp = new RideBDD(getContext());
                tmp.open();
                int lastBadBehaviourCount =tmp.getLastRideBadBehaviourCount();
                TVtotalBadBehaviors.setText(""+lastBadBehaviourCount);
                tmp.close();
            }
        }
    };



    private BroadcastReceiver mMessageReceiverSensor = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            int time = intent.getIntExtra("time", 0);
            String text = intent.getStringExtra("text");
            float value_safety_score = intent.getFloatExtra("value_safetyscore", 0);
            int new_value_safetyscore = Math.round(value_safety_score);
            int text_safety_score = intent.getIntExtra("text_safetyscore", 0);
            int bdlastride = intent.getIntExtra("badbehaviour", 0);
            //Log.i("Adneom", "--- Got message is " + message+" and time from receiver is "+time+" and texte from receiver is "+text+" value of safety score is "+value_safety_score+" and text of safety score is "+text_safety_score+" --- ");
            if (text != null && !text.equals("0") && value_safety_score >= 0 && text_safety_score >= 0) {
                //btn_last_drive.setText(text);//Time of lastRide
                // score_last_ride.setText(String.valueOf(text_safety_score));
                //fitChartLastRide.setValue(value_safety_score);
                //fitChartLastRide.setValue(new_value_safetyscore);
                // bad_behaviour_last_ride.setText(Integer.toString(bdlastride));

                tv_no_of_distractions.setText(""+bdlastride);
                //tv_last_ride_time.setText(""+text);

                String lapsedLastRideTime = "";
                lapsedLastRideTime= text.trim();
                if (!lapsedLastRideTime.isEmpty()) {
                    int lapsedLastRideTimeMins = Integer.valueOf(lapsedLastRideTime);


                    if (lapsedLastRideTimeMins < 60) {
                        int lessthenhour = (int) lapsedLastRideTimeMins;
                        String less_then_hour = lessthenhour + " mins";
                        tv_last_ride_time.setText(less_then_hour);

                    } else {
                        int hour = (int) (lapsedLastRideTimeMins / 60);
                        int mintue = (int) (lapsedLastRideTimeMins - (hour * 60));
                        String time_score_all_rides = hour + "h" + " " + mintue + "mins";
                        tv_last_ride_time.setText(time_score_all_rides);
                    }
                }

                String formattedDate= AppUtils.formate10LongDateToDisplay(Detector.FdApp.arrival_time);
                if(DataHandler.getStringPreferences(AppConstants.KEY_ARRIVAL)!=null){
                    tv_menu_arrival.setText(DataHandler.getStringPreferences(AppConstants.KEY_ARRIVAL));
                    // tv_menu_arrival.setText(formattedDate);
                }
                if(DataHandler.getStringPreferences(AppConstants.KEY_DEPARTURE)!=null){
                    tv_start_time.setText(DataHandler.getStringPreferences(AppConstants.KEY_DEPARTURE));
                    // tv_menu_arrival.setText(formattedDate);
                }
                //TODO

                if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)!=null && DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)==false) {
                    {
                        if (bdlastride == 0 || bdlastride == 1) {
                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_good));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.blue_outline));
                            }
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_good));
                            imageView_thums.setImageResource(R.drawable.thumbsup);
                        }
                        if (bdlastride > 1 && bdlastride <= 4) {
                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_netural));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.orange));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_netural));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.orange_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);
                        }
                        if (bdlastride >= 5) {
                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_bad));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.red));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_phonesaftey_bad));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.red_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);
                        }

                        manageAllRides();

                    }
                }
                else{
                    if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)!=null && DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)==true){
                        int score = DataHandler.getIntPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_SCORE);
                        if (score > 95 && bdlastride <= 1) {
                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_good));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.blue_outline));
                            }
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_good));
                            imageView_thums.setImageResource(R.drawable.thumbsup);

                        }
                        if (score > 95 && bdlastride >= 2 && bdlastride <= 4) {
                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_netural));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.orange));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_netural));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.orange_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);

                        }
                        if (score > 95 && bdlastride >= 5) {
                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_bad));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.red));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_bad));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.red_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);
                        }

                        if (score >= 75 && score <= 95 && bdlastride <= 1) {

                            tv_menu_remarks.setText(getResources().getString(R.string.menu_performance_remarks_header));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.blue_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);
                        }

                        if (score >= 75 && score <= 95 && bdlastride >= 2 && bdlastride <= 4) {

                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_netural));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.orange));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance_bad));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.orange_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);
                        }
                        if (score >= 75 && score <= 95 && bdlastride >= 5) {

                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_bad));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.red));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance_worst));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.red_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);
                        }

                        // when the driving performance is <75 %

                        if (score < 75 && bdlastride <= 1) {

                            tv_menu_remarks.setText(getResources().getString(R.string.menu_performance_remarks_header));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance_bad_2));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.blue_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);
                        }

                        if (score < 75 && bdlastride >= 2 && bdlastride <= 4) {

                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_netural));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.orange));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance_bad_3));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.orange_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);
                        }
                        if (score < 75 && bdlastride >= 5) {

                            tv_menu_remarks.setText(getResources().getString(R.string.menu_remarks_dangerous));
                            tv_menu_remarks.setTextColor(getResources().getColor(R.color.red));
                            tv_menu_remarks_desc.setText(getResources().getString(R.string.remarks_desc_driving_performance_worst_1));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                layout_behaviour.setBackground(getResources().getDrawable(R.drawable.red_outline));
                            }
                            imageView_thums.setVisibility(View.INVISIBLE);
                        }

                    }
                
                }
            }
            else {
                //if update in database (re calculate ):
                manageAllRides();
            }
        }
    };
    private BroadcastReceiver mMessageReceiverGPSENABLE = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                if (turOnGpsDialog!=null&&turOnGpsDialog.isShowing()){
                    turOnGpsDialog.dismiss();
                    return;
                }else{
                    turOnGpsDialog.show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }catch (OutOfMemoryError e){
                e.printStackTrace();
            }


        }
    };
    private BroadcastReceiver mMessageReceiverNotificationenable = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (turnOnNotifcationDialog !=null && turnOnNotifcationDialog.isShowing()){
                    turnOnNotifcationDialog.dismiss();
                    return;
                }else {
                    turnOnNotifcationDialog.show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }catch (OutOfMemoryError e){
                e.printStackTrace();
            }
        }
    };
    private BroadcastReceiver mMessageReceiverBTenable = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try{
                listenForBluetoothEnable();
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    };

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onDestroy() {

        super.onDestroy();
        // Unregister broadcast BT listeners
        try{
            if(mMessageReceiverBTenable !=null)
                getActivity().unregisterReceiver(mMessageReceiverBTenable);
            mMessageReceiverBTenable = null;

            if(mMessageReceiverDonotDistrub !=null)
                getActivity().unregisterReceiver(mMessageReceiverDonotDistrub);
            mMessageReceiverDonotDistrub = null;

            if(mMessageReceiverGPSENABLE !=null)
                getActivity().unregisterReceiver(mMessageReceiverGPSENABLE);
            mMessageReceiverGPSENABLE = null;

            if(mMessageReceiverNotificationenable !=null)
                getActivity().unregisterReceiver(mMessageReceiverNotificationenable);
            mMessageReceiverNotificationenable = null;

            if(mMessageReceiverSensor !=null)
                getActivity().unregisterReceiver(mMessageReceiverSensor);
            mMessageReceiverSensor = null;
            if(broadcastReceiverSpeeding !=null)
                getActivity().unregisterReceiver(broadcastReceiverSpeeding);
            broadcastReceiverSpeeding = null;
            if(mMessageReceiverSynchonization !=null)
                getActivity().unregisterReceiver(mMessageReceiverSynchonization);
            mMessageReceiverSynchonization = null;
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }


    }


}
*/
