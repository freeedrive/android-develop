package com.studio.barefoot.freeedrivebeacononlyapp.recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.ScoreSynchronizationActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.FetchDrivingPerformanceScores;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.UnReadMessagesAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LoggingOperations;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.formateLongToOnlyDateForServer;

//import com.freeedrive.R;

/**
 * This class allows to execute a synchronization
 * Created by gtshilombowanticale on 11-08-16.
 */
public class NetworkBroadcastReceiver extends BroadcastReceiver {

    /**
     * Represents the ride
     */
    RideBDD BDtmp;
    public  Timer drivingPerformanceCallTimer;

    /**
     * BoradcastReceiver method basic, used to detect a connection and to execute a synchronization
     *
     * @param context(in), @Activity represenst the activity
     * @param intent(in),  @Intent the represents the inten
     */
    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
                Long currentTime =  System.currentTimeMillis();
                String timea=  formateLongToOnlyDateForServer(currentTime);
                LoggingOperations.writeToFile(context,"<NetworkBroadCastReceiver> ----> NETWORK_RECEIVED >"+ timea + "I GOT INTERNET YEA");
                 Log.e("NETWORK_RECEIVED","I GOT INTERNET YEAAA");
                if (!AppUtils.isUpdated) {

                    try{
                        if (AppUtils.isNetworkAvailable() ){
                            if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY)){
                                AppUtils.rideSync(context);
                                List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                                mParams.add(new BasicNameValuePair("phone_number",DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
/*
                                UnReadMessagesAsyncTask unReadMessagesAsyncTask = new UnReadMessagesAsyncTask(context,WebServiceConstants.END_POINT_UNREAD_SMS_COUNT,mParams);
                                unReadMessagesAsyncTask.execute();*/
                            }



                        }

                /*        Intent i = new Intent(context.getApplicationContext(), ScoreSynchronizationActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);*/
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                  /*  differenceTimes(context);*/
                    //isUpdated = true;
                } else {
                    AppUtils.isUpdated = false;
                }
                Log.d("Network", "Internet YAY");
            } else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED) {
                Log.d("Network", "No internet :(");
            }
        }
    }


    /**
     * Allows to calculate the difference between the current time and the parameter
     *
     * @param ct(in), @Activity represents the activity
     */
    public void differenceTimes(Context ct) {
        //Allows to execute request if the listes are not empty :
        Boolean canExecuteAsynctaslk = true;

        AppUtils.isUpdated = true;
        if (DataHandler.getLongreferences(AppUtils.LAST_UPDATE) != null) {

            BDtmp = new RideBDD(ct);
            if (DataHandler.getLongreferences(AppUtils.LAST_UPDATE) > 0) {

                    //Log.i("Adneom","(WifiBroadcastReceiver)  prefs is not null and last update is > 0 ");
                    Boolean canSynchronize = false;
                    long lastUpdateTime = DataHandler.getLongreferences(AppUtils.LAST_UPDATE);
                    Date currentDate = new Date();
                    Date lastUpdate = new Date(lastUpdateTime);
                    DateFormat df = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");
                    String str = df.format(lastUpdate);
                    String strCurrentDate = df.format(currentDate);
                    //Log.i("Adneom","(WifiBroadcastReceiver) last update : "+str+" and current date is "+strCurrentDate);
                    String[] strLastUpdate = str.split(":");
                    String[] strCurrent = strCurrentDate.split(":");
                    String[] dateLastUpdateTab = strLastUpdate[0].split("/");
                    String[] dateCurrentTab = strCurrent[0].split("/");

                    int hourLastUpdate = Integer.parseInt(strLastUpdate[1]);
                    int hourCurrent = Integer.parseInt(strCurrent[1]);
                    int hoursDifference;

                    //Log.i("Adneom","(WifiBroadcastReceiver) last update is "+str+" and the current date is "+strCurrentDate+" and the difference between days is "+(Integer.parseInt(dateCurrentTab[0]) - Integer.parseInt(dateLastUpdateTab[0])));
                    // condition on the days
                    int dayCurrentDate = Integer.parseInt(dateCurrentTab[0]), dayLastUpdate = Integer.parseInt(dateLastUpdateTab[0]);
                    if (dayCurrentDate > dayLastUpdate) {
                        //difference between 24 and lastUpdate's hour:
                        int diffHoursFromLastUpdate = (24 - hourLastUpdate);
                        //sum between current date's date and diffHoursFromLastUpdate:
                        int totalHours = (diffHoursFromLastUpdate + hourCurrent);
                        //condition >= 11 hours OR diffHoursFromLastUpdate > 11 hours :
                        if (totalHours >= AppUtils.SYNCHRONIZATION_INTERVAL || diffHoursFromLastUpdate >= AppUtils.SYNCHRONIZATION_INTERVAL) {
                            //Log.i("Adneom","(WifiBroadcastReceiver)  condition if two days are different, last update is "+str+" and current date is "+str2);
                            canSynchronize = true;
                        }
                    } else {
                        hoursDifference = (hourCurrent - hourLastUpdate);
                        if (hoursDifference >= AppUtils.SYNCHRONIZATION_INTERVAL) {
                            //Log.i("Adneom","(WifiBroadcastReceiver)  condition if two days are equals, last update is "+str+" and current date is "+str2);
                            canSynchronize = true;
                        }
                    }

                    //synchronization >= 11h OR if difference between days > 1
                    if (canSynchronize || (Integer.parseInt(dateCurrentTab[0]) - Integer.parseInt(dateLastUpdateTab[0])) > 1) {
                        //update lastUpdate :
                        DataHandler.updatePreferences(AppUtils.LAST_UPDATE,currentDate.getTime());

                        //db:
                        BDtmp.open();
                        JSONArray listeRides = BDtmp.getAllRides(true);//BDtmp.getAllRidesNotUpdated();
                        JSONArray newListeRides = null;

                        if (listeRides == null || listeRides.length() == 0) {
                            //Log.i("Adneom","(WifiBroadcastReceiver) list is "+listeRides);
                            canExecuteAsynctaslk = false;
                        } else {
                            //keep just good rides :
                            newListeRides = containsCoordinates(listeRides);
                            if (newListeRides == null || newListeRides.length() == 0) {
                                canExecuteAsynctaslk = false;
                            }
                        }

                        if (canExecuteAsynctaslk) {

                            RideBDD rideBDD2 = new RideBDD(ct);
                            rideBDD2.open();
                            rideBDD2.getAllRides(true);
                            rideBDD2.close();
                        }
                    }
                }
            }
        }

    /**
     * Test if a ride in the list contains a coordinate et no a locality,
     * if yes we delete the list, we send just the ride with locality
     * @param liste,@JSONArray is a list of rides
     * @return @JSONArray, the new liste that contains only tjhe rides with their locality
     */
    public static JSONArray containsCoordinates(JSONArray liste){
        JSONArray newListe = null;
        if(liste != null && liste.length() > 0){
            //Log.i("Adneom","current liste is "+liste.toString());
            newListe = new JSONArray();
            for(int i = 0; i < liste.length();i++){
                JSONObject jsonObject = null;
                //Log.i("Adneom","liste is "+liste);
                try {
                    jsonObject = liste.getJSONObject(i);
                    if(jsonObject != null && jsonObject.has("departure_location") && jsonObject.has("arrival_location")){
                        Boolean depConatainsCoord = false;
                        Boolean arrConatainsCoord = false;
                        String departure = jsonObject.getString("departure_location");
                        String arrival = jsonObject.getString("arrival_location");

                        //not null and if not empty or contains "," : true
                        if( departure != null && (departure.equals("") || departure.contains(",")) ){
                            depConatainsCoord = true;
                        }
                        if( arrival != null && (arrival.equals("") || arrival.contains(",")) ){
                            arrConatainsCoord = true;
                        }
                        //if departure and arrival doesn't contain "," : add
                        if(!depConatainsCoord && !arrConatainsCoord){
                            newListe.put(jsonObject);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        //Log.i("Adneom"," new liste is "+newListe.toString());
        return newListe;
    }
    private void PerformanceTimer() {
        drivingPerformanceCallTimer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Log.e("Inside","PerformanceTimer");
                List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                mParams.add(new BasicNameValuePair("phone_number",DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
                FetchDrivingPerformanceScores fetchDrivingPerformanceScores = new FetchDrivingPerformanceScores(ApplicationController.getmAppcontext(), WebServiceConstants.END_POINT_SPEEDING_SCORE,mParams);
                fetchDrivingPerformanceScores.execute();
            }
        };
        drivingPerformanceCallTimer.schedule(timerTask,60000);
    }
}
