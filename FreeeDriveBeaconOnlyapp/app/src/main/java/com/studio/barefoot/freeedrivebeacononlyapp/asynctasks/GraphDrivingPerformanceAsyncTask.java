package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.FleetSafetyGraph.progressBarDialogNotifications;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.fdLogout;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler.FILE_NAME_SHARED_PREF;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler.deletePreference;


/**
 * Created by yasir on 22/05/2017.
 */

public class GraphDrivingPerformanceAsyncTask extends BaseAsyncTask {

    public JSONArray driveravgArray= null;
    public JSONArray companyavgArray = null;

    public GraphDrivingPerformanceAsyncTask(Context context, String route, List<NameValuePair> pp) {
        super(context, route, pp);
    }


    /**
     * AsyncTask method basic calls during a request, calls the parent's method.
     */
    protected String doInBackground(String... params) {
        return "";
    }


    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if(s != null) {
            int intResponse = Integer.parseInt(response);
            Log.e("ResponseCode", "" + intResponse);
            Log.e("response", "" + response);
            Log.e("resultat", "" + resultat);

           /* try{
                progressBarDialogProfile.dismiss();
                progressBarDialogProfile = null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }*/

            try{
                progressBarDialogNotifications.dismiss();
                progressBarDialogNotifications = null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }
            JSONObject objAccount = null;

            switch (intResponse) {
                case 200:
                    try{
                        if (resultat!=null || !resultat.isEmpty()) {
                            objAccount = new JSONObject(resultat);
                            //Log.e("objAccount"," : "+objAccount);

                            if(objAccount !=null){
                                driveravgArray = objAccount.getJSONArray("driverAvg");
                                companyavgArray = objAccount.getJSONArray("companyAvg");

                            }
                            try{
                                progressBarDialogNotifications.dismiss();
                                progressBarDialogNotifications = null;
                            }catch (NullPointerException exception){
                                exception.printStackTrace();
                            }
                        }
                        try{

                            //updateGraphFromServer(driveravgArray,companyavgArray);
                            //generateDataLine(driveravgArray,companyavgArray);
                            DataHandler.updatePreferences(AppConstants.GRAPH_DRIVING_PERFORMANCE_AVG,objAccount.toString());
                        }catch (Exception exception){
                            exception.printStackTrace();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("JSONExceptionInsurance",""+e);
                    }
                    break;
                case 401:
                    //Phone numbre not found
                    AlertDialog.Builder error_401 = new AlertDialog.Builder(context);
                    error_401.setMessage(context.getResources().getString(R.string.error_changePhone_401)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_401.show();
                    break;
                case 400:
                    //Phone numbre not found
                    AlertDialog.Builder error_400 = new AlertDialog.Builder(context);
                    error_400.setMessage(context.getResources().getString(R.string.error_auth_token_expire_400)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deletePreference(FILE_NAME_SHARED_PREF);
                            fdLogout(context);
                            DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY,false);
                            dialog.dismiss();
                        }
                    });
                    error_400.show();
                   break;
                case 500:
                    AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                    error_500.setMessage(context.getResources().getString(R.string.error_login_404)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_500.show();
                    try{
                        progressBarDialogNotifications.dismiss();
                        progressBarDialogNotifications = null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;
            }
        }else {
            try{
                progressBarDialogNotifications.dismiss();
                progressBarDialogNotifications = null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }
        }
    }
}
