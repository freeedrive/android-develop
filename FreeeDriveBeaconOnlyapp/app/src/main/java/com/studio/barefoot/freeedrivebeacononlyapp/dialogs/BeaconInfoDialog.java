package com.studio.barefoot.freeedrivebeacononlyapp.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;

/**
 * Created by macbookpro on 8/24/17.
 */

public class BeaconInfoDialog extends DialogFragment {
     ImageView cancel_dialog;
     Button btn_requestBeacon;
    public BeaconInfoDialog() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static BeaconInfoDialog newInstance(String title) {
        BeaconInfoDialog frag = new BeaconInfoDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.qrcode_info_layout, container);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cancel_dialog = (ImageView) view.findViewById(R.id.cancel_dialog);
        btn_requestBeacon = (Button) view.findViewById(R.id.btn_request_beacon);
        cancel_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        // Get field from view
/*
        mEditText = (EditText) view.findViewById(R.id.txt_your_name);
*/
        // Fetch arguments from bundle and set title
/*
        String title = getArguments().getString("title", "Enter Name");
*/
        // Show soft keyboard automatically and request focus to field
  /*      mEditText.requestFocus();*/

    }
}
