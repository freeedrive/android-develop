package com.studio.barefoot.freeedrivebeacononlyapp.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.FleetStatisticsAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.GetFleetStatisticsAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.FleetStatsBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;
import com.txusballesteros.widgets.FitChart;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MySafetyFragmentGraph.DemoModeDialog;


public class FleetStatisticsFragments extends Fragment {

    public static RecyclerView listViewFleetStatistics;
    private ArrayList<FleetStatsBeans> fleetBeansArrayList = new ArrayList<>();
    public static  FleetStatisticsAdapter fleetStatisticsAdapter;
    LinearLayoutManager mLayoutManager;
    View fleetviewStatistics;
    Fragment fragment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    public static ProgressBarDialog progressBarDialogFleetStats;
    TextView fleet_week_picker;
    public static TextView tv_nofleetdata,fleet_name,total_fleet_avgscore,fleet_percentage;
    ImageView fleet_weekly_navigationLeft,fleet_weekly_navigationRight,fleet_stats_arrowRight,fleet_stats_arrowLeft;
    public static FitChart fleetfitChart;
    int currentWeek = 0;
    int year = 0;
    int weekNo = 0;
    int counter = 0;
    Calendar calender;
    String preDate,endDate;
    public static  ArrayList<FleetStatsBeans> statsBeansArrayList = new ArrayList<>();
    public FleetStatisticsFragments() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fleetviewStatistics = inflater.inflate(R.layout.fragment_fleet_statistics, container, false);

        // For setting up the different logo of toll bar for demo and paid mode
        RelativeLayout toolbar = (RelativeLayout)fleetviewStatistics.findViewById(R.id.navbar);
        ImageView fd_logo = (ImageView)toolbar.findViewById(R.id.tol_bar_logo);
        if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)){
            fd_logo.setImageResource(R.drawable.logo_freeedrive_demo_mode);
        }  else{
            fd_logo.setImageResource(R.drawable.logo_topbar);
        }
        fleet_week_picker = (TextView) fleetviewStatistics.findViewById(R.id.fleet_stats_week_picker);
        fleet_weekly_navigationLeft = (ImageView) fleetviewStatistics.findViewById(R.id.weekly_stats_arrow_left);
        fleet_weekly_navigationRight = (ImageView) fleetviewStatistics.findViewById(R.id.weekly_stats_arrow_right);
        fleet_stats_arrowLeft = (ImageView) fleetviewStatistics.findViewById(R.id.fleet_stats_arrow_left);
        fleet_stats_arrowRight = (ImageView) fleetviewStatistics.findViewById(R.id.fleet_stats_arrow_right);
        fleetfitChart = (FitChart) fleetviewStatistics.findViewById(R.id.fleet_stats_fitcharttotalrides_avg_score);
        total_fleet_avgscore = (TextView) fleetviewStatistics.findViewById(R.id.fleet_stats_score_total_rides_avg);
        fleet_percentage = (TextView) fleetviewStatistics.findViewById(R.id.fleet_stats_score_percentage_avg_score);
        fleet_name = (TextView) fleetviewStatistics.findViewById(R.id.first_name_fleet_stats);

        tv_nofleetdata = (TextView) fleetviewStatistics.findViewById(R.id.tv_noFleetStatistics);

        calender = Calendar.getInstance();
        currentWeek = calender.get(Calendar.WEEK_OF_YEAR);
        year = calender.get(Calendar.YEAR);
        Log.e("Current Week No"," : "+currentWeek);
        Log.e("Current Year "," : "+year);
        fleet_week_picker.setText(getActivity().getResources().getString(R.string.weekno)+" "+ currentWeek);
        Log.e("Fleet Start Date",":"+AppUtils.getStartEndOFWeek(currentWeek,year,true));
        Log.e("Fleet End Date",":"+AppUtils.getStartEndOFWeek(currentWeek,year,false));
        fleet_weekly_navigationRight.setVisibility(View.INVISIBLE);

        listViewFleetStatistics = (RecyclerView) fleetviewStatistics.findViewById(R.id.fleetstatistics_list);
        mLayoutManager  = new LinearLayoutManager(getContext());
        listViewFleetStatistics.setLayoutManager(mLayoutManager);
        listViewFleetStatistics.setHasFixedSize(true);
        listViewFleetStatistics.setItemAnimator(new DefaultItemAnimator());
        /*fleetStatisticsAdapter = new FleetStatisticsAdapter(statsBeansArrayList);
        listViewFleetStatistics.setAdapter(fleetStatisticsAdapter);
        fleetStatisticsAdapter.notifyDataSetChanged();*/
        preDate = AppUtils.getStartEndOFWeek(currentWeek,year,true);
        endDate = AppUtils.getStartEndOFWeek(currentWeek,year,false);




        fleet_weekly_navigationLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fleet_weekly_navigationRight.setVisibility(View.VISIBLE);
                calender.add(Calendar.WEEK_OF_YEAR, -1);
                weekNo = calender.get(Calendar.WEEK_OF_YEAR);
                year = calender.get(Calendar.YEAR);
                fleet_week_picker.setText(getActivity().getResources().getString(R.string.weekno)+" "+ weekNo);
                if(weekNo == currentWeek){
                    fleet_weekly_navigationRight.setVisibility(View.INVISIBLE);
                }

                preDate = AppUtils.getStartEndOFWeek(weekNo,year,true);
                endDate = AppUtils.getStartEndOFWeek(weekNo,year,false);
                getfleetStatsWeeklyNerwork(preDate, endDate);
                counter ++;
                if(counter == 4){
                    fleet_weekly_navigationLeft.setVisibility(View.INVISIBLE);
                }

            }
        });
        fleet_weekly_navigationRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calender.add(Calendar.WEEK_OF_YEAR, 1);
                weekNo = calender.get(Calendar.WEEK_OF_YEAR);
                year = calender.get(Calendar.YEAR);
                fleet_week_picker.setText(getActivity().getResources().getString(R.string.weekno)+" "+ weekNo);
                if(weekNo == currentWeek){
                    fleet_weekly_navigationRight.setVisibility(View.INVISIBLE);
                }

                preDate = AppUtils.getStartEndOFWeek(weekNo,year,true);
                endDate = AppUtils.getStartEndOFWeek(weekNo,year,false);
                getfleetStatsWeeklyNerwork(preDate, endDate);
                counter --;
                fleet_weekly_navigationLeft.setVisibility(View.VISIBLE);
            }
        });


        fleet_stats_arrowLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new FleetSafetyGraph();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(getId(), fragment);
               // fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });


        fleet_stats_arrowRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fleet_stats_arrowLeft.setVisibility(View.VISIBLE);


                fleet_stats_arrowRight.setVisibility(View.INVISIBLE);
            }
        });
        getfleetStatsWeeklyNerwork(preDate, endDate);
        if(DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE_SUBSCRIPTION_EXPIRED) == true){
            DemoModeDialog();
        }
        return fleetviewStatistics;
    }



    @Override
    public void onPause() {
        super.onPause();

    }
    @Override
    public void onDetach() {
        super.onDetach();
        try{
            progressBarDialogFleetStats.dismiss();
            progressBarDialogFleetStats=null;
        }catch (NullPointerException exception){
            exception.printStackTrace();
        }
        preDate=null;
        endDate=null;

    }

    private void getfleetStatsWeeklyNerwork(String preDate, String endDate){
        try{
            if (AppUtils.isNetworkAvailable()) {

                progressBarDialogFleetStats =new ProgressBarDialog(getActivity());
                progressBarDialogFleetStats.setTitle(getString(R.string.title_progress_dialog));
                progressBarDialogFleetStats.setMessage(getString(R.string.body_progress_dialog));
                progressBarDialogFleetStats.show();

                RideBDD tmp = new RideBDD(getActivity());

                String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER);
                List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                final TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
                String deviceId = telephonyManager.getDeviceId();
                mParams.add(new BasicNameValuePair("devID",deviceId));
                mParams.add(new BasicNameValuePair("phone_number",phoneNumber));
                mParams.add(new BasicNameValuePair("from", preDate));
                mParams.add(new BasicNameValuePair("to", endDate));
                Log.e("PARAMS", "" + mParams);
                GetFleetStatisticsAsyncTask getFleetStatisticsAsyncTask = new GetFleetStatisticsAsyncTask(getActivity(), WebServiceConstants.END_POINT_FLEET_STATISTICS,mParams );
                getFleetStatisticsAsyncTask.execute();
            }else {
                String companyName = DataHandler.getStringPreferences(AppConstants.GRAPH_WEEKLY_COMPANYs_NAME);
                fleet_name.setText(companyName);

            }
        }catch (Exception exception){
            exception.printStackTrace();
        }
    }


}
