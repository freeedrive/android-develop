package com.studio.barefoot.freeedrivebeacononlyapp.dialogs;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.ChangePhoneNumberAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.NewPhoneNumberTextActivity.progressBarDialogReCHngPhn;

/**
 * Created by mcs on 1/18/2017.
 */

public class ChangePhoneNumberDialog extends BaseAlertDialog implements DialogInterface.OnClickListener {
    EditText et_old_phoneNumber, new_phone_no;
    ContentResolver resolver;
    public static ProgressBarDialog progressBarDialogCHngPhn;

    private boolean isDissmiss = true;
    View focusView = null;
    public boolean ReupdateNumber = false;

    public ChangePhoneNumberDialog(Context context) {
        super(context);


        LayoutInflater factory = LayoutInflater.from(context);
        final View progressBarView = factory.inflate(R.layout.custom_change_phn_dialog, null);

        setView(progressBarView);
        this.context = context;

        et_old_phoneNumber = (EditText) progressBarView.findViewById(R.id.et_old_phoneNumber);
        new_phone_no = (EditText) progressBarView.findViewById(R.id.new_phone_no);
        et_old_phoneNumber.setEnabled(false);
        if (DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER) != null && !DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER).isEmpty()) {
            et_old_phoneNumber.setText(DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER));
        }
        et_old_phoneNumber.setEnabled(false);
        setButton(BUTTON_POSITIVE, context.getString(R.string.change), this);
        setButton(BUTTON_NEGATIVE, context.getString(R.string.cancel), this);
        setTitle(context.getString(R.string.title_change_phone));
        resolver = context.getContentResolver();
        setCancelable(false);
    }

    public ChangePhoneNumberDialog(Context context, String phoneNumber, String newNumber) {
        super(context);


        LayoutInflater factory = LayoutInflater.from(context);
        final View progressBarView = factory.inflate(R.layout.custom_change_phn_dialog, null);

        setView(progressBarView);
        this.context = context;

        et_old_phoneNumber = (EditText) progressBarView.findViewById(R.id.et_old_phoneNumber);
        new_phone_no = (EditText) progressBarView.findViewById(R.id.new_phone_no);
        et_old_phoneNumber.setEnabled(false);
        et_old_phoneNumber.setText(phoneNumber);
        et_old_phoneNumber.setEnabled(false);
        setButton(BUTTON_POSITIVE, context.getString(R.string.change), this);
        setButton(BUTTON_NEGATIVE, context.getString(R.string.cancel), this);
        setTitle(context.getString(R.string.title_change_phone));
        resolver = context.getContentResolver();
        setCancelable(false);
        ReupdateNumber = true;
    }

    public ChangePhoneNumberDialog(Context context, String phoneNumber) {
        super(context);


        LayoutInflater factory = LayoutInflater.from(context);
        final View progressBarView = factory.inflate(R.layout.custom_change_phn_dialog, null);

        setView(progressBarView);
        this.context = context;

        et_old_phoneNumber = (EditText) progressBarView.findViewById(R.id.et_old_phoneNumber);
        new_phone_no = (EditText) progressBarView.findViewById(R.id.new_phone_no);
        et_old_phoneNumber.setEnabled(false);
        et_old_phoneNumber.setText(phoneNumber);
        et_old_phoneNumber.setEnabled(false);
        setButton(BUTTON_POSITIVE, context.getString(R.string.change), this);
        setButton(BUTTON_NEGATIVE, context.getString(R.string.cancel), this);
        setTitle(context.getString(R.string.title_change_phone));
        resolver = context.getContentResolver();
        setCancelable(false);
        ReupdateNumber = false;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int which) {
        switch (which) {
            case BUTTON_POSITIVE:
                Log.e("IamClicked", "clicked");
                setCancelable(false);
                et_old_phoneNumber.setError(null);
                new_phone_no.setError(null);
                isDissmiss = false;

                if (TextUtils.isEmpty(new_phone_no.getText().toString())) {
                    new_phone_no.setError(context.getString(R.string.error_field_required));
                    focusView = new_phone_no;

                } else if (TextUtils.isEmpty(new_phone_no.getText().toString())) {
                    new_phone_no.setError(context.getString(R.string.error_field_required));
                    focusView = new_phone_no;
                } else if (!AppUtils.isValidPhonemaxlength(new_phone_no.getText().toString())) {
                    new_phone_no.setError(context.getString(R.string.error_invalid_mobilenumber_length));
                    focusView = new_phone_no;
                } else if (!AppUtils.isValidPhoneForUpdatePhoneNUmber(new_phone_no.getText().toString())) {
                    new_phone_no.setError(context.getString(R.string.error_invalid_mobilenumber_during_update));
                    focusView = new_phone_no;
                } else {
                    if (!ReupdateNumber) {
                        changePhoneNumber();
                    } else {
                        RechangePhoneNumber();
                    }

                }

                break;
            case BUTTON_NEGATIVE:
                isDissmiss = true;
                if (isDissmiss == true) {
                    dismiss();
//					((StartupActivity)context).finish();
                }
                break;
        }
    }

    public void RechangePhoneNumber() {
        progressBarDialogReCHngPhn = new ProgressBarDialog(context);
        progressBarDialogReCHngPhn.setTitle("Verifying");
        progressBarDialogReCHngPhn.setMessage("Please wait..");
        progressBarDialogReCHngPhn.showProgressBar();
        progressBarDialogReCHngPhn.show();
        List<NameValuePair> mParams = new ArrayList<NameValuePair>();
        final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        mParams.add(new BasicNameValuePair("phone_number", et_old_phoneNumber.getText().toString()));
        mParams.add(new BasicNameValuePair("new_phone_number", new_phone_no.getText().toString()));
        Log.e("Params", "" + mParams);
        DataHandler.updatePreferences(AppConstants.PHONE_TEMP, new_phone_no.getText().toString());
        ChangePhoneNumberAsyncTask changePhoneNumberAsyncTask = new ChangePhoneNumberAsyncTask(context, WebServiceConstants.END_POINT_REUPDATE_PHONE_NUMBER, mParams,false);
        changePhoneNumberAsyncTask.execute();
    }

    public void changePhoneNumber() {
        progressBarDialogCHngPhn = new ProgressBarDialog(context);
        progressBarDialogCHngPhn.setTitle("Verifying");
        progressBarDialogCHngPhn.setMessage("Please wait..");
        progressBarDialogCHngPhn.showProgressBar();
        progressBarDialogCHngPhn.show();
        List<NameValuePair> mParams = new ArrayList<NameValuePair>();
        final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        String deviceId = telephonyManager.getDeviceId();
        mParams.add(new BasicNameValuePair("device_id",deviceId));
        mParams.add(new BasicNameValuePair("devID",deviceId));
        mParams.add(new BasicNameValuePair("phone_number_old",et_old_phoneNumber.getText().toString()));
        mParams.add(new BasicNameValuePair("phone_number",new_phone_no.getText().toString()));
        mParams.add(new BasicNameValuePair("lang", DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG)));
        Log.e("Params",""+mParams) ;
        DataHandler.updatePreferences(AppConstants.PHONE_TEMP,new_phone_no.getText().toString());
        ChangePhoneNumberAsyncTask changePhoneNumberAsyncTask = new ChangePhoneNumberAsyncTask(context, WebServiceConstants.END_POINT_UPDATE_PHONE_NUMBER,mParams);
        changePhoneNumberAsyncTask.execute();
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
    @Override
    public void dismiss() {
        try {
            if (isDissmiss == true) {
                super.dismiss();
            }
        }catch (Exception exception){
            exception.printStackTrace();
        }

    }
    }
