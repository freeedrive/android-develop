package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.QrCodeActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by mcs on 1/5/2017.
 */

public class UpdateDemoModeAsyncTask extends BaseAsyncTask {

    /**
     * AsyncTask method basic calls during a request, calls the parent's method.
     */
    protected String doInBackground(String... params) {

        return "";
    }
    public UpdateDemoModeAsyncTask(Context context, String route, List<NameValuePair> pp) {
        super(context, route, pp);
    }

    /**
     * AsyncTask method basic calls after a request.
     */
    protected void onPostExecute(String s) {
        super.onPostExecute(s);



        Log.e("s",s);
        if(s != null) {
            try {
                QrCodeActivity.progressBarDialogDemoMode.dismiss();
                QrCodeActivity.progressBarDialogDemoMode =null;
            }catch (NullPointerException e){

            }
            int intResponse = Integer.parseInt(response);
            Log.e("ResponseCode", "" + intResponse);
            Log.e("response", "" + response);
            Log.e("resultat",resultat);
            switch (intResponse) {
                case 200:
                    DataHandler.updatePreferences(AppConstants.DEMO_MODE,true);
                    DataHandler.updatePreferences(AppConstants.PREF_KEY_LANG, "EN");
                    Log.e("Response",""+intResponse);
                    FetchUserDetails();
                    break;

                case 500:
                    AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                    error_500.setMessage(context.getResources().getString(R.string.error_sms_500)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_500.show();
                    //internal error
                    break;
                case 401:
                    //phone no not found
                    AlertDialog.Builder error_401 = new AlertDialog.Builder(context);
                    error_401.setMessage(context.getResources().getString(R.string.error_changePhone_401)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_401.show();
                    break;
                case 422:
                    //phone no not found
                    AlertDialog.Builder error_422 = new AlertDialog.Builder(context);
                    error_422.setMessage("Please scan the provided beacon to go further").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_422.show();
                    break;


            }
        }else {
            try {
                QrCodeActivity.progressBarDialogDemoMode.dismiss();
                QrCodeActivity.progressBarDialogDemoMode =null;
            }catch (NullPointerException e){

            }

        }
    }
    private void FetchUserDetails() {
        /*progressBarDialogSyncData = new ProgressBarDialog(context);
        *//*progressBarDialogSyncData.setTitle(context.getString(R.string.title_progress_dialog));
        progressBarDialogSyncData.setMessage(context.getString(R.string.body_progress_dialog));*//*
        progressBarDialogSyncData.setTitle("Syncing");
        progressBarDialogSyncData.setMessage("wait..");
        progressBarDialogSyncData.show();*/
        List<NameValuePair> mParams = new ArrayList<NameValuePair>();
        final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = telephonyManager.getDeviceId();
        mParams.add(new BasicNameValuePair("devID", deviceId));
        mParams.add(new BasicNameValuePair("phone_number", DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
        FetchProfileAsyncTaskForLogin fetchProfileAsyncTask = new FetchProfileAsyncTaskForLogin(context, WebServiceConstants.END_POINT_FETCH_PROFILE, mParams);
        fetchProfileAsyncTask.execute();
    }
}
