package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;


import android.content.Context;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;

import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesActivity.deleteAllMessages;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesActivity.deleteMessages;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesActivity.progressBarDialogNotifications;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesActivity.selectAll;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesActivity.stringArrayListOfPositionsDeleted;


/**
 * Created by ttwyf on 1/20/2017.
 */

public class DeleteMessagesAsyncTask extends AsyncTask<Void,Void,Void> {
    /**
     * Url server
     */
    private static final String mUrl =  WebServiceConstants.WEBSERVICE_URL_PREFIX+WebServiceConstants.END_POINT_DELETE_NOTIFICATIONS;
            //"http://xyperdemos.com/pk_freeedrive_backend-master/public/api/score";
    /**
     * Represents the account token
     */
    private String token;
    /**
     * Represents the response code from server
     */
    private int responseCode;
    /**
     * Represents the resultat from server
     */
    private String resultat;
    /**
     * Represents the liste that contains all the rides
     */
    private JSONArray data;

    private String phoneNumber="";

    public boolean deletAll = false;
    /**
     * Represents the context, the Activity
     */
    private Context context;

    public DeleteMessagesAsyncTask(JSONArray listRide, Context context){
        //Log.i("Adneom","(MenuActivity) list is "+listRide);
        this.data = listRide;
        this.context = context;
        token = "Bearer "+ DataHandler.getStringPreferences(AppConstants.TOKEN_NUMBER);
        Log.e("AuthToken",token);
        deletAll = false;
        //db:

    }
    public DeleteMessagesAsyncTask(String phoneNumber, Context context){
        //Log.i("Adneom","(MenuActivity) list is "+listRide);
        this.phoneNumber = phoneNumber;
        this.context = context;
        token = "Bearer "+ DataHandler.getStringPreferences(AppConstants.TOKEN_NUMBER);
        Log.e("AuthToken",token);
        deletAll = true;
        //db:

    }
    @Override
    protected Void doInBackground(Void... voids) {
        try {
            URL url = new URL(mUrl);
            Log.e("Url",""+url);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            //urlConnection.setUseCaches(false);

            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Authorization", token);
            urlConnection.setRequestProperty("X-Requested-With", "XMLHttpRequest");

            //json in body:
            JSONObject dataObj = new JSONObject();
            if (deletAll){
                dataObj.put("phone_number",this.phoneNumber);

            }else{
                dataObj.put("message_ids", this.data);

            }


            //Log.i("Adneom", " (MenuActivity) data before update : " + dataObj);
            byte[] outputInBytes = dataObj.toString().getBytes("UTF-8");
            OutputStream os = urlConnection.getOutputStream();
            os.write(outputInBytes);
            os.close();

            responseCode= urlConnection.getResponseCode();
            String message =  urlConnection.getResponseMessage();
            Log.e("Response Message :",message);
            Log.e("Response Code :",""+responseCode);

            InputStream inputStream;
            // get stream
            if (responseCode< HttpURLConnection.HTTP_BAD_REQUEST) {
                Log.i("Adneom","Response code (SYNC) is "+responseCode+" and is OK");
                inputStream = urlConnection.getInputStream();
            } else {
                inputStream = urlConnection.getErrorStream();
                Log.i("Adneom","Response code (SYNC) is "+responseCode+" and is NOT OK");
            }

            // parse stream
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String temp, response = "";
            while ((temp = bufferedReader.readLine()) != null) {
                response += temp;
            }
            resultat = response;
            Log.e("resultAt",""+resultat);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.i("Adneom","Error url : "+e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("Adneom", "Error open connection : "+e.getMessage());
        } catch (JSONException e) {
            e.printStackTrace();
            Log.i("Adneom", "Error JSON OBJECT : " + e.getMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        try{

        }catch (NullPointerException exception){
            exception.printStackTrace();
        }
        try{
            progressBarDialogNotifications.dismiss();
            progressBarDialogNotifications=null;
        }catch (NullPointerException exception){
            exception.printStackTrace();
        }

        switch (responseCode){

            case 200:
                //Log.i("Adneom","(MenuActivity) code OK is "+responseCode+" \n from server : "+resultat);

                /*ScoreSynchronizationActivity.progressBarDialogNotifications.dismiss();
                ScoreSynchronizationActivity.progressBarDialogNotifications = null;*/
                String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER);
                final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                String deviceId = telephonyManager.getDeviceId();
                List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                mParams.add(new BasicNameValuePair("phone_number",phoneNumber));
                mParams.add(new BasicNameValuePair("devID",deviceId));
                Log.e("PARAMS", "" + mParams);
                deleteMessagesLocally();

                GetchNotificationAsyncTask getchNotificationAsyncTask = new GetchNotificationAsyncTask(context, WebServiceConstants.END_POINT_NOTIFICATIONS,mParams );
                getchNotificationAsyncTask.execute();
                //method to delete all messages locally
                stringArrayListOfPositionsDeleted.clear();

                try{
                    progressBarDialogNotifications.dismiss();
                    progressBarDialogNotifications=null;
                }catch (NullPointerException exception){
                    exception.printStackTrace();
                }
                try{
                    deleteAllMessages.setVisibility(View.INVISIBLE);
                    if (selectAll.getVisibility()==View.VISIBLE){
                        deleteMessages.setVisibility(View.VISIBLE);
                    }else{
                        deleteMessages.setVisibility(View.INVISIBLE);
                    }
                }catch (OutOfMemoryError memoryError){
                    memoryError.printStackTrace();
                }
                break;
            case 400: //
                Log.i("Adneom","(MenuActivity) code NOT OK is "+responseCode+" \n from server : "+resultat);
            case 409:
                Log.i("Adneom","(MenuActivity) code NOT OK is "+responseCode+" \n from server : "+resultat);
                break;
            case 500:

                Log.i("Adneom","(MenuActivity) code NOT OK is "+responseCode+" \n from server : "+resultat);
                break;
            default:
                try{

                }catch (NullPointerException exception){
                    exception.printStackTrace();
                }

                break;
        }

    }

    private void deleteMessagesLocally() {
        RideBDD tmp = new RideBDD(context);
        tmp.open();
        tmp.deleteMessageInDb();

        tmp.close();

    }


}

