package com.studio.barefoot.freeedrivebeacononlyapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hbb20.CountryCodePicker;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.DropDownListAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.RegisterAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.PrivacyDialogNew;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.PrivateHoursDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.UIUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterActivity extends AppCompatActivity {
    EditText et_frstName, et_LastName, et_Email, et_Phone, et_Language;
    TextView tv_Hello;
    View actionBarView;
    ImageView spinner,flags;
    Button img_Next_Register;
    String selectedLang = "";
    CountryCodePicker ccp;

    private String gcm_token = "";
    private String email = "";
    private String phoneNumber = "";
    private String numberFormat = "";
    private String correctPhoneNumber = "";
    private Locale locale = null;
    Configuration config;
    CheckBox checkBoxPrivateHours;
    int privateHour = 0;
    PrivacyDialogNew privacyDialog;
    Typeface typeface;
   public static  List<NameValuePair> mParams;
    public RegisterActivity() {
        LocaleUtils.updateConfig(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_register);
        et_frstName = (EditText) findViewById(R.id.et_firstName);
        et_LastName = (EditText) findViewById(R.id.et_lastName);
        et_Email = (EditText) findViewById(R.id.et_Email);
        et_Phone = (EditText) findViewById(R.id.et_phone);
        tv_Hello = (TextView) findViewById(R.id.new_driver_heading);
        spinner = (ImageView) findViewById(R.id.language_Spinner);
        flags = (ImageView) findViewById(R.id.flags_img);

        img_Next_Register = (Button) findViewById(R.id.img_Next_Register);
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        checkBoxPrivateHours = (CheckBox) findViewById(R.id.checkbox_private_hours);
        actionBarView = getLayoutInflater().inflate(R.layout.custom_toolbarr, null);

        et_Language = (EditText) findViewById(R.id.et_language);
  /*      et_frstName.setTypeface(UIUtils.getInstance().getLightFont(this));
        et_LastName.setTypeface(UIUtils.getInstance().getLightFont(this));
        et_Email.setTypeface(UIUtils.getInstance().getLightFont(this));
        et_Phone.setTypeface(UIUtils.getInstance().getLightFont(this));
        et_Language.setTypeface(UIUtils.getInstance().getLightFont(this));
        tv_Hello.setTypeface(UIUtils.getInstance().getMediumFont(this));*/

  /*     // et_Phone.setText(ccp.getDefaultCountryCodeWithPlus());
        et_Phone.setSelection(et_Phone.getText().length());
        setupActionBar();
        typeface = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        et_Phone.setTypeface(typeface);
        et_frstName.setTypeface(typeface);
        et_LastName.setTypeface(typeface);
        et_Email.setTypeface(typeface);
        et_Phone.setTypeface(typeface);
        */

        //et_Language.setText(getResources().getString(R.string.english));

        // Check for the language preference for translation

        setupActionBar();
         et_Language.setText(getResources().getString(R.string.english));
        // et_Language.setClickable(false);

        if (DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG) != null || !DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG).isEmpty()) {
            String currentLang = DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG);
            if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.fr))) {
                currentLang = getResources().getString(R.string.french);
                flags.setImageResource(R.drawable.france);
            } else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.nl))) {
                currentLang = getResources().getString(R.string.dutch);
                flags.setImageResource(R.drawable.netherlands);
            } else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.es))) {
                currentLang = getResources().getString(R.string.spanish);
                flags.setImageResource(R.drawable.spain);
            } else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.it))){
                currentLang = getResources().getString(R.string.italy);
                flags.setImageResource(R.drawable.italy);
            }/*else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.da))){
                currentLang = getResources().getString(R.string.danish);
                flags.setImageResource(R.drawable.denmark);
            }*//*else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.no))){
                currentLang = getResources().getString(R.string.norway);
                flags.setImageResource(R.drawable.norway);
            }*//*else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.de))){
                currentLang = getResources().getString(R.string.deutsch);
                flags.setImageResource(R.drawable.germany);
            }*/else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.pt))){
                currentLang = getResources().getString(R.string.portuguese);
                flags.setImageResource(R.drawable.portugal);
            }/*else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.fi))){
                currentLang = getResources().getString(R.string.finnish);
                flags.setImageResource(R.drawable.finland);
            }*/else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.en))) {
                currentLang = getResources().getString(R.string.english);
                flags.setImageResource(R.drawable.uk);
            }else {
                currentLang = getResources().getString(R.string.english);
                flags.setImageResource(R.drawable.uk);
            }
            et_Language.setText(currentLang);
        } else {
           // String currentLang = "en";

            String currentLang = config.locale.getLanguage();
            if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.fr))) {
                currentLang = getResources().getString(R.string.french);
                flags.setImageResource(R.drawable.france);
            } else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.nl))) {
                currentLang = getResources().getString(R.string.dutch);
                flags.setImageResource(R.drawable.netherlands);
            } else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.es))) {
                currentLang = getResources().getString(R.string.spanish);
                flags.setImageResource(R.drawable.spain);
            } else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.it))){
                currentLang = getResources().getString(R.string.italy);
                flags.setImageResource(R.drawable.italy);
            }/*else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.da))){
                currentLang = getResources().getString(R.string.danish);
                flags.setImageResource(R.drawable.denmark);
            }else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.no))){
                currentLang = getResources().getString(R.string.norway);
                flags.setImageResource(R.drawable.norway);
            }else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.de))){
                currentLang = getResources().getString(R.string.deutsch);
                flags.setImageResource(R.drawable.germany);
            }*/else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.pt))){
                currentLang = getResources().getString(R.string.portuguese);
                flags.setImageResource(R.drawable.portugal);
            }/*else if (currentLang.equalsIgnoreCase(getResources().getString(R.string.fi))){
                currentLang = getResources().getString(R.string.finnish);
                flags.setImageResource(R.drawable.finland);
            }*/else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.en))) {
                currentLang = getResources().getString(R.string.english);
                flags.setImageResource(R.drawable.uk);
            } else {
                currentLang = getResources().getString(R.string.english);
                flags.setImageResource(R.drawable.uk);
            }

            et_Language.setText(currentLang);
        }
        et_frstName.setMaxLines(1);
        et_frstName.setSingleLine();

        et_LastName.setMaxLines(1);
        et_LastName.setSingleLine();

        et_Email.setMaxLines(1);
        et_Email.setSingleLine();

        et_Phone.setMaxLines(1);
        et_Phone.setSingleLine();
        et_Language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spawnPopUp(et_Language);
            }
        });
        img_Next_Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    if (!checkFields()) {
                        Register();
                   /*     if (!DataHandler.getBooleanPreferences(AppConstants.USER_ACCEPTED_PRIVACY)) {
*//*
                            privacyDialog = new PrivacyDialog(RegisterActivity.this);
                            privacyDialog.show();*//*

                        }else{
                            Register();
                        }*/


                    }


            }
        });
        numberFormat = ccp.getDefaultCountryCodeWithPlus();
        et_Phone.setSelection(et_Phone.getText().length());
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                //  et_Sms_Code.setText(ccp.getSelectedCountryCodeWithPlus());
                numberFormat = ccp.getSelectedCountryCodeWithPlus();
                et_Phone.setText(ccp.getSelectedCountryCodeWithPlus());
                et_Phone.setSelection(et_Phone.getText().length());

            }
        });

      /*  InputMethodManager imm = (InputMethodManager) getSystemService(this.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);*/
        //spinner.setAdapter(spinnerAdapter);
        config = getBaseContext().getResources().getConfiguration();

        checkBoxPrivateHours.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    privateHour = 1;
                } else {
                    privateHour = 0;
                }
                Log.e("privateHours", "" + privateHour);
            }
        });
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }


    private void spawnPopUp(View layout1) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.pop_up_window, (ViewGroup) findViewById(R.id.popup_layout));
        int dp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, getResources().getDisplayMetrics());
        final PopupWindow popup = new PopupWindow(layout, dp, RelativeLayout.LayoutParams.WRAP_CONTENT, true);
        popup.setTouchable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.setOutsideTouchable(true);
        popup.setWidth(900);
        popup.setHeight(RelativeLayout.LayoutParams.WRAP_CONTENT);
        popup.setTouchInterceptor(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popup.dismiss();
                    return true;
                }
                return false;
            }
        });
        popup.setContentView(layout);
        popup.showAsDropDown(layout1);


        //populate the drop-down list
        final List<String> languages = new LinkedList<String>();
        languages.add(getResources().getString(R.string.english));
        languages.add(getResources().getString(R.string.french));
        languages.add(getResources().getString(R.string.dutch));
        languages.add(getResources().getString(R.string.spanish));
        languages.add(getResources().getString(R.string.italy));
       /* languages.add(getResources().getString(R.string.danish));
        languages.add(getResources().getString(R.string.norway));
        languages.add(getResources().getString(R.string.deutsch));*/
        languages.add(getResources().getString(R.string.portuguese));
        /*languages.add(getResources().getString(R.string.finnish));*/
        final ListView list = (ListView) layout.findViewById(R.id.list);
        DropDownListAdapter adapter = new DropDownListAdapter(this, languages);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedLang = languages.get(position).toString();
                popup.dismiss();
                et_Language.setText(selectedLang);
                if (selectedLang.equals(getResources().getString(R.string.english))) {
                    selectedLang = getBaseContext().getResources().getString(R.string.en);
                } else if (selectedLang.equals(getResources().getString(R.string.french))) {
                    selectedLang = getBaseContext().getResources().getString(R.string.fr);
                } else if (selectedLang.equals(getResources().getString(R.string.dutch))) {
                    selectedLang = getBaseContext().getResources().getString(R.string.nl);
                } else if (selectedLang.equals(getResources().getString(R.string.spanish))){
                    selectedLang = getBaseContext().getResources().getString(R.string.es);
                }else if (selectedLang.equals(getResources().getString(R.string.italy))){
                    selectedLang = getBaseContext().getResources().getString(R.string.it);
                }/*else if (selectedLang.equals(getResources().getString(R.string.danish))){
                    selectedLang  = getBaseContext().getResources().getString(R.string.da);
                }else if (selectedLang.equals(getResources().getString(R.string.norway))){
                    selectedLang  = getBaseContext().getResources().getString(R.string.no);
                }else if (selectedLang.equals(getResources().getString(R.string.deutsch))){
                    selectedLang  = getBaseContext().getResources().getString(R.string.de);
                }*/else if (selectedLang.equals(getResources().getString(R.string.portuguese))){
                    selectedLang  = getBaseContext().getResources().getString(R.string.pt);
                }/*else if (selectedLang.equals(getResources().getString(R.string.finnish))){
                    selectedLang = getBaseContext().getResources().getString(R.string.fi);
                }*/
                else {
                    selectedLang = getBaseContext().getResources().getString(R.string.en);
                }

                if(et_Phone.getText().toString().isEmpty()&&et_Email.getText().toString().isEmpty()&&et_frstName.getText().toString().isEmpty()&&et_LastName.getText().toString().isEmpty()){
                    ApplicationController controller = (ApplicationController) getApplication();
                    controller.updateLocale(selectedLang);
                    Intent intent = new Intent(RegisterActivity.this, RegisterActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    DataHandler.updatePreferences(AppConstants.PREF_KEY_LANG, selectedLang);
                }
                else{
                    DataHandler.updatePreferences(AppConstants.PREF_KEY_LANG, selectedLang);
                    if (selectedLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.fr))) {
                        selectedLang = getResources().getString(R.string.french);
                        flags.setImageResource(R.drawable.france);
                    } else if (selectedLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.nl))) {
                        selectedLang = getResources().getString(R.string.dutch);
                        flags.setImageResource(R.drawable.netherlands);
                    } else if (selectedLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.es))) {
                        selectedLang = getResources().getString(R.string.spanish);
                        flags.setImageResource(R.drawable.spain);
                    } else if (selectedLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.it))){
                        selectedLang = getResources().getString(R.string.italy);
                        flags.setImageResource(R.drawable.italy);
                    }/*else if (selectedLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.da))){
                        selectedLang = getResources().getString(R.string.danish);
                        flags.setImageResource(R.drawable.denmark);
                    }else if (selectedLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.no))){
                        selectedLang = getResources().getString(R.string.norway);
                        flags.setImageResource(R.drawable.norway);
                    }else if (selectedLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.de))){
                        selectedLang = getResources().getString(R.string.deutsch);
                        flags.setImageResource(R.drawable.germany);
                    }*/else if (selectedLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.pt))){
                        selectedLang = getResources().getString(R.string.portuguese);
                        flags.setImageResource(R.drawable.portugal);
                    }/*else if (selectedLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.fi))){
                        selectedLang = getResources().getString(R.string.finnish);
                        flags.setImageResource(R.drawable.finland);
                    }*/else if (selectedLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.en))) {
                        selectedLang = getResources().getString(R.string.english);
                        flags.setImageResource(R.drawable.uk);
                    } else {
                        selectedLang = getResources().getString(R.string.english);
                        flags.setImageResource(R.drawable.uk);
                    }

                }

            }
        });


    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    // Check the fields of register activity and then make a server request for it to register
    private void Register() {
        String lang = "";
        if (et_Language.getText().toString().equals(getResources().getString(R.string.english)))
            lang = getBaseContext().getResources().getString(R.string.en);
        else if (et_Language.getText().toString().equals(getResources().getString(R.string.dutch)))
            lang = getBaseContext().getResources().getString(R.string.nl);
        else if (et_Language.getText().toString().equals(getResources().getString(R.string.french)))
            lang = getBaseContext().getResources().getString(R.string.fr);
        else if (et_Language.getText().toString().equals(getResources().getString(R.string.spanish)))
            lang = getBaseContext().getResources().getString(R.string.es);
        else if (et_Language.getText().toString().equals(getResources().getString(R.string.italy)))
            lang = getBaseContext().getResources().getString(R.string.it);
        /*else if (et_Language.getText().toString().equals(getResources().getString(R.string.danish)))
            lang = getBaseContext().getResources().getString(R.string.da);
        else if (et_Language.getText().toString().equals(getResources().getString(R.string.norway))){
            lang = getBaseContext().getResources().getString(R.string.no);
        }else if (et_Language.getText().toString().equals(getResources().getString(R.string.deutsch))){
            lang = getBaseContext().getResources().getString(R.string.de);
        }*/else if (et_Language.getText().toString().equals(getResources().getString(R.string.portuguese))){
            lang = getBaseContext().getResources().getString(R.string.pt);
        }/*else if (et_Language.getText().toString().equals(getResources().getString(R.string.finnish))){
            lang = getBaseContext().getResources().getString(R.string.fi);
        }*/
        else
            lang = getBaseContext().getResources().getString(R.string.en);


        try {
                if (AppUtils.isNetworkAvailable()) {
                    final TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    String deviceId = mngr.getDeviceId();
                    correctPhoneNumber = numberFormat + phoneNumber;
                    DataHandler.updatePreferences(AppConstants.PHONE_TEMP, phoneNumber);
                    DataHandler.updatePreferences(AppConstants.PREF_KEY_LANG, lang);
        /* **********************  NEW BACK : register ************************* */
                     mParams = new ArrayList<NameValuePair>();
                    mParams.add(new BasicNameValuePair("first_name", et_frstName.getText().toString().trim()));
                    mParams.add(new BasicNameValuePair("last_name", et_LastName.getText().toString().trim()));
                    mParams.add(new BasicNameValuePair("email", et_Email.getText().toString().trim().toLowerCase()));
                    mParams.add(new BasicNameValuePair("phone_number",phoneNumber));
                    //mParams.add(new BasicNameValuePair("city",city.getText().toString()));
                    // String lng = (lang.isEmpty())? Locale.getDefault().getLanguage().toUpperCase(): lang.toString().toUpperCase();

                    mParams.add(new BasicNameValuePair("lang", lang));
                    mParams.add(new BasicNameValuePair("phone_model", Build.MODEL));
                    mParams.add(new BasicNameValuePair("phone_name", Build.MANUFACTURER));
                    mParams.add(new BasicNameValuePair("device_id", deviceId));
                    mParams.add(new BasicNameValuePair("devID",deviceId));
                    //add token from frirebase :

                    //  String gcm_token= FirebaseInstanceId.getInstance().getToken();
                    gcm_token = DataHandler.getStringPreferences(AppConstants.FIRE_BASE_TOKE);
                    //String version =   pInfo.versionName;
                    mParams.add(new BasicNameValuePair("gcm_token", gcm_token));
                    mParams.add(new BasicNameValuePair("phone_os_version", Build.VERSION.RELEASE));
                    mParams.add(new BasicNameValuePair("operating_system","A"));
                    mParams.add(new BasicNameValuePair("app_version","2.5"));
                    //mParams.add(new BasicNameValuePair("app_version",version));
                    //Log.i("Adneom", "Params : " + mParams);

                    long installed = this.getPackageManager().getPackageInfo("com.studio.barefoot.freeedrivebeacononlyapp", 0).firstInstallTime;
                    String installationDate = AppUtils.formateLongToOnlyDateForServer(installed);
                    mParams.add(new BasicNameValuePair("install_date", installationDate));
                    mParams.add(new BasicNameValuePair("private_hour_enable", String.valueOf(privateHour)));
                    Log.e("installationDate", "" + installationDate);
                    Log.e("installed", "" + installed);

                    JSONObject jsonObject = new JSONObject();
                    if (jsonObject != null) {

                        try {
                            jsonObject.put("first_name", et_frstName.getText().toString().trim());
                            jsonObject.put("last_name", et_LastName.getText().toString().trim());
                            jsonObject.put("email", et_Email.getText().toString().trim().toLowerCase());
                            jsonObject.put("phone_number", phoneNumber);
                            jsonObject.put("lang", lang);
                            jsonObject.put("gcm_token", gcm_token);

                            DataHandler.updatePreferences(AppConstants.TEMP_PROFILE_KEY, jsonObject.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    FragmentManager fm = getSupportFragmentManager();
                    DataHandler.updatePreferences(AppConstants.PHONE_NUMBER, phoneNumber);
                    DataHandler.updatePreferences(AppConstants.PHONE_TEMP, phoneNumber);
                    privacyDialog = new PrivacyDialogNew().newInstance(RegisterActivity.this,mParams);
                    privacyDialog.show(fm,"");

                  /*  RegisterAsyncTask registerAsyncTask = new RegisterAsyncTask(this, WebServiceConstants.END_POINT_REGISTER, mParams);
                    registerAsyncTask.execute();*/
                } else {
                    AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(this);
                    error_No_Internet.setMessage(this.getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_No_Internet.show();
                }

        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    // for the checking the field of the register forms
    public boolean checkFields() {
        et_frstName.setError(null);
        et_LastName.setError(null);
        et_Email.setError(null);
        et_Phone.setError(null);
        et_Language.setError(null);
        boolean cancel = false;
        View focusView = null;
        email = et_Email.getText().toString().toLowerCase().trim().replaceAll("\\s+$", "");
        phoneNumber = et_Phone.getText().toString().trim().replaceAll("\\s+$", "");

          if (TextUtils.isEmpty(phoneNumber)) {

            et_Phone.setError(getString(R.string.error_field_required));
            focusView = et_Phone;
            cancel = true;
        }else if (AppUtils.isValidPhone(phoneNumber)) {
              et_Phone.setError(getString(R.string.error_invalid_mobilenumber));
              focusView = et_Phone;
              cancel = true;
          }else if (!AppUtils.isValidPhonemaxlength(phoneNumber)) {
              et_Phone.setError(getString(R.string.error_invalid_mobilenumber_length));
              focusView = et_Phone;
              cancel = true;

          }
        else if (TextUtils.isEmpty(et_Language.getText().toString())) {

            et_Language.setError(getString(R.string.error_field_required));
            focusView = et_Language;
            cancel = true;
        } else if (TextUtils.isEmpty(et_frstName.getText().toString())) {

            et_frstName.setError(getString(R.string.error_field_required));
            focusView = et_frstName;
            cancel = true;

        } else if (TextUtils.isEmpty(et_LastName.getText().toString())) {

            et_LastName.setError(getString(R.string.error_field_required));
            focusView = et_LastName;
            cancel = true;
        } else if (TextUtils.isEmpty(et_Email.getText().toString().trim())) {

            et_Email.setError(getString(R.string.error_field_required));
            focusView = et_Email;
            cancel = true;
        } else if (!AppUtils.emailValidator(email)) {

            et_Email.setError(getString(R.string.valid_email));
            focusView = et_Email;
            cancel = true;
        }
        if (cancel) {

            focusView.requestFocus();

        }
        return cancel;
    }

    @Override
    protected void onResume() {

        InputMethodManager imm = (InputMethodManager) getSystemService(this.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        super.onResume();
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    @Override
    public void onBackPressed() {
        /*super.onBackPressed();*/
        startActivity(new Intent(RegisterActivity.this, VerificationActivity.class));
        finish();
    }
}
