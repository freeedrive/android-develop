package com.studio.barefoot.freeedrivebeacononlyapp.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Chronometer;
import android.widget.Toast;

import com.studio.barefoot.freeedrivebeacononlyapp.BlockedFreeDriveActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.LoginActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.VerificationActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.FetchDrivingPerformanceScores;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.ScoreAsynctask;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.RidesBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mcs on 12/26/2016.
 */

public class AppUtils
{

    //key for beacon ranging
    public static Boolean isBGServiceActive = false;
    public static Boolean beaconFound = false;
    public static Boolean rideWasActive = false;
    public static Boolean isInRange = false;
    public static Boolean IamInExit =false;
    public static Boolean inRegion = false;
    public static Boolean outRegion = false;
    public static Boolean isLocationSrvcStopped = false;
    // General Speed Check
    public static Boolean speedCheck = false;
    // Check if speed Less then 5 km/h
    public static Boolean speedlessCheck = false;
    public static Boolean speedMonitorCheck = false;
    public static Boolean startLocService = false;
    public static Boolean gpsenabled = false;
    public static Boolean bluetoothenabled = false;
    /**
     * Allows to execute once a synchronization from wifi broadcast receiver
     */
    public static Boolean isUpdated = false;
    public static double beaconDisconnectTimeStamp;
    /**
     * Indicate if the screen is unlock to manage Sensors
     * True by default : installed application
     */
    public static Boolean isUnlock = false;
    /**
     * Allows to know the chronometer is started.
     */
    public static Boolean isStarted = false;
    /**
     * Chronometer of Receiver, It allows to test if a new journey = 7 min.
     */
    public static Chronometer chronometer;

    /**
     * Represents a new journey, by default is TRUE for the first time
     */
    public static Boolean isNewJourney = true;

    /**
     * Key for last update to synchronize data to server.
     */
    public static final String LAST_UPDATE = "last update";
    /**
     * Save the ride from Receiver to get the departure location and the departure time,
     * if this is not a new journey
     */
    /**
     * Represents the synchronization interval
     */
    public static final int SYNCHRONIZATION_INTERVAL = 11;
    /**
     * Key of shared preferences to save the values of last ride in format JSON object
     */
    public static final String SAFETY_SCORE_LAST_RIDE = "last ride";
    public static RidesBeans tmpRide;
    //private static java.lang.String britishFormate="dd/MM/yy HH:mm ";
    private static java.lang.String britishFormate = "dd/MM/yy";
    private static java.lang.String weekly_format = "dd/MM";
    private static java.lang.String messagesFormat = "dd-MM-yyyy HH:mm";
    private static java.lang.String BfFormate = "HH:mm ";
    private static java.lang.String britishFormateNew = "yyyy-MM-dd HH:mm:ss";
    /**
     * Variable for Passenger Mode switch
     */
    public static int passengerMode = 0;
    /**
     * Indicate the Passenger Mode is On or Off
     */
    public static Boolean ispassengerMode = false;


    public static RideBDD rideBDD = null;
    public static int demototalRides;
    public static long totalDaysDemoMode;
    public static boolean IsBluetoothEnabled = false;
    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN = AppConstants.REG_EMIAL;
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
    public static String formateLongToOnlyDateForServer(Long start_dt) {
        String finalDate;
        Calendar newCalendar = Calendar.getInstance();
        newCalendar.setTimeInMillis(start_dt);
        Date newDate = new Date(start_dt);
        SimpleDateFormat newFormat = new SimpleDateFormat(britishFormateNew);
        finalDate = newFormat.format(newDate);
        return finalDate;
    }
    public static String formate10LongDateToDisplay(Long start_dt) {
        String finalDate;
   /*     Calendar newCalendar = Calendar.getInstance();
        newCalendar.setTimeInMillis(start_dt);
        Date newDate = new Date(start_dt);*/
        SimpleDateFormat newFormat = new SimpleDateFormat(BfFormate);
        finalDate = newFormat.format(start_dt * 1000L);
        return finalDate;
    }
    public static String formatDateForMessages(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = messagesFormat;
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
    public static String formatStringDate(String start_dt) {
        String finalDate;
   /*     Calendar newCalendar = Calendar.getInstance();
        newCalendar.setTimeInMillis(start_dt);
        Date newDate = new Date(start_dt);*/
        SimpleDateFormat newFormat = new SimpleDateFormat(weekly_format);
        finalDate = newFormat.format(start_dt);
        return finalDate;
    }
    public static String formatStringDateForGraph(String date) {
        String formatted_date="";
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat output = new SimpleDateFormat(weekly_format);
        try {
            Date oneWayTripDate = input.parse(date);                 // parse input
            formatted_date = output.format(oneWayTripDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatted_date ;
    }

    public static boolean isValidPhoneForUpdatePhoneNUmber(String value) {
        //TODO: Replace this with your own logic
        if(value.startsWith("+")){
            return true;
        }else {
            return false;
        }


    }
    public static boolean isValidPhone(String value) {
        //TODO: Replace this with your own logic
        if(value.startsWith("0")| value.startsWith("+")){
            return false;
        }else {
            return true;
        }


    }    public static boolean isValidChangePhone(String value) {
    //TODO: Replace this with your own logic
    if(value.startsWith("0")| value.startsWith("+")){
        return true;
    }else {
        return false;
    }


}
    public static void rideSync(Context context){
        rideBDD = new RideBDD(context);

        rideBDD.open();
        JSONArray jsonArray = new JSONArray();
        JSONArray jsonArraySubRides = new JSONArray();
        jsonArray = rideBDD.getAllRidesForSyncing();
        jsonArraySubRides = rideBDD.getAllSubRidesForSyncing();

 /*       if(rideBDD.getAllSubRidesForSyncing().length()>0){
            jsonArraySubRides = rideBDD.getAllSubRidesForSyncing();
        }else {
            jsonArraySubRides = jsonArraySubRides.put("No sub rides for this Ride");
        }*/

        Log.e("strListe", "" + jsonArray);
        try {
            if (jsonArray != null && jsonArray.length() > 0) {
                manageSynchronization(jsonArray, jsonArraySubRides, context);

            } else {
                int status = rideBDD.getLastRideIsPerformanceUpdatedStatus();
                if (status == 0) {
                    Log.e("statusOfupdation", "" + status);
                    final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                    String deviceId = telephonyManager.getDeviceId();
                    List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                    mParams.add(new BasicNameValuePair("phone_number", DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
                    mParams.add(new BasicNameValuePair("devID", deviceId));
                    FetchDrivingPerformanceScores fetchDrivingPerformanceScores = new FetchDrivingPerformanceScores(context, WebServiceConstants.END_POINT_SPEEDING_SCORE, mParams);
                    fetchDrivingPerformanceScores.execute();
                }

/*
                Toast.makeText(context, "There is no Ride to Sync !Thanks", Toast.LENGTH_SHORT).show();
*/

            }
        } catch (NullPointerException exception) {
            exception.printStackTrace();
        }
    }
    /**
     * Allows to execute the synchronization request and update all rides after synchronization.
     */
    public static void manageSynchronization(JSONArray liste,JSONArray subList,Context context){
        //Allows to execute request if the listes are not empty :
        Boolean canExecuteAsynctaslk = true;

        //db:
   /*     rideBDD = new RideBDD(context);
        rideBDD.open();*/
        //get all rides :
        JSONArray listeRides = liste;  //JSONArray listeRides = rideBDD.getAllRides(true);
        JSONArray newListeRides = null;

        //listes :
        if (listeRides == null || listeRides.length() == 0) {
            //Log.i("Adneom","(MenuActivity) list is "+listeRides);
            canExecuteAsynctaslk = false;
        } else {
            //keep just good rides :
            //newListeRides = containsCoordinates(listeRides);
            newListeRides = liste;
            if (newListeRides == null || newListeRides.length() == 0) {
                Log.i("Adneom", "(MenuActivity) newlistRides is " + newListeRides);
                canExecuteAsynctaslk = false;
            }
        }

        //execute request :
        if(canExecuteAsynctaslk){
            final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String deviceId = telephonyManager.getDeviceId();
            Long currentTime =  System.currentTimeMillis();
            String timea=  formateLongToOnlyDateForServer(currentTime);
            LoggingOperations.writeToFile(context,"WHEN ENDING THE RIDE THESE WERE MY VALUES > <appUtils> "+ timea +" "+newListeRides.toString());
            ScoreAsynctask synchronizationMAsynctask = new ScoreAsynctask(newListeRides,subList,context,deviceId);
            //  ScoreAsynctask synchronizationMAsynctask = new ScoreAsynctask(newListeRides,context);
            synchronizationMAsynctask.execute();
        }else{
            //listeRides or newListeRides are null or empty :
            try{
                /*if(progressBarDialogNotifications != null && progressBarDialogNotifications.isShowing()){
                    progressBarDialogNotifications.cancel();
                }*/
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }

        }
    }
    public static long differenceTimeUtils(long departure, long arrival){
        long convertToMinutes=0;
        if (departure!=0 && arrival!=0){
            Log.e("Bfpk", "departure" + departure + "arrival " + arrival);
            long timeDifference= TimeUnit.MILLISECONDS.toSeconds(arrival)-TimeUnit.MILLISECONDS.toSeconds(departure);
            // long timeDifference= arrival-departure;

            convertToMinutes = timeDifference/60;
            if (convertToMinutes<=1){
                convertToMinutes=1;
            }
        }else{
            convertToMinutes=1;
        }

        return (convertToMinutes);
    }

    public static boolean isValidPhonemaxlength(String value) {
        //TODO: Replace this with your own logic
        if (value.length()>=5&& value.length()<=16){
            return true;
        }else {
            return false;
        }
    }
    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ApplicationController.getmAppcontext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        Long currentTime =  System.currentTimeMillis();
        String timea=  formateLongToOnlyDateForServer(currentTime);
        LoggingOperations.writeToFile(ApplicationController.getmAppcontext(),"NETWORK STATUS "+ timea +" "+ activeNetworkInfo);
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public static String removeDashes(String c)
    {
        String dashless = c.replaceAll("\\-","" );

        return dashless;
    }

    public static void fdLogout(Context context){
        Intent intent = new Intent(context, VerificationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }
    public static void fdBlockedScreen(Context context){
        Intent intent = new Intent(context, BlockedFreeDriveActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }
    public static long getStartOfDayInMillis(String date)  {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(format.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }


    public static long getEndOfDayInMillis(String date){
        // Add one day's time to the beginning of the day.
        // 24 hours * 60 minutes * 60 seconds * 1000 milliseconds = 1 day
        return getStartOfDayInMillis(date) + (24 * 60 * 60 * 1000);
    }
    public static String getStartEndOFWeek(int enterWeek, int enterYear,boolean isfirst){
        //enterWeek is week number
        // enterYear is year
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.WEEK_OF_YEAR, enterWeek);
        calendar.set(Calendar.DAY_OF_WEEK,calendar.MONDAY);
        calendar.set(Calendar.YEAR, enterYear);


        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); // PST`
        Date startDate = calendar.getTime();
        String startDateInStr = formatter.format(startDate);


        Log.e("startDateInStr",startDateInStr);

        calendar.add(Calendar.DATE, 6);
        Date enddate = calendar.getTime();

        String endDaString = formatter.format(enddate);

        Log.e("endDaString",endDaString);
        if(isfirst){
            return startDateInStr;
        }else{
            return endDaString;
        }
    }

    //1 minute = 60 seconds
    //1 hour = 60 x 60 = 3600
    //1 day = 3600 x 24 = 86400
    public static long demomodetotalDays(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();


        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        return elapsedDays;
    }

    public static long totaldaysDemoMode(Context context){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calander = Calendar.getInstance();
        String endingDate = simpleDateFormat.format(calander.getTime());
        long totalDays = 0;
        try {
            RideBDD rideBDD = new RideBDD(context);
            rideBDD.open();
            String firstrideDate = rideBDD.getfirstridedateforDemoMode();
            Date startingDays = simpleDateFormat.parse(firstrideDate);
            Date currentdateDays = simpleDateFormat.parse(endingDate);

            totalDays = AppUtils.demomodetotalDays(startingDays,currentdateDays);
            Log.e("totalDays",":"+totalDays);
            rideBDD.close();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return totalDays;
    }
    public static void demoModeSubscription(Context context) {
        if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE) == true) {
            RideBDD tmp = new RideBDD(context);
            tmp.open();
            demototalRides = tmp.sumDemoRidesCount();
            totalDaysDemoMode = AppUtils.totaldaysDemoMode(context);
            if (demototalRides >= 40 || totalDaysDemoMode >= 30) {
                DataHandler.updatePreferences(AppConstants.DEMO_MODE_SUBSCRIPTION_EXPIRED,true);
            }
            tmp.close();
        }
    }
    /*public static void resetallflagsValue(){
        speedlessCheck = false;
        DataHandler.updatePreferences(AppConstants.RIDE_WAS_ACTIVE,false);
    }*/
    /*public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String todayDate = dateFormat.format(date);
        return todayDate;
    }*/


    /*    [string insertString:@"-" atIndex:8];
        [string insertString:@"-" atIndex:13];
        [string insertString:@"-" atIndex:18];
        [string insertString:@"-" atIndex:23];*/
    // 1089afe8-c1d3-11e6-a4a6-cec0c932ce01
    //  1089afe8c1d311e6a4a6cec0c932ce01
    public static String addDashes(String c) {
        String one = c.substring(0, 8);
        String two = c.substring(8, 12);
        String three = c.substring(12, 16);
        String four = c.substring(16, 20);
        String five = c.substring(20, 32);
        String dashed = one + "-" + two + "-" + three + "-" + four + "-" + five;
        return dashed;
    }
    /* Method checks if the app is in background or not
  */
    /*public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }*/
}
