package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.FleetStatisticsAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.FleetStatsBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.txusballesteros.widgets.FitChartValue;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.FleetStatisticsFragments.fleetStatisticsAdapter;

import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.FleetStatisticsFragments.fleet_name;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.FleetStatisticsFragments.fleet_percentage;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.FleetStatisticsFragments.fleetfitChart;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.FleetStatisticsFragments.listViewFleetStatistics;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.FleetStatisticsFragments.progressBarDialogFleetStats;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.FleetStatisticsFragments.statsBeansArrayList;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.FleetStatisticsFragments.total_fleet_avgscore;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.FleetStatisticsFragments.tv_nofleetdata;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.fdLogout;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler.FILE_NAME_SHARED_PREF;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler.deletePreference;

/**
 * Created by Yasir Barefoot on 8/30/2017.
 */

public class GetFleetStatisticsAsyncTask extends BaseAsyncTask {
    public static int company_total_driver = 0;


    public GetFleetStatisticsAsyncTask(Context context, String route, List<NameValuePair> pp) {
        super(context, route, pp);
    }


    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s != null){
            JSONArray jsonArray = null;
            int intResponse = Integer.parseInt(response);
            FleetStatsBeans fleetStatsBeans = null;
            FleetStatsBeans fleetBeans = null;
            JSONObject objAccount= new JSONObject();
            //  listViewNotifications.setAdapter(notificationsHistoryAdapter);
            RideBDD tmp = new RideBDD(context);
            try{
                progressBarDialogFleetStats.dismiss();
                progressBarDialogFleetStats=null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }

            switch (intResponse) {
                case 200:

                    try {
                        if (resultat!=null || !resultat.isEmpty()) {

                            objAccount = new JSONObject(resultat);

                            int company_avg = objAccount.getInt("company_avg");
                            String company_name = objAccount.getString("company_name");
                            company_total_driver = objAccount.getInt("company_total_driver");

                            Log.e("company_avg",""+company_avg);
                            Log.e("company_name",""+company_name);
                            /*fleetBeans = new FleetStatsBeans();*/
                            if (objAccount != null) {
                                jsonArray = objAccount.getJSONArray("score");
                                statsBeansArrayList = null;
                                statsBeansArrayList = new ArrayList<>();
                            if (jsonArray.length() > 0) {
                                tv_nofleetdata.setVisibility(View.INVISIBLE);
                                for (int i =0; i<jsonArray.length(); i++) {

                             /*        for (int i = 0;i<jsonArray.length();i++) {*/
                                    fleetStatsBeans = new FleetStatsBeans();

                                    objAccount = jsonArray.getJSONObject(i);

                                    fleetStatsBeans.setSafety_score(objAccount.getInt("avg_score"));
                                   /* fleetStatsBeans.setTotal_drivetime(objAccount.getInt("total_time"));*/
                                    fleetStatsBeans.setNo_of_drivers(objAccount.getInt("total_rides"));
                                   /* fleetStatsBeans.setNo_of_peeks(objAccount.getInt("total_bad_count"));*/
                                    fleetStatsBeans.setFleet_drivedate(objAccount.getString("departure_date"));
                                    fleetStatsBeans.setFleet_ranking(objAccount.getInt("rank_by_day"));

                                    statsBeansArrayList.add(fleetStatsBeans);
                                }
                            }


                                Log.e("statsBeansArrayList",":"+statsBeansArrayList.size());
                                try{


                                    fleet_name.setText(company_name);
                                    if (statsBeansArrayList.size()>0) {
                                        if (company_avg > 0) {
                                            Collection<FitChartValue> fitChartValues = new ArrayList<>();

                                            if (company_avg >= 80) {
                                                total_fleet_avgscore.setText("" + company_avg + "%");
                                                fleet_percentage.setTextColor(ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartGreen));
                                                total_fleet_avgscore.setTextColor(ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartGreen));
                                                fitChartValues.add(new FitChartValue(company_avg, ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartGreen)));

                                            } else if (company_avg >= 50 && company_avg < 80) {
                                                total_fleet_avgscore.setText("" + company_avg + "%");
                                                fleet_percentage.setTextColor(ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartOrange));
                                                total_fleet_avgscore.setTextColor(ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartOrange));
                                                fitChartValues.add(new FitChartValue(company_avg, ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartOrange)));

                                            } else if (company_avg > 0 && company_avg < 50) {
                                                total_fleet_avgscore.setText("" + company_avg + "%");
                                                fleet_percentage.setTextColor(ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartRed));
                                                total_fleet_avgscore.setTextColor(ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartRed));
                                                fitChartValues.add(new FitChartValue(company_avg, ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartRed)));
                                            }
                                            fleetfitChart.setValues(fitChartValues);
                                        }
                                    } else{
                                        total_fleet_avgscore.setText("100 %");
                                        fleetfitChart.setValue(100);
                                        fleet_percentage.setTextColor(ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartGreen));
                                        total_fleet_avgscore.setTextColor(ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartGreen));
                                    }


                                    /*fleet_percentage.setTextColor(context.getResources().getString(R.color.));*/
                                    fleetStatisticsAdapter = new FleetStatisticsAdapter(statsBeansArrayList);
                                    listViewFleetStatistics.setAdapter(fleetStatisticsAdapter);
                                    fleetStatisticsAdapter.notifyDataSetChanged();

                                    if(jsonArray.length()<=0){
                                        tv_nofleetdata.setText(context.getResources().getString(R.string.fleet_statistics));

                                        tv_nofleetdata.setVisibility(View.VISIBLE);
                                    }
                                }catch (NullPointerException e){
                                    e.printStackTrace();
                                }
                        }
                            try{
                                progressBarDialogFleetStats.dismiss();
                                progressBarDialogFleetStats=null;
                            }catch (NullPointerException exception){
                                exception.printStackTrace();
                            }
                            // DataHandler.updatePreferences(AppConstants.NOTIFICATION_KEY, notificationBeans);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case 400:
                    //Phone numbre not found
                    AlertDialog.Builder error_400 = new AlertDialog.Builder(context);
                    error_400.setMessage(context.getResources().getString(R.string.error_auth_token_expire_400)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deletePreference(FILE_NAME_SHARED_PREF);
                            fdLogout(context);
                            DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY,false);
                            dialog.dismiss();
                        }
                    });
                    error_400.show();
                    break;
                case 500:
                    try{
                        AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                        error_500.setMessage(context.getResources().getString(R.string.error_login_404)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_500.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    try{
                        progressBarDialogFleetStats.dismiss();
                        progressBarDialogFleetStats =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;
            }
            try{
                progressBarDialogFleetStats.dismiss();
                progressBarDialogFleetStats=null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }
        }else {
            try{
                progressBarDialogFleetStats.dismiss();
                progressBarDialogFleetStats=null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }

        }

    }
}
