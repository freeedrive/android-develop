package com.studio.barefoot.freeedrivebeacononlyapp;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.UpdateDemoModeAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class WelcomeDemoModeActivity extends AppCompatActivity {


    private int[] layouts;
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private Button btnSkipIntro;
    public static ProgressBarDialog progressBarDialogDemoMode;
    Button letsGo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.activity_welcome_demo_mode);

        btnSkipIntro = (Button) findViewById(R.id.btn_skipIntro);
        viewPager = (ViewPager) findViewById(R.id.demo_mode_view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);

        letsGo = ((Button) findViewById(R.id.btn_letsgo));
        letsGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBarDialogDemoMode = new ProgressBarDialog(WelcomeDemoModeActivity.this);
                progressBarDialogDemoMode.setTitle(getString(R.string.title_progress_dialog));
                progressBarDialogDemoMode.setMessage(getString(R.string.body_progress_dialog));
                progressBarDialogDemoMode.show();

                List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                mParams.add(new BasicNameValuePair("phone_number", DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
                UpdateDemoModeAsyncTask updateDemoModeAsyncTask = new UpdateDemoModeAsyncTask(WelcomeDemoModeActivity.this, WebServiceConstants.UPDATE_DEMO_MODE, mParams);
                updateDemoModeAsyncTask.execute();
            }
        });



        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = new int[]{
                R.layout.demomode_welcome_slide1,
                R.layout.demomode_welcome_slide2,
                R.layout.demomode_welcome_slide3,
                R.layout.demomode_welcome_slide4};

        // adding bottom dots
        addBottomDots(0);

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        btnSkipIntro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //When This Button Click Go to MainActivity
                 progressBarDialogDemoMode = new ProgressBarDialog(WelcomeDemoModeActivity.this);
                    progressBarDialogDemoMode.setTitle(getString(R.string.title_progress_dialog));
                    progressBarDialogDemoMode.setMessage(getString(R.string.body_progress_dialog));
                    progressBarDialogDemoMode.show();

                    List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                    mParams.add(new BasicNameValuePair("phone_number", DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
                    UpdateDemoModeAsyncTask updateDemoModeAsyncTask = new UpdateDemoModeAsyncTask(WelcomeDemoModeActivity.this, WebServiceConstants.UPDATE_DEMO_MODE, mParams);
                    updateDemoModeAsyncTask.execute();
            }
        });
    }

    private void launchHomeScreen() {
        DataHandler.updatePreferences(AppConstants.IS_FIRST_TIME_LAUNCH,false);
        startActivity(new Intent(WelcomeDemoModeActivity.this, MainActivity.class));
        finish();
    }


    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }



    //	viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            if(position == 3){
               letsGo.setVisibility(View.VISIBLE);
            }else{
                letsGo.setVisibility(View.INVISIBLE);
            }
            // changing the next button text 'NEXT' / 'GOT IT'
           /* if (position == layouts.length - 1) {
                // last page. make button text to GOT IT
                btnNext.setText(getString(R.string.start));
                btnSkip.setVisibility(View.GONE);
            } else {
                // still pages are left
                btnNext.setText(getString(R.string.next));
                btnSkip.setVisibility(View.VISIBLE);
            }*/
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);

            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}
