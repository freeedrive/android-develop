package com.studio.barefoot.freeedrivebeacononlyapp;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.Trace;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/*import com.amitshekhar.DebugDB;*/
import com.amitshekhar.DebugDB;
import com.google.firebase.iid.FirebaseInstanceId;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.MenuAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.MessagesAdapter;

import com.studio.barefoot.freeedrivebeacononlyapp.bottomnavigation.BottomBarHolderActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.bottomnavigation.NavigationPage;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.TurOnGpsDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.fragments.FleetStatisticsFragments;
import com.studio.barefoot.freeedrivebeacononlyapp.fragments.MessagesActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.fragments.MySafetyFragmentGraph;
import com.studio.barefoot.freeedrivebeacononlyapp.fragments.FleetSafetyGraph;
import com.studio.barefoot.freeedrivebeacononlyapp.services.ForegroundLocationService;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.PREF_CURRENT_LATITUDE;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.PREF_CURRENT_LONGTITUDE;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.fdBlockedScreen;

public class MenuActivity extends BottomBarHolderActivity {

    View actionBarView;
    public static DrawerLayout mDrawerLayout;
    public static ListView mMenuList;
    ImageView drawerImage, imageView_thums, action_share_on_newsfeed_via_actionbar;
    private String intentAction = "";
    FragmentTransaction fragmentTransaction;
    FragmentTransaction fragmentTransactionForHamburger;
    private TurOnGpsDialog turOnGpsDialog;


    RelativeLayout layout_behaviour;
    private int backCount = 0;

    private static final String TAG = MainActivity.class.getSimpleName();
    private BottomNavigationView bottomNavigation;
    private Fragment fragment;
    private Fragment fragment_hamburger;
    private FragmentManager fragmentManager;
    private FragmentManager fragmentManager_hamburger;
    private ForegroundLocationService mService = null;
    protected OnBackPressedListener onBackPressedListener;
    private boolean comingFromHamburger = false;
 /*   private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ForegroundLocationService.LocalBinder binder = (ForegroundLocationService.LocalBinder) service;
            mService = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };*/

    public MenuActivity() {
        LocaleUtils.updateConfig(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean fd_active_status_flag;

        turOnGpsDialog = new TurOnGpsDialog(this);
        String DbLog = DebugDB.getAddressLog().toString();
        Log.e("DbLog", DbLog);
       /* if (getIntent().getStringExtra(AppConstants.INTENT_KEY_MENU) != null && !getIntent().getStringExtra(AppConstants.INTENT_KEY_MENU).isEmpty() && getIntent().getStringExtra(AppConstants.INTENT_KEY_MENU).equalsIgnoreCase(AppConstants.INTENT_KEY_MENU)) {
            try {
                fragment_hamburger = new FleetStatisticsFragments();
                fragmentManager_hamburger = this.getSupportFragmentManager();
                fragmentTransactionForHamburger = fragmentManager_hamburger.beginTransaction();
                fragmentTransactionForHamburger.replace(R.id.activity_menu, fragment_hamburger);
                fragmentTransactionForHamburger.commit();
                comingFromHamburger = true;
            } catch (OutOfMemoryError memoryError) {
                memoryError.printStackTrace();
            }

            //finish();
        }*//* bindService(new Intent(this, ForegroundLocationService.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);*/

        // LocaleUtils.updateConfig(this);
        setContentView(R.layout.activity_menu);
        //Log.e("Your are","good to GO");
       /* bottomNavigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        removeShiftMode(bottomNavigation);*/
        /*setupNavigationView();*/

        NavigationPage page1 = new NavigationPage(ContextCompat.getDrawable(this, R.drawable.bottom_tap_icon_phone_safety_blue), new MySafetyFragmentGraph());
        NavigationPage page2 = new NavigationPage(ContextCompat.getDrawable(this, R.drawable.bottom_tap_icon_fleet_grey), new FleetSafetyGraph());
        List<NavigationPage> navigationPages = new ArrayList<>();
        navigationPages.add(page1);
        navigationPages.add(page2);

        super.setupBottomBarHolderActivity(navigationPages);
        //pushFragment(new MySafetyFragmentGraph());

       // actionBarView = getLayoutInflater().inflate(R.layout.custom_toolbarr, null);


        action_share_on_newsfeed_via_actionbar = (ImageView) findViewById(R.id.action_share_on_newsfeed_via_actionbar);

        try {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.e("refreshedToken", refreshedToken);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
         screenSizeInInches();
        /// RegisterRecievers();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerImage = (ImageView) findViewById(R.id.customisedDrawer);
        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                try {
                    if (mDrawerLayout == null) {
                        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
                    } else {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

                    }
                } catch (NullPointerException e) {
                }
                ;


            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });


        mMenuList = (ListView) mDrawerLayout.findViewById(R.id.listViewNotifications);
        /*mMenuList.setDivider(new ColorDrawable(Color.parseColor("#FEFDFD")));
        mMenuList.setDividerHeight(2);*/
        mMenuList.setDividerHeight(0);
        mMenuList.setDivider(null);
        mMenuList.setAdapter(new MenuAdapter(MenuActivity.this));
        mMenuList.setFooterDividersEnabled(false);

        /*if(itemCount.equals(itemCount)){
            if(DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)!=null && DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)==true){
                mMenuList.setDividerHeight(0);
                mMenuList.setDivider(null);
            }
        }*/
        mMenuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("position", "" + position);
                if (position == 0) {
                    Intent i = new Intent(MenuActivity.this, ProfileActivity.class);
                    startActivity(i);
                    /*mMenuList.getDivider().isVisible();*/
                } else if (position == 1) {
                    Intent i = new Intent(MenuActivity.this, MessagesActivity.class);
                    startActivity(i);
                    mDrawerLayout.closeDrawers();
                } else if (position == 2) {
                    fragment_hamburger = new FleetStatisticsFragments();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.main_container, fragment_hamburger);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawers();
                    /*fragment_hamburger = new FleetStatisticsFragments();
                    fragmentManager_hamburger = getSupportFragmentManager();
                    fragmentTransactionForHamburger = fragmentManager_hamburger.beginTransaction();
                    fragmentTransactionForHamburger.replace(R.id.activity_menu, fragment_hamburger);
                    fragmentTransactionForHamburger.commit();
                    comingFromHamburger = true;*/
                    /*Intent i = new Intent(MenuActivity.this, MenuActivity.class);
                    i.putExtra(AppConstants.INTENT_KEY_MENU, AppConstants.INTENT_KEY_MENU);
                    startActivity(i);*/

                } else if (position == 3) {
                   /* if (turOnGpsDialog!=null&&turOnGpsDialog.isShowing()){
                        return;
                    }
                    else{
                        turOnGpsDialog.show();
                    }*/
                    /*Intent i = new Intent(MenuActivity.this, AutoReplyActivity.class);
                    startActivity(i);*/
                } else if (position == 4) {

                    /*String jsonObjcurrentLocation = DataHandler.getStringPreferences(PREF_CURRENT_LATITUDE);
                    Log.e("jsonObjcurrLocation :", jsonObjcurrentLocation);

                    String latitude = "";
                    String longitude = "";
                    if (jsonObjcurrentLocation != null && !jsonObjcurrentLocation.isEmpty()) {
                        try {
                            JSONObject jsonObject = new JSONObject(jsonObjcurrentLocation);
                            *//*jsonObject = jsonObject.getJSONObject("nameValuePairs");*//*
                            Log.e("jsonObject", ":" + jsonObject);
                            if (jsonObject != null) {
                                latitude = jsonObject.getString("Latitude");
                                longitude = jsonObject.getString("Longitude");
                                Log.e("latitude", " : " + latitude);
                                Log.e("longitude", " : " + longitude);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                       *//* double latitude = 33.738045;
                        double longitude = 73.084488;*//*

                        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude + " &mode=w");
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");

                        startActivity(mapIntent);

                *//*if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }*//*

                    }*/
                    Intent i = new Intent(MenuActivity.this, FindMyVehicleActivity.class);
                    startActivity(i);/*else {
                        Intent i = new Intent(MenuActivity.this, FindMyVehicleActivity.class);
                        startActivity(i);
                    }*/

                }
                else if (position == 5) {
                    if(DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)!=null && DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)==true){
                        Intent i = new Intent(MenuActivity.this, OrderBeaconsActivity.class);
                        startActivity(i);
                    }
                } else if (position == 6) {
                    Intent i = new Intent(MenuActivity.this, FaqActivity.class);
                    startActivity(i);
                } else if (position == 7) {
                    Intent i = new Intent(MenuActivity.this, ContactUsActivity.class);
                    i.putExtra("accountMode", true);
                    startActivity(i);
                } else if (position == 8) {
                    Intent i = new Intent(MenuActivity.this, AccidentHelpActivity.class);
                    startActivity(i);                /* Intent i = new Intent(MenuActivity.this, FaqActivity.class);
                                    startActivity(i);*/
                }
                else if (position == 9) {
                                   /* Intent i = new Intent(MenuActivity.this, FaqActivity.class);
                                    startActivity(i);*/
                }

            }
        });
        // Get the ActionBar here to configure the way it behaves.

        // disable the default title element here (for centered title)

        setupActionBar();

        drawerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (!mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {


                        mDrawerLayout.openDrawer(Gravity.RIGHT);
                        mMenuList.setAdapter(new MenuAdapter(MenuActivity.this));
                    } else
                        mDrawerLayout.closeDrawers();
                } catch (Exception e) {
                    Log.e("Exception Menu Drawer", "" + e);
                }
//                mService.requestLocationUpdates();

            }
        });

        try {
            if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY)) {
                boolean intent_action = getIntent().getAction().equalsIgnoreCase("M");
                String intent = getIntent().getAction().toString();
                if (intent.equalsIgnoreCase("M")) {
                    Intent intent1 = new Intent(MenuActivity.this, MessagesActivity.class);
                    startActivity(intent1);
                }

            /*else if(intent.equalsIgnoreCase("T")){
                setupNavigationViewForPushNotification();
                pushFragment(new MyDailyStatsFragment());
            }*/
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            String intent_action = getIntent().getStringExtra("PushNotification");
            if (intent_action.equalsIgnoreCase("M")) {
                Intent intent1 = new Intent(MenuActivity.this, MessagesActivity.class);
                startActivity(intent1);
                //pushFragment(new MessagesActivity());
            }
            /*else if(intent_action.equalsIgnoreCase("T")){
                //Toast.makeText(this, "Fuck this shit", Toast.LENGTH_SHORT).show();
                setupNavigationViewForPushNotification();
                pushFragment(new MyDailyStatsFragment());
            }*/

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        action_share_on_newsfeed_via_actionbar.setImageResource(R.drawable.logo_demomode);
    }

    //Delete application cache
    public void deleteCache(Context context) {
        try {
            File cache = getCacheDir();
            File appDir = new File(cache.getParent());
            if (appDir.exists()) {
                String[] children = appDir.list();
                for (String s : children) {
                    if (!s.equals("lib")) {
                        deleteDir(new File(appDir, s));
                        Log.i("TAG", "File /data/data/APP_PACKAGE/" + s + " DELETED");
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public interface OnBackPressedListener {
        void doBack();
    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }


    //Disable the shiftmode of icon in Botton Navigation of Fragement
    public void removeShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("ERROR NO SUCH FIELD", "Unable to get shift mode field");
        } catch (IllegalAccessException e) {
            Log.e("ERROR ILLEGAL ALG", "Unable to change value of shift mode");
        }
    }


    /**
     * This method allows to close menu after the choice of a Bluetooth
     */
    public void closeD() {
        if (mDrawerLayout != null && mDrawerLayout.isShown()) {
            mDrawerLayout.closeDrawers();
        }
    }

    private boolean isNotificationManager() throws Exception {
        ContentResolver contentResolver = getContentResolver();
        String enabledNotificationListeners = Settings.Secure.getString(contentResolver, "enabled_notification_listeners");
        //Log.e("FD","enabledNotificationListeners: " + enabledNotificationListeners);
        String packageName = getPackageName();
        return enabledNotificationListeners != null && enabledNotificationListeners.contains(packageName);
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }



   /* private void setupNavigationViewForPushNotification(){

        if(bottomNavigation !=null){
            // Select first menu item by default and show Fragment accordingly.
            bottomNavigation.getMenu().getItem(0).setChecked(false);
            Menu menu = bottomNavigation.getMenu();

            if(intentAction.equalsIgnoreCase("M")){
            selectFragment(menu.getItem(3));
            bottomNavigation.getMenu().getItem(3).setChecked(true);
            }
            if(intentAction.equalsIgnoreCase("T")){
                selectFragment(menu.getItem(2));
                bottomNavigation.getMenu().getItem(2).setChecked(true);
            }
            //removeShiftMode((BottomNavigationView) bottomNavigation.getMenu().getItem(3));

            // Set action to perform when any menu-item is selected.
            bottomNavigation.setOnNavigationItemSelectedListener(
                    new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                            if(intentAction.equalsIgnoreCase("M")){

                                bottomNavigation.getMenu().getItem(0).setChecked(true);
                                selectFragment(item);
                                bottomNavigation.getMenu().getItem(3).setChecked(false);
                            }
                            if(intentAction.equalsIgnoreCase("T")){
                                bottomNavigation.getMenu().getItem(0).setChecked(true);
                                selectFragment(item);
                                bottomNavigation.getMenu().getItem(2).setChecked(false);
                            }

                            return true;
                        }
                    });

        }
    }*/

    public void backup() {
        try {
            File sdcard = Environment.getExternalStorageDirectory();
            File outputFile = new File(sdcard,
                    "FD.sql");

            if (!outputFile.exists())
                outputFile.createNewFile();

            File data = Environment.getDataDirectory();
            File inputFile = new File(data, "data/com.studio.barefoot.freeedrivebeacononlyapp/databases/freeedriveV2.db");
            InputStream input = new FileInputStream(inputFile);
            OutputStream output = new FileOutputStream(outputFile);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
            output.flush();
            output.close();
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new Error("Copying Failed");
        }
    }

    private void setupNavigationView() {

        if (bottomNavigation != null) {
            // Select first menu item by default and show Fragment accordingly.
            Menu menu = bottomNavigation.getMenu();
            if (comingFromHamburger == false) {
                selectFragment(menu.getItem(0));
            }


            // Set action to perform when any menu-item is selected.
            bottomNavigation.setOnNavigationItemSelectedListener(
                    new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                            selectFragment(item);
                            return true;
                        }
                    });

        }
    }

    /**
     * Perform action when any item is selected.
     *
     * @param item Item that is selected.
     */
    protected void selectFragment(MenuItem item) {

        //item.setChecked(true);

        switch (item.getItemId()) {

            case R.id.action_graph:
                pushFragment(new MySafetyFragmentGraph());
                break;
            case R.id.action_trips:
                pushFragment(new FleetSafetyGraph());
                break;
        }
    }

    /**
     * Method to push any fragment into given id.
     *
     * @param fragment An instance of Fragment to show into the given id.
     */
    protected void pushFragment(Fragment fragment) {
        if (fragment == null)
            return;

        fragmentManager = getSupportFragmentManager();
        if (fragmentManager != null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (ft != null) {
                ft.replace(R.id.main_container, fragment);
                ft.commit();
            }
        }
    }


    @Override
    public void onBackPressed() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            Trace.endSection();
        }
        if (onBackPressedListener != null && MessagesAdapter.isToggled) {
            onBackPressedListener.doBack();

        } else {


            closeD();
            backCount++;
            if (backCount == 2) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                startActivity(intent);
                moveTaskToBack(true);
                finish();
                backCount = 0;
            }
            //setResult(RESULT_OK);
        }
    }
    public void screenSizeInInches(){
        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width=dm.widthPixels;
        int height=dm.heightPixels;
        float dpHeight = dm.heightPixels / dm.density;

        double wi=(double)width/(double)dm.xdpi;
        double hi=(double)height/(double)dm.ydpi;
        double x = Math.pow(wi,2);
        double y = Math.pow(hi,2);
        double screenInches = Math.sqrt(x+y);
        Configuration configuration = this.getResources().getConfiguration();
        int screenheight = configuration.screenHeightDp; //The current width of the available screen space, in dp units, corresponding to screen width resource qualifier.
        Log.e("dpHeight"," H "+screenheight);

        Log.e("width"," H "+width);
        Log.e("height"," H "+height);
        int heightDif =  height/screenheight;
        Log.e("heightDif",""+heightDif);
        double pixes_difference = 0.0f;
        int cleaned_diff = screenheight-500;

        DataHandler.updatePreferences(AppConstants.PIXELS_DIFFERENCE,cleaned_diff);
        Log.e("ScreenSize",""+screenInches+" round"+Math.round(screenInches)+" floor" +Math.floor(screenInches)+" ceiling "+ Math.ceil(screenInches) +" format" +String.format(Locale.ENGLISH,"%.1f",screenInches));
        DataHandler.updatePreferences(AppConstants.PREF_KEY_SCREEN_SIZE,Double.valueOf(String.format(Locale.ENGLISH,"%.1f",screenInches)));
    }
    public int pxToDp(int px) {
        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onBackPressedListener = null;
        mDrawerLayout = null;
    }
}
