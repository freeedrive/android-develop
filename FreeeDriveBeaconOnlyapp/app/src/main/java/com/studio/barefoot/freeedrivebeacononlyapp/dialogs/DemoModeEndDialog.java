package com.studio.barefoot.freeedrivebeacononlyapp.dialogs;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.studio.barefoot.freeedrivebeacononlyapp.MenuActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.OrderBeaconsActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

/**
 * A simple {@link Fragment} subclass.
 */
public class DemoModeEndDialog extends DialogFragment {
    ImageView cancel_dialog;
    Button btn_orderBeacons;
    public DemoModeEndDialog() {
        // Required empty public constructor
    }
    public static DemoModeEndDialog newInstance(String title) {
        DemoModeEndDialog frag = new DemoModeEndDialog();

        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.custom_demo_mode_end_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cancel_dialog = (ImageView) view.findViewById(R.id.cancel_image);
        btn_orderBeacons = (Button) view.findViewById(R.id.btn_request_beacon);
        cancel_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        btn_orderBeacons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)!=null && DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)==true){
                    Intent i = new Intent(getActivity(), OrderBeaconsActivity.class);
                    startActivity(i);
                    dismiss();
                }
            }
        });

    }
}
