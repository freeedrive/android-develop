package com.studio.barefoot.freeedrivebeacononlyapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.LoginBean;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.PrivacyDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.PrivacyDialogNew;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.SuccessDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class VerificationActivity extends AppCompatActivity {
    Button imageViewLogin, imageViewRegister;
    private LoginBean loginBean;
    private String TAG = "tag";
    public int REQUEST_CODE = 007;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    View actionBarView;
    PrivacyDialog privacyDialog;
    public VerificationActivity() {
        LocaleUtils.updateConfig(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        imageViewLogin = (Button) findViewById(R.id.btn_Login);
        imageViewRegister = (Button) findViewById(R.id.btn_Register);
        actionBarView = getLayoutInflater().inflate(R.layout.custom_toolbarr, null);
        setupActionBar();
        //Spoon.screenshot(this, "VerficationActivity", VerificationActivity.class, "oncreate");

     /*   String DbLog = DebugDB.getAddressLog().toString();
        Log.e("DbLog",DbLog);*/
        //for 6.0+
//        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.SEND_SMS, Manifest.permission.CAMERA,Manifest.permission.READ_PHONE_STATE}, 1001);
        if (checkAndRequestPermissions()) {
            imageViewLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   /* SuccessDialog successDialog = new SuccessDialog(VerificationActivity.this);
                    successDialog.show();*/
                  /*  FragmentManager fm = getSupportFragmentManager();
                    PrivacyDialogNew privacyDialogNew = new PrivacyDialogNew().newInstance(VerificationActivity.this);
                    privacyDialogNew.show(fm,"");*/
                    startActivity(new Intent(VerificationActivity.this, NewPhoneNumberActivity.class));

                    //  startActivity(new Intent(VerificationActivity.this, QrCodeActivity.class));
                }
            });
            imageViewRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(VerificationActivity.this, RegisterActivity.class));
                }
            });
        }
    }
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }

    // Method to request and check for permissions from  user.
    private boolean checkAndRequestPermissions() {
        try {
            int camerapermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
            int permissionLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            int permissioncoarseLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            int permissionsendSms = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);
            int permissionreadphoneState = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            int permissionexternalStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int permissioncallphone = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
            List<String> listPermissionsNeeded = new ArrayList<>();
            if (camerapermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (permissioncoarseLocation != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add((Manifest.permission.ACCESS_COARSE_LOCATION));
            }
            if (permissionsendSms != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
            }
            if (permissionreadphoneState != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
            }
            if (permissionexternalStorage != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (permissioncallphone != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CALL_PHONE);
            }
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
                return false;
            }

        }catch (Exception e){

        }
        return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e(TAG, "Permission callBack Called");
        try{


        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> permission = new HashMap<>();
                permission.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                permission.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                permission.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                permission.put(Manifest.permission.SEND_SMS, PackageManager.PERMISSION_GRANTED);
                permission.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                permission.put(Manifest.permission.CALL_PHONE,PackageManager.PERMISSION_GRANTED);
                permission.put(Manifest.permission.READ_PHONE_STATE,PackageManager.PERMISSION_GRANTED);
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        permission.put(permissions[i], grantResults[i]);
                    //check for all permission
                    if (permission.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && permission.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && permission.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && permission.get(Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED && permission.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED && permission.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && permission.get(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                        imageViewLogin.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(VerificationActivity.this, NewPhoneNumberActivity.class));
                            }
                        });
                        imageViewRegister.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(VerificationActivity.this, RegisterActivity.class));
                            }
                        });
                    } else {
                        Log.e(TAG, "Permission are not granted");
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                            showDialogOK("We Need Camera Permission for Scan the UUID", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    switch (i) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkAndRequestPermissions();
                                            break;
//                                        case DialogInterface.BUTTON_NEGATIVE:
//                                            finish();
//                                            break;
                                    }
                                }
                            });
                        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                            showDialogOK("We Need Location Permission for Calculating the Speed throug GPS", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    switch (i) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkAndRequestPermissions();
                                            break;
//                                        case DialogInterface.BUTTON_NEGATIVE:
//                                            finish();
                                    }
                                }
                            });
                        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                            showDialogOK("We Need Location Permission for Calculating the Speed throug GPS", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    switch (i) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkAndRequestPermissions();
                                            break;
//                                        case DialogInterface.BUTTON_NEGATIVE:
//                                            finish();
                                    }
                                }
                            });
                        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS)) {
                            showDialogOK("We Need Send Message Permission for sending a AutoReply ", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    switch (i) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkAndRequestPermissions();
                                            break;
//                                        case DialogInterface.BUTTON_NEGATIVE:
//                                            finish();
                                    }
                                }
                            });
                        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)) {
                            showDialogOK("We Need Read Phone State Permission for getting Device ID", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    switch (i) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkAndRequestPermissions();
                                            break;
//                                        case DialogInterface.BUTTON_NEGATIVE:
//                                            finish();
                                    }
                                }
                            });
                        }
                        else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            showDialogOK("We Need Write External Storage Permission for creating the File", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    switch (i) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkAndRequestPermissions();
                                            break;
//                                        case DialogInterface.BUTTON_NEGATIVE:
//                                            finish();
                                    }
                                }
                            });
                        }else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {
                            showDialogOK("We Need Call Phone Permission for making a Call in the Case of Accident Help", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    switch (i) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkAndRequestPermissions();
                                            break;
//                                        case DialogInterface.BUTTON_NEGATIVE:
//                                            finish();
                                    }
                                }
                            });
                        }
                        else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                        }
                    }
                }
            }
        }
        }catch (Exception e){
            Log.e(TAG, "onRequestPermissionsResult: ",e );
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && checkAndRequestPermissions()) {
            imageViewLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(VerificationActivity.this, NewPhoneNumberActivity.class));
                }
            });
            imageViewRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(VerificationActivity.this, RegisterActivity.class));
                }
            });
        } else {
            checkAndRequestPermissions();
            //Toast.makeText(this, "Some thing went wrong", Toast.LENGTH_SHORT).show();
        }
    }
    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", okListener)
                .create()
                .show();
    }
    private void explain(String msg) {
        final android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(this);
        dialog.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        //  permissionsclass.requestPermission(type,code);
                        Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:com.studio.barefoot.freeedrivebeacononlyapp"));
                        startActivityForResult(intent, REQUEST_CODE);
                        //startActivityForResult(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:com.studio.barefoot.freeedrivebeacononlyapp")));
                    }
                });
//                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
//                        finish();
//                    }
//                });
        dialog.show();
    }
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    @Override
    public void onBackPressed() {
       /* if(privacyDialog.isShowing()){
            privacyDialog.dismiss();
        }*/
        //setResult(RESULT_OK);
    }
}