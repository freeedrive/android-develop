package com.studio.barefoot.freeedrivebeacononlyapp.dialogs;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.RegisterAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.RegisterActivity.mParams;

/**
 * Created by macbookpro on 8/24/17.
 */

public class PrivacyDialogNew extends DialogFragment {
     Button accept_button;
     TextView tv_privacy_Link,tv_privacy_desc;
     Context context;
    public PrivacyDialogNew() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }
    public static PrivacyDialogNew newInstance(Context context) {
        context = context;
        PrivacyDialogNew frag = new PrivacyDialogNew();
        return frag;
    }
    public static PrivacyDialogNew newInstance(Context context,List<NameValuePair> mParams) {
        mParams = mParams;
       context = context;
        PrivacyDialogNew frag = new PrivacyDialogNew();
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.custom_dialog_privacy, container);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        accept_button = (Button) view.findViewById(R.id.img_accept_privacy);
        tv_privacy_Link = (TextView) view.findViewById(R.id.tv_text_privacy_link) ;
        tv_privacy_desc = (TextView) view.findViewById(R.id.tv_text_privacy_2);
        tv_privacy_desc.setMovementMethod(LinkMovementMethod.getInstance());

        tv_privacy_desc.setLinksClickable(true);
        accept_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                dismissAllowingStateLoss();
                DataHandler.updatePreferences(AppConstants.USER_ACCEPTED_PRIVACY,true);
                Log.e("PARAMS>", "" + mParams);
                RegisterAsyncTask registerAsyncTask = new RegisterAsyncTask(getContext(), WebServiceConstants.END_POINT_REGISTER, mParams);
                registerAsyncTask.execute();
            }
        });


        tv_privacy_Link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/privacy"));
                intentBrowser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ApplicationController.getmAppcontext().startActivity(intentBrowser);
            }
        });

        // Get field from view
/*
        mEditText = (EditText) view.findViewById(R.id.txt_your_name);
*/
        // Fetch arguments from bundle and set title
/*
        String title = getArguments().getString("title", "Enter Name");
*/
        // Show soft keyboard automatically and request focus to field
  /*      mEditText.requestFocus();*/

    }
}
