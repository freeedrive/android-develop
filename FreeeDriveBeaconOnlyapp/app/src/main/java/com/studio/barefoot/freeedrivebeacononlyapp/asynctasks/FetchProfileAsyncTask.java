package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.studio.barefoot.freeedrivebeacononlyapp.BlockedFreeDriveActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.OrderBeaconsActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.ProfileActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.ProfileActivity.progressBarDialogProfile;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.PREF_KEY_FD_STATUS;

/**
 * Created by mcs on 12/28/2016.
 */

public class FetchProfileAsyncTask extends BaseAsyncTask {


    private JSONArray jsonArray;
    private String messenger = "";
    public boolean orderBeacon;

    public FetchProfileAsyncTask(Context context, String route, List<NameValuePair> pp) {
        super(context, route, pp);
    }
    public FetchProfileAsyncTask(Context context, String route, List<NameValuePair> pp,Boolean orderBeacons) {
        super(context, route, pp,orderBeacons);
        orderBeacon = orderBeacons;
    }

    /**
     * AsyncTask method basic calls during a request, calls the parent's method.
     */
    protected String doInBackground(String... params) {

        return "";
    }

    /**
     * AsyncTask method basic calls after a request.
     */
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (s != null) {
            int intResponse = Integer.parseInt(response);
            Log.e("ResponseCode", "" + intResponse);
            Log.e("response", "" + response);
            Log.e("resultat", "" + resultat);
           /* try{
                progressBarDialogProfile.dismiss();
                progressBarDialogProfile = null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }*/

            boolean fd_active_status_flag = false;
            JSONObject jsonObject = new JSONObject();

            switch (intResponse) {
                case 200:
                    try {
                        if (resultat != null || !resultat.isEmpty()) {
                            JSONObject objAccount = new JSONObject(resultat);
                            for (int i = 0; i < objAccount.length(); i++) {


                                if (jsonObject != null) {
                                    jsonObject.put("first_name", objAccount.getString("first_name"));
                                    jsonObject.put("last_name", objAccount.getString("last_name"));
                                    jsonObject.put("email", objAccount.getString("email"));
                                    jsonObject.put("phone_number", objAccount.getString("phone_number"));
                                    jsonObject.put("lang", objAccount.getString("lang"));
                                    jsonObject.put("uuid", objAccount.getString("uuid"));
                                    /*jsonObject.put("private_hour_enable", objAccount.getInt("private_hour_enable"));*/
                                    jsonObject.put("DrivePad",objAccount.getString("DrivePad"));
                                    String drivePad = objAccount.getString("DrivePad");
                                    Log.e("drivePad",drivePad);
                                    /*int private_hour_enable_bit = objAccount.getInt("private_hour_enable");*/
                                    fd_active_status_flag = objAccount.getBoolean("active");

                                }
                                if (fd_active_status_flag == false) {
                                    DataHandler.updatePreferences(PREF_KEY_FD_STATUS, fd_active_status_flag);
                                    Intent intent = new Intent(ApplicationController.getmAppcontext(), BlockedFreeDriveActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    ApplicationController.getmAppcontext().startActivity(intent);
                                }
                            }
                            //DataHandler.updatePreferences(PREF_KEY_FD_STATUS,fd_active_status_flag);
                            DataHandler.updatePreferences(AppConstants.TEMP_PROFILE_KEY, jsonObject.toString());
                           if (!orderBeacon){
                               Intent intentProfile = new Intent(context, ProfileActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                               intentProfile.putExtra("comingFromNetwork", "1");
                               context.startActivity(intentProfile);
                           }else{
                               Intent intentOrderBeacon = new Intent(context, OrderBeaconsActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                               context.startActivity(intentOrderBeacon);
                           }

                            try {
                                progressBarDialogProfile.dismiss();
                                progressBarDialogProfile = null;
                            } catch (NullPointerException exception) {
                                exception.printStackTrace();
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                /*case 200 :

                    break;
                case 409:

                    break;
                case 401:
                    //Number already exists
                    AlertDialog.Builder error_401 = new AlertDialog.Builder(context);
                    error_401.setMessage(context.getResources().getString(R.string.error_login_401)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_401.show();
                case 403:
                    AlertDialog.Builder error_422 = new AlertDialog.Builder(context);
                    error_422.setMessage(context.getResources().getString(R.string.error_login_403)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_422.show();
                    break;*/
                case 500:
                    AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                    error_500.setMessage(context.getResources().getString(R.string.error_login_404)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_500.show();
                    try {
                        progressBarDialogProfile.dismiss();
                        progressBarDialogProfile = null;
                    } catch (NullPointerException exception) {
                        exception.printStackTrace();
                    }
                    break;
            }
        } else {
            try {
                progressBarDialogProfile.dismiss();
                progressBarDialogProfile = null;
            } catch (NullPointerException exception) {
                exception.printStackTrace();
            }
        }
    }
}
