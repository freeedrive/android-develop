package com.studio.barefoot.freeedrivebeacononlyapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.RidesBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.txusballesteros.widgets.FitChart;
import com.txusballesteros.widgets.FitChartValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by macbookpro on 4/26/17.
 */

public class PhoneSafetyAdapter extends RecyclerView.Adapter<PhoneSafetyAdapter.MyViewHolder> {


    private ArrayList<RidesBeans> ridesBeansList;
    Context context;
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.phone_safety, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RidesBeans ridesBeans = ridesBeansList.get(position);
        int dailystatseachrideScore = 0;
        String startTimeOfRide = AppUtils.formate10LongDateToDisplay(ridesBeans.getDeparture_time());
        int elapsedTime = (int) ridesBeans.getTime_elapsed();
        int distractionsOfEachRide = ridesBeans.getBad_behaviour();
        int scoreofEachRide = (int) ridesBeans.getScore();
        Log.e("scoreofEachRide"," : "+scoreofEachRide);

        holder.startridetime.setText(startTimeOfRide);
        holder.elapsedTime.setText(elapsedTime+" mn ");

        Collection<FitChartValue> fitChartValues = new ArrayList<>();
        if(distractionsOfEachRide <5){
            holder.distractions.setText(""+distractionsOfEachRide);
            holder.distractions.setTextColor(context.getResources().getColor(R.color.colorPieChartGreen));


        }
        else if(distractionsOfEachRide >=5 && distractionsOfEachRide <10){
            holder.distractions.setText(""+distractionsOfEachRide);
            holder.distractions.setTextColor(context.getResources().getColor(R.color.colorPieChartOrange));


        }
        else{
            holder.distractions.setText(""+distractionsOfEachRide);
            holder.distractions.setTextColor(context.getResources().getColor(R.color.colorPieChartRed));
        }
        if(scoreofEachRide >=80){
           // holder.fitChartLastRideScore.setValue(scoreofEachRide);
            holder.dailystatsscore.setText(""+scoreofEachRide);
            holder.dailystatsscore.setTextColor(context.getResources().getColor(R.color.colorPieChartGreen));
            holder.daily_stats_score_last_ride_percentage.setTextColor(context.getResources().getColor(R.color.colorPieChartGreen));
            fitChartValues.add(new FitChartValue(scoreofEachRide, context.getResources().getColor(R.color.colorPieChartGreen)));
        }
        else if(scoreofEachRide >= 50 && scoreofEachRide < 80){

            //holder.fitChartLastRideScore.setValue(scoreofEachRide);
            holder.dailystatsscore.setText(""+scoreofEachRide);
            holder.dailystatsscore.setTextColor(context.getResources().getColor(R.color.colorPieChartOrange));
            holder.daily_stats_score_last_ride_percentage.setTextColor(context.getResources().getColor(R.color.colorPieChartOrange));
            fitChartValues.add(new FitChartValue(scoreofEachRide, context.getResources().getColor(R.color.colorPieChartOrange)));
        }
        else {
            //holder.fitChartLastRideScore.setValue(scoreofEachRide);
            holder.dailystatsscore.setText(""+scoreofEachRide);
            holder.dailystatsscore.setTextColor(context.getResources().getColor(R.color.colorPieChartRed));
            holder.daily_stats_score_last_ride_percentage.setTextColor(context.getResources().getColor(R.color.colorPieChartRed));
            fitChartValues.add(new FitChartValue(scoreofEachRide , context.getResources().getColor(R.color.colorPieChartRed)));
        }
        holder.fitChartLastRideScore.setValues(fitChartValues);

    }
    public PhoneSafetyAdapter(ArrayList<RidesBeans> moviesList) {
        this.ridesBeansList = moviesList;
    }
    @Override
    public int getItemCount() {
        return ridesBeansList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView  startridetime,elapsedTime, distractions,dailystatsscore,daily_stats_score_last_ride,daily_stats_score_last_ride_percentage;
        final FitChart fitChartLastRideScore;

        public MyViewHolder(View view) {
            super(view);

            startridetime = (TextView) view.findViewById(R.id.daily_stats_startTime);
            elapsedTime = (TextView) view.findViewById(R.id.daily_stats_elapsedTime);
            distractions = (TextView) view.findViewById(R.id.daily_stats_no_peeks_value);
            dailystatsscore = (TextView) view.findViewById(R.id.daily_stats_score_last_ride);
            daily_stats_score_last_ride = (TextView) view.findViewById(R.id.daily_stats_score_last_ride);
            daily_stats_score_last_ride_percentage = (TextView) view.findViewById(R.id.daily_stats_score_last_ride_percentage);
            fitChartLastRideScore = (FitChart) view.findViewById(R.id.daily_stats_fitchartlastride_score);
        }
    }
}
