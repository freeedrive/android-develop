package com.studio.barefoot.freeedrivebeacononlyapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ttwyf on 1/17/2017.
 */

public class FaqTextListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int HEADER = 0;
    public static final int CHILD = 1;


    private List<Item> data;

    public FaqTextListAdapter(List<Item> data){
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        Context context = parent.getContext();

        float dp = context.getResources().getDisplayMetrics().density;
        int subItemPaddingLeft = (int) (18 * dp);
        int subItemPaddingTopAndBottom = (int) (5 * dp);
        switch (viewType) {
            case HEADER:
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.faq_list_item, parent, false);
                ListHeaderViewHolder header = new ListHeaderViewHolder(view);
                return header;
            case CHILD:
                TextView itemTextView = new TextView(context);
                itemTextView.setPadding(subItemPaddingLeft, subItemPaddingTopAndBottom, 0, subItemPaddingTopAndBottom);
                itemTextView.setTextColor(0x88000000);
                itemTextView.setTextSize(15);
                itemTextView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                return new RecyclerView.ViewHolder(itemTextView) {
                };
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final Item item = data.get(position);
        switch (item.type) {

            case HEADER:
                final ListHeaderViewHolder itemController = (ListHeaderViewHolder) holder;
                itemController.refferalItem = item;
                final SpannableStringBuilder sb = new SpannableStringBuilder(item.text);
                int faq_Count = item.faqCount;
                final StyleSpan bss = new StyleSpan(Typeface.NORMAL); // Span to make text bold
                sb.setSpan(bss, 0, 10, Spannable.SPAN_INCLUSIVE_INCLUSIVE); //
                itemController.header_title.setText(sb);
                itemController.btn_faqCounts.setText(""+faq_Count);
                if (item.invisibleChildren == null) {
                    itemController.btn_expand_toggle.setImageResource(R.drawable.up_arrow);
                }else {
                    itemController.btn_expand_toggle.setImageResource(R.drawable.down_arrow);
                }


                itemController.btn_expand_toggle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (item.invisibleChildren == null) {
                            item.invisibleChildren = new ArrayList<Item>();
                            int count = 0;
                            int pos = data.indexOf(itemController.refferalItem);
                            while (data.size() > pos + 1 && data.get(pos + 1).type == CHILD) {
                                item.invisibleChildren.add(data.remove(pos + 1));
                                count++;
                            }
                            notifyItemRangeRemoved(pos + 1, count);
                            itemController.btn_expand_toggle.setImageResource(R.drawable.down_arrow);
                        }
                        else {
                            int pos = data.indexOf(itemController.refferalItem);
                            int index = pos + 1;
                            for (Item i : item.invisibleChildren) {
                                data.add(index, i);
                                index++;
                            }
                            notifyItemRangeInserted(pos + 1, index - pos - 1);
                            itemController.btn_expand_toggle.setImageResource(R.drawable.up_arrow);
                            item.invisibleChildren = null;
                        }


                    }
                });
                itemController.header_title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (item.invisibleChildren == null) {
                            item.invisibleChildren = new ArrayList<Item>();
                            int count = 0;
                            int pos = data.indexOf(itemController.refferalItem);
                            while (data.size() > pos + 1 && data.get(pos + 1).type == CHILD) {
                                item.invisibleChildren.add(data.remove(pos + 1));
                                count++;
                            }
                            notifyItemRangeRemoved(pos + 1, count);
                            itemController.btn_expand_toggle.setImageResource(R.drawable.down_arrow);
                        } else {
                            int pos = data.indexOf(itemController.refferalItem);
                            int index = pos + 1;
                            for (Item i : item.invisibleChildren) {
                                data.add(index, i);
                                index++;
                            }
                            notifyItemRangeInserted(pos + 1, index - pos - 1);
                            itemController.btn_expand_toggle.setImageResource(R.drawable.up_arrow);
                            item.invisibleChildren = null;
                        }


                    }
                });
                break;
            case CHILD:

                /*if(position == 21){
                    *//*ImageView imageView = (ImageView) holder.itemView;
                    imageView.setImageResource(R.drawable.datacusage);
                    *//**//*itemController.dataUsage.setVisibility(View.VISIBLE);*//**//*
                    *//**//*itemController.dataUsage.setImageResource(R.drawable.datacusage);*//*
                    Toast.makeText(context, "Cell 20 Is Clicked", Toast.LENGTH_SHORT).show();
                }*/
                Log.e("rowPosition","  "+position);
                TextView itemTextView = (TextView) holder.itemView;
                itemTextView.setText(data.get(position).text);
                if(position==2){
                    SpannableString styledString
                            = new SpannableString(data.get(position).text);
                    styledString.setSpan(new UnderlineSpan(),data.get(position).text.toString().length()-14,data.get(position).text.toString().length(),0);
                    styledString.setSpan(new StyleSpan(Typeface.BOLD),data.get(position).text.toString().length()-14,data.get(position).text.toString().length(),0);
                    styledString.setSpan(new ForegroundColorSpan(Color.BLUE), data.get(position).text.toString().length()-14,data.get(position).text.toString().length(), 0);
                    ClickableSpan clickableSpan = new ClickableSpan() {
                        @Override
                        public void onClick(View widget) {
                        Log.e("span","clicked");
                            Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.freeedrive.com/en/privacy")).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            ApplicationController.getmAppcontext().startActivity(intentBrowser);
                        }
                    };
                    styledString.setSpan(clickableSpan,data.get(position).text.toString().length()-14,data.get(position).text.toString().length(),0);
                    itemTextView.setMovementMethod(LinkMovementMethod.getInstance());
                    itemTextView.setText(styledString);
              /*       itemTextView.setOnClickListener(new View.OnClickListener() {
                         @Override
                         public void onClick(View v) {
                             Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.freeedrive.com/en/privacy")).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                             ApplicationController.getmAppcontext().startActivity(intentBrowser);
                         }
                     });*/
                }

                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).type;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private static class ListHeaderViewHolder extends RecyclerView.ViewHolder {
        public TextView header_title;
        public ImageView btn_expand_toggle;
        public Button btn_faqCounts;
        public Item refferalItem;
        public ImageView dataUsage;

        public ListHeaderViewHolder(View itemView) {
            super(itemView);
            try {
                header_title = (TextView) itemView.findViewById(R.id.header_title);
                //header_date = (TextView) itemView.findViewById(R.id.tv_title_notifications);
                btn_faqCounts = (Button) itemView.findViewById(R.id.btn_faq_count);
                btn_expand_toggle = (ImageView) itemView.findViewById(R.id.btn_expand_toggle);
                /*dataUsage = (ImageView) itemView.findViewById(R.id.imageviewdata_usage);*/
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    public static class Item {
        public int type;
        public Boolean b;
        public String text;
        public int faqCount;
        public int rowPosition;
        public List<Item> invisibleChildren;

        public Item() {
        }

        public Item(int type, String text,int faq_count) {
            this.type = type;
            this.faqCount = faq_count;
            this.text = text;
        }

        public Item(int child, String description) {
            this.type = child;
            this.text = description;
        }
    }

    public void animateTo(List<Item> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<Item> newModels) {
        for (int i = data.size() - 1; i >= 0; i--) {
            final Item model = data.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Item> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final Item model = newModels.get(i);
            if (!data.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Item> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final Item model = newModels.get(toPosition);
            final int fromPosition = data.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public Item removeItem(int position) {
        final Item model = data.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, Item model) {
        data.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final Item model = data.remove(fromPosition);
        data.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }


}
