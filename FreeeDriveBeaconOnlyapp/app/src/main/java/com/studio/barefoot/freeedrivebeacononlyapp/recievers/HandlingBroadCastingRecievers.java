package com.studio.barefoot.freeedrivebeacononlyapp.recievers;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Chronometer;

import com.studio.barefoot.freeedrivebeacononlyapp.beans.RidesBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.SubRideBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.sensors.FDsensors;
import com.studio.barefoot.freeedrivebeacononlyapp.services.BackgroundBeaconScan;
import com.studio.barefoot.freeedrivebeacononlyapp.services.Detector;
import com.studio.barefoot.freeedrivebeacononlyapp.services.FDGPSTracker;
import com.studio.barefoot.freeedrivebeacononlyapp.services.Foreground;
import com.studio.barefoot.freeedrivebeacononlyapp.services.ForegroundLocationService;
import com.studio.barefoot.freeedrivebeacononlyapp.services.LocationService;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LoggingOperations;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import static com.studio.barefoot.freeedrivebeacononlyapp.services.BackgroundBeaconScan.beaconenterregionCounter;
import static com.studio.barefoot.freeedrivebeacononlyapp.services.BackgroundBeaconScan.mService;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MySafetyFragmentGraph.rideneverStarted;
import static com.studio.barefoot.freeedrivebeacononlyapp.services.LocationService.accelerate;
import static com.studio.barefoot.freeedrivebeacononlyapp.services.LocationService.braking;

import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.ARRIVAL_LANG;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.ARRIVAL_LAT;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.DEPARTURE_LANG;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.DEPARTURE_LAT;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.PREF_CURRENT_LATITUDE;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.currentTime;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.ispassengerSwitch;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.minutesPassengerSwitchOff;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.subRideBeansArrayList;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.switchCheck;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.totalminutesPassengerMode;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.demoModeSubscription;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.formateLongToOnlyDateForServer;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.passengerMode;

/**
 * Created by mcs on 1/11/2017.
 */

public class HandlingBroadCastingRecievers extends BroadcastReceiver {
    /**
     * Allows to handle database
     */
    private RideBDD rideBDD;
    private RidesBeans rideFromDB;
    private Boolean isAlreadySaved = false;
    private boolean rideEnd;
    BluetoothAdapter bluetoothadapter;
    Context context;
    public static int reason_ride_end = 0;
    private float score=0;
    private int badCount=0;
    public  Timer beaconFd;
    private int demototalRides;
    private long totalDaysDemoMode;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        String action = intent.getAction();
        rideBDD = new RideBDD(context);
        //Reciever to handle beacon advertising of i am in range or out
        //rideEnd = intent.getBooleanExtra("RIDE_END_FLAG",false);
        LoggingOperations logger = new LoggingOperations();
        Thread.setDefaultUncaughtExceptionHandler(logger);
        bluetoothadapter = BluetoothAdapter.getDefaultAdapter();

        Long currentTime_receivers =  System.currentTimeMillis();
        String time_receivers=  formateLongToOnlyDateForServer(currentTime_receivers);
        LoggingOperations.writeToFile(context," INSIDE RECEIVERS > "+ time_receivers +" ACTION "+ action);

        if (action.equalsIgnoreCase("RIDE_END_FLAG")) {
            Long currentTime =  System.currentTimeMillis();
            String timea=  formateLongToOnlyDateForServer(currentTime);
            LoggingOperations.writeToFile(context,"RECIEVERS > "+timea +" RIDE END FLAG RECIEVED where ride was "+DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE));
            if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)){
                AppUtils.startLocService=false;
                try{
                    long totalTimeOver120 = 0;

                    if(LocationService.timeSave.size() == 0){

                        Log.e("No Rides","Size 0");

                    }
                    else{
                        for(int i=0;i<LocationService.timeSave.size();i++){
                            //Current Rides Bean Object
                            RidesBeans rb = (RidesBeans) LocationService.timeSave.get(i);

                            //Time Differene in Current RidesBean Object
                            long timeOver = rb.getEndtime().getTime() - rb.getStarttime().getTime();

                            totalTimeOver120 += timeOver;

                        }
                        LocationService.totalTimeinSeconds = totalTimeOver120 / 1000L;
                        Log.e("currentTime","totaltimeover120 :"+totalTimeOver120);
                        /*ridesBeans = null;

                        Log.e("currentTime","totalTimeinSeconds :"+totalTimeinSeconds);*/
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
                stopFreeeDriveAfter8mins(context);
                // stopFreeeDrive(context);
               /* Intent stopsrvc = new Intent(context, LocationService.class);
                Intent stopsrvc1 = new Intent(context, BackgroundBeaconScan.class);
                context.stopService(stopsrvc);
                context.stopService(stopsrvc1);

                context.startService(stopsrvc1);*/

            }
        }

        if (action.equalsIgnoreCase("IamInRange")) {
            Long currentTime =  System.currentTimeMillis();
            String timea=  formateLongToOnlyDateForServer(currentTime);
            LoggingOperations.writeToFile(context,"RECEIVERS > "+timea + "Ride was active >" + DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE));
            if(DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)==false) {
                DataHandler.deletePreference(PREF_CURRENT_LATITUDE);
                startFreeeDrive(context);
            }
        }
        if (action.equalsIgnoreCase("IamOutOfRange")) {
            if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)){
                AppUtils.startLocService=false;
                long totalTimeOver120 = 0;

                try {
                    if(LocationService.timeSave.size() == 0){
                        Log.e("ArraySize","Is 0");
                    }
                    else{
                        for(int i=0;i<LocationService.timeSave.size();i++){
                            //Current Rides Bean Object
                            RidesBeans rb = (RidesBeans) LocationService.timeSave.get(i);

                            //Time Differene in Current RidesBean Object
                            long timeOver = rb.getEndtime().getTime() - rb.getStarttime().getTime();

                            totalTimeOver120 += timeOver;

                        }
                        LocationService.totalTimeinSeconds = totalTimeOver120 / 1000;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                /*try{
                    long totalTimePassengerMode = 0;
                    if(LocationService.passengerModeTime.size() == 0){

                        Log.e("No Rides","Size 0");

                    }
                    else{
                        for(int i=0;i<LocationService.passengerModeTime.size();i++){
                            //Current Rides Bean Object
                            RidesBeans rb = (RidesBeans) LocationService.passengerModeTime.get(i);

                            //Time Differene in Current RidesBean Object
                            *//*long timeOver = System.currentTimeMillis() - rb.getPassengermodeOn().getTime();*//*
                            long timeOver  = rb.getPassengermodeOff().getTime() - rb.getPassengermodeOn().getTime();

                            totalTimePassengerMode += timeOver;

                        }
                        LocationService.totalminutesPassengerSwitchOn = totalTimePassengerMode / 1000L;
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                }*/
                stopFreeeDrive(context);
            }
        }


        if (action.equalsIgnoreCase("android.location.PROVIDERS_CHANGED")) {
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                //NO GPS
                AppUtils.gpsenabled = false;
                Long currentTime =  System.currentTimeMillis();
                String time=  formateLongToOnlyDateForServer(currentTime);

                LoggingOperations.writeToFile(context,"GPS was turned OFF  > "+time +" -- > "+ "OFF");
                Intent disConnectForegroundServiceIntent = new Intent(context, ForegroundLocationService.class);
                context.stopService(disConnectForegroundServiceIntent);
             /*   if (mService!=null){
                    mService.removeLocationUpdates();
                    mService.stopForeground(true);
                    mService.stopSelf();
                }*/
            } else {
//Gps is on
                Long currentTime =  System.currentTimeMillis();
                String time=  formateLongToOnlyDateForServer(currentTime);

                LoggingOperations.writeToFile(context,"GPS was turned ON  > "+time +" -- > "+ "ON");

                AppUtils.gpsenabled = true;
            }
        }

        if(action.equalsIgnoreCase(BluetoothAdapter.ACTION_STATE_CHANGED)) {

            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    AppUtils.speedlessCheck =true;
                    AppUtils.bluetoothenabled = false;
                    Long currentTime =  System.currentTimeMillis();
                    String time=  formateLongToOnlyDateForServer(currentTime);

                    LoggingOperations.writeToFile(context,"BLUETOOTH > "+time +" -- > "+ "OFF");
                    /*if(DataHandler.getBooleanPreferences(PREF_KEY_APP_TERMINATED)){
                        AppUtils.speedlessCheck = false;
                    }else {
                        DataHandler.updatePreferences(PREF_KEY_APP_TERMINATED, false);
                    }*/

                    // bluetoothadapter.disable();
                    // stopFDSensor();
                    //1 : have to ask
              /*      if (AppConstants.isLocationActive) {
                        Intent stopsrvc = new Intent(context,LocationService.class);
                        context.stopService(stopsrvc);
                    }*/
               /*     Log.e("turnOnBT","turnOnBT");
                    context.stopService(new Intent(context,BackgroundBeaconScan.class));
                    if (AppUtils.rideWasActive) {

                        stopFreeeDrive(context);
                    }*/
                    DataHandler.updatePreferences(AppConstants.IS_BLUETOOTH,false);
              /*      Intent disconnectServiceIntent = new Intent(context, BackgroundBeaconScan.class);
                    disconnectServiceIntent.setAction("Disconnect");
                    context.startService(disconnectServiceIntent);*/

                    AppUtils.IsBluetoothEnabled = false;
                    beaconenterregionCounter = true;
                    if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)){
                        EndRideAfterBToff(context);
                    }else{
                        if (mService!=null){
                            mService.removeLocationUpdates();

                            mService.removeLocationUpdates();
                            mService.stopForeground(true);
                            mService.stopSelf();
                        }
                    }
                    break;
                case BluetoothAdapter.STATE_ON:
                    Long currentTimeON =  System.currentTimeMillis();
                    String timeON=  formateLongToOnlyDateForServer(currentTimeON);
                    LoggingOperations.writeToFile(context,"BLUETOOTH > "+timeON +" -- > "+ "ON");
                    AppUtils.IsBluetoothEnabled = true;

                    DataHandler.updatePreferences(AppConstants.IS_BLUETOOTH,true);


                    if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY) && DataHandler.getBooleanPreferences(AppConstants.IS_BLUETOOTH) && !DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)){
                        context.stopService(new Intent(context, BackgroundBeaconScan.class));

                        Intent serviceIntent = new Intent(context, BackgroundBeaconScan.class);
                        serviceIntent.setAction("Connect");
                        context.startService(serviceIntent);
                        Log.e("Detector","I started beacon service");
                    }
                    if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)){
                     /*   if (beaconFd != null) {
                            beaconFd.cancel();
                            beaconFd =null;
                        }*/
                    }


            }
        }


    }
    private void stopFreeeDriveAfter8mins(Context context){
        try{
            Long currentTime =  System.currentTimeMillis();
            String timea=  formateLongToOnlyDateForServer(currentTime);
            LoggingOperations.writeToFile(context,"RECIEVERS > "+timea +" RIDE ENDED AFTER 8 mins");
            Long arrivaleTIme= System.currentTimeMillis()/1000L;
            DataHandler.updatePreferences(AppConstants.REDUCTED_ARRIVAL,arrivaleTIme);
            //  String time =AppUtils.formateLongToOnlyDateForServer(arrivaleTIme);
            String time =AppUtils.formate10LongDateToDisplay(arrivaleTIme);

            DataHandler.updatePreferences(AppConstants.KEY_ARRIVAL,time);
            stopFDSensor();
            // startFDTrackerGPS();
            reason_ride_end = 1;
            manageStopChronometer(context);
            DataHandler.updatePreferences(AppConstants.RIDE_WAS_ACTIVE,false);
            //  stopFDTrackerGPS();
            AppConstants.currentSpeed = 0;
            subRideBeansArrayList.clear();
            Intent stopsrvc = new Intent(context, LocationService.class);
            context.stopService(stopsrvc);

        }catch (Exception e){
            Log.e("Handling,Exception1",""+e);
        }



    }

    private void InsertSubRides(Context context) {

        RideBDD rideBDD = new RideBDD(context);
        rideBDD.open();
        int id = rideBDD.getRideID();
        if (subRideBeansArrayList != null && !subRideBeansArrayList.isEmpty() && subRideBeansArrayList.size() > 1) {
            for (int i = 0; i < subRideBeansArrayList.size(); i++) {
                SubRideBeans subRideBeans = (SubRideBeans) subRideBeansArrayList.get(i);

                String areaType = subRideBeans.getAreaType();
                String startLat = subRideBeans.getStartlat();
                String startLang = subRideBeans.getStartlng();
                String endLat = subRideBeans.getEndlat();
                String endLang = subRideBeans.getEndlng();
                Long currentTime = subRideBeans.getCurrentTime();
                double currentSpeed = subRideBeans.getCurrentSpeed();
                int timeInsideARegion = subRideBeans.getTimeInsideAregion();
                rideBDD.InsertSubRides(id, areaType, startLat, startLang, endLat, endLang, currentTime, currentSpeed,timeInsideARegion);
            }

        }

        rideBDD.close();
        //
    }

    private void stopFreeeDrive(Context context){
        try{
            Long currentTime =  System.currentTimeMillis();
            String timea=  formateLongToOnlyDateForServer(currentTime);
            LoggingOperations.writeToFile(context,"RECEIVERS > "+timea +" RIDE ENDED");
            Long arrivaleTIme= System.currentTimeMillis()/1000L;
            DataHandler.updatePreferences(AppConstants.REDUCTED_ARRIVAL,arrivaleTIme);
            //  String time =AppUtils.formateLongToOnlyDateForServer(arrivaleTIme);
            String time =AppUtils.formate10LongDateToDisplay(arrivaleTIme);
            DataHandler.updatePreferences(AppConstants.KEY_PASSENGER_MODE, 0);
            DataHandler.updatePreferences(AppConstants.KEY_ARRIVAL,time);
            stopFDSensor();
            //startFDTrackerGPS();
            reason_ride_end = 2;
            manageStopChronometer(context);
            DataHandler.updatePreferences(AppConstants.RIDE_WAS_ACTIVE,false);
            //stopFDTrackerGPS();
            AppConstants.currentSpeed = 0;
            subRideBeansArrayList.clear();
            Intent stopsrvc = new Intent(context, ForegroundLocationService.class);
            context.stopService(stopsrvc);
            if (mService!=null){
                mService.removeLocationUpdates();

                mService.removeLocationUpdates();
                mService.stopForeground(true);
                mService.stopSelf();
            }
            AppConstants.broadcastFlag = true;


            if (beaconFd != null) {
                beaconFd.cancel();
                beaconFd =null;
            }

        }catch (Exception e){
            Long currentTime =  System.currentTimeMillis();
            String timea=  formateLongToOnlyDateForServer(currentTime);

            Log.e("Handling,Exception1",""+e);
            LoggingOperations.writeToFile(context,"RECIEVERS Exception1 > "+timea +e);
        }



    }


    /**
     * Allows to start the tracker Location
     */
    private void startFDTrackerGPS(){
        if(Detector.FdApp.fdgpsTracker == null) {
            Detector.FdApp.fdgpsTracker = new FDGPSTracker(Detector.FdApp);
        }

        Detector.FdApp.fdgpsTracker.startFdTrackerLocation();
    }
    private void startFreeeDrive(Context context){

        Long currentTime =  System.currentTimeMillis();
        String timea=  formateLongToOnlyDateForServer(currentTime);
        LoggingOperations.writeToFile(context,"RECIEVERS > "+timea +" RIDE STARTED");
        Long departureTime= System.currentTimeMillis()/1000L;
        //  String time =AppUtils.formateLongToOnlyDateForServer(arrivaleTIme);
        String time =AppUtils.formate10LongDateToDisplay(departureTime);
        DataHandler.updatePreferences(AppConstants.KEY_DEPARTURE,time);
        DataHandler.updatePreferences(AppConstants.RIDE_WAS_ACTIVE,true);
        LoggingOperations.writeToFile(context,"RECIEVERS > "+timea + "Ride was active >" + DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE));
        //checkIfGpsIsEnabled(context);
        //  startFDTrackerGPS();

        //Test new journey:
        // isNewJourney();
        manageStartChronometer(context);
        startFDSensor();
    }

    /**
     * Allows to stop the tracker Location
     */
    private void stopFDTrackerGPS(){
        Detector.FdApp.fdgpsTracker.stopFdTrackerLocation();
    }

    /**
     * Allows to stop the sensor event
     */
    private void stopFDSensor(){
        Detector.FdApp.fdSensors.stopSensorsFD();
    }
    /**
     * Allows to stop a chronometer for a ride
     * @param c(in), @Activity represents the Activity
     */
    private void manageStopChronometer(Context c) {
        //launch chronometer :
/*        AppUtils.chronometer = new Chronometer(c);
        AppUtils.chronometer.setBase(SystemClock.elapsedRealtime());
        //   FDUtils.chronometer.start();
        AppUtils.chronometer.stop();
        AppUtils.isStarted = true;*/
        ;
        Long arrival =System.currentTimeMillis()/1000L;
        Log.e("Arrival",""+arrival);
        Detector.FdApp.arrival_time = arrival;
        DataHandler.updatePreferences(AppConstants.KEY_ARRIVAL_LONG,arrival);
        Detector.FdApp.fdSensors.journeyTotalTime();
        //database:
        RidesBeans ride = new RidesBeans();

        ride.setDeparture_time(DataHandler.getLongreferences(AppConstants.KEY_DEPARTURE_LONG));
        //To set departure location
        String localityDeparture="";
        if (DataHandler.getDoublePreference(AppConstants.DEPARTURE_LAT)!=null && DataHandler.getDoublePreference(AppConstants.DEPARTURE_LANG)!=null ){
            localityDeparture = addressFromLocation(c, DataHandler.getDoublePreference(DEPARTURE_LAT),DataHandler.getDoublePreference(DEPARTURE_LANG));
            if(localityDeparture.contains(",")){
                try{
                    //create a location from a string from Ride :
                    String[] tabDepartureLocation = localityDeparture.split(",");
                    Location locationTmpRide = new Location("Ride");
                    double lat = Double.parseDouble(tabDepartureLocation[0]);
                    double longi = Double.parseDouble(tabDepartureLocation[1]);
                    locationTmpRide.setLatitude(lat);
                    locationTmpRide.setLongitude(longi);
                    String localityDepartureSplitted = addressFromLocation(c, locationTmpRide);
                    Log.e("LocalityDeparture",localityDeparture);
                    //save the locality :
                    ride.setDeparture_location(localityDepartureSplitted);
                }catch (NumberFormatException fe){
                    fe.printStackTrace();
                    Log.e("E",fe.getMessage());
                }
            }else{
                ride.setDeparture_location(localityDeparture);
            }
        }else{
            ride.setDeparture_location("000");
        }

        if (DataHandler.getDoublePreference(ARRIVAL_LAT)!=null && DataHandler.getDoublePreference(ARRIVAL_LANG)!=null ){
            String localityArrival = addressFromLocation(c, DataHandler.getDoublePreference(ARRIVAL_LAT),DataHandler.getDoublePreference(ARRIVAL_LANG));
            if (localityArrival.contains(",")) {
                try {
                    //create a location from a string from Ride :
                    String[] tabDepartureLocation = localityArrival.split(",");
                    Location locationTmpRide = new Location("Ride");
                    double lat = Double.parseDouble(tabDepartureLocation[0]);
                    double longi = Double.parseDouble(tabDepartureLocation[1]);
                    locationTmpRide.setLatitude(lat);
                    locationTmpRide.setLongitude(longi);
                    String localityDepartureSplitted = addressFromLocation(c, locationTmpRide);
                    //save the locality :
                    ride.setArrival_location(localityDepartureSplitted);
                } catch (NumberFormatException fe) {
                    fe.printStackTrace();
                }
            } else {
                ride.setArrival_location(localityArrival);
            }
        }else{
            ride.setArrival_location("000");
        }


        ride.setArrival_time(DataHandler.getLongreferences(AppConstants.KEY_ARRIVAL_LONG));

        ride.setTime_elapsed(differenceTime(DataHandler.getLongreferences(AppConstants.KEY_DEPARTURE_LONG), DataHandler.getLongreferences(AppConstants.KEY_ARRIVAL_LONG)));


        int totalsecondPM = 0;
        if(!switchCheck){
            ride.setTotalpassengerModeTime(0);
            Long currentTimeRec =  System.currentTimeMillis();
            String timeRec=  formateLongToOnlyDateForServer(currentTimeRec);
            LoggingOperations.writeToFile(context,"ManageStopChronometer " +timeRec + " Passenger Mode_Time Elapsed ---------->:"+ 0 +":<----------- ");
        }
        else{
            if(ispassengerSwitch){
                passengerMode = DataHandler.getIntPreferencesPassengerMode(AppConstants.KEY_PASSENGER_MODE);
                if(passengerMode ==0){
                    totalsecondPM = (int) minutesPassengerSwitchOff;
                }else{
                    totalsecondPM= (int) (System.currentTimeMillis()/1000L - currentTime);
                    totalsecondPM = (int) (totalsecondPM + minutesPassengerSwitchOff);
                }
            }
            else{
                totalsecondPM = (int) (System.currentTimeMillis()/1000L- totalminutesPassengerMode);
            }

            if(totalsecondPM <=60){
                totalsecondPM = 1;
                ride.setTotalpassengerModeTime(totalsecondPM);
                Long currentTimeRec =  System.currentTimeMillis();
                String timeRec=  formateLongToOnlyDateForServer(currentTimeRec);
                LoggingOperations.writeToFile(context,"ManageStopChronometer " + timeRec + " Passenger Mode_Time Elapsed ---------->:"+ totalsecondPM +":<----------- ");
            }else{
                totalsecondPM = (int) TimeUnit.SECONDS.toMinutes(totalsecondPM);
                ride.setTotalpassengerModeTime(totalsecondPM);
                Long currentTimeRec =  System.currentTimeMillis();
                String timeRec=  formateLongToOnlyDateForServer(currentTimeRec);
                LoggingOperations.writeToFile(context,"ManageStopChronometer " + timeRec + " Passenger Mode_Time Elapsed ---------->:"+ totalsecondPM +":<----------- ");
            }
        }


        ride.setOrphandrideBit(AppConstants.ORPHAN_RIDE_BIT);
//        ride.setScore(Detector.FdApp.fdSensors.getNewAverage());
        ride.setScore(DataHandler.getfloatPreferences(AppConstants.RIDE_SCORE));
        ride.setBad_behaviour(DataHandler.getIntPreferences(AppConstants.SCORE_TOTAL_BAD_COUNTS));


        ride.setSend(false);
        ride.setTime(Detector.FdApp.fdSensors.getStringTime());
        //time in secondes:
        ride.setTimeSecondes(Detector.FdApp.fdSensors.getStaticTotalTime());
        //avg score :
        double tmpAVG = calculateAVGRide(ride.getTimeSecondes(),ride.getScore());
        ride.setAverageP(tmpAVG);
        ride.setTotalTimeAbove120(LocationService.totalTimeinSeconds);
        /*ride.setTotalpassengerModeTime(LocationService.totalminutesPassengerSwitchOn);*/
        ride.setCauseofendingRide(reason_ride_end);
        ride.setSudden_accelaration(accelerate);
        ride.setSudden_braking(braking);


        Log.e("Checking Preference","score "+DataHandler.getfloatPreferences(AppConstants.RIDE_SCORE)+" count " +DataHandler.getIntPreferences(AppConstants.SCORE_TOTAL_BAD_COUNTS));
        rideBDD.open();

        rideBDD.insertRide(ride);



        sendMessageToMenuActivity(c);

        InsertSubRides(context);
        rideBDD.close();
        LocationService.timeSave = null;
        /*LocationService.passengerModeTime = null;*/

        //Sync the Ride After Saving in DataBase
        try{
            if(AppUtils.isNetworkAvailable() ){
                AppUtils.rideSync(context);

            }
            else{
                AppUtils.isUpdated = false;
            }
        }catch (Exception exception){

            exception.printStackTrace();
            Long currentTimes =  System.currentTimeMillis();
            String time=  formateLongToOnlyDateForServer(currentTime);

            Log.e("Handling,Exception1",""+exception);
            LoggingOperations.writeToFile(context,"CHRONOMETER Exception1 > "+time +exception);
        }
        try{
            if(reason_ride_end != 0){
                reason_ride_end = 0;
                accelerate = 0;
                braking = 0;
            }
            if(AppConstants.ORPHAN_RIDE_BIT !=0){
                AppConstants.ORPHAN_RIDE_BIT = 0;
            }
            if(BackgroundBeaconScan.beaconlostCounter !=0){
                BackgroundBeaconScan.beaconlostCounter = 0;
            }
            if(totalminutesPassengerMode !=0){
                totalminutesPassengerMode = 0;
            }
            if(minutesPassengerSwitchOff !=0){
                minutesPassengerSwitchOff = 0;
            }
            Detector.FdApp.departureLocation = null;
            Detector.FdApp.arrivalLocation = null;
            Detector.FdApp.departure_time = 0;
            Detector.FdApp.arrival_time = 0;
            ispassengerSwitch = false;
            switchCheck = false;
            rideneverStarted = false;
            DataHandler.updatePreferences(AppConstants.KEY_PASSENGER_MODE,0);
            /*demoModeSubscription(context);*/
        }catch (NullPointerException e){
            e.printStackTrace();
        }

    }




    /**
     * Send an Intent with an action named "com.freeedrive_saving_driving.sensor_event".
     * The time represents the total time of a journey and the text represent the text to display in view chart are send to Menu Activity
     * to display on chart the values. Save information in the preferences
     *
     * @param context(in), @Activity represents the activity
     */
    public void sendMessageToMenuActivity(Context context){
        Log.e("sendmsgToMenuActivity","sensor message is sent");
        Intent intent = new Intent("com.freeedrive_saving_driving.sensor_event");

        //add data, app. is foreground :
        intent.putExtra("time","0");
        intent.putExtra("text","0");
        intent.putExtra("badbehaviour","0");
        intent.putExtra("value_safetyscore",0.0);
        intent.putExtra("text_safetyscore", "0");
        context.sendBroadcast(intent);
    }


    /**
     * Allows to calculate the average
     * @param sec(in), @Integer represents the secondes
     * @param score(in), @Double represents the score
     * @return(out), @Double represents the average
     */
    private double calculateAVGRide (int sec, double score){
        //Log.i("Adneom"," secondes "+sec+" and score "+score);
        return (sec * score);
    }

    /**
     * Calculate the difference between two times
     * @param departure(in), @Long is the departure time
     * @param arrival(in), @Long is the arrival time
     * @return(out), @Long the difference between departure time and arrival time
     */
    public long differenceTime(long departure, long arrival){
        long convertToMinutes=0;
        if (departure!=0 && arrival!=0){
            //  long timeDifference= TimeUnit.MILLISECONDS.toSeconds(arrival)-TimeUnit.MILLISECONDS.toSeconds(departure);
            long timeDifference= arrival-departure;

            convertToMinutes = timeDifference/60L;
            if (convertToMinutes<=1){
                convertToMinutes=1;
            }
        }else{
            convertToMinutes=1;
        }

        return (convertToMinutes);
    }



    /**
     * Get address from a location's lat lng
     * @param context, @Activity represents an Activity
     * @param Lat, @Location represents a location : latitude
     * @param Lng, @Location represents a location : latitude
     * @return the locality
     */
    private String addressFromLocation(Context context, Double Lat,Double Lng){
        String locality = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {

            List<Address> addresses = geocoder.getFromLocation(Lat,Lng, 1);
            if(addresses != null && addresses.size() > 0){
                //Log.i("Adneom"," result is "+addresses);
                Address address = addresses.get(0);
                if(address != null && address.getMaxAddressLineIndex() > 0){
                    //Log.i("Adneom"," locality "+address.getLocality());
                    locality = address.getLocality();
                    Log.e("Locality",address.getAdminArea()+"--"+address.getSubAdminArea()+"=="+address.getPremises()+"=="+address.getSubLocality()+"=="+address);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("E", e.getMessage());
            //Log.i("Adneom","error is "+e.getMessage());
        }

        try{
            if(locality.equalsIgnoreCase("")){
                locality = Lat+","+Lat;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return locality;
    }



    /**
     * Get address from a location
     * @param context, @Activity represents an Activity
     * @param loc, @Location represents a location : latitude and longitude
     * @return the locality
     */
    private String addressFromLocation(Context context, Location loc){
        String locality = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {

            List<Address> addresses = geocoder.getFromLocation(loc.getLatitude(),loc.getLongitude(), 1);
            if(addresses != null && addresses.size() > 0){
                //Log.i("Adneom"," result is "+addresses);
                Address address = addresses.get(0);
                if(address != null && address.getMaxAddressLineIndex() > 0){
                    //Log.i("Adneom"," locality "+address.getLocality());
                    locality = address.getLocality();
                    Log.e("Locality",address.getAdminArea()+"--"+address.getSubAdminArea()+"=="+address.getPremises()+"=="+address.getSubLocality()+"=="+address);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("E", e.getMessage());
            //Log.i("Adneom","error is "+e.getMessage());
        }

        try{
            if(locality.equalsIgnoreCase("")){
                locality = loc.getLatitude()+","+loc.getLongitude();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return locality;
    }
    /**
     * Allows to start the chronometer for a ride
     */
    private void manageStartChronometer(Context c){
        //was a reboot :
    /* if(Detector.FdApp.fdgpsTracker == null) {
            Detector.FdApp.fdgpsTracker = new FDGPSTracker(Detector.FdApp);
        }*/

        //reset cptSensor :
        Detector.cptSnsors = 0;
        //reset totaltimeinseconds over 120km
        LocationService.totalTimeinSeconds = 0;
        //  Date departureTime = new Date();
        Long departureTime = System.currentTimeMillis()/1000L;
        Log.e("FIRED","STARTED");
        Detector.FdApp.departure_time = departureTime;
        DataHandler.updatePreferences(AppConstants.KEY_DEPARTURE_LONG,departureTime);
        String dateFormatted = AppUtils.formate10LongDateToDisplay(departureTime);
        Log.e("DepartureTime",dateFormatted+" long >"+ Detector.FdApp.departure_time);
        Long currentTimeRec =  System.currentTimeMillis();
        String timeRec=  formateLongToOnlyDateForServer(currentTimeRec);
        LoggingOperations.writeToFile(context,"CHRONOMETER " +timeRec+" Start free drive with departure time to be > "+dateFormatted);
        AppUtils.chronometer = new Chronometer(c);
        AppUtils.chronometer.setBase(SystemClock.elapsedRealtime());
        AppUtils.chronometer.start();
        AppUtils.isStarted = true;

        //current location :

        /*Detector.FdApp.departureLocation = Detector.FdApp.fdgpsTracker.getLLLocation();
        if(Detector.FdApp.departureLocation != null){
            Log.i("Adneom","____ Departure location is ("+Detector.FdApp.departureLocation.getLatitude()+","+Detector.FdApp.departureLocation.getLongitude()+") ___ ");
        }*/
    }
    private void isNewJourney(){
        if(AppUtils.isStarted){

            //set totaltimeinsecond over 120km to = 0
            LocationService.totalTimeinSeconds = 0;

            long time2  = SystemClock.elapsedRealtime() - AppUtils.chronometer.getBase();
            Date dateAfterDisconnection = new Date(time2);
            DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
            String dateFormatted = formatter.format(dateAfterDisconnection);
            String[] tab = dateFormatted.split(":");

            //values  > 1 min:
            if(Integer.parseInt(tab[2]) >= 10 /*Integer.parseInt(tab[1]) >= 1*/){
                AppUtils.isNewJourney = true;
                //Log.i("Adneom"," *** is a new journey *** ");
            }else{
                //Log.i("Adneom"," *** is not a new journey *** ");
                //Log.i("Adneom"," The ride is "+FDUtils.tmpRide.getDriver_id()+" ***");
                AppUtils.isNewJourney = false;
            }

            AppUtils.chronometer.stop();
            AppUtils.isStarted = false;
            Log.e("isNewJourney","isNewJourney");

        }

    }
    private void startFDSensor() {
        Detector.FdApp.fdSensors.startSensorsFD();
    }

    private void checkIfGpsIsEnabled(final Context context) {
        LocationManager locManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        if(locManager != null){
            try{
                gps_enabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                //Log.i("Adneom","gps : "+gps_enabled);
                if(!gps_enabled){
                    sendAskingGPSToMenuActivity(context);
                }
            }catch (Exception ex){
                ex.printStackTrace();
                Log.e("Bfpk", "error " + ex.getMessage());
                Log.e("Bfpk",ex.getMessage());
            }
        }
    }

    private void sendAskingGPSToMenuActivity(Context context) {
        Intent intent = new Intent("com.freeedrive_saving_driving.enable_gps");
        //1 : have to ask
        intent.putExtra("asking","1");
        context.sendBroadcast(intent);
    }

    private void EndRideAfterBToff(final Context context){
        beaconFd = new Timer();
        Log.e("Handling reciever", "Started Timer BT disconnect" );
        Long currentTime =  System.currentTimeMillis();
        String timea=  formateLongToOnlyDateForServer(currentTime);
        LoggingOperations.writeToFile(context,"RECIEVERS > "+timea +" Started Timer BT disconnect");

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)){
                    Log.e("Handling reciever", "Executed disconnect timer" );

                    if (DataHandler.getBooleanPreferences(AppConstants.IS_BLUETOOTH)== false){
                        if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)){
                            stopFreeeDrive(context);

                        }

                    }
                    beaconFd=null;
                }
            }
        };
        beaconFd.schedule(timerTask,350000);
        //beaconFd.schedule(timerTask,12000);

    }

}
