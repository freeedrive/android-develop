package com.studio.barefoot.freeedrivebeacononlyapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.QrCodeAsyncTaskRecoveryViaEmail;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.UpdateDemoModeAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.BeaconInfoDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.QrCodeUIDdialog;
import com.studio.barefoot.freeedrivebeacononlyapp.qrcode.BarcodeGraphic;
import com.studio.barefoot.freeedrivebeacononlyapp.qrcode.BarcodeTrackerFactory;
import com.studio.barefoot.freeedrivebeacononlyapp.qrcode.CameraSourcePreview;
import com.studio.barefoot.freeedrivebeacononlyapp.qrcode.GraphicOverlay;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.qrcode.CameraSource;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class QrCodeActivity extends AppCompatActivity  {
    private static final String TAG = "Barcode-reader";
    TextView headingQrcode, headingDrivePadInfo;

    ImageView img_Scan_qrCode;
    Button scan,btn_demo_mode;
    RelativeLayout relativeLayout;
    View actionBarView;
    QrCodeUIDdialog qrCodeUIDdialog;

    private boolean goBack = false;
    private String languagePref = "";
    private CameraSource mCameraSource;
    private CameraSourcePreview mPreview;
    private GraphicOverlay<BarcodeGraphic> mGraphicOverlay;
    private int RC_HANDLE_GMS = 9001;
    // helper objects for detecting taps and pinches.
    private ScaleGestureDetector scaleGestureDetector;
    private GestureDetector gestureDetector;
    TextView qr_desc_email,qrCodeDesc;
    private static final int SELECT_PHOTO = 100;
    //for easy manipulation of the result
    public String barcode;
    Boolean QrcodeRecovery = false;
    public static ProgressBarDialog progressBarDialogDemoMode;
    public QrCodeActivity() {
        LocaleUtils.updateConfig(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code);
        // For setting up the different logo of toll bar for demo and paid mode
        RelativeLayout toolbar = (RelativeLayout)findViewById(R.id.topToolbar);
        relativeLayout = (RelativeLayout) findViewById(R.id.out);
        ImageView fd_logo = (ImageView)toolbar.findViewById(R.id.tol_bar_logo);
        if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)){
            fd_logo.setImageResource(R.drawable.logo_freeedrive_demo_mode);
        }  else{
            fd_logo.setImageResource(R.drawable.logo_topbar);
        }

        mPreview = (CameraSourcePreview) findViewById(R.id.preview);
        mGraphicOverlay = (GraphicOverlay<BarcodeGraphic>) findViewById(R.id.graphicOverlay);
        qr_desc_email = (TextView) findViewById(R.id.qr_desc_email);
        qrCodeDesc = (TextView)findViewById(R.id.is_new_device) ;
        btn_demo_mode = (Button)findViewById(R.id.btn_demo_mode);
        btn_demo_mode.setVisibility(View.GONE);
        createCameraSource(true, false);



        //  startCameraSource();


        gestureDetector = new GestureDetector(this, new CaptureGestureListener());
        scaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());

        Snackbar.make(mGraphicOverlay, getResources().getString(R.string.camera_capture),
                Snackbar.LENGTH_INDEFINITE)
                .show();

              /* if (getIntent().getStringExtra("invalidQrcode") != null && getIntent().getStringExtra("invalidQrcode").equalsIgnoreCase("invalidQrcode")) {
                   Toast toast= Toast.makeText(QrCodeActivity.this,
                           "Invalid Qr code, please scan again", Toast.LENGTH_LONG);
                   toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 0);
                   toast.show();
               }*/

        if (getIntent().getStringExtra(AppConstants.QR_CODE_RECOVERY) != null && getIntent().getStringExtra(AppConstants.QR_CODE_RECOVERY).equalsIgnoreCase(AppConstants.QR_CODE_RECOVERY_key)) {
            qr_desc_email.setVisibility(View.VISIBLE);
            btn_demo_mode.setBackgroundColor(QrCodeActivity.this.getResources().getColor(R.color.colorBTestMenu));
            btn_demo_mode.setClickable(false);
            btn_demo_mode.setVisibility(View.GONE);
            QrcodeRecovery = true;
            String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER);
            List<NameValuePair> mParams = new ArrayList<NameValuePair>();
            mParams.add(new BasicNameValuePair("phone_number", phoneNumber));
            QrCodeAsyncTaskRecoveryViaEmail qrCodeAsyncTaskRecoveryViaEmail = new QrCodeAsyncTaskRecoveryViaEmail(QrCodeActivity.this,WebServiceConstants.END_POINT_RECOVERY_VIA_EMAIL,mParams);
            qrCodeAsyncTaskRecoveryViaEmail.execute();

        }
        else{
            QrcodeRecovery = false;
        }


        qrCodeDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                BeaconInfoDialog beaconInfoDialog = BeaconInfoDialog.newInstance("");
                beaconInfoDialog.show(fm, "");
            }
        });

        qr_desc_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPic = new Intent(Intent.ACTION_PICK);
                photoPic.setType("image/*");
                startActivityForResult(photoPic, SELECT_PHOTO);
            }
        });

        if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY)) {
         btn_demo_mode.setBackgroundColor(QrCodeActivity.this.getResources().getColor(R.color.colorBTestMenu));
         btn_demo_mode.setClickable(false);
         btn_demo_mode.setVisibility(View.GONE);
        }

        /*btn_demo_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY)) {

                }
                else{
                    if (getIntent().getStringExtra(AppConstants.QR_CODE_RECOVERY) != null && getIntent().getStringExtra(AppConstants.QR_CODE_RECOVERY).equalsIgnoreCase(AppConstants.QR_CODE_RECOVERY_key)) {
                        btn_demo_mode.setBackgroundColor(QrCodeActivity.this.getResources().getColor(R.color.colorBTestMenu));
                        btn_demo_mode.setClickable(false);
                    }else {
                    Intent intent = new Intent(QrCodeActivity.this,WelcomeDemoModeActivity.class);
                    startActivity(intent);
                    }
                   *//* progressBarDialogDemoMode = new ProgressBarDialog(QrCodeActivity.this);
                    progressBarDialogDemoMode.setTitle(getString(R.string.title_progress_dialog));
                    progressBarDialogDemoMode.setMessage(getString(R.string.body_progress_dialog));
                    progressBarDialogDemoMode.show();

                    List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                    mParams.add(new BasicNameValuePair("phone_number", DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
                    UpdateDemoModeAsyncTask updateDemoModeAsyncTask = new UpdateDemoModeAsyncTask(QrCodeActivity.this, WebServiceConstants.UPDATE_DEMO_MODE, mParams);
                    updateDemoModeAsyncTask.execute();*//*
                }
            }
        });*/

        setupActionBar();
//comment test
        /*ActivityCompat.requestPermissions(QrCodeActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.SEND_SMS, android.Manifest.permission.CAMERA ,android.Manifest.permission.READ_PHONE_STATE}, 1001);*/
       /* if (getIntent().getStringExtra("validateQr") != null && getIntent().getStringExtra("validateQr").equalsIgnoreCase("validated")) {

            String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER);
            String uuid = DataHandler.getStringPreferences(AppConstants.TEMP_UUID);

            //  String uuid = DataHandler.getStringPreferences(AppConstants.UUID_iBEACON);

            List<NameValuePair> mParams = new ArrayList<NameValuePair>();
            final TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String deviceId = telephonyManager.getDeviceId();
            mParams.add(new BasicNameValuePair("devID",deviceId));
            mParams.add(new BasicNameValuePair("phone_number", phoneNumber));
            mParams.add(new BasicNameValuePair("uuid", uuid));
            progressBarDialogQrCodeDialog = new ProgressBarDialog(this);
            progressBarDialogQrCodeDialog.setTitle(getString(R.string.title_progress_dialog));
            progressBarDialogQrCodeDialog.setMessage(getString(R.string.body_progress_dialog));
            progressBarDialogQrCodeDialog.show();
            QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(ApplicationController.getmAppcontext(), WebServiceConstants.END_POINT_UUID_v3, mParams);
            qrCodeAsyncTask.execute();
            isFinishing();
            Log.e("qrAsync", "" + mParams);

        } else if (getIntent().getStringExtra("validateSerial") != null && getIntent().getStringExtra("validateSerial").equalsIgnoreCase("validatedSerial")) {
            String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER);
            String uuid = DataHandler.getStringPreferences(AppConstants.TEMP_IBKS_SERIAL_NO);
            Log.e("iBKS", "Serial :" + uuid);
            List<NameValuePair> mParams = new ArrayList<NameValuePair>();
            final TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String deviceId = telephonyManager.getDeviceId();
            mParams.add(new BasicNameValuePair("devID",deviceId));
            mParams.add(new BasicNameValuePair("phone_number", phoneNumber));
            mParams.add(new BasicNameValuePair("uuid", uuid));
            progressBarDialogQrCodeDialog = new ProgressBarDialog(this);
            progressBarDialogQrCodeDialog.setTitle(getString(R.string.title_progress_dialog));
            progressBarDialogQrCodeDialog.setMessage(getString(R.string.body_progress_dialog));
            progressBarDialogQrCodeDialog.show();
            QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(ApplicationController.getmAppcontext(), WebServiceConstants.END_POINT_UUID_v3, mParams);
            qrCodeAsyncTask.execute();
            isFinishing();
        } else if (getIntent().getStringExtra("comingFromProfile") != null && getIntent().getExtras().getString("comingFromProfile").equalsIgnoreCase("1")) {
            goBack = true;
        }
        headingDrivePadInfo = (TextView) findViewById(R.id.drivePadinfo);
        headingQrcode = (TextView) findViewById(R.id.tv_headingQr);
        scan = (Button) findViewById(R.id.img_Next_qrCode);
        actionBarView = getLayoutInflater().inflate(R.layout.custom_toolbarr, null);
        img_Scan_qrCode = (ImageView) findViewById(R.id.img_Scan_qrCode);
        //headingDrivePadInfo.setPaintFlags(headingDrivePadInfo.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

       *//*headingDrivePadInfo.setText(Html.fromHtml("<u>info@freeedrive.com</u>"));
        *//**//*headingDrivePadInfo.setText("info@freeedrive.com");*//**//*
        headingDrivePadInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "info@freeedrive.com" });
                startActivity(Intent.createChooser(intent, ""));
                *//**//*Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://info@freeedrive.com"));
                startActivity(intentBrowser);*//**//*
            }
        });*//*
        languagePref = DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG);
        String localLanguage = Locale.getDefault().getLanguage();
        if (languagePref != null || !localLanguage.isEmpty()) {
            if (languagePref.equalsIgnoreCase("en") || Locale.getDefault().getLanguage().equalsIgnoreCase("en")) {
                String qrdesc = this.getResources().getString(R.string.qr_desc);

                final SpannableStringBuilder sb = new SpannableStringBuilder(qrdesc);

                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 9, 16, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold

                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                        *//*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*//*
                    }
                };
                final StyleSpan bss_ = new StyleSpan(android.graphics.Typeface.BOLD);
                sb.setSpan(bss_, 44, 53, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                sb.setSpan(clickableSpan, 44, 53, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 44, 53, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingQrcode.setText(sb);
                headingQrcode.setMovementMethod(LinkMovementMethod.getInstance());


            } else if (languagePref.equalsIgnoreCase("es") || Locale.getDefault().getLanguage().equalsIgnoreCase("es")) {
                String qrdesc_es = this.getResources().getString(R.string.qr_desc);

                final SpannableStringBuilder sb = new SpannableStringBuilder(qrdesc_es);

                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 18, 28, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold

                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                        *//*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*//*
                    }
                };
                final StyleSpan bss_ = new StyleSpan(android.graphics.Typeface.BOLD);
                sb.setSpan(bss_, 58, 66, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                sb.setSpan(clickableSpan, 58, 66, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 58, 66, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingQrcode.setText(sb);
                headingQrcode.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (languagePref.equalsIgnoreCase("fr") || Locale.getDefault().getLanguage().equalsIgnoreCase("fr")) {
                String qrdesc = this.getResources().getString(R.string.qr_desc);

                final SpannableStringBuilder sb = new SpannableStringBuilder(qrdesc);

                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 20, 28, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold

                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                        *//*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*//*
                    }
                };
                final StyleSpan bss_ = new StyleSpan(android.graphics.Typeface.BOLD);
                sb.setSpan(bss_, 45, 53, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                sb.setSpan(clickableSpan, 45, 53, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 45, 53, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingQrcode.setText(sb);
                headingQrcode.setMovementMethod(LinkMovementMethod.getInstance());

            } else if (languagePref.equalsIgnoreCase("nl") || Locale.getDefault().getLanguage().equalsIgnoreCase("nl")) {
                String qrdesc = this.getResources().getString(R.string.qr_desc);

                final SpannableStringBuilder sb = new SpannableStringBuilder(qrdesc);

                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 11, 18, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold

                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                        *//*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*//*
                    }
                };
                final StyleSpan bss_ = new StyleSpan(android.graphics.Typeface.BOLD);
                sb.setSpan(bss_, 63, 71, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                sb.setSpan(clickableSpan, 63, 71, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 63, 71, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingQrcode.setText(sb);
                headingQrcode.setMovementMethod(LinkMovementMethod.getInstance());
            } else {
                String qrdesc = "Scan the QR code placed\\n on the back of your DrivePad.";

                final SpannableStringBuilder sb = new SpannableStringBuilder(qrdesc);

                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 9, 16, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold

                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                        *//*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*//*
                    }
                };
                final StyleSpan bss_ = new StyleSpan(android.graphics.Typeface.BOLD);
                sb.setSpan(bss_, 44, 53, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                sb.setSpan(clickableSpan, 44, 53, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 44, 53, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingQrcode.setText(sb);
                headingQrcode.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }

        // drive pad info

        if (languagePref != null && !languagePref.isEmpty()) {
            if (languagePref.equalsIgnoreCase("en")) {
                String drivepad_info = this.getResources().getString(R.string.drivpad_info);
                Log.e("drivepad_info", drivepad_info);
                final SpannableStringBuilder sb = new SpannableStringBuilder(drivepad_info);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 21, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                        *//*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*//*
                    }
                };
                sb.setSpan(clickableSpan, 21, 29, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingDrivePadInfo.setText(sb);
                headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());
               *//* String newconnector_en = this.getResources().getString(R.string.new_connector);

                final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_en);
                final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb2.setSpan(bss2, 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent startQrcodeIntent = new Intent(ProfileActivity.this,QrCodeActivity.class);
                        startQrcodeIntent.putExtra("comingFromProfile","1");
                        startActivity(startQrcodeIntent);
                    }
                };
                sb2.setSpan(clickableSpan1, 21, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorText)),21,25,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                tv_new_connector.setText(sb2);
                tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());
*//*


            } else if (languagePref.equalsIgnoreCase("es")) {
                String newphoneno_es = this.getResources().getString(R.string.drivpad_info);
                Log.e("newphoneno_es", newphoneno_es);
                final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_es);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 18, 26, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                       *//* changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*//*
                    }
                };
                sb.setSpan(clickableSpan, 18, 26, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 18, 26, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingDrivePadInfo.setText(sb);
                headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());
               *//* String newconnector_es = this.getResources().getString(R.string.new_connector);

                final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_es);
                final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb2.setSpan(bss2, 26, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent startQrcodeIntent = new Intent(ProfileActivity.this,QrCodeActivity.class);
                        startQrcodeIntent.putExtra("comingFromProfile","1");
                        startActivity(startQrcodeIntent);
                    }
                };
                sb2.setSpan(clickableSpan1, 26, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorText)),26,30,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                tv_new_connector.setText(sb2);
                tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());*//*
            } else if (languagePref.equalsIgnoreCase("fr")) {
                String newphoneno_fr = this.getResources().getString(R.string.drivpad_info);
                Log.e("newphoneno_fr", newphoneno_fr);
                final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_fr);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 22, 31, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                        *//*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*//*
                    }
                };
                sb.setSpan(clickableSpan, 22, 31, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 22, 31, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingDrivePadInfo.setText(sb);
                headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());

                *//*String newconnector_fr = this.getResources().getString(R.string.new_connector);
                final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_fr);
                final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb2.setSpan(bss2, 33, 36, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent startQrcodeIntent = new Intent(ProfileActivity.this,QrCodeActivity.class);
                        startQrcodeIntent.putExtra("comingFromProfile","1");
                        startActivity(startQrcodeIntent);
                    }
                };
                sb2.setSpan(clickableSpan1, 33, 36, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorText)),33,36,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                tv_new_connector.setText(sb2);
                tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());*//*
            } else if (languagePref.equalsIgnoreCase("nl")) {
                String newphoneno = this.getResources().getString(R.string.drivpad_info);
                final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno);
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb.setSpan(bss, 29, 37, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                        startActivity(intentBrowser);
                       *//* changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                        changePhoneNoDialog.show();*//*
                    }
                };
                sb.setSpan(clickableSpan, 29, 37, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 29, 37, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                headingDrivePadInfo.setText(sb);
                headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());

               *//* String newconnector_nl = this.getResources().getString(R.string.new_connector);
                final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_nl);
                final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                sb2.setSpan(bss2, 23, 27, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        Intent startQrcodeIntent = new Intent(ProfileActivity.this,QrCodeActivity.class);
                        startQrcodeIntent.putExtra("comingFromProfile","1");
                        startActivity(startQrcodeIntent);
                    }
                };
                sb2.setSpan(clickableSpan1, 23, 27, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorText)),23,27,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                tv_new_connector.setText(sb2);
                tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());*//*
            }


        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("en")) {
            String newphoneno_en = this.getResources().getString(R.string.drivpad_info);
            Log.e("newphoneno_en", newphoneno_en);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_en);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 21, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                    startActivity(intentBrowser);
                    *//*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                    changePhoneNoDialog.show();*//*
                }
            };
            sb.setSpan(clickableSpan, 21, 29, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            headingDrivePadInfo.setText(sb);
            headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());

            *//*String newconnector_en = this.getResources().getString(R.string.new_connector);

            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_en);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent startQrcodeIntent = new Intent(ProfileActivity.this,QrCodeActivity.class);
                    startQrcodeIntent.putExtra("comingFromProfile","1");
                    startActivity(startQrcodeIntent);
                }
            };
            sb2.setSpan(clickableSpan1, 21, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorText)),21,25,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_connector.setText(sb2);
            tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());*//*
        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("es")) {
            String newphoneno_es = this.getResources().getString(R.string.drivpad_info);
            Log.e("newphoneno_es", newphoneno_es);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_es);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 18, 26, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                    startActivity(intentBrowser);
                   *//* changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                    changePhoneNoDialog.show();*//*
                }
            };
            sb.setSpan(clickableSpan, 18, 26, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 18, 26, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            headingDrivePadInfo.setText(sb);
            headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());

*//*            String newconnector_es = this.getResources().getString(R.string.new_connector);

            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_es);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 26, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent startQrcodeIntent = new Intent(ProfileActivity.this,QrCodeActivity.class);
                    startQrcodeIntent.putExtra("comingFromProfile","1");
                    startActivity(startQrcodeIntent);
                }
            };
            sb2.setSpan(clickableSpan1, 26, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorText)),26,30,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_connector.setText(sb2);
            tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());*//*

        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("fr")) {
            String newphoneno_fr = this.getResources().getString(R.string.drivpad_info);
            Log.e("newphoneno_fr", newphoneno_fr);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_fr);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 22, 31, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                    startActivity(intentBrowser);
                    *//*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                    changePhoneNoDialog.show();*//*
                }
            };
            sb.setSpan(clickableSpan, 22, 31, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 22, 31, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            headingDrivePadInfo.setText(sb);
            headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());

            *//*String newconnector_fr = this.getResources().getString(R.string.new_connector);
            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_fr);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 33, 36, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent startQrcodeIntent = new Intent(ProfileActivity.this,QrCodeActivity.class);
                    startQrcodeIntent.putExtra("comingFromProfile","1");
                    startActivity(startQrcodeIntent);
                }
            };
            sb2.setSpan(clickableSpan1, 33, 36, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorText)),33,36,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_connector.setText(sb2);
            tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());*//*
        } else if (Locale.getDefault().getLanguage().equalsIgnoreCase("nl")) {
            String newphoneno = this.getResources().getString(R.string.drivpad_info);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 29, 37, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                    startActivity(intentBrowser);
                    *//*changePhoneNoDialog   = new ChangePhoneNumberDialog(ProfileActivity.this);
                    changePhoneNoDialog.show();*//*
                }
            };
            sb.setSpan(clickableSpan, 29, 37, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 29, 37, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            headingDrivePadInfo.setText(sb);
            headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());

            *//*String newconnector_nl = this.getResources().getString(R.string.new_connector);
            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_nl);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 23, 27, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent startQrcodeIntent = new Intent(ProfileActivity.this,QrCodeActivity.class);
                    startQrcodeIntent.putExtra("comingFromProfile","1");
                    startActivity(startQrcodeIntent);
                }
            };
            sb2.setSpan(clickableSpan1, 23, 27, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorText)),23, 27,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_connector.setText(sb2);
            tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());*//*
        } else {
            String newphoneno_en = "If you do not have a DrivePad,";
            Log.e("newphoneno_en", newphoneno_en);
            final SpannableStringBuilder sb = new SpannableStringBuilder(newphoneno_en);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb.setSpan(bss, 21, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/product/"));
                    startActivity(intentBrowser);
                    *//*changePhoneNoDialog = new ChangePhoneNumberDialog(ProfileActivity.this);
                    changePhoneNoDialog.show();*//*
                }
            };
            sb.setSpan(clickableSpan, 21, 29, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 29, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            headingDrivePadInfo.setText(sb);
            headingDrivePadInfo.setMovementMethod(LinkMovementMethod.getInstance());

            *//*String newconnector_en = "New DrivePad? Update here";

            final SpannableStringBuilder sb2 = new SpannableStringBuilder(newconnector_en);
            final StyleSpan bss2 = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
            sb2.setSpan(bss2, 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
            ClickableSpan clickableSpan1 = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent startQrcodeIntent = new Intent(ProfileActivity.this, QrCodeActivity.class);
                    startQrcodeIntent.putExtra("comingFromProfile", "1");
                    startActivity(startQrcodeIntent);
                }
            };
            sb2.setSpan(clickableSpan1, 21, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            sb2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorText)), 21, 25, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_new_connector.setText(sb2);
            tv_new_connector.setMovementMethod(LinkMovementMethod.getInstance());*/
        }


  /*      img_Scan_qrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtils.isNetworkAvailable()) {
                    QrScanner(v);
                    isFinishing();
                } else {
                    AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(QrCodeActivity.this);
                    error_No_Internet.setMessage(QrCodeActivity.this.getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_No_Internet.show();

                }




          *//*      //FOR UPDATION OF UUID
                  //code is 409 conflict uuid already exist
                //  String uuid = DataHandler.getStringPreferences(AppConstants.UUID);
                List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                mParams.add(new BasicNameValuePair("phone_number","+923361056568"));
                mParams.add(new BasicNameValuePair("uuid_old","09edc26d-80cc-493c-b8f5-9cd035c4"));
                mParams.add(new BasicNameValuePair("uuid","09edc26d-80cc-493c-b8f5-9cd035c5"));
                Log.e("Params",""+mParams);
                progressBarDialogQrCodeDialog = new ProgressBarDialog(QrCodeActivity.this);
                progressBarDialogQrCodeDialog.setTitle(getString(R.string.title_progress_dialog));
                progressBarDialogQrCodeDialog.setMessage(getString(R.string.body_progress_dialog));
                progressBarDialogQrCodeDialog.show();
                //   QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(ApplicationController.getmAppcontext(),"uuidUpdate",mParams);
                QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(ApplicationController.getmAppcontext(), WebServiceConstants.END_POINT_UPDATE_UUID,mParams);
                //QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(ApplicationController.getmAppcontext(), WebServiceConstants.END_POINT_UPDATE_PHONE_NUMBER,mParams);

                qrCodeAsyncTask.execute(*//*
                ;


                //   QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(ApplicationController.getmAppcontext(),"uuidUpdate",mParams);
                //   QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(ApplicationController.getmAppcontext(), WebServiceConstants.END_POINT_UUID,mParams);
                //QrCodeAsyncTask qrCodeAsyncTask = new QrCodeAsyncTask(ApplicationController.getmAppcontext(), WebServiceConstants.END_POINT_UPDATE_PHONE_NUMBER,mParams);

                //  qrCodeAsyncTask.execute();
            }
        });*/


/*    public void QrScanner(View view) {


        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.setAutoFocus(true);mScannerView.startCamera();

        // Start camera
        // mScannerView.resumeCameraPreview(this);
    }*/

/*    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }*/


    @Override
    public boolean onTouchEvent(MotionEvent e) {
        boolean b = scaleGestureDetector.onTouchEvent(e);

        boolean c = gestureDetector.onTouchEvent(e);

        return b || c || super.onTouchEvent(e);
    }


    //For custom fonts we are using calligraphy lib
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

 /*   @Override
    public void handleResult(Result rawResult) {
        Log.e("handler", rawResult.getText()); // Prints scan results
        Log.e("handler", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode)
        // show the scanner result into dialog box.

        qrCodeUIDdialog = new QrCodeUIDdialog(QrCodeActivity.this, rawResult.getText());
        qrCodeUIDdialog.show();
    }*/

    /*  public boolean isCameraUsebyApp() {
          Camera camera = null;
          try {
              camera = Camera.open();
          } catch (RuntimeException e) {
              return true;
          } finally {
              if (camera != null) camera.release();
          }
          return false;
      }*/
    @Override
    public void onBackPressed() {
        super.onBackPressed();
   /*     if (mScannerView == null) {
            mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        }
        mScannerView.stopCameraPreview();
        mScannerView.stopCamera();
        mScannerView = null;*/
/*        if (!goBack){
            if (mScannerView==null){
                mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
            }
            mScannerView.stopCameraPreview();
            mScannerView.stopCamera();
            mScannerView=null;
            startActivity(new Intent(this,QrCodeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }else {
            super.onBackPressed();
        }*/

        //setResult(RESULT_OK);
    }
    /**
     * Creates and starts the camera.  Note that this uses a higher resolution in comparison
     * to other detection examples to enable the barcode detector to detect small barcodes
     * at long distances.
     *
     * Suppressing InlinedApi since there is a check that the minimum version is met before using
     * the constant.
     */
    @SuppressLint("InlinedApi")
    private void createCameraSource(boolean autoFocus, boolean useFlash) {
        Context context = getApplicationContext();

        // A barcode detector is created to track barcodes.  An associated multi-processor instance
        // is set to receive the barcode detection results, track the barcodes, and maintain
        // graphics for each barcode on screen.  The factory is used by the multi-processor to
        // create a separate tracker instance for each barcode.
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(context).build();
        BarcodeTrackerFactory barcodeFactory = new BarcodeTrackerFactory(mGraphicOverlay);
        barcodeDetector.setProcessor(new MultiProcessor.Builder<>(barcodeFactory).build());

        if (!barcodeDetector.isOperational()) {
            // Note: The first time that an app using the barcode or face API is installed on a
            // device, GMS will download a native libraries to the device in order to do detection.
            // Usually this completes before the app is run for the first time.  But if that
            // download has not yet completed, then the above call will not detect any barcodes
            // and/or faces.
            //
            // isOperational() can be used to check if the required native libraries are currently
            // available.  The detectors will automatically become operational once the library
            // downloads complete on device.

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(this, "low storage", Toast.LENGTH_LONG).show();
                Log.w(TAG, "low storage");
            }

        }

        // Creates and starts the camera.  Note that this uses a higher resolution in comparison
        // to other detection examples to enable the barcode detector to detect small barcodes
        // at long distances.
        CameraSource.Builder builder = new CameraSource.Builder(getApplicationContext(), barcodeDetector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1600, 1024)
                .setRequestedFps(15.0f);

        // make sure that auto focus is an available option
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            builder = builder.setFocusMode(
                    autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE : null);
        }

        mCameraSource = builder
                .setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
                .build();
    }

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() throws SecurityException {
        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }
    @Override
    protected void onPause() {
        super.onPause();

        if (mPreview != null) {
            mPreview.stop();
        }
        /*   if (mScannerView == null) {
            mScannerView = new ZXingScannerView(this);
            mScannerView.stopCamera();
            // Programmatically initialize the scanner view
        } else {
            mScannerView.stopCamera();
        }*/
    }
    /**
     * Releases the resources associated with the camera source, the associated detectors, and the
     * rest of the processing pipeline.
     */

    /**
     * onTap returns the tapped barcode result to the calling Activity.
     *
     * @param rawX - the raw position of the tap
     * @param rawY - the raw position of the tap.
     * @return true if the activity is ending.
     */


    private boolean onTap(float rawX, float rawY) {
        // Find tap point in preview frame coordinates.
        int[] location = new int[2];
        mGraphicOverlay.getLocationOnScreen(location);
        float x = (rawX - location[0]) / mGraphicOverlay.getWidthScaleFactor();
        float y = (rawY - location[1]) / mGraphicOverlay.getHeightScaleFactor();

        // Find the barcode whose center is closest to the tapped point.
        Barcode best = null;
        float bestDistance = Float.MAX_VALUE;
        for (BarcodeGraphic graphic : mGraphicOverlay.getGraphics()) {
            Barcode barcode = graphic.getBarcode();
            if (barcode.getBoundingBox().contains((int) x, (int) y)) {
                // Exact hit, no need to keep looking.
                best = barcode;
                break;
            }
            float dx = x - barcode.getBoundingBox().centerX();
            float dy = y - barcode.getBoundingBox().centerY();
            float distance = (dx * dx) + (dy * dy);  // actually squared distance
            if (distance < bestDistance) {
                best = barcode;
                bestDistance = distance;
            }
        }

        if (best != null) {
            if (QrcodeRecovery == false){
                qrCodeUIDdialog = new QrCodeUIDdialog(QrCodeActivity.this, best.displayValue);
                qrCodeUIDdialog.show();
            }else{
                qrCodeUIDdialog = new QrCodeUIDdialog(QrCodeActivity.this, best.displayValue,true);
                qrCodeUIDdialog.show();
            }

          //  finish();
            return true;
        }
        return false;
    }

    private class CaptureGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            return onTap(e.getRawX(), e.getRawY()) || super.onSingleTapConfirmed(e);
        }
    }

    private class ScaleListener implements ScaleGestureDetector.OnScaleGestureListener {

        /**
         * Responds to scaling events for a gesture in progress.
         * Reported by pointer motion.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         * @return Whether or not the detector should consider this event
         * as handled. If an event was not handled, the detector
         * will continue to accumulate movement until an event is
         * handled. This can be useful if an application, for example,
         * only wants to update scaling factors if the change is
         * greater than 0.01.
         */
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            return false;
        }

        /**
         * Responds to the beginning of a scaling gesture. Reported by
         * new pointers going down.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         * @return Whether or not the detector should continue recognizing
         * this gesture. For example, if a gesture is beginning
         * with a focal point outside of a region where it makes
         * sense, onScaleBegin() may return false to ignore the
         * rest of the gesture.
         */
        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            return true;
        }

        /**
         * Responds to the end of a scale gesture. Reported by existing
         * pointers going up.
         * <p/>
         * Once a scale has ended, {@link ScaleGestureDetector#getFocusX()}
         * and {@link ScaleGestureDetector#getFocusY()} will return focal point
         * of the pointers remaining on the screen.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         */
        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            mCameraSource.doZoom(detector.getScaleFactor());
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPreview != null) {
            mPreview.release();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
//doing some uri parsing
                    Uri selectedImage = imageReturnedIntent.getData();
                    InputStream imageStream = null;
                    try {
                        //getting the image
                        imageStream = getContentResolver().openInputStream(selectedImage);
                    } catch (FileNotFoundException e) {
                        Toast.makeText(getApplicationContext(), "File not found", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    //decoding bitmap
                    Bitmap bMap = BitmapFactory.decodeStream(imageStream);
                    //Scan.setImageURI(selectedImage);// To display selected image in image view
                    int[] intArray = new int[bMap.getWidth() * bMap.getHeight()];
                    // copy pixel data from the Bitmap into the 'intArray' array
                    bMap.getPixels(intArray, 0, bMap.getWidth(), 0, 0, bMap.getWidth(),
                            bMap.getHeight());

                    LuminanceSource source = new RGBLuminanceSource(bMap.getWidth(),
                            bMap.getHeight(), intArray);
                    BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

                    Reader reader = new MultiFormatReader();// use this otherwise
                    // ChecksumException
                    try {
                        Hashtable<DecodeHintType, Object> decodeHints = new Hashtable<DecodeHintType, Object>();
                        decodeHints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
                        decodeHints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);

                        Result result = null;
                        try {
                            result = reader.decode(bitmap, decodeHints);
                        } catch (NotFoundException e) {
                            e.printStackTrace();
                        }
                       /* I have created a global string variable by the name of barcode to easily
                        manipulate data across the application */
                                barcode = result.getText().toString();

                        //do something with the results for demo i created a popup dialog
                        if (barcode != null) {

                            QrCodeUIDdialog qrCodeUIDdialog = new QrCodeUIDdialog(QrCodeActivity.this, barcode, true);
                            qrCodeUIDdialog.show();

          /*                  AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.setTitle("Scan Result");
                            builder.setIcon(R.mipmap.ic_launcher);
                            builder.setMessage("" + barcode);
                            AlertDialog alert1 = builder.create();
                            alert1.setButton(DialogInterface.BUTTON_POSITIVE, "Done", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent i = new Intent(getBaseContext(), MainActivity.class);
                                    startActivity(i);
                                }
                            });

                            alert1.setCanceledOnTouchOutside(false);

                            alert1.show();*/
                        }else
                            {
                                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                                builder.setTitle("Scan Result");
                                builder.setIcon(R.mipmap.ic_launcher);
                                builder.setMessage("Nothing found try a different image or try again");
                                AlertDialog alert1 = builder.create();
                                alert1.setButton(DialogInterface.BUTTON_POSITIVE, "Done", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                  /*      Intent i = new Intent(getBaseContext(), MainActivity.class);
                                        startActivity(i);*/
                                    }
                                });

                                alert1.setCanceledOnTouchOutside(false);

                                alert1.show();

                            }
                            //the end of do something with the button statement.

                        } catch(Resources.NotFoundException e){
                            Toast.makeText(getApplicationContext(), "Nothing Found", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        } catch(ChecksumException e){
                            Toast.makeText(getApplicationContext(), "Something weird happen, i was probably tired to solve this issue", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        } catch(FormatException e){
                            Toast.makeText(getApplicationContext(), "Wrong Barcode/QR format", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        } catch(NullPointerException e){
                            Toast.makeText(getApplicationContext(), "Something weird happen, i was probably tired to solve this issue", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                }
        }




    @Override
    protected void onResume() {
        super.onResume();

        startCameraSource();

     /*   if (mScannerView == null) {
            mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        }
        if (getIntent().getStringExtra("comingFromProfile") != null && getIntent().getExtras().getString("comingFromProfile").equalsIgnoreCase("1")) {
            goBack = true;
        }
*/
    }
/*    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
//doing some uri parsing
                    Uri selectedImage = imageReturnedIntent.getData();
                    InputStream imageStream = null;
                    try {
                        //getting the image
                        imageStream = getContentResolver().openInputStream(selectedImage);
                    } catch (FileNotFoundException e) {
                        Toast.makeText(getApplicationContext(), "File not found", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    //decoding bitmap
                    Bitmap bMap = BitmapFactory.decodeStream(imageStream);
                    //Scan.setImageURI(selectedImage);// To display selected image in image view
                    int[] intArray = new int[bMap.getWidth() * bMap.getHeight()];
                    // copy pixel data from the Bitmap into the 'intArray' array
                    bMap.getPixels(intArray, 0, bMap.getWidth(), 0, 0, bMap.getWidth(),
                            bMap.getHeight());

                    LuminanceSource source = new RGBLuminanceSource(bMap.getWidth(),
                            bMap.getHeight(), intArray);
                    BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

                    Reader reader = new MultiFormatReader();// use this otherwise
                    // ChecksumException
                    try {
                        Hashtable<DecodeHintType, Object> decodeHints = new Hashtable<DecodeHintType, Object>();
                        decodeHints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
                        decodeHints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);

                        Result result = reader.decode(bitmap, decodeHints);
                        /*//*I have created a global string variable by the name of barcode to easily manipulate data across the application*//*/
                        barcode =  result.getText().toString();

                        //do something with the results for demo i created a popup dialog
                        if(barcode!=null){

                            QrCodeUIDdialog qrCodeUIDdialog =  new QrCodeUIDdialog(QrCodeActivity.this,barcode,true);
                            qrCodeUIDdialog.show();

                    *//*        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.setTitle("Scan Result");
                            builder.setIcon(R.mipmap.ic_launcher);
                            builder.setMessage("" + barcode);
                            AlertDialog alert1 = builder.create();
                            alert1.setButton(DialogInterface.BUTTON_POSITIVE, "Done", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent i = new Intent (getBaseContext(),MainActivity.class);
                                    startActivity(i);
                                }
                            });

                            alert1.setCanceledOnTouchOutside(false);

                            alert1.show();*//*}
                        else
                        {
                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.setTitle("Scan Result");
                            builder.setIcon(R.mipmap.ic_launcher);
                            builder.setMessage("Nothing found try a different image or try again");
                            AlertDialog alert1 = builder.create();
                            alert1.setButton(DialogInterface.BUTTON_POSITIVE, "Done", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent i = new Intent (getBaseContext(),MainActivity.class);
                                    startActivity(i);
                                }
                            });

                            alert1.setCanceledOnTouchOutside(false);

                            alert1.show();

                        }
                        //the end of do something with the button statement.

                    } catch (NotFoundException e) {
                        Toast.makeText(getApplicationContext(), "Nothing Found", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    } catch (ChecksumException e) {
                        Toast.makeText(getApplicationContext(), "Something weird happen, i was probably tired to solve this issue", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    } catch (FormatException e) {
                        Toast.makeText(getApplicationContext(), "Wrong Barcode/QR format", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        Toast.makeText(getApplicationContext(), "Something weird happen, i was probably tired to solve this issue", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
        }
    }*/
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }
}
