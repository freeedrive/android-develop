package com.studio.barefoot.freeedrivebeacononlyapp.recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

/**
 * Created by kl on 2/18/16.
 */
public class SmsReceiver extends BroadcastReceiver
{
    String message="";
    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.e("SmsReceiver","BroadCastReceiver");
        try
        {
             if (!DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE))
                return;
        }
        catch (Exception e) {
            e.printStackTrace();
            return;
        }

        Bundle bundle = intent.getExtras();
        SmsMessage[] msgs = null;
        if (bundle != null)
        {
            Object[] pdus = (Object[]) bundle.get("pdus");
            msgs = new SmsMessage[pdus.length];
            for (int i=0;i<msgs.length;i++)
            {
                msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
            }

            if(DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)&& DataHandler.getIntPreferences(AppConstants.KEY_AUTOREPLY)==1)
            {
                //get sender phone number
                String senderPhoneNumber = msgs[0].getOriginatingAddress();
                Log.e("FD", "auto-replying to: " + senderPhoneNumber);

                Log.i("Adneom", " --- auto replying ro  : " + senderPhoneNumber+ " --- ");

                //send the default sms
                    if (DataHandler.getStringPreferences(AppConstants.KEY_AUTOREPLY_MSG)!=null&&!DataHandler.getStringPreferences(AppConstants.KEY_AUTOREPLY_MSG).equalsIgnoreCase("")){
                         message = DataHandler.getStringPreferences(AppConstants.KEY_AUTOREPLY_MSG);
                    }else{
                        message = context.getResources().getString(R.string.et_auto_reply);
                    }
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(senderPhoneNumber, null, message, null, null);
                    abortBroadcast();

            }
        }
    }
}
