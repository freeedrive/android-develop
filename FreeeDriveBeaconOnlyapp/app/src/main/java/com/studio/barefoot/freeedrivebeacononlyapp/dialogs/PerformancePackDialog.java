package com.studio.barefoot.freeedrivebeacononlyapp.dialogs;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.RegisterActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

/**
 * Created by mcs on 12/28/2016.
 */

public class PerformancePackDialog extends BaseAlertDialog implements DialogInterface.OnClickListener {
    TextView tv_privacy_Link,tv_text_privacy;

    ContentResolver resolver;
    KeyEvent keyEvent;

    public PerformancePackDialog(final Context context) {
        super(context);

        LayoutInflater factory = LayoutInflater.from(context);
        final View progressBarView = factory.inflate(R.layout.custom_performance_pack, null);

        setView(progressBarView);
        this.context = context;
        tv_text_privacy = (TextView) progressBarView.findViewById(R.id.tv_text_privacy);
        setButton(BUTTON_POSITIVE, context.getString(R.string.action_send), this);
        resolver = context.getContentResolver();
        //setCancelable(true);



    }



    @Override
    public void dismiss() {
            super.dismiss();
    }


    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which){
            case BUTTON_POSITIVE:
                dialog.dismiss();
                dismiss();
                break;

        }
    }
}
