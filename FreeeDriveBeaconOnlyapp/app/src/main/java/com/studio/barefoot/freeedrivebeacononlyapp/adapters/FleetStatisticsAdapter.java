package com.studio.barefoot.freeedrivebeacononlyapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.FleetStatsBeans;

import com.txusballesteros.widgets.FitChart;
import com.txusballesteros.widgets.FitChartValue;

import java.util.ArrayList;
import java.util.Collection;

import static com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.GetFleetStatisticsAsyncTask.company_total_driver;

/**
 * Created by Yasir Barefoot on 8/30/2017.
 */

public class FleetStatisticsAdapter extends RecyclerView.Adapter<FleetStatisticsAdapter.MyViewHolder> {


    private ArrayList<FleetStatsBeans> fleetBeansList;
    Context context;
    @Override
    public FleetStatisticsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        context = parent.getContext();
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fleet_statistics, parent, false);

        return new FleetStatisticsAdapter.MyViewHolder(itemView);    }

    @Override
    public void onBindViewHolder(FleetStatisticsAdapter.MyViewHolder holder, int position) {
        FleetStatsBeans fleetBeans = fleetBeansList.get(position);
        int totaldrives =fleetBeans.getNo_of_drivers();
        /*int total_drivetime = fleetBeans.getTotal_drivetime();
        int totalPeeks = fleetBeans.getNo_of_peeks();*/
        int totalScore =(int) fleetBeans.getSafety_score();
        String fleet_drive_dates = fleetBeans.getFleet_drivedate();
        int rankbyDay = fleetBeans.getFleet_ranking();
        /*holder.date.setText(dateofRide);
        holder.time.setText(startAndEndTimeOfRide);
        holder.weekly_no_of_peeks.setText("");*/
        holder.fleet_noofDrivers.setText(""+totaldrives);
        /*holder.fleet_total_driveTime.setText(""+total_drivetime);*/
        holder.fleet_driveDate.setText(""+fleet_drive_dates);
        try{
            if(rankbyDay == 0){
                holder.fleet_ranking.setText("N/A");
            }else {
            holder.fleet_ranking.setText(""+rankbyDay+"/"+company_total_driver);
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }





        Collection<FitChartValue> fitChartValues = new ArrayList<>();
       /* if(totalPeeks <5){
            holder.fleet_noofpeeks.setText(""+totalPeeks);
            holder.fleet_noofpeeks.setTextColor(context.getResources().getColor(R.color.colorPieChartGreen));
        }
        else if(totalPeeks >=5 && totalPeeks <10){
            holder.fleet_noofpeeks.setText(""+totalPeeks);
            holder.fleet_noofpeeks.setTextColor(context.getResources().getColor(R.color.colorPieChartOrange));
        }
        else{
            holder.fleet_noofpeeks.setText(""+totalPeeks);
            holder.fleet_noofpeeks.setTextColor(context.getResources().getColor(R.color.colorPieChartRed));

        }*/
        if(totalScore >= 80){
            holder.fleet_safetyScore.setText(""+totalScore);
            holder.fleet_safetyScore.setTextColor(context.getResources().getColor(R.color.colorPieChartGreen));
            holder.textPercentage.setTextColor(context.getResources().getColor(R.color.colorPieChartGreen));
            fitChartValues.add(new FitChartValue(totalScore, context.getResources().getColor(R.color.colorPieChartGreen)));
        }else if(totalScore >=50 && totalScore < 80){


            holder.fleet_safetyScore.setText(""+totalScore);
            holder.fleet_safetyScore.setTextColor(context.getResources().getColor(R.color.colorPieChartOrange));
            holder.textPercentage.setTextColor(context.getResources().getColor(R.color.colorPieChartOrange));
            fitChartValues.add(new FitChartValue(totalScore, context.getResources().getColor(R.color.colorPieChartOrange)));
        }
        else{

            holder.fleet_safetyScore.setText(""+totalScore);
            holder.fleet_safetyScore.setTextColor(context.getResources().getColor(R.color.colorPieChartRed));
            holder.textPercentage.setTextColor(context.getResources().getColor(R.color.colorPieChartRed));
            fitChartValues.add(new FitChartValue(totalScore , context.getResources().getColor(R.color.colorPieChartRed)));
        }
        holder.fleetFitChart.setValues(fitChartValues);

      /* if (distractionsOfEachRide == -1){
           distractionsOfEachRide = 0;
       }
        holder.weekly_no_of_peeks.setText(""+distractionsOfEachRide);*/
    }
    public FleetStatisticsAdapter(ArrayList<FleetStatsBeans> fleetBeansList) {
        this.fleetBeansList = fleetBeansList;
    }
    @Override
    public int getItemCount() {
        return fleetBeansList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView fleet_noofDrivers,fleet_total_driveTime,fleet_noofpeeks,fleet_safetyScore,textPercentage,fleet_driveDate,fleet_ranking;
        private FitChart fleetFitChart;

        public MyViewHolder(View view) {
            super(view);
            fleet_driveDate = (TextView) view.findViewById(R.id.fleet_drivedate);
            fleet_noofDrivers = (TextView) view.findViewById(R.id.fleet_noofdrivers);
            fleet_ranking = (TextView) view.findViewById(R.id.fleet_ranking);
            /*fleet_total_driveTime = (TextView) view.findViewById(R.id.fleet_total_driveTime);
            fleet_noofpeeks = (TextView) view.findViewById(R.id.fleet_noofpeeks);*/
            fleet_safetyScore = (TextView) view.findViewById(R.id.fleet_safetyScore);
            textPercentage = (TextView) view.findViewById(R.id.textPercentage);
            fleetFitChart = (FitChart) view.findViewById(R.id.fleet_fitchart);
        }
    }
}


