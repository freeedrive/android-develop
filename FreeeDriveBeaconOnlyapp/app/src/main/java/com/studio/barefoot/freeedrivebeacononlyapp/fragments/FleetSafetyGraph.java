package com.studio.barefoot.freeedrivebeacononlyapp.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.WeeklyStatsForGraphAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.PerformancePackDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;
import com.txusballesteros.widgets.FitChart;
import com.txusballesteros.widgets.FitChartValue;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MySafetyFragmentGraph.DemoModeDialog;


public class FleetSafetyGraph extends Fragment {

    View graphView;
    Button phoneSafety,drivingPerformance;
    Fragment fragment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    RelativeLayout relativeLayoutPhoneSafety,relativeLayoutdriving,relativeLayoutdrivingPerformance,relativeLayoutPhone;

    /*ImageView graph_stats_left_arrow,graph_stats_right_arrow;*/
    //TextView phonesafety,drivingperformance, driverWeeklyScoreStats, leasePlaneWeeklyScorestats,drivers_name;
    ImageView imageviewDailyStats,imageviewFleetStats,graph_stats_left_arrow,graph_stats_right_arrow;
    TextView phonesafety;
    TextView drivingperformance;
    static TextView driverWeeklyScoreStats;
    static TextView leasePlaneWeeklyScorestats;
    TextView drivers_name;
    static TextView companys_name;
    public static ProgressBarDialog progressBarDialogNotifications;
    static LineChart chart;
    static JSONObject jsonObject_driverAvg = new JSONObject();
    static JSONObject jsonObject_companyAvg = new JSONObject();
    PerformancePackDialog performancePackDialog;
    ImageView ImgDrivingPerformance;
    static FitChart driverWeeklySCoreCircle;
    static FitChart leasePlanWeeklyScoreCircle;
    public static int color = ApplicationController.getmAppcontext().getResources().getColor(R.color.colorBTestMenu);
    public static int colorplotlinedriver = ApplicationController.getmAppcontext().getResources().getColor(R.color.lease_plan_black);
    public static int colorplotlineCompany = ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPrimaryNew);
    public static int colordays = ApplicationController.getmAppcontext().getResources().getColor(R.color.grey_black);
    private float temptotalDailytime;
    private int totalbadbehaviour;
    Calendar calander;
    SimpleDateFormat df;
    String currentDate = "";
    public static ProgressBarDialog progressBarDialogWeeklyStatsGraph;

    public FleetSafetyGraph() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        graphView =  inflater.inflate(R.layout.weekly_stats_graph, container, false);
        // For setting up the different logo of toll bar for demo and paid mode
        RelativeLayout toolbar = (RelativeLayout)graphView.findViewById(R.id.navbar);
        ImageView fd_logo = (ImageView)toolbar.findViewById(R.id.tol_bar_logo);
        if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)){
            fd_logo.setImageResource(R.drawable.logo_freeedrive_demo_mode);
        }  else{
            fd_logo.setImageResource(R.drawable.logo_topbar);
        }
        driverWeeklySCoreCircle = (FitChart) graphView.findViewById(R.id.daily_stats_fitchartlastride_score_3) ;
        leasePlanWeeklyScoreCircle = (FitChart) graphView.findViewById(R.id.daily_stats_fitchartlastride_peeks) ;
        driverWeeklyScoreStats =(TextView) graphView.findViewById(R.id.daily_stats_score_last_ride_3) ;
        leasePlaneWeeklyScorestats =(TextView) graphView.findViewById(R.id.daily_stats_peeks) ;
      /*  imageviewDailyStats = (ImageView) graphView.findViewById(R.id.imageviewDailyStats);
        imageviewFleetStats = (ImageView) graphView.findViewById(R.id.imageviewFleetStats);*/
        graph_stats_left_arrow = (ImageView) graphView.findViewById(R.id.graph_stats_arrow_left);
        graph_stats_right_arrow = (ImageView) graphView.findViewById(R.id.graph_stats_arrow_right);
        drivers_name=(TextView) graphView.findViewById(R.id.drivers_name) ;
        graph_stats_left_arrow.setVisibility(View.INVISIBLE);
        SharedPreferences settings = null;
        chart = (LineChart) graphView.findViewById(R.id.chart);
        companys_name =(TextView) graphView.findViewById(R.id.avg_peeks) ;



        driverWeeklySCoreCircle.setValue(100f);
        driverWeeklySCoreCircle.setMaxValue(100f);
        leasePlanWeeklyScoreCircle.setMaxValue(100f);
        leasePlanWeeklyScoreCircle.setValue(100f);
        calander = Calendar.getInstance();
        df = new SimpleDateFormat("dd/MM/yyyy");
        currentDate = df.format(calander.getTime());


        graph_stats_right_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new FleetStatisticsFragments();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                //
                fragmentTransaction.replace(getId(), fragment);
                // fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });



          /*  imageviewDailyStats.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        imageviewFleetStats.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.fleet_icon_grey));
                        imageviewFleetStats.setBackground(null);
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        imageviewDailyStats.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.mysafety_icon_blue));
                        imageviewDailyStats.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_fragment_shape));
                    }
                }
            });

            imageviewFleetStats.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        imageviewDailyStats.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.mysafety_icon_grey));
                        imageviewDailyStats.setBackground(null);
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        imageviewFleetStats.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.fleet_icon_blue));
                        imageviewFleetStats.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_fragment_shape));
                    }
                }
            });*/

        //intiliaze the Views
/*        relativeLayoutPhoneSafety = (RelativeLayout) graphView.findViewById(R.id.insidecontaintermobilehotspot);
        relativeLayoutdriving = (RelativeLayout) graphView.findViewById(R.id.drivingrelativelayout);
        relativeLayoutdrivingPerformance = (RelativeLayout) graphView.findViewById(R.id.graph_drivingrelativelayout);
        relativeLayoutPhone = (RelativeLayout) graphView.findViewById(R.id.insidecontaintermobilehotspot);
        ImgDrivingPerformance = (ImageView) graphView.findViewById(R.id.drivingperformance);



        //intiliaze the TextViews
        phonesafety = (TextView) graphView.findViewById(R.id.textviewmobilehotSpot);
        drivingperformance = (TextView) graphView.findViewById(R.id.textviewdrivingperformance);

        phoneSafety = (Button) graphView.findViewById(R.id.graph_fragment_phonesafety_btn);
        drivingPerformance = (Button) graphView.findViewById(R.id.graph_fragment_drivingperformance_btn);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            relativeLayoutPhoneSafety.setBackground(getActivity().getResources().getDrawable(R.drawable.phonesafety_custom_shape));
        }
        phonesafety.setTextColor(getActivity().getResources().getColor(R.color.white));*/

        FetchMyWeeklyStats();
        //manageMyWeeklyStats();
        //setData();



        String profileData = DataHandler.getStringPreferences(AppConstants.TEMP_DISPLAY_KEY);
        try {
            if (profileData!=null && !profileData.equalsIgnoreCase("")) {
                JSONObject jsonObj = new JSONObject(profileData);
                if (jsonObj!=null) {
                    drivers_name.setText(jsonObj.getString("first_name"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // generateDataLine();
/*        if (!DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                relativeLayoutdrivingPerformance.setBackground(getActivity().getResources().getDrawable(R.drawable.performance_pack_custom_shape));
                drivingperformance.setTextColor(getActivity().getResources().getColor(R.color.colorBTestMenu));
                ImgDrivingPerformance.setImageResource(R.drawable.tab_icon_car_grey);
            }

        }
        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Toast.makeText(getContext(), "Chart Value Selected"+e , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected() {

            }
        });



        }*/

        //settings = getActivity().getSharedPreferences(FILE_NAME_SHARED_PREF,Context.MODE_PRIVATE);
/*        if(AppUtils.isNetworkAvailable()) {

             progressBarDialogNotifications =new ProgressBarDialog(getActivity());
             progressBarDialogNotifications.setTitle(getString(R.string.title_progress_dialog));
             progressBarDialogNotifications.setMessage(getString(R.string.body_progress_dialog));
             progressBarDialogNotifications.show();
             String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER);
            final TelephonyManager telephonyManager = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
            String deviceId = telephonyManager.getDeviceId();
             List<NameValuePair> mParams = new ArrayList<NameValuePair>();
             mParams.add(new BasicNameValuePair("phone_number",phoneNumber));
            mParams.add(new BasicNameValuePair("devID", deviceId));
             Log.e("PARAMS", "" + mParams);
             GraphPhoneSafetyAsyncTask getphonesafetygraphData = new GraphPhoneSafetyAsyncTask(getActivity(), WebServiceConstants.END_POINT_GRAPH_SCORE,mParams );
             getphonesafetygraphData.execute();
            //generateDataLine();
         }
         else if(!DataHandler.getStringPreferences(AppConstants.GRAPH_WEEKLY_SAFETY_AVG).isEmpty()){
            String graphValue = DataHandler.getStringPreferences(AppConstants.GRAPH_WEEKLY_SAFETY_AVG);
            try {
                JSONObject jsonObj = new JSONObject(graphValue);
                JSONArray graph_driverAvg_jsonArray = jsonObj.getJSONArray("driverAvg");
                JSONArray graph_companyAvg_jsonArray = jsonObj.getJSONArray("companyAvg");

                generateDataLine(graph_driverAvg_jsonArray,graph_companyAvg_jsonArray);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else{
            AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(getActivity());
            error_No_Internet.setMessage(getActivity().getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //NotificationActivity.super.onBackPressed();
                }
            });
            error_No_Internet.show();
        }*/

/*        drivingPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        relativeLayoutdrivingPerformance.setBackground(getActivity().getResources().getDrawable(R.drawable.driving_performance_custom_shape));
                    }
                    drivingperformance = (TextView) graphView.findViewById(R.id.textviewdrivingperformance);
                    drivingperformance.setTextColor(getActivity().getResources().getColor(R.color.white));


                    fragment = new FleetSafetyGraph();
                    fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.phone_fragment, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        relativeLayoutPhone.setBackground(getActivity().getResources().getDrawable(R.drawable.phone_safety_custom_shape));
                    }
                    phonesafety = (TextView) graphView.findViewById(R.id.textviewmobilehotSpot);
                    phonesafety.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
                }else {
                    performancePackDialog = new PerformancePackDialog(getActivity());
                    performancePackDialog.show();
                }
            }
        });*/
        if(DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE_SUBSCRIPTION_EXPIRED) == true){
            DemoModeDialog();
        }

        return graphView;


    }

    @Override
    public void onResume() {
        super.onResume();


    }
    @Override
    public void onDestroy() {

        super.onDestroy();
        progressBarDialogWeeklyStatsGraph=null;
        driverWeeklySCoreCircle=null;
        driverWeeklyScoreStats=null;
        leasePlaneWeeklyScorestats=null;
        leasePlanWeeklyScoreCircle=null;


    }
    private void FetchMyWeeklyStats() {

        if (AppUtils.isNetworkAvailable()){
            try{


                progressBarDialogWeeklyStatsGraph = new ProgressBarDialog(getActivity());
                progressBarDialogWeeklyStatsGraph.setTitle(getActivity().getString(R.string.title_progress_dialog));
                progressBarDialogWeeklyStatsGraph.setMessage(getActivity().getString(R.string.body_progress_dialog));
                progressBarDialogWeeklyStatsGraph.show();

                List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                mParams.add(new BasicNameValuePair("phone_number",DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
                final TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
                String deviceId = telephonyManager.getDeviceId();
                mParams.add(new BasicNameValuePair("devID",deviceId));
                WeeklyStatsForGraphAsyncTask weeklyStatsForGraphAsyncTask = new WeeklyStatsForGraphAsyncTask(getActivity(), WebServiceConstants.END_POINT_WEEKLY_STATS_GRAPH,mParams);
                weeklyStatsForGraphAsyncTask.execute();
            }catch (Exception e){



            }

        }else{
            if (DataHandler.getStringPreferences(AppConstants.GRAPH_WEEKLY_SAFETY_AVG)!=null && !DataHandler.getStringPreferences(AppConstants.GRAPH_WEEKLY_SAFETY_AVG).isEmpty()){
                try {

                    JSONArray driversArray = new JSONArray(DataHandler.getStringPreferences(AppConstants.GRAPH_WEEKLY_DRIVERS_AVG));
                    JSONArray companysArray = new JSONArray(DataHandler.getStringPreferences(AppConstants.GRAPH_WEEKLY_COMPANYs_AVG));
                    int todayDriverScore = DataHandler.getIntPreferences(AppConstants.GRAPH_WEEKLY_DRIVERs_AVG_TODAY);
                    int todaysCompanSCore = DataHandler.getIntPreferences(AppConstants.GRAPH_WEEKLY_COMPANYs_AVG_TODAY);
                    String companyName = DataHandler.getStringPreferences(AppConstants.GRAPH_WEEKLY_COMPANYs_NAME);

                    updateGraphFromServer(driversArray,companysArray,todayDriverScore,todaysCompanSCore,companyName);


                } catch (JSONException e) {
                    e.printStackTrace();
                }catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void manageMyWeeklyStats() {





        RideBDD tmp = new RideBDD(getContext());
        tmp.open();

        //for today's score
        try {


            long timestampstartDate = AppUtils.getStartOfDayInMillis(currentDate);
            Log.e("timestampstartdate",":"+timestampstartDate);
            long timestampnendDate = AppUtils.getEndOfDayInMillis(currentDate);
            Log.e("timestampnendDate",":"+timestampnendDate);
            int sumBadCountToday ;
            int totalRidesToday;
            float avgRideTime;
            float avgBadCountToday;
            float peeksColorCoding;
            JSONArray listeRides = tmp.getAllRidesByDateForDailyAvg(timestampstartDate,timestampnendDate);





            //the rides list :
            if (listeRides != null && listeRides.length()>0) {
                temptotalDailytime = tmp.totalElapsedtimeDaily(timestampstartDate,timestampnendDate);

                int totalAvergaeScore=0;

                totalAvergaeScore   = calculateAverage(listeRides, temptotalDailytime);
                sumBadCountToday = tmp.totalBadCountToday(timestampstartDate,timestampnendDate);

                totalRidesToday = tmp.totalRidesToday(timestampstartDate,timestampnendDate);
                avgBadCountToday= sumBadCountToday/totalRidesToday;

                avgRideTime=tmp.avgRideTime(timestampstartDate,timestampnendDate);
                peeksColorCoding = avgBadCountToday/avgRideTime;

                //TODO
                //peeks color coding


                String s = String.format("%.1f",avgBadCountToday);
                leasePlaneWeeklyScorestats.setText(""+s);
                leasePlanWeeklyScoreCircle.setValue(avgBadCountToday);
                float newAVGAllRides = (float) (totalAvergaeScore);


                if (newAVGAllRides > 100) {
                    newAVGAllRides = 100;
                } else if (newAVGAllRides <= 0) {
                    newAVGAllRides = 0;

                }
                Log.e("peeksColorCoding",""+peeksColorCoding);
                Collection<FitChartValue> fitChartValuesPeeks = new ArrayList<>();
                if (peeksColorCoding<=0.25 ){
                    leasePlaneWeeklyScorestats.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
                    fitChartValuesPeeks.add(new FitChartValue(avgBadCountToday, getActivity().getResources().getColor(R.color.colorPieChartGreen)));

                }else  if (peeksColorCoding>=0.25 && peeksColorCoding<=0.5){

                    leasePlaneWeeklyScorestats.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                    fitChartValuesPeeks.add(new FitChartValue(avgBadCountToday, getActivity().getResources().getColor(R.color.colorPieChartOrange)));

                }
                else if (peeksColorCoding>0.5){

                    leasePlaneWeeklyScorestats.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartRed));
                    fitChartValuesPeeks.add(new FitChartValue(avgBadCountToday, getActivity().getResources().getColor(R.color.colorPieChartRed)));

                }
                leasePlanWeeklyScoreCircle.setValues(fitChartValuesPeeks);

                int value_all_rides = (int) Math.round(newAVGAllRides);
                driverWeeklyScoreStats.setText(""+value_all_rides+"%");
                driverWeeklySCoreCircle.setValue(value_all_rides);
                Collection<FitChartValue> fitChartValues = new ArrayList<>();
                if (value_all_rides >=80){
                    driverWeeklyScoreStats.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
                    fitChartValues.add(new FitChartValue(value_all_rides, getActivity().getResources().getColor(R.color.colorPieChartGreen)));

                }else if (value_all_rides<80 && value_all_rides >=50){
                    driverWeeklyScoreStats.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                    fitChartValues.add(new FitChartValue(value_all_rides, getActivity().getResources().getColor(R.color.colorPieChartOrange)));

                }
                else if (value_all_rides< 50){
                    driverWeeklyScoreStats.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartRed));
                    fitChartValues.add(new FitChartValue(value_all_rides, getActivity().getResources().getColor(R.color.colorPieChartRed)));

                }

                driverWeeklySCoreCircle.setValues(fitChartValues);

                Log.e("avg/day-badcountavg/day" ,""+newAVGAllRides+"  ---"+avgBadCountToday);



            }else{
                driverWeeklySCoreCircle.setValue(100f);
                leasePlanWeeklyScoreCircle.setValue(2f);

            }
        }catch (IndexOutOfBoundsException e){
            Log.e("manageMyWeeklyStats",""+e);

        }
        tmp.close();

    }



    /**
     * This method allows to update the fit chart and driving time of All my rides.
     * It takes the rides from database, calculate the avg and driving time.
     */
    public int calculateAverage(JSONArray listRide, float totalTime) {
        double tempvaTotal_ = totalTime;
        double average = 0.0;
        for (int i = 0; i < listRide.length(); i++) {
            try {
                JSONObject obj = listRide.getJSONObject(i);
                double currentTime = Double.valueOf(obj.getInt("time_elapsed"));
                double currentScore = (obj.getDouble("score"));

                double divvar = currentTime / tempvaTotal_;

                average = average + currentScore * divvar;


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        //Math.round(average);
        int average_int = (int) average;
        Log.e("average", "" + average_int);

   /*     if (!DataHandler.getStringPreferences(AppConstants.SCORE_TOTAL_SCORE).isEmpty()){
            String serverAvg =DataHandler.getStringPreferences(AppConstants.SCORE_TOTAL_SCORE);
            int  intserverAvg = Integer.valueOf(serverAvg);
            int newAvg = (intserverAvg+average_int)/2;
            average_int =  Math.round(newAvg);;
        }*/
        return average_int;
    }

    public static LineData updateGraphFromServer(JSONArray driverArray , JSONArray companyArray,int driversAvgs,int companysAvg,String companyName) {

        driverWeeklyScoreStats.setText(""+driversAvgs+"%");
        leasePlaneWeeklyScorestats.setText(""+companysAvg+"%");
        companys_name.setText(companyName);

        int driversAvg = (int) Math.round(driversAvgs);
        Collection<FitChartValue> fitChartValues = new ArrayList<>();


        if (driversAvg >=80){
            driverWeeklyScoreStats.setTextColor(ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartGreen));
            fitChartValues.add(new FitChartValue(driversAvg, ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartGreen)));

        }else if (driversAvg<80 && driversAvg >=50){
            driverWeeklyScoreStats.setTextColor(ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartOrange));
            fitChartValues.add(new FitChartValue(driversAvg, ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartOrange)));

        }
        else if (driversAvg< 50){
            driverWeeklyScoreStats.setTextColor(ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartRed));
            fitChartValues.add(new FitChartValue(driversAvg, ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartRed)));

        }

        driverWeeklySCoreCircle.setValues(fitChartValues);


        int companysAvgs = (int) Math.round(companysAvg);
        Collection<FitChartValue> fitChartValuesC = new ArrayList<>();
        if (companysAvgs >=80){
            leasePlaneWeeklyScorestats.setTextColor(ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartGreen));
            fitChartValuesC.add(new FitChartValue(companysAvgs, ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartGreen)));

        }else if (companysAvgs<80 && companysAvgs >=50){
            leasePlaneWeeklyScorestats.setTextColor(ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartOrange));
            fitChartValuesC.add(new FitChartValue(companysAvgs, ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartOrange)));

        }
        else if (companysAvgs< 50){
            leasePlaneWeeklyScorestats.setTextColor(ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartRed));
            fitChartValuesC.add(new FitChartValue(companysAvgs, ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPieChartRed)));

        }

        leasePlanWeeklyScoreCircle.setValues(fitChartValuesC);

          Paint saint = new Paint();
        saint.setColor(Color.WHITE);
       // chart.getLegend().setEnabled(false);
       //
        // chart.getLegendRenderer().getLabelPaint().set(saint);



        LineData cd = null;
        String driverAvg_Score = null;
        String companyAvg_Score = null;
        String weekN0= "";
        JSONObject jsonObject_driverAvg = new JSONObject();
        JSONObject jsonObject_companyAvg = new JSONObject();
        ArrayList<Entry> driversWeeklyAvg = new ArrayList<>();
        ArrayList<Entry> companysWeeklyAvg = new ArrayList<>();
        final HashMap<Integer, String> weeksHashMap = new HashMap<>();

        List<String> weeks = new ArrayList<>();

        LineDataSet driver;
        LineDataSet company;
        for (int i = 0; i < driverArray.length(); i++) {
            try {
                jsonObject_driverAvg = driverArray.getJSONObject(i);
                driverAvg_Score = jsonObject_driverAvg.getString("avg_score");
                weekN0 =  String.valueOf(jsonObject_driverAvg.get("week"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            float graph_dirver_avg_score = Math.round(Float.parseFloat(driverAvg_Score));
            int roundAvg = (int) graph_dirver_avg_score;
            weeks.add(weekN0);
            driversWeeklyAvg.add(new Entry(i+1, roundAvg));
        }

        for (int i = 0; i < companyArray.length(); i++) {
            try {
                jsonObject_companyAvg = companyArray.getJSONObject(i);
                companyAvg_Score = jsonObject_companyAvg.getString("avg_score");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            float graph_company_avg_score = Float.parseFloat(companyAvg_Score);

            companysWeeklyAvg.add(new Entry(i+1, Math.round(graph_company_avg_score)));
        }





        // create a dataset and give it a type

        driver = new LineDataSet(driversWeeklyAvg, ApplicationController.getmAppcontext().getResources().getString(R.string.txt_fleet_driver));
        driver.setFillAlpha(110);
        driver.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
        driver.setColor(colorplotlineCompany);
        driver.setCircleColor(Color.BLUE);
        driver.setLineWidth(5f);
        driver.setCircleRadius(2.5f);
        driver.setDrawCircleHole(false);
        driver.setValueTextSize(9f);
        driver.setAxisDependency(YAxis.AxisDependency.LEFT);


        company = new LineDataSet(companysWeeklyAvg, ApplicationController.getmAppcontext().getResources().getString(R.string.txt_fleet_company));
        company.setFillAlpha(110);
        company.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);

        // set1.setFillColor(Color.RED);

        // set the line to be drawn like this "- - - - - -"
        // set1.enableDashedLine(10f, 5f, 0f);
        // set1.enableDashedHighlightLine(10f, 5f, 0f);


        //  peeks.set
        // x-axis representing days
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setEnabled(true);
        xAxis.setLabelCount(weeks.size(), true);
        xAxis.setAxisMinimum(1f);
        xAxis.setAxisMaximum(5f);
        xAxis.setTextColor(colordays);
        xAxis.enableGridDashedLine(1f, 1f, 1f);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        xAxis.setTextSize(14f);
        xAxis.setTextColor(colordays);
        xAxis.setYOffset(1f);
        int weeksOrder = 1;
        for (int i=0;i<weeks.size();i++){

            weeksHashMap.put(weeksOrder, "W-"+weeks.get(i));
            weeksOrder++;
        }
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return weeksHashMap.get((int)value);

            }
        });
        xAxis.setAvoidFirstLastClipping(true);

        company.setColor(colorplotlinedriver);
        company.setCircleColor(colorplotlinedriver);
        company.setLineWidth(3f);
        company.setCircleRadius(2.5f);
        company.setDrawCircleHole(false);
        company.setValueTextSize(9f);
        company.setAxisDependency(YAxis.AxisDependency.LEFT);
        YAxis leftAxis = chart.getAxisLeft();

        leftAxis.setAxisMaximum(103f);
        leftAxis.setAxisMinimum(-5f);
        leftAxis.setTextColor(color);
        //leftAxis.setYOffset(20f);
        leftAxis.setTextSize(12f);
        leftAxis.enableGridDashedLine(20f, 20f, 20f);

        leftAxis.setLabelCount(6, false);
        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setDecimalSeparatorAlwaysShown(false);

        leftAxis.setValueFormatter(new PercentFormatter(decimalFormat));

        YAxis rightAxis = chart.getAxisRight();
        /*rightAxis.setTypeface(mTf);*/
        rightAxis.setLabelCount(6, false);
            /*rightAxis.setDrawGridLines(true);*/
        rightAxis.setAxisMinimum(-5f); // this replaces setStartAtZero(true)
        rightAxis.setAxisMaximum(103f);
        rightAxis.setTextColor(Color.WHITE);
        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(driver); // add the datasets
        dataSets.add(company);

        // create a data object with the datasets
        LineData data = new LineData(dataSets);
        data.setValueFormatter(new DefaultValueFormatter(0));
        // set data
        chart.getDescription().setText("");
        chart.setHovered(true);
        chart.setScaleEnabled(false);
        chart.setData(data);
        chart.animateX(950);
        //for removing the the squares of pf color i.e legends of description

        chart.getLegend().setEnabled(false);
        //chart.setExtraBottomOffset(1f);
      //  chart.getLegendRenderer().getFormPaint().setColor(Color.WHITE);
      /*  Legend legend = chart.getLegend();
        //legend.setEnabled(false);
        legend.getColors()*/
        cd = new LineData();
        return cd;
    }


    /*private void setData() {


        ArrayList<Entry> leasePlanVals = setXXAxisValues();

        ArrayList<Entry> scoreVals = setYAxisValues();

        LineDataSet peeks;
        LineDataSet score;


        // create a dataset and give it a type
        if (leasePlanVals.size()>0 && scoreVals.size()>0){
            peeks = new LineDataSet(leasePlanVals, "PEEKS");
            peeks.setFillAlpha(110);
            peeks.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            peeks.setColor(colorplotlineCompany);
            peeks.setCircleColor(Color.BLUE);
            peeks.setLineWidth(3f);
            peeks.setCircleRadius(2.5f);
            peeks.setDrawCircleHole(false);
            peeks.setValueTextSize(9f);
            peeks.setAxisDependency(YAxis.AxisDependency.RIGHT);

            score = new LineDataSet(scoreVals, "SCORE");
            score.setFillAlpha(110);
            score.setMode(LineDataSet.Mode.CUBIC_BEZIER);

            // set1.setFillColor(Color.RED);

            // set the line to be drawn like this "- - - - - -"
            // set1.enableDashedLine(10f, 5f, 0f);
            // set1.enableDashedHighlightLine(10f, 5f, 0f);


            //  peeks.set
            // x-axis representing days
            XAxis xAxis = chart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setEnabled(true);
            xAxis.setLabelCount(5,false);
            xAxis.setAxisMinimum(0f);
            xAxis.setAxisMaximum(5f);
            xAxis.setTextColor(colordays);
            xAxis.enableGridDashedLine(1f,1f,1f);
            xAxis.setDrawGridLines(true);
            xAxis.setDrawAxisLine(true);
            xAxis.setTextSize(15f);
            xAxis.setTextColor(colordays);



            score.setColor(colorplotlinedriver);
            score.setCircleColor(colorplotlinedriver);
            score.setLineWidth(3f);
            score.setCircleRadius(2.5f);
            score.setDrawCircleHole(false);
            score.setValueTextSize(9f);

            YAxis leftAxis = chart.getAxisLeft();
            leftAxis.setAxisMaximum(101f);
            leftAxis.setAxisMinimum(-1f);
            leftAxis.setTextColor(color);
            //leftAxis.setYOffset(20f);
            leftAxis.enableGridDashedLine(20f, 20f, 20f);


            leftAxis.setLabelCount(6, false);
            YAxis rightAxis = chart.getAxisRight();
            rightAxis.setAxisMinimum(-1f);
            rightAxis.setAxisMaximum(101f);
            rightAxis.enableGridDashedLine(20f, 20f,20f);
            rightAxis.setTextColor(color);
            rightAxis.setTextSize(9f);

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(peeks); // add the datasets
            dataSets.add(score);

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            // set data
            chart.setHovered(true);

            chart.setData(data);
            chart.animateX(950);
        }

       *//* Legend l = chart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.CIRCLE);*//*
    }
*//*
    *//*
    *//**
     * generates a random ChartData object with just one DataSet
     *
     * @return
     *//**//*

    public static LineData generateDataLine(*//*
*//*JSONArray driverArray , JSONArray companyArray*//**//*
) {
        LineData cd = null;
        String driverAvg_Date = null;
        String driverAvg_Score = null;
        String companyAvg_Date = null;
        String companyAvg_Score = null;
        ArrayList<Entry> e1 = new ArrayList<Entry>();
        ArrayList<Entry> e2 = new ArrayList<Entry>();
        ArrayList<Entry> e3 = new ArrayList<Entry>();
        ArrayList<Entry> xVals = new ArrayList<>();
        ArrayList<Entry> yVals = new ArrayList<>();
        xVals = setXAxisValues();
        yVals = setYAxisValues();

*//*
*//*
        if(driverArray !=null && companyArray !=null){
*//**//*


 *//*
*//*       if (driverArray !=null) {
            for(int i = 0;i < driverArray.length();i++){

                try {
                    jsonObject_driverAvg = driverArray.getJSONObject(i);
                     Log.e("DriverAvgObject "," : "+jsonObject_driverAvg);

                    *//**//*
*//*
*//*driverAvg_Date = jsonObject_driverAvg.getString("date");
                    Log.e("GRAPH_driverAvg_Date", driverAvg_Date);*//**//*
*//*
*//*

                    driverAvg_Score = jsonObject_driverAvg.getString("value");
                    Log.e("GRAPH_drivingAvg_Score",driverAvg_Score);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                float graph_dirver_avg_score = Float.parseFloat(driverAvg_Score);
                e1.add(new Entry(i,graph_dirver_avg_score));


            }
        }*//**//*



     *//*
*//*   if(companyArray !=null){
            for(int i=0;i<companyArray.length();i++){
                try {
                    jsonObject_companyAvg = companyArray.getJSONObject(i);
                    Log.e("CompanyAvgObject "," : "+jsonObject_companyAvg);

                    companyAvg_Date = jsonObject_companyAvg.getString("date");
                    Log.e("companyAvg_Date",companyAvg_Date);
                    companyAvg_Score =jsonObject_companyAvg.getString("value");
                    Log.e("companyAvg_Score",companyAvg_Score);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                float graph_company_avg_score  = Float.parseFloat(companyAvg_Score);
                //e2.add(new Entry(i, e1.get(i).getY() - 30));
                e2.add(new Entry(i, graph_company_avg_score));

            }


        }*//**//*


            LineDataSet d1 = new LineDataSet(xVals, "");


            d1.setLineWidth(3.5f);
            //d1.setCircleRadius(4.5f);
            d1.setColor(colorplotlinedriver);
            d1.setDrawValues(true);
            d1.disableDashedLine();
            d1.setDrawCircleHole(false);


            LineDataSet d2 = new LineDataSet(yVals, "");
            d2.setLineWidth(3.5f);
            //d2.setCircleRadius(4.5f);
            d2.setColor(colorplotlineCompany);
            //d2.setColor(ColorTemplate.VORDIPLOM_COLORS[0]);

            d2.setDrawValues(true);
            d2.disableDashedLine();
            d2.setDrawCircleHole(true);



            ArrayList<ILineDataSet> sets = new ArrayList<ILineDataSet>();
            sets.add(d1);
            sets.add(d2);

            // apply styling
            chart.getDescription().setEnabled(false);
            chart.setDrawMarkers(true);
            chart.setDrawGridBackground(false);
            chart.setScaleEnabled(true);



            XAxis xAxis = chart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.TOP);
            xAxis.setEnabled(false);
            xAxis.setLabelCount(5);
            xAxis.setAxisMinimum(1f);
            xAxis.setAxisMaximum(5f);
            xAxis.setDrawGridLines(true);
            xAxis.setDrawAxisLine(true);


            YAxis leftAxis = chart.getAxisLeft();
            leftAxis.setAxisMaxValue(100f);
            leftAxis.setAxisMinValue(0f);

            leftAxis.setTextColor(color);
            //leftAxis.setYOffset(20f);
            leftAxis.enableGridDashedLine(20f, 20f, 20f);
        *//*
*//*leftAxis.setTypeface(mTf);*//**//*

            leftAxis.setLabelCount(6, false);
            *//*
*//*leftAxis.setDrawGridLines(true);*//**//*

            leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
            leftAxis.setAxisMaximum(100f);
            leftAxis.setTextSize(15f);
            YAxis rightAxis = chart.getAxisRight();
        *//*
*//*rightAxis.setTypeface(mTf);*//**//*

            rightAxis.setLabelCount(6, false);
            *//*
*//*rightAxis.setDrawGridLines(true);*//**//*

            rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
            rightAxis.setAxisMaximum(15f);
            rightAxis.enableGridDashedLine(3f, 3f, 3f);
            rightAxis.setTextColor(color);
            rightAxis.setTextSize(15f);

            // do not forget to refresh the chart
            // holder.chart.invalidate();
            chart.animateX(750);


            cd = new LineData(sets);
            // set data
            chart.setData(cd);


        *//*
*//*}*//**//*

        return cd;
    }
*//*

*/
// This is used to store Y-axis values
    private ArrayList<Entry> setXXAxisValues(){

        ArrayList<Entry> scores = new ArrayList<Entry>();
        ArrayList<Float> plottingPoints = new ArrayList<>();
        try {
            RideBDD rideBDD = new RideBDD(getActivity());
            rideBDD.open();
            long timestampnendDate = AppUtils.getEndOfDayInMillis(currentDate);
            plottingPoints = rideBDD.last5daysgraphplottingPointsPeeks(timestampnendDate);

            if (plottingPoints != null && plottingPoints.size() > 0) {


                for (int i = 0; i < plottingPoints.size(); i++) {
                    scores.add(new Entry(i, Math.round(plottingPoints.get(i))));

                }
            }

            //first value represts days and second value represents score or peek
/*    yVals.add(new Entry(0, 5));
    yVals.add(new Entry(1, 10));
    yVals.add(new Entry(2, 7));
    yVals.add(new Entry(3, 12));
    yVals.add(new Entry(5, 0));*/
  /*  yVals.add(new Entry(50.5f, 11));
    yVals.add(new Entry(90, 13));
    yVals.add(new Entry(150.9f, 14));*/

            rideBDD.close();

        }catch (IndexOutOfBoundsException e){
            Log.e("setXXAxisValues",""+e);
        }
        return scores;
    }

    // This is used to store Y-axis values
    private ArrayList<Entry> setYAxisValues(){
        ArrayList<Entry> score = new ArrayList<Entry>();
        ArrayList<Float> plottingPoints= new ArrayList<>();
        try {

            RideBDD rideBDD = new RideBDD(getActivity());
            rideBDD.open();
            long timestampnendDate = AppUtils.getEndOfDayInMillis(currentDate);

            plottingPoints = rideBDD.last5daysgraphplottingPointsScore(timestampnendDate);
            if (plottingPoints != null && plottingPoints.size() > 0) {


                for (int i = 0; i < plottingPoints.size(); i++) {
                    score.add(new Entry(i, Math.round(plottingPoints.get(i))));
                }
            }

/*
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        yVals.add(new Entry(0, 60));
        yVals.add(new Entry(1, 70));
        yVals.add(new Entry(2, 90));
        yVals.add(new Entry(3, 50));
        yVals.add(new Entry(4, 100));
        yVals.add(new Entry(5, 24));*/
/*        yVals.add(new Entry(70.5f, 2));
        yVals.add(new Entry(100, 3));
        yVals.add(new Entry(180.9f, 4));*/
            rideBDD.close();
        }catch (IndexOutOfBoundsException e){
            Log.e("setXXAxisValues",""+e);
        }
        return score;
    }
    private ArrayList<String> setXAxisValues(){
        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add("10");
        xVals.add("20");
        xVals.add("30");
        xVals.add("30.5");
        xVals.add("40");

        return xVals;
    }
 /*   private static ArrayList<Entry> setYAxisValues(){
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        yVals.add(new Entry(10, 10));
        yVals.add(new Entry(50, 70));
        yVals.add(new Entry(90, 100));

        return yVals;
    }*/

/*    private static ArrayList<Entry> setXAxisValues(){
        ArrayList<Entry> xVals = new ArrayList<Entry>();
        xVals.add(new Entry(0,2));
        xVals.add(new Entry(4,6));
        xVals.add(new Entry(8,12));


        return xVals;
    }*/

}
