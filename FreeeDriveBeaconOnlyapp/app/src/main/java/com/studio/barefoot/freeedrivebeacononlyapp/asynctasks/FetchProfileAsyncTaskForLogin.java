package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.MenuActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.VerificationActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;

import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.SuccessDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.services.BackgroundBeaconScan;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.LoginActivity.progressBarDialogLogin;
import static com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.ChangePhoneNumberAsyncTask.progressBarDialogSyncDataFromUpdatePhoneNUmberDialog;
import static com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.LoginAsyncTask.progressBarDialogSyncData;

/**
 * Created by mcs on 12/28/2016.
 */

public class FetchProfileAsyncTaskForLogin extends BaseAsyncTask {


    private JSONArray jsonArray;
    private String messenger="";
    private String uuid="";
    RideBDD rideBDD;
    ArrayList<String> stringListScores = new ArrayList<>();
    private String selectedLang ="";
    String time_zone = "";
        private String accidentEmail="";
        private String accidentPhone="";

    public FetchProfileAsyncTaskForLogin(Context context, String route, List<NameValuePair> pp) {
        super(context, route, pp);
    }



    /**
     * AsyncTask method basic calls during a request, calls the parent's method.
     */
    protected String doInBackground(String... params) {

        return "";
    }
    /**
     * AsyncTask method basic calls after a request.
     */
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s != null) {
            try{
                progressBarDialogSyncDataFromUpdatePhoneNUmberDialog.dismiss();
                progressBarDialogSyncDataFromUpdatePhoneNUmberDialog =null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }
            int intResponse = Integer.parseInt(response);
            Log.e("ResponseCode", "" + intResponse);
            Log.e("response", "" + response);
            Log.e("resultat", "" + resultat);
            JSONObject jsonObject = new JSONObject();
            JSONArray syncresponse = new JSONArray();
            JSONObject drivingPerformanceObject = new JSONObject();
            JSONObject drivePad = new JSONObject();
            boolean fd_active = false;

            switch (intResponse) {
                case 200:

                try{

                    if (resultat!=null || !resultat.isEmpty()) {

                        JSONObject objAccount = new JSONObject(resultat);
                        if (objAccount.getJSONArray("scores") != null && !objAccount.getJSONArray("scores").equals("")) {

                            syncresponse = objAccount.getJSONArray("scores");

                        }
                        if (objAccount != null) {

                            accidentEmail = objAccount.getString("accident_help_email");
                            accidentPhone = objAccount.getString("accident_help_phone_number");


                            DataHandler.updatePreferences(AppConstants.PREF_KEY_ACCIDENT_HELP_EMAIL,accidentEmail);
                            DataHandler.updatePreferences(AppConstants.PREF_KEY_ACCIDENT_HELP_PHONE,accidentPhone);
                        }
                        if (objAccount.getJSONObject("contract") != null && !objAccount.getJSONObject("contract").equals("0")) {
                            drivingPerformanceObject = objAccount.getJSONObject("contract");
                        }
                        String driverPadCheck = "";
                        driverPadCheck= objAccount.get("DrivePad").toString();

                        // since the server is sending null as a string not an empty container {}
                        if (driverPadCheck.equalsIgnoreCase("null")){

                        }else{
                            drivePad = objAccount.getJSONObject("DrivePad");

                        }

                     /*   if(objAccount.getJSONObject("DrivePad")!= null && objAccount.getJSONObject("DrivePad").length()>0 && !objAccount.getJSONObject("DrivePad").isNull("DrivePad")){
                            drivePad = objAccount.getJSONObject("DrivePad");

                        }*/
                        for (int i = 0; i < objAccount.length(); i++) {



                                if (jsonObject != null) {
                                    jsonObject.put("first_name", objAccount.getString("first_name"));
                                    jsonObject.put("city", objAccount.getString("city"));
                                    jsonObject.put("last_name", objAccount.getString("last_name"));
                                    jsonObject.put("email", objAccount.getString("email"));
                                    jsonObject.put("phone_number", objAccount.getString("phone_number"));
                                    jsonObject.put("lang", objAccount.getString("lang"));
                                    selectedLang = objAccount.getString("lang");

                                    jsonObject.put("uuid", objAccount.getString("uuid"));
                                    // for displaing synced data on a re-install
                                    jsonObject.put("total_bad_counts", objAccount.getString("total_bad_counts"));
                                    jsonObject.put("total_time_elapsed", objAccount.getString("total_time_elapsed"));

                                    jsonObject.put("avg_score", objAccount.getString("avg_score")).toString();
                                    //last ride

                                    jsonObject.put("last_time_elapsed", objAccount.getString("last_time_elapsed"));

                                    jsonObject.put("last_arrival_time", objAccount.getString("last_arrival_time"));

                                    jsonObject.put("last_count_bad_behaviour", objAccount.getString("last_count_bad_behaviour"));

                                    jsonObject.put("last_departure_time", objAccount.getString("last_departure_time"));

                                    jsonObject.put("active", objAccount.getBoolean("active"));
                                    jsonObject.put("avg_performance", objAccount.getInt("avg_performance"));
                                   /* DataHandler.updatePreferences(AppConstants.ACCESS_PRIVATE_HOURS, objAccount.getInt("private_hour_enable"));
                                    if (objAccount.getInt("private_hour_enable") == 0 || objAccount.getInt("private_hour_enable") == 1) {
                                        DataHandler.updatePreferences(AppConstants.ACCESS_PRIVATE_HOURS_UPDATED, true);
                                    }*/
                                    if (objAccount.get("avg_performance") != null && !objAccount.getString("avg_performance").equalsIgnoreCase("")) {
                                        int avg_performance = objAccount.getInt("avg_performance");
                                        DataHandler.updatePreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_SCORE, avg_performance);
                                    }
                                    DataHandler.updatePreferences(AppConstants.TOKEN_NUMBER, objAccount.getString("token"));
                                    Log.e("tokenL", objAccount.getString("token"));
                                    //for syncing entire data


                                    //insertDisplayValues(last_time_elapsed,last_arrival_time,last_count_bad_behaviour,totalTime,totalScore,totalBadCounts);

                                    uuid = objAccount.getString("uuid");
                                    DataHandler.updatePreferences(AppConstants.TEMP_DISPLAY_KEY, jsonObject.toString());
                                    //Log.e("jsonObject"," : "+jsonObject.toString());
                                }
                        }
                            if(drivePad != null && drivePad.length() > 0){
                                String drivepadName = "";
                                for(int j = 0; j < drivePad.length(); j++){
                                    drivepadName = drivePad.getString("name");
                                    Log.e("drivepadName",drivepadName);
                                }
                                DataHandler.updatePreferences(AppConstants.PREF_KEY_DRIVE_PAD_NAME,drivepadName);
                            }
                            if (drivingPerformanceObject != null && drivingPerformanceObject.length() > 0) {
                                boolean driving_performanceActive = false;
                                for (int k = 0; k < drivingPerformanceObject.length(); k++) {

                                    driving_performanceActive = drivingPerformanceObject.getBoolean("driving_performance");
                                    Log.e("driving_performance", ":" + driving_performanceActive);
                                 /*if (!driving_performanceActive.equalsIgnoreCase("")){
                                     break;
                                 }*/
                                }
                                if (driving_performanceActive == true) {
                                    DataHandler.updatePreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE, true);
                                } else {
                                    DataHandler.updatePreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE, false);

                                }
                            }
                       //kept here to delete the demo rides after sync is called
                        deleteAlreadySycedRides();

                        if (syncresponse != null && syncresponse.length() > 0) {
                                for (int j = 0; j < syncresponse.length(); j++) {
                                    JSONObject ScorejsonObject = syncresponse.getJSONObject(j);
/*
                                int remote_id = ScorejsonObject.getInt("remote_id");
*/
                                    String departure_time = ScorejsonObject.getString("departure_time");
                                    int driver_id = ScorejsonObject.getInt("driver_id");
                                    int company_id = ScorejsonObject.getInt("company_id");
                                    String arrival_time = ScorejsonObject.getString("arrival_time");
                                    int time_elapsed = ScorejsonObject.getInt("time_elapsed");
                                    double score = ScorejsonObject.getDouble("score");

                                    String departure_location = ScorejsonObject.getString("departure_location");
                                    String arrival_location = ScorejsonObject.getString("arrival_location");
                                    int count_bad_behaviour = ScorejsonObject.getInt("count_bad_behaviour");
                                    int high_ride_time = ScorejsonObject.getInt("high_ride_time");
                                    int high_ride_distance = ScorejsonObject.getInt("high_ride_distance");
                                    int sudden_accelerate = ScorejsonObject.getInt("sudden_accelerate");
                                    int sudden_braking = ScorejsonObject.getInt("sudden_brake");
                                    int performance_Count = ScorejsonObject.getInt("over_speeding_count");
                                    int orphanrideBit = ScorejsonObject.getInt("orphan_ride");
                                    String departureDate = ScorejsonObject.getString("departure_date");
                                    int passengerMode = ScorejsonObject.getInt("passenger_mode_timeelapsed");
                                 //   int count_bad_behaviour_below_speed = ScorejsonObject.getInt("count_bad_behaviour_below_speed");
                                    if (ScorejsonObject.getString("time_zone").equalsIgnoreCase("")) {
                                        time_zone = "0";
                                    } else {
                                        time_zone = ScorejsonObject.getString("time_zone");
                                    }


                                    String created_at = ScorejsonObject.getString("created_at");

                                    insertServerRidesInDB(departure_time, arrival_time, departure_location, arrival_location, time_elapsed, score, driver_id, company_id, count_bad_behaviour, high_ride_time, high_ride_distance, sudden_accelerate, sudden_braking, created_at, performance_Count, time_zone, departureDate,passengerMode,orphanrideBit);

                                }
                            }

                            String dashless = "";
                            if (uuid.contains("-")) {
                                dashless = AppUtils.removeDashes(uuid);
                                DataHandler.updatePreferences(AppConstants.UUID, dashless);

                            } else {
                                dashless = uuid;
                                DataHandler.updatePreferences(AppConstants.UUID, dashless);

                            }

                            if (dashless.length() == 32) {
                                final int mid = 20; //get the middle of the String
                                String[] parts = {dashless.substring(0, mid), dashless.substring(mid)};
                                String nameSpace = parts[0];
                                String instanceID = parts[1];
                                DataHandler.updatePreferences(AppConstants.UUID_NAME_SPACE, nameSpace);
                                DataHandler.updatePreferences(AppConstants.UUID_INSTANCE_ID, instanceID);
                                context.stopService(new Intent(context, BackgroundBeaconScan.class));
                                context.startService(new Intent(context,BackgroundBeaconScan.class));
                                Intent serviceIntent = new Intent(context, BackgroundBeaconScan.class);
                                serviceIntent.setAction("Connect");
                                context.startService(serviceIntent);
                                Log.e("uuid", "" + parts[0]);
                                Log.e("instanceID", "" + parts[1]);
                            }


                            try {
                                objAccount = null;
                                syncresponse = null;
                            } catch (Exception e) {

                            }

                            DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY, true);
                            DataHandler.updatePreferences(AppConstants.PREF_KEY_LANG, selectedLang);

                            ApplicationController controller = (ApplicationController) ((Activity) context).getApplication();
                            controller.updateLocale(selectedLang);


                        /*Intent menuAcitivtyIntent = new Intent(context, MenuActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(menuAcitivtyIntent);*/
                            try {
                                progressBarDialogSyncData.dismiss();
                                progressBarDialogSyncData = null;
                            } catch (NullPointerException exception) {
                                exception.printStackTrace();
                            }
                            try {
                                progressBarDialogSyncDataFromUpdatePhoneNUmberDialog.dismiss();
                                progressBarDialogSyncDataFromUpdatePhoneNUmberDialog = null;
                            } catch (NullPointerException exception) {
                                exception.printStackTrace();
                            }
                            if(DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)){
                                Intent menuAcitivtyIntent = new Intent(context, MenuActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(menuAcitivtyIntent);
                            }else{
                                SuccessDialog successDialog = new SuccessDialog(context);
                                successDialog.show();
                                successDialog = null;
                            }

                        }



                }catch (NullPointerException e){
                    e.printStackTrace();
                }

                catch (JSONException e) {

                    e.printStackTrace();
                }catch (Exception e) {
                    e.printStackTrace();

                }
                    break;

                /*case 200 :

                    break;
                case 409:

                    break;
                case 401:
                    //Number already exists
                    AlertDialog.Builder error_401 = new AlertDialog.Builder(context);
                    error_401.setMessage(context.getResources().getString(R.string.error_login_401)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_401.show();
                case 403:
                    AlertDialog.Builder error_422 = new AlertDialog.Builder(context);
                    error_422.setMessage(context.getResources().getString(R.string.error_login_403)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_422.show();
                    break;*/
                case 500:
                    try{
                        progressBarDialogSyncData.dismiss();
                        progressBarDialogSyncData =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }

                    try{
                        progressBarDialogSyncDataFromUpdatePhoneNUmberDialog.dismiss();
                        progressBarDialogSyncDataFromUpdatePhoneNUmberDialog =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    try{
                        AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                        error_500.setMessage(context.getResources().getString(R.string.error_login_404)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_500.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    break;
            }
        }else{


            try{
                progressBarDialogSyncDataFromUpdatePhoneNUmberDialog.dismiss();
                progressBarDialogSyncDataFromUpdatePhoneNUmberDialog =null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }
            try{
                progressBarDialogSyncData.dismiss();
                progressBarDialogSyncData =null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }

        }
    }

    private void deleteAlreadySycedRides() {
        RideBDD tmp = new RideBDD(context);
        tmp.open();
        tmp.deleteDataInDb();
        tmp.close();
    }

    private void insertServerRidesInDB(String deptTime,String arrivalTime,String deptLoc,String arrivalLoc,int timeLapsed,double score,int driverId,int companyID,int countBad,int highRideTime,int highRideDist,int suddenAccelerate,int suddenBraking,String createdAt,int performance_Count,String time_zone,String date,int passengerMode,int orphanRide) {
        RideBDD tmp = new RideBDD(context);
        tmp.open();
        tmp.insertFetchedRides(deptTime,arrivalTime,deptLoc,arrivalLoc,timeLapsed,score,driverId,companyID,countBad,highRideTime,highRideDist,suddenAccelerate,suddenBraking,createdAt,performance_Count,time_zone,date,passengerMode,orphanRide);
        tmp.close();
    }


}
