package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.studio.barefoot.freeedrivebeacononlyapp.AccidentHelpActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.InsuranceActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.AccidentHelpAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.AccidentHelpBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.AccidentHelpActivity.accidentHelpAdapter;
import static com.studio.barefoot.freeedrivebeacononlyapp.AccidentHelpActivity.accidentHelpBeans;
import static com.studio.barefoot.freeedrivebeacononlyapp.AccidentHelpActivity.listViewAccidentHelp;
import static com.studio.barefoot.freeedrivebeacononlyapp.AccidentHelpActivity.progressBarAccidentHelp;

import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.fdLogout;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler.FILE_NAME_SHARED_PREF;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler.deletePreference;

/**
 * Created by Yasir Barefoot on 9/6/2017.
 */

public class AccidentHelpDetailsAsyncTask extends BaseAsyncTask {

    private String accidentPhone = "";
    private String accidentEmail = "";

    public AccidentHelpDetailsAsyncTask(Context context, String route, List<NameValuePair> pp) {
        super(context, route, pp);
    }

    /**
     * AsyncTask method basic calls during a request, calls the parent's method.
     */
    protected String doInBackground(String... params) {

        return "";
    }

    /**
     * AsyncTask method basic calls after a request.
     */
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (s != null) {
            int intResponse = Integer.parseInt(response);
            Log.e("ResponseCode", "" + intResponse);
            Log.e("response", "" + response);
            Log.e("resultat", "" + resultat);
            JSONArray jsonArray = null;
            AccidentHelpBeans accidentHelp = null;
           /* try{
                progressBarDialogProfile.dismiss();
                progressBarDialogProfile = null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }*/

            try {
                progressBarAccidentHelp.dismiss();
                progressBarAccidentHelp = null;
            } catch (NullPointerException exception) {
                exception.printStackTrace();
            }
            switch (intResponse) {
                case 200:
                    try {
                        if (resultat != null || !resultat.isEmpty()) {
                            JSONObject objAccount = new JSONObject(resultat);

                            if (objAccount != null) {

                                 accidentEmail = objAccount.getString("email");
                                 accidentPhone = objAccount.getString("phone_number");

                                jsonArray = objAccount.getJSONArray("accident_fleet_message_steps");

                                accidentHelpBeans = null;
                                accidentHelpBeans = new ArrayList<>();
                                if (jsonArray.length() > 0) {
                                    for (int k = 0; k < jsonArray.length(); k++) {

                                        accidentHelp = new AccidentHelpBeans();
                                        objAccount = jsonArray.getJSONObject(k);
                                        accidentHelp.setAccidenthelpCount(k+1);
                                        accidentHelp.setTitle(objAccount.getString("value"));
                                        String accidentPhone = objAccount.getString("number");
                                        if(accidentPhone!=null && !accidentPhone.equalsIgnoreCase("null")){
                                            accidentHelp.setPhonenumber(accidentPhone);

                                        }
                                        accidentHelpBeans.add(accidentHelp);
                                    }
                                }
                                Log.e("Accident Help", " : " + accidentHelpBeans.size());
                                    /*Boolean isSend = (objAccount.getString("policy_heading")!=null) ? true : false;

                                    jsonObject.put("policy_number",objAccount.getString("policy_number"));
                                    jsonObject.put("phone_number",objAccount.getString("phone_number"));
                                    jsonObject.put("email",objAccount.getString("email"));
                                    jsonObject.put("damage_case_step",objAccount.getString("damage_case_step"));
                                    jsonObject.put("fleet_message",objAccount.getString("fleet_message"));
                                    jsonObject.put("image",objAccount.getString("image"));
                                    jsonObject.put("blobImage",objAccount.getString("blobImage"));
                                    if ((isSend)) jsonObject.put("policy_heading",objAccount.getString("policy_heading"));*/

                                DataHandler.updatePreferences(AppConstants.PREF_KEY_ACCIDENT_HELP_EMAIL,accidentEmail);
                                DataHandler.updatePreferences(AppConstants.PREF_KEY_ACCIDENT_HELP_PHONE,accidentPhone);
                            }

                            accidentHelpAdapter = new AccidentHelpAdapter(accidentHelpBeans);
                            listViewAccidentHelp.setAdapter(accidentHelpAdapter);
                            accidentHelpAdapter.notifyDataSetChanged();



                            if (jsonArray.length() <= 0) {
                                /*tv_nofleetdata.setText(context.getResources().getString(R.string.fleet_statistics));

                                tv_nofleetdata.setVisibility(View.VISIBLE);*/
                            }
                            /*Log.e("insuranceP",jsonObject.toString());
                            DataHandler.updatePreferences(AppConstants.INSURANCE_POLICY_KEY,jsonObject.toString());
                            Intent intentProfile =new Intent(context,InsuranceActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intentProfile.putExtra("comingFromNetwork","1");
                            context.startActivity(intentProfile);*/

                            listViewAccidentHelp = null;
                            try {
                                progressBarAccidentHelp.dismiss();
                                progressBarAccidentHelp = null;
                            } catch (NullPointerException exception) {
                                exception.printStackTrace();
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("JSONExceptionInsurance", "" + e);
                    }
                    break;
                case 404:
                    //Number already exists
                    AlertDialog.Builder error_401 = new AlertDialog.Builder(context);
                    error_401.setMessage(context.getResources().getString(R.string.error_Insurance_404)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_401.show();
                    break;
                case 400:
                    //Phone numbre not found
                    AlertDialog.Builder error_400 = new AlertDialog.Builder(context);
                    error_400.setMessage(context.getResources().getString(R.string.error_auth_token_expire_400)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deletePreference(FILE_NAME_SHARED_PREF);
                            fdLogout(context);
                            DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY, false);
                            dialog.dismiss();
                        }
                    });
                    error_400.show();
                    break;
                case 500:
                    AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                    error_500.setMessage(context.getResources().getString(R.string.error_login_404)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_500.show();
                    try {
                        progressBarAccidentHelp.dismiss();
                        progressBarAccidentHelp = null;
                    } catch (NullPointerException exception) {
                        exception.printStackTrace();
                    }
                    break;
            }
        } else {
            try {
                progressBarAccidentHelp.dismiss();
                progressBarAccidentHelp = null;
            } catch (NullPointerException exception) {
                exception.printStackTrace();
            }
        }
    }
}
