package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.MenuActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.QrCodeActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.SmsConfirmationActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.LoginActivity.progressBarDialogLogin;

/**
 * Created by mcs on 12/28/2016.
 */

public class LoginAsyncTask extends BaseAsyncTask {
    private JSONArray jsonArray;
    public static ProgressBarDialog progressBarDialogSyncData;


    public LoginAsyncTask(Context context, String route, List<NameValuePair> pp) {
        super(context, route, pp);
    }

    /**
     * AsyncTask method basic calls during a request, the parent's method is called
     */
    protected String doInBackground(String... params) {
        return "";
    }

    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if(s != null) {
            int intResponse = Integer.parseInt(response);
            Log.e("Response",""+intResponse);
            try{
                Log.e("token",resultat);

            }catch (NullPointerException exception){
                exception.printStackTrace();
            }

            switch (intResponse){
                case 200 : //success
                     if(!resultat.isEmpty()){
                         String token = resultat;
                         token =  token.replaceAll("\"", "");
                         DataHandler.updatePreferences(AppConstants.TOKEN_NUMBER,token);
                         DataHandler.updatePreferences(AppConstants.WABCO_ACCOUNT_VERIFY,false);
                         Log.e("Token Login",resultat);
                     }

                    //sms verfified and uuid is also verified
                  //  if (DataHandler.getStringPreferences(AppConstants.UUID).equalsIgnoreCase("")||DataHandler.getStringPreferences(AppConstants.UUID)==null){
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    FetchUserDetails();

                  //  }else{
                    /*    Intent menuAcitivtyIntent = new Intent(context, MenuActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(menuAcitivtyIntent);
                    }*/

                 break;
                case 202 :
                    DataHandler.updatePreferences(AppConstants.WABCO_ACCOUNT_VERIFY,true);
                    context.startActivity(new Intent(context, QrCodeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    break;
                case 203 :
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    FetchUserDetails();
                    break;
                case 409://sms confirmation
                /*    Intent menuAcitivtyIntent2 = new Intent(context, MenuActivity.class);
                    context.startActivity(menuAcitivtyIntent2);*/
                    Intent smsCodeIntent = new Intent(context, SmsConfirmationActivity.class);
                  //  smsCodeIntent.putExtra(AppConstants.EXTRA_KEY_LOGIN_ACTIVITY,AppConstants.EXTRA_VALUE_LOGIN_ACTIVITY);
                    context.startActivity(smsCodeIntent);
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;
                case 401://user not registered
                    try{
                        AlertDialog.Builder error_401 = new AlertDialog.Builder(context);
                        error_401.setMessage(context.getResources().getString(R.string.error_login_401aa)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_401.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                     break;
                case 403:// User deleted his account
                    try{
                        AlertDialog.Builder error_422 = new AlertDialog.Builder(context);
                        error_422.setMessage(context.getResources().getString(R.string.error_login_403aa)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_422.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;

                case 404:
                    try{
                        AlertDialog.Builder error_404 = new AlertDialog.Builder(context);
                        error_404.setMessage(context.getResources().getString(R.string.error_login_404)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_404.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;
                case 405:
                    Intent qrCodeIntent = new Intent(context, QrCodeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(qrCodeIntent);
                 /*   DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY,true);
                    Intent menuAcitivtyIntent3 = new Intent(context, MenuActivity.class);
                    context.startActivity(menuAcitivtyIntent3);*/
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;
                case 406:
                    //In case when $$$$$ qrcode
                    Intent qrocdeRecoveryIntent = new Intent(context,QrCodeActivity.class);
                    qrocdeRecoveryIntent.putExtra(AppConstants.QR_CODE_RECOVERY,AppConstants.QR_CODE_RECOVERY_key);
                    context.startActivity(qrocdeRecoveryIntent);
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;
                case 408:
                    try{
                        AlertDialog.Builder error_408 = new AlertDialog.Builder(context);
                        error_408.setMessage(context.getResources().getString(R.string.error_407_device_exit)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_408.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;
                    //demo mode user
                case 411:
                    DataHandler.updatePreferences(AppConstants.DEMO_MODE,true);
                    FetchUserDetails();
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;
                    // demo mode user has changed the device
                case 412:
                    Intent smsCodeIntentFDemo = new Intent(context, SmsConfirmationActivity.class);
                    smsCodeIntentFDemo.putExtra("HIDEEDITVIEW","HIDEEDITVIEW");
                    //  smsCodeIntent.putExtra(AppConstants.EXTRA_KEY_LOGIN_ACTIVITY,AppConstants.EXTRA_VALUE_LOGIN_ACTIVITY);
                    context.startActivity(smsCodeIntentFDemo);
                    DataHandler.updatePreferences(AppConstants.DEMO_MODE,true);
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;

                // manual intervention

                case 422:
                    try{
                        AlertDialog.Builder error_408 = new AlertDialog.Builder(context);
                        error_408.setMessage(context.getResources().getString(R.string.error_413_demo)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_408.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;
                case 593:
                    //uuid does not exists
                    context.startActivity(new Intent(context,QrCodeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    DataHandler.updatePreferences((AppConstants.USER_AWAITING_QR_CODE),true);

                    break;
                case 500:
                    try{
                        AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                        error_500.setMessage(context.getResources().getString(R.string.error_login_404)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_500.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    try{
                        progressBarDialogLogin.dismiss();
                        progressBarDialogLogin =null;
                    }catch (NullPointerException exception){
                        exception.printStackTrace();
                    }
                    break;

            }
        }else {
            try{
                progressBarDialogLogin.dismiss();
                progressBarDialogLogin =null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }

        }
        }

    private void FetchUserDetails() {
        progressBarDialogSyncData = new ProgressBarDialog(context);
        /*progressBarDialogSyncData.setTitle(context.getString(R.string.title_progress_dialog));
        progressBarDialogSyncData.setMessage(context.getString(R.string.body_progress_dialog));*/
        progressBarDialogSyncData.setTitle("Syncing");
        progressBarDialogSyncData.setMessage("wait..");
        progressBarDialogSyncData.show();
        List<NameValuePair> mParams = new ArrayList<NameValuePair>();
        final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = telephonyManager.getDeviceId();
        mParams.add(new BasicNameValuePair("devID",deviceId));
        mParams.add(new BasicNameValuePair("phone_number",DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));

        FetchProfileAsyncTaskForLogin fetchProfileAsyncTask = new FetchProfileAsyncTaskForLogin(context, WebServiceConstants.END_POINT_FETCH_PROFILE,mParams);
        fetchProfileAsyncTask.execute();
    }
}
