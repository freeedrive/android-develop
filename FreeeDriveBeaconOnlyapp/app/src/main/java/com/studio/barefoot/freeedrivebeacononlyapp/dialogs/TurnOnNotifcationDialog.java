package com.studio.barefoot.freeedrivebeacononlyapp.dialogs;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;

/**
 * Created by mcs on 12/28/2016.
 */

public class TurnOnNotifcationDialog extends BaseAlertDialog implements DialogInterface.OnClickListener {
   TextView tv_uuid;
    ContentResolver resolver;


    public TurnOnNotifcationDialog(Context context) {
        super(context);

        LayoutInflater factory = LayoutInflater.from(context);
        final View progressBarView = factory.inflate(R.layout.custom_dialog_uuid, null);

        setView(progressBarView);
        this.context = context;
        tv_uuid = (TextView) progressBarView.findViewById(R.id.tv_uuid);
        setButton(BUTTON_POSITIVE, context.getString(R.string.action_send), this);
        setButton(BUTTON_NEGATIVE, context.getString(R.string.act_cancel), this);
        setCancelable(false);
        setTitle(context.getString(R.string.title_turn_on_notification));
        resolver = context.getContentResolver();

            tv_uuid.setText(context.getString(R.string.turn_on_notificatios));
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which){
            case BUTTON_POSITIVE:
                dialog.dismiss();
                Intent intent = new Intent();
                intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                intent.putExtra("app_package", context.getPackageName());
                intent.putExtra("app_uid", context.getApplicationInfo().uid);
                context.startActivity(intent);
               //context.startActivity(new Intent("android.settings.APP_NOTIFICATION_SETTINGS"));
                 dismiss();
                break;
            case BUTTON_NEGATIVE:
                dismiss();
        }

    }

    @Override
    public void dismiss() {
            super.dismiss();
    }
}
