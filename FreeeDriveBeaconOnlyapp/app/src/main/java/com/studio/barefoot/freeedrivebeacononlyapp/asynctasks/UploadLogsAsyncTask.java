package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LoggingOperations;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbookpro on 9/22/17.
 */

public class UploadLogsAsyncTask extends AsyncTask<Void,Void,Void>  {


    private static final String mUrl = WebServiceConstants.WEBSERVICE_URL_PREFIX + WebServiceConstants.END_POINT_LOGS;
    //"http://xyperdemos.com/pk_freeedrive_backend-master/public/api/score";

    /**
     * Represents the response code from server
     */
    private int responseCode;
    /**
     * Represents the resultat from server
     */
    private String resultat;

    private Context context;

    File fileLog;

    DataOutputStream dos = null;
    String lineEnd = "\r\n";
    String twoHyphens = "--";
    String boundary = "*****";
    int bytesRead, bytesAvailable, bufferSize;
    byte[] buffer;
    int maxBufferSize = 1 * 1024 * 1024;
    String upLoadServerUri = null;

     String phoneNumbers="";
    final String uploadFileName = "myfile";
    FileInputStream fileInputStream;
    public UploadLogsAsyncTask(File file,String phoneNumber) {
       phoneNumbers = phoneNumber;
        fileLog = file;
    }



    @Override
    protected Void doInBackground(Void... params) {



        try {
            List<NameValuePair> mParams = new ArrayList<>();
            mParams.add(new BasicNameValuePair("phone_number", DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
            URL url = new URL(mUrl);
            Log.e("Url", "" + url);
            File sourceFile = new File(fileLog.toURI());
            if (sourceFile.isFile()){
                fileInputStream = new FileInputStream(sourceFile);
            }

            String fileName = fileLog.toURI().toString();
            Log.e("fileName",fileName);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setUseCaches(false);

            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Connection", "Keep-Alive");
            urlConnection.setRequestProperty("ENCTYPE", "multipart/form-data");
            urlConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            urlConnection.setRequestProperty("logFile", "FD_logs.txt");
            urlConnection.setRequestProperty("phone_number", "+923155435167");
            dos = new DataOutputStream(urlConnection.getOutputStream());
            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=logFile;filename="+phoneNumbers+".txt"+"\""+ lineEnd);

            dos.writeBytes(lineEnd);
            // create a buffer of  maximum size
            bytesAvailable = fileInputStream.available();

            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // read file and write it into form...
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0)
            {

                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            }

            // send multipart form data necesssary after file data...
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            //Expermentation for json arrays

     /*       String listofRides = this.data.toString();
            String listofSubRides = this.subdata.toString();

            Log.e("listofRides",listofRides);
            Log.e("listofSubRides",listofSubRides);*/

            //json in body:
/*
            if (mParams!=null){
                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(mParams));
                writer.flush();
                writer.close();
                os.close();
            }*/

            responseCode = urlConnection.getResponseCode();
            String message = urlConnection.getResponseMessage();
            Log.e("Response Message :", message);
            Log.e("Response Code :", "" + responseCode);

            InputStream inputStream;
            // get stream
            if (responseCode < HttpURLConnection.HTTP_BAD_REQUEST) {
                Log.i("Adneom", "Response code (SYNC) is " + responseCode + " and is OK");
                inputStream = urlConnection.getInputStream();
            } else {
                inputStream = urlConnection.getErrorStream();
                Log.i("Adneom", "Response code (SYNC) is " + responseCode + " and is NOT OK");
            }

            // parse stream
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String temp, response = "";
            while ((temp = bufferedReader.readLine()) != null) {
                response += temp;
            }
            resultat = response;
            Log.e("resultAt", "" + resultat);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.i("Adneom", "Error url : " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("Adneom", "Error open connection : " + e.getMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        try {
            fileInputStream.close();
            dos.flush();
            dos.close();
            Log.e("responseCode",""+responseCode);
            Log.e("responseCode",""+responseCode);

            switch (responseCode) {

                case 200:
                    LoggingOperations.deleteFile(fileLog);
            }
        }catch (Exception e){

        }
    }
    /**
     * Allows to the parameters to support UTF-8
     * @param params(in), @List represent the parameters list
     * @return(out), the paraemters in String
     */
    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }
}