package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.studio.barefoot.freeedrivebeacononlyapp.MenuActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.ProfileActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.QrCodeActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.services.BackgroundBeaconScan;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.dialogs.QrCodeUIDdialog.progressBarDialogQrCodeDialog;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.PREF_KEY_FD_STATUS;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.TEMP_UUID;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.fdBlockedScreen;


/**
 * Created by mcs on 1/5/2017.
 */

public class QrCodeAsyncTask extends BaseAsyncTask {
    public static ProgressBarDialog progressBarDialogSyncData;
    private String tokenwithoutColon;
    String uuid = "";
    public String dashless = "";

    public QrCodeAsyncTask(Context context, String route, List<NameValuePair> pp) {
        super(context, route, pp);
    }

    /**
     * AsyncTask method basic calls during a request, calls the parent's method.
     */
    protected String doInBackground(String... params) {

        return "";
    }

    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (s != null) {

            try {
                progressBarDialogQrCodeDialog.dismiss();
                progressBarDialogQrCodeDialog = null;
            } catch (NullPointerException exception) {
                exception.printStackTrace();
            }


            int intResponse = Integer.parseInt(response);
            JSONObject drivingPerformanceObject = new JSONObject();
            JSONObject settingsFD = new JSONObject();


            switch (intResponse) {
                case 200:
                    if (resultat != null || !resultat.isEmpty()) {
                        Log.e("ResponseCode", "" + intResponse);
                        Log.e("response", "" + response);


                        try {
                            JSONObject objAccount = new JSONObject(resultat);


                            if (objAccount.getJSONObject("contract") != null && !objAccount.getJSONObject("contract").equals("")) {
                                drivingPerformanceObject = objAccount.getJSONObject("contract");
                            }


                      /*      if (objAccount.getJSONArray("settings") != null && !objAccount.getJSONObject("settings").equals("")) {
                                settingsFD = objAccount.getJSONArray("settings");
                            }*/
                            for (int i = 0; i < objAccount.length(); i++) {

                                if (!objAccount.getString("token").isEmpty()) {
                                    tokenwithoutColon = objAccount.getString("token");
                                    tokenwithoutColon = tokenwithoutColon.replaceAll("\"", "");
                                    DataHandler.updatePreferences(AppConstants.TOKEN_NUMBER, tokenwithoutColon);

                                }
                                if (objAccount != null) {
                                    boolean fd_active_status_flag = objAccount.getBoolean("active");

                                    if (fd_active_status_flag == false) {
                                        DataHandler.updatePreferences(PREF_KEY_FD_STATUS, fd_active_status_flag);
                                        DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY, false);
                                        fdBlockedScreen(ApplicationController.getmAppcontext());
                                        /*Intent intent = new Intent(ApplicationController.getmAppcontext(), BlockedFreeDriveActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        ApplicationController.getmAppcontext().startActivity(intent);*/
                                        return;
                                    }
                                }

                            }

                            if (drivingPerformanceObject != null && drivingPerformanceObject.length() > 0) {
                                boolean driving_performanceActive = false;
                                for (int k = 0; k < drivingPerformanceObject.length(); k++) {

                                    driving_performanceActive = drivingPerformanceObject.getBoolean("driving_performance");
                                    Log.e("driving_performance", ":" + driving_performanceActive);
                                 /*if (!driving_performanceActive.equalsIgnoreCase("")){
                                     break;
                                 }*/
                                }
                                if (driving_performanceActive == true) {
                                    DataHandler.updatePreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE, true);
                                } else {
                                    DataHandler.updatePreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE, false);

                                }
                            }
/*
                            if (settingsFD!=null&&settingsFD.length()>0){
                                for (int k=0;k<settingsFD.length();k++){

                                   int speedBrakeTime= settingsFD.getInt("speed_brake_interval_time");
                                 *//*if (!driving_performanceActive.equalsIgnoreCase("")){
                                     break;
                                 }*//*
                                }

                            }*/
                            Log.e("Response", "" + intResponse);

                            if (objAccount != null && objAccount.length() > 0) {
                                uuid = objAccount.getString("uuid");
                                if (uuid.contains("-")){
                                      dashless= AppUtils.removeDashes(uuid);
                                      DataHandler.updatePreferences(AppConstants.UUID,dashless);
                                }
                                else {
                                    dashless=uuid;
                                    DataHandler.updatePreferences(AppConstants.UUID,dashless);
                                  }
                                final int mid = 20; //get the middle of the String
                                String[] parts = {dashless.substring(0, mid), dashless.substring(mid)};
                                String nameSpace = parts[0];
                                String instanceID = parts[1];
                                DataHandler.updatePreferences(AppConstants.UUID_NAME_SPACE, nameSpace);
                                DataHandler.updatePreferences(AppConstants.UUID_INSTANCE_ID, instanceID);
                                DataHandler.updatePreferences(AppConstants.DEMO_MODE,false);
                                DataHandler.updatePreferences(AppConstants.DEMO_MODE_SUBSCRIPTION_EXPIRED,false);
                            }


                           /* String uuid  = DataHandler.getStringPreferences(TEMP_UUID);
                            DataHandler.updatePreferences(AppConstants.UUID,uuid);
                            String uuid_name_space = DataHandler.getStringPreferences(AppConstants.TEMP_UUID_NAME_SPACE);
                            DataHandler.updatePreferences(AppConstants.UUID_NAME_SPACE,uuid_name_space);
                            String uuid_instance_id = DataHandler.getStringPreferences(AppConstants.TEMP_UUID_INSTANCE_ID);
                            DataHandler.updatePreferences(AppConstants.UUID_INSTANCE_ID,uuid_instance_id);
                            String temp_ibks_serial_no = DataHandler.getStringPreferences(AppConstants.TEMP_IBKS_SERIAL_NO);
                            DataHandler.updatePreferences(AppConstants.IBKS_SERIAL_NO, temp_ibks_serial_no);

                            DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY, true);
                            context.stopService(new Intent(context, BackgroundBeaconScan.class));
                            context.startService(new Intent(context, BackgroundBeaconScan.class));
                            DataHandler.updatePreferences((AppConstants.USER_AWAITING_QR_CODE), false);
                            /*Intent menuAcitivtyIntent = new Intent(context, MenuActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            context.startActivity(menuAcitivtyIntent);*/
                            FetchUserDetails();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                        /*case 422:
                            //duplicate entiry
                            try {
                                AlertDialog.Builder error_422 = new AlertDialog.Builder(context);
                                error_422.setMessage(context.getResources().getString(R.string.error_login_500)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();

                                    }
                                });
                                error_422.show();
                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                            break;*/
                case 500:
                    //internal error
                    try {
                        AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                        error_500.setMessage(context.getResources().getString(R.string.error_login_404)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_500.show();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                    break;
                case 302:
                    //UUID is not assign to any company

                    Toast.makeText(context, context.getResources().getString(R.string.error_no_uuid_against_302), Toast.LENGTH_LONG).show();
                    try {
                        AlertDialog.Builder builder_302 = new AlertDialog.Builder(context);
                        builder_302.setMessage(context.getResources().getString(R.string.error_no_uuid_against_302))
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                }).create().show();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                    break;

                case 303:
                    //UUID is not assign to any company
                    Toast.makeText(context, context.getResources().getString(R.string.error_uuid_not_assign_303), Toast.LENGTH_LONG).show();
                    try {
                        AlertDialog.Builder builder_304 = new AlertDialog.Builder(context);
                        builder_304.setMessage(context.getResources().getString(R.string.error_uuid_not_assign_303))
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                }).create().show();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                    break;
                case 304:
                    //Serial Number Or UUID not Valid
                    Toast.makeText(context, context.getResources().getString(R.string.error_serial_or_uuid_304), Toast.LENGTH_LONG).show();
                    try {
                        AlertDialog.Builder builder_304 = new AlertDialog.Builder(context);
                        builder_304.setMessage(context.getResources().getString(R.string.error_serial_or_uuid_304))
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                }).create().show();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                    break;
                case 409:
                    //internal error
                    Toast.makeText(context, context.getResources().getString(R.string.error_uuid_already_assign), Toast.LENGTH_LONG).show();
                    try {
                        AlertDialog.Builder error_409 = new AlertDialog.Builder(context);
                        error_409.setMessage(context.getResources().getString(R.string.error_uuid_already_assign)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_409.show();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                    break;
                case 401://user not registered
                    Toast.makeText(context, context.getResources().getString(R.string.user_not_found_403), Toast.LENGTH_LONG).show();

                    try {
                        progressBarDialogQrCodeDialog.dismiss();
                        progressBarDialogQrCodeDialog = null;
                    } catch (NullPointerException exception) {
                        exception.printStackTrace();
                    }
                    try {
                        AlertDialog.Builder error_401 = new AlertDialog.Builder(context);
                        error_401.setMessage(context.getResources().getString(R.string.error_login_401aa)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_401.show();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }

                    break;
                case 403:
                    //internal error
                    Toast.makeText(context, context.getResources().getString(R.string.user_not_found_403), Toast.LENGTH_LONG).show();

                    try {
                        AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                        error_500.setMessage(context.getResources().getString(R.string.user_not_found_403)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_500.show();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                    break;
                   /* try{
                        AlertDialog.Builder error_304 = new AlertDialog.Builder(context);
                        error_304.setMessage(context.getResources().getString(R.string.error_serial_or_uuid_304)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_304.show();
                    }
                    catch (Exception exception){
                        exception.printStackTrace();
                    }*/


            }
        } else {
            try {
                progressBarDialogQrCodeDialog.dismiss();
                progressBarDialogQrCodeDialog = null;
            } catch (NullPointerException exception) {
                exception.printStackTrace();
            }
        }
    }


    private void FetchUserDetails() {
        /*progressBarDialogSyncData = new ProgressBarDialog(context);
        *//*progressBarDialogSyncData.setTitle(context.getString(R.string.title_progress_dialog));
        progressBarDialogSyncData.setMessage(context.getString(R.string.body_progress_dialog));*//*
        progressBarDialogSyncData.setTitle("Syncing");
        progressBarDialogSyncData.setMessage("wait..");
        progressBarDialogSyncData.show();*/
        List<NameValuePair> mParams = new ArrayList<NameValuePair>();
        final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = telephonyManager.getDeviceId();
        mParams.add(new BasicNameValuePair("devID", deviceId));
        mParams.add(new BasicNameValuePair("phone_number", DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
        FetchProfileAsyncTaskForLogin fetchProfileAsyncTask = new FetchProfileAsyncTaskForLogin(context, WebServiceConstants.END_POINT_FETCH_PROFILE, mParams);
        fetchProfileAsyncTask.execute();
    }

}
