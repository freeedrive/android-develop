package com.studio.barefoot.freeedrivebeacononlyapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.app.AlertDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.android.gms.maps.model.PolylineOptions;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.DemoModeEndDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.fragments.MySafetyFragmentGraph;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataParser;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.PREF_CURRENT_LATITUDE;

public class FindMyVehicleActivity extends AppCompatActivity
        implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    GoogleMap mGoogleMap;
    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    Button btn_location_save, btn_delete_location;
    float latitude = 0;
    float longitude = 0;
    ArrayList<LatLng> latLngs;
    private int demoallrides;
    private long totaldaysdemoMode;
    DemoModeEndDialog endDialog;
    FragmentManager fm;


    public FindMyVehicleActivity() {
        LocaleUtils.updateConfig(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_my_vehicle);
        // For setting up the different logo of toll bar for demo and paid mode
        RelativeLayout toolbar = (RelativeLayout)findViewById(R.id.topToolbar);
        ImageView fd_logo = (ImageView)toolbar.findViewById(R.id.tol_bar_logo);

        if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)){
            fd_logo.setImageResource(R.drawable.logo_freeedrive_demo_mode);
        }  else{
            fd_logo.setImageResource(R.drawable.logo_topbar);
        }
        fm = getSupportFragmentManager();
        endDialog = DemoModeEndDialog.newInstance("");
        final RideBDD tmp = new RideBDD(FindMyVehicleActivity.this);
        tmp.open();
        /*getSupportActionBar().setTitle("Find My Vehicle");*/
        // Initializing
        latLngs = new ArrayList<>();

        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);
        setupActionBar();
        btn_location_save = (Button) findViewById(R.id.btn_save_location);
        btn_delete_location = (Button) findViewById(R.id.btn_delete_location);
        if(DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE) == true){
            demoallrides =  tmp.sumDemoRidesCount();
            totaldaysdemoMode = AppUtils.totaldaysDemoMode(FindMyVehicleActivity.this);
            if(demoallrides >= 40 || totaldaysdemoMode >= 30){
                btn_location_save.setBackgroundColor(FindMyVehicleActivity.this.getResources().getColor(R.color.colorBTestMenu));
                btn_location_save.setClickable(false);
            }
            tmp.close();
        }
        btn_location_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE) == true){
                if(demoallrides >= 40 || totaldaysdemoMode >= 30){
                    btn_location_save.setBackgroundColor(FindMyVehicleActivity.this.getResources().getColor(R.color.colorBTestMenu));
                    btn_location_save.setClickable(false);
                }
                else{
                    if (latitude != 0 && longitude != 0) {
                        JSONObject jsonObject = new JSONObject();

                        try {
                            float destination_latitude = (float) 33.7125420;
                            float destinaton_longitude = (float) 73.0717130;
                            jsonObject.put("Latitude", latitude);
                            jsonObject.put("Longitude", longitude);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        DataHandler.updatePreferences(PREF_CURRENT_LATITUDE, jsonObject);
                        AlertDialog.Builder reg = new AlertDialog.Builder(FindMyVehicleActivity.this);
                        reg.setMessage(getResources().getString(R.string.desc_current_location)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                           /* Intent intent = new Intent(FindMyVehicleActivity.this, MenuActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);*/
                            }
                        });
                        reg.show();
                    }
                }
                }
                else{
                if (latitude != 0 && longitude != 0) {
                    JSONObject jsonObject = new JSONObject();

                    try {
                        float destination_latitude = (float) 33.7125420;
                        float destinaton_longitude = (float) 73.0717130;
                        jsonObject.put("Latitude", latitude);
                        jsonObject.put("Longitude", longitude);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    DataHandler.updatePreferences(PREF_CURRENT_LATITUDE, jsonObject);
                    AlertDialog.Builder reg = new AlertDialog.Builder(FindMyVehicleActivity.this);
                    reg.setMessage(getResources().getString(R.string.desc_current_location)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                           /* Intent intent = new Intent(FindMyVehicleActivity.this, MenuActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);*/
                        }
                    });
                    reg.show();
                }

            }
            }
        });
        btn_delete_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String jsonObjcurrentLocation = DataHandler.getStringPreferences(PREF_CURRENT_LATITUDE);
                Log.e("jsonObjcurrLocation :", jsonObjcurrentLocation);
                if (jsonObjcurrentLocation != null && !jsonObjcurrentLocation.isEmpty()) {
                    DataHandler.deletePreference(AppConstants.PREF_CURRENT_LATITUDE);
                    /*if (marker!=null) {
                        marker.remove();
                        marker=null;
                    }*/
                    mGoogleMap.clear();
                    AlertDialog.Builder reg = new AlertDialog.Builder(FindMyVehicleActivity.this);
                    reg.setMessage(getResources().getString(R.string.desc_delete_location)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    });
                    reg.show();
                }

            }
        });

        if(DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE_SUBSCRIPTION_EXPIRED) == true){
            DemoModeDialog();
        }
    }

    private void DemoModeDialog() {

        try {
            if (endDialog != null && endDialog.isAdded()) {
                return;
            } else {
               /* Log.e("BF-PK-FD", "DEMO_MODE_END_POPUP SHOW");*/
                endDialog.show(fm, "");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

   /* private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        Log.e("str_origin", str_origin);

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        Log.e("str_dest", str_dest);

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }*/
   @Override
   protected void attachBaseContext(Context context) {
       super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
   }

    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
            } /*else {
                //Request Location Permission
                checkLocationPermission();
            }*/
        } else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
        }


        // Setting onclick event listener for the map
        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
               /* // Already two locations
                if (latLngs.size() > 1) {
                    latLngs.clear();
                    mGoogleMap.clear();
                }
                // Adding new item to the ArrayList
                latLngs.add(latLng);
                // Creating MarkerOptions
                MarkerOptions options = new MarkerOptions();

                // Setting the position of the marker
                options.position(latLng);

                *//**
                 * For the start location, the color of marker is GREEN and
                 * for the end location, the color of marker is RED.
                 *//*
                if (latLngs.size() == 1) {
                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                } else if (latLngs.size() == 2) {
                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                }
                mGoogleMap.addMarker(options);

                // Checks, whether start and end locations are captured
                if (latLngs.size() >= 2) {
                    LatLng origin = latLngs.get(0);
                    LatLng dest = latLngs.get(1);
                    Log.e("Origin",":" +origin);
                    Log.e("Destination",":" +dest);
                    // Getting URL to the Google Directions API
                    String url = getUrl(origin, dest);
                    Log.d("onMapClick", url.toString());
                    FetchUrl FetchUrl = new FetchUrl();

                    // Start downloading json data from Google Directions API
                    FetchUrl.execute(url);
                    //move map camera
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
                    mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));
                }*/
            }
        });
        getDestinationLocation();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), 15));
        LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        latitude = (float) latLng.latitude;
        longitude = (float) latLng.longitude;
        latLngs.add(latLng);
        Log.e("Latitude", " : " + latitude);
        Log.e("Longtitude", " : " + longitude);
       /* BitmapDescriptor icon_find_my_car = BitmapDescriptorFactory.fromResource(R.drawable.icon_find_my_car);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Parked Car");
        markerOptions.icon(icon_find_my_car);
        mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);*/
        /*CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng) // Sets the center of the map to location user
                .zoom(5)                   // Sets the zoom
                .bearing(360)                // Sets the orientation of the camera to east
                .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/

        // Checks, whether start and end locations are captured
       /* if (latLngs.size() >= 2 && locationAvailable) {
            LatLng origin = latLngs.get(0);
            LatLng dest = latLngs.get(1);
            Log.e("Origin", ":" + origin);
            Log.e("Destination", ":" + dest);
            // Getting URL to the Google Directions API
            String url = getUrl(origin, dest);
            Log.d("onLocationChange", url.toString());
            FetchUrl FetchUrl = new FetchUrl();
            locationAvailable = false;
            // Start downloading json data from Google Directions API
            FetchUrl.execute(url);
            //move map camera
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
            mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        }*/
    }
    @SuppressLint("RestrictedApi")
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    // Fetches data from url passed
    /*private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }*/

    public void getDestinationLocation(){
        String jsonObjcurrentLocation = DataHandler.getStringPreferences(PREF_CURRENT_LATITUDE);
        Log.e("jsonObjcurrLocation :", jsonObjcurrentLocation);

        String latitude_ = "";
        String longitude_ = "";
        if (jsonObjcurrentLocation != null && !jsonObjcurrentLocation.isEmpty()) {
            try {
                JSONObject jsonObject = new JSONObject(jsonObjcurrentLocation);
                            /*jsonObject = jsonObject.getJSONObject("nameValuePairs");*/
                Log.e("jsonObject", ":" + jsonObject);
                if (jsonObject != null) {
                    latitude_ = jsonObject.getString("Latitude");
                    longitude_ = jsonObject.getString("Longitude");
                    Log.e("latitude", " : " + latitude);
                    Log.e("longitude", " : " + longitude);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


            /*Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude_ + "," + longitude_ + " &mode=w");
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);*/
            // Already two locations

            double destination_latitude = Double.parseDouble(latitude_);
            double destination_longitude = Double.parseDouble(longitude_);
            LatLng latLng = new LatLng(destination_latitude, destination_longitude);
            // Adding new item to the ArrayList
           /* latLngs.add(latLng);*/
            // Creating MarkerOptions
            MarkerOptions options = new MarkerOptions();

            // Setting the position of the marker
            options.position(latLng);
            options.title("Parked Car");

         /* For the start location, the color of marker is GREEN and
         for the end location, the color of marker is RED.*/

            /*if (latLngs.size() == 1) {
                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            }
            else if (latLngs.size() == 2) {
                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }*/
            mGoogleMap.addMarker(options);

        }
    }

    /**
     * A method to download json data from url
     */
   /* private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }*/

    /**
     * A class to parse the Google Places in JSON format
     */
    /*private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(20);
                lineOptions.color(ContextCompat.getColor(getApplicationContext(),R.color.freedrivelogo_color));

                Log.d("onPostExecute", "onPostExecute lineoptions decoded");

            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                mGoogleMap.addPolyline(lineOptions);
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }
    }*/
}