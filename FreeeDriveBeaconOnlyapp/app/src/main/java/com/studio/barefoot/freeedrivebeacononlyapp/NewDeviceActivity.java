package com.studio.barefoot.freeedrivebeacononlyapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;

public class NewDeviceActivity extends AppCompatActivity {
       Button yes,no;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_device);
        yes = (Button) findViewById(R.id.btn_yes_new_device);
        no = (Button) findViewById(R.id.btn_no_new_device);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intentNewNumberActivity = new Intent(NewDeviceActivity.this,NewPhoneNumberActivity.class);
                intentNewNumberActivity.putExtra(AppConstants.IsThisNewDevice,true);
                startActivity(intentNewNumberActivity);
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intentNewNumberActivity = new Intent(NewDeviceActivity.this,NewPhoneNumberActivity.class);
                intentNewNumberActivity.putExtra(AppConstants.IsThisNewDevice,false);
                startActivity(intentNewNumberActivity);
            }
        });
    }
}
