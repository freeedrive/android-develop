package com.studio.barefoot.freeedrivebeacononlyapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AutoReplyActivity extends AppCompatActivity {
     Switch autoReplySwitch;
     Button autoreply_btn_save;
     EditText et_autoreply;
    String message="";
    public AutoReplyActivity()
    {
        LocaleUtils.updateConfig(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_reply);
       /* autoReplySwitch = (Switch) findViewById(R.id.auto_switch_button);*/
        autoreply_btn_save = (Button) findViewById(R.id.autoreply_btn_save);
        et_autoreply = (EditText) findViewById(R.id.et_autoreply);


            /*if (DataHandler.getIntPreferences(AppConstants.KEY_AUTOREPLY)==1) {
                autoReplySwitch.setChecked(true);
            } else {
                autoReplySwitch.setChecked(false);

            }*/


        if (DataHandler.getStringPreferences(AppConstants.KEY_AUTOREPLY_MSG)!=null&&!DataHandler.getStringPreferences(AppConstants.KEY_AUTOREPLY_MSG).isEmpty()){
            message = DataHandler.getStringPreferences(AppConstants.KEY_AUTOREPLY_MSG);
            et_autoreply.setText(message);
        }else{
            message = this.getResources().getString(R.string.et_auto_reply);
            et_autoreply.setText(message);
        }

        /*autoReplySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    DataHandler.updatePreferences(AppConstants.KEY_AUTOREPLY,1);
                }else if (!b){
                    DataHandler.updatePreferences(AppConstants.KEY_AUTOREPLY,0);
                }
            }
        });*/
        setupActionBar();
        autoreply_btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataHandler.updatePreferences(AppConstants.KEY_AUTOREPLY_MSG,et_autoreply.getText().toString());
                startActivity(new Intent(AutoReplyActivity.this,MenuActivity.class));
                finish();

            }
        });
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }
}
