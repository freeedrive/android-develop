package com.studio.barefoot.freeedrivebeacononlyapp.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.PhoneSafetyAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.UploadLogsAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.RidesBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.DemoModeEndDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.PerformancePackDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LoggingOperations;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;
import com.txusballesteros.widgets.FitChart;
import com.txusballesteros.widgets.FitChartValue;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MySafetyFragmentGraph.DemoModeDialog;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyDailyStatsFragment extends Fragment {

    RecyclerView listViewPhoneSafety;
    private ArrayList<RidesBeans> ridesBeansArrayList = new ArrayList<>();
    private PhoneSafetyAdapter phoneSafetyAdapter;
    LinearLayoutManager mLayoutManager;
    View tripphoneSafety;
    Fragment fragment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    RelativeLayout relativeLayout_trip_phone_saftey, relativeLayoutdriving, relativeLayout_trip_driving_performance, relativeLayoutPhone;
    TextView daily_stats_date_picker_date, emptyRidesTextView, daily_stats_score_last_ride_percentage_avg_score, firstnamedailyStats, dailystatsavgScore;
    TextView dailystatstextview;
    FitChart daily_stats_total_rides_avg_score;
    Calendar calander;
    String formattedDate = "";
    String currentDate = "";
    SimpleDateFormat df;
    String dayLongName = "";
    private int dailystatstotalavgScore = 0;
    ImageView ImgDrivingPerformance, daily_stats_arrow_right, daily_stats_arrow_left, navigationLeft, navigationRight;
    PerformancePackDialog performancePackDialog;
    private int rideLenght=0;

    public MyDailyStatsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        tripphoneSafety = inflater.inflate(R.layout.daily_stats_fragment, container, false);
        // For setting up the different logo of toll bar for demo and paid mode
        RelativeLayout toolbar = (RelativeLayout)tripphoneSafety.findViewById(R.id.navbar);
        ImageView fd_logo = (ImageView)toolbar.findViewById(R.id.tol_bar_logo);
        if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)){
            fd_logo.setImageResource(R.drawable.logo_freeedrive_demo_mode);
        }  else{
            fd_logo.setImageResource(R.drawable.logo_topbar);
        }

        emptyRidesTextView = (TextView) tripphoneSafety.findViewById(R.id.emptyTextViewForPhoneSafetRides);
        ImgDrivingPerformance = (ImageView) tripphoneSafety.findViewById(R.id.drivingperformance);
        firstnamedailyStats = (TextView) tripphoneSafety.findViewById(R.id.first_name_daily_stats);
        dailystatstextview = (TextView) tripphoneSafety.findViewById(R.id.daily_stats);
        dailystatsavgScore = (TextView) tripphoneSafety.findViewById(R.id.daily_stats_score_total_rides_avg);
        daily_stats_score_last_ride_percentage_avg_score = (TextView) tripphoneSafety.findViewById(R.id.daily_stats_score_last_ride_percentage_avg_score);
       /* daily_stats_score_total_rides = (TextView) tripphoneSafety.findViewById(R.id.daily_stats_score_total_rides);*/
        daily_stats_total_rides_avg_score = (FitChart) tripphoneSafety.findViewById(R.id.daily_stats_fitcharttotalrides_avg_score);
        daily_stats_arrow_right = (ImageView) tripphoneSafety.findViewById(R.id.daily_stats_arrow_right);
        daily_stats_arrow_left = (ImageView) tripphoneSafety.findViewById(R.id.daily_stats_arrow_left);
        navigationLeft = (ImageView) tripphoneSafety.findViewById(R.id.graph_stats_arrow_left);
        navigationRight = (ImageView) tripphoneSafety.findViewById(R.id.graph_stats_arrow_right);
        daily_stats_date_picker_date = (TextView) tripphoneSafety.findViewById(R.id.daily_stats_date_picker);
        calander = Calendar.getInstance();
        dayLongName = calander.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
        dayLongName = dayLongName.substring(0,1).toUpperCase() + dayLongName.substring(1).toLowerCase();
        /*capitalizeAllWords(dayLongName);*/
        df = new SimpleDateFormat("dd/MM/yyyy");
        currentDate = df.format(calander.getTime());



        Log.e("dayOfTheWeek", dayLongName);
        daily_stats_date_picker_date.setText(currentDate + "\n" + dayLongName);
        long timestampstartDate = AppUtils.getStartOfDayInMillis(currentDate);
        Log.e("timestampstartdate", ":" + timestampstartDate);
        long timestampnendDate = AppUtils.getEndOfDayInMillis(currentDate);
        Log.e("timestampnendDate", ":" + timestampnendDate);
        daily_stats_arrow_right.setVisibility(View.INVISIBLE);

        rideLenght = getPhoneSafetyRides(timestampstartDate, timestampnendDate);
        listViewPhoneSafety = (RecyclerView) tripphoneSafety.findViewById(R.id.listViewPhoneSafety);

        mLayoutManager = new LinearLayoutManager(getActivity());

        listViewPhoneSafety.setHasFixedSize(true);
        listViewPhoneSafety.setLayoutManager(mLayoutManager);
        //listViewPhoneSafety.addItemDecoration(new PhoneSafetDividerDecor(getActivity(), LinearLayoutManager.VERTICAL));
        listViewPhoneSafety.setItemAnimator(new DefaultItemAnimator());

        phoneSafetyAdapter = new PhoneSafetyAdapter(ridesBeansArrayList);
        listViewPhoneSafety.setAdapter(phoneSafetyAdapter);

        phoneSafetyAdapter.notifyDataSetChanged();


        dailystatstotalavgScore = (int) getdailystatsavgScore(timestampstartDate, timestampnendDate);
        if (rideLenght>0) {
            if (dailystatstotalavgScore >= 0) {
                //initializing the fitchartcollection
                Collection<FitChartValue> fitChartValues = new ArrayList<>();
                if (dailystatstotalavgScore >= 80) {
                    dailystatsavgScore.setText("" + dailystatstotalavgScore + "%");
                    dailystatsavgScore.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
                    daily_stats_score_last_ride_percentage_avg_score.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
                    fitChartValues.add(new FitChartValue(dailystatstotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartGreen)));
            /*daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
                } else if (dailystatstotalavgScore >= 50 && dailystatstotalavgScore < 80) {
                    dailystatsavgScore.setText("" + dailystatstotalavgScore + "%");
                    dailystatsavgScore.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                    daily_stats_score_last_ride_percentage_avg_score.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                    fitChartValues.add(new FitChartValue(dailystatstotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartOrange)));
            /*daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
                } else if (dailystatstotalavgScore >= 0 && dailystatstotalavgScore < 50) {
                    dailystatsavgScore.setText("" + dailystatstotalavgScore + "%");
                    dailystatsavgScore.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartRed));
                    daily_stats_score_last_ride_percentage_avg_score.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartRed));
                    fitChartValues.add(new FitChartValue(dailystatstotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartRed)));
            /*daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
                }
                daily_stats_total_rides_avg_score.setValues(fitChartValues);
            }
        }
        else {
            daily_stats_total_rides_avg_score.setValue(100);
            dailystatsavgScore.setText("100%");

        }




        String jsonObjProfile = DataHandler.getStringPreferences(AppConstants.TEMP_DISPLAY_KEY);

        String firstName = "";

        if (jsonObjProfile != null && !jsonObjProfile.isEmpty()) {

            try {
                JSONObject jsonObject = new JSONObject(jsonObjProfile);
                firstnamedailyStats.setText(jsonObject.getString("first_name"));

            } catch (Exception exception) {
                exception.printStackTrace();
            }

        }

        /*imageviewDailyStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    imageviewFleetStats.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.fleet_icon_grey));
                    imageviewFleetStats.setBackground(null);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    imageviewDailyStats.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.mysafety_icon_blue));
                    imageviewDailyStats.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_fragment_shape));
                }
                fragment = new MySafetyFragmentGraph();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.trip_fragment, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }

        });

        imageviewFleetStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    imageviewDailyStats.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.mysafety_icon_grey));
                    imageviewDailyStats.setBackground(null);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    imageviewFleetStats.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.fleet_icon_blue));
                    imageviewFleetStats.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_fragment_shape));
                }

            }
        });*/
        navigationLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new MySafetyFragmentGraph();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(getId(), fragment);
                // fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        navigationRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new MyWeeklyStats();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(getId(), fragment);
                // fragmentTransaction.addToBackStack(null);
                Log.e("fragCountD", "onClick: "+fragmentManager.getBackStackEntryCount());
                fragmentTransaction.commit();
            }
        });
        //Layout intiliaze
        /*relativeLayout_trip_phone_saftey = (RelativeLayout) tripphoneSafety.findViewById(R.id.insidecontaintermobilehotspot);
        relativeLayoutdriving = (RelativeLayout) tripphoneSafety.findViewById(R.id.drivingrelativelayout);
        relativeLayout_trip_driving_performance = (RelativeLayout) tripphoneSafety.findViewById(R.id.drivingrelativelayout);
        relativeLayoutPhone = (RelativeLayout) tripphoneSafety.findViewById(R.id.insidecontaintermobilehotspot);

        //textview inttiliaze
        phonesafety = (TextView) tripphoneSafety.findViewById(R.id.textviewmobilehotSpot);`
        drivingperformance = (TextView) tripphoneSafety.findViewById(R.id.textviewdrivingperformance);

        tripfragment_phone_safety = (Button) tripphoneSafety.findViewById(R.id.tripfragment_phonesafety_btn);



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            relativeLayout_trip_phone_saftey.setBackground(getActivity().getResources().getDrawable(R.drawable.phonesafety_custom_shape));
        }
        phonesafety.setTextColor(getActivity().getResources().getColor(R.color.white));
        if (!DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                relativeLayoutdriving.setBackground(getActivity().getResources().getDrawable(R.drawable.performance_pack_custom_shape));
                drivingperformance.setTextColor(getActivity().getResources().getColor(R.color.colorBTestMenu));
                ImgDrivingPerformance.setImageResource(R.drawable.tab_icon_car_grey);
            }

        }*/

        daily_stats_arrow_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                calander.add(Calendar.DAY_OF_MONTH, 1);
                dayLongName = calander.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
                formattedDate = df.format(calander.getTime());
                dayLongName = dayLongName.substring(0,1).toUpperCase() + dayLongName.substring(1).toLowerCase();
                daily_stats_date_picker_date.setText(formattedDate + "\n" + dayLongName);

                long previousselectedDate = AppUtils.getStartOfDayInMillis(formattedDate) / 1000;
                long previousselectedendDate = AppUtils.getEndOfDayInMillis(formattedDate) / 1000;
                Log.e("TimeStampStartDate", ":" + previousselectedDate);
                Log.e("TimeStampEndtDate", ":" + previousselectedendDate);

                if (formattedDate.equalsIgnoreCase(currentDate)) {
                    daily_stats_arrow_right.setVisibility(View.INVISIBLE);
                }

                dailystatstotalavgScore = (int) getdailystatsavgScore(previousselectedDate, previousselectedendDate);
                Collection<FitChartValue> fitChartValues = new ArrayList<>();
                if (dailystatstotalavgScore >=80) {
                    dailystatsavgScore.setText("" + dailystatstotalavgScore + "%");
                    /*daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
                    dailystatsavgScore.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
                    daily_stats_score_last_ride_percentage_avg_score.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
                    fitChartValues.add(new FitChartValue(dailystatstotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartGreen)));
                } else if (dailystatstotalavgScore >= 50 && dailystatstotalavgScore < 80) {
                    dailystatsavgScore.setText("" + dailystatstotalavgScore + "%");
                    /*daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
                    dailystatsavgScore.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                    daily_stats_score_last_ride_percentage_avg_score.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                    fitChartValues.add(new FitChartValue(dailystatstotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartOrange)));
                } else {
                    dailystatsavgScore.setText("" + dailystatstotalavgScore + "%");
                    /*daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
                    dailystatsavgScore.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartRed));
                    daily_stats_score_last_ride_percentage_avg_score.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartRed));
                    fitChartValues.add(new FitChartValue(dailystatstotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartRed)));
                }
                daily_stats_total_rides_avg_score.setValues(fitChartValues);

                getPhoneSafetyRides(previousselectedDate, previousselectedendDate);
                phoneSafetyAdapter = new PhoneSafetyAdapter(ridesBeansArrayList);
                listViewPhoneSafety.setAdapter(phoneSafetyAdapter);


            }
        });
        daily_stats_arrow_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                daily_stats_arrow_right.setVisibility(View.VISIBLE);
                calander.add(Calendar.DAY_OF_MONTH, -1);

                formattedDate = df.format(calander.getTime());
                dayLongName = calander.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
                dayLongName = dayLongName.substring(0,1).toUpperCase() + dayLongName.substring(1).toLowerCase();
                daily_stats_date_picker_date.setText(formattedDate + "\n" + dayLongName);
                long previousselectedDate = AppUtils.getStartOfDayInMillis(formattedDate) / 1000;
                long previousselectedendDate = AppUtils.getEndOfDayInMillis(formattedDate) / 1000;
                Log.e("TimeStamp StartDate", ":" + previousselectedDate);
                Log.e("TimeStamp EndtDate", ":" + previousselectedendDate);

                if (formattedDate.equalsIgnoreCase(currentDate)) {
                    daily_stats_arrow_right.setVisibility(View.INVISIBLE);
                }

                dailystatstotalavgScore = (int) getdailystatsavgScore(previousselectedDate, previousselectedendDate);
                Collection<FitChartValue> fitChartValues = new ArrayList<>();
                if (dailystatstotalavgScore >=80) {
                    dailystatsavgScore.setText("" + dailystatstotalavgScore + "%");
                   /* daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
                    dailystatsavgScore.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
                    daily_stats_score_last_ride_percentage_avg_score.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
                    fitChartValues.add(new FitChartValue(dailystatstotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartGreen)));
                } else if (dailystatstotalavgScore >= 50 && dailystatstotalavgScore < 80) {
                    dailystatsavgScore.setText("" + dailystatstotalavgScore + "%");
                    /*daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
                    dailystatsavgScore.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                    daily_stats_score_last_ride_percentage_avg_score.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                    fitChartValues.add(new FitChartValue(dailystatstotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartOrange)));

                } else{

                    dailystatsavgScore.setText("" + dailystatstotalavgScore + "%");
                   /* daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
                    dailystatsavgScore.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartRed));
                    daily_stats_score_last_ride_percentage_avg_score.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartRed));
                    fitChartValues.add(new FitChartValue(dailystatstotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartRed)));
                }

                daily_stats_total_rides_avg_score.setValues(fitChartValues);
                /*dailystatsavgScore.setText(""+dailystatstotalavgScore);
                daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
                getPhoneSafetyRides(previousselectedDate, previousselectedendDate);
                phoneSafetyAdapter = new PhoneSafetyAdapter(ridesBeansArrayList);
                listViewPhoneSafety.setAdapter(phoneSafetyAdapter);

            }
        });

       /* dailystatstextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fragment = new MyWeeklyStats();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.my_daily_stats_fragment, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });*/
        /*tripfragment_driving_performance.setOnClickListener(new View.OnClickListener() {s
            @Override
            public void onClick(View v) {

                fragment = new MyWeeklyStats();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.trip_fragment, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                *//*if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        relativeLayout_trip_driving_performance.setBackground(getActivity().getResources().getDrawable(R.drawable.driving_performance_custom_shape));
                    }
                    drivingperformance = (TextView) tripphoneSafety.findViewById(R.id.textviewdrivingperformance);
                    drivingperformance.setTextColor(getActivity().getResources().getColor(R.color.white));

                    //Select * from table_rides where DEPARTURE_TIME >= 1503306374 AND DEPARTURE_TIME <= 1503399354

                    //SELECT sum(SCORE* TIME_ELAPSED) / sum(TIME_ELAPSED) as avg_score FROM table_rides  where DEPARTURE_TIME  BETWEEN 1503306253 AND 1503485756
                    //SELECT sum(SCORE * TIME_ELAPSED) / sum(TIME_ELAPSED) as avg_score,sum(COUNT_BAD_BEHAVIOUR),sum(TIME_ELAPSED),COL_DEPARTURE_DATE from table_rides where COL_DEPARTURE_DATE >= '2017-08-21' AND COL_DEPARTURE_DATE <= '2017-08-23'  Group By COL_DEPARTURE_DATE
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        relativeLayoutPhone.setBackground(getActivity().getResources().getDrawable(R.drawable.phone_safety_custom_shape));
                    }
                    phonesafety = (TextView) tripphoneSafety.findViewById(R.id.textviewmobilehotSpot);
                    phonesafety.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
                }else{
                    performancePackDialog = new PerformancePackDialog(getActivity());
                    performancePackDialog.show();
                }*//*

            }
        });*/
       // ifHuaweiAlert();
        if(DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE_SUBSCRIPTION_EXPIRED) == true){
            DemoModeDialog();
        }
        return tripphoneSafety;
    }
    private void ifHuaweiAlert() {
        final SharedPreferences settings = getContext().getSharedPreferences("ProtectedApps", MODE_PRIVATE);
        final String saveIfSkip = "skipProtectedAppsMessage";
        boolean skipMessage = settings.getBoolean(saveIfSkip, false);
        if (!skipMessage) {
            final SharedPreferences.Editor editor = settings.edit();
            Intent intent = new Intent();
            intent.setClassName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity");
            if (isCallable(intent)) {
                final AppCompatCheckBox dontShowAgain = new AppCompatCheckBox(getContext());
                dontShowAgain.setText("Do not show again");
                dontShowAgain.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        editor.putBoolean(saveIfSkip, isChecked);
                        editor.apply();
                    }
                });

                new AlertDialog.Builder(getContext())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Huawei Protected Apps")
                        .setMessage(String.format("%s requires to be enabled in 'Protected Apps' to function properly.%n", getString(R.string.app_name)))
                        .setView(dontShowAgain)
                        .setPositiveButton("Protected Apps", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                huaweiProtectedApps();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, null)
                        .show();
            } else {
                editor.putBoolean(saveIfSkip, true);
                editor.apply();
            }
        }
    }
    private boolean isCallable(Intent intent) {
        List<ResolveInfo> list = getContext().getPackageManager().queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    private void huaweiProtectedApps() {
        try {
            String cmd = "am start -n com.huawei.systemmanager/.optimize.process.ProtectActivity";
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                cmd += " --user " + getUserSerial();
            }
            Runtime.getRuntime().exec(cmd);
        } catch (IOException ignored) {
        }
    }

    private String getUserSerial() {
        //noinspection ResourceType
        Object userManager = getActivity().getSystemService("user");
        if (null == userManager) return "";

        try {
            Method myUserHandleMethod = android.os.Process.class.getMethod("myUserHandle", (Class<?>[]) null);
            Object myUserHandle = myUserHandleMethod.invoke(android.os.Process.class, (Object[]) null);
            Method getSerialNumberForUser = userManager.getClass().getMethod("getSerialNumberForUser", myUserHandle.getClass());
            Long userSerial = (Long) getSerialNumberForUser.invoke(userManager, myUserHandle);
            if (userSerial != null) {
                return String.valueOf(userSerial);
            } else {
                return "";
            }
        } catch (NoSuchMethodException | IllegalArgumentException | InvocationTargetException | IllegalAccessException ignored) {
        }
        return "";
    }
    private int getPhoneSafetyRides(long startdayDate, long enddayDate) {
        RideBDD tmp = new RideBDD(getContext());
        tmp.open();
        ridesBeansArrayList = tmp.getAllRidesByDate(startdayDate, enddayDate);
        Log.e("ridesBeansArrayList", "" + ridesBeansArrayList);
        tmp.close();
        if (ridesBeansArrayList.size() == 0) {
            emptyRidesTextView.setVisibility(View.VISIBLE);
            dailystatsavgScore.setText("100%");
            daily_stats_total_rides_avg_score.setValue(100);
            dailystatsavgScore.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
            daily_stats_score_last_ride_percentage_avg_score.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
        } else {
            rideLenght = 1;
            emptyRidesTextView.setVisibility(View.INVISIBLE);
        }
        return rideLenght;

    }

    private float getdailystatsavgScore(long preTime, long endtime) {
        RideBDD tmp = new RideBDD(getContext());
        tmp.open();
        float dailystatstotalscoreAvg = 0;
        dailystatstotalscoreAvg = tmp.dailystatstotalscoreAvg(preTime, endtime);
        tmp.close();
      /*  if (dailystatstotalscoreAvg == 0) {
            dailystatsavgScore.setText("100%");
            daily_stats_total_rides_avg_score.setValue(100);
            dailystatsavgScore.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
            daily_stats_score_last_ride_percentage_avg_score.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
            Log.e("No Score Is", "Available");
        }*/
        return dailystatstotalscoreAvg;
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDetach() {
        super.onDetach();


    }
    protected void hideKeyboard(View view)
    {
        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
