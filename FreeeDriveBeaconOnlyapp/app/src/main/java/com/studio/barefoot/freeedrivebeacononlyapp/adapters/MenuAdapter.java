package com.studio.barefoot.freeedrivebeacononlyapp.adapters;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

import java.util.ArrayList;

import static com.studio.barefoot.freeedrivebeacononlyapp.MenuActivity.mMenuList;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.passengerMode;


//import com.freeedrive.R;

public class MenuAdapter extends BaseAdapter {
    public Context mActivity;
    private int autoReply = 100;
    String menudrawerLanaguge = "";
    int messgaes_count = 0;


    public MenuAdapter(Context ma) {

        this.mActivity = ma;
        if (DataHandler.getIntPreferences(AppConstants.MESSAGES_COUNT) >= 0) {
            messgaes_count = DataHandler.getIntPreferences(AppConstants.MESSAGES_COUNT);
        }
    }

    @Override
    public int getCount() {

        return 10;
    }

    @Override
    public Object getItem(int arg0) {
        return 0;
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(mActivity).inflate(R.layout.menu_cell, null);
        TextView tv = (TextView) convertView.findViewById(R.id.tv);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.dividerimg);
        final ImageView iv = (ImageView) convertView.findViewById(R.id.auto_switch_button_autoreply);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passengerMode = DataHandler.getIntPreferencesPassengerMode(AppConstants.KEY_PASSENGER_MODE);
                if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)) {

                    if (passengerMode == 0) {

                        iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.off_switch));
                        DataHandler.updatePreferences(AppConstants.KEY_PASSENGER_MODE, 1);
                    } else {
                        DataHandler.updatePreferences(AppConstants.KEY_PASSENGER_MODE, 0);
                        iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.on_switch));
                    }
                }
                /*autoReply = DataHandler.getIntPreferences(AppConstants.KEY_AUTOREPLY);
				if (autoReply==0){
					iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.off_switch));

					DataHandler.updatePreferences(AppConstants.KEY_AUTOREPLY,1);
				}else  {
					DataHandler.updatePreferences(AppConstants.KEY_AUTOREPLY,0);
					iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.on_switch));

				}*/
            }
        });
        //tv.setTypeface(UIUtils.getInstance().getMediumFont(mActivity));
        //	ImageView iv = (ImageView) convertView.findViewById(R.id.iv);
        if (position == 0) {
            tv.setText(mActivity.getResources().getString(R.string.title_profile));
            iv.setVisibility(View.INVISIBLE);

            //iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.bluetooth));
        }
        if (position == 1) {
			/*tv.setText(mActivity.getResources().getString(R.string.heading_fleet_messages));
			iv.setVisibility(View.INVISIBLE);
			btn_msgs_count.setVisibility(View.VISIBLE);
				btn_msgs_count.setText(""+messgaes_count);*/
            convertView = LayoutInflater.from(mActivity).inflate(R.layout.custom_fleet_messages_layout, null);
            TextView text_view = (TextView) convertView.findViewById(R.id.tv_heading);
            ImageView imageView1 = (ImageView) convertView.findViewById(R.id.dividerimage);
            Button btn_msgs_counts = (Button) convertView.findViewById(R.id.btn_messages_counts);
            text_view.setText(mActivity.getResources().getString(R.string.heading_fleet_messages));

            if (messgaes_count > 0) {
                btn_msgs_counts.setVisibility(View.VISIBLE);
                btn_msgs_counts.setText("" + messgaes_count);
            } else {
                float pixels_height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45, mActivity.getResources().getDisplayMetrics());

                btn_msgs_counts.setVisibility(View.INVISIBLE);
                RelativeLayout.LayoutParams llp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, (int) pixels_height);
                float pixels_margin_left = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 55, mActivity.getResources().getDisplayMetrics());
                float pixels_margin_top = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, mActivity.getResources().getDisplayMetrics());

                llp.setMargins((int) pixels_margin_left, (int) pixels_margin_top, 0, 0);
                text_view.setLayoutParams(llp);


            }

            //iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.notifications));
        }
        /*if (position == 2) {
            tv.setText(mActivity.getResources().getString(R.string.heading_fleet_statistics));
            iv.setVisibility(View.INVISIBLE);
			convertView = LayoutInflater.from(mActivity).inflate(R.layout.custom_fleet_messages_layout, null);
			TextView text_view = (TextView) convertView.findViewById(R.id.tv_heading);
			Button btn_msgs_counts = (Button) convertView.findViewById(R.id.btn_messages_counts);
			text_view.setText(mActivity.getResources().getString(R.string.heading_fleet_messages));
			btn_msgs_counts.setVisibility(View.VISIBLE);
			//btn_msgs_counts.setText(""+messgaes_count);
			//iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.notifications));
		}*/
		if(position == 2) {
			tv.setText(mActivity.getResources().getString(R.string.heading_fleet_statistics));
			iv.setVisibility(View.INVISIBLE);


            //iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.notifications));
        }
        if (position == 3) {
            tv.setText(mActivity.getResources().getString(R.string.title_passenger_mode));
            tv.isClickable();
            iv.setVisibility(View.VISIBLE);

            passengerMode = DataHandler.getIntPreferencesPassengerMode(AppConstants.KEY_PASSENGER_MODE);
            if (passengerMode == 1) {
                iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.off_switch));
            } else {
                iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.on_switch));
            }
            //	iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.auto));
        }
        if (position == 4) {
            tv.setText(mActivity.getResources().getString(R.string.title_find_my_vehicle));
            iv.setVisibility(View.INVISIBLE);
        }
        if (position == 5) {
            if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE) != null && DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE) == true) {

                convertView = LayoutInflater.from(mActivity).inflate(R.layout.info_layout, null);
                TextView text_view = (TextView) convertView.findViewById(R.id.text_bottom_view);
                ImageView imageView1 = (ImageView) convertView.findViewById(R.id.dividerimage);
                View view = (convertView.findViewById(R.id.top_view));
                Log.e("bytes Check", "" + DataHandler.getIntPreferences(AppConstants.PIXELS_DIFFERENCE));
                float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DataHandler.getIntPreferences(AppConstants.PIXELS_DIFFERENCE), mActivity.getResources().getDisplayMetrics());

                view.setLayoutParams(new RelativeLayout.LayoutParams(-2, (int) pixels));

                view.setOnClickListener(null);
                text_view.setText(mActivity.getResources().getString(R.string.title_order_beacons));
                iv.setVisibility(View.INVISIBLE);
            }
            else {
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.info_layout, null);
                TextView text_view = (TextView) convertView.findViewById(R.id.text_bottom_view);
                ImageView imageView1 = (ImageView) convertView.findViewById(R.id.dividerimage);
                View view = (convertView.findViewById(R.id.top_view));
                Log.e("bytes Check", "" + DataHandler.getIntPreferences(AppConstants.PIXELS_DIFFERENCE));
                float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DataHandler.getIntPreferences(AppConstants.PIXELS_DIFFERENCE), mActivity.getResources().getDisplayMetrics());

                view.setLayoutParams(new RelativeLayout.LayoutParams(-2, (int) pixels));
                imageView.setVisibility(View.INVISIBLE);
                iv.setVisibility(View.INVISIBLE);
                text_view.setVisibility(View.INVISIBLE);
                convertView.setOnClickListener(null);
                imageView1.setVisibility(View.INVISIBLE);
                /*list_count.remove(5);
                    notifyDataSetChanged();*/
            }
			/*else{
				convertView.getTag();
				notifyDataSetChanged();
				iv.setVisibility(View.INVISIBLE);
				convertView.setVisibility(View.INVISIBLE);
			}*/

        }
        if (position == 6) {

            /*convertView = LayoutInflater.from(mActivity).inflate(R.layout.info_layout, null);
            TextView text_view = (TextView) convertView.findViewById(R.id.text_bottom_view);
            ImageView imageView1 = (ImageView) convertView.findViewById(R.id.dividerimage);
            View view = (convertView.findViewById(R.id.top_view));
            Log.e("bytes Check", "" + DataHandler.getIntPreferences(AppConstants.PIXELS_DIFFERENCE));
            float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DataHandler.getIntPreferences(AppConstants.PIXELS_DIFFERENCE), mActivity.getResources().getDisplayMetrics());

            view.setLayoutParams(new RelativeLayout.LayoutParams(-2, (int) pixels));*/


		/*	if (DataHandler.getDoublePreference(AppConstants.PREF_KEY_SCREEN_SIZE)!=null&&DataHandler.getDoublePreference(AppConstants.PREF_KEY_SCREEN_SIZE)>0.0){
				 Log.e("bytes Check",""+DataHandler.getDoublePreference(AppConstants.PREF_KEY_SCREEN_SIZE));


				 if (DataHandler.getDoublePreference(AppConstants.PREF_KEY_SCREEN_SIZE)>4.3 && DataHandler.getDoublePreference(AppConstants.PREF_KEY_SCREEN_SIZE)<=5.1){
					  view.setBackground(mActivity.getResources().getDrawable(R.drawable.custom_drawer_gap_shape));
				 }else if (DataHandler.getDoublePreference(AppConstants.PREF_KEY_SCREEN_SIZE)<=4.3){
					 view.setBackground(mActivity.getResources().getDrawable(R.drawable.custom_drawer_gap_shape_small));

				 }else {
					 view.setBackground(mActivity.getResources().getDrawable(R.drawable.custom_drawer_gap_shape_big));

				 }

			 }*/
            /*convertView.setOnClickListener(null);*/
            iv.setVisibility(View.INVISIBLE);
            tv.setText(mActivity.getResources().getString(R.string.FAQ));
            //text_view.setTextSize(19f);


        }
        if (position == 7) {
            tv.setText(mActivity.getResources().getString(R.string.title_contact_us));
            iv.setVisibility(View.INVISIBLE);

            //	iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.credits));
        }
        if (position == 8) {
            tv.setText(mActivity.getResources().getString(R.string.title_accident_help));
            tv.setTextColor(mActivity.getResources().getColor(R.color.orange));
            iv.setVisibility(View.INVISIBLE);
            //	iv.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.credits));
        }
        if (position == 9) {
            tv.setText("2.5");
            tv.setClickable(false);
            tv.setTextColor(mActivity.getResources().getColor(R.color.colorBorder));
            iv.setVisibility(View.INVISIBLE);
            imageView.setVisibility(View.INVISIBLE);
            convertView.setOnClickListener(null);
        }

        //	tv.setTypeface(UIUtils.getInstance().getLightFont(mActivity));

        return convertView;
    }
}
