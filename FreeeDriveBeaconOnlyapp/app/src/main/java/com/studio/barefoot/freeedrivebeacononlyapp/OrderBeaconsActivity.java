package com.studio.barefoot.freeedrivebeacononlyapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.FetchProfileAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.OrderBeaconsAsyncTasks;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.OrderBeaconSuccessDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.interfacess.IRate;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.ProfileActivity.progressBarDialogProfile;


public class OrderBeaconsActivity extends AppCompatActivity implements IRate{
    EditText et_frstName, et_LastName, et_Email, et_Phone,et_no_of_beacons;
    Button btn_order_beacons;
    public static ProgressBarDialog progressBarDialogOrderBeacons;
    private String TAG="OrderBeaconsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_beacons);
        // For setting up the different logo of toll bar for demo and paid mode
        RelativeLayout toolbar = (RelativeLayout)findViewById(R.id.topToolbar);
        ImageView fd_logo = (ImageView)toolbar.findViewById(R.id.tol_bar_logo);
        if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)){
            fd_logo.setImageResource(R.drawable.logo_freeedrive_demo_mode);
        }  else{
            fd_logo.setImageResource(R.drawable.logo_topbar);
        }

        setupActionBar();
        et_LastName = (EditText) findViewById(R.id.et_lastName);
        et_Email = (EditText) findViewById(R.id.et_Email);
        et_frstName = (EditText) findViewById(R.id.et_firstName);
        et_Phone = (EditText) findViewById(R.id.et_phn);
        et_no_of_beacons = (EditText) findViewById(R.id.et_no_of_beacons);
        btn_order_beacons = (Button) findViewById(R.id.btn_order_beacons);
        et_frstName.setMaxLines(1);
        et_frstName.setSingleLine();

        et_LastName.setMaxLines(1);
        et_LastName.setSingleLine();

        et_Email.setMaxLines(1);
        et_Email.setSingleLine();


        et_Phone.setMaxLines(1);
        et_Phone.setSingleLine();


        et_no_of_beacons.setMaxLines(1);
        et_no_of_beacons.setSingleLine();
        setupProfile();

        btn_order_beacons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (!checkFields()) {
                        if (AppUtils.isNetworkAvailable()) {
                            if (progressBarDialogOrderBeacons == null) {
                                progressBarDialogOrderBeacons = new ProgressBarDialog(OrderBeaconsActivity.this);
                            }
                            progressBarDialogOrderBeacons.setTitle(getString(R.string.title_progress_dialog));
                            progressBarDialogOrderBeacons.setMessage(getString(R.string.body_progress_dialog));
                            progressBarDialogOrderBeacons.show();
                            List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                            String name,phn,email,l_name="";

                            if (et_Phone.getText().toString().isEmpty()){
                                phn = et_Phone.getHint().toString();
                            }else{
                                phn = et_Phone.getText().toString();
                            }

                            if (et_frstName.getText().toString().isEmpty()){
                                name = et_frstName.getHint().toString();
                            }else{
                                name = et_frstName.getText().toString();
                            }

                            if (et_LastName.getText().toString().isEmpty()){
                                l_name = et_LastName.getHint().toString();
                            }else{
                                l_name = et_LastName.getText().toString();
                            }

                            if (et_Email.getText().toString().isEmpty()){
                                email = et_Email.getHint().toString();
                            }else{
                                email = et_Email.getText().toString();
                            }



                            mParams.add(new BasicNameValuePair("phone_number",phn));
                            mParams.add(new BasicNameValuePair("first_name",name));
                            mParams.add(new BasicNameValuePair("last_name",l_name));
                            mParams.add(new BasicNameValuePair("email",email));
                            mParams.add(new BasicNameValuePair("beacon_order",et_no_of_beacons.getText().toString()));

                            Log.e(TAG, "PARAMS "+mParams );

                            OrderBeaconsAsyncTasks orderBeaconsAsyncTasks = new OrderBeaconsAsyncTasks(OrderBeaconsActivity.this,WebServiceConstants.END_POINT_ORDER_BEACONS,mParams,OrderBeaconsActivity.this);
                            orderBeaconsAsyncTasks.execute();
                        } else {
                            AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(OrderBeaconsActivity.this);
                            error_No_Internet.setMessage(OrderBeaconsActivity.this.getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            error_No_Internet.show();
                        }
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }

            }
        });

    }


    private void setupProfile() {
        String jsonObjProfile = DataHandler.getStringPreferences(AppConstants.TEMP_PROFILE_KEY);
        Log.e("jsonObjProfile :", jsonObjProfile);
        if (jsonObjProfile != null && !jsonObjProfile.isEmpty()) {
            try {
                JSONObject jsonObject = new JSONObject(jsonObjProfile);
                et_frstName.setHint(jsonObject.getString("first_name"));
                et_LastName.setHint(jsonObject.getString("last_name"));
                et_Email.setHint(jsonObject.getString("email").trim());
                et_Phone.setHint(DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER));
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }


        } else {
            if (AppUtils.isNetworkAvailable()) {

                fetchUserData();
            } else {
                AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(OrderBeaconsActivity.this);
                error_No_Internet.setMessage(OrderBeaconsActivity.this.getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                error_No_Internet.show();
            /*    try{
                    progressBarDialogProfile.dismiss();
                    progressBarDialogProfile = null;
                }catch (NullPointerException exception){
                    exception.printStackTrace();
                }*/
            }
        }
    }

    public void fetchUserData() {

        try {
            if (progressBarDialogProfile == null) {
                progressBarDialogProfile = new ProgressBarDialog(OrderBeaconsActivity.this);
            }
            progressBarDialogProfile.setTitle(getString(R.string.title_progress_dialog));
            progressBarDialogProfile.setMessage(getString(R.string.body_progress_dialog));
            progressBarDialogProfile.show();
            List<NameValuePair> mParams = new ArrayList<NameValuePair>();
            final TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String deviceId = telephonyManager.getDeviceId();
            mParams.add(new BasicNameValuePair("devID",deviceId));
            mParams.add(new BasicNameValuePair("phone_number", DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
            Log.e("ProfileParams", mParams.toString());
            FetchProfileAsyncTask fetchProfileAsyncTask = new FetchProfileAsyncTask(OrderBeaconsActivity.this, WebServiceConstants.END_POINT_FETCH_PROFILE, mParams, true);
            fetchProfileAsyncTask.execute();
        } catch (NullPointerException exception) {

        }

    }

    public boolean checkFields() {
        et_frstName.setError(null);
        et_LastName.setError(null);
        et_Email.setError(null);
        /*et_Phone.setError(null);*/
        et_Phone.setError(null);
        et_no_of_beacons.setError(null);
        boolean cancel = false;
        View focusView = null;
        // email= et_Email.getText().toString().trim().replaceAll("\\s++$", "");
        // email= et_Email.getText().toString().trim().replaceAll("^\\s+|\\s+$", "");


        String phn,name,l_name,email="";
        if (et_Phone.getText().toString().isEmpty()){
            phn = et_Phone.getHint().toString();
        }else{
            phn = et_Phone.getText().toString();
        }

        if (et_frstName.getText().toString().isEmpty()){
            name = et_frstName.getHint().toString();
        }else{
            name = et_frstName.getText().toString();
        }

        if (et_LastName.getText().toString().isEmpty()){
            l_name = et_LastName.getHint().toString();
        }else{
            l_name = et_LastName.getText().toString();
        }

        if (et_Email.getText().toString().isEmpty()){
            email = et_Email.getHint().toString();
        }else{
            email = et_Email.getText().toString();
        }



        if (TextUtils.isEmpty(name)) {
                   et_frstName.setError(getString(R.string.error_field_required));
                   focusView = et_frstName;
                   cancel = true;




        } else if (TextUtils.isEmpty(l_name)) {

                et_LastName.setError(getString(R.string.error_field_required));
                focusView = et_LastName;
                cancel = true;
        } else if (TextUtils.isEmpty(email)) {

                et_Email.setError(getString(R.string.error_field_required));
                focusView = et_Email;
                cancel = true;
        } else if (!AppUtils.emailValidator(email)) {

                et_Email.setError(getString(R.string.valid_email));
                focusView = et_Email;
                cancel = true;
        }
        /*else if (TextUtils.isEmpty(et_Phone.getText().toString().trim())) {

            et_Phone.setError(getString(R.string.error_field_required));
            focusView = et_Phone;
            cancel = true;
        }*/ else if (TextUtils.isEmpty(phn)) {
            et_Phone.setError(getString(R.string.error_field_required));
            focusView = et_Phone;
            cancel = true;
        }
        else if (AppUtils.isValidPhone(phn)) {
                    et_Phone.setError(getString(R.string.error_invalid_mobilenumber));
                    focusView = et_Phone;
                    cancel = true;
                }else if (!AppUtils.isValidPhonemaxlength(phn)) {
                    et_Phone.setError(getString(R.string.error_invalid_mobilenumber_length));
                    focusView = et_Phone;
                    cancel = true;

                }
        else if (TextUtils.isEmpty(et_no_of_beacons.getText().toString())) {

            et_no_of_beacons.setError(getString(R.string.error_field_required));
            focusView = et_no_of_beacons;
            cancel = true;
        }
        if (cancel) {

            focusView.requestFocus();

        }
        return cancel;
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }

    @Override
    public void managerate(String response) {
        Log.e("Response",""+response);
        try{
            if(response != null && response.length() > 0){
                //Log.i("Adneom", "RateActivity) response from server is " + response);
                /*final AlertDialog.Builder builder = new AlertDialog.Builder(ContactUsActivity.this);
                builder.setMessage(getResources().getString(R.string.popup_confirmation_feedback));
                builder.setPositiveButton(getResources().getString(R.string.popup_confirmation_btn_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        Intent intent = new Intent(ContactUsActivity.this,MenuActivity.class);
                        startActivity(intent);
                    }
                });
                builder.show();*/
                FragmentManager fm = getSupportFragmentManager();
                OrderBeaconSuccessDialog beaconInfoDialog = OrderBeaconSuccessDialog.newInstance("");
                beaconInfoDialog.show(fm, "");
            }
        }catch (Exception exception){
            exception.printStackTrace();
        }

    }


}
