package com.studio.barefoot.freeedrivebeacononlyapp.dialogs;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.RegisterActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.VerificationActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.RegisterAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;

import java.util.List;

/**
 * Created by mcs on 12/28/2016.
 */

public class PrivacyDialog extends BaseAlertDialog  {
    TextView tv_privacy_Link;
    Button btn_accept_privacy;

    ContentResolver resolver;
    KeyEvent keyEvent;

    public PrivacyDialog(final Context context, final List<NameValuePair> mParams) {
        super(context);
        LayoutInflater factory = LayoutInflater.from(context);
        final View progressBarView = factory.inflate(R.layout.custom_dialog_privacy, null);

        setView(progressBarView);
        this.context = context;
        tv_privacy_Link = (TextView) progressBarView.findViewById(R.id.tv_text_privacy_link);
        btn_accept_privacy = (Button) progressBarView.findViewById(R.id.img_accept_privacy);
        tv_privacy_Link.setPaintFlags(tv_privacy_Link.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tv_privacy_Link.setText(context.getResources().getString(R.string.link_privacy_dialog));
        resolver = context.getContentResolver();
        keyEvent = new KeyEvent(4,4);
        setCancelable(false);

        tv_privacy_Link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.freeedrive.com/privacy"));
                intentBrowser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ApplicationController.getmAppcontext().startActivity(intentBrowser);
            }
        });

        btn_accept_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                DataHandler.updatePreferences(AppConstants.USER_ACCEPTED_PRIVACY,true);
                RegisterAsyncTask registerAsyncTask = new RegisterAsyncTask(getContext(), WebServiceConstants.END_POINT_REGISTER, mParams);
                registerAsyncTask.execute();

            }
        });
        onKeyDown(4,keyEvent);
    }



    @Override
    public void dismiss() {
            super.dismiss();
    }


}
