package com.studio.barefoot.freeedrivebeacononlyapp.dialogs;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;

/**
 * Created by mcs on 12/28/2016.
 */

public class OpenBatterySettingsDialog extends BaseAlertDialog implements DialogInterface.OnClickListener {
   TextView tv_uuid;
    ContentResolver resolver;


    public OpenBatterySettingsDialog(Context context) {
        super(context);

        LayoutInflater factory = LayoutInflater.from(context);
        final View progressBarView = factory.inflate(R.layout.custom_dialog_uuid, null);

        setView(progressBarView);
        this.context = context;
        tv_uuid = (TextView) progressBarView.findViewById(R.id.tv_uuid);
        setButton(BUTTON_POSITIVE, context.getString(R.string.action_send), this);
        setButton(BUTTON_NEGATIVE, context.getString(R.string.act_cancel), this);
        setCancelable(false);
        /*setTitle("ACCESS DONOT DISTURB MODE");*/

        setTitle(context.getResources().getString(R.string.battery_settings));
        resolver = context.getContentResolver();

            tv_uuid.setText(context.getResources().getString(R.string.desc_battery_settings));
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which){
            case BUTTON_POSITIVE:
                dialog.dismiss();
                String packageName = context.getPackageName();

                Intent batterySettingsIntent = new Intent();
                batterySettingsIntent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                batterySettingsIntent.setData(Uri.parse("package:" + packageName));
                context.startActivity(batterySettingsIntent);
               //context.startActivity(new Intent("android.settings.APP_NOTIFICATION_SETTINGS"));
                 dismiss();
                break;
            case BUTTON_NEGATIVE:
                dismiss();
        }

    }

    @Override
    public void dismiss() {
            super.dismiss();
    }
}
