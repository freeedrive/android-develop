package com.studio.barefoot.freeedrivebeacononlyapp.beans;

/**
 * Created by Yasir Barefoot on 8/30/2017.
 */

public class FleetStatsBeans {

    private int no_of_drivers;
    /*private int total_drivetime;
    private int no_of_peeks;*/
    private float safety_score;
    private double company_avg;
    private String company_name;
    private String fleet_drivedate;
    private int fleet_ranking;

    public int getNo_of_drivers() {
        return no_of_drivers;
    }

    public void setNo_of_drivers(int no_of_drivers) {
        this.no_of_drivers = no_of_drivers;
    }

    /*public int getTotal_drivetime() {
        return total_drivetime;
    }

    public void setTotal_drivetime(int total_drivetime) {
        this.total_drivetime = total_drivetime;
    }

    public int getNo_of_peeks() {
        return no_of_peeks;
    }

    public void setNo_of_peeks(int no_of_peeks) {
        this.no_of_peeks = no_of_peeks;
    }*/

    public float getSafety_score() {
        return safety_score;
    }

    public void setSafety_score(float safety_score) {
        this.safety_score = safety_score;
    }

    public double getCompany_avg() {
        return company_avg;
    }

    public void setCompany_avg(double company_avg) {
        this.company_avg = company_avg;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getFleet_drivedate() {
        return fleet_drivedate;
    }

    public void setFleet_drivedate(String fleet_drivedate) {
        this.fleet_drivedate = fleet_drivedate;
    }

    public int getFleet_ranking() {
        return fleet_ranking;
    }

    public void setFleet_ranking(int fleet_ranking) {
        this.fleet_ranking = fleet_ranking;
    }
}
