package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.MenuActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.QrCodeActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.OrderBeaconSuccessDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.interfacess.IRate;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

import org.apache.http.NameValuePair;

import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.OrderBeaconsActivity.progressBarDialogOrderBeacons;


/**
 * Created by mcs on 1/5/2017.
 */

public class OrderBeaconsAsyncTasks extends BaseAsyncTask {

    private IRate iRate;

    /**
     * AsyncTask method basic calls during a request, calls the parent's method.
     */
    protected String doInBackground(String... params) {

        return "";
    }
    public OrderBeaconsAsyncTasks(Context context, String route, List<NameValuePair> pp, IRate iRate) {
        super(context, route, pp);
        this.iRate = iRate;
    }

    /**
     * AsyncTask method basic calls after a request.
     */
    protected void onPostExecute(String s) {
        super.onPostExecute(s);



        Log.e("s",s);
        if(s != null) {
            try {
                progressBarDialogOrderBeacons.dismiss();
                progressBarDialogOrderBeacons =null;
            }catch (NullPointerException e){

            }
            int intResponse = Integer.parseInt(response);
            Log.e("ResponseCode", "" + intResponse);
            Log.e("response", "" + response);
            Log.e("resultat",resultat);
            switch (intResponse) {
                case 200:
                    Log.e("Response",""+intResponse);
                    iRate.managerate(resultat);

                    break;

                case 500:
                    AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                    error_500.setMessage(context.getResources().getString(R.string.error_sms_500)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_500.show();
                    //internal error
                    break;
                case 401:
                    //phone no not found
                    AlertDialog.Builder error_401 = new AlertDialog.Builder(context);
                    error_401.setMessage(context.getResources().getString(R.string.error_changePhone_401)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_401.show();
                    break;


            }
        }else {
            try {
                progressBarDialogOrderBeacons.dismiss();
                progressBarDialogOrderBeacons =null;
            }catch (NullPointerException e){

            }

        }
    }
}
