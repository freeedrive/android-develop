package com.studio.barefoot.freeedrivebeacononlyapp.services;

/**
 * Created by macbookpro on 1/15/18.
 */

/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


        import android.app.ActivityManager;
        import android.app.Notification;
        import android.app.NotificationManager;
        import android.app.PendingIntent;
        import android.app.Service;
        import android.content.Context;
        import android.content.Intent;
        import android.content.res.Configuration;
        import android.location.Location;
        import android.media.RingtoneManager;
        import android.net.Uri;
        import android.os.Binder;
        import android.os.Build;
        import android.os.Handler;
        import android.os.HandlerThread;
        import android.os.IBinder;
        import android.os.Looper;
        import android.support.annotation.NonNull;
        import android.support.v4.app.NotificationCompat;
        import android.support.v4.content.LocalBroadcastManager;
        import android.util.Log;

        import com.google.android.gms.location.FusedLocationProviderClient;
        import com.google.android.gms.location.LocationCallback;
        import com.google.android.gms.location.LocationRequest;
        import com.google.android.gms.location.LocationResult;
        import com.google.android.gms.location.LocationServices;
        import com.google.android.gms.tasks.OnCompleteListener;
        import com.google.android.gms.tasks.Task;
        import com.studio.barefoot.freeedrivebeacononlyapp.MainActivity;
        import com.studio.barefoot.freeedrivebeacononlyapp.R;
        import com.studio.barefoot.freeedrivebeacononlyapp.beans.RidesBeans;
        import com.studio.barefoot.freeedrivebeacononlyapp.beans.SubRideBeans;
        import com.studio.barefoot.freeedrivebeacononlyapp.fragments.MySafetyFragmentGraph;
        import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
        import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
        import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
        import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocationUtils;
        import com.studio.barefoot.freeedrivebeacononlyapp.utils.LoggingOperations;

        import java.sql.Timestamp;
        import java.util.ArrayList;
        import java.util.Timer;
        import java.util.TimerTask;

        import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.broadcastFlag;
        import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.currentSpeed;
        import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.currentTime;
        import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.minutesPassengerSwitchOff;
        import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.passengerModeFlag;
        import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.subRideBeansArrayList;
        import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.totalminutesPassengerMode;
        import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.formateLongToOnlyDateForServer;
        import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.ispassengerMode;
        import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.passengerMode;
        import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.speedMonitorCheck;

/**
 * A bound and started service that is promoted to a foreground service when location updates have
 * been requested and all clients unbind.
 *
 * For apps running in the background on "O" devices, location is computed only once every 10
 * minutes and delivered batched every 30 minutes. This restriction applies even to apps
 * targeting "N" or lower which are run on "O" devices.
 *
 * This sample show how to use a long-running service for location updates. When an activity is
 * bound to this service, frequent location updates are permitted. When the activity is removed
 * from the foreground, the service promotes itself to a foreground service, and location updates
 * continue. When the activity comes back to the foreground, the foreground service stops, and the
 * notification assocaited with that service is removed.
 */
public class ForegroundLocationService extends Service {

    private static final String PACKAGE_NAME =
            "com.google.android.gms.location.sample.locationupdatesforegroundservice";

    private static final String TAG = ForegroundLocationService.class.getSimpleName();

    static final String ACTION_BROADCAST = PACKAGE_NAME + ".broadcast";

    static final String EXTRA_LOCATION = PACKAGE_NAME + ".location";
    private static final String EXTRA_STARTED_FROM_NOTIFICATION = PACKAGE_NAME + ".started_from_notification";

    private final IBinder mBinder = new LocalBinder();

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 500;

    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    /**
     * The identifier for the notification displayed for the foreground service.
     */
    private static final int NOTIFICATION_ID = 123456;

    /**
     * Used to check whether the bound activity has really gone away and not unbound as part of an
     * orientation change. We create a foreground service notification only if the former takes
     * place.
     */
    private boolean mChangingConfiguration = false;

    private NotificationManager mNotificationManager;

    /**
     * Contains parameters used by {@link com.google.android.gms.location.FusedLocationProviderApi}.
     */
    private LocationRequest mLocationRequest;

    /**
     * Provides access to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Callback for changes in location.
     */
    private LocationCallback mLocationCallback;

    private Handler mServiceHandler;

    /**
     * The current location.
     */
    private Location mLocation;


    Location mCurrentLocation;
    Timer shortTimer;
    Timer longTimer;
    boolean shortTimerPause = false;
    boolean isTaskCompleted = false;
    private long minutesPassengerSwitchOn = 0;





    /*public static final String

            ACTION_LOCATION_BROADCAST = BackgroundBeaconScan.class.getName() + "LocationBroadcast",
            EXTRA_LATITUDE = "extra_latitude",
            CURRENT_SPEED = "current_speed",
            EXTRA_LONGITUDE = "extra_longitude";*/

    Timer future2minTimer;
    public static ArrayList<Object> timeSave = new ArrayList<Object>();
    /*public static ArrayList<Object> passengerModeTime = new ArrayList<Object>();*/
    RidesBeans ridesBeans;
    public static long totalTimeinSeconds = 0;
    boolean greaterthan120speedflag = true;
    boolean speedIsGreaterConfifenceFlag = false;
    boolean speedIsLessConfifenceFlag = false;
    boolean lessthan120speedflag = false;
    private boolean lessthan10speedflag = false;

    public ArrayList<Float> arrayListSpeed = new ArrayList<Float>();
    public ArrayList<Float> speedCompare = new ArrayList<Float>();
    int speedCount = 0;
    public static int accelerate = 0;
    public static int braking = 0;
    float previousSpeed = 0.0f;
    float latestSpeed;
    int speedCount_ = 0;
    Location mLastLocation;


    SubRideBeans subRideBeans;

    double differenceTime = 0.5;
    Long lastTimeStamp = 0L;
    String currentRegion = "";
    private int secsInsideAregion = 0;

    //for manual speed
    double lat_old=0.0;
    double lon_old=0.0;
    double time=2;
    private int countValue=0;


    public ForegroundLocationService() {
    }

    @Override
    public void onCreate() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                onNewLocation(locationResult.getLastLocation());
            }
        };

        createLocationRequest();
        getLastLocation();

        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        mServiceHandler = new Handler(handlerThread.getLooper());
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        LoggingOperations logger = new LoggingOperations();
        Thread.setDefaultUncaughtExceptionHandler(logger);
        Long currentTime =  System.currentTimeMillis();
        String time=  formateLongToOnlyDateForServer(currentTime);
        LoggingOperations.writeToFile(this,"LOCATION > "+time +" -- > "+ "ON");
        timeSave = new ArrayList<Object>();
       // mNotificationManager.notify(NOTIFICATION_ID, getNotification());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "Service started");
        boolean startedFromNotification = intent.getBooleanExtra(EXTRA_STARTED_FROM_NOTIFICATION, false);

        // We got here because the user decided to remove location updates from the notification.
        if (startedFromNotification) {
            removeLocationUpdates();
            stopSelf();
        }

        // Tells the system to not try to recreate the service after it has been killed.
        return START_NOT_STICKY;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mChangingConfiguration = true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) comes to the foreground
        // and binds with this service. The service should cease to be a foreground service
        // when that happens.
        Log.e(TAG, "in onBind()");
        stopForeground(true);
        mChangingConfiguration = false;
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) returns to the foreground
        // and binds once again with this service. The service should cease to be a foreground
        // service when that happens.
        Log.e(TAG, "in onRebind()");
        stopForeground(true);
        mChangingConfiguration = false;
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.e(TAG, "Last client unbound from service");

        // Called when the last client (MainActivity in case of this sample) unbinds from this
        // service. If this method is called due to a configuration change in MainActivity, we
        // do nothing. Otherwise, we make this service a foreground service.
        if (!mChangingConfiguration && LocationUtils.requestingLocationUpdates(this)) {
            Log.e(TAG, "Starting foreground service");
            /*
            // TODO(developer). If targeting O, use the following code.
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
                mNotificationManager.startServiceInForeground(new Intent(this,
                        ForegroundLocationService.class), NOTIFICATION_ID, getNotification());
            } else {
                startForeground(NOTIFICATION_ID, getNotification());
            }
             */
            startForeground(NOTIFICATION_ID, getNotification());
        }
        return true; // Ensures onRebind() is called when a client re-binds.
    }

    @Override
    public void onDestroy() {
        mServiceHandler.removeCallbacksAndMessages(null);
        AppConstants.isLocationActive = false;
        broadcastFlag = true;
        stopForeground(true);
        stopSelf();
        mNotificationManager.cancelAll();


        Log.e(TAG, "Location Destroyed");

        try {
            Long currentTime =  System.currentTimeMillis();
            String time=  formateLongToOnlyDateForServer(currentTime);
            LoggingOperations.writeToFile(this,"LOCATION > "+time +" -- > "+ "OFF");
            if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)) {
                if (mCurrentLocation != null && mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {
                    Detector.FdApp.arrivalLocation = mCurrentLocation;
                } else {
                    Detector.FdApp.arrivalLocation = mLastLocation;
                }
            }
            Log.d(TAG, "onStop fired ..............");
            AppConstants.isLocationActive = false;

            mCurrentLocation = null;
            if (longTimer != null) {
                longTimer.cancel();
                longTimer = null;
                shortTimerPause = false;
                Log.e("longTimer", "Cancel");
                //startFDSensor();
            }
       /*     if(future2minTimer != null){
                future2minTimer.cancel();
                future2minTimer = null;
                shortTimerPause = false;
                Log.e("longTimer","Cancel");
                //startFDSensor();
            }*/
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    /**
     * Makes a request for location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void requestLocationUpdates() {
        Log.e(TAG, "Requesting location updates");
        LocationUtils.setRequestingLocationUpdates(this, true);
        startService(new Intent(getApplicationContext(), ForegroundLocationService.class));
        startForeground(9,getNotification());
        try {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback, Looper.myLooper());
        } catch (SecurityException unlikely) {
            LocationUtils.setRequestingLocationUpdates(this, false);
            Log.e(TAG, "Lost location permission. Could not request updates. " + unlikely);
        }
    }

    /**
     * Removes location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void removeLocationUpdates() {
        Log.e(TAG, "Removing location updates");
        try {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        //    LocationUtils.setRequestingLocationUpdates(this, false);
            stopSelf();
        } catch (SecurityException unlikely) {
            LocationUtils.setRequestingLocationUpdates(this, true);
            Log.e(TAG, "Lost location permission. Could not remove updates. " + unlikely);
        }
        AppConstants.isLocationActive = false;
        mServiceHandler.removeCallbacksAndMessages(null);
        stopForeground(true);
        stopSelf();
        mNotificationManager.cancel(NOTIFICATION_ID);
    }

    /**
     * Returns the {@link NotificationCompat} used as part of the foreground service.
     */
    private Notification getNotification() {
        Intent intent = new Intent(this, ForegroundLocationService.class);


        // Extra to help us figure out if we arrived in onStartCommand via the notification or not.
        intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true);

        // The PendingIntent that leads to a call to onStartCommand() in this service.
        PendingIntent servicePendingIntent = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // The PendingIntent to launch activity.
        PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, ForegroundLocationService.class), 0);

        return new NotificationCompat.Builder(this)
                .addAction(R.drawable.botom_bar_car_icon, this.getString(R.string.label_foreground_notification),
                        servicePendingIntent)
                .setContentText(this.getString(R.string.context_foreground_notification))
                .setContentTitle(this.getString(R.string.title_foreground_notification))
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.drawable.freee_drive_statusbar_logo)
                .setAutoCancel(true)
                .setDeleteIntent(servicePendingIntent)
                .setWhen(System.currentTimeMillis()).build();
    }

    private void getLastLocation() {
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnCompleteListener(new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                mLocation = task.getResult();
                            } else {
                                Log.e(TAG, "Failed to get location.");
                            }
                        }
                    });
        } catch (SecurityException unlikely) {
            Log.e(TAG, "Lost location permission." + unlikely);
        }
    }

    private void onNewLocation(Location location) {

        Log.e(TAG, "New location: " + location);

        mLocation = location;

        AppConstants.isLocationActive = true;



        mCurrentLocation = location;
        speedCount++;
        //mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        // updateUI();
        try {

            currentSpeed = 0.0f;
            if (null != mCurrentLocation) {
                String lat = String.valueOf(mCurrentLocation.getLatitude());
                String lng = String.valueOf(mCurrentLocation.getLongitude());
                //String speed = String.valueOf(mCurrentLocation.getSpeed());
                Log.e("Latitude :", lat);
                Log.e("Longitude :", lng);
                // sendBroadcastMessage(location);

                //currentSpeed = mCurrentLocation.getSpeed()*3.6f;

                Log.e(TAG, "Speed " + mLocation.getSpeed());
                lat_old=mCurrentLocation.getLatitude();
                lon_old=mCurrentLocation.getLongitude();

                if (mCurrentLocation != null && mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {
                    if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE))
                        DataHandler.updatePreferences(AppConstants.ARRIVAL_LAT,mCurrentLocation.getLatitude());
                    DataHandler.updatePreferences(AppConstants.ARRIVAL_LANG,mCurrentLocation.getLongitude());
                }
                //  mCurrentLocation=null;
                //currentSpeed = 150;

               currentSpeed = MySafetyFragmentGraph.speed;
                arrayListSpeed.add(currentSpeed);
                LoggingOperations logger = new LoggingOperations();
                Thread.setDefaultUncaughtExceptionHandler(logger);
                Long currentTimeLogs =  System.currentTimeMillis();
                String time=  formateLongToOnlyDateForServer(currentTimeLogs);
                LoggingOperations.writeToFile(this,time+"--- Lat >: "+ lat + "Lng > :  " + lng + "-->Speed : " + currentSpeed);

          /*  speedCompare.add(currentSpeed);
            for(int i=0;i<speedCompare.size();i++){
                speedCompare.set(0,currentSpeed);

                if(speedCompare.size() > 2){
                    speedCompare.remove(0);
                    float difference = speedCompare.get(0) - speedCompare.get(1);
                    Log.e("difference",""+difference);
                }


            }*/

          /*  arrayListSpeed.set(0,currentSpeed);

            if(arrayListSpeed.size() > 2){

                arrayListSpeed.remove(0);
                float difference = arrayListSpeed.get(1) - arrayListSpeed.get(0);
                Log.e("difference",""+difference);
                if(difference == 23){
                    accelerate ++;
                    Log.e("Acceleration",""+accelerate);
                }
                else if(difference!=23){
                    braking++;
                    Log.e("Braking",""+braking);
                }



            }*/
                if (AppUtils.isUnlock){
                    countValue = 3;
                }else{
                    countValue =2;
                }

                if (speedCount >= countValue) {
                    int speedgreaterConfidence = 0;
                    int speedLessConfidence = 0;

                    for (int i = 0; i < arrayListSpeed.size(); i++) {
                        if (arrayListSpeed.get(i) >= 15) {
                            speedgreaterConfidence++;
                            Log.e("speedgreaterConfidence", "" + speedgreaterConfidence);
                        } else {
                            speedLessConfidence++;
                            Log.e("speedLessConfidence", "" + speedLessConfidence);
                        }
                    }

                    if (speedgreaterConfidence >= countValue) {
                        speedIsGreaterConfifenceFlag = true;
                        speedIsLessConfifenceFlag = false;
                    }
                    if (speedLessConfidence >= countValue) {
                        speedIsGreaterConfifenceFlag = false;
                        speedIsLessConfifenceFlag = true;
                    }
                    arrayListSpeed.clear();
                    speedCount = 0;
                }
                if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE) && DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)) {
                    speedCount_++;
                    float differenceSpeed = currentSpeed - previousSpeed;
                    if (speedCount_ == 2) {


                        if (previousSpeed == 0) {

                            previousSpeed = currentSpeed;
                            speedCount_ = 0;
                            return;
                        }
                        previousSpeed = currentSpeed;
                        Log.e("difference", "" + differenceSpeed);
                        if (differenceSpeed >= 23) {
                            accelerate++;
                            Log.e("difference", "accelerate :" + accelerate);
                        } else if (differenceSpeed <= -23) {
                            braking++;
                            Log.e("difference", "braking :" + braking);
                        }
                        speedCount_ = 0;
                    }
                }


                Log.e("Current_Speed :", "" + currentSpeed);
                Log.e("CountValue :", "" + countValue);

                //currentspeed = MenuActivity.speed;
                //        startRide();
                //currentspeed is greater then 5 km/h Start a Ride
                if (AppUtils.isInRange) {
                    if (speedIsGreaterConfifenceFlag) {
                        passengerMode = DataHandler.getIntPreferencesPassengerMode(AppConstants.KEY_PASSENGER_MODE);
                        if (broadcastFlag) {
                            Log.e("Passenger"," Mode "+passengerMode);
                            /*if(passengerMode == 1 && !passengerModeFlag){ // when bit 0 that mean passenger mode is off and 1 for On
                                passengerModeFlag = true;
                                ispassengerMode = false;

                                minutesPassengerSwitchOn = System.currentTimeMillis();
                                Log.e("minutesPassengerS",":"+minutesPassengerSwitchOn);

                            }*/
                            if(passengerMode == 0){
                                ispassengerMode = true;
                            }

                            BackgroundBeaconScan.beaconlostCounter--;
                            AppConstants.ORPHAN_RIDE_BIT = 0;
                            Intent intent = new Intent("IamInRange");
                            sendBroadcast(intent);

                          /*  Detector.FdApp.departureLocation = null;
                            Detector.FdApp.arrivalLocation = null;*/
                            Log.e("FIRED", "INRANGE");
                            Long currentTimeRec =  System.currentTimeMillis();
                            String timeRec=  formateLongToOnlyDateForServer(currentTimeRec);
                            LoggingOperations.writeToFile(this,"RECIEVER " +timeRec+" I am in Range Fired");
                            LoggingOperations.writeToFile(this,"I STARTED THE RIDE AT >" +timeRec);

                            broadcastFlag = false;
                            timeSave = new ArrayList<Object>();
                            /*passengerModeTime = new ArrayList<Object>();*/
                            if (mCurrentLocation != null && mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {
                                DataHandler.updatePreferences(AppConstants.DEPARTURE_LAT,mCurrentLocation.getLatitude());
                                DataHandler.updatePreferences(AppConstants.DEPARTURE_LANG,mCurrentLocation.getLongitude());
                                Detector.FdApp.departureLocation = mCurrentLocation;
                            } else {
                                Detector.FdApp.departureLocation = mLastLocation;
                            }
                            DataHandler.deletePreference(AppConstants.PREF_CURRENT_LATITUDE);
                        }
                        speedMonitorCheck = true;
                        AppUtils.speedCheck = true;
                        AppUtils.speedlessCheck = false;



                /*if(currentSpeed > 15){*/
//                FDUtils.isInRange = true;

                        if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE) == true) {


                            if (subRideBeans != null) {
                                if (currentSpeed < 15) {
                                    if (!subRideBeans.getStartlat().isEmpty() && !subRideBeans.getStartlng().isEmpty()) {
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {
                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                            subRideBeansArrayList.add(subRideBeans);
                                            subRideBeans = null;
                                            currentRegion = "";
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                            subRideBeansArrayList.add(subRideBeans);
                                            subRideBeans = null;
                                            currentRegion = "";
                                        }

                                    }
                                }

                            }
                            //1.2.2 -Y type region
                            if (currentSpeed >= 15 && currentSpeed < 50) {
                                Long currentTimeStamp = System.currentTimeMillis() / 1000L;

                                if (!currentRegion.isEmpty()) {
                                    if (subRideBeans == null) {
                                        subRideBeans = new SubRideBeans();
                                        secsInsideAregion = 0;
                                    }
                                    if (currentRegion.equalsIgnoreCase("L")) {
                                        secsInsideAregion++;
                                        if (subRideBeans.getStartlat().isEmpty() && subRideBeans.getStartlng().isEmpty()) {
                                            subRideBeans.setAreaType("L");
                                            if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                                subRideBeans.setStartlat(String.valueOf(mCurrentLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mCurrentLocation.getLongitude()));
                                            } else {
                                                subRideBeans.setStartlat(String.valueOf(mLastLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mLastLocation.getLongitude()));
                                            }

                                            subRideBeans.setCurrentSpeed(currentSpeed);
                                            subRideBeans.setCurrentTime(currentTimeStamp);

                                        }
                                    } else {
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        secsInsideAregion = 0;
                                        subRideBeans = null;
                                        currentRegion = "L";

                                    }


                                    if ((currentTimeStamp - lastTimeStamp) / 60 > differenceTime) {
                                        lastTimeStamp = currentTimeStamp;
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        subRideBeans = null;

                                    }
                                } else {
                                    currentRegion = "L";
                                }
                            }
                            //1.2.2 -Y type region
                            if (currentSpeed >= 50 && currentSpeed < 70) {
                                Long currentTimeStamp = System.currentTimeMillis() / 1000L;

                                if (!currentRegion.isEmpty()) {
                                    if (subRideBeans == null) {
                                        subRideBeans = new SubRideBeans();
                                        secsInsideAregion = 0;
                                    }
                                    if (currentRegion.equalsIgnoreCase("Y")) {
                                        secsInsideAregion++;
                                        if (subRideBeans.getStartlat().isEmpty() && subRideBeans.getStartlng().isEmpty()) {
                                            subRideBeans.setAreaType("Y");
                                            if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                                subRideBeans.setStartlat(String.valueOf(mCurrentLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mCurrentLocation.getLongitude()));
                                            } else {
                                                subRideBeans.setStartlat(String.valueOf(mLastLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mLastLocation.getLongitude()));
                                            }
                                            subRideBeans.setCurrentSpeed(currentSpeed);
                                            subRideBeans.setCurrentTime(currentTimeStamp);

                                        }
                                    } else {
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        secsInsideAregion = 0;
                                        subRideBeans = null;
                                        currentRegion = "Y";
                                    }


                                    if ((currentTimeStamp - lastTimeStamp) / 60 > differenceTime) {
                                        lastTimeStamp = currentTimeStamp;
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        subRideBeans = null;

                                    }
                                } else {
                                    currentRegion = "Y";
                                }
                            }


                            //1.2.2 -Z type region

                            else if (currentSpeed > 70 && currentSpeed < 90) {

                                //current z
                                // previous q
                                //if mismatch we update end lat/lng and put in array and object to be null
                                // if they matched , we get the start lat lng ,they are not empty, and update end lat/lng


                                // very first logic update and then previous = z and current =z


                                Long currentTimeStamp = System.currentTimeMillis() / 1000L;

                                if (!currentRegion.isEmpty()) {
                                    if (subRideBeans == null) {
                                        subRideBeans = new SubRideBeans();

                                    }
                                    if (currentRegion.equalsIgnoreCase("Z")) {
                                        secsInsideAregion++;
                                        if (subRideBeans.getStartlat().isEmpty() && subRideBeans.getStartlng().isEmpty()) {
                                            subRideBeans.setAreaType("Z");
                                            if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                                subRideBeans.setStartlat(String.valueOf(mCurrentLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mCurrentLocation.getLongitude()));
                                            } else {
                                                subRideBeans.setStartlat(String.valueOf(mLastLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mLastLocation.getLongitude()));
                                            }

                                            subRideBeans.setCurrentSpeed(currentSpeed);
                                            subRideBeans.setCurrentTime(currentTimeStamp);

                                        }
                                    } else {
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        secsInsideAregion = 0;
                                        subRideBeans = null;
                                        currentRegion = "Z";
                                    }
                                    if ((currentTimeStamp - lastTimeStamp) / 60 > differenceTime) {
                                        lastTimeStamp = currentTimeStamp;
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        subRideBeans = null;

                                    }
                                } else {
                                    currentRegion = "Z";
                                }


                            }
                            //1.2.2 W- type region

                            else if (currentSpeed > 90 && currentSpeed < 120) {


                                Long currentTimeStamp = System.currentTimeMillis() / 1000L;

                                if (!currentRegion.isEmpty()) {
                                    if (subRideBeans == null) {
                                        subRideBeans = new SubRideBeans();
                                        secsInsideAregion = 0;

                                    }
                                    if (currentRegion.equalsIgnoreCase("W")) {
                                        secsInsideAregion++;

                                        if (subRideBeans.getStartlat().isEmpty() && subRideBeans.getStartlng().isEmpty()) {
                                            subRideBeans.setAreaType("W");
                                            if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                                subRideBeans.setStartlat(String.valueOf(mCurrentLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mCurrentLocation.getLongitude()));
                                            } else {
                                                subRideBeans.setStartlat(String.valueOf(mLastLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mLastLocation.getLongitude()));
                                            }

                                            subRideBeans.setCurrentSpeed(currentSpeed);
                                            subRideBeans.setCurrentTime(currentTimeStamp);

                                        }
                                    } else {
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        secsInsideAregion = 0;
                                        subRideBeans = null;
                                        currentRegion = "W";
                                    }
                                    if ((currentTimeStamp - lastTimeStamp) / 60 > differenceTime) {
                                        lastTimeStamp = currentTimeStamp;
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        subRideBeans = null;

                                    }
                                } else {
                                    currentRegion = "W";
                                }


                            }
                            //1.2.2 -Q type region

                            else if (currentSpeed > 120) {


                                Long currentTimeStamp = System.currentTimeMillis() / 1000L;

                                if (!currentRegion.isEmpty()) {
                                    if (subRideBeans == null) {
                                        subRideBeans = new SubRideBeans();
                                        secsInsideAregion = 0;

                                    }
                                    if (currentRegion.equalsIgnoreCase("Q")) {
                                        secsInsideAregion++;

                                        if (subRideBeans.getStartlat().isEmpty() && subRideBeans.getStartlng().isEmpty()) {
                                            subRideBeans.setAreaType("Q");
                                            if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                                subRideBeans.setStartlat(String.valueOf(mCurrentLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mCurrentLocation.getLongitude()));
                                            } else {
                                                subRideBeans.setStartlat(String.valueOf(mLastLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mLastLocation.getLongitude()));
                                            }
                                            subRideBeans.setCurrentSpeed(currentSpeed);
                                            subRideBeans.setCurrentTime(currentTimeStamp);

                                        }
                                    } else {
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        subRideBeans = null;
                                        secsInsideAregion = 0;
                                        currentRegion = "Q";
                                    }
                                    if ((currentTimeStamp - lastTimeStamp) / 60 > differenceTime) {
                                        lastTimeStamp = currentTimeStamp;
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        subRideBeans = null;

                                    }
                                } else {
                                    currentRegion = "Q";
                                }


                            }
                        }

                        Log.e("subRideBeansArrayList", "array :" + subRideBeansArrayList.size() + " elements" + subRideBeansArrayList);
                        Log.e("sec in a region", ">> " + secsInsideAregion);


                        if (currentSpeed > 30 && greaterthan120speedflag) {

                            ridesBeans = new RidesBeans();

                            Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                            ridesBeans.setStarttime(timestamp);

                            Log.e("currentTime", "timeStamp Greater :" + timestamp);
                            //Toast.makeText(getApplicationContext(), "timeStamp Greater :"+timestamp, Toast.LENGTH_LONG).show();
                            greaterthan120speedflag = false;
                            lessthan120speedflag = true;
                            lessthan10speedflag = true;
                        }
                        if (currentSpeed < 30 && lessthan120speedflag && lessthan10speedflag) {

                            Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                            ridesBeans.setEndtime(timestamp);
                            Log.e("currentTime", "timeStamp Less :" + timestamp);
                            //Toast.makeText(getApplicationContext(), "timeStamp Less :"+timestamp, Toast.LENGTH_LONG).show();
                            greaterthan120speedflag = true;
                            lessthan120speedflag = false;
                            timeSave.add(ridesBeans);
                       /* if(ridesBeans != null){
                            ridesBeans = null;
                            Log.e("rideBeans","Is Null");
                        }*/
                            Log.e("currentTime", "array :" + timeSave.size());

                        }

                        //if speed is increaase within 1 minute 45 seconds
                        if (longTimer != null) {
                            longTimer.cancel();
                            longTimer = null;
                            shortTimerPause = false;
                            Log.e("longTimer", "Cancel");
                            //startFDSensor();
                        }
                    /*if(future2minTimer != null){
                       future2minTimer.cancel();
                       future2minTimer = null;
                   }*/
                        if(passengerMode == 1 && !passengerModeFlag){
                            passengerModeFlag = true;
                            ispassengerMode = false;
                            totalminutesPassengerMode =  System.currentTimeMillis()/1000L;
                            /*totalminutesPassengerMode = minutesPassengerSwitchOn;*/
                            AppConstants.switchCheck = true;
                            Log.e("totalminutes","PassengerSwitchOn :" + totalminutesPassengerMode);
                            Long currentTimeRec =  System.currentTimeMillis();
                            String timeRec=  formateLongToOnlyDateForServer(currentTimeRec);
                            LoggingOperations.writeToFile(this,"RECIEVER " +timeRec+ "Passenger Mode ---------->:"+ passengerMode +":<----------- ");

                        }
                        if(passengerMode == 0 && passengerModeFlag){
                            ispassengerMode = true;
                            passengerModeFlag = false;
                            minutesPassengerSwitchOff += System.currentTimeMillis()/1000L - totalminutesPassengerMode;

                            /*totalminutesPassengerMode = minutesPassengerSwitchOff;*/
                            AppConstants.ispassengerSwitch = true;
                            currentTime = System.currentTimeMillis()/1000L;
                            Log.e("totalminutes","currentTime :" + currentTime);
                            Log.e("totalminutes","PassengerSwitchOff :" + minutesPassengerSwitchOff);
                            Long currentTimeRec =  System.currentTimeMillis();
                            String timeRec=  formateLongToOnlyDateForServer(currentTimeRec);
                            LoggingOperations.writeToFile(this,"RECIEVER " + timeRec + " Passenger Mode ---------->:"+ passengerMode +":<----------- ");

                        }

                    } else if (speedIsLessConfifenceFlag) {
                        speedMonitorCheck = true;
                        if (speedIsLessConfifenceFlag && AppUtils.speedCheck) {

/*
                else if (currentSpeed <=15&&AppUtils.speedCheck){
*/
                            AppUtils.speedlessCheck = true;

                            if (AppUtils.speedlessCheck) {
//                    FDUtils.speedCheck = false;
                                if (lessthan10speedflag) {
                                    if (!greaterthan120speedflag) {
                                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                                        ridesBeans.setEndtime(timestamp);
                                        Log.e("currentTime", "timeStamp Less :" + timestamp);
                                        timeSave.add(ridesBeans);
                                        Log.e("currentTime", "array :" + timeSave.size());
                                        greaterthan120speedflag = true;
                                        Log.e("currentTime", "insidelessthan10speed");
                                    }
                                    lessthan10speedflag = false;
                                }
                                shortTimer();

                            }
                        }
                    }
                }

//            futureTimer();

            /*else{
                longTimer();
            }*/
                /*if(BackgroundBeaconScan.beaconlostCounter <=1 && DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE )&& currentSpeed > 15){
                    AppConstants.ORPHAN_RIDE_BIT = 0;
                }*/

            }else{
                Long currentTimeLogs =  System.currentTimeMillis();
                String time=  formateLongToOnlyDateForServer(currentTimeLogs);
                LoggingOperations.writeToFile(this,"LOCATION NULL "+time+" location obj " +mCurrentLocation+"-->Speed : " + currentSpeed);

            }
        } catch (Exception e) {
            Long currentTimeLogs =  System.currentTimeMillis();
            String time=  formateLongToOnlyDateForServer(currentTimeLogs);
            LoggingOperations.writeToFile(this,"LOCATION exception "+time+" & e is " +e);

            e.printStackTrace();
        }








        // Notify anyone listening for broadcasts about the new location.
/*        Intent intent = new Intent(ACTION_BROADCAST);
        intent.putExtra(EXTRA_LOCATION, location);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);*/

        // Update notification content if running as a foreground service.
      /*  if (serviceIsRunningInForeground(this)) {
            mNotificationManager.notify(NOTIFICATION_ID, getNotification());
        }*/
    }

    /**
     * Sets the location request parameters.
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }
    public void shortTimer() {
        LoggingOperations.writeToFile(this,"Short Timer ---------->:  Hit  :<----------- ");
        shortTimer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                //   stopFDSensor();
                //  getToasthandlerpauseRide.sendEmptyMessage(0);

                AppUtils.speedCheck = false;
                shortTimer.cancel();
                //shortTimer = null;
                if (!shortTimerPause) {

                    //longTimer();
             /*       if (BackgroundBeaconScan.beaconlostPermanently){
                        Log.e("Bfpk","Istarted Future timer");
                        futureTimer();
                    }*/
                }

            }
        };
        shortTimer.schedule(timerTask, 5000);
    }
    /**
     * Class used for the client Binder.  Since this service runs in the same process as its
     * clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public ForegroundLocationService getService() {
            return ForegroundLocationService.this;
        }
    }

    /**
     * Returns true if this is a foreground service.
     *
     * @param context The {@link Context}.
     */
    public boolean serviceIsRunningInForeground(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(
                Integer.MAX_VALUE)) {
            if (getClass().getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }
            }
        }
        return false;
    }
}

