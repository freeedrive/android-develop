package com.studio.barefoot.freeedrivebeacononlyapp.services;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.RidesBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.fragments.MySafetyFragmentGraph;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.SubRideBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LoggingOperations;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.broadcastFlag;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.currentSpeed;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.currentTime;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.minutesPassengerSwitchOff;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.passengerModeFlag;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.subRideBeansArrayList;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.totalminutesPassengerMode;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.formateLongToOnlyDateForServer;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.ispassengerMode;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.passengerMode;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.speedMonitorCheck;


public class LocationService extends Service implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    static final Double EARTH_RADIUS = 6371.00;
    private double manualSpeed = 0.0;
    private int countValue = 0;


    public abstract interface LocationCallback {
        public void handleNewLocation(Location location);
    }

    private static final String TAG = "LocationService";
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final long INTERVAL = 2000;
    private static final long FASTEST_INTERVAL = 1000;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;
    Timer shortTimer;
    Timer longTimer;
    boolean shortTimerPause = false;
    boolean isTaskCompleted = false;
    private long minutesPassengerSwitchOn = 0;



    /*public static final String

            ACTION_LOCATION_BROADCAST = BackgroundBeaconScan.class.getName() + "LocationBroadcast",
            EXTRA_LATITUDE = "extra_latitude",
            CURRENT_SPEED = "current_speed",
            EXTRA_LONGITUDE = "extra_longitude";*/

    Timer future2minTimer;
    public static ArrayList<Object> timeSave = new ArrayList<Object>();
    /*public static ArrayList<Object> passengerModeTime = new ArrayList<Object>();*/
    RidesBeans ridesBeans;
    public static long totalTimeinSeconds = 0;
    boolean greaterthan120speedflag = true;
    boolean speedIsGreaterConfifenceFlag = false;
    boolean speedIsLessConfifenceFlag = false;
    boolean lessthan120speedflag = false;
    private boolean lessthan10speedflag = false;

    public ArrayList<Float> arrayListSpeed = new ArrayList<Float>();
    public ArrayList<Float> speedCompare = new ArrayList<Float>();
    int speedCount = 0;
    public static int accelerate = 0;
    public static int braking = 0;
    float previousSpeed = 0.0f;
    float latestSpeed;
    int speedCount_ = 0;
    Location mLastLocation;


    SubRideBeans subRideBeans;

    double differenceTime = 0.5;
    Long lastTimeStamp = 0L;
    String currentRegion = "";
    private int secsInsideAregion = 0;

    //for manual speed
    double lat_old = 0.0;
    double lon_old = 0.0;
    double time = 2;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate ...............................");
        //show error dialog if GoolglePlayServices not available
        if (!isGooglePlayServicesAvailable()) {
            /*try{
                AlertDialog.Builder error_2 = new AlertDialog.Builder(this);
                error_2.setMessage(this.getResources().getString(R.string.playservices_not_available)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                error_2.show();
            }catch (Exception exception){
                exception.printStackTrace();
            }*/
            Toast.makeText(this, "GooglePlayServices Not Available Please Update it", Toast.LENGTH_LONG).show();
        }
        AppConstants.isLocationActive = true;
        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                //   .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        LoggingOperations logger = new LoggingOperations();
        Thread.setDefaultUncaughtExceptionHandler(logger);
        Long currentTime = System.currentTimeMillis();
        String time = formateLongToOnlyDateForServer(currentTime);
        LoggingOperations.writeToFile(this, "LOCATION > " + time + " -- > " + "ON");
        timeSave = new ArrayList<Object>();
       /* passengerModeTime = new ArrayList<Object>();*/
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Log.d(TAG, "onStart fired ..............");
        mGoogleApiClient.connect();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        broadcastFlag = true;
        /*stopForeground(true);*/
        /*stopSelf();*/


        Log.e(TAG, "Location Destroyed");

        try {
            Long currentTime = System.currentTimeMillis();
            String time = formateLongToOnlyDateForServer(currentTime);
            LoggingOperations.writeToFile(this, "LOCATION > " + time + " -- > " + "OFF");
            if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)) {
                if (mCurrentLocation != null && mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {
                    Detector.FdApp.arrivalLocation = mCurrentLocation;
                } else {
                    Detector.FdApp.arrivalLocation = mLastLocation;
                }
            }
            stopLocationUpdates();
            Log.d(TAG, "onStop fired ..............");
            mGoogleApiClient.disconnect();
            Log.d(TAG, "isConnected ...............: " + mGoogleApiClient.isConnected());
            AppConstants.isLocationActive = false;

            mGoogleApiClient = null;
            mCurrentLocation = null;
            if (longTimer != null) {
                longTimer.cancel();
                longTimer = null;
                shortTimerPause = false;
                Log.e("longTimer", "Cancel");
                //startFDSensor();
            }
       /*     if(future2minTimer != null){
                future2minTimer.cancel();
                future2minTimer = null;
                shortTimerPause = false;
                Log.e("longTimer","Cancel");
                //startFDSensor();
            }*/
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected - isConnected ...............: " + mGoogleApiClient.isConnected());
     /*   Intent intent = new Intent( this, ActivityRecognizedService.class );
        PendingIntent pendingIntent = PendingIntent.getService( this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT );
        ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates( mGoogleApiClient, 500, pendingIntent );*/
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        Long currentTimeLogs = System.currentTimeMillis();
        String time = formateLongToOnlyDateForServer(currentTimeLogs);
        LoggingOperations.writeToFile(this, time + " ON CONNECTED WITH location");
        startLocationUpdates();
    }

    protected void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        Log.e(TAG, "Location update started ..............: ");
        Long currentTimeLogs = System.currentTimeMillis();
        String time = formateLongToOnlyDateForServer(currentTimeLogs);
        LoggingOperations.writeToFile(this, time + " Connection LOCATION STARTED");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Long currentTimeLogs = System.currentTimeMillis();
        String time = formateLongToOnlyDateForServer(currentTimeLogs);
        LoggingOperations.writeToFile(this, time + " Connection SUSPENDED with location");
        currentSpeed = 0;
        speedCount = 0;
        speedCount_ = 0;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

         /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution() && getApplicationContext() instanceof Activity) {
            try {
                Activity activity = (Activity) getApplicationContext();
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(activity, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            /*
             * Thrown if Google Play services canceled the original
             * PendingIntent
             */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
        Log.d(TAG, "Connection failed: " + connectionResult.toString());
        Long currentTimeLogs = System.currentTimeMillis();
        String time = formateLongToOnlyDateForServer(currentTimeLogs);
        LoggingOperations.writeToFile(this, time + " Connection failed with location");

        currentSpeed = 0;
        speedCount = 0;
        speedCount_ = 0;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e(TAG, "Firing onLocationChanged..............................................");
        mCurrentLocation = location;
        speedCount++;
        //mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        // updateUI();
        try {

            currentSpeed = 0.0f;
            if (null != mCurrentLocation) {
                String lat = String.valueOf(mCurrentLocation.getLatitude());
                String lng = String.valueOf(mCurrentLocation.getLongitude());
                //String speed = String.valueOf(mCurrentLocation.getSpeed());
                Log.e("Latitude :", lat);
                Log.e("Longitude :", lng);
                // sendBroadcastMessage(location);

                //currentSpeed = mCurrentLocation.getSpeed()*3.6f;


                double distance = CalculationByDistance(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), lat_old, lon_old);

                manualSpeed = distance / time;
                lat_old = mCurrentLocation.getLatitude();
                lon_old = mCurrentLocation.getLongitude();

                Long currentTimeLogss = System.currentTimeMillis();
                String times = formateLongToOnlyDateForServer(currentTimeLogss);
                LoggingOperations.writeToFile(this, times + "--- Lat >: " + lat + "Lng > :  " + lng + "-->MANUAL CALCULATED Speed : " + manualSpeed);
                LoggingOperations.writeToFile(this, times + " distance" + distance);

                Log.e("speed fromlast location", "" + manualSpeed);

                if (mCurrentLocation != null && mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {
                    if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE))
                        DataHandler.updatePreferences(AppConstants.ARRIVAL_LAT, mCurrentLocation.getLatitude());
                    DataHandler.updatePreferences(AppConstants.ARRIVAL_LANG, mCurrentLocation.getLongitude());
                }
                //  mCurrentLocation=null;
                //currentSpeed = 150;

                currentSpeed = MySafetyFragmentGraph.speed;
                arrayListSpeed.add(currentSpeed);
                LoggingOperations logger = new LoggingOperations();
                Thread.setDefaultUncaughtExceptionHandler(logger);
                Long currentTimeLogs = System.currentTimeMillis();
                String time = formateLongToOnlyDateForServer(currentTimeLogs);
                LoggingOperations.writeToFile(this, time + "--- Lat >: " + lat + "Lng > :  " + lng + "-->Speed : " + currentSpeed);

          /*  speedCompare.add(currentSpeed);
            for(int i=0;i<speedCompare.size();i++){
                speedCompare.set(0,currentSpeed);

                if(speedCompare.size() > 2){
                    speedCompare.remove(0);
                    float difference = speedCompare.get(0) - speedCompare.get(1);
                    Log.e("difference",""+difference);
                }


            }*/

          /*  arrayListSpeed.set(0,currentSpeed);

            if(arrayListSpeed.size() > 2){

                arrayListSpeed.remove(0);
                float difference = arrayListSpeed.get(1) - arrayListSpeed.get(0);
                Log.e("difference",""+difference);
                if(difference == 23){
                    accelerate ++;
                    Log.e("Acceleration",""+accelerate);
                }
                else if(difference!=23){
                    braking++;
                    Log.e("Braking",""+braking);
                }


            }*/


                if (speedCount == 3) {
                    int speedgreaterConfidence = 0;
                    int speedLessConfidence = 0;

                    for (int i = 0; i < arrayListSpeed.size(); i++) {
                        if (arrayListSpeed.get(i) >= 15) {
                            speedgreaterConfidence++;
                            Log.e("speedgreaterConfidence", "" + speedgreaterConfidence);
                        } else {
                            speedLessConfidence++;
                            Log.e("speedLessConfidence", "" + speedLessConfidence);
                        }
                    }

                    if (speedgreaterConfidence == 3) {
                        speedIsGreaterConfifenceFlag = true;
                        speedIsLessConfifenceFlag = false;
                    }
                    if (speedLessConfidence == 3) {
                        speedIsGreaterConfifenceFlag = false;
                        speedIsLessConfifenceFlag = true;
                    }
                    arrayListSpeed.clear();
                    speedCount = 0;
                }
                if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE) && DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE)) {
                    speedCount_++;
                    float differenceSpeed = currentSpeed - previousSpeed;
                    if (speedCount_ == 2) {


                        if (previousSpeed == 0) {

                            previousSpeed = currentSpeed;
                            speedCount_ = 0;
                            return;
                        }
                        previousSpeed = currentSpeed;
                        Log.e("difference", "" + differenceSpeed);
                        if (differenceSpeed >= 23) {
                            accelerate++;
                            Log.e("difference", "accelerate :" + accelerate);
                        } else if (differenceSpeed <= -23) {
                            braking++;
                            Log.e("difference", "braking :" + braking);
                        }
                        speedCount_ = 0;
                    }
                }


                Log.e("Current_Speed :", "" + currentSpeed);
                Log.e("CountValue :", "" + countValue);

                //currentspeed is greater then 5 km/h Start a Ride
                if (AppUtils.isInRange) {
                    if (speedIsGreaterConfifenceFlag) {
                        passengerMode = DataHandler.getIntPreferencesPassengerMode(AppConstants.KEY_PASSENGER_MODE);
                        if (broadcastFlag) {
                            Log.e("Passenger", " Mode " + passengerMode);
                            /*if(passengerMode == 1 && !passengerModeFlag){ // when bit 0 that mean passenger mode is off and 1 for On
                                passengerModeFlag = true;
                                ispassengerMode = false;

                                minutesPassengerSwitchOn = System.currentTimeMillis();
                                Log.e("minutesPassengerS",":"+minutesPassengerSwitchOn);

                            }*/
                            if (passengerMode == 0) {
                                ispassengerMode = true;
                            }

                            BackgroundBeaconScan.beaconlostCounter--;
                            AppConstants.ORPHAN_RIDE_BIT = 0;
                            Intent intent = new Intent("IamInRange");
                            sendBroadcast(intent);
                          /*  Detector.FdApp.departureLocation = null;
                            Detector.FdApp.arrivalLocation = null;*/
                            Log.e("FIRED", "INRANGE");
                            Long currentTimeRec = System.currentTimeMillis();
                            String timeRec = formateLongToOnlyDateForServer(currentTimeRec);
                            LoggingOperations.writeToFile(this, "RECIEVER " + timeRec + " I am in Range Fired");
                            broadcastFlag = false;
                            MySafetyFragmentGraph.rideneverStarted = true;
                            timeSave = new ArrayList<Object>();
                            /*passengerModeTime = new ArrayList<Object>();*/
                            if (mCurrentLocation != null && mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {
                                DataHandler.updatePreferences(AppConstants.DEPARTURE_LAT, mCurrentLocation.getLatitude());
                                DataHandler.updatePreferences(AppConstants.DEPARTURE_LANG, mCurrentLocation.getLongitude());
                                Detector.FdApp.departureLocation = mCurrentLocation;
                            } else {
                                Detector.FdApp.departureLocation = mLastLocation;
                            }
                            DataHandler.deletePreference(AppConstants.PREF_CURRENT_LATITUDE);
                        }
                        speedMonitorCheck = true;
                        AppUtils.speedCheck = true;
                        AppUtils.speedlessCheck = false;



                /*if(currentSpeed > 15){*/
//                FDUtils.isInRange = true;

                        if (DataHandler.getBooleanPreferences(AppConstants.PREF_KEY_DRIVING_PERFORMANCE_ACTIVE) == true) {


                            if (subRideBeans != null) {
                                if (currentSpeed < 15) {
                                    if (!subRideBeans.getStartlat().isEmpty() && !subRideBeans.getStartlng().isEmpty()) {
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {
                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                            subRideBeansArrayList.add(subRideBeans);
                                            subRideBeans = null;
                                            currentRegion = "";
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                            subRideBeansArrayList.add(subRideBeans);
                                            subRideBeans = null;
                                            currentRegion = "";
                                        }

                                    }
                                }

                            }
                            //1.2.2 -Y type region
                            if (currentSpeed >= 15 && currentSpeed < 50) {
                                Long currentTimeStamp = System.currentTimeMillis() / 1000L;

                                if (!currentRegion.isEmpty()) {
                                    if (subRideBeans == null) {
                                        subRideBeans = new SubRideBeans();
                                        secsInsideAregion = 0;
                                    }
                                    if (currentRegion.equalsIgnoreCase("L")) {
                                        secsInsideAregion++;
                                        if (subRideBeans.getStartlat().isEmpty() && subRideBeans.getStartlng().isEmpty()) {
                                            subRideBeans.setAreaType("L");
                                            if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                                subRideBeans.setStartlat(String.valueOf(mCurrentLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mCurrentLocation.getLongitude()));
                                            } else {
                                                subRideBeans.setStartlat(String.valueOf(mLastLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mLastLocation.getLongitude()));
                                            }

                                            subRideBeans.setCurrentSpeed(currentSpeed);
                                            subRideBeans.setCurrentTime(currentTimeStamp);

                                        }
                                    } else {
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        secsInsideAregion = 0;
                                        subRideBeans = null;
                                        currentRegion = "L";

                                    }


                                    if ((currentTimeStamp - lastTimeStamp) / 60 > differenceTime) {
                                        lastTimeStamp = currentTimeStamp;
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        subRideBeans = null;

                                    }
                                } else {
                                    currentRegion = "L";
                                }
                            }
                            //1.2.2 -Y type region
                            if (currentSpeed >= 50 && currentSpeed < 70) {
                                Long currentTimeStamp = System.currentTimeMillis() / 1000L;

                                if (!currentRegion.isEmpty()) {
                                    if (subRideBeans == null) {
                                        subRideBeans = new SubRideBeans();
                                        secsInsideAregion = 0;
                                    }
                                    if (currentRegion.equalsIgnoreCase("Y")) {
                                        secsInsideAregion++;
                                        if (subRideBeans.getStartlat().isEmpty() && subRideBeans.getStartlng().isEmpty()) {
                                            subRideBeans.setAreaType("Y");
                                            if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                                subRideBeans.setStartlat(String.valueOf(mCurrentLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mCurrentLocation.getLongitude()));
                                            } else {
                                                subRideBeans.setStartlat(String.valueOf(mLastLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mLastLocation.getLongitude()));
                                            }
                                            subRideBeans.setCurrentSpeed(currentSpeed);
                                            subRideBeans.setCurrentTime(currentTimeStamp);

                                        }
                                    } else {
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        secsInsideAregion = 0;
                                        subRideBeans = null;
                                        currentRegion = "Y";
                                    }


                                    if ((currentTimeStamp - lastTimeStamp) / 60 > differenceTime) {
                                        lastTimeStamp = currentTimeStamp;
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        subRideBeans = null;

                                    }
                                } else {
                                    currentRegion = "Y";
                                }
                            }


                            //1.2.2 -Z type region

                            else if (currentSpeed > 70 && currentSpeed < 90) {

                                //current z
                                // previous q
                                //if mismatch we update end lat/lng and put in array and object to be null
                                // if they matched , we get the start lat lng ,they are not empty, and update end lat/lng


                                // very first logic update and then previous = z and current =z


                                Long currentTimeStamp = System.currentTimeMillis() / 1000L;

                                if (!currentRegion.isEmpty()) {
                                    if (subRideBeans == null) {
                                        subRideBeans = new SubRideBeans();

                                    }
                                    if (currentRegion.equalsIgnoreCase("Z")) {
                                        secsInsideAregion++;
                                        if (subRideBeans.getStartlat().isEmpty() && subRideBeans.getStartlng().isEmpty()) {
                                            subRideBeans.setAreaType("Z");
                                            if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                                subRideBeans.setStartlat(String.valueOf(mCurrentLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mCurrentLocation.getLongitude()));
                                            } else {
                                                subRideBeans.setStartlat(String.valueOf(mLastLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mLastLocation.getLongitude()));
                                            }

                                            subRideBeans.setCurrentSpeed(currentSpeed);
                                            subRideBeans.setCurrentTime(currentTimeStamp);

                                        }
                                    } else {
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        secsInsideAregion = 0;
                                        subRideBeans = null;
                                        currentRegion = "Z";
                                    }
                                    if ((currentTimeStamp - lastTimeStamp) / 60 > differenceTime) {
                                        lastTimeStamp = currentTimeStamp;
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        subRideBeans = null;

                                    }
                                } else {
                                    currentRegion = "Z";
                                }


                            }
                            //1.2.2 W- type region

                            else if (currentSpeed > 90 && currentSpeed < 120) {


                                Long currentTimeStamp = System.currentTimeMillis() / 1000L;

                                if (!currentRegion.isEmpty()) {
                                    if (subRideBeans == null) {
                                        subRideBeans = new SubRideBeans();
                                        secsInsideAregion = 0;

                                    }
                                    if (currentRegion.equalsIgnoreCase("W")) {
                                        secsInsideAregion++;

                                        if (subRideBeans.getStartlat().isEmpty() && subRideBeans.getStartlng().isEmpty()) {
                                            subRideBeans.setAreaType("W");
                                            if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                                subRideBeans.setStartlat(String.valueOf(mCurrentLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mCurrentLocation.getLongitude()));
                                            } else {
                                                subRideBeans.setStartlat(String.valueOf(mLastLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mLastLocation.getLongitude()));
                                            }

                                            subRideBeans.setCurrentSpeed(currentSpeed);
                                            subRideBeans.setCurrentTime(currentTimeStamp);

                                        }
                                    } else {
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        secsInsideAregion = 0;
                                        subRideBeans = null;
                                        currentRegion = "W";
                                    }
                                    if ((currentTimeStamp - lastTimeStamp) / 60 > differenceTime) {
                                        lastTimeStamp = currentTimeStamp;
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        subRideBeans = null;

                                    }
                                } else {
                                    currentRegion = "W";
                                }


                            }
                            //1.2.2 -Q type region

                            else if (currentSpeed > 120) {


                                Long currentTimeStamp = System.currentTimeMillis() / 1000L;

                                if (!currentRegion.isEmpty()) {
                                    if (subRideBeans == null) {
                                        subRideBeans = new SubRideBeans();
                                        secsInsideAregion = 0;

                                    }
                                    if (currentRegion.equalsIgnoreCase("Q")) {
                                        secsInsideAregion++;

                                        if (subRideBeans.getStartlat().isEmpty() && subRideBeans.getStartlng().isEmpty()) {
                                            subRideBeans.setAreaType("Q");
                                            if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                                subRideBeans.setStartlat(String.valueOf(mCurrentLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mCurrentLocation.getLongitude()));
                                            } else {
                                                subRideBeans.setStartlat(String.valueOf(mLastLocation.getLatitude()));
                                                subRideBeans.setStartlng(String.valueOf(mLastLocation.getLongitude()));
                                            }
                                            subRideBeans.setCurrentSpeed(currentSpeed);
                                            subRideBeans.setCurrentTime(currentTimeStamp);

                                        }
                                    } else {
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        subRideBeans = null;
                                        secsInsideAregion = 0;
                                        currentRegion = "Q";
                                    }
                                    if ((currentTimeStamp - lastTimeStamp) / 60 > differenceTime) {
                                        lastTimeStamp = currentTimeStamp;
                                        if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {

                                            subRideBeans.setEndlat(String.valueOf(mCurrentLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mCurrentLocation.getLongitude()));
                                        } else {
                                            subRideBeans.setEndlat(String.valueOf(mLastLocation.getLatitude()));
                                            subRideBeans.setEndlng(String.valueOf(mLastLocation.getLongitude()));
                                        }
                                        subRideBeans.setTimeInsideAregion(secsInsideAregion);
                                        subRideBeansArrayList.add(subRideBeans);
                                        subRideBeans = null;

                                    }
                                } else {
                                    currentRegion = "Q";
                                }


                            }
                        }

                        Log.e("subRideBeansArrayList", "array :" + subRideBeansArrayList.size() + " elements" + subRideBeansArrayList);
                        Log.e("sec in a region", ">> " + secsInsideAregion);


                        if (currentSpeed > 120 && greaterthan120speedflag) {

                            ridesBeans = new RidesBeans();

                            Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                            ridesBeans.setStarttime(timestamp);

                            Log.e("currentTime", "timeStamp Greater :" + timestamp);
                            //Toast.makeText(getApplicationContext(), "timeStamp Greater :"+timestamp, Toast.LENGTH_LONG).show();
                            greaterthan120speedflag = false;
                            lessthan120speedflag = true;
                            lessthan10speedflag = true;
                        }
                        if (currentSpeed < 120 && lessthan120speedflag && lessthan10speedflag) {

                            Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                            ridesBeans.setEndtime(timestamp);
                            Log.e("currentTime", "timeStamp Less :" + timestamp);
                            //Toast.makeText(getApplicationContext(), "timeStamp Less :"+timestamp, Toast.LENGTH_LONG).show();
                            greaterthan120speedflag = true;
                            lessthan120speedflag = false;
                            timeSave.add(ridesBeans);
                       /* if(ridesBeans != null){
                            ridesBeans = null;
                            Log.e("rideBeans","Is Null");
                        }*/
                            Log.e("currentTime", "array :" + timeSave.size());

                        }

                        //if speed is increaase within 1 minute 45 seconds
                        if (longTimer != null) {
                            longTimer.cancel();
                            longTimer = null;
                            shortTimerPause = false;
                            Log.e("longTimer", "Cancel");
                            //startFDSensor();
                        }
                    /*if(future2minTimer != null){
                       future2minTimer.cancel();
                       future2minTimer = null;
                   }*/
                        if (passengerMode == 1 && !passengerModeFlag) {
                            passengerModeFlag = true;
                            ispassengerMode = false;
                            totalminutesPassengerMode = System.currentTimeMillis() / 1000L;
                            /*totalminutesPassengerMode = minutesPassengerSwitchOn;*/
                            AppConstants.switchCheck = true;
                            Log.e("totalminutes", "PassengerSwitchOn :" + totalminutesPassengerMode);
                            Long currentTimeRec = System.currentTimeMillis();
                            String timeRec = formateLongToOnlyDateForServer(currentTimeRec);
                            LoggingOperations.writeToFile(this, "RECIEVER " + timeRec + "Passenger Mode ---------->:" + passengerMode + ":<----------- ");

                        }
                        if (passengerMode == 0 && passengerModeFlag) {
                            ispassengerMode = true;
                            passengerModeFlag = false;
                            minutesPassengerSwitchOff += System.currentTimeMillis() / 1000L - totalminutesPassengerMode;

                            /*totalminutesPassengerMode = minutesPassengerSwitchOff;*/
                            AppConstants.ispassengerSwitch = true;
                            currentTime = System.currentTimeMillis() / 1000L;
                            Log.e("totalminutes", "currentTime :" + currentTime);
                            Log.e("totalminutes", "PassengerSwitchOff :" + minutesPassengerSwitchOff);
                            Long currentTimeRec = System.currentTimeMillis();
                            String timeRec = formateLongToOnlyDateForServer(currentTimeRec);
                            LoggingOperations.writeToFile(this, "RECIEVER " + timeRec + " Passenger Mode ---------->:" + passengerMode + ":<----------- ");

                        }

                    } else if (speedIsLessConfifenceFlag) {
                        speedMonitorCheck = true;
                        if (speedIsLessConfifenceFlag && AppUtils.speedCheck) {

/*
                else if (currentSpeed <=15&&AppUtils.speedCheck){
*/
                            AppUtils.speedlessCheck = true;

                            if (AppUtils.speedlessCheck) {
//                    FDUtils.speedCheck = false;
                                if (lessthan10speedflag) {
                                    if (!greaterthan120speedflag) {
                                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                                        ridesBeans.setEndtime(timestamp);
                                        Log.e("currentTime", "timeStamp Less :" + timestamp);
                                        timeSave.add(ridesBeans);
                                        Log.e("currentTime", "array :" + timeSave.size());
                                        greaterthan120speedflag = true;
                                        Log.e("currentTime", "insidelessthan10speed");
                                    }
                                    lessthan10speedflag = false;
                                }
                                shortTimer();

                            }
                        }
                    }
                }

//            futureTimer();

            /*else{
                longTimer();
            }*/
                /*if(BackgroundBeaconScan.beaconlostCounter <=1 && DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE )&& currentSpeed > 15){
                    AppConstants.ORPHAN_RIDE_BIT = 0;
                }*/

            } else {
                Long currentTimeLogs = System.currentTimeMillis();
                String time = formateLongToOnlyDateForServer(currentTimeLogs);
                LoggingOperations.writeToFile(this, "LOCATION NULL " + time + " location obj " + mCurrentLocation + "-->Speed : " + currentSpeed);

            }
        } catch (Exception e) {
            Long currentTimeLogs = System.currentTimeMillis();
            String time = formateLongToOnlyDateForServer(currentTimeLogs);
            LoggingOperations.writeToFile(this, "LOCATION exception " + time + " & e is " + e);

            e.printStackTrace();
        }

    }

    /*public void futureTimer(){
       future2minTimer = new Timer();
       TimerTask timerTask = new TimerTask() {
           @Override
            public void run() {
                if (BackgroundBeaconScan.beaconlostPermanently)

               {
                   Log.e("Bfpk","I executed future timer");

                   isTaskCompleted = true;
                   AppUtils.speedCheck = false;
                   // toastHandler.sendEmptyMessage(0);
                   rideEnd(isTaskCompleted);
                   broadcastFlag = true;
                   try {
                       if(longTimer != null){
                           longTimer.cancel();
                           longTimer = null;
                           shortTimerPause = false;
                           Log.e("longTimer","Cancel");
                           //startFDSensor();
                       }
                      // future2minTimer.cancel();

                   }catch (Exception e){

                   }
               }
          }
       };
        future2minTimer.schedule(timerTask,120000);

   }*/
    public void shortTimer() {
        LoggingOperations.writeToFile(this, "Short Timer ---------->:  Hit  :<----------- ");
        shortTimer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                //   stopFDSensor();
                //  getToasthandlerpauseRide.sendEmptyMessage(0);

                AppUtils.speedCheck = true;
                shortTimer.cancel();
                //shortTimer = null;
                if (!shortTimerPause) {
                    //if demo mode is true run this timer
                    /*if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE) == true) {
                        longTimer();
                    }*/
             /*       if (BackgroundBeaconScan.beaconlostPermanently){
                        Log.e("Bfpk","Istarted Future timer");
                        futureTimer();
                    }*/
                }

            }
        };
        shortTimer.schedule(timerTask, 5000);
    }

    public void longTimer() {
        shortTimerPause = true;

        Log.e("Inside", "LongTimer");
        if (shortTimerPause) {
            longTimer = new Timer();
            TimerTask timerTask1 = new TimerTask() {
                @Override
                public void run() {
                    if (currentSpeed <= 15) {
                        if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)) {
                            if (mCurrentLocation != null && mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {
                                Detector.FdApp.arrivalLocation = mCurrentLocation;
                            } else {
                                Detector.FdApp.arrivalLocation = mLastLocation;
                            }

                        }
                        Log.e("Inside", "LongTimer RUN");
                        isTaskCompleted = true;
                        AppUtils.speedCheck = false;
                        // toastHandler.sendEmptyMessage(0);
                        rideEnd(isTaskCompleted);
                        broadcastFlag = true;
                        longTimer.cancel();
                        speedCount = 0;
                        speedCount_ = 0;
                    }

                }
            };
            longTimer.schedule(timerTask1, 900000);
            //longTimer.schedule(timerTask1, 180000);
        }
    }

    /* private void sendBroadcastMessage(Location location) {
         if (location != null) {
             Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
             intent.putExtra(EXTRA_LATITUDE, location.getLatitude());
             intent.putExtra(EXTRA_LONGITUDE, location.getLongitude());
             intent.putExtra(CURRENT_SPEED, currentSpeed);
             LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
         }
     }*/

    /**
     * This Method sendBroadCast when Long Timer is Completed to End the Ride. While Staying in Beacon Zone
     **/
    private void rideEnd(boolean rideisFininshed) {
        if (rideisFininshed) {
            if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)) {
                Log.e("inside", "rideEnd");
                Intent intent = new Intent("RIDE_END_FLAG");
                //intent.putExtra("RIDE_END_FLAG", rideisFininshed);
                sendBroadcast(intent);
                shortTimerPause = false;
                Long currentTimeRec = System.currentTimeMillis();
                String timeRec = formateLongToOnlyDateForServer(currentTimeRec);
                LoggingOperations.writeToFile(this, "RECIEVER " + timeRec + " RIDE END FLAG FIRED");
                //This section is for take the difference of start and end time

                 /*long totalTimeOver120 = 0;

                 long totalTimeinMins = 0;
                 for(int i=0;i<timeSave.size();i++){
                     //Current Rides Bean Object
                     RidesBeans rb = (RidesBeans) timeSave.get(i);

                     //Time Differene in Current RidesBean Object
                     long timeOver = rb.getEndtime().getTime() - rb.getStarttime().getTime();

                     totalTimeOver120 += timeOver;

                 }
                 totalTimeinSeconds = totalTimeOver120 / 1000;
                 totalTimeinMins  = totalTimeOver120 /60000;
                 ridesBeans = null;
                 Log.e("currentTime","totaltimeover120 :"+totalTimeOver120);
                 Log.e("currentTime","totalTimeinSeconds :"+totalTimeinSeconds);
                 Log.e("currentTime","totalTimeinSeconds :"+totalTimeinMins);*/

            }

        }
    }
    //    private void updateUI() {
//        Log.d(TAG, "UI update initiated .............");
//        if (null != mCurrentLocation) {
//            String lat = String.valueOf(mCurrentLocation.getLatitude());
//            String lng = String.valueOf(mCurrentLocation.getLongitude());
//            String speed = String.valueOf(mCurrentLocation.getSpeed());
//            tvLocation.setText("At Time: " + mLastUpdateTime + "\n" +
//                    "Latitude: " + lat + "\n" +
//                    "Longitude: " + lng + "\n" +
//                    "Speed: " + speed + "\n" +
//                    "Accuracy: " + mCurrentLocation.getAccuracy() + "\n" +
//                    "Provider: " + mCurrentLocation.getProvider());
//        } else {
//            Log.d(TAG, "location is null ...............");
//        }
//    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
        Log.d(TAG, "Location update stopped .......................");
        speedCount = 0;
        speedCount_ = 0;
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    public double CalculationByDistance(double lat1, double lon1, double lat2, double lon2) {
        double Radius = EARTH_RADIUS;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return Radius * c;
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int code = api.isGooglePlayServicesAvailable(this);
        /*int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);*/
        if (ConnectionResult.SUCCESS == code) {
            LoggingOperations.writeToFile(this, "PLAY SERVICES+ " + "TRUE");
            return true;
        } else {
            LoggingOperations.writeToFile(this, "PLAY SERVICES+ " + "FALSE");
            //GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }
}
