package com.studio.barefoot.freeedrivebeacononlyapp.appcontroller;

import android.Manifest;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.support.multidex.MultiDex;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.sensors.FDsensors;
import com.studio.barefoot.freeedrivebeacononlyapp.services.Detector;
import com.studio.barefoot.freeedrivebeacononlyapp.services.FDGPSTracker;
import com.studio.barefoot.freeedrivebeacononlyapp.services.ForegroundLocationService;
import com.studio.barefoot.freeedrivebeacononlyapp.services.LocationService;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LoggingOperations;

import java.util.Date;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants.RIDE_WAS_ACTIVE;
import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.formateLongToOnlyDateForServer;

/**
 * Created by mcs on 12/21/2016.
 */

public class ApplicationController extends Application
{

    private  static Context mAppcontext;
    /**
     * Represents the Sensor
     */
    public FDsensors fdSensors;

    /**
     * Timestamp for the departure time
     */
    public static long departure_time;
    /**
     * Tiestamp for the arrival time
     */
    public static long arrival_time;

    /**
     * Represents the departure's location
     */
    public Location departureLocation;

    /**
     * Represents the arrival's location
     */
    public Location arrivalLocation;
    /**
     * Allows to get the current location
     */

    BluetoothAdapter mBluetoothAdaptater;
    public FDGPSTracker fdgpsTracker;
    private Locale locale = null;
    String language = "";
    Configuration config;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Fabric.with(this, new Answers());
   /*     if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);*/

        mAppcontext = getApplicationContext();
        //setDevicePolicy();

        new DataHandler(mAppcontext);
        DataHandler.updatePreferences(RIDE_WAS_ACTIVE, false);
        AppConstants.broadcastFlag =true;
        AppConstants.broadcastFlag =true;
        config = getBaseContext().getResources().getConfiguration();
        try{
            language = DataHandler.getStringPreferences(AppConstants.PREF_KEY_LANG);
            Log.e("language","appcontroller :"+language);
            if(language !=null && !language.isEmpty()){
                //default to the phone's language if EN, FR or NL
                String lang = language;
                Log.e("lang","if :"+lang);
                locale = new Locale(lang);
                locale.setDefault(locale);
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

            }
            else{
                //default to the phone's language if EN, FR or NL
               // String currentLang = "en";

               String currentLang = config.locale.getLanguage();
                if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.fr))){
                    currentLang=getBaseContext().getResources().getString(R.string.fr);
                }else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.nl))){
                    currentLang=getBaseContext().getResources().getString(R.string.nl);
                }else if(currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.es))){
                    currentLang=getBaseContext().getResources().getString(R.string.es);
                }else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.en))){
                    currentLang=getBaseContext().getResources().getString(R.string.en);
                }else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.da))){
                    currentLang=getBaseContext().getResources().getString(R.string.da);
                }else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.it))){
                    currentLang=getBaseContext().getResources().getString(R.string.it);
                }else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.no))){
                    currentLang=getBaseContext().getResources().getString(R.string.no);
                }else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.de))){
                    currentLang=getBaseContext().getResources().getString(R.string.de);
                }else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.pt))){
                    currentLang=getBaseContext().getResources().getString(R.string.pt);
                }else if (currentLang.equalsIgnoreCase(getBaseContext().getResources().getString(R.string.fi))){
                    currentLang=getBaseContext().getResources().getString(R.string.fi);
                }else {
                    currentLang=getBaseContext().getResources().getString(R.string.en);
                }
                LocaleUtils.setLocale(new Locale(currentLang));
                LocaleUtils.updateConfig(this, getBaseContext().getResources().getConfiguration());
            }
        }catch (Exception e){
            e.printStackTrace();

            //default to the phone's language if EN, FR or NL
            String currentLang = config.locale.getLanguage();
          //  String currentLang = "en";
            LocaleUtils.setLocale(new Locale(currentLang));
            LocaleUtils.updateConfig(this, getBaseContext().getResources().getConfiguration());
        }


        DataHandler.updatePreferences(AppConstants.FIRST_TIME_USER,1);
        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/DINNextLTPro-LightCondensed.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
        startService(new Intent(this, Detector.class));
        fdSensors = new FDsensors(getmAppcontext());
        fdgpsTracker = new FDGPSTracker(getmAppcontext());

        //set unlock to TRUE by default :
        AppUtils.isUnlock = false;
    /*    if(DataHandler.getLongreferences(AppUtils.LAST_UPDATE)!=null){
            if (DataHandler.getLongreferences(AppUtils.LAST_UPDATE)==0){
                Date lastUpdate = new Date();
                DataHandler.updatePreferences(AppUtils.LAST_UPDATE,lastUpdate.getTime());
            }
        }*/

    }
    public void updateLocale(String lang)
    {
        LocaleUtils.setLocale(new Locale(lang));
        //    LocaleUtils.getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        LocaleUtils.updateConfig(this, getBaseContext().getResources().getConfiguration());


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleUtils.updateConfig(this, newConfig);
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    @Override
    public void onTerminate() {
        super.onTerminate();
        startService(new Intent(this, Detector.class));
        Long currentTimeLogs =  System.currentTimeMillis();
        String time=  formateLongToOnlyDateForServer(currentTimeLogs);

        int permissionexternalStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionexternalStorage == PackageManager.PERMISSION_GRANTED) {
            LoggingOperations.writeToFile(this, "Application KILLED" + time);
        }
        Log.e("Application Controller","Killed");
        AppConstants.broadcastFlag =true;
        AppConstants.broadcastFlag =true;
        /*AppUtils.appTerminated = true;*/
        DataHandler.updatePreferences(AppConstants.KEY_PASSENGER_MODE, 0);
    }
    public static Context getmAppcontext() {
        return mAppcontext;
    }

    /*public  static void setmAppcontext(Context appcontext){
        mAppcontext = appcontext;
    }*/

 /*   private void setDevicePolicy() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD) {
            final StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    try {
                        StrictMode.setThreadPolicy(policy);
                    } catch (Throwable ex) {}
                }
            });
        }
    }*/


}
