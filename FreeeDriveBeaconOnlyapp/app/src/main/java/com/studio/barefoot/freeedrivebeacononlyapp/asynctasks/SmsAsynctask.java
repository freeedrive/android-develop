package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.MenuActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.QrCodeActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.SmsConfirmationActivity.progressBarDialogSMS;

/**
 * Created by mcs on 1/5/2017.
 */

public class SmsAsynctask extends BaseAsyncTask {
    public static ProgressBarDialog progressBarDialogSyncData;

    /**
     * AsyncTask method basic calls during a request, calls the parent's method.
     */
    protected String doInBackground(String... params) {

        return "";
    }

    public SmsAsynctask(Context context, String route, List<NameValuePair> pp) {
        super(context, route, pp);
    }

    /**
     * AsyncTask method basic calls after a request.
     */
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try {
            progressBarDialogSMS.dismiss();
            progressBarDialogSMS = null;
        } catch (NullPointerException exception) {
            exception.printStackTrace();
        }

        Log.e("s", s);
        if (s != null) {
            int intResponse = Integer.parseInt(response);
            Log.e("ResponseCode", "" + intResponse);
            Log.e("response", "" + response);

            switch (intResponse) {
                case 200:
                    Log.e("Response", "" + intResponse);
                    //token //uuid exist
                    try {
         /*               JSONObject objAccount = new JSONObject(resultat);
                        String token = objAccount.getString("message");*/
                        if (!resultat.isEmpty()) {
                            String token = resultat;
                            token = token.replaceAll("\"", "");
                            DataHandler.updatePreferences(AppConstants.TOKEN_NUMBER, token);
                            Log.e("token", token);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    DataHandler.updatePreferences(AppConstants.USER_AWAITING_SMS, false);
                    String verifiedPhoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_TEMP);
                    DataHandler.updatePreferences(AppConstants.PHONE_NUMBER, verifiedPhoneNumber);
                    DataHandler.updatePreferences(AppConstants.PREF_KEY_LOGIN_ACTIVITY, true);
                    FetchUserDetails();

                    //context.startActivity(new Intent(context, MenuActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    break;
                case 422:
                    //duplicate entiry
                    break;
                case 404:
                    try {
                        AlertDialog.Builder error_404 = new AlertDialog.Builder(context);
                        error_404.setMessage(context.getResources().getString(R.string.error_sms_404)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_404.show();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }

                    //not found
                    break;
                case 500:
                    try {
                        AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                        error_500.setMessage(context.getResources().getString(R.string.error_sms_500)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_500.show();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }

                    //internal error
                    break;

                case 402:
// Qrcode recovery
                    String verifiedPhoneNumberSMS = DataHandler.getStringPreferences(AppConstants.PHONE_TEMP);
                    DataHandler.updatePreferences(AppConstants.PHONE_NUMBER, verifiedPhoneNumberSMS);
                    Intent qrocdeRecoveryIntent = new Intent(context, QrCodeActivity.class);
                    qrocdeRecoveryIntent.putExtra(AppConstants.QR_CODE_RECOVERY, AppConstants.QR_CODE_RECOVERY_key);
                    context.startActivity(qrocdeRecoveryIntent);

                    break;

                case 403:
                    //authentication for qrcode

                    DataHandler.updatePreferences(AppConstants.USER_AWAITING_SMS, false);
                    context.startActivity(new Intent(context, QrCodeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    DataHandler.updatePreferences((AppConstants.USER_AWAITING_QR_CODE), true);
                    break;
                case 409:
                    //uuid doesno... take to qrcode
                    try {
                        AlertDialog.Builder error_409 = new AlertDialog.Builder(context);
                        error_409.setMessage(context.getResources().getString(R.string.error_sms_409)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_409.show();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }

                    //token
                    //sms code is not valid
                    break;
                case 405:
                    //sms code has expired
                    try {
                        AlertDialog.Builder error_405 = new AlertDialog.Builder(context);
                        error_405.setMessage(context.getResources().getString(R.string.error_sms_405)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_405.show();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }

                    break;
                case 411:
                    DataHandler.updatePreferences(AppConstants.DEMO_MODE, true);
                    FetchUserDetails();
                    break;
            }
        } else {
            try {
                progressBarDialogSMS.dismiss();
                progressBarDialogSMS = null;
            } catch (NullPointerException exception) {
                exception.printStackTrace();
            }
        }
    }


    private void FetchUserDetails() {
        progressBarDialogSyncData = new ProgressBarDialog(context);
        /*progressBarDialogSyncData.setTitle(context.getString(R.string.title_progress_dialog));
        progressBarDialogSyncData.setMessage(context.getString(R.string.body_progress_dialog));*/
        progressBarDialogSyncData.setTitle("Syncing");
        progressBarDialogSyncData.setMessage("wait..");
        progressBarDialogSyncData.show();
        List<NameValuePair> mParams = new ArrayList<NameValuePair>();
        final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        String deviceId = telephonyManager.getDeviceId();
        mParams.add(new BasicNameValuePair("devID",deviceId));
        mParams.add(new BasicNameValuePair("phone_number",DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));

        FetchProfileAsyncTaskForLogin fetchProfileAsyncTask = new FetchProfileAsyncTaskForLogin(context, WebServiceConstants.END_POINT_FETCH_PROFILE,mParams);
        fetchProfileAsyncTask.execute();
    }

}
