package com.studio.barefoot.freeedrivebeacononlyapp.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.MenuActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.DividerItemDecoration;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.ExpandableListAdapter;

import com.studio.barefoot.freeedrivebeacononlyapp.adapters.MessagesAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.DeleteMessagesAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.GetchNotificationAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.NotificationBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.DemoModeEndDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.studio.barefoot.freeedrivebeacononlyapp.adapters.MessagesAdapter.isToggled;
import static com.studio.barefoot.freeedrivebeacononlyapp.adapters.MessagesAdapter.ridesBeansList;
import static com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.GetchNotificationAsyncTask.testMEssagesAdapter;
import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MySafetyFragmentGraph.DemoModeDialog;


public class MessagesActivity extends AppCompatActivity implements MenuActivity.OnBackPressedListener{
    public static ProgressBarDialog progressBarDialogNotifications;
    List<ExpandableListAdapter.Item> data;

    public static TextView tv_noNotifications;
    public static TextView /*deleteMessages*/deleteAllMessages;
    public static ImageView deleteMessages;
    public static CheckBox selectAll;
    //public static TextView delete;
    LinearLayoutManager linearLayoutManager;
    public static Boolean   areAllSelected = false;
    public static RecyclerView listViewNMessages;
    MenuActivity.OnBackPressedListener onBackPressedListener;
    public static ArrayList<Integer> stringArrayListOfPositionsDeleted = new ArrayList<>();
    public static  ArrayList<NotificationBeans> listNotifications = new ArrayList<>();
    DemoModeEndDialog endDialog;
    FragmentManager fm;

    public MessagesActivity() {
        // Required empty public constructor
        LocaleUtils.updateConfig(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_messages);

        // For setting up the different logo of toll bar for demo and paid mode
        RelativeLayout toolbar = (RelativeLayout)findViewById(R.id.topToolbar);
        ImageView fd_logo = (ImageView)toolbar.findViewById(R.id.tol_bar_logo);
        if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)){
            fd_logo.setImageResource(R.drawable.logo_freeedrive_demo_mode);
        }  else{
            fd_logo.setImageResource(R.drawable.logo_topbar);
        }

        fm = getSupportFragmentManager();
        endDialog = DemoModeEndDialog.newInstance("");
        // ((MenuActivity) getActivity()).setOnBackPressedListener(this);
         onBackPressedListener = new MessagesActivity();
        isToggled = false;
        stringArrayListOfPositionsDeleted.clear();

        setupActionBar();
        tv_noNotifications = (TextView)findViewById(R.id.tv_noNotifications);

        deleteMessages = (ImageView)findViewById(R.id.text_delete);
        deleteAllMessages = (TextView)findViewById(R.id.text_delete_all);
        selectAll = (CheckBox) findViewById(R.id.select_All);
        // delete = (TextView) findViewById(R.id.deleteButton);

        try{
            listViewNMessages = (RecyclerView)findViewById(R.id.listViewNotifications);
            linearLayoutManager = new LinearLayoutManager(MessagesActivity.this,LinearLayoutManager.VERTICAL,false);
            listViewNMessages.setLayoutManager(linearLayoutManager);
            data = new ArrayList<>();
        }catch (OutOfMemoryError memoryError){
            memoryError.printStackTrace();
        }


        setUpItemTouchHelper();
        setUpAnimationDecoratorHelper();


        selectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    areAllSelected = true;
                    ArrayList<NotificationBeans> listNotificationss = new ArrayList<>();
                    listNotificationss = listNotifications     ;
                    testMEssagesAdapter = new MessagesAdapter(listNotifications);
                    listViewNMessages.setAdapter(testMEssagesAdapter);
                    testMEssagesAdapter.notifyDataSetChanged();

                    if (MessagesActivity.areAllSelected){
                        stringArrayListOfPositionsDeleted.clear();
                        for (int i =0; i<ridesBeansList.size();i++){

                            stringArrayListOfPositionsDeleted.add(ridesBeansList.get(i).getMessagesID());

                        }
                    }
                    Log.e("ListOfIdstoBeDeleted",stringArrayListOfPositionsDeleted.toString());
                }else {
                    areAllSelected = false;
                    ArrayList<NotificationBeans> listNotificationss = new ArrayList<>();
                    listNotificationss = listNotifications     ;
                    testMEssagesAdapter = new MessagesAdapter(listNotifications);
                    listViewNMessages.setAdapter(testMEssagesAdapter);
                    testMEssagesAdapter.notifyDataSetChanged();
                    stringArrayListOfPositionsDeleted.clear();
                    Log.e("ListOfIdsNotToBeDeleted",stringArrayListOfPositionsDeleted.toString());

                }
            }
        });
        try{
            if (AppUtils.isNetworkAvailable()) {

                progressBarDialogNotifications =new ProgressBarDialog(MessagesActivity.this);
                progressBarDialogNotifications.setTitle(getString(R.string.title_progress_dialog));
                progressBarDialogNotifications.setMessage(getString(R.string.body_progress_dialog));
                progressBarDialogNotifications.show();

                RideBDD tmp = new RideBDD(MessagesActivity.this);

                tmp.open();
                 int count = tmp.sumMessageCount();
                if (count>0){
                    JSONArray listoFids = new JSONArray(tmp.getAlltheIDsOftheDeletedMessages());
                    if (listoFids.length()>0){
                        DeleteMessagesAsyncTask deleteMessagesAsyncTask = new DeleteMessagesAsyncTask(listoFids, MessagesActivity.this);
                        deleteMessagesAsyncTask.execute();
                    }
                }

                tmp.close();

                String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER);
                List<NameValuePair> mParams = new ArrayList<NameValuePair>();
                mParams.add(new BasicNameValuePair("phone_number",phoneNumber));
                Log.e("PARAMS", "" + mParams);
                GetchNotificationAsyncTask getchNotificationAsyncTask = new GetchNotificationAsyncTask(MessagesActivity.this, WebServiceConstants.END_POINT_NOTIFICATIONS,mParams );
                getchNotificationAsyncTask.execute();
/*
                tmp.open();
                listNotifications = tmp.getAllMessagesFromDB();
                listViewNMessages.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                testMEssagesAdapter = new MessagesAdapter(listNotifications);
                listViewNMessages.setAdapter(testMEssagesAdapter);
                testMEssagesAdapter.notifyDataSetChanged();
                tmp.close();*/
            }else {

                RideBDD tmp = new RideBDD(getApplicationContext());
                tmp.open();
                listNotifications = new ArrayList<>();
                listNotifications = tmp.getAllMessagesFromDB();
                if (listNotifications.size()==0){
                    tv_noNotifications.setText(MessagesActivity.this.getResources().getString(R.string.emptyMessages));
                    tv_noNotifications.setVisibility(View.VISIBLE);
                    deleteMessages.setVisibility(View.INVISIBLE);
                }
                else{
                    listViewNMessages.addItemDecoration(new DividerItemDecoration(MessagesActivity.this, LinearLayoutManager.VERTICAL));
                    testMEssagesAdapter = new MessagesAdapter(listNotifications);
                    listViewNMessages.setAdapter(testMEssagesAdapter);
                    testMEssagesAdapter.notifyDataSetChanged();
                    DataHandler.updatePreferences(AppConstants.MESSAGES_COUNT,0);

                }

                tmp.close();

         /*       AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(getActivity());
                error_No_Internet.setMessage(getActivity().getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //NotificationActivity.super.onBackPressed();
                    }
                });
                error_No_Internet.show();*/
            }
        }catch (Exception exception){
            exception.printStackTrace();
        }
/*        deleteAllMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtils.isNetworkAvailable()) {
                    AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(getActivity());
                    error_No_Internet.setMessage("Are you sure you want to delete messages.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            progressBarDialogNotifications = new ProgressBarDialog(getActivity());
                            progressBarDialogNotifications.setTitle(getString(R.string.title_progress_dialog));
                            progressBarDialogNotifications.setMessage(getString(R.string.updating_progress_dialog));
                            progressBarDialogNotifications.show();
                            JSONArray listoFids = new JSONArray(stringArrayListOfPositionsDeleted);
                            DeleteMessagesAsyncTask deleteMessagesAsyncTask = new DeleteMessagesAsyncTask(DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER), getActivity());
                            deleteMessagesAsyncTask.execute();
                            deleteAllMessages.setVisibility(View.INVISIBLE);
                            //NotificationActivity.super.onBackPressed();
                        }
                    });
                    error_No_Internet.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        } });
                    error_No_Internet.show();


                }else {
                    AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(getActivity());
                    error_No_Internet.setMessage(getActivity().getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            //NotificationActivity.super.onBackPressed();
                        }
                    });
                    error_No_Internet.show();
                }
            }
        });*/

        deleteMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteMessages();
            }
        });

        if(DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE_SUBSCRIPTION_EXPIRED) == true){
            DemoModeDialog();
        }
    }

    public void DemoModeDialog() {

        try {
            if (endDialog != null && endDialog.isAdded()) {
                return;
            } else {
               /* Log.e("BF-PK-FD", "DEMO_MODE_END_POPUP SHOW");*/
                endDialog.show(fm, "");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void deleteMessages(){
        if (AppUtils.isNetworkAvailable()) {

            if (stringArrayListOfPositionsDeleted.size() > 0) {
                AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(MessagesActivity.this);
                error_No_Internet.setMessage(MessagesActivity.this.getResources().getString(R.string.delete_messages)).setPositiveButton(MessagesActivity.this.getResources().getString(R.string.string_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        progressBarDialogNotifications = new ProgressBarDialog(MessagesActivity.this);
                        progressBarDialogNotifications.setTitle(MessagesActivity.this.getResources().getString(R.string.title_progress_dialog));
                        progressBarDialogNotifications.setMessage(MessagesActivity.this.getResources().getString(R.string.updating_progress_dialog));
                        progressBarDialogNotifications.show();
                        JSONArray listoFids = new JSONArray(stringArrayListOfPositionsDeleted);
                        DeleteMessagesAsyncTask deleteMessagesAsyncTask = new DeleteMessagesAsyncTask(listoFids, MessagesActivity.this);
                        deleteMessagesAsyncTask.execute();
                        //NotificationActivity.super.onBackPressed();
                    }
                });
                error_No_Internet.setNegativeButton(MessagesActivity.this.getResources().getString(R.string.act_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        MessagesAdapter adapter = (MessagesAdapter)listViewNMessages.getAdapter();
                        adapter.notifyDataSetChanged();
                    } });
                error_No_Internet.show();



            }
        }else {

            AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(MessagesActivity.this);
            error_No_Internet.setMessage(MessagesActivity.this.getResources().getString(R.string.delete_messages)).setPositiveButton(MessagesActivity.this.getResources().getString(R.string.string_ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                    RideBDD tmp1 = new RideBDD(MessagesActivity.this);
                    tmp1.open();
                    for(int i=0 ;i < stringArrayListOfPositionsDeleted.size();i++){
                        tmp1.updateMessageIsDelete(stringArrayListOfPositionsDeleted.get(i));
                    }
                    stringArrayListOfPositionsDeleted.clear();


                    listNotifications = new ArrayList<>();
                    listNotifications = tmp1.getAllMessagesFromDB();
                    if (listNotifications.size()==0){
                        tv_noNotifications.setText(MessagesActivity.this.getResources().getString(R.string.emptyMessages));
                        tv_noNotifications.setVisibility(View.VISIBLE);
                        deleteMessages.setVisibility(View.INVISIBLE);
                        selectAll.setVisibility(View.INVISIBLE);
                        testMEssagesAdapter = new MessagesAdapter(listNotifications);
                        listViewNMessages.setAdapter(testMEssagesAdapter);
                        testMEssagesAdapter.notifyDataSetChanged();
                        selectAll.setChecked(false);
                    }else{
                        selectAll.setChecked(false);
                        MessagesActivity.areAllSelected = false;
                        listViewNMessages.addItemDecoration(new DividerItemDecoration(MessagesActivity.this, LinearLayoutManager.VERTICAL));
                        testMEssagesAdapter = new MessagesAdapter(listNotifications);
                        listViewNMessages.setAdapter(testMEssagesAdapter);
                        testMEssagesAdapter.notifyDataSetChanged();
                    }

                    tmp1.close();

                    //NotificationActivity.super.onBackPressed();
                }
            });
            error_No_Internet.setNegativeButton(MessagesActivity.this.getResources().getString(R.string.act_cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    MessagesAdapter adapter = (MessagesAdapter)listViewNMessages.getAdapter();
                    adapter.notifyDataSetChanged();
                } });
            error_No_Internet.show();


       /*     AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(getActivity());
            error_No_Internet.setMessage(getActivity().getResources().getString(R.string.error_No_Internet)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //NotificationActivity.super.onBackPressed();
                }
            });
            error_No_Internet.show();
        }*/
        }
    }


    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    private void setUpItemTouchHelper() {

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            // we want to cache these and not allocate anything repeatedly in the onChildDraw method
            Drawable background;
            Drawable xMark;
            int xMarkMargin;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(MessagesActivity.this.getResources().getColor(R.color.colorTextV3Light));
                xMark = ContextCompat.getDrawable(MessagesActivity.this, R.drawable.delete_message);
                xMark.setColorFilter(MessagesActivity.this.getResources().getColor(R.color.colorBG), PorterDuff.Mode.SRC_ATOP);
                xMarkMargin = (int) MessagesActivity.this.getResources().getDimension(R.dimen.ic_clear_margin);
                initiated = true;
            }

            // not important, we don't want drag & drop
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                MessagesAdapter testAdapter = (MessagesAdapter)recyclerView.getAdapter();
              /*  if (testAdapter.isUndoOn() && testAdapter.isPendingRemoval(position)) {
                    return 0;
                }*/
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int swipedPosition = viewHolder.getAdapterPosition();
                MessagesAdapter adapter = (MessagesAdapter)listViewNMessages.getAdapter();
                boolean undoOn = adapter.isUndoOn();
                adapter.remove(swipedPosition);
                deleteMessages();
            /*    if (undoOn) {
                    adapter.pendingRemoval(swipedPosition);
                } else {
                    adapter.remove(swipedPosition);
                }*/
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                View itemView = viewHolder.itemView;

                // not sure why, but this method get's called for viewholder that are already swiped away
                if (viewHolder.getAdapterPosition() == -1) {
                    // not interested in those
                    return;
                }

                if (!initiated) {
                    init();
                }

                // draw red background
                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);

                // draw x mark
                int itemHeight = itemView.getBottom() - itemView.getTop();
                int intrinsicWidth = xMark.getIntrinsicWidth();
                int intrinsicHeight = xMark.getIntrinsicWidth();

                int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
                int xMarkRight = itemView.getRight() - xMarkMargin;
                int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight)/2;
                int xMarkBottom = xMarkTop + intrinsicHeight;
                xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);

                xMark.draw(c);

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

        };
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        mItemTouchHelper.attachToRecyclerView(listViewNMessages);
    }

    /**
     * We're gonna setup another ItemDecorator that will draw the red background in the empty space while the items are animating to thier new positions
     * after an item is removed.
     */
    private void setUpAnimationDecoratorHelper() {
        listViewNMessages.addItemDecoration(new RecyclerView.ItemDecoration() {

            // we want to cache this and not allocate anything repeatedly in the onDraw method
            Drawable background;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(MessagesActivity.this.getResources().getColor(R.color.colorTextV3));
                initiated = true;
            }

            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {

                if (!initiated) {
                    init();
                }

                // only if animation is in progress
                if (parent.getItemAnimator().isRunning()) {

                    // some items might be animating down and some items might be animating up to close the gap left by the removed item
                    // this is not exclusive, both movement can be happening at the same time
                    // to reproduce this leave just enough items so the first one and the last one would be just a little off screen
                    // then remove one from the middle

                    // find first child with translationY > 0
                    // and last one with translationY < 0
                    // we're after a rect that is not covered in recycler-view views at this point in time
                    View lastViewComingDown = null;
                    View firstViewComingUp = null;

                    // this is fixed
                    int left = 0;
                    int right = parent.getWidth();

                    // this we need to find out
                    int top = 0;
                    int bottom = 0;

                    // find relevant translating views
                    int childCount = parent.getLayoutManager().getChildCount();
                    for (int i = 0; i < childCount; i++) {
                        View child = parent.getLayoutManager().getChildAt(i);
                        if (child.getTranslationY() < 0) {
                            // view is coming down
                            lastViewComingDown = child;
                        } else if (child.getTranslationY() > 0) {
                            // view is coming up
                            if (firstViewComingUp == null) {
                                firstViewComingUp = child;
                            }
                        }
                    }

                    if (lastViewComingDown != null && firstViewComingUp != null) {
                        // views are coming down AND going up to fill the void
                        top = lastViewComingDown.getBottom() + (int) lastViewComingDown.getTranslationY();
                        bottom = firstViewComingUp.getTop() + (int) firstViewComingUp.getTranslationY();
                    } else if (lastViewComingDown != null) {
                        // views are going down to fill the void
                        top = lastViewComingDown.getBottom() + (int) lastViewComingDown.getTranslationY();
                        bottom = lastViewComingDown.getBottom();
                    } else if (firstViewComingUp != null) {
                        // views are coming up to fill the void
                        top = firstViewComingUp.getTop();
                        bottom = firstViewComingUp.getTop() + (int) firstViewComingUp.getTranslationY();
                    }

                    background.setBounds(left, top, right, bottom);
                    background.draw(c);

                }
                super.onDraw(c, parent, state);
            }

        });
    }
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }

    @Override
    public void doBack() {
        isToggled = false;
        ArrayList<NotificationBeans> listNotificationss = new ArrayList<>();
         listNotificationss = listNotifications     ;
        testMEssagesAdapter = new MessagesAdapter(listNotifications);
        listViewNMessages.setAdapter(testMEssagesAdapter);
        testMEssagesAdapter.notifyDataSetChanged();

        MenuActivity.mDrawerLayout.closeDrawers();
    /*    String phoneNumber = DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER);
        List<NameValuePair> mParams = new ArrayList<NameValuePair>();
        mParams.add(new BasicNameValuePair("phone_number",phoneNumber));
        Log.e("PARAMS", "" + mParams);
        GetchNotificationAsyncTask getchNotificationAsyncTask = new GetchNotificationAsyncTask(getActivity(), WebServiceConstants.END_POINT_NOTIFICATIONS,mParams );
        getchNotificationAsyncTask.execute();*/
    try{
        deleteAllMessages.setVisibility(View.INVISIBLE);
        deleteMessages.setVisibility(View.INVISIBLE);
        selectAll.setVisibility(View.INVISIBLE);
        selectAll.setChecked(false);
        stringArrayListOfPositionsDeleted.clear();
    }catch (OutOfMemoryError memoryError){
        memoryError.printStackTrace();
    }
    }
}
