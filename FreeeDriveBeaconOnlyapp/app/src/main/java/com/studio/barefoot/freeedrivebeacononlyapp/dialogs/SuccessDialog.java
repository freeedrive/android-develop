package com.studio.barefoot.freeedrivebeacononlyapp.dialogs;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.MenuActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.RegisterActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

/**
 * Created by mcs on 12/28/2016.
 */

public class SuccessDialog extends BaseAlertDialog {

      Button next;
    ContentResolver resolver;
    KeyEvent keyEvent;

    public SuccessDialog(final Context context) {
        super(context);

        LayoutInflater factory = LayoutInflater.from(context);
        final View progressBarView = factory.inflate(R.layout.custom_success_dialog, null);

        setView(progressBarView);
          next = (Button) progressBarView.findViewById(R.id.img_accept_privacy);
        this.context = context;
        resolver = context.getContentResolver();
        keyEvent = new KeyEvent(4,4);
        setCancelable(false);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                Intent menuAcitivtyIntent = new Intent(context, MenuActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(menuAcitivtyIntent);
            }
        });

        onKeyDown(4,keyEvent);
    }



    @Override
    public void dismiss() {
            super.dismiss();
    }



}
