package com.studio.barefoot.freeedrivebeacononlyapp.utils;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.services.Detector;

import java.io.BufferedWriter;

import java.io.File;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Yasir Barefoot on 9/6/2017.
 */

public class LoggingOperations implements Thread.UncaughtExceptionHandler {
    private Thread.UncaughtExceptionHandler exceptionHandler;
    public static String TAG = "LoggingOperations";

    public LoggingOperations() {
        this.exceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
    }

    public void uncaughtException(Thread thread, Throwable error) {
        StackTraceElement[] array = error.getStackTrace();
        String report = error.toString() + "\n\n";
        report += "--------- Stack Trace\n\n";
        for (int i = 0; i < array.length; i++) {
            report += array[i].toString() + "\n";
        }
        report += "--------- Cause\n\n";
        Throwable cause = error.getCause();
        if (cause != null) {
            report += cause.toString() + "\n\n";
            array = cause.getStackTrace();
            for (int i = 0; i < array.length; i++) {
                report += array[i].toString() + "\n";
            }
        }

      //  writeToFile(report);
        exceptionHandler.uncaughtException(thread, error);
    }
    public static void writeToFile(String message) {
        try {
                if (message != null) {
//				Log.i("WSActivision", message);

                    File trace = getLogFile();
                    if (trace != null) {
                        BufferedWriter bufferedOutputStream = new BufferedWriter(new FileWriter(trace, true));

                   /* SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                    Date date = new Date();
                    String reportTime = dateFormat.format(date);*/

                        message = "---" + message + "\n";

                        bufferedOutputStream.append(message);
                        bufferedOutputStream.flush();
                        bufferedOutputStream.close();
                    }
                }
            }
         catch(Exception ex){
            //writeToFile("Class:LoggingOp, Method:writeToFile, Msg:exception while writing msg to log, ex:" + ex);
            ex.printStackTrace();
        }
    }
    public static void writeToFile(Context context,String message) {
        try {
            int permissionexternalStorage = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionexternalStorage == PackageManager.PERMISSION_GRANTED) {
                if (message != null) {
//				Log.i("WSActivision", message);

                    File trace = getLogFile();
                    if (trace != null) {

                        ///chang
                        BufferedWriter bufferedOutputStream = new BufferedWriter(new FileWriter(trace, true));

                   /* SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                    Date date = new Date();
                    String reportTime = dateFormat.format(date);*/

                        message = "---" + message + "\n";

                        bufferedOutputStream.append(message);
                        bufferedOutputStream.flush();
                        bufferedOutputStream.close();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch(Exception ex){
            writeToFile(context,"Class:LoggingOp, Method:writeToFile, Msg:exception while writing msg to log, ex:" + ex);
            ex.printStackTrace();
        }
    }

    public static void writeToFile(Exception exception) {
        exception.printStackTrace();
        try {
            if (exception != null) {

                File trace = getLogFile();
                if (trace != null) {
                    PrintWriter printWriter = new PrintWriter(new FileWriter(trace, true));
                    exception.printStackTrace(printWriter);
                    printWriter.flush();
                    printWriter.close();
                }
            }
        } catch (Exception ex) {
          //  writeToFile("Class:LoggingOp, Method:writeToFile, Msg:exception while writing Exception to log, ex:"+ex);
            ex.printStackTrace();
        }
    }


    public static File getLogFile() {
        //Logs
        try {
            File trace = null;
            if (android.os.Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                String root = Environment.getExternalStorageDirectory().toString();
                File directory = new File(root, "FreeedriveLogs");
                if (!directory.exists()) directory.mkdirs();

                trace = new File(directory,"FD_logs.txt");
                if (!trace.exists()) trace.createNewFile();
            }
            return trace;
        } catch (Exception ex) {
        //    writeToFile("Class:LoggingOp, Method:writeToFile, Msg:exception while getting log file, ex:"+ex);
            return null;
        }
    }
    public static File deleteFile(File uri){
      try {
          File fdelete = new File(uri.getPath());
          if (fdelete.exists()) {
              if (fdelete.delete()) {
                  Log.e(TAG, "deleteSUCCESS: ");
              } else {
                  Log.e(TAG, "deleteFAILED: ");
              }
          }
      }catch (Exception e){

      }
        return null;
    }
}


