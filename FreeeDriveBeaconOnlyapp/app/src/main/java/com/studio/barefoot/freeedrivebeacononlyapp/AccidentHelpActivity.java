package com.studio.barefoot.freeedrivebeacononlyapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.studio.barefoot.freeedrivebeacononlyapp.adapters.AccidentHelpAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.asynctasks.AccidentHelpDetailsAsyncTask;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.AccidentHelpBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.DemoModeEndDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.WebServiceConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class AccidentHelpActivity extends AppCompatActivity {
    public static ProgressBarDialog progressBarAccidentHelp;
    public static  ArrayList<AccidentHelpBeans> accidentHelpBeans = new ArrayList<>();
    public static RecyclerView listViewAccidentHelp;
    public static AccidentHelpAdapter accidentHelpAdapter;
    LinearLayoutManager mLayoutManager;
    DemoModeEndDialog endDialog;
    FragmentManager fm;

    public AccidentHelpActivity() {
        LocaleUtils.updateConfig(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accident_help);
        setupActionBar();
        // For setting up the different logo of toll bar for demo and paid mode
        RelativeLayout toolbar = (RelativeLayout)findViewById(R.id.topToolbar);
        ImageView fd_logo = (ImageView)toolbar.findViewById(R.id.tol_bar_logo);
        if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)){
            fd_logo.setImageResource(R.drawable.logo_freeedrive_demo_mode);
        }  else{
            fd_logo.setImageResource(R.drawable.logo_topbar);
        }

        fm = getSupportFragmentManager();
        endDialog = DemoModeEndDialog.newInstance("");
        try{
            listViewAccidentHelp = (RecyclerView) findViewById(R.id.accidentHelp_list);
            mLayoutManager  = new LinearLayoutManager(AccidentHelpActivity.this);
            listViewAccidentHelp.setLayoutManager(mLayoutManager);
            listViewAccidentHelp.setHasFixedSize(true);
            listViewAccidentHelp.setItemAnimator(new DefaultItemAnimator());
        }catch (OutOfMemoryError memoryError){
            memoryError.printStackTrace();
        }


        if(AppUtils.isNetworkAvailable()){
            FetchAccidentHelpDetails();
        }
        else {
            AlertDialog.Builder error_No_Internet = new AlertDialog.Builder(this);
            error_No_Internet.setMessage(this.getResources().getString(R.string.error_No_Internet)).setPositiveButton(this.getResources().getString(R.string.string_ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            error_No_Internet.show();
        }
        if(DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE_SUBSCRIPTION_EXPIRED) == true){
            DemoModeDialog();
        }
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }

    private void DemoModeDialog() {

        try {
            if (endDialog != null && endDialog.isAdded()) {
                return;
            } else {
               /* Log.e("BF-PK-FD", "DEMO_MODE_END_POPUP SHOW");*/
                endDialog.show(fm, "");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    private void FetchAccidentHelpDetails() {

        Log.e("TimeZone", TimeZone.getDefault().getID());
        Log.e("TimeZonegetDSTSavings",""+TimeZone.getDefault().getDSTSavings());
        TimeZone t1z = TimeZone.getDefault();
        Log.e("t1z","="+t1z.getDisplayName(false,TimeZone.LONG)+"...."+t1z.getDisplayName(false,TimeZone.SHORT));

        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        Log.e("TimeZOneCal","="+tz.getDisplayName());

        progressBarAccidentHelp = new ProgressBarDialog(AccidentHelpActivity.this);
        progressBarAccidentHelp.setTitle(getString(R.string.title_progress_dialog));
        progressBarAccidentHelp.setMessage(getString(R.string.body_progress_dialog));
        progressBarAccidentHelp.show();
        List<NameValuePair> mParams = new ArrayList<NameValuePair>();
        final TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = telephonyManager.getDeviceId();
        mParams.add(new BasicNameValuePair("devID",deviceId));
        mParams.add(new BasicNameValuePair("phone_number", DataHandler.getStringPreferences(AppConstants.PHONE_NUMBER)));
        AccidentHelpDetailsAsyncTask accidentHelpDetailsAsyncTask = new AccidentHelpDetailsAsyncTask(this, WebServiceConstants.END_POINT_FETCH_INSURANCE, mParams);
        accidentHelpDetailsAsyncTask.execute();

    }
}
