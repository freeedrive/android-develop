package com.studio.barefoot.freeedrivebeacononlyapp.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.studio.barefoot.freeedrivebeacononlyapp.FindMyVehicleActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.ScoreSynchronizationActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.appcontroller.ApplicationController;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.DemoModeEndDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.OpenBatterySettingsDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.OrderBeaconSuccessDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.PerformancePackDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.TurOnGpsDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.TurnOnBTDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.TurnOnDonotDisturbDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.TurnOnNotifcationDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.services.BackgroundBeaconScan;
import com.studio.barefoot.freeedrivebeacononlyapp.services.LocationService;
import com.studio.barefoot.freeedrivebeacononlyapp.services.Detector;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LoggingOperations;
import com.txusballesteros.widgets.FitChart;
import com.txusballesteros.widgets.FitChartValue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils.formateLongToOnlyDateForServer;


public class MySafetyFragmentGraph extends Fragment {

    View graphView;
    public static Button phoneSafety, drivingPerformance, btn_start_stop_ride;
    Fragment fragment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    RelativeLayout relativeLayoutPhoneSafety, relativeLayoutdriving, relativeLayoutdrivingPerformance, relativeLayoutPhone;

    ImageView imageviewDailyStats, imageviewFleetStats, graph_stats_left_arrow, graph_stats_right_arrow;
    TextView phonesafety, drivingperformance, daily_stats_score_last_ride_3, daily_stats_peeks, drivers_name, tv_remaining_rides;
    public static ProgressBarDialog progressBarDialogNotifications;
    LineChart chart;
    static JSONObject jsonObject_driverAvg = new JSONObject();
    static JSONObject jsonObject_companyAvg = new JSONObject();
    PerformancePackDialog performancePackDialog;
    ImageView ImgDrivingPerformance;
    FitChart driverSCoreCircle;
    FitChart peeksCircle;
    EditText enterSpeed;
    public static boolean rideneverStarted = false;
    public static int speed;
    public static int color = ApplicationController.getmAppcontext().getResources().getColor(R.color.colorBTestMenu);
    public static int colorplotlinedriver = ApplicationController.getmAppcontext().getResources().getColor(R.color.colorPrimaryNew);
    public static int colorplotlineCompany = ApplicationController.getmAppcontext().getResources().getColor(R.color.lease_plan_black);
    public static int colordays = ApplicationController.getmAppcontext().getResources().getColor(R.color.grey_black);
    private float temptotalDailytime;
    private int totalbadbehaviour;
    Calendar calander;
    SimpleDateFormat df;
    String currentDate = "";
    private OpenBatterySettingsDialog openBatterySettingsDialog;
    private TurnOnBTDialog turnOnBTDialog;
    private TurnOnDonotDisturbDialog turnOnDonotDisturbDialog;
    private TurnOnNotifcationDialog turnOnNotifcationDialog;
    private TurOnGpsDialog turOnGpsDialog;
    private Float peeksOverloading = 0f;
    private int countDemoRides = 0;
    private long DEMO_MODE_POPUP = 20000;
    public static FragmentManager fm;
    public static DemoModeEndDialog endDialog;
    private int demototalRides;
    private long totalDaysDemoMode;
    Timer demomodeTimer;

    public MySafetyFragmentGraph() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        graphView = inflater.inflate(R.layout.fragment_my_safety_stats_graph, container, false);
        // For setting up the different logo of toll bar for demo and paid mode
        RelativeLayout toolbar = (RelativeLayout) graphView.findViewById(R.id.navbar);
        ImageView fd_logo = (ImageView) toolbar.findViewById(R.id.tol_bar_logo);
        if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)) {
            fd_logo.setImageResource(R.drawable.logo_freeedrive_demo_mode);
        } else {
            fd_logo.setImageResource(R.drawable.logo_topbar);
        }

        driverSCoreCircle = (FitChart) graphView.findViewById(R.id.daily_stats_fitchartlastride_score_3);
        peeksCircle = (FitChart) graphView.findViewById(R.id.daily_stats_fitchartlastride_peeks);
        daily_stats_score_last_ride_3 = (TextView) graphView.findViewById(R.id.daily_stats_score_last_ride_3);
        daily_stats_peeks = (TextView) graphView.findViewById(R.id.daily_stats_peeks);
        /*imageviewDailyStats = (ImageView) graphView.findViewById(R.id.imageviewDailyStats);
        imageviewFleetStats = (ImageView) graphView.findViewById(R.id.imageviewFleetStats);*/
        graph_stats_left_arrow = (ImageView) graphView.findViewById(R.id.graph_stats_arrow_left);
        graph_stats_right_arrow = (ImageView) graphView.findViewById(R.id.graph_stats_arrow_right);
        enterSpeed = (EditText) graphView.findViewById(R.id.manualSpeeds);
        drivers_name = (TextView) graphView.findViewById(R.id.drivers_name);
        tv_remaining_rides = (TextView) graphView.findViewById(R.id.tv_remaining_rides);
        btn_start_stop_ride = (Button) graphView.findViewById(R.id.btn_start_stop_ride);

        graph_stats_left_arrow.setVisibility(View.INVISIBLE);
        SharedPreferences settings = null;
        chart = (LineChart) graphView.findViewById(R.id.chart);
      /*  if (!DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)) {
            chart.setMinimumHeight(1100);
        }else{
            chart.setMinimumHeight(500);
        }*/
        driverSCoreCircle.setValue(100f);

        peeksCircle.setMaxValue(15f);
        peeksCircle.setValue(15f);
        calander = Calendar.getInstance();
        df = new SimpleDateFormat("dd/MM/yyyy");
        currentDate = df.format(calander.getTime());


        turOnGpsDialog = new TurOnGpsDialog(getContext());
        turnOnBTDialog = new TurnOnBTDialog(getContext());
        openBatterySettingsDialog = new OpenBatterySettingsDialog(getContext());
        turnOnDonotDisturbDialog = new TurnOnDonotDisturbDialog(getContext());
        turnOnNotifcationDialog = new TurnOnNotifcationDialog(getContext());
        fm = getActivity().getSupportFragmentManager();
        endDialog = DemoModeEndDialog.newInstance("");

        //For setting the text view of remaing rides


        //openBatterySettingsDialog.show();


        /*imageviewDailyStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    imageviewFleetStats.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.fleet_icon_grey));
                    imageviewFleetStats.setBackground(null);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    imageviewDailyStats.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.mysafety_icon_blue));
                    imageviewDailyStats.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_fragment_shape));
                }
            }
        });

        imageviewFleetStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    imageviewDailyStats.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.mysafety_icon_grey));
                    imageviewDailyStats.setBackground(null);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    imageviewFleetStats.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.fleet_icon_blue));
                    imageviewFleetStats.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_fragment_shape));
                }
                fragment = new FleetSafetyGraph();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.phone_fragment, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });*/
//TODO we check expiration and run a handler to show dialog

        btn_start_stop_ride.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {

                if (DataHandler.getBooleanPreferences(AppConstants.RIDE_BUTTON_PRESSED)) {
                    DataHandler.updatePreferences(AppConstants.RIDE_BUTTON_PRESSED, false);
                    btn_start_stop_ride.setBackground(getActivity().getResources().getDrawable(R.drawable.blue_outline));
                    btn_start_stop_ride.setText("START A DEMO RIDE");
                    btn_start_stop_ride.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryNew));
                    Log.e("startCalled", "start called");
                    getActivity().stopService(new Intent(getActivity(), LocationService.class));


                    if (!rideneverStarted) {
                        try {
                            AlertDialog.Builder popUp = new AlertDialog.Builder(getActivity());
                            popUp.setMessage(getActivity().getResources().getString(R.string.ride_never_started)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            popUp.show();
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }

                    if (DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)) {
                        Intent sendInRangeIntent = new Intent("IamOutOfRange");
                        getActivity().sendBroadcast(sendInRangeIntent);
                       /* int count_rides=DataHandler.getIntPreferences(AppConstants.COUNT_DEMO_RIDES);
                        DataHandler.updatePreferences(AppConstants.COUNT_DEMO_RIDES,count_rides-1);
                        String remaingRides = String.valueOf(DataHandler.getIntPreferences(AppConstants.COUNT_DEMO_RIDES)+"/40");
                        String headingRemaingRides = "Remaining Rides : ";
                        String spannableText = headingRemaingRides+remaingRides;
                        Spannable spanText = new SpannableString(spannableText);
                        spanText.setSpan(new ForegroundColorSpan(getResources()
                                .getColor(R.color.colorPrimaryNew)), headingRemaingRides.length(), headingRemaingRides.length()+remaingRides.length(), 0);

                        tv_remaining_rides.setText(spanText);
*/
                    }


                } else {
                    if ((40 - countDemoRides) < 3) {

                        try {
                            AlertDialog.Builder popUp = new AlertDialog.Builder(getActivity());
                            popUp.setMessage(getActivity().getResources().getString(R.string.popUpDemo)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            popUp.show();
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }
                    DataHandler.updatePreferences(AppConstants.RIDE_BUTTON_PRESSED, true);
                    btn_start_stop_ride.setBackground(getActivity().getResources().getDrawable(R.drawable.orange_outline));
                    btn_start_stop_ride.setText("STOP DEMO RIDE");
                    btn_start_stop_ride.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                    getActivity().startService(new Intent(getActivity(), LocationService.class));
                    AppUtils.isInRange = true;
                    Log.e("stopcalled", "stopcalled");
                }

            }
        });

        graph_stats_right_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fragment = new MyDailyStatsFragment();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.popBackStackImmediate();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(getId(), fragment);

                //   fragmentTransaction.addToBackStack(null);
                Log.e("fragCountS", "onClick: " + fragmentManager.getBackStackEntryCount());
                fragmentTransaction.commit();
            }
        });
        //intiliaze the Views
/*        relativeLayoutPhoneSafety = (RelativeLayout) graphView.findViewById(R.id.insidecontaintermobilehotspot);
        relativeLayoutdriving = (RelativeLayout) graphView.findViewById(R.id.drivingrelativelayout);
        relativeLayoutdrivingPerformance = (RelativeLayout) graphView.findViewById(R.id.graph_drivingrelativelayout);
        relativeLayoutPhone = (RelativeLayout) graphView.findViewById(R.id.insidecontaintermobilehotspot);
        ImgDrivingPerformance = (ImageView) graphView.findViewById(R.id.drivingperformance);



        //intiliaze the TextViews
        phonesafety = (TextView) graphView.findViewById(R.id.textviewmobilehotSpot);
        drivingperformance = (TextView) graphView.findViewById(R.id.textviewdrivingperformance);

        phoneSafety = (Button) graphView.findViewById(R.id.graph_fragment_phonesafety_btn);
        drivingPerformance = (Button) graphView.findViewById(R.id.graph_fragment_drivingperformance_btn);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            relativeLayoutPhoneSafety.setBackground(getActivity().getResources().getDrawable(R.drawable.phonesafety_custom_shape));
        }
        phonesafety.setTextColor(getActivity().getResources().getColor(R.color.white));*/


        String profileData = DataHandler.getStringPreferences(AppConstants.TEMP_DISPLAY_KEY);
        try {
            if (profileData != null && !profileData.equalsIgnoreCase("")) {
                JSONObject jsonObj = new JSONObject(profileData);
                if (jsonObj != null) {
                    drivers_name.setText(jsonObj.getString("first_name"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        enterSpeed.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {

                try {


                    String temp = enterSpeed.getText().toString();
                    //speedCheck.setText(temp);
                    speed = Integer.parseInt(temp);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        manageMySafetyStats();
        setData();


       /* if(DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE) == true){
            if(demototalRides != 40){
                btn_start_stop_ride.setBackgroundColor(getActivity().getResources().getColor(R.color.colorBTestMenu));
                btn_start_stop_ride.setClickable(false);
            }
        }*/
        // TODO IMP DEMO MODE SUBCRIPTION OVER
        /*demoModeSubscription();*/
        return graphView;

    }


    /*public void demomodeendTimer() {

        demomodeTimer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                demomodeTimer.cancel();
                //shortTimer = null;
                demoModeSubscription();
                DemoModeDialog();
            }
        };
        demomodeTimer.schedule(timerTask, DEMO_MODE_POPUP);
    }*/

   /* private Handler demomodeendHanlder = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try {

                if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE) == true) {
                  // fixme rename me ass
                    DemoModeDialog();
                    demomodeendHanlder.sendMessageDelayed(new Message(), DEMO_MODE_POPUP);
                } else {
                    if (demomodeendHanlder != null) {
                        demomodeendHanlder.removeCallbacksAndMessages(null);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            //demomodeendHanlder.removeCallbacksAndMessages(null);
        }
    };*/


    public static  void DemoModeDialog() {

        try {
            if (endDialog != null && endDialog.isAdded()) {
                return;
            } else {
               /* Log.e("BF-PK-FD", "DEMO_MODE_END_POPUP SHOW");*/
                endDialog.show(fm, "");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    @Override
    public void onResume() {
        super.onResume();

        getActivity().registerReceiver(mMessageReceiverSensor, new IntentFilter("com.freeedrive_saving_driving.sensor_event"));
//register to MessageReceiver to receive a message to activate GPS:
        getActivity().registerReceiver(mMessageReceiverGPSENABLE, new IntentFilter("com.freeedrive_saving_driving.enable_gps"));
        //register to MessageReceiver to receive a message to launch synchronization :
        getActivity().registerReceiver(mMessageReceiverBTenable, new IntentFilter("com.freeedrive.bluetooth_enable"));
        getActivity().registerReceiver(mMessageReceiverNotificationenable, new IntentFilter("com.freeedrive.notification_settings"));
        //register to MessageReceiver to receive a message to launch synchronization :
        getActivity().registerReceiver(mMessageReceiverSynchonization, new IntentFilter("com.freeedrive_saving_driving.synchronization"));

        getActivity().registerReceiver(mMessageReceiverDonotDistrub, new IntentFilter("android.settings.NOTIFICATION_POLICY_ACCESS_SETTINGS"));

        getActivity().registerReceiver(mBatteryRecieiver, new IntentFilter("battery_settings"));

    }


    /**
     * Handler for received Intents for the "com.freeedrive_saving_driving.synchronization" event
     * It allows to launch sync with the new list with the localities  :
     */
    private BroadcastReceiver mMessageReceiverDonotDistrub = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            try {
                if (turnOnDonotDisturbDialog != null && turnOnDonotDisturbDialog.isShowing()) {
                    return;
                } else {
                    turnOnDonotDisturbDialog.show();
                    if (mMessageReceiverDonotDistrub != null)
                        getActivity().unregisterReceiver(mMessageReceiverDonotDistrub);
                    mMessageReceiverDonotDistrub = null;
                }
            } catch (Exception e) {

            }


        }
    };


    /**
     * Handler for received Intents for the "com.freeedrive_saving_driving.synchronization" event
     * It allows to launch sync with the new list with the localities  :
     */
    private BroadcastReceiver mMessageReceiverSynchonization = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            Intent i = new Intent(getActivity(), ScoreSynchronizationActivity.class);
            startActivity(i);
        }
    };


    private BroadcastReceiver mMessageReceiverSensor = new BroadcastReceiver() {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("mMessageReceiverSensor", "on recieve");
            manageMySafetyStats();
            setData();
            try{
                if(DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE_SUBSCRIPTION_EXPIRED)== true){
                    btn_start_stop_ride.setBackgroundColor(getActivity().getResources().getColor(R.color.colorBTestMenu));
                    btn_start_stop_ride.setClickable(false);
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    };

    private BroadcastReceiver mMessageReceiverGPSENABLE = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                if (turOnGpsDialog != null && turOnGpsDialog.isShowing()) {
                    return;
                } else {
                    turOnGpsDialog.show();
                    if (mMessageReceiverGPSENABLE != null)
                        getActivity().unregisterReceiver(mMessageReceiverGPSENABLE);
                    mMessageReceiverGPSENABLE = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            }


        }
    };

    private BroadcastReceiver mMessageReceiverNotificationenable = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (turnOnNotifcationDialog != null && turnOnNotifcationDialog.isShowing()) {
                    return;
                } else {
                    turnOnNotifcationDialog.show();
                    Long currentTimeLogs = null;
                    if (mMessageReceiverNotificationenable != null)
                        currentTimeLogs = System.currentTimeMillis();
                    String time = formateLongToOnlyDateForServer(currentTimeLogs);

                    int permissionexternalStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

                    if (permissionexternalStorage == PackageManager.PERMISSION_GRANTED) {
                        LoggingOperations.writeToFile(getActivity(), "NOTIFICATION DIALOG WAS SHOWN TO user" + time);
                    }
                    if (mMessageReceiverNotificationenable != null)
                        getActivity().unregisterReceiver(mMessageReceiverNotificationenable);
                    mMessageReceiverNotificationenable = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            }
        }
    };
    private BroadcastReceiver mMessageReceiverBTenable = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                listenForBluetoothEnable();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };


    private BroadcastReceiver mBatteryRecieiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                openBatterySettingsDialog();

                Long currentTimeLogs =  System.currentTimeMillis();
                String time=  formateLongToOnlyDateForServer(currentTimeLogs);

                int permissionexternalStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

                if (permissionexternalStorage == PackageManager.PERMISSION_GRANTED) {
                    LoggingOperations.writeToFile(getActivity(), "BATTERY DIALOG WAS SHOWN TO user" + time);
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }
    };

    private void openBatterySettingsDialog() {
        if (openBatterySettingsDialog != null && openBatterySettingsDialog.isShowing()) {
            return;
        } else {
            openBatterySettingsDialog.show();
        }

    }

    public void listenForBluetoothEnable() {

        Boolean isEnabled = false;
        BluetoothAdapter mBluetoothAdaptater = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdaptater != null && mBluetoothAdaptater.isEnabled()) {
            isEnabled = true;
        }

        try {
            if (turnOnBTDialog != null && turnOnBTDialog.isShowing()) {
                return;
            } else if (!isEnabled) {
                if (turnOnBTDialog.isShowing()) {
                    return;
                } else {
                    turnOnBTDialog.show();
                    if (mMessageReceiverBTenable != null)
                        getActivity().unregisterReceiver(mMessageReceiverBTenable);
                    mMessageReceiverBTenable = null;

                }

            }
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //  closeD();


    }


    @Override
    public void onDestroy() {

        super.onDestroy();
        // Unregister broadcast BT listeners
        try {
            if (mMessageReceiverBTenable != null)
                getActivity().unregisterReceiver(mMessageReceiverBTenable);
            mMessageReceiverBTenable = null;

            if (mBatteryRecieiver != null)
                getActivity().unregisterReceiver(mBatteryRecieiver);
            mBatteryRecieiver = null;

            if (mMessageReceiverDonotDistrub != null)
                getActivity().unregisterReceiver(mMessageReceiverDonotDistrub);
            mMessageReceiverDonotDistrub = null;

            if (mMessageReceiverGPSENABLE != null)
                getActivity().unregisterReceiver(mMessageReceiverGPSENABLE);
            mMessageReceiverGPSENABLE = null;

            if (mMessageReceiverNotificationenable != null)
                getActivity().unregisterReceiver(mMessageReceiverNotificationenable);
            mMessageReceiverNotificationenable = null;

            if (mMessageReceiverSensor != null)
                getActivity().unregisterReceiver(mMessageReceiverSensor);
            mMessageReceiverSensor = null;
            if (mMessageReceiverSynchonization != null)
                getActivity().unregisterReceiver(mMessageReceiverSynchonization);
            mMessageReceiverSynchonization = null;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        //todo we nullify this handler call backs


    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void manageMySafetyStats() {
        RideBDD tmp = new RideBDD(getContext());
        tmp.open();

        //for today's score
        try {


            long timestampstartDate = AppUtils.getStartOfDayInMillis(currentDate);
            Log.e("timestampstartdate", ":" + timestampstartDate);
            long timestampnendDate = AppUtils.getEndOfDayInMillis(currentDate);
            Log.e("timestampnendDate", ":" + timestampnendDate);
            Log.e("currentDate", currentDate);
            float sumBadCountToday;
            float totalRidesToday;
            float avgRideTime;
            float avgBadCountToday;
            float peeksColorCoding;
            JSONArray listeRides = tmp.getAllRidesByDateForDailyAvg(timestampstartDate, timestampnendDate);


            //the rides list :
            if (listeRides != null && listeRides.length() > 0) {
                temptotalDailytime = tmp.totalElapsedtimeDaily(timestampstartDate, timestampnendDate);

                int totalAvergaeScore = 0;

                totalAvergaeScore = calculateAverage(listeRides, temptotalDailytime);
                sumBadCountToday = tmp.totalBadCountToday(timestampstartDate, timestampnendDate);

                totalRidesToday = tmp.totalRidesToday(timestampstartDate, timestampnendDate);
                avgBadCountToday = sumBadCountToday / totalRidesToday;

                avgRideTime = tmp.avgRideTime(timestampstartDate, timestampnendDate);
                peeksColorCoding = (float) avgBadCountToday / avgRideTime;

                //TODO
                //peeks color coding

                int avgPeeks = Math.round(avgBadCountToday);
                /*String s = String.format("%.1f",avgBadCountToday);
                Log.e("AVG PEEKS",s);*/
                daily_stats_peeks.setText("" + avgPeeks);
                peeksCircle.setValue(15f);
                float newAVGAllRides = (float) (totalAvergaeScore);


                if (newAVGAllRides > 100) {
                    newAVGAllRides = 100;
                } else if (newAVGAllRides <= 0) {
                    newAVGAllRides = 0;

                }
                Log.e("peeksColorCoding", "" + peeksColorCoding);
                Collection<FitChartValue> fitChartValuesPeeks = new ArrayList<>();
                if (peeksColorCoding <= 0.25) {
                    daily_stats_peeks.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
                    fitChartValuesPeeks.add(new FitChartValue(15f, getActivity().getResources().getColor(R.color.colorPieChartGreen)));

                } else if (peeksColorCoding >= 0.25 && peeksColorCoding <= 0.5) {

                    daily_stats_peeks.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                    fitChartValuesPeeks.add(new FitChartValue(15f, getActivity().getResources().getColor(R.color.colorPieChartOrange)));

                } else if (peeksColorCoding > 0.5) {

                    daily_stats_peeks.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartRed));
                    fitChartValuesPeeks.add(new FitChartValue(15f, getActivity().getResources().getColor(R.color.colorPieChartRed)));

                }
                peeksCircle.setValues(fitChartValuesPeeks);

                int value_all_rides = (int) Math.round(newAVGAllRides);
                daily_stats_score_last_ride_3.setText("" + value_all_rides + "%");
                driverSCoreCircle.setValue(value_all_rides);
                Collection<FitChartValue> fitChartValues = new ArrayList<>();
                if (value_all_rides >= 80) {
                    daily_stats_score_last_ride_3.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
                    fitChartValues.add(new FitChartValue(value_all_rides, getActivity().getResources().getColor(R.color.colorPieChartGreen)));

                } else if (value_all_rides < 80 && value_all_rides >= 50) {
                    daily_stats_score_last_ride_3.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                    fitChartValues.add(new FitChartValue(value_all_rides, getActivity().getResources().getColor(R.color.colorPieChartOrange)));

                } else if (value_all_rides < 50) {
                    daily_stats_score_last_ride_3.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartRed));
                    fitChartValues.add(new FitChartValue(value_all_rides, getActivity().getResources().getColor(R.color.colorPieChartRed)));

                }

                driverSCoreCircle.setValues(fitChartValues);

                Log.e("avg/day-badcountavg/day", "" + newAVGAllRides + "  ---" + avgBadCountToday);


            } else {
                driverSCoreCircle.setValue(100f);
                peeksCircle.setValue(15f);

            }
        } catch (IndexOutOfBoundsException e) {
            Log.e("manageMySafetyStats", "" + e);

        }
        if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)) {


            if (DataHandler.getBooleanPreferences(AppConstants.RIDE_BUTTON_PRESSED) && DataHandler.getBooleanPreferences(AppConstants.RIDE_WAS_ACTIVE)) {
                btn_start_stop_ride.setBackground(getActivity().getResources().getDrawable(R.drawable.orange_outline));
                btn_start_stop_ride.setText("STOP DEMO RIDE");
                btn_start_stop_ride.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
            } else {
                DataHandler.updatePreferences(AppConstants.RIDE_BUTTON_PRESSED, false);
                btn_start_stop_ride.setBackground(getActivity().getResources().getDrawable(R.drawable.blue_outline));
                btn_start_stop_ride.setText("START A DEMO RIDE");
                btn_start_stop_ride.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryNew));
            }

            countDemoRides = 40 - tmp.sumDemoRidesCount();

            if (countDemoRides <= 0) {
                String remaingRides = String.valueOf("0" + "/40");
                String headingRemaingRides = "Remaining Rides : ";
                String spannableText = headingRemaingRides + remaingRides;
                Spannable spanText = new SpannableString(spannableText);
                spanText.setSpan(new ForegroundColorSpan(getResources()
                        .getColor(R.color.colorPrimaryNew)), headingRemaingRides.length(), headingRemaingRides.length() + remaingRides.length(), 0);
                tv_remaining_rides.setText(spanText);
            } else {
                Log.e("countDemoRides ", "" + countDemoRides);
                String remaingRides = String.valueOf(countDemoRides + "/40");
                String headingRemaingRides = "Remaining Rides : ";
                String spannableText = headingRemaingRides + remaingRides;
                Spannable spanText = new SpannableString(spannableText);
                spanText.setSpan(new ForegroundColorSpan(getResources()
                        .getColor(R.color.colorPrimaryNew)), headingRemaingRides.length(), headingRemaingRides.length() + remaingRides.length(), 0);
                tv_remaining_rides.setText(spanText);
            }

        } else {
            btn_start_stop_ride.setVisibility(View.GONE);
            tv_remaining_rides.setVisibility(View.GONE);
        }

        tmp.close();

    }


    private void demoModeSubscription() {
        if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE) == true) {
            RideBDD tmp = new RideBDD(getActivity());
            tmp.open();
            demototalRides = tmp.sumDemoRidesCount();
            totalDaysDemoMode = AppUtils.totaldaysDemoMode(getActivity());
            if (demototalRides >= 40 || totalDaysDemoMode >= 30) {
                btn_start_stop_ride.setBackgroundColor(getActivity().getResources().getColor(R.color.colorBTestMenu));
                btn_start_stop_ride.setClickable(false);
                DataHandler.updatePreferences(AppConstants.DEMO_MODE_SUBSCRIPTION_EXPIRED,true);
                //demomodeendTimer();
                /*demomodeendHanlder.sendMessageDelayed(new Message(), DEMO_MODE_POPUP)*/;
                DemoModeDialog();
            }
            tmp.close();
        }
    }


    private boolean isNotificationManager() throws Exception {
        ContentResolver contentResolver = getContext().getContentResolver();
        String enabledNotificationListeners = Settings.Secure.getString(contentResolver, "enabled_notification_listeners");
        //Log.e("FD","enabledNotificationListeners: " + enabledNotificationListeners);
        String packageName = getContext().getPackageName();
        return enabledNotificationListeners != null && enabledNotificationListeners.contains(packageName);
    }

    /**
     * This method allows to update the fit chart and driving time of All my rides.
     * It takes the rides from database, calculate the avg and driving time.
     */
    public int calculateAverage(JSONArray listRide, float totalTime) {
        double tempvaTotal_ = totalTime;
        double average = 0.0;
        for (int i = 0; i < listRide.length(); i++) {
            try {
                JSONObject obj = listRide.getJSONObject(i);
                double currentTime = Double.valueOf(obj.getInt("time_elapsed"));
                double currentScore = (obj.getDouble("score"));

                double divvar = currentTime / tempvaTotal_;

                average = average + currentScore * divvar;


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        double roundavg = Math.round(average);
        int average_int = (int) roundavg;
        Log.e("average", "" + average_int);

   /*     if (!DataHandler.getStringPreferences(AppConstants.SCORE_TOTAL_SCORE).isEmpty()){
            String serverAvg =DataHandler.getStringPreferences(AppConstants.SCORE_TOTAL_SCORE);
            int  intserverAvg = Integer.valueOf(serverAvg);
            int newAvg = (intserverAvg+average_int)/2;
            average_int =  Math.round(newAvg);;
        }*/
        return average_int;
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void setData() {
        int[] numArr = {1, 2, 3, 4, 5};
        final List<String> dates = FetchPeekDates();
        int days = 1;
        final HashMap<Integer, String> hashMapDates = new HashMap<>();
        final HashMap<Integer, String> scoreLabels = new HashMap<>();
        scoreLabels.put(0, "0%");
        scoreLabels.put(1, "20%");
        scoreLabels.put(2, "40%");
        scoreLabels.put(3, "60%");
        scoreLabels.put(4, "80%");
        scoreLabels.put(5, "100%");


        for (int i = 0; i < dates.size(); i++) {

            hashMapDates.put(i, AppUtils.formatStringDateForGraph(dates.get(i)));
            days++;
        }


        List<Entry> entries1 = new ArrayList<Entry>();

        for (int num : numArr) {
            entries1.add(new Entry(num, num));
        }

        ArrayList<Entry> peeksVals = FetchPeekValues();

        ArrayList<Entry> scoreVals = FetchScoreValues();

        LineDataSet peeks;
        LineDataSet score;
        // create a dataset and give it a type
        if (peeksVals.size() > 0 && scoreVals.size() > 0) {
            peeks = new LineDataSet(peeksVals, "");
            peeks.setFillAlpha(110);
            peeks.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
            peeks.setColor(colorplotlineCompany);
            peeks.setCircleColor(Color.BLACK);
            peeks.setLineWidth(3f);
            peeks.setCircleRadius(2.5f);
            peeks.setDrawCircleHole(false);
            peeks.setValueTextSize(9f);
            peeks.setAxisDependency(YAxis.AxisDependency.RIGHT);

            score = new LineDataSet(scoreVals, "");
            score.setFillAlpha(110);
            score.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
            score.setAxisDependency(YAxis.AxisDependency.LEFT);
            score.setColor(colorplotlinedriver);
            score.setCircleColor(colorplotlinedriver);
            score.setLineWidth(3f);
            score.setCircleRadius(2.5f);
            score.setDrawCircleHole(false);
            score.setValueTextSize(9f);

            // set1.setFillColor(Color.RED);

            // set the line to be drawn like this "- - - - - -"
            // set1.enableDashedLine(10f, 5f, 0f);
            // set1.enableDashedHighlightLine(10f, 5f, 0f);


            //  peeks.set
            // x-axis representing days
            XAxis xAxis = chart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setEnabled(true);
            xAxis.setLabelCount(5, true);

            xAxis.setAxisMinimum(0f);
            xAxis.setAxisMaximum(4f);
            xAxis.setTextColor(colordays);
            //xAxis.enableGridDashedLine(1f,1f,1f);
            xAxis.setDrawGridLines(true);
            xAxis.setDrawAxisLine(true);
            xAxis.setTextSize(9f);
            if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)) {
                xAxis.setTextSize(9f);
            } else {
                xAxis.setTextSize(15f);
            }
            xAxis.setTextColor(colordays);

            xAxis.setValueFormatter(new IAxisValueFormatter() {
                @Override
                public String getFormattedValue(float value, AxisBase axis) {
                    return dates.get((int) value);

                }
            });


            YAxis leftAxis = chart.getAxis(YAxis.AxisDependency.LEFT);
            leftAxis.setAxisMaximum(103f);

            leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            leftAxis.setEnabled(true);
            leftAxis.setAxisMinimum(-3f);
            leftAxis.setTextColor(color);
            leftAxis.setTextSize(12f);
            //leftAxis.setYOffset(20f);
            leftAxis.enableGridDashedLine(20f, 20f, 20f);
            leftAxis.setLabelCount(scoreLabels.size(), false);
            DecimalFormat decimalFormat = new DecimalFormat();
            decimalFormat.setDecimalSeparatorAlwaysShown(false);

            leftAxis.setValueFormatter(new PercentFormatter(decimalFormat));
 /*          leftAxis.setValueFormatter(new IAxisValueFormatter() {
               @Override
               public String getFormattedValue(float value, AxisBase axis) {
                   return scoreLabels.get((int)value);

               }
           });*/


            YAxis rightAxis = chart.getAxisRight();
            rightAxis.setAxisMinimum(-1f);
            if (peeksOverloading > 15 && peeksOverloading < 99) {
                rightAxis.setAxisMaximum(peeksOverloading + 1);
            } else if (peeksOverloading >= 100) {
                rightAxis.setAxisMaximum(101f);
            } else {
                rightAxis.setAxisMaximum(16f);
            }

            rightAxis.enableGridDashedLine(3f, 3f, 3f);
            rightAxis.setTextColor(color);
            rightAxis.setTextSize(12f);

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(peeks); // add the datasets
            dataSets.add(score);

            // create a data object with the datasets
            LineData data = new LineData(dataSets);
            data.setValueFormatter(new DefaultValueFormatter(0));
            // set data
            //  chart.getDescription().setEnabled(false)
            //;
            chart.getDescription().setText("");
            //for removing the the squares of pf color i.e legends of description
            chart.getLegend().setEnabled(false);
            chart.setHovered(true);
            chart.setScaleEnabled(false);
            chart.setData(data);
            chart.setPinchZoom(false);
            chart.animate();
            chart.setExtraBottomOffset(1f);
            chart.animateX(950);
            chart.setLayoutMode(ViewGroup.LAYOUT_MODE_OPTICAL_BOUNDS);
        }

       /* Legend l = chart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.CIRCLE);*/
    }
/*
    */

    /**
     * generates a random ChartData object with just one DataSet
     *
     * @return
     *//*

    public static LineData generateDataLine(*/
/*JSONArray driverArray , JSONArray companyArray*//*
) {
        LineData cd = null;
        String driverAvg_Date = null;
        String driverAvg_Score = null;
        String companyAvg_Date = null;
        String companyAvg_Score = null;
        ArrayList<Entry> e1 = new ArrayList<Entry>();
        ArrayList<Entry> e2 = new ArrayList<Entry>();
        ArrayList<Entry> e3 = new ArrayList<Entry>();
        ArrayList<Entry> xVals = new ArrayList<>();
        ArrayList<Entry> yVals = new ArrayList<>();
        xVals = setXAxisValues();
        yVals = FetchScoreValues();

*/
/*
        if(driverArray !=null && companyArray !=null){
*//*


 */
/*       if (driverArray !=null) {
            for(int i = 0;i < driverArray.length();i++){

                try {
                    jsonObject_driverAvg = driverArray.getJSONObject(i);
                     Log.e("DriverAvgObject "," : "+jsonObject_driverAvg);

                    *//*
*/
/*driverAvg_Date = jsonObject_driverAvg.getString("date");
                    Log.e("GRAPH_driverAvg_Date", driverAvg_Date);*//*
*/
/*

                    driverAvg_Score = jsonObject_driverAvg.getString("value");
                    Log.e("GRAPH_drivingAvg_Score",driverAvg_Score);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                float graph_dirver_avg_score = Float.parseFloat(driverAvg_Score);
                e1.add(new Entry(i,graph_dirver_avg_score));


            }
        }*//*



     */
/*   if(companyArray !=null){
            for(int i=0;i<companyArray.length();i++){
                try {
                    jsonObject_companyAvg = companyArray.getJSONObject(i);
                    Log.e("CompanyAvgObject "," : "+jsonObject_companyAvg);

                    companyAvg_Date = jsonObject_companyAvg.getString("date");
                    Log.e("companyAvg_Date",companyAvg_Date);
                    companyAvg_Score =jsonObject_companyAvg.getString("value");
                    Log.e("companyAvg_Score",companyAvg_Score);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                float graph_company_avg_score  = Float.parseFloat(companyAvg_Score);
                //e2.add(new Entry(i, e1.get(i).getY() - 30));
                e2.add(new Entry(i, graph_company_avg_score));

            }


        }*//*


            LineDataSet d1 = new LineDataSet(xVals, "");


            d1.setLineWidth(3.5f);
            //d1.setCircleRadius(4.5f);
            d1.setColor(colorplotlinedriver);
            d1.setDrawValues(true);
            d1.disableDashedLine();
            d1.setDrawCircleHole(false);


            LineDataSet d2 = new LineDataSet(yVals, "");
            d2.setLineWidth(3.5f);
            //d2.setCircleRadius(4.5f);
            d2.setColor(colorplotlineCompany);
            //d2.setColor(ColorTemplate.VORDIPLOM_COLORS[0]);

            d2.setDrawValues(true);
            d2.disableDashedLine();
            d2.setDrawCircleHole(true);



            ArrayList<ILineDataSet> sets = new ArrayList<ILineDataSet>();
            sets.add(d1);
            sets.add(d2);

            // apply styling
            chart.getDescription().setEnabled(false);
            chart.setDrawMarkers(true);
            chart.setDrawGridBackground(false);
            chart.setScaleEnabled(true);



            XAxis xAxis = chart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.TOP);
            xAxis.setEnabled(false);
            xAxis.setLabelCount(5);
            xAxis.setAxisMinimum(1f);
            xAxis.setAxisMaximum(5f);
            xAxis.setDrawGridLines(true);
            xAxis.setDrawAxisLine(true);


            YAxis leftAxis = chart.getAxisLeft();
            leftAxis.setAxisMaxValue(100f);
            leftAxis.setAxisMinValue(0f);

            leftAxis.setTextColor(color);
            //leftAxis.setYOffset(20f);
            leftAxis.enableGridDashedLine(20f, 20f, 20f);
        */
/*leftAxis.setTypeface(mTf);*//*

            leftAxis.setLabelCount(6, false);
            */
/*leftAxis.setDrawGridLines(true);*//*

            leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
            leftAxis.setAxisMaximum(100f);
            leftAxis.setTextSize(15f);
            YAxis rightAxis = chart.getAxisRight();
        */
/*rightAxis.setTypeface(mTf);*//*

            rightAxis.setLabelCount(6, false);
            */
/*rightAxis.setDrawGridLines(true);*//*

            rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
            rightAxis.setAxisMaximum(15f);
            rightAxis.enableGridDashedLine(3f, 3f, 3f);
            rightAxis.setTextColor(color);
            rightAxis.setTextSize(15f);

            // do not forget to refresh the chart
            // holder.chart.invalidate();
            chart.animateX(750);


            cd = new LineData(sets);
            // set data
            chart.setData(cd);


        */
/*}*//*

        return cd;
    }
*/
    private List<String> FetchPeekDates() {

        List<String> dates = new ArrayList<String>();
        try {
            RideBDD rideBDD = new RideBDD(getActivity());
            rideBDD.open();
            long timestampnendDate = AppUtils.getEndOfDayInMillis(currentDate);
            dates = rideBDD.last5DATESgraphplottingPointsPeeks(timestampnendDate);
            Collections.reverse(dates);
            if (dates.size() < 5) {
                for (int i = dates.size(); i < 5; i++) {
                    dates.add(i, "");
                }
            }


            //first value represts days and second value represents score or peek
/*    yVals.add(new Entry(0, 5));
    yVals.add(new Entry(1, 10));
    yVals.add(new Entry(2, 7));
    yVals.add(new Entry(3, 12));
    yVals.add(new Entry(5, 0));*/
  /*  yVals.add(new Entry(50.5f, 11));
    yVals.add(new Entry(90, 13));
    yVals.add(new Entry(150.9f, 14));*/

            rideBDD.close();

        } catch (IndexOutOfBoundsException e) {
            Log.e("FetchPeekValues", "" + e);
        }
        return dates;
    }


    // This is used to store Y-axis values
    private ArrayList<Entry> FetchPeekValues() {

        ArrayList<Entry> peeks = new ArrayList<Entry>();
        ArrayList<Float> plottingPoints = new ArrayList<>();
        try {
            RideBDD rideBDD = new RideBDD(getActivity());
            rideBDD.open();
            long timestampnendDate = AppUtils.getEndOfDayInMillis(currentDate);
            plottingPoints = rideBDD.last5daysgraphplottingPointsPeeks(timestampnendDate);
            Collections.reverse(plottingPoints);
            if (plottingPoints != null && plottingPoints.size() > 0) {


                for (int i = 0; i < plottingPoints.size(); i++) {
                    if (plottingPoints.get(i) >= 15) {
                        peeksOverloading = plottingPoints.get(i);
                    }
                    peeks.add(new Entry(i, Math.round(plottingPoints.get(i))));

                }
            }

            //first value represts days and second value represents score or peek
/*    yVals.add(new Entry(0, 5));
    yVals.add(new Entry(1, 10));
    yVals.add(new Entry(2, 7));
    yVals.add(new Entry(3, 12));
    yVals.add(new Entry(5, 0));*/
  /*  yVals.add(new Entry(50.5f, 11));
    yVals.add(new Entry(90, 13));
    yVals.add(new Entry(150.9f, 14));*/

            rideBDD.close();

        } catch (IndexOutOfBoundsException e) {
            Log.e("FetchPeekValues", "" + e);
        }
        return peeks;
    }

    // This is used to store Y-axis values
    private ArrayList<Entry> FetchScoreValues() {
        ArrayList<Entry> score = new ArrayList<Entry>();
        ArrayList<Float> plottingPoints = new ArrayList<>();
        try {

            RideBDD rideBDD = new RideBDD(getActivity());
            rideBDD.open();
            long timestampnendDate = AppUtils.getEndOfDayInMillis(currentDate);

            plottingPoints = rideBDD.last5daysgraphplottingPointsScore(timestampnendDate);
            Collections.reverse(plottingPoints);
            if (plottingPoints != null && plottingPoints.size() > 0) {


                for (int i = 0; i < plottingPoints.size(); i++) {
                    score.add(new Entry(i, Math.round(plottingPoints.get(i))));
                }
            }

/*
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        yVals.add(new Entry(0, 60));
        yVals.add(new Entry(1, 70));
        yVals.add(new Entry(2, 90));
        yVals.add(new Entry(3, 50));
        yVals.add(new Entry(4, 100));
        yVals.add(new Entry(5, 24));*/
/*        yVals.add(new Entry(70.5f, 2));
        yVals.add(new Entry(100, 3));
        yVals.add(new Entry(180.9f, 4));*/
            rideBDD.close();
        } catch (IndexOutOfBoundsException e) {
            Log.e("FetchPeekValues", "" + e);
        }
        return score;
    }

    private ArrayList<String> setXAxisValues() {
        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add("10");
        xVals.add("20");
        xVals.add("30");
        xVals.add("30.5");
        xVals.add("40");

        return xVals;
    }
 /*   private static ArrayList<Entry> FetchScoreValues(){
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        yVals.add(new Entry(10, 10));
        yVals.add(new Entry(50, 70));
        yVals.add(new Entry(90, 100));

        return yVals;
    }*/

    /*    private static ArrayList<Entry> setXAxisValues(){
            ArrayList<Entry> xVals = new ArrayList<Entry>();
            xVals.add(new Entry(0,2));
            xVals.add(new Entry(4,6));
            xVals.add(new Entry(8,12));


            return xVals;
        }*/
    @Override
    public void onPause() {
        super.onPause();
        Log.e(getClass().getName(), "onPause: ");

    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e(getClass().getName(), "onDetach: ");


    }

    private void setupActionBar() {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.custom_toolbarr);
            actionBar.setShowHideAnimationEnabled(true);
            //  setListenerForActionBarCustomView(actionBarView);
        }
    }

}
