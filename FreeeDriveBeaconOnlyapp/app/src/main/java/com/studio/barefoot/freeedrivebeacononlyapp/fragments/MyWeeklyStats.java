package com.studio.barefoot.freeedrivebeacononlyapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.adapters.MyWeeklyStatsAdapter;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.RidesBeans;
import com.studio.barefoot.freeedrivebeacononlyapp.databases.RideBDD;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppUtils;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.LocaleUtils;
import com.txusballesteros.widgets.FitChart;
import com.txusballesteros.widgets.FitChartValue;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import static com.studio.barefoot.freeedrivebeacononlyapp.fragments.MySafetyFragmentGraph.DemoModeDialog;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyWeeklyStats extends Fragment {

    View tripView;
    Button tripfragment_phone_safety, tripfragment_driving_performance;
    Fragment fragment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    ImageView weeklystatsleftarrow,weeklystatsrightarrow,navigationLeft,navigationRight;
    RelativeLayout relativeLayout_trip_phone_saftey, relativeLayoutdriving, relativeLayout_trip_driving_performance, relativeLayoutPhone;
    TextView txtweeklydatePicker,weeklyscoreAverage,weeklyscorePercentage,firstnameweeklyStats,drivingperformance,emptyRidesTextView;
    FitChart weekly_total_avg_score;
    TextView text_weekly_stats;

    Calendar calender;
    int weekNo = 0;
    int currentWeek = 0;
    int year;
    private int weeklytotalavgScore = 0;
    int counter = 0;
    RecyclerView listViewDrivingPerformance;
    private ArrayList<RidesBeans> ridesBeansArrayList = new ArrayList<>();
    private MyWeeklyStatsAdapter myWeeklyStatsAdapter;
    LinearLayoutManager mLayoutManager;
    View rootView;


    public MyWeeklyStats() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.my_weekly_stats_fragment, container, false);
        // For setting up the different logo of toll bar for demo and paid mode
        RelativeLayout toolbar = (RelativeLayout)rootView.findViewById(R.id.navbar);
        ImageView fd_logo = (ImageView)toolbar.findViewById(R.id.tol_bar_logo);
        if (DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE)){
            fd_logo.setImageResource(R.drawable.logo_freeedrive_demo_mode);
        }  else{
            fd_logo.setImageResource(R.drawable.logo_topbar);
        }
        emptyRidesTextView = (TextView) rootView.findViewById(R.id.emptyTextViewForPhoneSafetRides);
        weekly_total_avg_score = (FitChart) rootView.findViewById(R.id.weekl_stats_fitcharttotalrides_avg_score);
        weeklyscoreAverage = (TextView) rootView.findViewById(R.id.weekly_stats_score_total_rides_avg);
        weeklyscorePercentage = (TextView) rootView.findViewById(R.id.weekly_stats_score_last_ride_percentage_avg_score);
        firstnameweeklyStats = (TextView) rootView.findViewById(R.id.first_name_weekly_stats);
        weeklystatsleftarrow = (ImageView) rootView.findViewById(R.id.weekly_stats_arrow_leftt);
        weeklystatsrightarrow = (ImageView) rootView.findViewById(R.id.weekly_stats_arrow_right);
      /*  imageviewDailyStats = (ImageView) rootView.findViewById(R.id.imageviewDailyStats);
        imageviewFleetStats = (ImageView) rootView.findViewById(R.id.imageviewFleetStats);*/
        navigationLeft = (ImageView) rootView.findViewById(R.id.graph_stats_arrow_left);
        navigationRight = (ImageView) rootView.findViewById(R.id.graph_stats_arrow_right);
        txtweeklydatePicker = (TextView) rootView.findViewById(R.id.weekly_stats_date_picker);
        weeklystatsrightarrow.setVisibility(View.INVISIBLE);
        navigationRight.setVisibility(View.INVISIBLE);
        calender = Calendar.getInstance();
        currentWeek = calender.get(Calendar.WEEK_OF_YEAR);
        year = calender.get(Calendar.YEAR);
        Log.e("Current Week No"," : "+currentWeek);
        Log.e("Current Year "," : "+year);
        txtweeklydatePicker.setText(getActivity().getResources().getString(R.string.weekno)+" "+ currentWeek);
        Log.e("Start Date",":"+AppUtils.getStartEndOFWeek(currentWeek,year,true));
        Log.e("End Date",":"+AppUtils.getStartEndOFWeek(currentWeek,year,false));

        text_weekly_stats = (TextView) rootView.findViewById(R.id.txt_weekly_stats);
        /*text_weekly_stats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new BreakTimeFragment();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(getId(), fragment);
                // fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });*/


        getPhoneSafetyRides(AppUtils.getStartEndOFWeek(currentWeek,year,true),AppUtils.getStartEndOFWeek(currentWeek,year,false));

        listViewDrivingPerformance = (RecyclerView) rootView.findViewById(R.id.list_view_driving_performance);

        mLayoutManager  = new LinearLayoutManager(getActivity());

        listViewDrivingPerformance.setHasFixedSize(true);
        listViewDrivingPerformance.setLayoutManager(mLayoutManager);
        /*listViewDrivingPerformance.addItemDecoration(new PhoneSafetDividerDecor(getActivity(), LinearLayoutManager.VERTICAL));*/
        listViewDrivingPerformance.setItemAnimator(new DefaultItemAnimator());
        myWeeklyStatsAdapter = new MyWeeklyStatsAdapter(ridesBeansArrayList);
        listViewDrivingPerformance.setAdapter(myWeeklyStatsAdapter);
        myWeeklyStatsAdapter.notifyDataSetChanged();

        weeklytotalavgScore = (int) getweeklysavgScore(AppUtils.getStartEndOFWeek(currentWeek,year,true),AppUtils.getStartEndOFWeek(currentWeek,year,false));
        //initializing the fitchartcollection
        if (ridesBeansArrayList.size()>0) {

            if (weeklytotalavgScore >= 0) {
                Collection<FitChartValue> fitChartValues = new ArrayList<>();
                if (weeklytotalavgScore >= 80) {
                    weeklyscoreAverage.setText("" + weeklytotalavgScore + "%");
                    weeklyscoreAverage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
                    weeklyscorePercentage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
                    fitChartValues.add(new FitChartValue(weeklytotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartGreen)));
            /*daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
                } else if (weeklytotalavgScore >= 50 && weeklytotalavgScore < 80) {
                    weeklyscoreAverage.setText("" + weeklytotalavgScore + "%");
                    weeklyscoreAverage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                    weeklyscorePercentage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                    fitChartValues.add(new FitChartValue(weeklytotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartOrange)));
            /*daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
                } else if (weeklytotalavgScore >= 0 && weeklytotalavgScore < 50) {
                    weeklyscoreAverage.setText("" + weeklytotalavgScore + "%");
                    weeklyscoreAverage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartRed));
                    weeklyscorePercentage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartRed));
                    fitChartValues.add(new FitChartValue(weeklytotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartRed)));
            /*daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
                }
                weekly_total_avg_score.setValues(fitChartValues);
            }
        }else {
            weekly_total_avg_score.setValue(100);
        }

        String jsonObjProfile = DataHandler.getStringPreferences(AppConstants.TEMP_DISPLAY_KEY);
        String firstName = "";
        if(jsonObjProfile !=null && !jsonObjProfile.isEmpty()){

            try{
                JSONObject jsonObject = new JSONObject(jsonObjProfile);
                firstName = jsonObject.getString("first_name");
                Log.e("firstName",firstName);
                firstnameweeklyStats.setText(firstName);
            }catch (Exception exception){
                exception.printStackTrace();
            }

        }

       navigationLeft.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               fragment = new MyDailyStatsFragment();
               fragmentManager = getActivity().getSupportFragmentManager();
               fragmentTransaction = fragmentManager.beginTransaction();
               fragmentTransaction.replace(getId(), fragment);
              // fragmentTransaction.addToBackStack(null);
               fragmentTransaction.commit();
               Log.e("fragCountW", "onClick: "+fragmentManager.getBackStackEntryCount());
           }
       });
       weeklystatsleftarrow.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               weeklystatsrightarrow.setVisibility(View.VISIBLE);
               calender.add(Calendar.WEEK_OF_YEAR, -1);
               weekNo = calender.get(Calendar.WEEK_OF_YEAR);
               year = calender.get(Calendar.YEAR);
               txtweeklydatePicker.setText(getActivity().getResources().getString(R.string.weekno)+" "+ weekNo);

               if(weekNo == currentWeek){
                   weeklystatsrightarrow.setVisibility(View.INVISIBLE);
               }
               weeklytotalavgScore = (int) getweeklysavgScore(AppUtils.getStartEndOFWeek(weekNo,year,true),AppUtils.getStartEndOFWeek(weekNo,year,false));
               Collection<FitChartValue> fitChartValues = new ArrayList<>();
               if(weeklytotalavgScore >=80){
                   weeklyscoreAverage.setText(""+weeklytotalavgScore+"%");
                   weeklyscoreAverage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
                   weeklyscorePercentage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
                   fitChartValues.add(new FitChartValue(weeklytotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartGreen)));
            /*daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
               }
               else if(weeklytotalavgScore >= 50 && weeklytotalavgScore < 80){
                   weeklyscoreAverage.setText(""+weeklytotalavgScore+"%");
                   weeklyscoreAverage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                   weeklyscorePercentage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                   fitChartValues.add(new FitChartValue(weeklytotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartOrange)));
            /*daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
               }
               else{
                   weeklyscoreAverage.setText(""+weeklytotalavgScore+"%");
                   weeklyscoreAverage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartRed));
                   weeklyscorePercentage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartRed));
                   fitChartValues.add(new FitChartValue(weeklytotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartRed)));
            /*daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
               }
               weekly_total_avg_score.setValues(fitChartValues);
               getPhoneSafetyRides(AppUtils.getStartEndOFWeek(weekNo,year,true),AppUtils.getStartEndOFWeek(weekNo,year,false));
               myWeeklyStatsAdapter = new MyWeeklyStatsAdapter(ridesBeansArrayList);
               listViewDrivingPerformance.setAdapter(myWeeklyStatsAdapter);
               counter++;
               if(counter == 4){
                   weeklystatsleftarrow.setVisibility(View.INVISIBLE);
               }
           }
       });
        weeklystatsrightarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calender.add(Calendar.WEEK_OF_YEAR, 1);
                weekNo = calender.get(Calendar.WEEK_OF_YEAR);
                txtweeklydatePicker.setText(getActivity().getResources().getString(R.string.weekno)+" "+ weekNo);

                if(weekNo == currentWeek){
                    weeklystatsrightarrow.setVisibility(View.INVISIBLE);
                }
                weeklytotalavgScore = (int) getweeklysavgScore(AppUtils.getStartEndOFWeek(weekNo,year,true),AppUtils.getStartEndOFWeek(weekNo,year,false));
                Collection<FitChartValue> fitChartValues = new ArrayList<>();
                if(weeklytotalavgScore >=80){
                    weeklyscoreAverage.setText(""+weeklytotalavgScore+"%");
                    weeklyscoreAverage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
                    weeklyscorePercentage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartGreen));
                    fitChartValues.add(new FitChartValue(weeklytotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartGreen)));
            /*daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
                }else if(weeklytotalavgScore >= 50 && weeklytotalavgScore < 80){
                    weeklyscoreAverage.setText(""+weeklytotalavgScore+"%");
                    weeklyscoreAverage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                    weeklyscorePercentage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartOrange));
                    fitChartValues.add(new FitChartValue(weeklytotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartOrange)));
            /*daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
                }else{
                    weeklyscoreAverage.setText(""+weeklytotalavgScore+"%");
                    weeklyscoreAverage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartRed));
                    weeklyscorePercentage.setTextColor(getActivity().getResources().getColor(R.color.colorPieChartRed));
                    fitChartValues.add(new FitChartValue(weeklytotalavgScore, getActivity().getResources().getColor(R.color.colorPieChartRed)));
            /*daily_stats_total_rides_avg_score.setValue(dailystatstotalavgScore);*/
                }
                weekly_total_avg_score.setValues(fitChartValues);
                getPhoneSafetyRides(AppUtils.getStartEndOFWeek(weekNo,year,true),AppUtils.getStartEndOFWeek(weekNo,year,false));
                myWeeklyStatsAdapter = new MyWeeklyStatsAdapter(ridesBeansArrayList);
                listViewDrivingPerformance.setAdapter(myWeeklyStatsAdapter);
                counter--;
                weeklystatsleftarrow.setVisibility(View.VISIBLE);
            }
        });

        /*tripfragment_driving_performance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    relativeLayout_trip_driving_performance.setBackground(getActivity().getResources().getDrawable(R.drawable.driving_performance_custom_shape));
                }
                drivingperformance = (TextView) rootView.findViewById(R.id.textviewdrivingperformance);
                drivingperformance.setTextColor(getActivity().getResources().getColor(R.color.white));

                //test commit
                fragment = new MyWeeklyStats();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.trip_fragment, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    relativeLayoutPhone.setBackground(getActivity().getResources().getDrawable(R.drawable.phone_safety_custom_shape));
                }
                phonesafety = (TextView) rootView.findViewById(R.id.textviewmobilehotSpot);
                phonesafety.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
            }
        });


*/
        if(DataHandler.getBooleanPreferences(AppConstants.DEMO_MODE_SUBSCRIPTION_EXPIRED) == true){
            DemoModeDialog();
        }
        return rootView;
    }

    private void getPhoneSafetyRides(String startDate,String endDate) {


        RideBDD tmp = new RideBDD(getContext());
        tmp.open();
        ridesBeansArrayList= tmp.getAllRideByWeek(startDate,endDate);
        Log.e("RideBeansListLength",""+ridesBeansArrayList.size());
        tmp.close();
        if (ridesBeansArrayList.size()==0){
            emptyRidesTextView.setVisibility(View.VISIBLE);
            weeklyscoreAverage.setText("100%");
            weekly_total_avg_score.setValue(100);
            weeklyscoreAverage.setTextColor(getActivity().getResources().getColor(R.color.colorFitChartMenu));
            weeklyscorePercentage.setTextColor(getActivity().getResources().getColor(R.color.colorFitChartMenu));
        }else{
            emptyRidesTextView.setVisibility(View.INVISIBLE);
        }


    }



    private float getweeklysavgScore(String preDate, String endDate){
        RideBDD tmp = new RideBDD(getContext());
        tmp.open();
        float weeklytotalscoreAvg = 0;
        weeklytotalscoreAvg = tmp.weeklytotalscoreAvg(preDate,endDate);
        tmp.close();
        if(weeklytotalscoreAvg == 0){
            weeklyscoreAverage.setText("100%");
            weekly_total_avg_score.setValue(100);
            weeklyscoreAverage.setTextColor(getActivity().getResources().getColor(R.color.colorFitChartMenu));
            weeklyscorePercentage.setTextColor(getActivity().getResources().getColor(R.color.colorFitChartMenu));
            Log.e("No Score Is","Available");
        }
        return weeklytotalscoreAvg;
    }



}

