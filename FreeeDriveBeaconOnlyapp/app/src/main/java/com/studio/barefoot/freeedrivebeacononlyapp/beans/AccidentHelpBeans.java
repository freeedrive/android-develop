package com.studio.barefoot.freeedrivebeacononlyapp.beans;

/**
 * Created by Yasir Barefoot on 9/8/2017.
 */

public class AccidentHelpBeans {

    private String title;
    private int accidenthelpCount;
    private String phonenumber;
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getAccidenthelpCount() {
        return accidenthelpCount;
    }

    public void setAccidenthelpCount(int accidenthelpCount) {
        this.accidenthelpCount = accidenthelpCount;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
}
