package com.studio.barefoot.freeedrivebeacononlyapp.adapters;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.beans.AccidentHelpBeans;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Yasir Barefoot on 9/8/2017.
 */

public class AccidentHelpAdapter extends RecyclerView.Adapter<AccidentHelpAdapter.MyViewHolder> {
    private ArrayList<AccidentHelpBeans> accidentHelpBeansArrayList;
    Context context;
    @Override
    public AccidentHelpAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.accident_adapter, parent, false);
        context = parent.getContext();
        return new AccidentHelpAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final AccidentHelpBeans accidentHelpBeans = accidentHelpBeansArrayList.get(position);

        String title = accidentHelpBeans.getTitle();
        int accidenthelpCount = accidentHelpBeans.getAccidenthelpCount();
     if (accidentHelpBeans.getPhonenumber()!= null && !accidentHelpBeans.getPhonenumber().isEmpty()){
         holder.accidentTitle.setText(accidentHelpBeans.getTitle());
         holder.accidentHelpCount.setText(""+accidenthelpCount);
         holder.accidentTitle.setAllCaps(true);
         holder.accidentTitle.setTextColor(context.getResources().getColor(R.color.orange));
         holder.accidentHelpCount.setTextColor(context.getResources().getColor(R.color.white));
         holder.accidentTitle.setPaintFlags(holder.accidentTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
             holder.accidentHelpCount.setBackground(context.getResources().getDrawable(R.drawable.circular_shape_accident_help_phone_no));
         }
         holder.accidentTitle.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Log.e("Click me",""+accidentHelpBeans.getPhonenumber());
                 Intent intent = new Intent(Intent.ACTION_CALL);
                 intent.setData(Uri.parse("tel:" +accidentHelpBeans.getPhonenumber()));
                 context.startActivity(intent);
             }
         });
     }else{
         holder.accidentTitle.setText(title);
         holder.accidentHelpCount.setText(""+accidenthelpCount);
     }




        /*holder.date.setText(dateofRide);
        holder.time.setText(startAndEndTimeOfRide);
        holder.weekly_no_of_peeks.setText("");*/


    }
    public AccidentHelpAdapter(ArrayList<AccidentHelpBeans> helpBeanses) {
        this.accidentHelpBeansArrayList = helpBeanses;
    }
    @Override
    public int getItemCount() {
        return accidentHelpBeansArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView accidentTitle,accidentHelpCount;


        public MyViewHolder(View view) {
            super(view);
            accidentTitle = (TextView) view.findViewById(R.id.accident_title);
            accidentHelpCount = (Button) view.findViewById(R.id.btn_accidenthelp_count);
        }
    }
}
