package com.studio.barefoot.freeedrivebeacononlyapp.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.studio.barefoot.freeedrivebeacononlyapp.MenuActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;

/**
 * Created by Yasir Barefoot on 9/5/2017.
 */

public class FeedBackDialog extends DialogFragment {
    Button next;

    public FeedBackDialog(){

    }
    public static FeedBackDialog newInstance(String title) {
        FeedBackDialog frag = new FeedBackDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.custom_contact_us_dialog, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        next = (Button) view.findViewById(R.id.btn_contact_us);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MenuActivity.class);
                startActivity(intent);
                dismiss();
                getActivity().finish();
            }
        });
    }
}