package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.R;

import org.apache.http.NameValuePair;
import org.json.JSONObject;

import java.util.List;

import static com.studio.barefoot.freeedrivebeacononlyapp.SmsConfirmationActivity.progressBarDialogSMS;

/**
 * Created by mcs on 12/28/2016.
 */

public class ResendSMSAsyncTask extends BaseAsyncTask {



    public ResendSMSAsyncTask(Context context, String route, List<NameValuePair> pp) {
        super(context, route, pp);
    }


    /**
     * AsyncTask method basic calls during a request, calls the parent's method.
     */
    protected String doInBackground(String... params) {

        return "";
    }
    /**
     * AsyncTask method basic calls after a request.
     */
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s != null) {
            int intResponse = Integer.parseInt(response);
            Log.e("ResponseCode", "" + intResponse);
            Log.e("response", "" + response);
            Log.e("resultat", "" + resultat);
            try{
                progressBarDialogSMS.dismiss();
                progressBarDialogSMS = null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }

            JSONObject jsonObject = new JSONObject();

            switch (intResponse) {
                case 200:
                    try{
                        AlertDialog.Builder error_200 = new AlertDialog.Builder(context);
                        error_200.setMessage(context.getResources().getString(R.string.error_200)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_200.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }


                        break;

                case 401:
                    //Number already exists
                    try{
                        AlertDialog.Builder error_401 = new AlertDialog.Builder(context);
                        error_401.setMessage(context.getResources().getString(R.string.error_login_401aa)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_401.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }

                /*case 200 :

                    break;
                case 409:

                    break;

                case 403:
                    AlertDialog.Builder error_422 = new AlertDialog.Builder(context);
                    error_422.setMessage(context.getResources().getString(R.string.error_login_403)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    error_422.show();
                    break;*/
                    break;
            }
        }else {
            try{
                progressBarDialogSMS.dismiss();
                progressBarDialogSMS = null;
            }catch (NullPointerException exception){
                exception.printStackTrace();
            }
        }
    }
}
