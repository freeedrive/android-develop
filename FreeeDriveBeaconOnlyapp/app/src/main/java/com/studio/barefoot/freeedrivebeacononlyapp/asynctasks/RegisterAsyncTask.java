package com.studio.barefoot.freeedrivebeacononlyapp.asynctasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.studio.barefoot.freeedrivebeacononlyapp.NewPhoneNumberTextActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.QrCodeActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.R;
import com.studio.barefoot.freeedrivebeacononlyapp.RegisterActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.SmsConfirmationActivity;
import com.studio.barefoot.freeedrivebeacononlyapp.dialogs.ProgressBarDialog;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.AppConstants;
import com.studio.barefoot.freeedrivebeacononlyapp.utils.DataHandler;

import org.apache.http.NameValuePair;

import java.util.List;


/**
 * Created by mcs on 12/28/2016.
 */

public class RegisterAsyncTask extends BaseAsyncTask {
    public static ProgressBarDialog progressBarDialogRegister;
    public RegisterAsyncTask(Context context, String route, List<NameValuePair> pp) {
        super(context, route, pp);
        progressBarDialogRegister = new ProgressBarDialog(context);
        progressBarDialogRegister.setTitle(context.getString(R.string.title_progress_dialog));
        progressBarDialogRegister.setMessage(context.getString(R.string.body_progress_dialog));
        progressBarDialogRegister.show();
    }


    /**
     * AsyncTask method basic calls during a request, calls the parent's method.
     */
    protected String doInBackground(String... params) {

        return "";
    }
    /**
     * AsyncTask method basic calls after a request.
     */
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s != null) {
            int intResponse = Integer.parseInt(response);
            Log.e("ResponseCode", "" + intResponse);
            Log.e("response", "" + response);
            try{
                progressBarDialogRegister.dismiss();
                progressBarDialogRegister = null;
            }catch (Exception exception){
                exception.printStackTrace();
            }

            switch (intResponse) {
                case 200 :
                    //sms verification
                    DataHandler.updatePreferences(AppConstants.USER_AWAITING_SMS,true);
                    Intent smsCodeIntent = new Intent(context, SmsConfirmationActivity.class);
                    //  smsCodeIntent.putExtra(AppConstants.EXTRA_KEY_LOGIN_ACTIVITY,AppConstants.EXTRA_VALUE_LOGIN_ACTIVITY);
                    context.startActivity(new Intent(context,SmsConfirmationActivity.class).putExtra(AppConstants.PHONE_NUMBER,DataHandler.getStringPreferences(AppConstants.PHONE_TEMP)));

                    DataHandler.updatePreferences(AppConstants.ACCESS_PRIVATE_HOURS_UPDATED,true);
                    DataHandler.updatePreferences(AppConstants.WABCO_ACCOUNT_VERIFY,false);
                    break;
                case 202 :
                    DataHandler.updatePreferences(AppConstants.WABCO_ACCOUNT_VERIFY,true);
                    context.startActivity(new Intent(context, QrCodeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    break;
                case 409:
                    //phone no already exit
                    try{
                        progressBarDialogRegister.dismiss();
                        progressBarDialogRegister = null;
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    try{
                        AlertDialog.Builder error_401 = new AlertDialog.Builder(context);
                        error_401.setMessage(context.getResources().getString(R.string.error_phoneno_already_exit_409)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_401.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    break;

                case 401:
                    //Number already exists
                    try{
                        AlertDialog.Builder error_401 = new AlertDialog.Builder(context);
                        error_401.setMessage(context.getResources().getString(R.string.error_login_401aa)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_401.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }

                    break;
                case 403:
                    try{
                        AlertDialog.Builder error_403 = new AlertDialog.Builder(context);
                        error_403.setMessage(context.getResources().getString(R.string.error_login_403aa)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_403.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }

                    break;
                case 404:
                    try{
                        AlertDialog.Builder error_404 = new AlertDialog.Builder(context);
                        error_404.setMessage(context.getResources().getString(R.string.error_login_404)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_404.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }

                    break;
                case 422:
                    try{
                        progressBarDialogRegister.dismiss();
                        progressBarDialogRegister = null;
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }
                    try{
                        AlertDialog.Builder error_422 = new AlertDialog.Builder(context);
                        error_422.setMessage(context.getResources().getString(R.string.error_login_422_register)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_422.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }

                    break;
                case 502:
                    try{
                        AlertDialog.Builder error_422 = new AlertDialog.Builder(context);
                        error_422.setMessage(context.getResources().getString(R.string.error_sms_service_502)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_422.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }

                    break;
                case 500:
                    try{
                        AlertDialog.Builder error_500 = new AlertDialog.Builder(context);
                        error_500.setMessage(context.getResources().getString(R.string.error_rate_500)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        error_500.show();
                    }catch (Exception exception){
                        exception.printStackTrace();
                    }

                    break;
                // ecuador phone number exception,it has to surpass
                case 593:
                    DataHandler.updatePreferences(AppConstants.USER_AWAITING_SMS,false);
                    context.startActivity(new Intent(context,QrCodeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    DataHandler.updatePreferences((AppConstants.USER_AWAITING_QR_CODE),true);
                    DataHandler.updatePreferences(AppConstants.ACCESS_PRIVATE_HOURS_UPDATED,true);
                    break;
                //thanks now code is merged complete
            }
        }else {
            try{
                progressBarDialogRegister.dismiss();
                progressBarDialogRegister = null;
            }catch (Exception exception){
                exception.printStackTrace();
            }
        }
    }
}
